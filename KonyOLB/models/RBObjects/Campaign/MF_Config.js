define([],function(){
	var mappings = {
		"scale" : "scale",
		"screenName" : "screenName",
	};
	Object.freeze(mappings);
	
	var typings = {
		"scale" : "string",
		"screenName" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"scale",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Campaign"
	};
	Object.freeze(config);
	
	return config;
})
