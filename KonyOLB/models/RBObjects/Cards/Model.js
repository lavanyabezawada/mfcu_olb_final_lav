define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "Cards", "objectService" : "RBObjects"};
	
	var setterFunctions = {
		accountId : function(val, state){
			context["field"]  = "accountId";
			context["metadata"] = (objectMetadata ? objectMetadata["accountId"] : null);
			state['accountId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Action : function(val, state){
			context["field"]  = "Action";
			context["metadata"] = (objectMetadata ? objectMetadata["Action"] : null);
			state['Action'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardHolderName : function(val, state){
			context["field"]  = "cardHolderName";
			context["metadata"] = (objectMetadata ? objectMetadata["cardHolderName"] : null);
			state['cardHolderName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardId : function(val, state){
			context["field"]  = "cardId";
			context["metadata"] = (objectMetadata ? objectMetadata["cardId"] : null);
			state['cardId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardNumber : function(val, state){
			context["field"]  = "cardNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["cardNumber"] : null);
			state['cardNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardStatus : function(val, state){
			context["field"]  = "cardStatus";
			context["metadata"] = (objectMetadata ? objectMetadata["cardStatus"] : null);
			state['cardStatus'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardType : function(val, state){
			context["field"]  = "cardType";
			context["metadata"] = (objectMetadata ? objectMetadata["cardType"] : null);
			state['cardType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		errmsg : function(val, state){
			context["field"]  = "errmsg";
			context["metadata"] = (objectMetadata ? objectMetadata["errmsg"] : null);
			state['errmsg'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		expiryDate : function(val, state){
			context["field"]  = "expiryDate";
			context["metadata"] = (objectMetadata ? objectMetadata["expiryDate"] : null);
			state['expiryDate'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Reason : function(val, state){
			context["field"]  = "Reason";
			context["metadata"] = (objectMetadata ? objectMetadata["Reason"] : null);
			state['Reason'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		success : function(val, state){
			context["field"]  = "success";
			context["metadata"] = (objectMetadata ? objectMetadata["success"] : null);
			state['success'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		userId : function(val, state){
			context["field"]  = "userId";
			context["metadata"] = (objectMetadata ? objectMetadata["userId"] : null);
			state['userId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		userName : function(val, state){
			context["field"]  = "userName";
			context["metadata"] = (objectMetadata ? objectMetadata["userName"] : null);
			state['userName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		creditLimit : function(val, state){
			context["field"]  = "creditLimit";
			context["metadata"] = (objectMetadata ? objectMetadata["creditLimit"] : null);
			state['creditLimit'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		availableCredit : function(val, state){
			context["field"]  = "availableCredit";
			context["metadata"] = (objectMetadata ? objectMetadata["availableCredit"] : null);
			state['availableCredit'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		serviceProvider : function(val, state){
			context["field"]  = "serviceProvider";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceProvider"] : null);
			state['serviceProvider'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		billingAddress : function(val, state){
			context["field"]  = "billingAddress";
			context["metadata"] = (objectMetadata ? objectMetadata["billingAddress"] : null);
			state['billingAddress'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		cardProductName : function(val, state){
			context["field"]  = "cardProductName";
			context["metadata"] = (objectMetadata ? objectMetadata["cardProductName"] : null);
			state['cardProductName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		secondaryCardHolder : function(val, state){
			context["field"]  = "secondaryCardHolder";
			context["metadata"] = (objectMetadata ? objectMetadata["secondaryCardHolder"] : null);
			state['secondaryCardHolder'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		withdrawlLimit : function(val, state){
			context["field"]  = "withdrawlLimit";
			context["metadata"] = (objectMetadata ? objectMetadata["withdrawlLimit"] : null);
			state['withdrawlLimit'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		accountNumber : function(val, state){
			context["field"]  = "accountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
			state['accountNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		accountName : function(val, state){
			context["field"]  = "accountName";
			context["metadata"] = (objectMetadata ? objectMetadata["accountName"] : null);
			state['accountName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		maskedAccountNumber : function(val, state){
			context["field"]  = "maskedAccountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["maskedAccountNumber"] : null);
			state['maskedAccountNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		maskedCardNumber : function(val, state){
			context["field"]  = "maskedCardNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["maskedCardNumber"] : null);
			state['maskedCardNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isInternational : function(val, state){
			context["field"]  = "isInternational";
			context["metadata"] = (objectMetadata ? objectMetadata["isInternational"] : null);
			state['isInternational'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		ids : function(val, state){
			context["field"]  = "ids";
			context["metadata"] = (objectMetadata ? objectMetadata["ids"] : null);
			state['ids'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Destinations : function(val, state){
			context["field"]  = "Destinations";
			context["metadata"] = (objectMetadata ? objectMetadata["Destinations"] : null);
			state['Destinations'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Cards : function(val, state){
			context["field"]  = "Cards";
			context["metadata"] = (objectMetadata ? objectMetadata["Cards"] : null);
			state['Cards'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Channel_id : function(val, state){
			context["field"]  = "Channel_id";
			context["metadata"] = (objectMetadata ? objectMetadata["Channel_id"] : null);
			state['Channel_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		StartDate : function(val, state){
			context["field"]  = "StartDate";
			context["metadata"] = (objectMetadata ? objectMetadata["StartDate"] : null);
			state['StartDate'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		EndDate : function(val, state){
			context["field"]  = "EndDate";
			context["metadata"] = (objectMetadata ? objectMetadata["EndDate"] : null);
			state['EndDate'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		additionNotes : function(val, state){
			context["field"]  = "additionNotes";
			context["metadata"] = (objectMetadata ? objectMetadata["additionNotes"] : null);
			state['additionNotes'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		phonenumber : function(val, state){
			context["field"]  = "phonenumber";
			context["metadata"] = (objectMetadata ? objectMetadata["phonenumber"] : null);
			state['phonenumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		request_id : function(val, state){
			context["field"]  = "request_id";
			context["metadata"] = (objectMetadata ? objectMetadata["request_id"] : null);
			state['request_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		bankName : function(val, state){
			context["field"]  = "bankName";
			context["metadata"] = (objectMetadata ? objectMetadata["bankName"] : null);
			state['bankName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		AccountType : function(val, state){
			context["field"]  = "AccountType";
			context["metadata"] = (objectMetadata ? objectMetadata["AccountType"] : null);
			state['AccountType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		RequestCode : function(val, state){
			context["field"]  = "RequestCode";
			context["metadata"] = (objectMetadata ? objectMetadata["RequestCode"] : null);
			state['RequestCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		RequestReason : function(val, state){
			context["field"]  = "RequestReason";
			context["metadata"] = (objectMetadata ? objectMetadata["RequestReason"] : null);
			state['RequestReason'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Channel : function(val, state){
			context["field"]  = "Channel";
			context["metadata"] = (objectMetadata ? objectMetadata["Channel"] : null);
			state['Channel'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Address_id : function(val, state){
			context["field"]  = "Address_id";
			context["metadata"] = (objectMetadata ? objectMetadata["Address_id"] : null);
			state['Address_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		communication_id : function(val, state){
			context["field"]  = "communication_id";
			context["metadata"] = (objectMetadata ? objectMetadata["communication_id"] : null);
			state['communication_id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		CardNumbers : function(val, state){
			context["field"]  = "CardNumbers";
			context["metadata"] = (objectMetadata ? objectMetadata["CardNumbers"] : null);
			state['CardNumbers'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		lastNinetyDays : function(val, state){
			context["field"]  = "lastNinetyDays";
			context["metadata"] = (objectMetadata ? objectMetadata["lastNinetyDays"] : null);
			state['lastNinetyDays'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		errorMessage : function(val, state){
			context["field"]  = "errorMessage";
			context["metadata"] = (objectMetadata ? objectMetadata["errorMessage"] : null);
			state['errorMessage'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		dbpErrCode : function(val, state){
			context["field"]  = "dbpErrCode";
			context["metadata"] = (objectMetadata ? objectMetadata["dbpErrCode"] : null);
			state['dbpErrCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		dbpErrMsg : function(val, state){
			context["field"]  = "dbpErrMsg";
			context["metadata"] = (objectMetadata ? objectMetadata["dbpErrMsg"] : null);
			state['dbpErrMsg'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		newPin : function(val, state){
			context["field"]  = "newPin";
			context["metadata"] = (objectMetadata ? objectMetadata["newPin"] : null);
			state['newPin'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		MFAAttributes : function(val, state){
			context["field"]  = "MFAAttributes";
			context["metadata"] = (objectMetadata ? objectMetadata["MFAAttributes"] : null);
			state['MFAAttributes'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		serviceName : function(val, state){
			context["field"]  = "serviceName";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceName"] : null);
			state['serviceName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		serviceKey : function(val, state){
			context["field"]  = "serviceKey";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceKey"] : null);
			state['serviceKey'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Otp : function(val, state){
			context["field"]  = "Otp";
			context["metadata"] = (objectMetadata ? objectMetadata["Otp"] : null);
			state['Otp'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		OtpGenaratedts : function(val, state){
			context["field"]  = "OtpGenaratedts";
			context["metadata"] = (objectMetadata ? objectMetadata["OtpGenaratedts"] : null);
			state['OtpGenaratedts'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		securityQuestions : function(val, state){
			context["field"]  = "securityQuestions";
			context["metadata"] = (objectMetadata ? objectMetadata["securityQuestions"] : null);
			state['securityQuestions'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		customerAnswer : function(val, state){
			context["field"]  = "customerAnswer";
			context["metadata"] = (objectMetadata ? objectMetadata["customerAnswer"] : null);
			state['customerAnswer'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		questionId : function(val, state){
			context["field"]  = "questionId";
			context["metadata"] = (objectMetadata ? objectMetadata["questionId"] : null);
			state['questionId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		maxFailedAttemptsAllowed : function(val, state){
			context["field"]  = "maxFailedAttemptsAllowed";
			context["metadata"] = (objectMetadata ? objectMetadata["maxFailedAttemptsAllowed"] : null);
			state['maxFailedAttemptsAllowed'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		ValidDate : function(val, state){
			context["field"]  = "ValidDate";
			context["metadata"] = (objectMetadata ? objectMetadata["ValidDate"] : null);
			state['ValidDate'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		unsuccessfulLoginAttempts : function(val, state){
			context["field"]  = "unsuccessfulLoginAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["unsuccessfulLoginAttempts"] : null);
			state['unsuccessfulLoginAttempts'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isUserAccountLocked : function(val, state){
			context["field"]  = "isUserAccountLocked";
			context["metadata"] = (objectMetadata ? objectMetadata["isUserAccountLocked"] : null);
			state['isUserAccountLocked'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Phone : function(val, state){
			context["field"]  = "Phone";
			context["metadata"] = (objectMetadata ? objectMetadata["Phone"] : null);
			state['Phone'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		Email : function(val, state){
			context["field"]  = "Email";
			context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
			state['Email'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		sacMaxResendRequestsAllowed : function(val, state){
			context["field"]  = "sacMaxResendRequestsAllowed";
			context["metadata"] = (objectMetadata ? objectMetadata["sacMaxResendRequestsAllowed"] : null);
			state['sacMaxResendRequestsAllowed'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		remainingResendAttempts : function(val, state){
			context["field"]  = "remainingResendAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["remainingResendAttempts"] : null);
			state['remainingResendAttempts'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		remainingFailedAttempts : function(val, state){
			context["field"]  = "remainingFailedAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["remainingFailedAttempts"] : null);
			state['remainingFailedAttempts'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isOtpVerified : function(val, state){
			context["field"]  = "isOtpVerified";
			context["metadata"] = (objectMetadata ? objectMetadata["isOtpVerified"] : null);
			state['isOtpVerified'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		lockUser : function(val, state){
			context["field"]  = "lockUser";
			context["metadata"] = (objectMetadata ? objectMetadata["lockUser"] : null);
			state['lockUser'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		logoutUser : function(val, state){
			context["field"]  = "logoutUser";
			context["metadata"] = (objectMetadata ? objectMetadata["logoutUser"] : null);
			state['logoutUser'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		lockoutTime : function(val, state){
			context["field"]  = "lockoutTime";
			context["metadata"] = (objectMetadata ? objectMetadata["lockoutTime"] : null);
			state['lockoutTime'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		isOTPExpired : function(val, state){
			context["field"]  = "isOTPExpired";
			context["metadata"] = (objectMetadata ? objectMetadata["isOTPExpired"] : null);
			state['isOTPExpired'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
	};
	
	
	//Create the Model Class
	function Cards(defaultValues){
		var privateState = {};
			context["field"]  = "accountId";
			context["metadata"] = (objectMetadata ? objectMetadata["accountId"] : null);
			privateState.accountId = defaultValues?(defaultValues["accountId"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountId"], context):null):null;
			context["field"]  = "Action";
			context["metadata"] = (objectMetadata ? objectMetadata["Action"] : null);
			privateState.Action = defaultValues?(defaultValues["Action"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Action"], context):null):null;
			context["field"]  = "cardHolderName";
			context["metadata"] = (objectMetadata ? objectMetadata["cardHolderName"] : null);
			privateState.cardHolderName = defaultValues?(defaultValues["cardHolderName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardHolderName"], context):null):null;
			context["field"]  = "cardId";
			context["metadata"] = (objectMetadata ? objectMetadata["cardId"] : null);
			privateState.cardId = defaultValues?(defaultValues["cardId"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardId"], context):null):null;
			context["field"]  = "cardNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["cardNumber"] : null);
			privateState.cardNumber = defaultValues?(defaultValues["cardNumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardNumber"], context):null):null;
			context["field"]  = "cardStatus";
			context["metadata"] = (objectMetadata ? objectMetadata["cardStatus"] : null);
			privateState.cardStatus = defaultValues?(defaultValues["cardStatus"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardStatus"], context):null):null;
			context["field"]  = "cardType";
			context["metadata"] = (objectMetadata ? objectMetadata["cardType"] : null);
			privateState.cardType = defaultValues?(defaultValues["cardType"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardType"], context):null):null;
			context["field"]  = "errmsg";
			context["metadata"] = (objectMetadata ? objectMetadata["errmsg"] : null);
			privateState.errmsg = defaultValues?(defaultValues["errmsg"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["errmsg"], context):null):null;
			context["field"]  = "expiryDate";
			context["metadata"] = (objectMetadata ? objectMetadata["expiryDate"] : null);
			privateState.expiryDate = defaultValues?(defaultValues["expiryDate"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["expiryDate"], context):null):null;
			context["field"]  = "Reason";
			context["metadata"] = (objectMetadata ? objectMetadata["Reason"] : null);
			privateState.Reason = defaultValues?(defaultValues["Reason"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Reason"], context):null):null;
			context["field"]  = "success";
			context["metadata"] = (objectMetadata ? objectMetadata["success"] : null);
			privateState.success = defaultValues?(defaultValues["success"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["success"], context):null):null;
			context["field"]  = "userId";
			context["metadata"] = (objectMetadata ? objectMetadata["userId"] : null);
			privateState.userId = defaultValues?(defaultValues["userId"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["userId"], context):null):null;
			context["field"]  = "userName";
			context["metadata"] = (objectMetadata ? objectMetadata["userName"] : null);
			privateState.userName = defaultValues?(defaultValues["userName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["userName"], context):null):null;
			context["field"]  = "creditLimit";
			context["metadata"] = (objectMetadata ? objectMetadata["creditLimit"] : null);
			privateState.creditLimit = defaultValues?(defaultValues["creditLimit"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["creditLimit"], context):null):null;
			context["field"]  = "availableCredit";
			context["metadata"] = (objectMetadata ? objectMetadata["availableCredit"] : null);
			privateState.availableCredit = defaultValues?(defaultValues["availableCredit"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["availableCredit"], context):null):null;
			context["field"]  = "serviceProvider";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceProvider"] : null);
			privateState.serviceProvider = defaultValues?(defaultValues["serviceProvider"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["serviceProvider"], context):null):null;
			context["field"]  = "billingAddress";
			context["metadata"] = (objectMetadata ? objectMetadata["billingAddress"] : null);
			privateState.billingAddress = defaultValues?(defaultValues["billingAddress"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["billingAddress"], context):null):null;
			context["field"]  = "cardProductName";
			context["metadata"] = (objectMetadata ? objectMetadata["cardProductName"] : null);
			privateState.cardProductName = defaultValues?(defaultValues["cardProductName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["cardProductName"], context):null):null;
			context["field"]  = "secondaryCardHolder";
			context["metadata"] = (objectMetadata ? objectMetadata["secondaryCardHolder"] : null);
			privateState.secondaryCardHolder = defaultValues?(defaultValues["secondaryCardHolder"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["secondaryCardHolder"], context):null):null;
			context["field"]  = "withdrawlLimit";
			context["metadata"] = (objectMetadata ? objectMetadata["withdrawlLimit"] : null);
			privateState.withdrawlLimit = defaultValues?(defaultValues["withdrawlLimit"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["withdrawlLimit"], context):null):null;
			context["field"]  = "accountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountNumber"], context):null):null;
			context["field"]  = "accountName";
			context["metadata"] = (objectMetadata ? objectMetadata["accountName"] : null);
			privateState.accountName = defaultValues?(defaultValues["accountName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountName"], context):null):null;
			context["field"]  = "maskedAccountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["maskedAccountNumber"] : null);
			privateState.maskedAccountNumber = defaultValues?(defaultValues["maskedAccountNumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["maskedAccountNumber"], context):null):null;
			context["field"]  = "maskedCardNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["maskedCardNumber"] : null);
			privateState.maskedCardNumber = defaultValues?(defaultValues["maskedCardNumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["maskedCardNumber"], context):null):null;
			context["field"]  = "isInternational";
			context["metadata"] = (objectMetadata ? objectMetadata["isInternational"] : null);
			privateState.isInternational = defaultValues?(defaultValues["isInternational"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isInternational"], context):null):null;
			context["field"]  = "ids";
			context["metadata"] = (objectMetadata ? objectMetadata["ids"] : null);
			privateState.ids = defaultValues?(defaultValues["ids"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ids"], context):null):null;
			context["field"]  = "Destinations";
			context["metadata"] = (objectMetadata ? objectMetadata["Destinations"] : null);
			privateState.Destinations = defaultValues?(defaultValues["Destinations"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Destinations"], context):null):null;
			context["field"]  = "Cards";
			context["metadata"] = (objectMetadata ? objectMetadata["Cards"] : null);
			privateState.Cards = defaultValues?(defaultValues["Cards"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Cards"], context):null):null;
			context["field"]  = "Channel_id";
			context["metadata"] = (objectMetadata ? objectMetadata["Channel_id"] : null);
			privateState.Channel_id = defaultValues?(defaultValues["Channel_id"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Channel_id"], context):null):null;
			context["field"]  = "StartDate";
			context["metadata"] = (objectMetadata ? objectMetadata["StartDate"] : null);
			privateState.StartDate = defaultValues?(defaultValues["StartDate"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["StartDate"], context):null):null;
			context["field"]  = "EndDate";
			context["metadata"] = (objectMetadata ? objectMetadata["EndDate"] : null);
			privateState.EndDate = defaultValues?(defaultValues["EndDate"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["EndDate"], context):null):null;
			context["field"]  = "additionNotes";
			context["metadata"] = (objectMetadata ? objectMetadata["additionNotes"] : null);
			privateState.additionNotes = defaultValues?(defaultValues["additionNotes"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["additionNotes"], context):null):null;
			context["field"]  = "phonenumber";
			context["metadata"] = (objectMetadata ? objectMetadata["phonenumber"] : null);
			privateState.phonenumber = defaultValues?(defaultValues["phonenumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phonenumber"], context):null):null;
			context["field"]  = "request_id";
			context["metadata"] = (objectMetadata ? objectMetadata["request_id"] : null);
			privateState.request_id = defaultValues?(defaultValues["request_id"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["request_id"], context):null):null;
			context["field"]  = "bankName";
			context["metadata"] = (objectMetadata ? objectMetadata["bankName"] : null);
			privateState.bankName = defaultValues?(defaultValues["bankName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["bankName"], context):null):null;
			context["field"]  = "AccountType";
			context["metadata"] = (objectMetadata ? objectMetadata["AccountType"] : null);
			privateState.AccountType = defaultValues?(defaultValues["AccountType"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["AccountType"], context):null):null;
			context["field"]  = "RequestCode";
			context["metadata"] = (objectMetadata ? objectMetadata["RequestCode"] : null);
			privateState.RequestCode = defaultValues?(defaultValues["RequestCode"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["RequestCode"], context):null):null;
			context["field"]  = "RequestReason";
			context["metadata"] = (objectMetadata ? objectMetadata["RequestReason"] : null);
			privateState.RequestReason = defaultValues?(defaultValues["RequestReason"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["RequestReason"], context):null):null;
			context["field"]  = "Channel";
			context["metadata"] = (objectMetadata ? objectMetadata["Channel"] : null);
			privateState.Channel = defaultValues?(defaultValues["Channel"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Channel"], context):null):null;
			context["field"]  = "Address_id";
			context["metadata"] = (objectMetadata ? objectMetadata["Address_id"] : null);
			privateState.Address_id = defaultValues?(defaultValues["Address_id"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Address_id"], context):null):null;
			context["field"]  = "communication_id";
			context["metadata"] = (objectMetadata ? objectMetadata["communication_id"] : null);
			privateState.communication_id = defaultValues?(defaultValues["communication_id"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["communication_id"], context):null):null;
			context["field"]  = "CardNumbers";
			context["metadata"] = (objectMetadata ? objectMetadata["CardNumbers"] : null);
			privateState.CardNumbers = defaultValues?(defaultValues["CardNumbers"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["CardNumbers"], context):null):null;
			context["field"]  = "lastNinetyDays";
			context["metadata"] = (objectMetadata ? objectMetadata["lastNinetyDays"] : null);
			privateState.lastNinetyDays = defaultValues?(defaultValues["lastNinetyDays"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lastNinetyDays"], context):null):null;
			context["field"]  = "errorMessage";
			context["metadata"] = (objectMetadata ? objectMetadata["errorMessage"] : null);
			privateState.errorMessage = defaultValues?(defaultValues["errorMessage"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["errorMessage"], context):null):null;
			context["field"]  = "dbpErrCode";
			context["metadata"] = (objectMetadata ? objectMetadata["dbpErrCode"] : null);
			privateState.dbpErrCode = defaultValues?(defaultValues["dbpErrCode"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dbpErrCode"], context):null):null;
			context["field"]  = "dbpErrMsg";
			context["metadata"] = (objectMetadata ? objectMetadata["dbpErrMsg"] : null);
			privateState.dbpErrMsg = defaultValues?(defaultValues["dbpErrMsg"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dbpErrMsg"], context):null):null;
			context["field"]  = "newPin";
			context["metadata"] = (objectMetadata ? objectMetadata["newPin"] : null);
			privateState.newPin = defaultValues?(defaultValues["newPin"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["newPin"], context):null):null;
			context["field"]  = "MFAAttributes";
			context["metadata"] = (objectMetadata ? objectMetadata["MFAAttributes"] : null);
			privateState.MFAAttributes = defaultValues?(defaultValues["MFAAttributes"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["MFAAttributes"], context):null):null;
			context["field"]  = "serviceName";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceName"] : null);
			privateState.serviceName = defaultValues?(defaultValues["serviceName"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["serviceName"], context):null):null;
			context["field"]  = "serviceKey";
			context["metadata"] = (objectMetadata ? objectMetadata["serviceKey"] : null);
			privateState.serviceKey = defaultValues?(defaultValues["serviceKey"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["serviceKey"], context):null):null;
			context["field"]  = "Otp";
			context["metadata"] = (objectMetadata ? objectMetadata["Otp"] : null);
			privateState.Otp = defaultValues?(defaultValues["Otp"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Otp"], context):null):null;
			context["field"]  = "OtpGenaratedts";
			context["metadata"] = (objectMetadata ? objectMetadata["OtpGenaratedts"] : null);
			privateState.OtpGenaratedts = defaultValues?(defaultValues["OtpGenaratedts"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["OtpGenaratedts"], context):null):null;
			context["field"]  = "securityQuestions";
			context["metadata"] = (objectMetadata ? objectMetadata["securityQuestions"] : null);
			privateState.securityQuestions = defaultValues?(defaultValues["securityQuestions"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["securityQuestions"], context):null):null;
			context["field"]  = "customerAnswer";
			context["metadata"] = (objectMetadata ? objectMetadata["customerAnswer"] : null);
			privateState.customerAnswer = defaultValues?(defaultValues["customerAnswer"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerAnswer"], context):null):null;
			context["field"]  = "questionId";
			context["metadata"] = (objectMetadata ? objectMetadata["questionId"] : null);
			privateState.questionId = defaultValues?(defaultValues["questionId"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["questionId"], context):null):null;
			context["field"]  = "maxFailedAttemptsAllowed";
			context["metadata"] = (objectMetadata ? objectMetadata["maxFailedAttemptsAllowed"] : null);
			privateState.maxFailedAttemptsAllowed = defaultValues?(defaultValues["maxFailedAttemptsAllowed"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["maxFailedAttemptsAllowed"], context):null):null;
			context["field"]  = "ValidDate";
			context["metadata"] = (objectMetadata ? objectMetadata["ValidDate"] : null);
			privateState.ValidDate = defaultValues?(defaultValues["ValidDate"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ValidDate"], context):null):null;
			context["field"]  = "unsuccessfulLoginAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["unsuccessfulLoginAttempts"] : null);
			privateState.unsuccessfulLoginAttempts = defaultValues?(defaultValues["unsuccessfulLoginAttempts"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["unsuccessfulLoginAttempts"], context):null):null;
			context["field"]  = "isUserAccountLocked";
			context["metadata"] = (objectMetadata ? objectMetadata["isUserAccountLocked"] : null);
			privateState.isUserAccountLocked = defaultValues?(defaultValues["isUserAccountLocked"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isUserAccountLocked"], context):null):null;
			context["field"]  = "Phone";
			context["metadata"] = (objectMetadata ? objectMetadata["Phone"] : null);
			privateState.Phone = defaultValues?(defaultValues["Phone"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Phone"], context):null):null;
			context["field"]  = "Email";
			context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
			privateState.Email = defaultValues?(defaultValues["Email"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Email"], context):null):null;
			context["field"]  = "sacMaxResendRequestsAllowed";
			context["metadata"] = (objectMetadata ? objectMetadata["sacMaxResendRequestsAllowed"] : null);
			privateState.sacMaxResendRequestsAllowed = defaultValues?(defaultValues["sacMaxResendRequestsAllowed"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["sacMaxResendRequestsAllowed"], context):null):null;
			context["field"]  = "remainingResendAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["remainingResendAttempts"] : null);
			privateState.remainingResendAttempts = defaultValues?(defaultValues["remainingResendAttempts"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["remainingResendAttempts"], context):null):null;
			context["field"]  = "remainingFailedAttempts";
			context["metadata"] = (objectMetadata ? objectMetadata["remainingFailedAttempts"] : null);
			privateState.remainingFailedAttempts = defaultValues?(defaultValues["remainingFailedAttempts"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["remainingFailedAttempts"], context):null):null;
			context["field"]  = "isOtpVerified";
			context["metadata"] = (objectMetadata ? objectMetadata["isOtpVerified"] : null);
			privateState.isOtpVerified = defaultValues?(defaultValues["isOtpVerified"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isOtpVerified"], context):null):null;
			context["field"]  = "lockUser";
			context["metadata"] = (objectMetadata ? objectMetadata["lockUser"] : null);
			privateState.lockUser = defaultValues?(defaultValues["lockUser"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lockUser"], context):null):null;
			context["field"]  = "logoutUser";
			context["metadata"] = (objectMetadata ? objectMetadata["logoutUser"] : null);
			privateState.logoutUser = defaultValues?(defaultValues["logoutUser"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["logoutUser"], context):null):null;
			context["field"]  = "lockoutTime";
			context["metadata"] = (objectMetadata ? objectMetadata["lockoutTime"] : null);
			privateState.lockoutTime = defaultValues?(defaultValues["lockoutTime"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lockoutTime"], context):null):null;
			context["field"]  = "isOTPExpired";
			context["metadata"] = (objectMetadata ? objectMetadata["isOTPExpired"] : null);
			privateState.isOTPExpired = defaultValues?(defaultValues["isOTPExpired"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isOTPExpired"], context):null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountId" : {
					get : function(){
						context["field"]  = "accountId";
			        	context["metadata"] = (objectMetadata ? objectMetadata["accountId"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountId, context);},
					set : function(val){
						setterFunctions['accountId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Action" : {
					get : function(){
						context["field"]  = "Action";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Action"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Action, context);},
					set : function(val){
						setterFunctions['Action'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardHolderName" : {
					get : function(){
						context["field"]  = "cardHolderName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardHolderName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardHolderName, context);},
					set : function(val){
						setterFunctions['cardHolderName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardId" : {
					get : function(){
						context["field"]  = "cardId";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardId"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardId, context);},
					set : function(val){
						setterFunctions['cardId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardNumber" : {
					get : function(){
						context["field"]  = "cardNumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardNumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardNumber, context);},
					set : function(val){
						setterFunctions['cardNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardStatus" : {
					get : function(){
						context["field"]  = "cardStatus";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardStatus"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardStatus, context);},
					set : function(val){
						setterFunctions['cardStatus'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardType" : {
					get : function(){
						context["field"]  = "cardType";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardType"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardType, context);},
					set : function(val){
						setterFunctions['cardType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"errmsg" : {
					get : function(){
						context["field"]  = "errmsg";
			        	context["metadata"] = (objectMetadata ? objectMetadata["errmsg"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.errmsg, context);},
					set : function(val){
						setterFunctions['errmsg'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"expiryDate" : {
					get : function(){
						context["field"]  = "expiryDate";
			        	context["metadata"] = (objectMetadata ? objectMetadata["expiryDate"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.expiryDate, context);},
					set : function(val){
						setterFunctions['expiryDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Reason" : {
					get : function(){
						context["field"]  = "Reason";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Reason"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Reason, context);},
					set : function(val){
						setterFunctions['Reason'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){
						context["field"]  = "success";
			        	context["metadata"] = (objectMetadata ? objectMetadata["success"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.success, context);},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"userId" : {
					get : function(){
						context["field"]  = "userId";
			        	context["metadata"] = (objectMetadata ? objectMetadata["userId"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.userId, context);},
					set : function(val){
						setterFunctions['userId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"userName" : {
					get : function(){
						context["field"]  = "userName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["userName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.userName, context);},
					set : function(val){
						setterFunctions['userName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"creditLimit" : {
					get : function(){
						context["field"]  = "creditLimit";
			        	context["metadata"] = (objectMetadata ? objectMetadata["creditLimit"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.creditLimit, context);},
					set : function(val){
						setterFunctions['creditLimit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"availableCredit" : {
					get : function(){
						context["field"]  = "availableCredit";
			        	context["metadata"] = (objectMetadata ? objectMetadata["availableCredit"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.availableCredit, context);},
					set : function(val){
						setterFunctions['availableCredit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"serviceProvider" : {
					get : function(){
						context["field"]  = "serviceProvider";
			        	context["metadata"] = (objectMetadata ? objectMetadata["serviceProvider"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.serviceProvider, context);},
					set : function(val){
						setterFunctions['serviceProvider'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billingAddress" : {
					get : function(){
						context["field"]  = "billingAddress";
			        	context["metadata"] = (objectMetadata ? objectMetadata["billingAddress"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.billingAddress, context);},
					set : function(val){
						setterFunctions['billingAddress'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardProductName" : {
					get : function(){
						context["field"]  = "cardProductName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["cardProductName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.cardProductName, context);},
					set : function(val){
						setterFunctions['cardProductName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"secondaryCardHolder" : {
					get : function(){
						context["field"]  = "secondaryCardHolder";
			        	context["metadata"] = (objectMetadata ? objectMetadata["secondaryCardHolder"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.secondaryCardHolder, context);},
					set : function(val){
						setterFunctions['secondaryCardHolder'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"withdrawlLimit" : {
					get : function(){
						context["field"]  = "withdrawlLimit";
			        	context["metadata"] = (objectMetadata ? objectMetadata["withdrawlLimit"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.withdrawlLimit, context);},
					set : function(val){
						setterFunctions['withdrawlLimit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountNumber" : {
					get : function(){
						context["field"]  = "accountNumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountNumber, context);},
					set : function(val){
						setterFunctions['accountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountName" : {
					get : function(){
						context["field"]  = "accountName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["accountName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountName, context);},
					set : function(val){
						setterFunctions['accountName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"maskedAccountNumber" : {
					get : function(){
						context["field"]  = "maskedAccountNumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["maskedAccountNumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.maskedAccountNumber, context);},
					set : function(val){
						setterFunctions['maskedAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"maskedCardNumber" : {
					get : function(){
						context["field"]  = "maskedCardNumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["maskedCardNumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.maskedCardNumber, context);},
					set : function(val){
						setterFunctions['maskedCardNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isInternational" : {
					get : function(){
						context["field"]  = "isInternational";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isInternational"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isInternational, context);},
					set : function(val){
						setterFunctions['isInternational'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ids" : {
					get : function(){
						context["field"]  = "ids";
			        	context["metadata"] = (objectMetadata ? objectMetadata["ids"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ids, context);},
					set : function(val){
						setterFunctions['ids'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Destinations" : {
					get : function(){
						context["field"]  = "Destinations";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Destinations"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Destinations, context);},
					set : function(val){
						setterFunctions['Destinations'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Cards" : {
					get : function(){
						context["field"]  = "Cards";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Cards"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Cards, context);},
					set : function(val){
						setterFunctions['Cards'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Channel_id" : {
					get : function(){
						context["field"]  = "Channel_id";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Channel_id"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Channel_id, context);},
					set : function(val){
						setterFunctions['Channel_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"StartDate" : {
					get : function(){
						context["field"]  = "StartDate";
			        	context["metadata"] = (objectMetadata ? objectMetadata["StartDate"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.StartDate, context);},
					set : function(val){
						setterFunctions['StartDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"EndDate" : {
					get : function(){
						context["field"]  = "EndDate";
			        	context["metadata"] = (objectMetadata ? objectMetadata["EndDate"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.EndDate, context);},
					set : function(val){
						setterFunctions['EndDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"additionNotes" : {
					get : function(){
						context["field"]  = "additionNotes";
			        	context["metadata"] = (objectMetadata ? objectMetadata["additionNotes"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.additionNotes, context);},
					set : function(val){
						setterFunctions['additionNotes'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"phonenumber" : {
					get : function(){
						context["field"]  = "phonenumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["phonenumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phonenumber, context);},
					set : function(val){
						setterFunctions['phonenumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"request_id" : {
					get : function(){
						context["field"]  = "request_id";
			        	context["metadata"] = (objectMetadata ? objectMetadata["request_id"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.request_id, context);},
					set : function(val){
						setterFunctions['request_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankName" : {
					get : function(){
						context["field"]  = "bankName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["bankName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.bankName, context);},
					set : function(val){
						setterFunctions['bankName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"AccountType" : {
					get : function(){
						context["field"]  = "AccountType";
			        	context["metadata"] = (objectMetadata ? objectMetadata["AccountType"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.AccountType, context);},
					set : function(val){
						setterFunctions['AccountType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"RequestCode" : {
					get : function(){
						context["field"]  = "RequestCode";
			        	context["metadata"] = (objectMetadata ? objectMetadata["RequestCode"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.RequestCode, context);},
					set : function(val){
						setterFunctions['RequestCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"RequestReason" : {
					get : function(){
						context["field"]  = "RequestReason";
			        	context["metadata"] = (objectMetadata ? objectMetadata["RequestReason"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.RequestReason, context);},
					set : function(val){
						setterFunctions['RequestReason'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Channel" : {
					get : function(){
						context["field"]  = "Channel";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Channel"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Channel, context);},
					set : function(val){
						setterFunctions['Channel'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Address_id" : {
					get : function(){
						context["field"]  = "Address_id";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Address_id"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Address_id, context);},
					set : function(val){
						setterFunctions['Address_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"communication_id" : {
					get : function(){
						context["field"]  = "communication_id";
			        	context["metadata"] = (objectMetadata ? objectMetadata["communication_id"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.communication_id, context);},
					set : function(val){
						setterFunctions['communication_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"CardNumbers" : {
					get : function(){
						context["field"]  = "CardNumbers";
			        	context["metadata"] = (objectMetadata ? objectMetadata["CardNumbers"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.CardNumbers, context);},
					set : function(val){
						setterFunctions['CardNumbers'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lastNinetyDays" : {
					get : function(){
						context["field"]  = "lastNinetyDays";
			        	context["metadata"] = (objectMetadata ? objectMetadata["lastNinetyDays"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lastNinetyDays, context);},
					set : function(val){
						setterFunctions['lastNinetyDays'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"errorMessage" : {
					get : function(){
						context["field"]  = "errorMessage";
			        	context["metadata"] = (objectMetadata ? objectMetadata["errorMessage"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.errorMessage, context);},
					set : function(val){
						setterFunctions['errorMessage'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"dbpErrCode" : {
					get : function(){
						context["field"]  = "dbpErrCode";
			        	context["metadata"] = (objectMetadata ? objectMetadata["dbpErrCode"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dbpErrCode, context);},
					set : function(val){
						setterFunctions['dbpErrCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"dbpErrMsg" : {
					get : function(){
						context["field"]  = "dbpErrMsg";
			        	context["metadata"] = (objectMetadata ? objectMetadata["dbpErrMsg"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dbpErrMsg, context);},
					set : function(val){
						setterFunctions['dbpErrMsg'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"newPin" : {
					get : function(){
						context["field"]  = "newPin";
			        	context["metadata"] = (objectMetadata ? objectMetadata["newPin"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.newPin, context);},
					set : function(val){
						setterFunctions['newPin'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"MFAAttributes" : {
					get : function(){
						context["field"]  = "MFAAttributes";
			        	context["metadata"] = (objectMetadata ? objectMetadata["MFAAttributes"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.MFAAttributes, context);},
					set : function(val){
						setterFunctions['MFAAttributes'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"serviceName" : {
					get : function(){
						context["field"]  = "serviceName";
			        	context["metadata"] = (objectMetadata ? objectMetadata["serviceName"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.serviceName, context);},
					set : function(val){
						setterFunctions['serviceName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"serviceKey" : {
					get : function(){
						context["field"]  = "serviceKey";
			        	context["metadata"] = (objectMetadata ? objectMetadata["serviceKey"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.serviceKey, context);},
					set : function(val){
						setterFunctions['serviceKey'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Otp" : {
					get : function(){
						context["field"]  = "Otp";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Otp"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Otp, context);},
					set : function(val){
						setterFunctions['Otp'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"OtpGenaratedts" : {
					get : function(){
						context["field"]  = "OtpGenaratedts";
			        	context["metadata"] = (objectMetadata ? objectMetadata["OtpGenaratedts"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.OtpGenaratedts, context);},
					set : function(val){
						setterFunctions['OtpGenaratedts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"securityQuestions" : {
					get : function(){
						context["field"]  = "securityQuestions";
			        	context["metadata"] = (objectMetadata ? objectMetadata["securityQuestions"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.securityQuestions, context);},
					set : function(val){
						setterFunctions['securityQuestions'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"customerAnswer" : {
					get : function(){
						context["field"]  = "customerAnswer";
			        	context["metadata"] = (objectMetadata ? objectMetadata["customerAnswer"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerAnswer, context);},
					set : function(val){
						setterFunctions['customerAnswer'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"questionId" : {
					get : function(){
						context["field"]  = "questionId";
			        	context["metadata"] = (objectMetadata ? objectMetadata["questionId"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.questionId, context);},
					set : function(val){
						setterFunctions['questionId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"maxFailedAttemptsAllowed" : {
					get : function(){
						context["field"]  = "maxFailedAttemptsAllowed";
			        	context["metadata"] = (objectMetadata ? objectMetadata["maxFailedAttemptsAllowed"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.maxFailedAttemptsAllowed, context);},
					set : function(val){
						setterFunctions['maxFailedAttemptsAllowed'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ValidDate" : {
					get : function(){
						context["field"]  = "ValidDate";
			        	context["metadata"] = (objectMetadata ? objectMetadata["ValidDate"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ValidDate, context);},
					set : function(val){
						setterFunctions['ValidDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"unsuccessfulLoginAttempts" : {
					get : function(){
						context["field"]  = "unsuccessfulLoginAttempts";
			        	context["metadata"] = (objectMetadata ? objectMetadata["unsuccessfulLoginAttempts"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.unsuccessfulLoginAttempts, context);},
					set : function(val){
						setterFunctions['unsuccessfulLoginAttempts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isUserAccountLocked" : {
					get : function(){
						context["field"]  = "isUserAccountLocked";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isUserAccountLocked"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isUserAccountLocked, context);},
					set : function(val){
						setterFunctions['isUserAccountLocked'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Phone" : {
					get : function(){
						context["field"]  = "Phone";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Phone"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Phone, context);},
					set : function(val){
						setterFunctions['Phone'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Email" : {
					get : function(){
						context["field"]  = "Email";
			        	context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Email, context);},
					set : function(val){
						setterFunctions['Email'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sacMaxResendRequestsAllowed" : {
					get : function(){
						context["field"]  = "sacMaxResendRequestsAllowed";
			        	context["metadata"] = (objectMetadata ? objectMetadata["sacMaxResendRequestsAllowed"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.sacMaxResendRequestsAllowed, context);},
					set : function(val){
						setterFunctions['sacMaxResendRequestsAllowed'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"remainingResendAttempts" : {
					get : function(){
						context["field"]  = "remainingResendAttempts";
			        	context["metadata"] = (objectMetadata ? objectMetadata["remainingResendAttempts"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.remainingResendAttempts, context);},
					set : function(val){
						setterFunctions['remainingResendAttempts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"remainingFailedAttempts" : {
					get : function(){
						context["field"]  = "remainingFailedAttempts";
			        	context["metadata"] = (objectMetadata ? objectMetadata["remainingFailedAttempts"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.remainingFailedAttempts, context);},
					set : function(val){
						setterFunctions['remainingFailedAttempts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isOtpVerified" : {
					get : function(){
						context["field"]  = "isOtpVerified";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isOtpVerified"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isOtpVerified, context);},
					set : function(val){
						setterFunctions['isOtpVerified'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lockUser" : {
					get : function(){
						context["field"]  = "lockUser";
			        	context["metadata"] = (objectMetadata ? objectMetadata["lockUser"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lockUser, context);},
					set : function(val){
						setterFunctions['lockUser'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"logoutUser" : {
					get : function(){
						context["field"]  = "logoutUser";
			        	context["metadata"] = (objectMetadata ? objectMetadata["logoutUser"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.logoutUser, context);},
					set : function(val){
						setterFunctions['logoutUser'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lockoutTime" : {
					get : function(){
						context["field"]  = "lockoutTime";
			        	context["metadata"] = (objectMetadata ? objectMetadata["lockoutTime"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lockoutTime, context);},
					set : function(val){
						setterFunctions['lockoutTime'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isOTPExpired" : {
					get : function(){
						context["field"]  = "isOTPExpired";
			        	context["metadata"] = (objectMetadata ? objectMetadata["isOTPExpired"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isOTPExpired, context);},
					set : function(val){
						setterFunctions['isOTPExpired'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});
			
			//converts model object to json object.
			this.toJsonInternal = function() {
				return Object.assign({}, privateState);
			};

			//overwrites object state with provided json value in argument.
			this.fromJsonInternal = function(value) {
									privateState.accountId = value?(value["accountId"]?value["accountId"]:null):null;
					privateState.Action = value?(value["Action"]?value["Action"]:null):null;
					privateState.cardHolderName = value?(value["cardHolderName"]?value["cardHolderName"]:null):null;
					privateState.cardId = value?(value["cardId"]?value["cardId"]:null):null;
					privateState.cardNumber = value?(value["cardNumber"]?value["cardNumber"]:null):null;
					privateState.cardStatus = value?(value["cardStatus"]?value["cardStatus"]:null):null;
					privateState.cardType = value?(value["cardType"]?value["cardType"]:null):null;
					privateState.errmsg = value?(value["errmsg"]?value["errmsg"]:null):null;
					privateState.expiryDate = value?(value["expiryDate"]?value["expiryDate"]:null):null;
					privateState.Reason = value?(value["Reason"]?value["Reason"]:null):null;
					privateState.success = value?(value["success"]?value["success"]:null):null;
					privateState.userId = value?(value["userId"]?value["userId"]:null):null;
					privateState.userName = value?(value["userName"]?value["userName"]:null):null;
					privateState.creditLimit = value?(value["creditLimit"]?value["creditLimit"]:null):null;
					privateState.availableCredit = value?(value["availableCredit"]?value["availableCredit"]:null):null;
					privateState.serviceProvider = value?(value["serviceProvider"]?value["serviceProvider"]:null):null;
					privateState.billingAddress = value?(value["billingAddress"]?value["billingAddress"]:null):null;
					privateState.cardProductName = value?(value["cardProductName"]?value["cardProductName"]:null):null;
					privateState.secondaryCardHolder = value?(value["secondaryCardHolder"]?value["secondaryCardHolder"]:null):null;
					privateState.withdrawlLimit = value?(value["withdrawlLimit"]?value["withdrawlLimit"]:null):null;
					privateState.accountNumber = value?(value["accountNumber"]?value["accountNumber"]:null):null;
					privateState.accountName = value?(value["accountName"]?value["accountName"]:null):null;
					privateState.maskedAccountNumber = value?(value["maskedAccountNumber"]?value["maskedAccountNumber"]:null):null;
					privateState.maskedCardNumber = value?(value["maskedCardNumber"]?value["maskedCardNumber"]:null):null;
					privateState.isInternational = value?(value["isInternational"]?value["isInternational"]:null):null;
					privateState.ids = value?(value["ids"]?value["ids"]:null):null;
					privateState.Destinations = value?(value["Destinations"]?value["Destinations"]:null):null;
					privateState.Cards = value?(value["Cards"]?value["Cards"]:null):null;
					privateState.Channel_id = value?(value["Channel_id"]?value["Channel_id"]:null):null;
					privateState.StartDate = value?(value["StartDate"]?value["StartDate"]:null):null;
					privateState.EndDate = value?(value["EndDate"]?value["EndDate"]:null):null;
					privateState.additionNotes = value?(value["additionNotes"]?value["additionNotes"]:null):null;
					privateState.phonenumber = value?(value["phonenumber"]?value["phonenumber"]:null):null;
					privateState.request_id = value?(value["request_id"]?value["request_id"]:null):null;
					privateState.bankName = value?(value["bankName"]?value["bankName"]:null):null;
					privateState.AccountType = value?(value["AccountType"]?value["AccountType"]:null):null;
					privateState.RequestCode = value?(value["RequestCode"]?value["RequestCode"]:null):null;
					privateState.RequestReason = value?(value["RequestReason"]?value["RequestReason"]:null):null;
					privateState.Channel = value?(value["Channel"]?value["Channel"]:null):null;
					privateState.Address_id = value?(value["Address_id"]?value["Address_id"]:null):null;
					privateState.communication_id = value?(value["communication_id"]?value["communication_id"]:null):null;
					privateState.CardNumbers = value?(value["CardNumbers"]?value["CardNumbers"]:null):null;
					privateState.lastNinetyDays = value?(value["lastNinetyDays"]?value["lastNinetyDays"]:null):null;
					privateState.errorMessage = value?(value["errorMessage"]?value["errorMessage"]:null):null;
					privateState.dbpErrCode = value?(value["dbpErrCode"]?value["dbpErrCode"]:null):null;
					privateState.dbpErrMsg = value?(value["dbpErrMsg"]?value["dbpErrMsg"]:null):null;
					privateState.newPin = value?(value["newPin"]?value["newPin"]:null):null;
					privateState.MFAAttributes = value?(value["MFAAttributes"]?value["MFAAttributes"]:null):null;
					privateState.serviceName = value?(value["serviceName"]?value["serviceName"]:null):null;
					privateState.serviceKey = value?(value["serviceKey"]?value["serviceKey"]:null):null;
					privateState.Otp = value?(value["Otp"]?value["Otp"]:null):null;
					privateState.OtpGenaratedts = value?(value["OtpGenaratedts"]?value["OtpGenaratedts"]:null):null;
					privateState.securityQuestions = value?(value["securityQuestions"]?value["securityQuestions"]:null):null;
					privateState.customerAnswer = value?(value["customerAnswer"]?value["customerAnswer"]:null):null;
					privateState.questionId = value?(value["questionId"]?value["questionId"]:null):null;
					privateState.maxFailedAttemptsAllowed = value?(value["maxFailedAttemptsAllowed"]?value["maxFailedAttemptsAllowed"]:null):null;
					privateState.ValidDate = value?(value["ValidDate"]?value["ValidDate"]:null):null;
					privateState.unsuccessfulLoginAttempts = value?(value["unsuccessfulLoginAttempts"]?value["unsuccessfulLoginAttempts"]:null):null;
					privateState.isUserAccountLocked = value?(value["isUserAccountLocked"]?value["isUserAccountLocked"]:null):null;
					privateState.Phone = value?(value["Phone"]?value["Phone"]:null):null;
					privateState.Email = value?(value["Email"]?value["Email"]:null):null;
					privateState.sacMaxResendRequestsAllowed = value?(value["sacMaxResendRequestsAllowed"]?value["sacMaxResendRequestsAllowed"]:null):null;
					privateState.remainingResendAttempts = value?(value["remainingResendAttempts"]?value["remainingResendAttempts"]:null):null;
					privateState.remainingFailedAttempts = value?(value["remainingFailedAttempts"]?value["remainingFailedAttempts"]:null):null;
					privateState.isOtpVerified = value?(value["isOtpVerified"]?value["isOtpVerified"]:null):null;
					privateState.lockUser = value?(value["lockUser"]?value["lockUser"]:null):null;
					privateState.logoutUser = value?(value["logoutUser"]?value["logoutUser"]:null):null;
					privateState.lockoutTime = value?(value["lockoutTime"]?value["lockoutTime"]:null):null;
					privateState.isOTPExpired = value?(value["isOTPExpired"]?value["isOTPExpired"]:null):null;
			};

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Cards);
	
	//Create new class level validator object
	BaseModel.Validator.call(Cards);
	
	var registerValidatorBackup = Cards.registerValidator;
	
	Cards.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Cards.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'cancelCard' with service id 'updateCard7127'
	Cards.cancelCard = function(params, onCompletion){
		return Cards.customVerb('cancelCard', params, onCompletion);
	};
	//For Operation 'getCardsForAdmin' with service id 'GetCardsForAdmin1780'
	Cards.getCardsForAdmin = function(params, onCompletion){
		return Cards.customVerb('getCardsForAdmin', params, onCompletion);
	};
	//For Operation 'reportLost' with service id 'updateCard5113'
	Cards.reportLost = function(params, onCompletion){
		return Cards.customVerb('reportLost', params, onCompletion);
	};
	//For Operation 'deleteTravelNotification' with service id 'deleteTravelNotification6950'
	Cards.deleteTravelNotification = function(params, onCompletion){
		return Cards.customVerb('deleteTravelNotification', params, onCompletion);
	};
	//For Operation 'unlockCard' with service id 'updateCard6945'
	Cards.unlockCard = function(params, onCompletion){
		return Cards.customVerb('unlockCard', params, onCompletion);
	};
	//For Operation 'getTravelNotification' with service id 'getTravelNotification2919'
	Cards.getTravelNotification = function(params, onCompletion){
		return Cards.customVerb('getTravelNotification', params, onCompletion);
	};
	//For Operation 'updateCardForAdmin' with service id 'UpdateCardForAdmin3853'
	Cards.updateCardForAdmin = function(params, onCompletion){
		return Cards.customVerb('updateCardForAdmin', params, onCompletion);
	};
	//For Operation 'getTravelNotificationStatus' with service id 'getTravelNotificationStatus8935'
	Cards.getTravelNotificationStatus = function(params, onCompletion){
		return Cards.customVerb('getTravelNotificationStatus', params, onCompletion);
	};
	//For Operation 'createTravelNotification' with service id 'createTravelNotification6410'
	Cards.createTravelNotification = function(params, onCompletion){
		return Cards.customVerb('createTravelNotification', params, onCompletion);
	};
	//For Operation 'updateTravelNotification' with service id 'updateTravelNotification4123'
	Cards.updateTravelNotification = function(params, onCompletion){
		return Cards.customVerb('updateTravelNotification', params, onCompletion);
	};
	//For Operation 'getCardsByUsername' with service id 'getCardsByUsername7853'
	Cards.getCardsByUsername = function(params, onCompletion){
		return Cards.customVerb('getCardsByUsername', params, onCompletion);
	};
	//For Operation 'getCardListForEnrolment' with service id 'getCardListForEnrolment1875'
	Cards.getCardListForEnrolment = function(params, onCompletion){
		return Cards.customVerb('getCardListForEnrolment', params, onCompletion);
	};
	//For Operation 'replaceCard' with service id 'updateCard3212'
	Cards.replaceCard = function(params, onCompletion){
		return Cards.customVerb('replaceCard', params, onCompletion);
	};
	//For Operation 'changePIN' with service id 'updateCard1565'
	Cards.changePIN = function(params, onCompletion){
		return Cards.customVerb('changePIN', params, onCompletion);
	};
	//For Operation 'createCardRequest' with service id 'createCardRequest1362'
	Cards.createCardRequest = function(params, onCompletion){
		return Cards.customVerb('createCardRequest', params, onCompletion);
	};
	//For Operation 'lockCard' with service id 'deleteTransactionsForLockedCard9350'
	Cards.lockCard = function(params, onCompletion){
		return Cards.customVerb('lockCard', params, onCompletion);
	};
	//For Operation 'getActiveCards' with service id 'getActiveCards8542'
	Cards.getActiveCards = function(params, onCompletion){
		return Cards.customVerb('getActiveCards', params, onCompletion);
	};
	
	var relations = [
	];
	
	Cards.relations = relations;
	
	Cards.prototype.isValid = function(){
		return Cards.isValid(this);
	};
	
	Cards.prototype.objModelName = "Cards";
	
	/*This API allows registration of preprocessors and postprocessors for model.
	 *It also fetches object metadata for object. 
	 *Options Supported
	 *preProcessor  - preprocessor function for use with setters.
	 *postProcessor - post processor callback for use with getters.
	 *getFromServer - value set to true will fetch metadata from network else from cache.
	 */
	Cards.registerProcessors = function(options, successCallback, failureCallback) {
	
		if(!options) {
			options = {};
		}
			
		if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
			preProcessorCallback = options["preProcessor"];
		}
		
		if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])){
			postProcessorCallback = options["postProcessor"];
		}
		
		function metaDataSuccess(res) {
			objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
			successCallback();
		}
		
		function metaDataFailure(err) {
			failureCallback(err);
		}
		
		kony.mvc.util.ProcessorUtils.getMetadataForObject("RBObjects", "Cards", options, metaDataSuccess, metaDataFailure);
	};
	
	//clone the object provided in argument.
	Cards.clone = function(objectToClone) {
		var clonedObj = new Cards();
		clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
		return clonedObj;
	};
	
	return Cards;
});