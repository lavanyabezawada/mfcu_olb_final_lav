define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function ACHTransactionsRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	ACHTransactionsRepository.prototype = Object.create(BaseRepository.prototype);
	ACHTransactionsRepository.prototype.constructor = ACHTransactionsRepository;

	//For Operation 'createACHCollectionWithVendor' with service id 'ACHVendorMockService7979'
	ACHTransactionsRepository.prototype.createACHCollectionWithVendor = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('createACHCollectionWithVendor',params, onCompletion);
	};
	//For Operation 'getACHTransactionDetailsById' with service id 'FetchACHTransactionDetails8543'
	ACHTransactionsRepository.prototype.getACHTransactionDetailsById = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('getACHTransactionDetailsById',params, onCompletion);
	};
	//For Operation 'createACHTransaction' with service id 'createTransaction2519'
	ACHTransactionsRepository.prototype.createACHTransaction = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('createACHTransaction',params, onCompletion);
	};
	//For Operation 'createACHPaymentWithVendor' with service id 'ACHVendorMockService4729'
	ACHTransactionsRepository.prototype.createACHPaymentWithVendor = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('createACHPaymentWithVendor',params, onCompletion);
	};
	//For Operation 'RejectedTransactions' with service id 'FetchAllRejectedACHTransactions9987'
	ACHTransactionsRepository.prototype.RejectedTransactions = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('RejectedTransactions',params, onCompletion);
	};
	//For Operation 'SaveAsTemplate' with service id 'SaveTransactionAsTemplate3794'
	ACHTransactionsRepository.prototype.SaveAsTemplate = function(params,onCompletion){
		return ACHTransactionsRepository.prototype.customVerb('SaveAsTemplate',params, onCompletion);
	};
	
	
	return ACHTransactionsRepository;
})