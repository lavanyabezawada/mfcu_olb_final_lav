#!/bin/bash
if [ "$1" != "" ]; then 
    echo $1
else
	error_exit $? "Kony Visualizer Web Archive File Path must be present"
	
fi


# $new={"$1"::-4}

#tempdir=$(mktemp -d)

#-d is for directory
unzip $1  -d tempdir
rm -rf $1
cd tempdir

#cache id
grep -Eo -m 1 '/[0-9]{13}' meta.json | grep -Eo '[0-9]{13}' > cacheIdFile.txt
value=$(<cacheIdFile.txt)

rm -rf cacheIdFile.txt

cd $value
cd desktopweb
mkdir obf

cd appjs
mv app.js ../obf/app.js
mv BusinessBanking/*Controller.js ../obf
mv *Module/*Controller.js ../obf


cd ../jslib
mv konyframework.js ../obf/konyframework.js



#Obfuscate file remove original and make obfuscated in common folder
cd ..

javascript-obfuscator obf --compact true --control-flow-flattening false --rename-globals false --unicode-escape-sequence false --self-defending true --log true

cd obf
rm -rf app.js konyframework.js kvmodules.js *Controller.js
mv konyframework-obfuscated.js ../jslib/konyframework.js
mv app-obfuscated.js ../appjs/app.js

#Accounts Module
mv frmAccountsDetailsController-obfuscated.js ../appjs/AccountsModule/frmAccountsDetailsController.js
mv frmAccountsLandingController-obfuscated.js ../appjs/AccountsModule/frmAccountsLandingController.js
mv frmPrintTransactionController-obfuscated.js ../appjs/AccountsModule/frmPrintTransactionController.js
mv frmPrintTransferController-obfuscated.js ../appjs/AccountsModule/frmPrintTransferController.js
mv frmScheduledTransactionsController-obfuscated.js ../appjs/AccountsModule/frmScheduledTransactionsController.js

#AlertsMsgsModule
mv frmNotificationsAndMessagesController-obfuscated.js ../appjs/AlertsMsgsModule/frmNotificationsAndMessagesController.js

#AuthModule
mv frmEnrollNowController-obfuscated.js ../appjs/AuthModule/frmEnrollNowController.js
mv frmLoginController-obfuscated.js ../appjs/AuthModule/frmLoginController.js
mv frmLoginLanguageController-obfuscated.js ../appjs/AuthModule/frmLoginLanguageController.js
mv frmMFAPreLoginController-obfuscated.js ../appjs/AuthModule/frmMFAPreLoginController.js

#BillPayModule
mv frmAcknowledgementController-obfuscated.js ../appjs/BillPayModule/frmAcknowledgementController.js
mv frmAddPayeeController-obfuscated.js ../appjs/BillPayModule/frmAddPayeeController.js
mv frmBillPayController-obfuscated.js ../appjs/BillPayModule/frmBillPayController.js
mv frmConfirmController-obfuscated.js ../appjs/BillPayModule/frmConfirmController.js

#BusinessBanking
mv frmBBUsersDashboardController-obfuscated.js ../appjs/BusinessBanking/frmBBUsersDashboardController.js
mv frmUserManagementController-obfuscated.js ../appjs/BusinessBanking/frmUserManagementController.js

#CardManagementModule
mv frmCardManagementController-obfuscated.js ../appjs/AlertsMsgsModule/frmCardManagementController.js

#FeedbackModule
mv frmCustomerFeedbackController-obfuscated.js ../appjs/FeedbackModule/frmCustomerFeedbackController.js

#InformationContentModule
mv frmContactUsPrivacyTandCController-obfuscated.js ../appjs/InformationContentModule/frmContactUsPrivacyTandCController.js
mv frmOnlineHelpController-obfuscated.js ../appjs/InformationContentModule/frmOnlineHelpController.js

#LoanPayModule
mv frmPayDueAmountController-obfuscated.js ../appjs/LoanPayModule/frmPayDueAmountController.js

#LocateUsModule
mv frmLocateUsController-obfuscated.js ../appjs/LocateUsModule/frmLocateUsController.js

#MultiFactorAuthenticationModule
mv frmMFATransactionsController-obfuscated.js ../appjs/MultiFactorAuthenticationModule/frmMFATransactionsController.js
mv frmMultiFactorAuthenticationController-obfuscated.js ../appjs/MultiFactorAuthenticationModule/frmMultiFactorAuthenticationController.js

#NAOModule
mv frmNAOController-obfuscated.js ../appjs/NAOModule/frmNAOController.js

#NUOModule
mv frmNewUserOnboardingController-obfuscated.js ../appjs/NUOModule/frmNewUserOnboardingController.js

#PayAPersonModule
mv frmPayAPersonController-obfuscated.js ../appjs/PayAPersonModule/frmPayAPersonController.js

#PersonalFinanceManagementModule
mv frmPersonalFinanceManagementController-obfuscated.js ../appjs/PersonalFinanceManagementModule/frmPersonalFinanceManagementController.js

#ProfileModule
mv frmProfileManagementController-obfuscated.js ../appjs/ProfileModule/frmProfileManagementController.js

#StopPaymentsModule
mv frmStopPaymentsController-obfuscated.js ../appjs/StopPaymentsModule/frmStopPaymentsController.js

#SurveyModule
mv frmCustomerFeedbackSurveyController-obfuscated.js ../appjs/SurveyModule/frmCustomerFeedbackSurveyController.js

#TransferEurModule
mv frmAcknowledgementEurController-obfuscated.js ../appjs/TransferEurModule/frmAcknowledgementEurController.js
mv frmAddExternalAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmAddExternalAccountEurController.js
mv frmAddInternalAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmAddInternalAccountEurController.js
mv frmConfirmAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmConfirmAccountEurController.js
mv frmConfirmEurController-obfuscated.js ../appjs/TransferEurModule/frmConfirmEurController.js
mv frmTransfersEurController-obfuscated.js ../appjs/TransferEurModule/frmTransfersEurController.js
mv frmVerifyAccountEurController-obfuscated.js ../appjs/TransferEurModule/frmVerifyAccountEurController.js

#TransferModule
mv frmAddExternalAccountController-obfuscated.js ../appjs/TransferModule/frmAddExternalAccountController.js
mv frmAddInternalAccountController-obfuscated.js ../appjs/TransferModule/frmAddInternalAccountController.js
mv frmConfirmAccountController-obfuscated.js ../appjs/TransferModule/frmConfirmAccountController.js
mv frmTransfersController-obfuscated.js ../appjs/TransferModule/frmTransfersController.js
mv frmVerifyAccountController-obfuscated.js ../appjs/TransferModule/frmVerifyAccountController.js

#WireTransferModule
mv frmWireTransferController-obfuscated.js ../appjs/TransferModule/frmWireTransferController.js


cd ..
rm -rf obf
cd ../..

newvalue=$(( $value + 1 ))
mv $value $newvalue

sed -i -e "s+$value+$newvalue+g" meta.json

cd nocache/desktopweb
sed -i -e "s+$value+$newvalue+g" kony.manifest

cd ../..
if [ "$(uname -s)" == "Darwin" ]; then
   echo "*************************this one $1***********"
   cd ../tempdir
   zip -r "$1" . 
else
   "C:\Program Files\7-Zip\7z.exe" a -tzip "../$1"
fi

rm -rf tempdir

