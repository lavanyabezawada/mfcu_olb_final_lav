define(['CommonUtilities'], function (CommonUtilities) {
    var cardsJSON = [];
    var userDetailsJSON = [];
    var userId = "";


    /**
     * Enroll Presenation to handle all enroll related functionalities. intialize members.
     * @class
     * @alias module:Enroll_PresentationController
     */

    function EnrollPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
        //scopeEnroll = this;
    }

    inheritsFrom(EnrollPresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Method to intialize Enroll presentation scope data.
     */
    EnrollPresentationController.prototype.initializePresentationController = function () {
        this.enrollFormName = "frmEnrollNow";

    };
    /**
     * Entry Method - to navigate to enroll now form and display UI on the basis of context sent
     * @param {Object} [context] - object key data map to update view 
     */
    EnrollPresentationController.prototype.showEnrollPage = function (context) {
        var navManager = applicationManager.getNavigationManager();
        if (kony.application.getCurrentForm().id !== "frmEnrollNow") {
            navManager.navigateTo("frmEnrollNow");
        }
        if (context) {
            navManager.updateForm(context);
        }
    };
    /**
     * Method to call verify user service in Auth Manager and call respective success or failure callback methods in Enroll Presenation controller.
     * Executed when the user clicks on Enroll button after entering the details. 
     * @param {Object}  detailsJSON  - Parameters required to check if user is already enrolled
     * @param {string} [detailsJSON.Ssn] ssn number
     * @param {string} [detailsJSON.LastName] last name of user
     * @param {boolean} [detailsJSON.DateOfBirth] Date of birth
     */
    EnrollPresentationController.prototype.verifyUser = function (detailsJSON) {
        applicationManager.getNewUserBusinessManager().setUserDetailsForEnroll(detailsJSON);
        applicationManager.getAuthManager().VerifyUserisalreadyEnrolled(detailsJSON, this.onSuccessVerifyUSer.bind(this, detailsJSON), this.onFailureVerifyUSer.bind(this));
    };

    /**
    * Method for success callback of verify user 
    * @param {string} [detailsJSON.Ssn] ssn number
    * @param {string} [detailsJSON.LastName] last name of user
    * @param {boolean} [detailsJSON.DateOfBirth] Date of birth
    * @param {Object} response response for whether user is already enrolled or not
    */

    EnrollPresentationController.prototype.onSuccessVerifyUSer = function (response, detailsJSON) {
        var authManager = applicationManager.getAuthManager();
        authManager.setServicekey(detailsJSON.serviceKey);
        response.userDetails = detailsJSON;
        var context = {
            "action": "VerifyUserSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    /**
    * Method for failure callback of verify user 
    * @param {Object} response response for whether user is already enrolled or not
    */
    EnrollPresentationController.prototype.onFailureVerifyUSer = function (response) {

        var context = {
            "action": "VerifyUserFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };

    /**
    * Method to call fetch cards service in cards Manager and call respective success or failure callback methods in Enroll Presenation controller.
    * Executed when the user clicks on Enroll button after entering the details. 
    * @param {Object}  detailsJSON  - Parameters required to fetch cards for enroll
    * @param {string} [detailsJSON.Ssn] ssn number
    * @param {string} [detailsJSON.LastName] last name of user
    * @param {boolean} [detailsJSON.DateOfBirth] Date of birth
    */
    EnrollPresentationController.prototype.goToPasswordResetOptionsPage = function (params) {
        applicationManager.getNavigationManager().updateForm({});
        applicationManager.getAuthManager().requestEnrollOTP(params, this.onrequestEnrollOTPSuccess.bind(this), this.onrequestEnrollOTPFailure.bind(this));
    };
    /**
    * Method for success callback of fetch cards for enroll 
    * @param {Object} response response for whether cards fetched successfully and user can navigate to passwords restting page.
    */
    EnrollPresentationController.prototype.onrequestEnrollOTPSuccess = function (response) {
        var authManager = applicationManager.getAuthManager();
        if (response && response.MFAAttributes) {
            authManager.setMFAResponse(response);
            authManager.setCommunicationType(response.MFAAttributes.communicationType);
            authManager.setServicekey(response.MFAAttributes.serviceKey);
        }
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "showScreenToEnterSecureCode": response
        });
    };
    /**
   * Method for failure callback of fetch cards for enroll 
   * @param {Object} response response for whether cards fetched successfully and user can navigate to passwords restting page.
   */
    EnrollPresentationController.prototype.onrequestEnrollOTPFailure = function (response) {

        var context = {
            "action": "PasswordResetFailure",
            "data": response
        };

        applicationManager.getNavigationManager().updateForm(context);

    };

    /**
    * Method to create cards JSON after successful fetching of cards
    * @param {Object} response  - response object for fetch cards which is required to create cards JSON
    */
    EnrollPresentationController.prototype.getCards = function (response) {
        var cardNumberJSON = response;
        cardsJSON = [];
        if (cardNumberJSON.length !== 0) {
            for (var index in cardNumberJSON) {
                var cardNumber = cardNumberJSON[index]["cardNumber"];
                var maskedcardNumber = this.maskCreditCardNumber(cardNumber);// TODO : Check for common method
                var tmpIndex = cardsJSON.length;
                cardsJSON[tmpIndex] = {};
                cardsJSON[tmpIndex][cardNumber] = maskedcardNumber;

            }
        }
    };


    /**
    * Method will mask credit card number as per requirements
    * @param {Number} cardNumber - card number required to get the masked card number
    */
    EnrollPresentationController.prototype.maskCreditCardNumber = function (cardNumber) {
        var maskedCreditNumber;
        var firstfour = cardNumber.substring(0, 4);
        var lastfour = cardNumber.substring(cardNumber.length - 4, cardNumber.length);
        maskedCreditNumber = firstfour + "XXXXXXXX" + lastfour;
        return maskedCreditNumber;
    };

    /**
    * Method will call service for validation of cvv and call success or failure callbacks
    * @param {Number} maskedCardNumber - masked card number which we received after masking 
    * @param {Number} cvv - svv which user entered 
    */

    EnrollPresentationController.prototype.cvvValidate = function (maskedCardNumber, cvv) {
        var cvvJSON = userDetailsJSON;
        cvvJSON.cvv = cvv;
        var unmaskedCardNumber = this.getUnMaskedCardNumber(maskedCardNumber);// TODO : check for common method
        cvvJSON.cardNumber = unmaskedCardNumber;
        applicationManager.getAuthManager().verifyCVV(cvvJSON, this.onSuccesscvvValidate.bind(this), this.onFailurecvvValidate.bind(this));
    };
    /**
    * Method called as success calback for cvvValidate
    * @param {Object} response - response from service for cvvValidate
    */
    EnrollPresentationController.prototype.onSuccesscvvValidate = function (response) {
        var context = {
            "action": "CVVValidateSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
   * Method called as failure calback for cvvValidate
   * @param {Object} response - response from service for cvvValidate
   */
    EnrollPresentationController.prototype.onFailurecvvValidate = function (response) {

        var context = {
            "action": "CVVValidateFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };

    /**
    * Method to get unmasked card number from masked card number
    * @param {Number} maskedCardNumber - masked card number required for getting unmasked card number
    */
    EnrollPresentationController.prototype.getUnMaskedCardNumber = function (maskedCardNumber) {
        for (var key in cardsJSON) {
            if (cardsJSON.hasOwnProperty(key)) {
                var val = cardsJSON[key];
                if (CommonUtilities.substituteforIncludeMethod(JSON.stringify(val), maskedCardNumber)) {
                    var pos = JSON.stringify(val).indexOf(':', 1);
                    return JSON.stringify(val).substring(2, pos - 1);
                }
            }
        }
        return null;

    };

    /**
    * Method will call otp validate service from auth manager and call success or failure callbacks
    * @param {String} otp to be validated
    */ 
    EnrollPresentationController.prototype.otpValidate = function (otp) {
        var param = { "Otp": otp };
        applicationManager.getAuthManager().verifyOTP(param, this.onSuccessotpValidate.bind(this), this.onFailureotpValidate.bind(this));
    };
    /**
    * Method will be called as success callback for otp validate method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessotpValidate = function (response) {
        var context = {
            "action": "OTPValidateSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
   * Method will be called as failure callback for otp validate method
   * @param {Object} response - response object which we receive from service
   */
    EnrollPresentationController.prototype.onFailureotpValidate = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            var context = {
                "action": "OTPValidateFailure",
                "data": response.errorMessage
            };
            applicationManager.getNavigationManager().updateForm(context);
        }
    };

    /**
    * Method will be called to fetch otp by calling fetchOTP service of auth manager
    */
    EnrollPresentationController.prototype.requestOTP = function () {
        applicationManager.getAuthManager().fetchOTP({}, this.onSuccessrequestOTP.bind(this), this.onFailurerequestOTP.bind(this));
    };
    /**
    * Method will be called as success callback for requestOTP method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessrequestOTP = function (response) {
        var context = {
            "action": "OTPResponseSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
   * Method will be called as failure callback for request otp method
   * @param {Object} response - response object which we receive from service
   */
    EnrollPresentationController.prototype.onFailurerequestOTP = function (response) {

        var context = {
            "action": "OTPResponseFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };
    /**
    * Method will be called to invoke fetch otp service of auth manager  
    */
    EnrollPresentationController.prototype.resendOTP = function () {
        applicationManager.getAuthManager().fetchOTP({}, this.onSuccessresendOTP.bind(this), this.onFailureresendOTP.bind(this));
    };
    /**
    * Method will be called as success callback for resendOTP method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessresendOTP = function (response) {
        var context = {
            "action": "OTPResendSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
   * Method will be called as failure callback for resendOTP method
   * @param {Object} response - response object which we receive from service
   */
    EnrollPresentationController.prototype.onFailureresendOTP = function (response) {

        var context = {
            "action": "OTPResendFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };

    /**
    * Method will be called to invoke create user service
    * @param {String} uName - username for creating user
    * @param {String} password - password for creating user
    */
    EnrollPresentationController.prototype.createUser = function (uName, password) {
        this.userName = uName;
        var userDetails = {
            "uName": uName,
            "password": password,
            "serviceKey": applicationManager.getAuthManager().getServicekey()
        }
        applicationManager.getNewUserBusinessManager().setUsernameAndPassword(userDetails);
        applicationManager.getNewUserBusinessManager().createUserForEnroll(this.onSuccessCreateUser.bind(this), this.onFailureCreateUser.bind(this));
    };
    /**
    * Method will be called as success callback for create user method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessCreateUser = function (response) {
        var context = {
            "action": "CreateUserSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
    * Method will be called as failure callback for create user method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onFailureCreateUser = function (response) {

        var context = {
            "action": "CreateUserFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };
    /**
    * Method will be called to get security questions
    * @param {Object} response - response object which we receive from service which will have security questions
    */
    EnrollPresentationController.prototype.fetchSecurityQuestions = function () {

        applicationManager.getAuthManager().fetchSecurityQuestionsForEnroll(this.onSuccessfetchSecurityQuestions.bind(this), this.onFailurefetchSecurityQuestions.bind(this));
    };
    /**
    * Method will be called as success callback for get security questions method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessfetchSecurityQuestions = function (response) {
        var context = {
            "securityQuestions": [],
            "flagToManipulate": []
        };
        var i = 0;

        while (i < response.records.length) {
            context.securityQuestions[i] = response.records[i].SecurityQuestion;
            context.flagToManipulate[i] = "false";
            i++;

        }
        context = {
            "action": "FetchQuestionsSuccess",
            "data": { "context": context, "response": response.records }
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
    * Method will be called as failure callback for get security questions method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onFailurefetchSecurityQuestions = function (response) {

        var context = {
            "action": "FetchQuestionsFailure",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);

    };
    /**
      * saveSecurityQuestions : function to save security questions and given answers
      * @param {Object} data questions and answers JSON
      */
    EnrollPresentationController.prototype.saveSecurityQuestions = function (data) {
        data = JSON.stringify(data);
        data = data.replace(/"/g , "'");
        var securityQuestionsData = {
            userName: this.userName,
            securityQuestions: data
        };
        applicationManager.getAuthManager().saveSecurityQuestionsForEnroll(securityQuestionsData, this.onSuccesSsaveSecurityQuestions.bind(this), this.onFailureSaveSecurityQuestions.bind(this));
    };
    /**
    * Method will be called as success callback for save security questions method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccesSsaveSecurityQuestions = function (response) {
        var context = {
            "action": "SaveQuestionsSuccess",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
    * Method will be called as failure callback for save security questions method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onFailureSaveSecurityQuestions = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            var context = {
                "action": "SaveQuestionsFailure",
                "data": response.errorMessage
            };
            applicationManager.getNavigationManager().updateForm(context);
        }
    };
    

    /**
    * Method will be called for fetching username and password policies
    */
    EnrollPresentationController.prototype.getUserNameAndPasswordPolicies = function () {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        var params = {
            "ruleForCustomer": true,
            "policyForCustomer": true
            };
        applicationManager.getAuthManager().getUsernameAndPasswordRulesAndPolicies(params,this.onSuccesUserNameAndPasswordPolicies.bind(this), this.onFailureUserNameAndPasswordPolicies.bind(this));
    };

    /**
    * Method will be called as success callback for get user name policies method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccesUserNameAndPasswordPolicies = function (response) {
        var context = {
            "action": "usernamePoliciesRules",
            "data": response
        };
        var validationUtility = applicationManager.getValidationUtilManager();
        validationUtility.createRegexForUsernameValidation(response.usernamerules);
        validationUtility.createRegexForPasswordValidation(response.passwordrules);
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
    * Method will be called as failure callback for get user name policies method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onFailureUserNameAndPasswordPolicies = function (response) {
        var context = {
            "action": "usernamePoliciesRules",
            "data": response
        };
        applicationManager.getNavigationManager().updateForm(context);
    };



    /**
   * navigateToServerDownScreen :Function to navigate to server down screen
   */
    EnrollPresentationController.prototype.navigateToServerDownScreen = function () {
        // var context = {
        //     "action": "ServerDown"
        // };
        // this.showEnrollPage(context); //issue with enroll ui for server down.
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.showLoginScreen({
            "hideProgressBar": true,
            "action": "ServerDown"
        });


    };

    /**
     * Entry Method - to navigate to enroll now form and display UI on the basis of context sent
     */
    EnrollPresentationController.prototype.showEnrollPage = function (context) {
        var navManager = applicationManager.getNavigationManager();
        if (kony.application.getCurrentForm().id !== "frmEnrollNow") {
            navManager.navigateTo("frmEnrollNow");
        }
        var configManager = applicationManager.getConfigurationManager();
        if(context && context.identifier) {
            this.showCreatePasswordUsingDeepLinkingPage(context.identifier);
        } else if (configManager.isBusinessBankingEnabled()) {
            navManager.updateForm({ "enrollTypeSelectionRequired": true });
        } else {
            navManager.updateForm({ "enrollTypeSelectionNotRequired": true });
        }
    };

    EnrollPresentationController.prototype.checkIfBusinessBankingMemberExists = function (detailsJSON) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        applicationManager.getNewUserBusinessManager().setUserDetailsForEnroll(detailsJSON);
        applicationManager.getAuthManager().vefifyUserOrMemberIsAlreadyEnrolled(detailsJSON, this.onSuccesscheckIfBusinessBankingMemberExists.bind(this), this.onFailurecheckIfBusinessBankingMemberExists.bind(this));

    };
    EnrollPresentationController.prototype.onSuccesscheckIfBusinessBankingMemberExists = function (response) {
        var authManager = applicationManager.getAuthManager();
        authManager.setServicekey(response.serviceKey);
        var params = {
            "MFAAttributes": {
                "serviceKey": authManager.getServicekey()
            }
        }
        applicationManager.getNewUserBusinessManager().requestOTPPreLoginMB(params, this.onSuccessrequestOTPPreLoginMB.bind(this), this.onFailurerequestOTPPreLoginMB.bind(this));
    };
    EnrollPresentationController.prototype.onSuccessrequestOTPPreLoginMB = function (response) {
        var authManager = applicationManager.getAuthManager();
        if (response && response.MFAAttributes) {
            authManager.setMFAResponse(response);
            authManager.setCommunicationType(response.MFAAttributes.communicationType);
            authManager.setServicekey(response.MFAAttributes.serviceKey);
        }
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "showScreenToEnterSecureCodeMB": response
        });
    };
    EnrollPresentationController.prototype.onFailurerequestOTPPreLoginMB = function (response) {
        if (response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "showErrorInUserDetailsPageBB": response.errorMessage
            });
        }
    };
    EnrollPresentationController.prototype.onFailurecheckIfBusinessBankingMemberExists = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "showErrorInUserDetailsPageBB": response.errorMessage
            });
        }
    };

    /**
    * Method will be called to fetch otp by calling fetchOTP service of auth manager
    */
    EnrollPresentationController.prototype.requestOTPBB = function () {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        applicationManager.getAuthManager().fetchOTP({}, this.onSuccessrequestOTPBB.bind(this), this.onFailurerequestOTPBB.bind(this));
    };
    /**
    * Method will be called as success callback for requestOTP method
    * @param {Object} response - response object which we receive from service
    */
    EnrollPresentationController.prototype.onSuccessrequestOTPBB = function () {
        var context = {
            "hideProgressBar": true,
            "showSendOTPScreen2MBB": {}
        };
        applicationManager.getNavigationManager().updateForm(context);
    };
    /**
   * Method will be called as failure callback for request otp method
   * @param {Object} response - response object which we receive from service
   */
    EnrollPresentationController.prototype.onFailurerequestOTPBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
        }
        //this.onFailurerequestOTP(response);
    };

    EnrollPresentationController.prototype.validateOTPBB = function (otp) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        var param = { "Otp": otp }
        applicationManager.getAuthManager().verifyOTP(param, this.onSuccessValidateOTPBB.bind(this), this.onFailureValidateOTPBB.bind(this));
    };

    EnrollPresentationController.prototype.onSuccessValidateOTPBB = function () {
        var params = {
            "ruleForCustomer": true,
            "policyForCustomer": true
            };
        applicationManager.getAuthManager().getUsernameAndPasswordRulesAndPolicies(params,this.onSuccessGetUsernamePoliciesMBB.bind(this), this.onFailureGetUsernamePoliciesMBB.bind(this));
    };
    EnrollPresentationController.prototype.onSuccessGetUsernamePoliciesMBB = function (response) {
        var context = {
            "hideProgressBar": true,
            "showCreateUserAndPasswordScreenMBB": response
        };
        var validationUtility = applicationManager.getValidationUtilManager();
        validationUtility.createRegexForUsernameValidation(response.usernamerules);
        validationUtility.createRegexForPasswordValidation(response.passwordrules);
        applicationManager.getNavigationManager().updateForm(context);
    };
    EnrollPresentationController.prototype.onFailureGetUsernamePoliciesMBB = function (response) {
        this.onFailureUserNamePolicies(response);
    };
    EnrollPresentationController.prototype.onFailureValidateOTPBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "otpValidationFailureBB": response.errorMessage
            });
        }
    };

    EnrollPresentationController.prototype.resendOTPBB = function () {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        applicationManager.getAuthManager().fetchOTP({}, this.onSuccessResendOTPBB.bind(this), this.onFailureResendOTPBB.bind(this));
    };

    EnrollPresentationController.prototype.onSuccessResendOTPBB = function () {
        var context = {
            "hideProgressBar": true,
            "resendOtpBB": {}
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    EnrollPresentationController.prototype.onFailureResendOTPBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
        }
    };

    EnrollPresentationController.prototype.createUserBB = function (uName, password) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        var userDetails = {
            "uName": uName,
            "password": password,
            "serviceKey": applicationManager.getAuthManager().getServicekey()
        }
        applicationManager.getNewUserBusinessManager().setUsernameAndPassword(userDetails);
        applicationManager.getNewUserBusinessManager().createBBUserForEnroll(this.onSuccessCreateUserBB.bind(this), this.onFailureCreateUserBB.bind(this));
    };

    EnrollPresentationController.prototype.onSuccessCreateUserBB = function (response) {
        var isEsignAgreementRequired = false;
        if(response.isEAgreementRequired === "true" && response.isEagreementSigned === "false") {
            isEsignAgreementRequired = true;
        }
        var context = {
            "hideProgressBar": true,
            "showChooseSecuritySettingsScreenMBB": {
                "isEsignAgreementRequired": isEsignAgreementRequired
            }
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    EnrollPresentationController.prototype.onFailureCreateUserBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            var context = {
                "hideProgressBar": true,
                "createUserBBFailure": response.errorMessage
            };
            applicationManager.getNavigationManager().updateForm(context);
        }
    };

    EnrollPresentationController.prototype.fetchSecurityQuestionsBB = function () {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        applicationManager.getAuthManager().fetchSecurityQuestionsForEnroll(this.onSuccessfetchSecurityQuestionsBB.bind(this), this.onFailurefetchSecurityQuestions.bind(this));

    };

    EnrollPresentationController.prototype.onSuccessfetchSecurityQuestionsBB = function (response) {
        var i = 0;
        var securityQuestions = [];
        var flagToManipulate = [];
        while (i < response.records.length) {
            securityQuestions[i] = response.records[i].SecurityQuestion;
            flagToManipulate[i] = "false";
            i++;
        }
        var context = {
            "hideProgressBar": true,
            "showSecurityQuestionsDetailsScreenMBB": {
                "data": {
                    securityQuestions: securityQuestions,
                    flagToManipulate: flagToManipulate
                },
                "backendResponse": response.records
            }
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    EnrollPresentationController.prototype.onFailurefetchSecurityQuestionsBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
        }
    };

    EnrollPresentationController.prototype.saveSecurityQuestionsBB = function (data) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        data = JSON.stringify(data);
        data = data.replace(/"/g , "'");
        var securityQuestionsData = {
            userName: this.userName,
            securityQuestions: data
        };
        applicationManager.getAuthManager().saveSecurityQuestionsForEnroll(securityQuestionsData, this.onSuccesSsaveSecurityQuestionsBB.bind(this), this.onFailureSaveSecurityQuestionsBB.bind(this));
    };

    EnrollPresentationController.prototype.onSuccesSsaveSecurityQuestionsBB = function (response) {
        var context = {
            "hideProgressBar": true,
            "showEsignAgreementScreenMBB": {}
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    EnrollPresentationController.prototype.onFailureSaveSecurityQuestionsBB = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "saveSecurityQuestionsFailureBB" : response.errorMessage
            });
        }
        //this.onFailureSaveSecurityQuestions(response);
    };

    EnrollPresentationController.prototype.downloadESignAgreement = function () {
        var mfURL = KNYMobileFabric.mainRef.config.services_meta.RBObjects.url;
        var url = mfURL + "/operations/DbxUser/downloadEAgreementPdf";
        CommonUtilities.downloadFile(
            {
                "url": url,
                "filename": "e-sign Agreement.pdf"
            }
        );
    };

    EnrollPresentationController.prototype.showCreatePasswordUsingDeepLinkingPage = function (identifier) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        var params = { "identifier": identifier };
        applicationManager.getAuthManager().isResetPasswordLinkActive(params, this.onSuccessIsResetPasswordLinkActive.bind(this, identifier), this.onFailureIsResetPasswordLinkActive.bind(this));
    };

    EnrollPresentationController.prototype.onSuccessIsResetPasswordLinkActive = function (identifier, response) {
      var params = {
            "ruleForCustomer": true,
            "policyForCustomer": true
            };
        applicationManager.getAuthManager().getUsernameAndPasswordRulesAndPolicies(params,this.onSuccesUserNamePoliciesUsingResetLink.bind(this, identifier), this.onFailureUserNamePoliciesUsingResetLink.bind(this));
    };

    EnrollPresentationController.prototype.onFailureIsResetPasswordLinkActive = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "invalidPasswordLink": {
                    "errorMessage": response.errorMessage
                }
            });
        }
    };

    EnrollPresentationController.prototype.onSuccesUserNamePoliciesUsingResetLink = function (identifier, response) {
        var validationUtility = applicationManager.getValidationUtilManager();
        validationUtility.createRegexForUsernameValidation(response.usernamerules);
        validationUtility.createRegexForPasswordValidation(response.passwordrules);
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "showCreateUserAndPasswordScreenUsingResetLinkMBB": {
                "rules": response,
                "identifier": identifier
            }
        });
    };

    EnrollPresentationController.prototype.onFailureUserNamePoliciesUsingResetLink = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
        }
    };

    EnrollPresentationController.prototype.createPasswordForOrganizationEmployee = function (data) {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
        this.userName = data.UserName;
        var params = {
            "identifier": data.identifier,
            "UserName": data.UserName,
            "Password": data.Password
        };
        applicationManager.getAuthManager().setOrgPasswordForEmployee(params, this.onSuccesCreatePasswordForOrganizationEmployee.bind(this), this.onFailureCreatePasswordForOrganizationEmployee.bind(this));
    };

    EnrollPresentationController.prototype.onSuccesCreatePasswordForOrganizationEmployee = function (response) {
        var isEsignAgreementRequired = false;
        if (response.isEAgreementRequired === "true" && response.isEagreementSigned === "false") {
            isEsignAgreementRequired = true;
        }
        var context = {
            "hideProgressBar": true,
            "showChooseSecuritySettingsScreenMBB": {
                isEsignAgreementRequired: isEsignAgreementRequired
            }
        };
        applicationManager.getNavigationManager().updateForm(context);
    };

    EnrollPresentationController.prototype.onFailureCreatePasswordForOrganizationEmployee = function (response) {
        if(response.isServerUnreachable) {
            this.navigateToServerDownScreen();
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "createPasswordForOrganizationEmployeeFailure" : response.errorMessage
            });
        }
    };

    EnrollPresentationController.prototype.verifyOTPPreLogin = function (params) {
        applicationManager.getAuthManager().verifyOTPPreLogin(params, this.verifyOTPPreLoginSuccess.bind(this), this.verifyOTPPreLoginFailure.bind(this));
    };

    EnrollPresentationController.prototype.verifyOTPPreLoginSuccess = function (response) {
        var authManager = applicationManager.getAuthManager();
        var MFAResponse = authManager.getMFAResponse();
        if (response.MFAAttributes) {
            if (response.MFAAttributes.securityKey) {
                MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
                MFAResponse.MFAAttributes.communicationType = authManager.getCommunicationType();
                MFAResponse.MFAAttributes.isOTPExpired = false;
            } else if (response.MFAAttributes.isOTPExpired) {
                MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                MFAResponse.MFAAttributes.isOTPExpired = response.MFAAttributes.isOTPExpired;
                MFAResponse.MFAAttributes.communicationType = authManager.getCommunicationType();
            }
            authManager.setMFAResponse(MFAResponse);
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "showSecureAccessCodeScreenAfterResend": authManager.getMFAResponse()
            });
        } else {
            this.onSuccessotpValidate();
        }
    };
    EnrollPresentationController.prototype.verifyOTPPreLoginFailure = function (response) {
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isEnteredOTPIncorrect": response.serverErrorRes
        });
    };

    EnrollPresentationController.prototype.verifyOTPPreLoginMB = function (params) {
        applicationManager.getNewUserBusinessManager().verifyOTPPreLoginMB(params, this.verifyOTPPreLoginMBSuccess.bind(this), this.verifyOTPPreLoginMBFailure.bind(this));
    };

    EnrollPresentationController.prototype.verifyOTPPreLoginMBSuccess = function (response) {
        var authManager = applicationManager.getAuthManager();
        var MFAResponse = authManager.getMFAResponse();
        if (response.MFAAttributes) {
            if (response.MFAAttributes.securityKey) {
                MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
                MFAResponse.MFAAttributes.communicationType = authManager.getCommunicationType();
                MFAResponse.MFAAttributes.isOTPExpired = false;
            } else if (response.MFAAttributes.isOTPExpired) {
                MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                MFAResponse.MFAAttributes.isOTPExpired = response.MFAAttributes.isOTPExpired;
                MFAResponse.MFAAttributes.communicationType = authManager.getCommunicationType();
            }
            authManager.setMFAResponse(MFAResponse);
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true,
                "showSecureAccessCodeScreenAfterResendMB": authManager.getMFAResponse()
            });
        } else {
            this.onSuccessValidateOTPBB();
        }
    };
    EnrollPresentationController.prototype.verifyOTPPreLoginMBFailure = function (response) {
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isEnteredOTPIncorrectMB": response.serverErrorRes
        });
    };

    EnrollPresentationController.prototype.resendOTPMB = function (params) {
        applicationManager.getNewUserBusinessManager().requestOTPPreLoginMB(params, this.verifyOTPPreLoginMBSuccess.bind(this), this.verifyOTPPreLoginMBFailure.bind(this));
    };

    EnrollPresentationController.prototype.resendOTPForResetPassword = function (params) {
        applicationManager.getAuthManager().requestEnrollOTP(params, this.verifyOTPPreLoginSuccess.bind(this), this.verifyOTPPreLoginFailure.bind(this));
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneEmail = function (params) {
        applicationManager.getAuthManager().requestEnrollOTP(params, this.requestOTPUsingPhoneEmailSuccess.bind(this), this.requestOTPUsingPhoneEmailFailure.bind(this));
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneEmailSuccess = function (response) {
        var authManager = applicationManager.getAuthManager();
        authManager.setMFAResponse(response);
        var MFAResponse = authManager.getMFAResponse();
        MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
        MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isOTPReceived": authManager.getMFAResponse()
        });
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneEmailFailure = function (response) {
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isOTPRequestFailed": response.serverErrorRes
        });
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneAndEmailMB = function (params) {
        applicationManager.getNewUserBusinessManager().requestOTPPreLoginMB(params, this.requestOTPUsingPhoneEmailMBSuccess.bind(this), this.requestOTPUsingPhoneEmailMBFailure.bind(this));
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneEmailMBSuccess = function (response) {
        var authManager = applicationManager.getAuthManager();
        authManager.setMFAResponse(response);
        var MFAResponse = authManager.getMFAResponse();
        MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
        MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isOTPReceivedMB": authManager.getMFAResponse()
        });
    };

    EnrollPresentationController.prototype.requestOTPUsingPhoneEmailMBFailure = function (response) {
        applicationManager.getNavigationManager().updateForm({
            "hideProgressBar": true,
            "isOTPRequestFailed": response.serverErrorRes
        });
    };


    return EnrollPresentationController;
});
