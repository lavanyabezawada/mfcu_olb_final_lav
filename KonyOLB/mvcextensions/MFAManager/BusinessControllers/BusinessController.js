define([], function () {
  function MFAManager() {
    kony.mvc.Business.Controller.call(this);
    scope_MFAManager = this;
    this.MFAResponse = "";
    this.flowType = "";
    this.servicekey = "";
    this.MFAType = "";
    this.communicationType = "";
    this.serviceId = "";
    this.operationType = "";
    this.userName = "";
  }
   inheritsFrom(MFAManager,  kony.mvc.Business.Delegator);
MFAManager.prototype.initializeBusinessController = function(){};

  MFAManager.prototype.initMFAFlow = function (mfaJSON) {
    scope_MFAManager.setMFAResponse(mfaJSON.response);
    scope_MFAManager.setMFAFlowType(mfaJSON.flowType);
    scope_MFAManager.setServicekey(mfaJSON.response.MFAAttributes.serviceKey);
    scope_MFAManager.setMFAType(mfaJSON.response.MFAAttributes.MFAType);
    scope_MFAManager.setUserName(mfaJSON.userName);
    scope_MFAManager.setCommunicationType(mfaJSON.response.MFAAttributes.communicationType);
    applicationManager.getPresentationUtility().MFA.navigateBasedOnMFAType();
  };

  MFAManager.prototype.setMFAResponse = function (MFAResponse) {
    this.MFAResponse = MFAResponse;
  };
  
  MFAManager.prototype.getMFAResponse = function () {
    return this.MFAResponse;
  };
  
  MFAManager.prototype.setUserName = function (userName) {
   this.userName = userName;
  };
  
  MFAManager.prototype.getUserName = function () {
    return this.userName;
  };

  MFAManager.prototype.setMFAFlowType = function (flowType) {
    this.flowType = flowType;
  };
  MFAManager.prototype.getMFAFlowType = function () {
    return this.flowType;
  };

  MFAManager.prototype.setMFAOperationType = function (operationType) {
    this.operationType = operationType;
  };
  MFAManager.prototype.getMFAOperationType = function () {
    return this.flowType;
  };
  MFAManager.prototype.setServicekey = function (servicekey) {
    this.servicekey = servicekey;
  };
  MFAManager.prototype.getServicekey = function () {
    return this.servicekey;
  };

  MFAManager.prototype.setMFAType = function (MFAType) {
    this.MFAType = MFAType;
  };
  MFAManager.prototype.getMFAType = function () {
    return this.MFAType;
  };

  MFAManager.prototype.setCommunicationType = function (communicationType) {
    this.communicationType = communicationType;
  };
  MFAManager.prototype.getCommunicationType = function () {
    return this.communicationType;
  };
  MFAManager.prototype.setServiceId = function (serviceId) {
    this.serviceId = serviceId;
  };
  MFAManager.prototype.getServiceId = function (serviceId) {
    return this.serviceId;
  };

  MFAManager.prototype.requestOTP = function (params) {
    var transactionManager = applicationManager.getTransactionManager();
    if (this.flowType == "LoginMFA") {
      this.requestLoginMFAOtp(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_USERNAME") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserName(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_PASSWORD") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserPassword(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    }
    else if (this.flowType === "BULK_BILL_PAY") {
      applicationManager.getTransactionManager().createBulkBillPayPayement(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));

    } else if (this.operationType === "CREATE") {
      transactionManager.createTransaction(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.operationType === "UPDATE") {
      transactionManager.updateTransaction(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "SECURITYQUESTION_RESET") {
      this.requestUpdateSecurityQuestionsOTP(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "LOCK_CARD") {
      applicationManager.getCardsManager().lockCard(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "UNLOCK_CARD") {
      applicationManager.getCardsManager().unLockCard(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_DEBIT") {
      applicationManager.getCardsManager().changePin(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "REPORT_LOST") {
       applicationManager.getCardsManager().reportLost(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "CANCEL_CARD") {
        applicationManager.getCardsManager().updateCardStatus(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "REPLACE_CARD") {
        applicationManager.getCardsManager().replaceCard(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_CREDIT") {
      applicationManager.getCardsManager().createCardRequest(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
    }
  };

  MFAManager.prototype.requestLoginMFAOtp = function (params, presentationSuccessCallback, presentationErrorCallback) {
    var userobj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
    userobj.customVerb('requestLoginMFAOTP', params, getUserCompletionCallback);
    function getUserCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  MFAManager.prototype.requestUpdateSecurityQuestionsOTP = function (params, presentationSuccessCallback, presentationErrorCallback) {
    var userobj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
    userobj.customVerb('requestUpdateSecurityQuestionsOTP', params, getUserCompletionCallback);
    function getUserCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };


  MFAManager.prototype.requestOTPSuccess = function (response) {
    this.setMFAResponse(response);
    this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
    this.MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
    applicationManager.getPresentationUtility().MFA.showMFAOTPScreen();
  };
  MFAManager.prototype.requestOTPFailure = function (error) {
    applicationManager.getPresentationUtility().MFA.mfaOTPError(error);
  };

  MFAManager.prototype.verifyOTP = function (params) {
    var transactionManager = applicationManager.getTransactionManager();
    if (this.flowType == "LoginMFA") {
      this.verifyLoginMFAOtp(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_USERNAME") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserName(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_PASSWORD") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserPassword(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType === "BULK_BILL_PAY") {
      transactionManager.createBulkBillPayPayement(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.operationType === "CREATE") {
      transactionManager.createTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.operationType === "UPDATE") {
      transactionManager.updateTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType === "SECURITYQUESTION_RESET") {
      this.verifyUpdateSecurityQuestionsOTP(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "LOCK_CARD") {
      applicationManager.getCardsManager().lockCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "UNLOCK_CARD") {
      applicationManager.getCardsManager().unLockCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }  else if (this.flowType === "CHANGE_PIN_DEBIT") {
      applicationManager.getCardsManager().changePin(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "REPORT_LOST") {
       applicationManager.getCardsManager().reportLost(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "CANCEL_CARD") {
        applicationManager.getCardsManager().updateCardStatus(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "REPLACE_CARD") {
        applicationManager.getCardsManager().replaceCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_CREDIT") {
      applicationManager.getCardsManager().createCardRequest(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
  };
  MFAManager.prototype.resendOTP = function (params) {
    var transactionManager = applicationManager.getTransactionManager();
    if (this.flowType == "LoginMFA") {
      this.requestLoginMFAOtp(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_USERNAME") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserName(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_PASSWORD") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserPassword(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType === "BULK_BILL_PAY") {
      transactionManager.createBulkBillPayPayement(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.operationType === "CREATE") {
      transactionManager.createTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.operationType === "UPDATE") {
      transactionManager.updateTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
    else if (this.flowType === "SECURITYQUESTION_RESET") {
      this.requestUpdateSecurityQuestionsOTP(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }else if (this.flowType === "LOCK_CARD") {
      applicationManager.getCardsManager().lockCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "UNLOCK_CARD") {
      applicationManager.getCardsManager().unLockCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }  else if (this.flowType === "CHANGE_PIN_DEBIT") {
      applicationManager.getCardsManager().changePin(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "REPORT_LOST") {
       applicationManager.getCardsManager().reportLost(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "CANCEL_CARD") {
        applicationManager.getCardsManager().updateCardStatus(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "REPLACE_CARD") {
        applicationManager.getCardsManager().replaceCard(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_CREDIT") {
      applicationManager.getCardsManager().createCardRequest(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
    }
  };


  MFAManager.prototype.verifyUpdateSecurityQuestionsOTP = function (params, presentationSuccessCallback, presentationErrorCallback) {
    var userobj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
    userobj.customVerb('verifyUpdateSecurityQuestionsOTP', params, getUserCompletionCallback);
    function getUserCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  MFAManager.prototype.verifyLoginMFAOtp = function (params, presentationSuccessCallback, presentationErrorCallback) {
    var userobj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
    userobj.customVerb('verifyLoginMFAOTP', params, getUserCompletionCallback);
    function getUserCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  MFAManager.prototype.verifyOTPSuccess = function (response) {
    if (response.MFAAttributes) {
      if (response.MFAAttributes.securityKey) {
        this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
        this.MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
        this.MFAResponse.MFAAttributes.communicationType = this.getCommunicationType();
        this.MFAResponse.MFAAttributes.isOTPExpired = false;
      } else if (response.MFAAttributes.isOTPExpired) {
        this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
        this.MFAResponse.MFAAttributes.isOTPExpired = response.MFAAttributes.isOTPExpired;
        this.MFAResponse.MFAAttributes.communicationType = this.getCommunicationType();
      }

      applicationManager.getPresentationUtility().MFA.setSecureCodeScreen(response);
    } else {
      applicationManager.getPresentationUtility().MFA.navigateToAckScreen(response);
    }
  };

  MFAManager.prototype.isTransactionalError = function (error) {
    if (error.serverErrorRes.dbpErrCode) {
            if(error.serverErrorRes.dbpErrCode>=10500 && error.serverErrorRes.dbpErrCode<=10700){
                return false;
            }else{
                return true;
            }
        } else {
            return true;
        }
  };

  MFAManager.prototype.verifyOTPFailure = function (error) {
    if (this.isTransactionalError(error)) {
      applicationManager.getPresentationUtility().MFA.navigateToTransactionScreen(error);
    } else {
      applicationManager.getPresentationUtility().MFA.enteredIncorrectOTP(error.serverErrorRes);
    }
  };

  MFAManager.prototype.verifySecurityQuestions = function (params) {
    var transactionManager = applicationManager.getTransactionManager();
    if (this.flowType == "LoginMFA") {
      this.verifyLoginMFASecurityQuestions(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_USERNAME") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserName(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }
    else if (this.flowType == "UPDATE_PASSWORD") {
      var userObj = applicationManager.getUserPreferencesManager();
      userObj.updateUserPassword(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }
    else if (this.flowType === "BULK_BILL_PAY") {
      applicationManager.getTransactionManager().createBulkBillPayPayement(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.operationType === "CREATE") {
      transactionManager.createTransaction(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.operationType === "UPDATE") {
      transactionManager.updateTransaction(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.flowType === "LOCK_CARD") {
      applicationManager.getCardsManager().lockCard(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }  else if (this.flowType === "UNLOCK_CARD") {
      applicationManager.getCardsManager().unLockCard(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_DEBIT") {
      applicationManager.getCardsManager().changePin(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.flowType === "REPORT_LOST") {
       applicationManager.getCardsManager().reportLost(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.flowType === "CANCEL_CARD") {
        applicationManager.getCardsManager().updateCardStatus(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }  else if (this.flowType === "REPLACE_CARD") {
        applicationManager.getCardsManager().replaceCard(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    } else if (this.flowType === "CHANGE_PIN_CREDIT") {
      applicationManager.getCardsManager().createCardRequest(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
    }
  };

  MFAManager.prototype.verifyLoginMFASecurityQuestions = function (params, presentationSuccessCallback, presentationErrorCallback) {
    var userobj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
    userobj.customVerb('verifyLoginMFASecurityQuestions', params, getUserCompletionCallback);
    function getUserCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  MFAManager.prototype.verifyAnswersSuccess = function (response) {
    if (response.MFAAttributes) {
      this.setMFAResponse(response);
      applicationManager.getPresentationUtility().MFA.navigateToSecurityQuestion();

    }
    else {
      applicationManager.getPresentationUtility().MFA.navigateToAckScreen(response);
    }
  };

  MFAManager.prototype.verifyAnswersFailure = function (error) {

    if (this.isTransactionalError(error)) {
      applicationManager.getPresentationUtility().MFA.navigateToTransactionScreen(error);
    } else {
      applicationManager.getPresentationUtility().MFA.enteredIncorrectAnswer(error.serverErrorRes);
    }

  };
  return MFAManager;
});