define(['CommonUtilities', 'OLBConstants', 'ViewConstants'], function (CommonUtilities, OLBConstants, ViewConstants) {
  var frmTransfers = "frmTransfersEur";
  var frmConfirm = "frmConfirmEur";
  var frmConfirmAccount = "frmConfirmAccountEur";
  var frmVerifyAccount = "frmVerifyAccountEur";
  var frmAddExternalAccount = "frmAddExternalAccountEur";
  var frmAddInternalAccount = "frmAddInternalAccountEur";
  var frmAcknowledgement = "frmAcknowledgementEur";
  this.transferData= "";

  function handleDateFormat(date) {
    if (date === undefined || date === null || date === '') {
      return null;
    }
    if (date instanceof Date) {
      return date.getFullYear() + '-' + (date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth()) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    } else {
      var dateObj = new Date(date);
      return dateObj.getFullYear() + '-' + (dateObj.getMonth() < 10 ? '0' + dateObj.getMonth() : dateObj.getMonth()) + '-' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate());
    }
  }
  function TransferPresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(TransferPresentationController, kony.mvc.Presentation.BasePresenter);
  /** Presents Transfer Form
     * @param {object} viewModel - To handle the flow of where it is suppose to go
     */
  TransferPresentationController.prototype.presentTransfers = function (viewModel) {
    applicationManager.getNavigationManager().navigateTo(frmTransfers);
    applicationManager.getNavigationManager().updateForm(viewModel, frmTransfers);
  };


  /** Search Payees 
     * @member  Transfer_PresentationController
     * @param  {object} data Search Inputs
     * @throws {void} None
     * @returns {void} None
     */
  TransferPresentationController.prototype.searchTransferPayees = function (data) {
    if (data && data.searchKeyword.length > 0) {
      var searchInputs = {
        searchString: data.searchKeyword
      }
      var criteria = kony.mvc.Expression.eq("searchString", data.searchKeyword)
      this.showProgressBar();
      applicationManager.getAccountManager().fetchExternalAccountsByCriteria(criteria, this.searchSuccess.bind(this, searchInputs), this.searchFailure.bind(this));
    }
  };

  TransferPresentationController.prototype.searchSuccess = function (searchInputs, payees) {
    var viewModel = {};
    viewModel.searchTransferPayees = {
      externalAccounts: payees,
      searchInputs: searchInputs
    }
    this.hideProgressBar();
    this.presentTransfers(viewModel)
  }
  TransferPresentationController.prototype.searchFailure = function (searchInputs, response) {
    var viewModel = {};
    viewModel.searchTransferPayees = { "error": response.errorMessage };
    this.hideProgressBar();
    this.presentTransfers(viewModel)
  }

  /** Entry Point Method of Transfer Module
     * @param {object} context - value to handle the flow
     */
  TransferPresentationController.prototype.showTransferScreen = function (context) {
    var initialContext = context || {};
    if (initialContext.initialView === undefined) {
      initialContext.initialView = "makeTransfer";
    }
    if (initialContext.initialView === "recent") {
      this.resetAndShowProgressBar();
      this.showRecentUserTransactions();
      return;
    }
    if (initialContext.initialView === "externalAccounts") {
      this.resetAndShowProgressBar();
      this.showExternalAccounts();
      return;
    }
    if (initialContext.initialView === "addInternalAccounts") {
      applicationManager.getNavigationManager().navigateTo(frmAddInternalAccount);
      this.showSameBankAccounts();
      return;
    }
    if (initialContext.initialView === "addExternalAccounts") {
      applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
      this.showDomesticAccounts();
      return;
    }
    if (initialContext.transactionObject) {
      this.resetAndShowProgressBar();
      this.repeatTransfer(initialContext.transactionObject, initialContext.onCancelCreateTransfer);
      return;
    }
    if (initialContext.editTransactionObject) {
      this.resetAndShowProgressBar();
      this.showMakeTransferForEditTransaction(context.editTransactionObject, context.onCancelCreateTransfer);
      return;
    }
    if (initialContext.accountTo) {
      this.resetAndShowProgressBar();
      this.loadAccountsByTransferType(null, initialContext.accountTo);
      return;
    }
    this.presentTransfers({
      gateway: { overrideFromAccount: initialContext.accountObject ? initialContext.accountObject.accountID : null }
    })

  };

  TransferPresentationController.prototype.resetAndShowProgressBar = function () {
    this.presentTransfers({
      resetAndShowProgressBar: {}
    })
  }

  /** Edit Transfer - 
     * @param  {object} transaction Model  Object
     */

  TransferPresentationController.prototype.editTransfer = function (transactionObject) {
    var date = CommonUtilities.getFrontendDateString(transactionObject.scheduledDate);
    var modifiedDate = CommonUtilities.sendDateToBackend(date);
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var transactionOBJ = new transactionsModel({
      'transactionId': transactionObject.transactionId,
      'isScheduled': transactionObject.isScheduled,
      'fromAccountNumber': transactionObject.fromAccountNumber,
      'amount': transactionObject.amount,
      'transactionsNotes': transactionObject.transactionsNotes,
      'toAccountNumber': transactionObject.toAccountNumber,
      'frequencyType': transactionObject.frequencyType,
      'transactionType': transactionObject.transactionType,
      'scheduledDate': modifiedDate,
      'ExternalAccountNumber': transactionObject.ExternalAccountNumber,
      'numberOfRecurrences': transactionObject.numberOfRecurrences,
      'frequencyStartDate': handleDateFormat(transactionObject.frequencyStartDate),
      'frequencyEndDate': handleDateFormat(transactionObject.frequencyEndDate),
      'category': transactionObject.Category
    });
    this.showProgressBar();
    applicationManager.getTransactionManager().
      updateTransaction(transactionOBJ, this.editTransactionSuccess.bind(this), this.editTransactionError.bind(this))
  };

  TransferPresentationController.prototype.editTransactionSuccess = function () {
    this.getScheduledUserTransactions();
  }

  TransferPresentationController.prototype.editTransactionError = function (response) {
    var transferData = {};
    transferData.transferError = response.errorMessage;
    this.hideProgressBar();
    this.presentTransfers(transferData);
  }

  TransferPresentationController.prototype.deleteTransfer = function (transaction) {
    this.showProgressBar();
    applicationManager.getTransactionManager()
      .deleteTransaction({
        transactionId: transaction.transactionId,
        transactionType: transaction.transactionType
     }, this.cancelTransactionOccurrenceSuccessCallback.bind(this), this.cancelTransactionOccurrenceErrorCallback.bind(this));
  };

  TransferPresentationController.prototype.cancelTransactionOccurrence = function (transaction) {
    applicationManager.getTransactionManager().deleteRecurrenceTransaction(transaction, this.cancelTransactionOccurrenceSuccessCallback.bind(this), this.cancelTransactionOccurrenceErrorCallback.bind(this));
  }

  TransferPresentationController.prototype.cancelTransactionOccurrenceSuccessCallback = function () {
    this.getScheduledUserTransactions();
  }
  TransferPresentationController.prototype.cancelTransactionOccurrenceErrorCallback = function (response) {
    this.hideProgressBar();
    this.presentServerError(response.errorMessage);
  }

  /**Fetches the list of pending accounts
   */
  TransferPresentationController.prototype.showPendingAccountsCount = function () {
    var RecipientManger = applicationManager.getRecipientsManager();
    RecipientManger.fetchAllExternalBenificiaries(this.fetchAllExternalBenificiariesSuccess.bind(this), this.fetchAllExternalBenificiariesError.bind(this));
  };
  /**Success callback after fetching external account and then update form transfer
  */
  TransferPresentationController.prototype.fetchAllExternalBenificiariesSuccess = function (data) {
    var accounts = {
      pendingAccounts: data
    };
    this.presentTransfers(accounts);
  };
  /**Error callback after fetching external account and then update form transfer
  */
  TransferPresentationController.prototype.fetchAllExternalBenificiariesError = function (error) {
    var accounts = {
      pendingAccounts: { error: true }
    };
    this.presentTransfers(accounts);
  };
  /** Shows Same Bank Accounts
     */
  TransferPresentationController.prototype.showSameBankAccounts = function () {
    var self = this;
    var RecipientManger = applicationManager.getRecipientsManager();
    this.showProgressBar();
    RecipientManger.fetchAllInternalBenificiaries(this.showSameBankAccountsSuccess.bind(this), this.showSameBankAccountsError.bind(this));
  };
  /** Success method when all internal beneficiaries are fetched successfully
     */
  TransferPresentationController.prototype.showSameBankAccountsSuccess = function (response) {
    var accounts = {
      sameAccounts: response
    };
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo('frmAddInternalAccount');
    applicationManager.getNavigationManager().updateForm(accounts, 'frmAddInternalAccount');
  };
  /** Error method when all internal beneficiaries are not fetched successfully
     */
  TransferPresentationController.prototype.showSameBankAccountsError = function (error) {

  };
  /** Parallely fetches the initial data - User Accounts and User Profile
     * @param  {object} businessController 
     * @param  {function} success callback when successfull 
     * @param  {function} error callback when unsuccessfull 
     */
  var fetchUserAccountsAndProfile = function (businessController, success, error) {
    function completionCallback(asyncResponse) {
      if (asyncResponse.isAllSuccess()) {
        var responseList = asyncResponse.responses;
        success(responseList[0].data, responseList[1].data, applicationManager.getUserPreferencesManager().getUserObj());
      }
      else {
        error();
      }
    }
    var asyncManager = applicationManager.getAsyncManager();
    asyncManager.callAsync([
      asyncManager.asyncItem(applicationManager.getAccountManager(), 'fetchInternalAccounts'),
      asyncManager.asyncItem(applicationManager.getAccountManager(), 'fetchExternalAccounts'),
    ], completionCallback);
  };
  /**Load Accounts by Transfer Type
     * @param {string} type Type of Transfer
     * @param {object} accountTo Account to object
     * @param {object} accountFrom Account From Object
     */
  TransferPresentationController.prototype.loadAccountsByTransferType = function (type, accountTo, accountFrom) {
    this.showProgressBar();
    fetchUserAccountsAndProfile(this.businessController, this.onDataFetchComplete.bind(this, type, accountTo, accountFrom));
  };
  /**Updates the form Transfer After all the service responce comes
     * @param {string} type Type of Transfer
     * @param {object} accountTo Account to object
     * @param {object} accountFrom Account From Object
     * @param {object} userAccounts All Accounts of the User object
     * @param {object} externalAccounts All External Accounts of the User object
     * @param {object} userProfile  User Profile of the user object
     */
  TransferPresentationController.prototype.onDataFetchComplete = function (type, accountTo, accountFrom, userAccounts, externalAccounts, userProfile) {
    var accountsValues = {
      accountsValue: {
        type: type,
        accountTo: accountTo,
        accountFrom: accountFrom,
        userAccounts: userAccounts,
        externalAccounts: externalAccounts,
        userProfile: userProfile
      }

    };
    this.hideProgressBar();
    this.presentTransfers(accountsValues);
  };
  /** Confirm Transfer  - Shows Confirmation for transfer
     * @param {object} makeTransferViewModel data of transfer
     * @param {object} formData new data
     */
  TransferPresentationController.prototype.confirmTransfer = function (makeTransferViewModel, formData) {
    var transfer = {
      transferConfirm: {
        makeTransferViewModel: makeTransferViewModel,
        transferData: formData
      }
    };
    applicationManager.getNavigationManager().navigateTo(frmConfirm);
    applicationManager.getNavigationManager().updateForm(transfer, frmConfirm);

  };
  /**Shows Transfer form with existing transferData
     * @param {object} transferConfirmViewModel
     */
  TransferPresentationController.prototype.modifyTransaction = function (transferConfirmViewModel) {
    this.presentTransfers({ modifyTransaction: transferConfirmViewModel.transferData });
  };

  /**Saves Transfer Data
     * @param {object} transferData Create Transfer from form Data
     */
  TransferPresentationController.prototype.createTransfer = function (transferData) {
    this.transferData = transferData;
     var mfaParams = {
            serviceName: "SERVICE_ID_1",
        };
    var transactionManager = applicationManager.getTransactionManager();
    transactionManager.setTransactionAttribute("fromAccountNumber", transferData.fromAccountNumber.accountID);
    transactionManager.setTransactionAttribute("amount", transferData.amount);
    transactionManager.setTransactionAttribute("transactionsNotes", transferData.notes);
    transactionManager.setTransactionAttribute("ExternalAccountNumber", transferData.ExternalAccountNumber);
    transactionManager.setTransactionAttribute("isScheduled", transferData.isScheduled);
    transactionManager.setTransactionAttribute("transactionType", transferData.transactionType);
    transactionManager.setTransactionAttribute("toAccountNumber", transferData.toAccountNumber);
    transactionManager.setTransactionAttribute("frequencyType", transferData.frequencyType);
    transactionManager.setTransactionAttribute("numberOfRecurrences", transferData.numberOfRecurrences);
    transactionManager.setTransactionAttribute("frequencyEndDate", transferData.frequencyEndDate);
    transactionManager.setTransactionAttribute("scheduledDate", transferData.scheduledDate);
    transactionManager.setTransactionAttribute("IBAN", transferData.IBAN);
    transactionManager.setTransactionAttribute("swiftCode", transferData.swiftCode);
    transactionManager.setTransactionAttribute("bankName", transferData.bankName);
    transactionManager.setTransactionAttribute("transactionCurrency", transferData.transactionCurrency);
    transactionManager.setTransactionAttribute("feeCurrency", transferData.feeCurrency);
    transactionManager.setTransactionAttribute("fee", transferData.fee);
    transactionManager.setTransactionAttribute("feePaidByReceipent", transferData.feePaidByReceipent);
    transactionManager.setTransactionAttribute("feePaidByReceipent", transferData.feePaidByReceipent);
    transactionManager.setTransactionAttribute("beneficiaryName", transferData.reciepientName);
    transactionManager.setTransactionAttribute("MFAAttributes", mfaParams);
    this.showProgressBar(); 
    transactionManager.createTransaction(transactionManager.getTransactionObject(), this.createTransferSuccessCallback.bind(this), this.createTransferErrorCallback.bind(this));
  };
  /**Success callback after the transaction is saved
   * @param {object} transferData transaction data
   * @param {object} response success response from backend
  */
  TransferPresentationController.prototype.createTransferSuccessCallback = function (response) {
     if (response.referenceId) {
        this.transferData.referenceId = response.referenceId;
        var acknowledgeViewModel = {};
        acknowledgeViewModel.transferData = this.transferData;
        this.fetchUserAccountAndNavigate(acknowledgeViewModel);
     }else {
            var mfaJSON = {
               "serviceName" : "SERVICE_ID_1",
                "flowType": "TRANSFERS_EURO",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
  };
  /**Error callback after the transaction is saved
   * @param {object} response failure response from backend
  */
  TransferPresentationController.prototype.createTransferErrorCallback = function (response) {
    var viewmodel = {};
    viewmodel.transferError = response.errorMessage
    this.hideProgressBar();
    this.presentTransfers(viewmodel);
  };
  /** fetch Scheduled user Transactions 
     * @param  {object} config configuration by which scheduled transaction are fetched  
     * @param  {object} recordNum Pagination Number
     */
  TransferPresentationController.prototype.fetchScheduledUserTransactions = function (config, recordNum) {
    var paginationManager = applicationManager.getPaginationManager();
    var params = paginationManager.getValues(scheduledConfig, config);
    if (config !== undefined) {
      params.sortBy = config.sortBy;
    }
    var criteria = {
      "firstRecordNumber": params.offset,
      "lastRecordNumber": params.limit,
      "isScheduled": 1,
      "sortBy": params.sortBy,
      "order": params.order
    }
    var transactionManager = applicationManager.getTransactionManager();
    this.showProgressBar();
    transactionManager.fetchScheduledUserTransactions(criteria, this.fetchScheduledUserTransactionsSuccess.bind(this), this.fetchScheduledUserTransactionsFailure.bind(this));
  };
  /**Success callback after the Scheduled transaction are fetched
   * @param {object} response user Scheduled transactions
 */
  TransferPresentationController.prototype.fetchScheduledUserTransactionsSuccess = function (response) {
    var paginationManager = applicationManager.getPaginationManager();
    var viewProperties = {};
    if (response.length > 0) {
      paginationManager.updatePaginationValues();
      viewProperties.scheduledTransfers = response;
      viewProperties.pagination = paginationManager.getValues(scheduledConfig);
      viewProperties.config = scheduledConfig;
    } else {
      var values = paginationManager.getValues(scheduledConfig);
      if (values.offset > 0) {
        viewProperties.noMoreRecords = true;
      } else {
        viewProperties.fromScheduleTransfer = true;
        viewProperties.noTransaction = true;
      }
    }
    this.hideProgressBar();
    this.presentTransfers(viewProperties);
  };

  /**Error callback after fetching of  Scheduled transaction fails
   * @param {object} response failure object of scheduled transactions
 */
  TransferPresentationController.prototype.fetchScheduledUserTransactionsFailure = function (response) {
    var noTransaction = "noTransaction";
    this.hideProgressBar();
    this.presentTransfers(noTransaction);
  };

  /**Initialize the value to fetch the next page Scheduled Transactions
 */
  TransferPresentationController.prototype.fetchNextScheduledUserTransactions = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getNextPage();
    this.fetchScheduledUserTransactions();
  };
  /**Initialize the value to fetch the previous page Scheduled Transactions
 */
  TransferPresentationController.prototype.fetchPreviousScheduledUserTransactions = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getPreviousPage();
    this.fetchScheduledUserTransactions();
  };
  /**Reset the value to fetch the Scheduled Transactions
 */
  TransferPresentationController.prototype.getScheduledUserTransactions = function (data) {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.resetValues();
    this.fetchScheduledUserTransactions(data);
  };
  /**Shows Server error
     * @param  {object} data - Data from server
     */
  TransferPresentationController.prototype.presentServerError = function (data) {
    var error = {
      serverError: data
    };
    this.hideProgressBar();
    this.presentTransfers(error);
  };
  var recentConfig = {
    'sortBy': 'transactionDate',
    'defaultSortBy': 'transactionDate',
    'order': OLBConstants.DESCENDING_KEY,
    'defaultOrder': OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit': OLBConstants.PAGING_ROWS_LIMIT
  };

  var scheduledConfig = {
    'sortBy': 'scheduledDate',
    'defaultSortBy': 'scheduledDate',
    'order': OLBConstants.DESCENDING_KEY,
    'defaultOrder': OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit': OLBConstants.PAGING_ROWS_LIMIT
  };


  /**Fetch recent User Transactions
     * @param  {object} data object with respect of which recent transactions are fetched 
     */
  TransferPresentationController.prototype.fetchRecentUserTransactions = function (data) {
    var paginationManager = applicationManager.getPaginationManager();
    var params = paginationManager.getValues(recentConfig, data);
    if (data !== undefined) {
      params.sortBy = data.sortBy;
    }
    var criteria = {
      "firstRecordNumber": params.offset,
      "lastRecordNumber": params.limit,
      "isScheduled": 0,
      "sortBy": params.sortBy,
      "order": params.order
    }
    var transactionManager = applicationManager.getTransactionManager();
    this.showProgressBar();
    transactionManager.fetchUserRecentTransactions(criteria, this.fetchRecentUserTransactionsSuccess.bind(this), this.fetchRecentUserTransactionsFailure.bind(this));
  };
  /**Fetch recent User Transactions success callback
     * @param  {object} response response object which have recent transactions
     */
  TransferPresentationController.prototype.fetchRecentUserTransactionsSuccess = function (response) {
    var paginationManager = applicationManager.getPaginationManager();
    var viewProperties = {};
    if (response.length > 0) {
      paginationManager.updatePaginationValues();
      viewProperties.recentTransfers = response;
      viewProperties.pagination = paginationManager.getValues(recentConfig);
      viewProperties.config = recentConfig;
    } else {
      var values = paginationManager.getValues(recentConfig);
      if (values.offset > 0) {
        viewProperties.noMoreRecords = true;
      } else {
        viewProperties.fromRecentTransfer = true;
        viewProperties.noTransaction = true;
      }
    }
    this.hideProgressBar();
    this.presentTransfers(viewProperties);
  };
  /**Fetch recent User Transactions failure callback
     * @param  {object} response response object which comes form service when the service fails
     */
  TransferPresentationController.prototype.fetchRecentUserTransactionsFailure = function (response) {
    var noTransaction = "noTransaction";
    this.hideProgressBar();
    this.presentTransfers(noTransaction);

  };
  /**Initialises the value to fetch recent transaction for next page
     */
  TransferPresentationController.prototype.fetchNextRecentUserTransactions = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getNextPage();
    this.fetchRecentUserTransactions();
  };
  /**Initialises the value to fetch previous transactions for previous date
     */
  TransferPresentationController.prototype.fetchPreviousRecentUserTransactions = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getPreviousPage();
    this.fetchRecentUserTransactions();
  };
  /**Resets the pagination value to fetch recent transactions
     */
  TransferPresentationController.prototype.showRecentUserTransactions = function (sortingData) {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.resetValues();
    this.fetchRecentUserTransactions(sortingData);
  };
  /**Resets the pagination value to fetch external transactions
   * @param {object} data data for sorting 
*/
  TransferPresentationController.prototype.showExternalAccounts = function (data) {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.resetValues();
    this.fetchExternalAccounts(data);
  };
  /**Shows External Accounts Based on flow
     * @param {object} navFlow flow from where the external accounts are suppose to show
     */
  TransferPresentationController.prototype.fetchExternalAccounts = function (navFlow) {
    var paginationManager = applicationManager.getPaginationManager();
    var params = paginationManager.getValues(recentConfig, navFlow);
    if (navFlow !== undefined) {
      params.sortBy = navFlow.sortBy;
    }
    else {
      params.sortBy = "createdOn";
    }
    this.getExternalAccounts({
      "offset": params.offset,
      "limit": params.limit,
      'resetSorting': true,
      "sortBy": params.sortBy,
      "order": params.order
    });

    // if(navFlow==undefined){
    //   this.getExternalAccounts({
    //     "offset":params.offset,
    //     "limit":params.limit,
    //     'resetSorting': true,
    //     "sortBy" : params.sortBy,
    //     "order" : params.order
    //   });
    // }else 
  };
  /** Get External accounts from backend
     * @param {object} value - Sorting and pagination parameters 
     */
  TransferPresentationController.prototype.getExternalAccounts = function (value) {
    var recipientManager = applicationManager.getRecipientsManager();
    this.showProgressBar();
    recipientManager.fetchAllExternalAccountsWithPagination(value, this.getExternalAccountsSuccess.bind(this), this.getExternalAccountsFailure.bind(this));
  };
  /**Success callback after external accounts are fetched and updates the from transfers
     * @param  {object} response object which consists of external account
     */
  TransferPresentationController.prototype.getExternalAccountsSuccess = function (response) {
    var paginationManager = applicationManager.getPaginationManager();
    var viewProperties = {};
    if (response.length > 0) {
      paginationManager.updatePaginationValues();
      viewProperties.externalAccounts = response;
      viewProperties.pagination = paginationManager.getValues(recentConfig);
      viewProperties.pagination.limit = response.length;
      viewProperties.config = recentConfig;
    } else {
      var values = paginationManager.getValues(recentConfig);
      if (values.offset > 0) {
        viewProperties.noMoreRecords = true;
      } else {
        viewProperties.fromExternalAccount = true;
        viewProperties.noTransaction = true;
      }
    }
    this.hideProgressBar();
    this.presentTransfers(viewProperties);
  };
  /**Failure callback when external accounts are fetched
     * @param  {object} response failure object which comes from backend
     */
  TransferPresentationController.prototype.getExternalAccountsFailure = function (response) {
    var errorExternalAccounts = "errorExternalAccounts";
    this.hideProgressBar();
    this.presentTransfers(errorExternalAccounts);

  };
  /**Initialises the pagination values to fetch external accounts for next page
     */
  TransferPresentationController.prototype.fetchNextExternalAccounts = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getNextPage();
    this.fetchExternalAccounts();
  };
  /**Initialises the pagination values to fetch external accounts for previous page
     */
  TransferPresentationController.prototype.fetchPreviousExternalAccounts = function () {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.getPreviousPage();
    this.fetchExternalAccounts();
  };

  var externalAccountsConfig = {
    'sortBy': 'nickName',
    'defaultSortBy': 'nickName',
    'order': OLBConstants.ASCENDING_KEY,
    'defaultOrder': OLBConstants.ASCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit': OLBConstants.PAGING_ROWS_LIMIT
  };

  /** Shows Transfer form with existing transaction
     * @param {object} transactionObject Transaction Object
     * @param {function} onBackPressed when cancel is clicked 
     */
  TransferPresentationController.prototype.repeatTransfer = function (transactionObject, onBackPressed) {
    this.showProgressBar();
    fetchUserAccountsAndProfile(this.businessController, this.repeatTransferSuccess.bind(this, transactionObject, onBackPressed));
  };
  /** Repeats the transaction successfully and then maps the data
     * @param {object} transactionObject Transaction Object
     * @param {function} onBackPressed when cancel is clicked 
     * @param {object} userAccounts All Accounts of the User object
     * @param {object} externalAccounts All External Accounts of the User object
     */
  TransferPresentationController.prototype.repeatTransferSuccess = function (transactionObject, onBackPressed, userAccounts, externalAccounts) {
    var transaction = {
      repeatTransaction: {
        transactionObject: transactionObject,
        onBackPressed: onBackPressed,
        userAccounts: userAccounts,
        externalAccounts: externalAccounts
      }
    };
    this.hideProgressBar();
    this.presentTransfers(transaction);
  };
  /** Shows Transfer for Editing a Transaction
     * @param {object} editTransactionObject Objct of Transaction Model
     * @param {function} onCancelCreateTransfer function to be binded on click of cancel button
     */
  TransferPresentationController.prototype.showMakeTransferForEditTransaction = function (editTransactionObject, onCancelCreateTransfer) {
    this.showProgressBar();
    fetchUserAccountsAndProfile(this.businessController, this.showMakeTransferForEditTransactionSuccess.bind(this, editTransactionObject, onCancelCreateTransfer));
  };
  /** Success callback after Editing a Transaction
     * @param {object} editTransactionObject Objct of Transaction Model
     * @param {function} onCancelCreateTransfer function to be binded on click of cancel button
     * @param {object} userAccounts All Accounts of the User object
     * @param {object} externalAccounts All External Accounts of the User object
     */
  TransferPresentationController.prototype.showMakeTransferForEditTransactionSuccess = function (editTransactionObject, onCancelCreateTransfer, userAccounts, externalAccounts) {
    var transaction = {
      editTransaction: {
        editTransactionObject: editTransactionObject,
        onCancelCreateTransfer: onCancelCreateTransfer,
        userAccounts: userAccounts,
        externalAccounts: externalAccounts
      }
    };
    this.hideProgressBar();
    this.presentTransfers(transaction);
  };
  /** Save Changed External Account
     * @param {object} editedInfo Save Changed External Account to backend
     */
  TransferPresentationController.prototype.saveChangedExternalAccount = function (editedInfo) {
    var recipientsManager = applicationManager.getRecipientsManager();
    this.showProgressBar();
    recipientsManager.editABenificiary(editedInfo, this.saveChangedExternalAccountSuccess.bind(this), this.saveChangedExternalAccountFailure.bind(this));
  };
  /** Success callback after Saving Changed External Account
     * @param {object} response response Changing External Account to backend
     */
  TransferPresentationController.prototype.saveChangedExternalAccountSuccess = function (response) {
    this.showExternalAccounts();
  };
  /** Failure callback after Saving Changed External Account
  */
  TransferPresentationController.prototype.saveChangedExternalAccountFailure = function () {
    var error = {
      serverError: true
    };
    this.hideProgressBar();
    this.presentTransfers(error);
  };
  /** Deletes External Account
     * @param {string} accountNumber Account Number
     */
  TransferPresentationController.prototype.deleteExternalAccount = function (params) {
    var recipientsManager = applicationManager.getRecipientsManager();
    this.showProgressBar();
    recipientsManager.deleteABenificiary(params, this.deleteExternalAccountSuccess.bind(this), this.deleteExternalAccountFailure.bind(this));
  };
  /** Shows External Account after deletion
     * @param {object} response Success response after deletion
     */
  TransferPresentationController.prototype.deleteExternalAccountSuccess = function (response) {
    this.showExternalAccounts();
  };
  /** Failure callback when Deletion of External Account fails
     */
  TransferPresentationController.prototype.deleteExternalAccountFailure = function () {
    var error = {
      serverError: true
    };
    this.hideProgressBar();
    this.presentTransfers(error);
  };
  /** Shows details of the account to be verified
     * @param {object} selectedRow Data of the account seleted
     */
  TransferPresentationController.prototype.showVerifyAccounts = function (selectedRow) {
    var accounts = {
      verifyAccounts: selectedRow
    };
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm(accounts, frmVerifyAccount);
  };

  TransferPresentationController.prototype.showDomesticAccountsFromTransferFlow = function (data) {
    var accountsManager = applicationManager.getAccountManager();
    var servicesForUser = applicationManager.getConfigurationManager().getServicesListForUser();
    var serviceName = "Interbank Account to Account Fund Transfer";
    if (servicesForUser) {
      serviceName = servicesForUser.filter(function (dataItem) {
        if (dataItem.displayName === "OtherBankAccountsTransfer") return true;
      });
      if (serviceName && serviceName.length > 0) {
        serviceName = serviceName[0].serviceName;
      }
    }
    var params = {
      "IBAN": data.IBAN,
      "serviceName": serviceName
    };
    accountsManager.fetchBankDetails(params, this.showDomesticAccountsFromTransferFlowSuccess.bind(this, data), this.showDomesticAccountsFromTransferFlowFailure.bind(this, data));
  };
  TransferPresentationController.prototype.showDomesticAccountsFromTransferFlowSuccess = function (data, response) {
    data.bankName = "Kony Bank";
    this.showDomesticAccounts(data);
  };
  TransferPresentationController.prototype.showDomesticAccountsFromTransferFlowFailure = function (data, response) {
    data.bankName = "Kony Bank";
    this.showDomesticAccounts(data);
  };
  /** Show Domestic Accounts 
     */
  TransferPresentationController.prototype.showDomesticAccounts = function (data) {
    var recipientsManager = applicationManager.getRecipientsManager();
    this.showProgressBar();
    recipientsManager.fetchAllExternalBenificiaries(this.showDomesticAccountsSuccess.bind(this, data), this.showDomesticAccountsFailure.bind(this));
  };
  /** Gives Domestic Accounts
     * @param {object} response Success response after deletion
     */
  TransferPresentationController.prototype.showDomesticAccountsSuccess = function (data, response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    var viewModel = {
      "domesticAccounts": {
        data: data,
        accountsList: response
      }
    };
    applicationManager.getNavigationManager().updateForm(viewModel, frmAddExternalAccount);
  };
  /** Failure callback when Domestic Accounts fetch fails
     */
  TransferPresentationController.prototype.showDomesticAccountsFailure = function () {
    var error = {
      serverError: true
    };
    this.hideProgressBar();
    this.presentTransfers(error);
  };
  /** Shows International Accounts
     * @param  {object} fController  
     */
  TransferPresentationController.prototype.showInternationalAccounts = function (data) {
    this.showProgressBar();
    var recipientsManager = applicationManager.getRecipientsManager();
    recipientsManager.fetchAllExternalBenificiaries(this.showInternationalAccountsSuccess.bind(this, data), this.showInternationalAccountsFailure.bind(this));
  };
  /** Gives International Accounts
     * @param {object} response Success response after deletion
     */
  TransferPresentationController.prototype.showInternationalAccountsSuccess = function (data, response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    var viewModel = {
      "internationalAccounts": {
        data: data,
        accountsList: response
      }
    };
    applicationManager.getNavigationManager().updateForm(viewModel, frmAddExternalAccount);
  };
  /** Failure callback when International Accounts fetch fails
     */
  TransferPresentationController.prototype.showInternationalAccountsFailure = function () {
    var error = {
      serverError: true
    };
    this.presentTransfers(error)
  };
  /** Shows Selected Account Transactions
   * @param {object} selectedRow Selected Row data
   */
  TransferPresentationController.prototype.showSelectedAccountTransactions = function (selectedRow) {
    var transactionManager = applicationManager.getTransactionManager();
    this.showProgressBar();
    var param = {
      "firstRecordNumber": 0,
      "lastRecordNumber": 1000
    };
    if(selectedRow.isInternationalAccount === "true") {
      param.accountNumber = selectedRow.txtAccountNumber.text;
    } else {
      param.IBAN = selectedRow.txtAccountNumber.text;
    }
    transactionManager.fetchToExternalAccountTransactions(param, this.showSelectedAccountTransactionsSuccess.bind(this, selectedRow), this.showSelectedAccountTransactionsFailure.bind(this))
  };
  /** Gives Selected Account transaction
       * @param {object} selectedRow External Account Data
       * @param {object} response Success response after deletion
       */
  TransferPresentationController.prototype.showSelectedAccountTransactionsSuccess = function (selectedRow, response) {
    response.push({
      "accountNumber": selectedRow.lblAccountTypeValue,
      "nickName": selectedRow.lblAccountName
    })
    this.hideProgressBar();
    this.presentTransfers({ "viewExternalAccountTransactionActivity": response });
  };
  /** Failure callback when Selected Account transaction fails
     */
  TransferPresentationController.prototype.showSelectedAccountTransactionsFailure = function () {
    var error = {
      serverError: true
    };
    this.hideProgressBar();
    this.presentTransfers(error);
  };

  /** fetches Bank Details
   * @param {String} routingNumber routing number 
   * @param {String} serviceName  service name
   */
  TransferPresentationController.prototype.fetchBankDetails = function (IBAN, serviceName) {
    var self = this;
    var params = {
      "IBAN": IBAN,
      "serviceName": serviceName
    }
    this.showProgressBar();
    var accountsManager = applicationManager.getAccountManager();
    accountsManager.fetchBankDetails(params, this.fetchBankDetailsSuccess.bind(this), this.fetchBankDetailsFailure.bind(this))
  };
  /** Gives Details of the bank
     * @param {object} response Success response of bank details
     */
  TransferPresentationController.prototype.fetchBankDetailsSuccess = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().updateForm(
      {
        "updateBankName": {
          bankName: response.bankName
        }
      }
      , frmAddExternalAccount);
  };
  /** Failure callback when fetching of bank details fails
     */
  TransferPresentationController.prototype.fetchBankDetailsFailure = function () {
    this.hideProgressBar();
    var error = {
      serverError: true
    };
    applicationManager.getNavigationManager().updateForm(error, frmAddExternalAccount);

  };
  /** fetches Bank Details For InternationalTransfer
    * @param {String} swiftCode swift code 
    * @param {String} serviceName  service name
    */
  TransferPresentationController.prototype.fetchBankDetailsForInternationalTransfer = function (swiftCode, serviceName) {
    var params = {
      "swiftCode": swiftCode,
      "serviceName": serviceName
    };
    this.showProgressBar();
    var accountsManager = applicationManager.getAccountManager();
    accountsManager.fetchBankDetails(params, this.fetchBankDetailsForInternationalTransferSuccess.bind(this), this.fetchBankDetailsForInternationalTransferFailure.bind(this))
  };
  /** Gives Details of the bank for international transfer
     * @param {object} response Success response of bank details
     */
  TransferPresentationController.prototype.fetchBankDetailsForInternationalTransferSuccess = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().updateForm({ "updateInternationalBankName": { data: response.bankName } }, frmAddExternalAccount);
  };
  /** Failure callback when fetching of bank details for international transfer fails
     */
  TransferPresentationController.prototype.fetchBankDetailsForInternationalTransferFailure = function () {
    this.hideProgressBar();
    var error = {
      serverError: true
    };
    applicationManager.getNavigationManager().updateForm(error, frmAddExternalAccount);
  };
  /** Present frmConfirmAccount for international Account
   * @param {object} viewModel Details of the Account
   */
  TransferPresentationController.prototype.addInternationalAccount = function (viewModel) {
    applicationManager.getNavigationManager().navigateTo(frmConfirmAccount);
    applicationManager.getNavigationManager().updateForm({ "internationalAccount": viewModel }, frmConfirmAccount);
  };
  /** Present frmConfirmAccount for domestic Account
   * @param {object} viewModel Details of the Account
   */
  TransferPresentationController.prototype.addDomesticAccount = function (viewModel) {
    applicationManager.getNavigationManager().navigateTo(frmConfirmAccount);
    applicationManager.getNavigationManager().updateForm({ "domesticAccount": viewModel }, frmConfirmAccount);
  };
  /** Storing data for next form
     * @param {object} viewModel Data of the account to be created
     */
  TransferPresentationController.prototype.dataForNextForm = function (viewModel) {
    this.viewModelData = viewModel;
  };
  /** Present frmPrintTransfer
   * @param {object} viewModel Details of the Account
   */
  TransferPresentationController.prototype.showPrintPage = function (data) {
    applicationManager.getNavigationManager().navigateTo("frmPrintTransfer");
    applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
  };
  /** Assigns the frequency
     */
  TransferPresentationController.prototype.assignFrequency = function (viewModel) {
    var transactionManager = applicationManager.getTransactionManager();
    var frequency = transactionManager.getAvailableFrequencyType();
    this.presentTransfers({ "frequencyModel": frequency });
  };
  /** Cancel The Transaction or adding of account
     */
  TransferPresentationController.prototype.cancelTransaction = function () {
    var data = this.viewModelData;
    if (kony.application.getCurrentForm().id !== frmAddExternalAccount || kony.application.getCurrentForm().id !== frmAddInternalAccount) {
      this.presentTransfers();
    }
    else {
      if (data.internalAccount) {
        this.showSameBankAccounts();
      }
      else if (data.domesticAccount) {
        this.showDomesticAccounts();
      } else {
        this.showInternationalAccounts();
      }
    }
  };
  /** Modify External Account
     */
  TransferPresentationController.prototype.modifyAccountInfo = function (type) {
    var typeMap = {
      'internal': frmAddInternalAccount,
      'domestic': frmAddExternalAccount,
      'international': frmAddExternalAccount
    }
    applicationManager.getNavigationManager().navigateTo(typeMap[type]);
  };
  /** Present frmConfirmAccount
    * @param {object} viewModel data of Account
    */
  TransferPresentationController.prototype.addInternalAccount = function (viewModel) {
    applicationManager.getNavigationManager().navigateTo(frmConfirmAccount);
    applicationManager.getNavigationManager().updateForm({ "internalAccount": viewModel }, frmConfirmAccount);
  };
  /** Adding of new Account 
  */
  TransferPresentationController.prototype.navigateToVerifyAccount = function (data) {
    var accountsManager = applicationManager.getAccountManager();
    var self = this;
    var result = data;
    if (data.internalAccount) {
      data.internalAccount["isSameBankAccount"] = "true"; //Adding an additional field to identify an external account within same bank
      data.internalAccount["isInternationalAccount"] = "false";
      data.internalAccount["isVerified"] = "true";
      this.showProgressBar();
      accountsManager.createExternalAccounts(data.internalAccount, this.internalAccountSuccess.bind(this, result), this.internalAccountFailure.bind(this));
    }

    if (data.domesticAccount) {
      data.domesticAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.domesticAccount["isInternationalAccount"] = "false";
      data.domesticAccount.isVerified = "true";
      this.showProgressBar();
      accountsManager.createExternalAccounts(data.domesticAccount, this.domesticAccountSuccess.bind(this, result), this.domesticAccountFailure.bind(this));
    }
    if (data.internationalAccount) {
      data.internationalAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.internationalAccount["isInternationalAccount"] = "true";
      data.internationalAccount.isVerified = "true";
      this.showProgressBar();
      accountsManager.createExternalAccounts(data.internationalAccount, this.internationalAccountSuccess.bind(this, result), this.internationalAccountFailure.bind(this));
    }
  };
  /** Success call when creation of internal account is done
       * @param {object} result account data
       * @param {object} response response from the backend
       */
  TransferPresentationController.prototype.internalAccountSuccess = function (result, response) {
    this.hideProgressBar();
    result.internalAccount["referenceNo"] = response.Id; //Fetching the reference Id form the service 
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm(result, frmVerifyAccount);
  };
  /** Failure call when creation of internal account fails
   * @param {object} response response from the backend
   */
  TransferPresentationController.prototype.internalAccountFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddInternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverError": response.errorMessage }, frmAddInternalAccount);
  };
  /** Success call when creation of domestic account is done
   * @param {object} result account data
   * @param {object} response response from the backend
   */
  TransferPresentationController.prototype.domesticAccountSuccess = function (result, response) {
    this.hideProgressBar();
    result.domesticAccount["referenceNo"] = response.Id; //Fetching the reference Id form the service 
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm(result, frmVerifyAccount);
  };
  /** Failure call when creation of domestic account fails
   * @param {object} response response from the backend
   */
  TransferPresentationController.prototype.domesticAccountFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverDomesticError": response.errorMessage }, frmAddExternalAccount);
  };
  /** Success call when creation of international account is done
   * @param {object} result account data
   * @param {object} response response from the backend
   */
  TransferPresentationController.prototype.internationalAccountSuccess = function (result, response) {
    this.hideProgressBar();
    result.internationalAccount["referenceNo"] = response.Id; //Fetching the reference Id form the service 
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm(result, frmVerifyAccount);
  };
  /** failure call when creation of international account fails
   * @param {object} response response from backend
   */
  TransferPresentationController.prototype.internationalAccountFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverInternationalError": response.errorMessage }, frmAddExternalAccount);
  };
  /** Adds Another Account 
   */
  TransferPresentationController.prototype.addAnotherAccount = function (data) {
    if (data.internalAccount) {
      this.showSameBankAccounts();
    }
    if (data.domesticAccount) {
      this.showDomesticAccounts();
    }
    if (data.internationalAccount) {
      this.showInternationalAccounts();
    }

  };
  /** Verify Account By Credentials
     * @param  {object} data Data Of Verification Page 
     */
  TransferPresentationController.prototype.verifyCredentials = function (data, viewmodel) {
    var recipientManager = applicationManager.getRecipientsManager();
    this.showProgressBar();
    recipientManager.verifyExternalBankAccount(data, this.verifyCredentialsSuccess.bind(this, viewmodel), this.verifyCredentialsFailure.bind(this));
  };
  /** Verify Account By Credentials Success flow
   */
  TransferPresentationController.prototype.verifyCredentialsSuccess = function (viewmodel, response) {
    if (response.result === "Failed") {
      this.hideProgressBar();
      applicationManager.getNavigationManager().updateForm({ "invalidCredential": "true" }, frmVerifyAccount);
      return;
    }
    this.toAddVerifyAccount(viewmodel);
  };
  /** Verify Account By Credentials Failure flow
   */
  TransferPresentationController.prototype.verifyCredentialsFailure = function () {
    this.hideProgressBar();
    CommonUtilities.showServerDownScreen();
  };
  /** Add Account to verify state
     * @param  {object} viewModel Data Of Verification Page 
     */
  TransferPresentationController.prototype.addToVerifyAccount = function (viewModel, data) {
    var recipientManager = applicationManager.getRecipientsManager();
    if (data.domesticAccount) {
      data.domesticAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.domesticAccount["isInternationalAccount"] = "false";
      data.domesticAccount.isVerified = "false";
      this.showProgressBar();
      recipientManager.createABenificiary(data.domesticAccount, this.createDomesticAccountSuccess.bind(this, viewModel, data.domesticAccount.accountNumber), this.createDomesticAccountFailure.bind(this));
    }
    if (data.internationalAccount) {
      data.internationalAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.internationalAccount["isInternationalAccount"] = "true";
      data.internationalAccount.isVerified = "false";
      this.showProgressBar();
      recipientManager.createABenificiary(data.internationalAccount, this.createInternationalAccountSuccess.bind(this, viewModel, data.internationalAccount.accountNumber), this.createInternationalAccountFailure.bind(this));
    }
  };
  /** Make Trail Deposit for Domestic Account Success Flow
   */
  TransferPresentationController.prototype.makeTrailDepositDomesticSuccess = function () {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm({ "validateByTrialDeposit": "successfull" }, frmVerifyAccount);
  };
  /** Make Trail Deposit for Domestic Account Failure Flow
   */
  TransferPresentationController.prototype.makeTrailDepositDomesticFailure = function () {
    this.hideProgressBar();
  };
  /** Make Trail Deposit for International Account Success Flow
   */
  TransferPresentationController.prototype.makeTrailDepositInternationalSuccess = function () {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm({ "validateByTrialDeposit": "successfull" }, frmVerifyAccount);
  };
  /** Make Trail Deposit for International Account Failure Flow
   */
  TransferPresentationController.prototype.makeTrailDepositInternationalFailure = function () {
    this.hideProgressBar();
  };
  /** Create Domestic Account Success Flow
   * @param  {object} viewModel Data of Account
   * @param  {String} accountNumber Account Number
   */
  TransferPresentationController.prototype.createDomesticAccountSuccess = function (viewModel, accountNumber) {
    if (viewModel.ExternalAccountNumber) {
      var transactionManager = applicationManager.getTransactionManager();
      transactionManager.makeTrailDeposit(accountNumber, this.makeTrailDepositDomesticSuccess.bind(this), this.makeTrailDepositDomesticFailure.bind(this));
    }
  };
  /** Create Domestic Account Failure Flow
   * @param  {object} response Failure Response From Back end
   */
  TransferPresentationController.prototype.createDomesticAccountFailure = function (response) {
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverDomesticError": response.errorMessage }, frmVerifyAccount);
  };
  /** Create International Account Success Flow
   * @param  {object} viewModel Data Of Account 
   * @param  {String} accountNumber Account Number
   */
  TransferPresentationController.prototype.createInternationalAccountSuccess = function (viewModel, accountNumber) {
    if (viewModel.ExternalAccountNumber) {
      var transactionManager = applicationManager.getTransactionManager();
      transactionManager.makeTrailDeposit(accountNumber, this.makeTrailDepositInternationalSuccess.bind(this), this.makeTrailDepositInternationalFailure.bind(this));
    }
  };
  /** Create International Account Failure Flow
   * @param  {object} response Failure Response From Back end
   */
  TransferPresentationController.prototype.createInternationalAccountFailure = function (response) {
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverInternationalError": response.errorMessage }, frmAddExternalAccount);
  };
  /** Verify and Add External Account
  * @param {object} selectedRow Selected Row Data 
  */
  TransferPresentationController.prototype.confirmVerifyAndAdd = function (selectedRow) {
    var params = {
      "accountNumber": selectedRow.accountNumber,
      "firstDeposit": selectedRow.firstDeposit,
      "secondDeposit": selectedRow.secondDeposit
    };
    this.showProgressBar();
    var transactionManager = applicationManager.getTransactionManager();
    transactionManager.verifyTrailDeposit(params, this.verifyTrailDepositSuccess.bind(this, params), this.verifyTrailDepositFailure.bind(this));
  };
  /** Edit Account details on verifying Trail Deposit
      * @param  {object} params Data Deposit
      */
  TransferPresentationController.prototype.verifyTrailDepositSuccess = function (params) {
    params.isVerified = 1;
    var recipientManager = applicationManager.getRecipientsManager();
    recipientManager.editABenificiary(params, this.editAccountDetailsSucess.bind(this), this.editAccountDetailsFailure.bind(this));
  };
  /** Failure flow of verifing Trail Deposit
   * @param  {object} response Failure Response From Back end
   */
  TransferPresentationController.prototype.verifyTrailDepositFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmVerifyAccount);
    applicationManager.getNavigationManager().updateForm({ "errorVerifyAccount": response.errorMessage }, frmVerifyAccount);
  };
  /** Shows External Account On editing Account Details
   */
  TransferPresentationController.prototype.editAccountDetailsSucess = function () {
    this.fetchExternalAccounts();
  };
  /** Create International Account Failure Flow
   * @param  {object} response Failure Response From Back end
   */
  TransferPresentationController.prototype.editAccountDetailsFailure = function (response) {
    this.hideProgressBar();
    this.presentTransfers({ "serverError": response.errorMessage });
  };
  /** Add Accounts
   */
  TransferPresentationController.prototype.toAddVerifyAccount = function (data) {
    var recipientManager = applicationManager.getRecipientsManager();
    if (data.domesticAccount) {
      data.domesticAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.domesticAccount["isInternationalAccount"] = "false";
      data.domesticAccount.isVerified = "false";
      recipientManager.createABenificiary(data.domesticAccount, this.toAddVerifyDomesticAccountSuccess.bind(this), this.toAddVerifyDomesticAccountFailure.bind(this));
    }
    if (data.internationalAccount) {
      data.internationalAccount["isSameBankAccount"] = "false"; //Adding an additional field to identify an external account within same bank
      data.internationalAccount["isInternationalAccount"] = "true";
      data.internationalAccount.isVerified = "false";
      recipientManager.createABenificiary(data.internationalAccount, this.toAddVerifyInternationalAccountSuccess.bind(this), this.toAddVerifyInternationalAccountFailure.bind(this));
    }
  };
  /** Add Domestic Account Success Flow
     */
  TransferPresentationController.prototype.toAddVerifyDomesticAccountSuccess = function () {
    this.hideProgressBar();
    applicationManager.getNavigationManager().updateForm({ "validateByCredential": "successfull" }, frmVerifyAccount);
  };
  /** Add Domestic Account Failure Flow
   * @param {object} response Failure Response from backend
   */
  TransferPresentationController.prototype.toAddVerifyDomesticAccountFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverDomesticError": response.errorMessage }, frmAddExternalAccount);
  };
  /** Add International Account Success Flow
   */
  TransferPresentationController.prototype.toAddVerifyInternationalAccountSuccess = function () {
    this.hideProgressBar();
    applicationManager.getNavigationManager().updateForm({ "validateByCredential": "successfull" }, frmVerifyAccount);
  };
  /** Add International Account Failure Flow
   * @param {object} response Failure Response from backend
   */
  TransferPresentationController.prototype.toAddVerifyInternationalAccountFailure = function (response) {
    this.hideProgressBar();
    applicationManager.getNavigationManager().navigateTo(frmAddExternalAccount);
    applicationManager.getNavigationManager().updateForm({ "serverInternationalError": response.errorMessage }, frmAddExternalAccount);
  };
  TransferPresentationController.prototype.getSelectedExternalAccountDomestic = function(value) {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.resetValues();
    this.fetchSelectedExternalAccountsDomestic(value);
  };
  TransferPresentationController.prototype.fetchSelectedExternalAccountsDomestic = function (value) {
    var paginationManager = applicationManager.getPaginationManager();
    var params = paginationManager.getValues(recentConfig);
    params = {
      "IBAN": value,
      "offset": params.offset,
      "limit": params.limit,
      "sortBy": params.sortBy,
      "order": params.order
    };
    var accountManager = applicationManager.getAccountManager();
    this.showProgressBar();
    accountManager.fetchExternalAccounts(this.fetchExternalAccountsDomesticSuccess.bind(this, params), this.fetchExternalAccountsFailure.bind(this));
  };
  TransferPresentationController.prototype.fetchExternalAccountsDomesticSuccess = function (params, response) {
    var accounts = [];
    var count = 0;
    for (count = 0; count < response.length; count++) {
      if (response[count].IBAN === params.IBAN) {
        break;
      }
    }
    var temp = Math.floor(count / params.limit) * params.limit;
    for (var i = temp; i < temp + params.limit; i++) {
      if (response[i] !== undefined && response[i] !== null) {
        accounts.push(response[i]);
      }
    }
    var config = {
      "offset": temp,
      "limit": temp + params.limit,
      "index": (count % 10)
    };
    var data = {
      "accounts": accounts,
      "config": config
    };
    this.hideProgressBar();
    data.pagination = applicationManager.getPaginationManager().getValues(recentConfig);
    data.pagination.limit = response.length;
    this.presentTransfers({ "viewSelectedExternalAccount": data });
  };
  /** resets pagination manager and fetches external account
   * @param {object} value Account Number of the external Account
   */
  TransferPresentationController.prototype.getSelectedExternalAccount = function (value) {
    var paginationManager = applicationManager.getPaginationManager();
    paginationManager.resetValues();
    this.fetchSelectedExternalAccounts(value);
  };
  /** Fetches External Accounts
   * @param {object} value Account Number of the external Account
   */
  TransferPresentationController.prototype.fetchSelectedExternalAccounts = function (value) {
    var paginationManager = applicationManager.getPaginationManager();
    var params = paginationManager.getValues(recentConfig);
    params = {
      "accountNumber": value,
      "offset": params.offset,
      "limit": params.limit,
      "sortBy": params.sortBy,
      "order": params.order
    };
    var accountManager = applicationManager.getAccountManager();
    this.showProgressBar();
    accountManager.fetchExternalAccounts(this.fetchExternalAccountsSuccess.bind(this, params), this.fetchExternalAccountsFailure.bind(this));
  };
  /** Sets External Account Data
   * @param {object} params Config params to fetch Accounts
   * @param {object} response Failure Response from backend
   */
  TransferPresentationController.prototype.fetchExternalAccountsSuccess = function (params, response) {
    var accounts = [];
    var count = 0;
    for (count = 0; count < response.length; count++) {
      if (response[count].accountNumber === params.accountNumber) {
        break;
      }
    }
    var temp = Math.floor(count / params.limit) * params.limit;
    for (var i = temp; i < temp + params.limit; i++) {
      if (response[i] !== undefined && response[i] !== null) {
        accounts.push(response[i]);
      }
    }
    var config = {
      "offset": temp,
      "limit": temp + params.limit,
      "index": (count % 10)
    };
    var data = {
      "accounts": accounts,
      "config": config
    };
    this.hideProgressBar();
    data.pagination = applicationManager.getPaginationManager().getValues(recentConfig);
    data.pagination.limit = response.length;
    this.presentTransfers({ "viewSelectedExternalAccount": data });
  };
  /** When fetching of external Account fails
   */
  TransferPresentationController.prototype.fetchExternalAccountsFailure = function () {
    this.hideProgressBar();
  };
  /** Refresh user account from which transfer is made and shows Acknowledge
   * @param  {object} acknowledgeViewModel JSON containing transfer data and reference number
   */
  TransferPresentationController.prototype.fetchUserAccountAndNavigate = function (acknowledgeViewModel) {
    var accountManager = applicationManager.getAccountManager();
    accountManager.fetchInternalAccounts(this.fetchUserAccountAndNavigateSuccess.bind(this, acknowledgeViewModel), this.fetchUserAccountAndNavigatesFailure.bind(this));
  };

  TransferPresentationController.prototype.fetchUserAccountAndNavigatesFailure = function () {
    this.hideProgressBar();
    CommonUtilities.showServerDownScreen();
  }
  /** When fetching of external Account Succeeds
   * @param {object} acknowledgeViewModel data of transaction
   * @param {object} response response from backend
       */
  TransferPresentationController.prototype.fetchUserAccountAndNavigateSuccess = function (acknowledgeViewModel, response) {
    this.hideProgressBar();
    acknowledgeViewModel.transferData.accountFrom = response.filter(function (account) { return acknowledgeViewModel.transferData.accountFrom.accountID === account.accountID })[0];
    this.presentTransferAcknowledge({
      transferAcknowledge: acknowledgeViewModel
    });
  };
  /** When fetching of external Account fails
   */
  TransferPresentationController.prototype.fetchUserAccountAndNavigateFailure = function () {

  };
  /** Present frmAcknowledgement
   * @param {object} viewModel acknowledgement data
   */
  TransferPresentationController.prototype.presentTransferAcknowledge = function (viewModel) {
    applicationManager.getNavigationManager().navigateTo(frmAcknowledgement);
    applicationManager.getNavigationManager().updateForm(viewModel, frmAcknowledgement);
  }
  /** Present frmAcknowledgement
     * @param {object} account Account Data
     * @return {String} Weather account is internal or external
     */
  TransferPresentationController.prototype.getTransferType = function (account) {
    var accountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts")
    return account instanceof accountsModel ? "InternalTransfer" : "ExternalTransfer";
  }

  TransferPresentationController.prototype.showProgressBar = function () {
    applicationManager.getNavigationManager().updateForm({ isLoading: true });
  }
  TransferPresentationController.prototype.hideProgressBar = function () {
    applicationManager.getNavigationManager().updateForm({ isLoading: false });
  }


  /** Fetches External Accounts
* @param {object} data IBAN or reciepient name
*/
  TransferPresentationController.prototype.fetchExternalAccountsByIbanOrName = function (data) {
    if (data && data.searchKeyword.length > 0) {
      var records = applicationManager.getAccountManager().searchExternalAccounts(data.searchKeyword);
      this.fetchExternalAccountsByIbanOrNameSuccess(records);
    }
  };

  /**
   * used to handle the IBAN or Name Success schenario
   * @param {*} res response
   */
  TransferPresentationController.prototype.fetchExternalAccountsByIbanOrNameSuccess = function (res) {
    var view = {};
    view.externalAccountsByIbanorName = res;
    applicationManager.getNavigationManager().updateForm(view, frmTransfers);
  };

  /**
   * used to handle the error schenario
   * @param {*} res response
   */
  TransferPresentationController.prototype.fetchExternalAccountsByIbanOrNameFailure = function (res) {
    var errorExternalAccounts = "errorExternalAccounts";
    this.presentTransfers(errorExternalAccounts);
  };


  /** Fetches External Accounts
  * @param {object} data Account Number
  */
  TransferPresentationController.prototype.fetchExternalAccountsByAccountNumber = function (data) {
    if (data && data.searchKeyword.length > 0) {
      var records = applicationManager.getAccountManager().searchExternalInternationalAccounts(data.searchKeyword);
      this.fetchExternalAccountsByAccountNumberSuccess(records);
    }
  };

  /**
   * used to handle the IBAN or Name Success schenario
   * @param {*} res response
   */
  TransferPresentationController.prototype.fetchExternalAccountsByAccountNumberSuccess = function (res) {
    var view = {};
    view.interNationalExternalAccounts = res;
    applicationManager.getNavigationManager().updateForm(view, frmTransfers);
  };

  /**
   * used to handle the error schenario
   * @param {*} res response
   */
  TransferPresentationController.prototype.fetchExternalAccountsByAccountNumberFailure = function (res) {
    var errorExternalAccounts = "errorExternalAccounts";
    this.presentTransfers(errorExternalAccounts);
  };


  /**
   * used to get the swift code search string
   * @param {*} obj obj
   */
  TransferPresentationController.prototype.fetchExternalAccountsBySwiftCode = function (obj) {
    var params = {
      "swiftCode": obj.searchKeyword,
      "serviceName": "International+Account+to+Account+Fund+Transfer"
    }
    var accountsManager = applicationManager.getAccountManager();
    accountsManager.fetchBankDetails(params, this.fetchExternalAccountsBySwiftCodeSuccess.bind(this), this.fetchExternalAccountsBySwiftCodeFailure.bind(this))
  };

  /**
   * swift code serach success schenario
   * @param {object} res response
   */
  TransferPresentationController.prototype.fetchExternalAccountsBySwiftCodeSuccess = function(res)
  {
    var view = {};
    view.swiftCodeSuccess = true;
    view.bankName = res.bankName;
    applicationManager.getNavigationManager().updateForm(view, frmTransfers);
  };

  /** 
   * swift code serach failure schenario
   * @param {object} res response
  */
  TransferPresentationController.prototype.fetchExternalAccountsBySwiftCodeFailure = function()
  {
    var view = {};
    view.swiftCodeSuccess = true;
    view.bankName = "Kony Bank";
    applicationManager.getNavigationManager().updateForm(view, frmTransfers);
  };

  return TransferPresentationController;
});