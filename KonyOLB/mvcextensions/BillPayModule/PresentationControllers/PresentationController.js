

define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {

    /**
     * Description of BillPay Presentation Controller.
     * @class
     * @alias module:BillPayPresentationController
    */

    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;

    function BillPayPresentationController() {
        //History configuration
        this.historyConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };

        //Scheduled configuration
        this.scheduledConfig = {
            'sortBy': 'scheduledDate',
            'defaultSortBy': 'scheduledDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY
        };

        //Manage Payee Configuration
        this.managePayeeConfig = {
            'sortBy': 'payeeNickName',
            'defaultSortBy': 'payeeNickName',
            'order': OLBConstants.ASCENDING_KEY,
            'defaultOrder': OLBConstants.ASCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };

        //Payment Due configuration
        this.paymentDueConfig = {
            'sortBy': 'billDueDate',
            'defaultSortBy': 'billDueDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY
        };

        // All payees configuration
        this.allPayeesConfig = {
            'sortBy': 'payeeNickName',
            'defaultSortBy': 'payeeNickName',
            'order': OLBConstants.ASCENDING_KEY,
            'defaultOrder': OLBConstants.ASCENDING_KEY
        };

        MDABasePresenter.call(this);
    }

    inheritsFrom(BillPayPresentationController, MDABasePresenter);

    /** 
     * payee details information
    */
    var payeeDetails = {

        /**
         * initialization
         */
        init: function () {

            this.selectedBillerDetails = {
                billerId: null,
                billerName: null,
                billerCategoryName: null,
                billerAddress: null
            };
            //Biller Details if Biller not present
            this.billerId = null;
            this.billerName = null;//  PayeeName
            this.addressLine1 = null// (Street)
            this.addressLine2 = null;
            this.address = null;
            this.billerCategoryName = null;
            this.cityName = null;
            //Payee Details
            this.zipCode = null;
            this.accountNumber = null;
            this.policyNumber = null;
            this.mobileNumber = null;
            this.payeeNickName = null;
            this.nameOnBill = null;
            this.note = null;
           this.ackPayABill = "";
        },

        /**
         * used to update the selected Biller Details
         * @param {object} biller biller Details
         */
        updateSelectedBiller: function (biller) {
            this.selectedBillerDetails = biller;
        },

        /**
         * used to validate the biller details
         * @param {object} payeeInfo payee information
         * @returns {boolean} status
         */
        isBillerDetailValid: function (payeeInfo) {
            if (payeeInfo.isManualUpdate || (this.selectedBillerDetails.billerName != null
                && this.selectedBillerDetails.billerName.trim() !== "" && this.selectedBillerDetails.billerName == payeeInfo.billerName)) {
                return true;
            }
            return false;
        },
        /**
         * used to validate the input
         * @param {object} payeeInfo payee information
         * @returns {boolean} valid input status
         */
        isValidInput: function (payeeInfo) {
            if (!this.selectedBillerDetails || !this.selectedBillerDetails.billerId) {
                return "INVALID_BILLER";
            }
            if (this.selectedBillerDetails.billerName.trim() !== "" && this.selectedBillerDetails.billerName !== payeeInfo.billerName) {
                return "INVALID_BILLER"
            }
            if (payeeInfo.accountNumber !== payeeInfo.accountNumberAgain) {
                return this.billerCategoryName == 'Phone' ? "MISMATCH_RELATIONSHIPNUMBER" : "MISMATCH_ACCOUNTNUMBER";
            }
            return true;
        },
        /**
         * used to update the payee details
         * @param {object} payeeInfo payee information
         */
        updatePayeeDetails: function (payeeInfo) {
            if (payeeInfo.isManualUpdate) {
                this.selectedBillerDetails = null;
                this.billerId = null;
                this.billerName = payeeInfo.billerName;
                this.addressLine1 = payeeInfo.addressLine1;
                this.addressLine2 = payeeInfo.addressLine2;
                this.cityName = payeeInfo.cityName;
                this.billerAddress = this.addressLine1 + " , " + (this.addressLine2 ? (this.addressLine2+ " , ") :" ") + this.cityName + " , " + payeeInfo.zipCode;

                if (payeeInfo.noAccountNumber) {
                    this.note = payeeInfo.note;
                    this.accountNumber = "";
                } else {
                    this.note = null;
                    this.accountNumber = payeeInfo.accountNumber;
                }
                this.mobileNumber = null;

            } else {
                //update selected biller
                this.billerId = this.selectedBillerDetails.billerId;
                this.billerName = this.selectedBillerDetails.billerName;
                this.billerAddress = this.selectedBillerDetails.billerAddress;
                this.billerCategoryName = this.selectedBillerDetails.billerCategoryName;
                this.addressLine1 = null;
                this.addressLine2 = null;
                this.cityName = null;
                //update account number
                if (this.billerCategoryName == 'Credit Card' || (this.billerCategoryName == 'Utilities')) {
                    this.accountNumber = payeeInfo.accountNumber;
                    this.mobileNumber = null;
                } else if (this.billerCategoryName == 'Phone') {
                    this.accountNumber = payeeInfo.relationShipNumber || payeeInfo.accountNumber;
                    this.mobileNumber = payeeInfo.mobileNumber;
                } else if (this.billerCategoryName == 'Insurence') {
                    this.accountNumber = payeeInfo.accountNumber;
                    this.mobileNumber = payeeInfo.policyNumber || payeeInfo.mobileNumber;
                }
            }
            this.nameOnBill = applicationManager.getUserPreferencesManager().getUserObj().userfirstname
                + " " + applicationManager.getUserPreferencesManager().getUserObj().userlastname;
            this.payeeNickName = this.billerName;
            this.zipCode = payeeInfo.zipCode;
        },
        /**
         * update the transaction id
         * @param {object} transaction transaction id
         */
        updateTransationId: function (transaction) {
            this.transactionId = transaction.transactionId;
        },
        /**
         * used to update the payee.
         * @param {object} payee payee information
         */
        updatePayeeId: function (payee) {
            this.payeeId = payee.payeeId;
        },
        /**
         * used to update the confirmation payee details
         * @param {object} payeeInfo payee information
         */
        updateConfirmPayeeDetails: function (payeeInfo) {
            // If the user input empty retain the default value.
            if (payeeInfo.payeeNickName.trim() !== "") {
                this.payeeNickName = payeeInfo.payeeNickName;
            }
            if (payeeInfo.nameOnBill.trim() !== "") {
                this.nameOnBill = payeeInfo.nameOnBill;
            }
        },
        /**
         * used to get the transaction id
         * @returns {string} transactionId
         */
        getTransationId: function () {
            return this.transactionId;
        },
        /**
         * used to get the one time payement information.
         * @returns {object} one time object
         */
        getOneTimePayeeInfo: function () {
            return {
                billerId: this.billerId,
                billerCategoryName: this.billerCategoryName,
                zipCode: this.zipCode,
                mobileNumber: this.mobileNumber,
                billerName: this.billerName,
                billerAddress: this.billerAddress,
                accountNumber: this.accountNumber,
            };
        },
        /**
         * used to get the request
         * @returns {object} one time object
         */
        getRequestObject: function () {
            return {
                accountNumber: this.accountNumber,
                street: this.addressLine1,
                addressLine2: this.addressLine2,
                cityName: this.cityName,
                payeeNickName: this.payeeNickName,
                zipCode: this.zipCode,
                companyName: this.billerName,
                notes: this.note,
                nameOnBill: this.nameOnBill,
                billerId: this.billerId,
                phone: this.mobileNumber
            };
        },
        /**
         * used to get the payee details updated biller information
         * @returns {object} biller details information
         */
        getPayeeDetailsToUpdatePage: function () {
            return {
                billerName: this.billerName,
                billerAddress: this.billerAddress,
                accountNumber: this.accountNumber,
                payeeNickName: this.payeeNickName,
                nameOnBill: this.nameOnBill
            };
        },
        /**
         * used to get the payee details confirmation screen
         * @returns {object} confirmation information
         */
        getPayeeDetailsConfirmPage: function () {
            return {
                billerName: this.billerName,
                billerAddress: this.billerAddress,
                accountNumber: this.accountNumber,
                payeeNickName: this.payeeNickName,
                nameOnBill: this.nameOnBill
            };
        },
        /**
         * used to get the payee details success screen
         * @returns {object} success object
         */
        getPayeeDetailsSuccessPage: function () {
            return {
                billerName: this.billerName,
                billerAddress: this.billerAddress,
                accountNumber: this.accountNumber,
                payeeNickName: this.payeeNickName,
                nameOnBill: this.nameOnBill
            };
        },
        /**
         * used to get the payee details payement screen
         * @returns {object} payement object
         */
        getPayeeDetailsForPayment: function () {
            return {
                billerName: this.billerName,
                billerAddress: this.billerAddress,
                accountNumber: this.accountNumber,
                payeeNickname: this.payeeNickName,
                nameOnBill: this.nameOnBill,
                payeeId: this.payeeId,
                show: "PayABill"
            };
        }
    };

    //Used to store the last search values
    var lastSearchValue;

    /**Entry Point method for BillPayModule
     * @param {Object} context  - used to load a particular module in billPay
     * @param {Object} sender  - used to know the sender, where i get the requested.
     * @param {string} [context.History] loads the history flow in  billPay.
     * @param {string} [context.PayABill] loads the single billPay screen.
     * @param {string} [context.AllPayees] - loads the AllPayees flow in billPay.
     * @param {string} [context.ScheduleBills] - loads the scheule Bills flow in billPay.
     * @param {string} [context.DueBills] - loads the scheule Bills flow in billPay.
     * @param {string} [context.ManagePayees] - loads the managePayees flow in billPay.
     * @param {boolean} loadBills -- used to handle the hamburger menu loading
     * @param {object} data -- data
     */

    BillPayPresentationController.prototype.showBillPayData = function (sender, context, loadBills, data) {
        var scopeObj = this;
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var billPayEligibility = userPreferencesManager.checkBillPayEligibilityForUser();
        if (billPayEligibility === "Activated") {
            applicationManager.getAccountManager().fetchInternalAccounts(function () { }, function () { });
            scopeObj.showView("frmBillPay", { "resetBillPayForm": true });
            scopeObj.showProgressBar("frmBillPay");
            if (context && (context === "History" || context === "PayABill" || context === "AllPayees" || context === "ManagePayees" || context === "showOneTimePayment" || context === "PayABillWithContext")) {
                scopeObj.intialBillPayView(context);
            }
            switch (context) {
                case "ManagePayees":
                    if (loadBills) {
                        scopeObj.loadManagePayeesWithBills();
                    } else {
                        scopeObj.showManagePayees();
                    }
                    break;
                case "AllPayees":
                    scopeObj.showAllPayees();
                    break;
                case "History":
                    if (loadBills) {
                        scopeObj.loadHistoryWithBills();
                    } else {
                        scopeObj.showBillPayHistory();
                    }
                    break;
                case "ScheduleBills":
                    scopeObj.fetchScheduledBills();
                    break;
                case "DueBills":
                    scopeObj.getPayementBills({}, { "dueBills": false });
                    break;
                case "PayABill":
                    scopeObj.getSingleBillPay(data, sender);
                    break;
                case "AddPayee":
                    scopeObj.navigateToAddPayee();
                    break;
                case "showOneTimePayment":
                    scopeObj.showOneTimePaymentPage(sender, data);
                    break;
                case "PayABillWithContext":
                    scopeObj.loadPayABillWithPayees(data, sender);
                    break;
                case "MakeOneTimePayment":
                    scopeObj.navigateToOneTimePayement();
                    break;
                default:
                    scopeObj.loadPayeesWithBills();
            }
        }
        else {
            if (billPayEligibility === "NotActivated") {
                scopeObj.showProgressBar("frmBillPay");
                scopeObj.showBillPayDeactivatedView(scopeObj.getBillPaySupportedAccounts());
            } else {
                applicationManager.getNavigationManager().navigateTo("frmBillPay");
                scopeObj.showBillPayNotEligibleView();
            }
        }
    };

    /** 
     * used to load the manage payees with bills
    */
    BillPayPresentationController.prototype.loadManagePayeesWithBills = function () {
        var scopeObj = this;
        var asyncManager = applicationManager.getAsyncManager();
        var recipientsManager = applicationManager.getRecipientsManager();
        var billManager = applicationManager.getBillManager();
        applicationManager.getPaginationManager().resetValues();
        var billParams = [];
        var manageParams = [];
        billParams.push(applicationManager.getPaginationManager().getValues(scopeObj.paymentDueConfig));
        var params = applicationManager.getPaginationManager().getValues(this.managePayeeConfig);
        var criteria;
        criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("limit", params.limit),
            kony.mvc.Expression.eq("offset", params.offset),
            kony.mvc.Expression.eq("sortBy", params.sortBy),
            kony.mvc.Expression.eq("order", params.order)
        )
        manageParams.push(criteria);
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(recipientsManager, 'fetchPayeesList', manageParams),
                asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', billParams)
            ],
            scopeObj.loadManagePayeeWithBillCompletionCallBack.bind(scopeObj)
        );
    }
    /** 
     * used  to load the manage payee with bill success schenario
       * @param {Object} syncResponseObject response
    */
    BillPayPresentationController.prototype.loadManagePayeeWithBillCompletionCallBack = function (syncResponseObject) {
        var scopeObj = this;
        if (syncResponseObject.isAllSuccess()) {
            scopeObj.fetchManagePayeesSuccessCallBack(syncResponseObject.responses[0].data);
            scopeObj.fetchDueBillsSuccessCallBack({ "dueBills": true }, syncResponseObject.responses[1].data);
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }
    }
    /** 
     * used to load the  history and bills
    */
    BillPayPresentationController.prototype.loadHistoryWithBills = function () {
        var scopeObj = this;
        var asyncManager = applicationManager.getAsyncManager();
        var transactionManager = applicationManager.getTransactionManager();
        var billManager = applicationManager.getBillManager();
        applicationManager.getPaginationManager().resetValues();
        var billParams = [];
        billParams.push(applicationManager.getPaginationManager().getValues(scopeObj.paymentDueConfig));
        var params = applicationManager.getPaginationManager().getValues(scopeObj.historyConfig);
        var historyParams = [];
        historyParams.push(params);
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(transactionManager, 'fetchUserBillPayPostedTransactions', historyParams),
                asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', billParams)
            ],
            scopeObj.loadHistoryWithBillCompletionCallBack.bind(scopeObj)
        );
    };

    /**
     * used to fetch Both allPayees and BillS success scheanrio 
     * @param {Object} syncResponseObject response
    */
    BillPayPresentationController.prototype.loadHistoryWithBillCompletionCallBack = function (syncResponseObject) {
        var scopeObj = this;
        if (syncResponseObject.isAllSuccess()) {
            scopeObj.fetchUserbillPayHistorySuccessCallback(syncResponseObject.responses[0].data);
            scopeObj.fetchDueBillsSuccessCallBack({ "dueBills": true }, syncResponseObject.responses[1].data);
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }
    }
    /** 
     * used to fetch Both allPayees and BillS
    */
    BillPayPresentationController.prototype.loadPayeesWithBills = function () {
        var scopeObj = this;
        var asyncManager = applicationManager.getAsyncManager();
        var recipientsManager = applicationManager.getRecipientsManager();
        var billManager = applicationManager.getBillManager();
        var params = applicationManager.getPaginationManager().getValues(scopeObj.allPayeesConfig);
        var criteria;
        criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("sortBy", params.sortBy),
            kony.mvc.Expression.eq("order", params.order)
        );
        var payeeParams = [];
        payeeParams.push(criteria);
        var billParams = [];
        billParams.push(applicationManager.getPaginationManager().getValues(this.paymentDueConfig));
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(recipientsManager, 'fetchPayeesList', payeeParams),
                asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', billParams)
            ],
            this.loadPayABillCompletionCallBack.bind(this)
        );
    };

    /**
     * used to fetch Both allPayees and BillS success scheanrio 
     * @param {Object} syncResponseObject response
    */
    BillPayPresentationController.prototype.loadPayABillCompletionCallBack = function (syncResponseObject) {
        var scopeObj = this;
        if (syncResponseObject.isAllSuccess()) {
            scopeObj.fetchAllPayeesSuccessCallBack(syncResponseObject.responses[0].data);
            scopeObj.fetchDueBillsSuccessCallBack({ "dueBills": true }, syncResponseObject.responses[1].data);
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }
    };

    /** 
     * used to load the all payees
     * @param {object} data data
     * @param {object} sender sender
    */
    BillPayPresentationController.prototype.loadPayABillWithPayees = function (data, sender) {
        var asyncManager = applicationManager.getAsyncManager();
        var recipientsManager = applicationManager.getRecipientsManager();
        var billManager = applicationManager.getBillManager();
        var params = [];
        params.push(applicationManager.getPaginationManager().getValues(this.paymentDueConfig));
        var criteria = kony.mvc.Expression.eq("searchString", "");
        var payeeParams = [];
        payeeParams.push(criteria);
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(recipientsManager, 'fetchPayeesList', payeeParams),
                asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', params)
            ],
            this.loadPayABillWithPayeesCompletionCallBack.bind(this, data, sender)
        );
    };

    /**
     * Method used as completion call back for the async call to bills and payees.
     * @param {object} sender sender name
     * @param {Object} data - contains the addNewTravelPlan property.
     * @param {Object} syncResponseObject - contains the async manager respnose.
     */
    BillPayPresentationController.prototype.loadPayABillWithPayeesCompletionCallBack = function (data, sender, syncResponseObject) {
        var scopeObj = this;
        if (syncResponseObject.isAllSuccess()) {
            applicationManager.getNavigationManager().updateForm({ payees: syncResponseObject.responses[0].data, sender: sender }, "frmBillPay")
            data.billid = syncResponseObject.responses[0].data[0].billid;
            data.billDueDate = syncResponseObject.responses[0].data[0].billDueDate;
            data.dueAmount = syncResponseObject.responses[0].data[0].dueAmount;
            data.eBillSupport = syncResponseObject.responses[0].data[0].eBillSupport;
            data.eBillStatus = syncResponseObject.responses[0].data[0].eBillStatus;
            data.billGeneratedDate = syncResponseObject.responses[0].data[0].billGeneratedDate;
            data.ebillURL = syncResponseObject.responses[0].data[0].ebillURL;
            scopeObj.getSingleBillPay(data, "quickAction");
            scopeObj.fetchDueBillsSuccessCallBack({ "dueBills": true }, syncResponseObject.responses[1].data);
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }
    }

    /**Method for used to display the BillPay Not Eligibility View.
     * 
    */
    BillPayPresentationController.prototype.showBillPayNotEligibleView = function () {
        applicationManager.getNavigationManager().updateForm({ "showNotEligibleView": "showNotEligibleView" }, "frmBillPay");
    };

    /**
     * used to show the billPay Page and executes the particular Page.
     * @param {string} frm  used to load the form 
     * @param {object}  data  used to load the particular form and having key value pair.
     */
    BillPayPresentationController.prototype.showView = function (frm, data) {
        applicationManager.getNavigationManager().navigateTo(frm);
        applicationManager.getNavigationManager().updateForm(data, frm);
    };/**
  * @module BillPayPresentationController
*/

    /**
     * used to show the billPay Deactivation Page.
     * @param {billPayAcccounts} billPayAcccounts list of Accounts supported by billPay
     */
    BillPayPresentationController.prototype.showBillPayDeactivatedView = function (billPayAcccounts) {
        var self = this;
        self.showView("frmBillPay", { "showDeactivatedView": "showDeactivatedView", "billPayAcccounts": billPayAcccounts });
    };

    /**
    * used to show the loading indicator
    * @param {string} frm form Name
    */

    BillPayPresentationController.prototype.showProgressBar = function (frm) {
        applicationManager.getNavigationManager().updateForm({
            "ProgressBar": {
                show: true
            }}, frm);
    };

    /**
     * hideProgressBar - This method is used to hide ProgressBar
     * @param {string} frm form Name
     */
    BillPayPresentationController.prototype.hideProgressBar = function (frm) {
        applicationManager.getNavigationManager().updateForm({
            "ProgressBar": {
                show: false
            }}, frm);
    };

    /** 
        * used to activte the BillPay and set default billPay Account Number
        * @param {string}  accountNumber Selected BillPay Account Number
       */
    BillPayPresentationController.prototype.activateBillPay = function (accountNumber) {
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var param = {
            "userName": userPreferencesManager.getUserObj().userName
        }
        userPreferencesManager.activateBillPay(param, this.activateBillPaySuccess.bind(this, accountNumber), this.activateBillPayFailure.bind(this));
    };

    /**
    * handels the billPay  Activation Success scenario
    *  @param {accountNumber} string Selected BillPay Account Number
    */

    BillPayPresentationController.prototype.activateBillPaySuccess = function (accountNumber) {
        if (applicationManager.getConfigurationManager().defaultBillPayAccountSelection === 'true') {
            applicationManager.getNavigationManager().updateForm({ "showBillPayActivationAck": true }, "frmBillPay")
        } else {
            var userPreferencesManager = applicationManager.getUserPreferencesManager();
            var param = {
                "userName": userPreferencesManager.getUserObj().userName,
                "default_account_billPay": accountNumber
            }
            userPreferencesManager.updateBillPayPreferedAccountNumber(param, this.updateBillPayPreferedAccountNumberSuccessCallBack.bind(this), this.updateBillPayPreferedAccountNumberFailureCallBack.bind(this));
        }
    }

    /**
      * handels the billPay  Activation Error scenario
      * @param {object} response error response
    */

    BillPayPresentationController.prototype.activateBillPayFailure = function (response) {
        var self = this;
        if(response.errorMessage){
            self.showServerError(response.errorMessage);
        }
        else{
            self.showServerError();
        }
    }

    /**
     * billPay default Account Number Success Scehnario. 
     * @param {param}  param used to send userName with Default BillPay Account Number  
     */
    BillPayPresentationController.prototype.updateBillPayPreferedAccountNumberSuccessCallBack = function (param) {
        applicationManager.getNavigationManager().updateForm({ "showBillPayActivationAck": true }, "frmBillPay")

    };

    /**
     * used to handle the billPay error schenario. 
    */
    BillPayPresentationController.prototype.updateBillPayPreferedAccountNumberFailureCallBack = function (response) {
        var self = this;
        if(response.errorMessage){
            self.showServerError(response.errorMessage);
        }
        else{
            self.showServerError();
        }
    };

    /**
     * used to update the billPayPrefered Account number 
     * @param {string} accountNumber account number
    */
    BillPayPresentationController.prototype.updateBillPayPreferedAccount = function (accountNumber) {
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var param = {
            "userName": userPreferencesManager.getUserObj().userName,
            "default_account_billPay": accountNumber
        }
        userPreferencesManager.updateBillPayPreferedAccountNumber(param, function () { }, function () { });
    };


    /**
     * All Payees Flow 
     * @param {obejct} sortingInputs sorting inputs
     */
    BillPayPresentationController.prototype.showAllPayees = function (sortingInputs) {
        var scopeObj = this;
        scopeObj.showProgressBar("frmBillPay");
        applicationManager.getPaginationManager().resetValues();
        var params = applicationManager.getPaginationManager().getValues(scopeObj.allPayeesConfig, sortingInputs);
        var criteria;
        criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("sortBy", params.sortBy),
            kony.mvc.Expression.eq("order", params.order)
        )
        applicationManager.getRecipientsManager().fetchPayeesList(criteria, this.fetchAllPayeesSuccessCallBack.bind(this), this.fetchAllPayeesErrorCallBack.bind(this));
    }

    /** 
     * used to fetch categoris and payees
     * @param {object} syncResponseObject syncResponseObject
    */
    BillPayPresentationController.prototype.fetchAllPayeesWithBillSuccessCallBack = function(syncResponseObject)
    {
        var self = this;
        var sortingValues = applicationManager.getPaginationManager().getValues(self.allPayeesConfig);  
        if (syncResponseObject.isAllSuccess()) {

        applicationManager.getNavigationManager().updateForm({
            "allPayees": syncResponseObject.responses[0].data, "sortingInputs": sortingValues,
            "billPayAccounts": self.getBillPaySupportedAccounts()
        }, "frmBillPay");
        }else{
            self.showServerError(true); 
        }
    };

    /**
     * get All Payees success schenario
     * @param {object} response all payees response
     */
    BillPayPresentationController.prototype.fetchAllPayeesSuccessCallBack = function (response) {
        var self = this;
        var sortingValues = applicationManager.getPaginationManager().getValues(self.allPayeesConfig);
        applicationManager.getNavigationManager().updateForm({
            "allPayees": response, "sortingInputs": sortingValues,
            "billPayAccounts": self.getBillPaySupportedAccounts()
        }, "frmBillPay");
    }

    /** 
     * get All Payees error schenario. 
     * @param {object} res response
    */
    BillPayPresentationController.prototype.fetchAllPayeesErrorCallBack = function (res) {
        var scopeObj = this;
        scopeObj.showServerError(res.errorMessage);
    }

    /**
     * used to show the billPay due flow.
     * @param {object} dataInputs  sorting parameters
     * @param {boolean} loadBills status of bills loaded or not
     */
    BillPayPresentationController.prototype.getPayementBills = function (dataInputs, loadBills) {
        var scopeObj = this;
        var params = applicationManager.getPaginationManager().getValues(scopeObj.paymentDueConfig, dataInputs);
        applicationManager.getBillManager().fetchUserBillPayPendingTransactions(params, this.fetchDueBillsSuccessCallBack.bind(this, loadBills), this.fetchDueBillsErrorCallBack.bind(this));
    };

    /**
     * handels the dueBills count and total DueBills Amount and dueBills List.
     * @param {boolean} loadBills status of bills loaded or not
     * @param {object} response list of due Bills
     */
    BillPayPresentationController.prototype.fetchDueBillsSuccessCallBack = function (loadBills, response) {
        var scopeObj = this;
        var viewProperties = {};
        var billDueData = {
            count: response.length,
            totalDueAmount: 0
        };
        var paginationManager = applicationManager.getPaginationManager();
        viewProperties.noOfRecords = paginationManager.getValues(this.paymentDueConfig);

        if (loadBills.dueBills) {
            billDueData.dueBills = response.map(function (dataItem) {
                billDueData.totalDueAmount += parseFloat(dataItem.dueAmount);
            });
            applicationManager.getNavigationManager().updateForm({ "billDueData": billDueData }, "frmBillPay");

        } else {
            applicationManager.getNavigationManager().updateForm({ "paymentDueBills": response, "noOfRecords": viewProperties.noOfRecords, "billPayAccounts": scopeObj.getBillPaySupportedAccounts() }, "frmBillPay");
        }
    }

    /**
     * used to hadle the Due Bills Error Schenario
     * @param {object} res error object 
     */
    BillPayPresentationController.prototype.fetchDueBillsErrorCallBack = function (res) {
        var scopeObj = this;
        scopeObj.showServerError(res.errorMessage);
    }

    /**
     *  used to get scheduled bills for user.
     *  @param {object} sortingInputs sorting inputs
    */
    BillPayPresentationController.prototype.fetchScheduledBills = function (sortingInputs) {
        var scopeObj = this;
        var params = applicationManager.getPaginationManager().getValues(scopeObj.scheduledConfig, sortingInputs);
        applicationManager.getTransactionManager().fetchUserBillPayScheduledTransactions(params, this.fetchScheduledBillsSuccessCallback.bind(this), this.fetchScheduledBillsErrorCallback.bind(this));
    };

    /**
     * scheduleBillPay success schenario 
     * @param {object} response list of schedudle bills
    */
    BillPayPresentationController.prototype.fetchScheduledBillsSuccessCallback = function (response) {
        var scopeObj = this;
        var sortingInputs = applicationManager.getPaginationManager().getValues(scopeObj.scheduledConfig);
        applicationManager.getNavigationManager().updateForm({ 'scheduledBills': response, "sortingInputs": sortingInputs }, "frmBillPay");
    };


    /**
     * scheduleBillPay Error Schenario
     * @param {object} res error object
    * */
    BillPayPresentationController.prototype.fetchScheduledBillsErrorCallback = function (res) {
        var scopeObj = this;
        scopeObj.showServerError(res.errorMessage);
    };

    /**
     * shows the error message with error response.
     * @param {object} data used to show the error message
     */
    BillPayPresentationController.prototype.showServerError = function (data) {
        applicationManager.getNavigationManager().updateForm({ "serverError": data }, "frmBillPay")
    };

    /**
     * used to set Initial BillPay Page
     * @param {object} context intial view of bill Pay
     */
    BillPayPresentationController.prototype.intialBillPayView = function (context) {
        applicationManager.getNavigationManager().updateForm({
            "intialView": context
        }, "frmBillPay")
    };

    /**
     * used to fetch BillPay History 
     */
    BillPayPresentationController.prototype.showBillPayHistory = function () {
        applicationManager.getPaginationManager().resetValues();
        this.fetchUserBillPayHistory();

    };

    /**
     * used to show the billPay History page
     * @param {object} response getting billpay history with pagination
     */
    BillPayPresentationController.prototype.fetchUserbillPayHistorySuccessCallback = function (response) {
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            paginationManager.updatePaginationValues();
            viewProperties.noOfRecords = paginationManager.getValues(this.historyConfig);
            viewProperties.billpayHistory = response;
        }
        else {
            var values = paginationManager.getValues();
            if (values.offset === 0) {
                viewProperties.noHistory = true;
            }
            else {
                viewProperties.noMoreRecords = true;
                this.hideProgressBar("frmBillPay");
            }
        }
        applicationManager.getNavigationManager().updateForm({ "billpayHistory": viewProperties }, "frmBillPay");
    };

    /**
     * used to show the error message
     * @param {object} res error object
     */
    BillPayPresentationController.prototype.fetchUserbillPayHistoryErrorCallback = function (res) {
        var self = this;
        self.showServerError(res.errorMessage);
    }
    /**
     * used to show Manage Payees
     * @param {string} offSetVal used to se offSet Value    
     */
    BillPayPresentationController.prototype.showManagePayees = function () {
        var scopeObj = this;
        scopeObj.showProgressBar("frmBillPay");
        applicationManager.getPaginationManager().resetValues();
        scopeObj.managePayeePagination();
    }

    /** 
     * used to perform the pagination
     * @param {object} sortingInputs sorting input values
    */
    BillPayPresentationController.prototype.managePayeePagination = function (sortingInputs) {
        var params = applicationManager.getPaginationManager().getValues(this.managePayeeConfig, sortingInputs);
        var criteria;
        criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("limit", params.limit),
            kony.mvc.Expression.eq("offset", params.offset),
            kony.mvc.Expression.eq("sortBy", params.sortBy),
            kony.mvc.Expression.eq("order", params.order)
        )
        applicationManager.getRecipientsManager().fetchPayeesList(criteria, this.fetchManagePayeesSuccessCallBack.bind(this), this.fetchManagePayeesErrorCallBack.bind(this));
    };

    /** 
     * used to show managePayee flow.
     * @param {object} response list of payees
     * 
    */
    BillPayPresentationController.prototype.fetchManagePayeesSuccessCallBack = function (response) {
        var viewProperties = {};
        var paginationManager = applicationManager.getPaginationManager();
        if (response.length > 0) {
            paginationManager.updatePaginationValues();
            viewProperties.noOfRecords = paginationManager.getValues(this.managePayeeConfig);
            viewProperties.managePayee = response;
        }
        else {
            var values = paginationManager.getValues();
            if (values.offset === 0) {
                viewProperties.noHistory = true;
            }
            else {
                viewProperties.noMoreRecords = true;
                this.hideProgressBar("frmBillPay");
            }
        }
        applicationManager.getNavigationManager().updateForm({ "managePayee": viewProperties }, "frmBillPay");
    }

    /**
     * used to show manage payees error schenario
     * @param {object} res error object
     */
    BillPayPresentationController.prototype.fetchManagePayeesErrorCallBack = function (res) {
        var self = this;
        self.showServerError(res.errorMessage);
    };

    /** 
     * used to fetch the history of past bills
     * @param {object} sortingInputs sorting inputs
    */
    BillPayPresentationController.prototype.fetchUserBillPayHistory = function (sortingInputs) {
        var self = this;
        self.showProgressBar("frmBillPay");
        var params = applicationManager.getPaginationManager().getValues(this.historyConfig, sortingInputs);
        applicationManager.getTransactionManager().fetchUserBillPayPostedTransactions(params, this.fetchUserbillPayHistorySuccessCallback.bind(this), this.fetchUserbillPayHistoryErrorCallback.bind(this));
    };

    /** 
     * used to fetch the history of next past bills
     * pagination next button on-Click
    */
    BillPayPresentationController.prototype.fetchNextUserBillPayHistory = function () {
        applicationManager.getPaginationManager().getNextPage();
        this.fetchUserBillPayHistory();
    };

    /**
     * used to fetch the history of previous past bills
     * pagination previous button on-click
     */

    BillPayPresentationController.prototype.fetchPreviousUserBillPayHistory = function () {
        applicationManager.getPaginationManager().getPreviousPage();
        this.fetchUserBillPayHistory();
    };

    /** 
   * used to fetch the payees of next
   * pagination next button on-Click
  */
    BillPayPresentationController.prototype.fetchNextManagePayees = function () {
        applicationManager.getPaginationManager().getNextPage();
        this.managePayeePagination();
    };

    /**
     * used to fetch the payees of previous 
     * pagination previous button on-click
     */

    BillPayPresentationController.prototype.fetchPreviousManagePayees = function () {
        applicationManager.getPaginationManager().getPreviousPage();
        this.managePayeePagination();
    };

    /**
     * used to navigate the Bulk BillPay Confirmation Screen
     * @param {BulkPayRecords}  BulkPayRecords list of records
    */
    BillPayPresentationController.prototype.setDataForBulkBillPayConfirm = function (BulkPayRecords) {
        applicationManager.getNavigationManager().navigateTo("frmConfirm");
        applicationManager.getNavigationManager().updateForm({ "bulkPayRecords": BulkPayRecords }, "frmConfirm");
    };

    /**
     * to Validate minimum and maximum limit for the amount
     * @param {object}  amount- amount
     * @returns {object} result result - object contains isAmountValid and errMsg
   */

    BillPayPresentationController.prototype.validateBillPayAmount = function (amount) {
        var minBillPayLimit = parseFloat(applicationManager.getConfigurationManager().minBillPayLimit);
        var maxBillPayLimit = parseFloat(applicationManager.getConfigurationManager().maxBillPayLimit);
        var result = {
            isAmountValid: false
        };
        if (amount < minBillPayLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(minBillPayLimit);
        } else if (amount > maxBillPayLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(maxBillPayLimit);
        } else {
            result.isAmountValid = true;
        }
        return result;
    };

    /** 
     * used to get the default billPay Account Number
     * @returns {accountNumber} accountNumber for BillPay
    */
    BillPayPresentationController.prototype.getBillPayPreferedAccountNumber = function () {
        return applicationManager.getUserPreferencesManager().getDefaultAccountforBillPay();
    };

    /**
     * used to create the bulkPayment transaction
     * @param {list} bulkPayRecords list of transactions.
     */
    BillPayPresentationController.prototype.createBulkPayments = function (bulkPayRecords) {
        var transactions = [];
        var mfaManager = applicationManager.getMFAManager();
        var self = this;
        self.showProgressBar("frmConfirm");
         var displayName = "BillPay";
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
              serviceName: mfaManager.getServiceId(),
        };
        var formatUtil = applicationManager.getFormatUtilManager();
        for (var index in bulkPayRecords) {
            var record = bulkPayRecords[index];
            transactions.push({
                'accountNumber': record.accountNumber,
                'scheduledDate': formatUtil.convertToUTC(record.lblSendOn),
                'billid': record.billid,
                'payeeId': record.payeeId,
                'paidAmount': applicationManager.getFormatUtilManager().deFormatAmount(record.lblAmount.replace(applicationManager.getConfigurationManager().getCurrencyCode(), "")),
                'deliverBy': formatUtil.convertToUTC(record.lblDeliverBy),
                'transactionsNotes' : record.transactionsNotes
            });
        }
        var transactionsList = JSON.stringify(transactions);
        transactionsList.replace(/"/g, "'");
        var params = {
            'bulkPayString' : transactionsList,
            'MFAAttributes' : mfaParams
        };
        applicationManager.getTransactionManager().createBulkBillPayPayement(params, this.createBulkPaymentsSuccessCallBack.bind(this), this.createBulkPaymentsErrorCallBack.bind(this));
    };

    /** 
     * used to handle the bulk payement success schenario
     * @param {list} response -- list of response records
     * 
    */
    BillPayPresentationController.prototype.createBulkPaymentsSuccessCallBack = function (response) {
       var mfaManager = applicationManager.getMFAManager();
        if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "BULK_BILL_PAY",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
           
        } else {
           applicationManager.getNavigationManager().navigateTo("frmAcknowledgement");
            applicationManager.getNavigationManager().updateForm({
                "bulkPay": response
            }, "frmAcknowledgement");
        }
       
    };

    /**
     *used to handle the bulk payement error schenario.
     * @param {object} response bulk bill pay response
     */
    BillPayPresentationController.prototype.createBulkPaymentsErrorCallBack = function (response) {
        var self = this;
        response.bulkBillPayement = true;
        self.showProgressBar("frmBillPay");
        applicationManager.getNavigationManager().navigateTo("frmBillPay");
        self.showServerError(response);
    };

    /** 
     * used to cancel the bulkBillPay
     */
    BillPayPresentationController.prototype.cancelBulkPay = function () {
        this.showBillPayData();
    };

    /** 
    * used to modify the bulk billPay screen 
    */
     BillPayPresentationController.prototype.modifyPayement = function () {
        applicationManager.getNavigationManager().navigateTo("frmBillPay");
        applicationManager.getNavigationManager().updateForm({
            modifyPayment: true
        }, "frmBillPay");
    };

    /**
     * Method to fetch all transaction by Payee
     * @param {object} params payeeId
    */
    BillPayPresentationController.prototype.fetchPayeeBills = function (params) {
        params.limit = applicationManager.getConfigurationManager().payeeBillsLimit;
        applicationManager.getTransactionManager().fetchPayeeBill(params, this.fetchPayeeBillsSuccessCallBack.bind(this), this.fetchPayeeBillsErrorCallBack.bind(this));
    };

    /**
     * individual payee transactions
     * @param {object} repsponse list of transactions by the payee
    */

    BillPayPresentationController.prototype.fetchPayeeBillsSuccessCallBack = function (response) {
        applicationManager.getNavigationManager().updateForm({ 'payeeActivities': response }, "frmBillPay");
    };

    /**
     *  individul payee transactions error schenario.
     *  @param {object} response error object
    */
    BillPayPresentationController.prototype.fetchPayeeBillsErrorCallBack = function (response) {
        var scopeObj = this;
        scopeObj.showServerError(response);
    };


    /**
     *  individul payee transactions error schenario.
     *  @param {object} response error object
    */
    BillPayPresentationController.prototype.navigateToManagePayeeFromViewActivity = function (response) {
        applicationManager.getNavigationManager().updateForm({ "navigateToManagePayeeFromViewActivity": response }, "frmBillPay");
    };

    /**     
        * Method to call delete Command Handler to delete biller in manage payee.     
        * @param {object} request delete object 
    */
    BillPayPresentationController.prototype.deleteManagePayee = function (request) {
        var self = this;
        self.showProgressBar("frmBillPay");
        applicationManager.getRecipientsManager().deletePayeeById(request.payeeId, this.deletePayeeByIdSuccessCallBack.bind(this), this.deletePayeeByIdErrorCallBack.bind(this));
    };

    /** 
     * used to  payee deletion success schenario
     * @param {object} response reponse of delete payee
    */
    BillPayPresentationController.prototype.deletePayeeByIdSuccessCallBack = function (response) {
        var self = this;
        self.managePayeePagination();
    };


    /** 
     * used to  payee deletion error schenario
     * @param {object} response response
    */
    BillPayPresentationController.prototype.deletePayeeByIdErrorCallBack = function (response) {
        var self = this;
        self.showServerError(response);
    };

    /**     
     * Method to deactivate EBill in manage payee.      
     * @param {object} payeeId    payee id
     */
    BillPayPresentationController.prototype.deactivateEbill = function (payeeId) {
        var eBillJson = {
            "payeeId": payeeId,
            "EBillEnable": 0
        };
        applicationManager.getRecipientsManager().updatePayeeDetails(eBillJson, this.updateSuccessCallBack.bind(this), this.updateErrorCallBack.bind(this));

    };

    /**
     * used to handels the bill pay deActivation success.
     * @param {object} response success reponse
     */
    BillPayPresentationController.prototype.updateSuccessCallBack = function (response) {
        var self = this;
        self.managePayeePagination();
    };

    /**
     * handels the error call back
     * @param {object} response success response
     */
    BillPayPresentationController.prototype.updateErrorCallBack = function (response) {
        var self = this;
        self.showServerError(response);
    };


    /**
    * Method to activate EBill in manage payee.
    * @param {String} data payee id
    * @param {string} selectedTab selected tab
    */
    BillPayPresentationController.prototype.modifyEbillStatus = function (data, selectedTab) {
        var self = this;
        self.showProgressBar("frmBillPay");
        var EbillChangeJSON = {
            "payeeId": data["payeeId"],
            "EBillEnable": 1
        };
        if (selectedTab === "managePayees") {
            applicationManager.getRecipientsManager().updatePayeeDetails(EbillChangeJSON, this.updateSuccessCallBack.bind(this), this.updateErrorCallBack.bind(this));
        }
        else {
            applicationManager.getRecipientsManager().updatePayeeDetails(EbillChangeJSON, this.updateStatusAllPayessSuccessCallBack.bind(this), this.updateErrorCallBack.bind(this));
        }
    };

    /**
     * used to navigate the allPayees flow
     * @param {object} response reponse
     */
    BillPayPresentationController.prototype.updateStatusAllPayessSuccessCallBack = function (response) {
        var self = this;
        self.showProgressBar("frmBillPay");
        self.showAllPayees();
    };


    /**     
     * Method to call Update Command Handler to update edited details in manage payee edit.     
     * @param {object} payeeRecord from updateBillPayee command handler     
     */
    BillPayPresentationController.prototype.updateManagePayee = function (payeeRecord) {
        var self = this;
        self.showProgressBar("frmBillPay");
        applicationManager.getRecipientsManager().updatePayeeDetails(payeeRecord, this.updateSuccessCallBack.bind(this), this.updateErrorCallBack.bind(this));
    };

    /**
    * Search BillPay Payees. 
    * @param {object}  data search string
    */

    BillPayPresentationController.prototype.searchBillPayPayees = function (data) {
        if (data && data.searchKeyword.length > 0) {
            var criteria = kony.mvc.Expression.eq("searchString", data.searchKeyword);
            applicationManager.getRecipientsManager().fetchPayeesList(criteria, this.fetchSearchPayeesListSuccessCallBack.bind(this, { "searchString": data.searchKeyword }),
                this.fetchSearchPayeesListFailureCallBack.bind(this))
        } else {
            applicationManager.getRecipientsManager().fetchPayeesList({}, this.fetchSearchPayeesListSuccessCallBack.bind(this, true),
                this.fetchSearchPayeesListFailureCallBack.bind(this))
        }
    };

    /**
     * search payee success schenario
    */

    BillPayPresentationController.prototype.fetchSearchPayeesListSuccessCallBack = function (searchInputs, response) {
        var dataModel = {}
        dataModel.managePayee = response;
        dataModel.searchvisibility = true;
        applicationManager.getNavigationManager().updateForm({ "managePayee": dataModel }, "frmBillPay");
    };

    /** 
     *  used to perform the error schenario
     *  @param {object} response response
    */
    BillPayPresentationController.prototype.fetchSearchPayeesListFailureCallBack = function (response) {
        var self = this;
        self.showServerError(response);
    }

    /** 
     * method for getting frequncies
     */
    BillPayPresentationController.prototype.getFrequencies = function () {
        //self.frequencies = applicationManager.getTransactionManager().frequencyTypes;
        var self = this;
        self.frequencies = {
            "Once": "i18n.transfers.frequency.once",
            "Daily": "i18n.Transfers.Daily",
            "Weekly": "i18n.Transfers.Weekly",
            "BiWeekly": "i18n.Transfers.EveryTwoWeeks",
            "Monthly": "i18n.Transfers.Monthly",
            "Quarterly": "i18n.Transfers.Quaterly",
            "Half Yearly": "i18n.Transfers.HalfYearly",
            "Yearly": "i18n.Transfers.Yearly"
        };
        self.getFrequencies = function () {
            var frequencies = self.frequencies;
            var list = [];
            for (var key in frequencies) {
                if (frequencies.hasOwnProperty(key)) {
                    list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
                }
            }
            return list;
        };
    };

    /**
     * method for getting for howLong values
    */

    BillPayPresentationController.prototype.getHowLongValues = function () {
        var self = this;
        self.forHowLong = {
            ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
            NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
        };
        self.getHowLongValues = function () {
            var howLongValues = self.forHowLong;
            var list = [];
            for (var key in howLongValues) {
                if (howLongValues.hasOwnProperty(key)) {
                    list.push([key, kony.i18n.getLocalizedString(howLongValues[key])]);
                }
            }
            return list;
        };
    };

    /**
     * used to get billPayAccounts
     * @returns {object} -- list of bill Pay accounts
    */
    BillPayPresentationController.prototype.getBillPaySupportedAccounts = function () {
        return applicationManager.getAccountManager().getBillPaySupportedAccounts();
    }

    /**
     * used to navigate the payABill confirmation screen
     * @param {object} payABill input transaction
     */
    BillPayPresentationController.prototype.setDataForConfirm = function (payABill) {
        applicationManager.getNavigationManager().navigateTo("frmConfirm");
        applicationManager.getNavigationManager().updateForm({ "payABill": payABill }, "frmConfirm");
    };

    /**
    * used to navigate the payABill confirmation screen
    * @returns {boolean} returns true or false
    */
    BillPayPresentationController.prototype.getDefaultBillPayPopUp = function () {
        return applicationManager.getUserPreferencesManager().isSetAccountPopupEnabled()
    };


    /**
        * checkMFASingleBillPay:   Method for handling MFA for SingleBillPay 
        * @param {object} ackPayABill input transaction
    */
    BillPayPresentationController.prototype.checkMFASingleBillPay = function (ackPayABill) {
        var self = this;
        self.singleBillPayPayement(ackPayABill);
    };

    /**
     * navigateToBillPay Page
    */
    BillPayPresentationController.prototype.navigateToBillPayForm = function () {
        applicationManager.getNavigationManager().navigateTo("frmBillPay");
    };

    /**
        * checkMFASingleBillPay:   Method for handling MFA for SingleBillPay 
        * @param {object} ackPayABill input transaction
    */
    BillPayPresentationController.prototype.singleBillPayPayement = function (ackPayABill) {
       var self = this;
      var mfaManager = applicationManager.getMFAManager();
        this.ackPayABill = ackPayABill;
        var date = ackPayABill.sendOn;
        var displayName = "BillPay";
        var formatUtil = applicationManager.getFormatUtilManager();
        applicationManager.getPresentationUtility().MFA.getServiceIdBasedOnDisplayName(displayName);
        var mfaParams = {
              serviceName: mfaManager.getServiceId(),
        };
        var addPayABillData = {
            amount: this.ackPayABill.amount,
            payeeId: this.ackPayABill.payeeId,
            billid: this.ackPayABill.billid,
            fromAccountNumber: this.ackPayABill.fromAccountNumber,
            transactionsNotes: this.ackPayABill.notes,
            scheduledDate: date,
            transactionType: applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.BILLPAY),
            "MFAAttributes" : mfaParams,
            "deliverBy": formatUtil.convertToUTC(this.ackPayABill.deliveryDate),

            //billCategory: ackPayABill.categories
        };
        if (this.ackPayABill.frequencyType == kony.i18n.getLocalizedString(self.frequencies.Once)) {
            addPayABillData.numberOfRecurrences = "";
            addPayABillData.frequencyStartDate = "";
            addPayABillData.frequencyEndDate = "";
            addPayABillData.frequencyType = this.ackPayABill.frequencyType;
        } else if (this.ackPayABill.hasHowLong == "NO_OF_RECURRENCES") {
            addPayABillData.frequencyStartDate = "";
            addPayABillData.frequencyEndDate = "";
            addPayABillData.frequencyType = this.ackPayABill.frequencyType;
            addPayABillData.numberOfRecurrences = this.ackPayABill.numberOfRecurrences;
        } else if (this.ackPayABill.hasHowLong == "ON_SPECIFIC_DATE") {
            addPayABillData.numberOfRecurrences = "";
            addPayABillData.frequencyType = this.ackPayABill.frequencyType;
            addPayABillData.frequencyEndDate = this.ackPayABill.frequencyEndDate;
            addPayABillData.frequencyStartDate = this.ackPayABill.frequencyStartDate;
        }
        if (this.ackPayABill.gettingFromOneTimePayment) {
            addPayABillData.toAccountNumber = this.ackPayABill.accountNumber;
            addPayABillData.zipCode = this.ackPayABill.zipCode;
            addPayABillData.billerId = this.ackPayABill.billerId;
        }
        if (this.ackPayABill.isScheduled === "false" || !this.ackPayABill.referenceNumber) {
            mfaManager.setMFAOperationType("CREATE");
            applicationManager.getTransactionManager().createTransaction(addPayABillData, self.singleBillPaySuccessCallBack.bind(this), self.singleBillPayFailureCallBack.bind(this));
        } else {
             mfaManager.setMFAOperationType("UPDATE");
            addPayABillData.transactionId = this.ackPayABill.referenceNumber;
            applicationManager.getTransactionManager().updateTransaction(addPayABillData, self.singleBillPaySuccessCallBack.bind(this), self.singleBillPayFailureCallBack.bind(this));
        }
    };

    /** 
     * used to handels the single billPay success schenario
     * @param {object} ackPayABill input data
     * @param {object} response reposne data
    */
    BillPayPresentationController.prototype.singleBillPaySuccessCallBack = function (response) {
      var self = this;
        var mfaManager = applicationManager.getMFAManager();
        if (response.referenceId) {
            self.getAccountDataByAccountId(this.ackPayABill, response.referenceId);
        }else if (response.transactionId) {
            self.getAccountDataByAccountId(this.ackPayABill, response.transactionId);
        } else {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "SINGLE_BILL_PAY",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }
    };

    /**
     * used to handels the single billPay error schenario
     * @param {object} ackPayABill input data
     * @param {object} response respone data
     */
    BillPayPresentationController.prototype.singleBillPayFailureCallBack = function (response) {
        var self = this;
        if (this.ackPayABill.gettingFromOneTimePayment) {
            response.gettingFromOneTimePayment = this.ackPayABill.gettingFromOneTimePayment;
        }
        else {
            response.gettingFromSingleBillPay = true;
        }
        applicationManager.getNavigationManager().navigateTo("frmBillPay");
        self.showServerError(response);
    };


    /**
      * fetch the billPay account
      * @param {Object} ackPayABill input data
      * @param {string} referenceId reference id
     */
    BillPayPresentationController.prototype.getAccountDataByAccountId = function (ackPayABill, referenceId) {
        var self = this;
        applicationManager.getAccountManager().fetchInternalAccounts(self.billPayAccountsSuccessCallBack.bind(this, ackPayABill, referenceId), self.billPayAccountsErrorCallBack.bind(ackPayABill, referenceId, this));
    };

    /**
     * used to navigate the acknowledgement screen
     * @param {object} ackPayABill input data
     * @param {object} referenceId  reference id
     * @param {object} response response data
     */
    BillPayPresentationController.prototype.billPayAccountsSuccessCallBack = function (ackPayABill, referenceId, response) {
        var accountData = applicationManager.getAccountManager().getInternalAccountByID(ackPayABill.fromAccountNumber);
        applicationManager.getNavigationManager().navigateTo("frmAcknowledgement");
        applicationManager.getNavigationManager().updateForm({ "ackPayABill": { "savedData": ackPayABill, "referenceId": referenceId, "accountData": accountData } }, "frmAcknowledgement");
    };

    /**
     * used to navigate the acknowledgement screen
     * @param {object} addPayABillData input data
     * @param {object} referenceId  reference id
     * @param {object} response  response data
     */
    BillPayPresentationController.prototype.billPayAccountsErrorCallBack = function (addPayABillData, referenceId, response) {
        var accountData = applicationManager.getAccountManager().getInternalAccountByID(addPayABillData.fromAccountNumber);
        applicationManager.getNavigationManager().navigateTo("frmAcknowledgement");
        applicationManager.getNavigationManager().updateForm({ "ackPayABill": { "savedData": addPayABillData, "referenceId": referenceId, "accountData": accountData } }, "frmAcknowledgement");
    };

    /**
     * used to navigate the singel BillPayement screen
     * @param {object} data  data
     * @param {string} sender sender
     */
    BillPayPresentationController.prototype.getSingleBillPay = function (data, sender) {
        var asyncManager = applicationManager.getAsyncManager();
        var billManager = applicationManager.getBillManager();
        var billParams = [];
        function showPayABillForm() {
            if (sender === "acknowledgement") {
                applicationManager.getNavigationManager().updateForm({ "singleBillPayData": data }, "frmBillPay");
            }
            else if (sender === "quickAction") {
                applicationManager.getNavigationManager().updateForm({ "singleBillPayData": data, "context": sender }, "frmBillPay");
            } else {
                applicationManager.getNavigationManager().updateForm({ "singleBillPayData": data }, "frmBillPay");
            }
        }
        if (sender === "Accounts") {
            //TODO remove async manager if not needed
            asyncManager.callAsync(
                [
                    asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', billParams)
                ], this.showBillPayFormAllSuccessCallback.bind(this, data, sender));
        } else {
            showPayABillForm();
        }
    };

    /**
     * used to set the bill pay data
     * @param {*} data data
     * @param {*} sender  sender
     * @param {*} syncResponseObject syncResponseObject 
     */
    BillPayPresentationController.prototype.showBillPayFormAllSuccessCallback = function (data, sender, syncResponseObject) {
        var scopeObj = this;
        //Need to change
        if (syncResponseObject.responses[0].data) {
            scopeObj.fetchDueBillsSuccessCallBack({
                "dueBills": true
            }, syncResponseObject.responses[0].data);
            applicationManager.getNavigationManager().updateForm({ "singleBillPayData": data }, "frmBillPay");
        } else {
            applicationManager.getNavigationManager().updateForm({
                "singleBillPayData": data
            }, "frmBillPay");
        }
    };

    /**
     * used to delete the scheduled  BillPay Transaction
     * @param {object} params transaction object
     */
    BillPayPresentationController.prototype.deleteScheduledTransaction = function (params) {
        applicationManager.getTransactionManager().deleteTransaction(params, this.deleteTransactionSuccessCallBack.bind(this), this.deleteTransactionFailureCallBack.bind(this));
    };

    /**
     * handels the transaction success call Back
     * @param {object} response success response
     */
    BillPayPresentationController.prototype.deleteTransactionSuccessCallBack = function (response) {
        var deleteSchedule = {};
        deleteSchedule.successData = "success";
        applicationManager.getNavigationManager().updateForm({ "deleteSchedule": deleteSchedule }, "frmBillPay");
    };

    /**
     * handels the transaction delete error call Back
     * @param {object} response error response
     */
    BillPayPresentationController.prototype.deleteTransactionFailureCallBack = function (response) {
        var scopeObj = this;
        scopeObj.showServerError(response);
    }

    /**
     * Method to cancel transaction occurrence
     * @param {object} transaction object
     */
    BillPayPresentationController.prototype.cancelScheduledTransactionOccurrence = function (transaction) {
        applicationManager.getTransactionManager().deleteRecurrenceTransaction(transaction, this.deleteTransactionSuccessCallBack.bind(this), this.deleteTransactionFailureCallBack.bind(this));
    };

    /** 
     * used to update the showBillPayPoPUp
    */
    BillPayPresentationController.prototype.updateShowBillPayFromAccPop = function () {
        var param = {
            showBillPayFromAccPopup: false
        };
        applicationManager.getUserPreferencesManager().updateUserDetails(param, function () { }, function () { });
    };

    /**
     * Method for navigate to Cofirm form
     *@param {object} data for form
     */
    BillPayPresentationController.prototype.navigateToAcknowledgementForm = function (data) {
        applicationManager.getNavigationManager().navigateTo("frmAcknowledgement");
        applicationManager.getNavigationManager().updateForm(data, "frmAcknowledgement");
    };
    /** Present frmPrintTransfer
     * @param {object} viewModel Details of the Account
     */
    BillPayPresentationController.prototype.showPrintPage = function (data) {
        applicationManager.getNavigationManager().navigateTo("frmPrintTransfer");
        applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
    };

    /**
     * used to fetch the billers
     * @param {string} searchValue search string 
     */
    BillPayPresentationController.prototype.fetchBillerList = function (searchValue) {
        var self = this;
        if (searchValue === null || searchValue.trim() === "") {
            return;
        }
        self.showProgressBar("frmAddPayee");
        lastSearchValue = searchValue;
        applicationManager.getRecipientsManager().fetchPayeeSuggestions(searchValue, self.fetchBillerListSuccessCallBack.bind(this, searchValue), self.fetchBillerListFailureCallBack.bind(this));
    };

    /** 
     * handels the fetch biller success schenario
     * @param {string} searchValue search value
     * @param {object} response list of billers
    */
    BillPayPresentationController.prototype.fetchBillerListSuccessCallBack = function (searchValue, response) {
        var self = this;
        if (lastSearchValue === searchValue) { // because response is async.
            self.hideProgressBar("frmAddPayee");
            self.getBillerList(response);
        }
    };

    /** 
     * handels the fetch biller failure schenario
     * @param {object} response error response
    */
    BillPayPresentationController.prototype.fetchBillerListFailureCallBack = function () {
        var self = this;
        self.hideProgressBar("frmAddPayee");
    };

    /**
     * used to set the biller list
     * @param {object} billersList biller list
     */
    BillPayPresentationController.prototype.getBillerList = function (billersList) {
        var self = this;
        /**
         * used to create the biller information
         * @param {object} biller biller object
         * @returns {object} billerObj biller object 
         */
        var createBillerList = function (biller) {
            var billerObj = {
                billerName: biller.billerName,
                billerId: biller.id,
                billerCategoryId: biller.billerCategoryId,
                billerCategoryName: biller.billerCategoryName,
                billerAddress: biller.address,
                /**
                 * used to set the biller selection
                 * @returns {object} biller information
                 */
                onBillerSelection: function () {
                    return self.updateBillerDetails(billerObj);
                }
            };
            return billerObj;
        };
        applicationManager.getNavigationManager().updateForm({ "billersList": billersList.map(createBillerList) }, "frmAddPayee")
    };

    /**
     * used to update the biller details
     * @param {object} biller biller information
    */
    BillPayPresentationController.prototype.updateBillerDetails = function (biller) {
        applicationManager.getNavigationManager().updateForm({ "billerDetails": biller }, "frmAddPayee");
        payeeDetails.updateSelectedBiller(biller);
    };

    /** 
     * used to navigate the Add Payee screen
    */
    BillPayPresentationController.prototype.navigateToAddPayee = function () {
        var initialConfig = { 'firstLoad': true };
        payeeDetails.init();
        applicationManager.getNavigationManager().navigateTo("frmAddPayee");
        applicationManager.getNavigationManager().updateForm(initialConfig, "frmAddPayee");
        this.showProgressBar("frmAddPayee");
        this.getRegisteredPayeeList();
    };

    /** 
     * used to get the registered payees list
    */
    BillPayPresentationController.prototype.getRegisteredPayeeList = function () {
        var criteria;
        criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("sortBy", "billDueDate"),
            kony.mvc.Expression.eq("order", "desc")
        )
        applicationManager.getRecipientsManager().fetchPayeesList(criteria, this.fetchGetRegisteredPayeeListSuccessCallBack.bind(this), this.fetchGetRegisteredPayeeListErrorCallBack.bind(this));
    }

    /**
     * used to handels the get registered payee success schenario
     * @param {object} response list of payees
     */
    BillPayPresentationController.prototype.fetchGetRegisteredPayeeListSuccessCallBack = function (response) {
        var self = this;
        self.showRegisteredPayeeList(response);
    }
    /**
     * used to handels the get registered payees error schenario
     * @param {object} response error response 
     */
    BillPayPresentationController.prototype.fetchGetRegisteredPayeeListErrorCallBack = function (response) {

    }
    /**
     * used to show the list of all payees on the screen.
     * @param {object} payeeList list of payees 
     */
    BillPayPresentationController.prototype.showRegisteredPayeeList = function (payeeList) {
        var self = this;
        /**
         * payee list informtion
         * @param {object} payee payee
         * @returns {object} payeeObj payee object
         */
        var createPayeeList = function (payee) {
            var payeeObj = {
                payeeName: payee.payeeNickName || payee.companyName,
                lastPaidDate: payee.lastPaidDate,
                lastPaidAmount: payee.lastPaidAmount,
                payeeId: payee.payeeId,
                accountNumber: payee.accountNumber,
                billDueDate: payee.billDueDate,
                billid: payee.billid,
                dueAmount: payee.dueAmount,
                payeeNickname: payee.payeeNickName || payee.companyName,
                eBillStatus: payee.eBillStatus,
                show: "PayABill",
                /**
                 * used to navigate the view details
                 */
                onViewDetailsClick: function () {
                    self.showBillPayData(null, "ManagePayees", false);
                },
                /**
                 * used to navigate the make payement screen
                 */
                onPayBillsClick: function () {
                    self.makePayment(payeeObj);
                }
            };
            return payeeObj;
        };
        applicationManager.getNavigationManager().updateForm({ "registeredPayeeList": payeeList.map(createPayeeList) }, "frmAddPayee");
        self.hideProgressBar("frmAddPayee");
    };

    /**
     * used to navigate the make billPayement screen
     * @param {object} payee payee information
     */

    BillPayPresentationController.prototype.makePayment = function (payee) {
        var self = this;
        if (payee) {
            payee.onCancel = self.showBillPayData.bind(self);
            self.showBillPayData(null, "PayABill", true, payee);
        } else {
            var payeeObj = payeeDetails.getPayeeDetailsForPayment();
            payeeObj.onCancel = self.showBillPayData.bind(self);
            self.showBillPayData(null, "PayABill", true, payeeObj);
        }
    };

    /**
     * used to navigate the make one time payement screen
     * @param {function} isCancelCallBack cancel callback
     */

    BillPayPresentationController.prototype.navigateToOneTimePayement = function (isCancelCallBack) {
        if (isCancelCallBack !== true) {
            var initialConfig = { 'initOneTimePayment': true };
            payeeDetails.init();
            applicationManager.getNavigationManager().navigateTo("frmAddPayee");
            applicationManager.getNavigationManager().updateForm(initialConfig, "frmAddPayee");
            this.getRegisteredPayeeList();
        } else {
            applicationManager.getNavigationManager().navigateTo("frmAddPayee");
            applicationManager.getNavigationManager().updateForm({}, "frmAddPayee");
        }
    };

    /**
     * used to update the one time payement information
     * @param {object} payeeInfo payee information
     */
    BillPayPresentationController.prototype.updateOneTimePayeeInfo = function (payeeInfo) {
        var errorMessage;
        if ((errorMessage = payeeDetails.isValidInput(payeeInfo)) === true) {
            payeeDetails.updatePayeeDetails(payeeInfo);
            this.navigateToPayBills(payeeDetails.getOneTimePayeeInfo());
        } else {
            applicationManager.getNavigationManager().navigateTo("frmAddPayee");
            applicationManager.getNavigationManager().updateForm({ "validationError": errorMessage }, "frmAddPayee");
        }
    };

    /**
     * used to update the biller
     * @param {object} transaction transaction
     */
    BillPayPresentationController.prototype.showUpdateBillerPageWithTransaction = function (transaction) {
        payeeDetails.updateTransationId(transaction);
        applicationManager.getNavigationManager().navigateTo("frmAddPayee");
        applicationManager.getNavigationManager().updateForm({ "payeeUpdateDetails": payeeDetails.getPayeeDetailsToUpdatePage() }, "frmAddPayee");
    };
    /**
     * used to update the biller page
     * @param {object} payeeInfo payee information
     */
    BillPayPresentationController.prototype.showUpdateBillerPage = function (payeeInfo) {
        if (payeeInfo && !payeeDetails.isBillerDetailValid(payeeInfo)) {
            applicationManager.getNavigationManager().updateForm({ isInvalidPayee: true }, "frmAddPayee");
        } else {
            if (payeeInfo) { payeeDetails.updatePayeeDetails(payeeInfo) };
            applicationManager.getNavigationManager().updateForm({ "payeeUpdateDetails": payeeDetails.getPayeeDetailsToUpdatePage() }, "frmAddPayee");
        }
    };
    /**
     * shows the add payee confirmation screen
     * @param {object} payeeInfo payee information 
     */
    BillPayPresentationController.prototype.showAddPayeeConfirmPage = function (payeeInfo) {
        payeeDetails.updateConfirmPayeeDetails(payeeInfo);
        applicationManager.getNavigationManager().updateForm({ payeeConfirmDetails: payeeDetails.getPayeeDetailsConfirmPage() }, "frmAddPayee");
    };
    /**
     * shows the add payee success screen
     * @param {object} response  reponse
     */
    BillPayPresentationController.prototype.showAddPayeeSucess = function (response) {
        if (response === undefined || response === null) {
            this.addPayeeDetails();
        } else if (response.payeeId !== undefined && response.payeeId !== "") {
            payeeDetails.updatePayeeId({
                payeeId: response.payeeId
            });
            if (payeeDetails.getTransationId()) {
                this.updateOneTimePayment({
                    payeeId: response.payeeId,
                    transactionId: payeeDetails.getTransationId()
                });
            }
            this.hideProgressBar("frmAddPayee");
            applicationManager.getNavigationManager().updateForm({ payeeSuccessDetails: payeeDetails.getPayeeDetailsSuccessPage() }, "frmAddPayee");
        } else {
            this.showAddPayeeError(response);
        }
    };
    /**
     * show update Biller page with error message
     * @param {object} response  response
     */
    BillPayPresentationController.prototype.showAddPayeeError = function (response) {
        //show update Biller page with error message
        var errmsg = {
            payeeUpdateDetails: payeeDetails.getPayeeDetailsToUpdatePage()
        }
        if (response == undefined || !response.errmsg) {
            errmsg.errorInAddingPayee = "Server error";
        } else {
            errmsg.errorInAddingPayee = response.errmsg;
        }
        this.hideProgressBar("frmAddPayee");
        applicationManager.getNavigationManager().updateForm(errmsg, "frmAddPayee");
    }

    /** 
     * used to create the payee
    */
    BillPayPresentationController.prototype.addPayeeDetails = function () {
        applicationManager.getRecipientsManager().createPayee(payeeDetails.getRequestObject(), this.addPayeeSuccessCallBack.bind(this), this.addPayeeFailureCallbak.bind(this));
    };

    /** 
     * add payee success schenario
    * @param {object} response  respone
    */
    BillPayPresentationController.prototype.addPayeeSuccessCallBack = function (response) {
        var self = this;
        self.showAddPayeeSucess(response);
    };

    /** 
    * add payee success schenario
    * @param {object} response error respone
   */
    BillPayPresentationController.prototype.addPayeeFailureCallbak = function (response) {
        var self = this;
        self.showAddPayeeError(response);
    };

    /**
     * used to update the one time payement
     * @param {object} data data 
     */
    BillPayPresentationController.prototype.updateOneTimePayment = function (data) {
        applicationManager.getTransactionManager().updateTransaction(data, function () { }, function () { });
    };

    /**
     * used to navigate the make one time payement screen 
     * @param {object} payee payee 
     */
    BillPayPresentationController.prototype.navigateToPayBills = function (payee) {
        var self = this;
        self.showOneTimePayment(payee);
    };

    /**
     * used to show the one time payement page
     * @param {object} data one time payement page 
     * @param {object} sender sender name
     */
    BillPayPresentationController.prototype.showOneTimePayment = function (data, sender) {
        var self = this;
        self.showBillPayData(sender, "showOneTimePayment", true, data);
    };

    /**
     * used to navigate the one time payement page
     * @param {object}  sender one time payement page 
     * @param {object} data  sender name
     */
    BillPayPresentationController.prototype.showOneTimePaymentPage = function (sender, data) {

        var oneTimePayementObj = {
            data: data,
            sender: sender
        };
        var asyncManager = applicationManager.getAsyncManager();
        var billManager = applicationManager.getBillManager();
        var billParams = [];

        applicationManager.getNavigationManager().navigateTo("frmBillPay");
        applicationManager.getNavigationManager().updateForm({ "showOneTimePayment": oneTimePayementObj }, "frmBillPay");
        //TODO check whether to remove fetchUserBillPayPendingTransactions
        /*
        if (categoryLen.length === 0) {
            asyncManager.callAsync(
                [
                    asyncManager.asyncItem(billManager, 'fetchUserBillPayPendingTransactions', billParams),
                    asyncManager.asyncItem(billManager, 'fetchBillerCategories', [])
                ], this.showOneTimePaymentSuccessCallBack.bind(this, oneTimePayementObj));
        } else {
            applicationManager.getNavigationManager().navigateTo("frmBillPay");
            applicationManager.getNavigationManager().updateForm({ "showOneTimePayment": oneTimePayementObj }, "frmBillPay");
        }
        */
    };


    /**
     * used to handels the oneTimePayement success
     * @param {object} oneTimePayementObj oneTimePayementObj
     * @param {object} syncResponseObject syncResponseObject
     */
    BillPayPresentationController.prototype.showOneTimePaymentSuccessCallBack = function (oneTimePayementObj, syncResponseObject) {
        //Need to change
        var scopeObj = this;
        if (syncResponseObject.responses[0].data) {
            scopeObj.fetchDueBillsSuccessCallBack({
                "dueBills": true
            }, syncResponseObject.responses[0].data);
        }
        applicationManager.getNavigationManager().updateForm({ "showOneTimePayment": oneTimePayementObj }, "frmBillPay");
    };
    /**
     * used to Format the amount
     * @param {string} amount amount
     * @param {boolean} currencySymbolNotRequired currency symbol required
     * @returns {string} formated amount
     */
    BillPayPresentationController.prototype.formatAmount = function (amount, currencySymbolNotRequired) {
        if (currencySymbolNotRequired) {
            return applicationManager.getFormatUtilManager().formatAmount(amount);
        }
        else {
            return applicationManager.getFormatUtilManager().formatAmountandAppendCurrencySymbol(amount);
        }
    };

    /** 
     * used to navigate the new acount opening flow
    */
    BillPayPresentationController.prototype.openNewBillPayAccount = function () {
        var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        nuoModule.presentationController.showNewAccountOpening();
    };

    /** 
     * used to navigate the new acount opening flow
    */
    BillPayPresentationController.prototype.cancelEligibile = function () {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.presentAccountsLanding();
    };

    return BillPayPresentationController;
});