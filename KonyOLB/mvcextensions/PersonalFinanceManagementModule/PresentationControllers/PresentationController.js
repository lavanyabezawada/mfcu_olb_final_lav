define(['CommonUtilities','OLBConstants'], function(CommonUtilities,OLBConstants) {

  function PersonalFinanceManagement_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
    this.initializePresentationController();
  }
  inheritsFrom(PersonalFinanceManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
  /**
     * initializePresentationController - Method to intialize Presentation controller data , called by constructor
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {Object} dataInputs - dataInputs to configure Presentation controller. (optional) - useful when we need customize.
     * @returns {}
     * @throws {}
     */
  PersonalFinanceManagement_PresentationController.prototype.initializePresentationController = function() {
    var scopeObj = this;
    scopeObj.viewModel = {};
    scopeObj.serverErrorVieModel = 'serverError';
    var configManager = applicationManager.getConfigurationManager();
    scopeObj.sortUnCatTranascionsConfig = {
      'sortBy': 'transactionDate',
      'defaultSortBy': 'transactionDate',
      'order': configManager.OLBConstants.DESCENDING_KEY,
      'defaultOrder': configManager.OLBConstants.DESCENDING_KEY
    };
  };

  /**
     * naviageteToAccountLandingPage : used to naviagte the dashboard page.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.naviageteToAccountLandingPage = function() {
    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountsModule.presentationController.showAccountsDashboard();
  };

  /**
     * presentPFM : Method for used to initalize the form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of Years.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.initPFMForm = function(data) {
    var date = new Date();
    var presentYear = date.getFullYear();
    var presentMonth = date.getMonth();
    if(presentYear && date.getMonth()===0)
    {
      presentYear = presentYear - 1;
      presentMonth = 12;
    }
    var self = this;
    applicationManager.getNavigationManager().navigateTo("frmPersonalFinanceManagement");
    self.showProgressBar();
    var viewModel = {};
    viewModel.hideflex = true;
    viewModel.getYears = self.getPFMYears();
    viewModel.getMonths = self.getPFMMonthsByYear(presentYear);
    viewModel.showMonthlyDonutChart = true;
    self.getMonthlySpending(true,presentMonth,presentYear,viewModel);
    self.getYearlySpending(presentYear);
    self.getPFMRelatedAccounts(presentMonth,presentYear);
    self.getPFMBudgetChart(presentMonth,presentYear);
  };

  /**
     * formatMonthlyCategorizedTransactions : used to format the Monthly Categorized transactions.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get monthly data
     * @returns {} 
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.formatMonthlyCategorizedTransactions = function(data) {
    if (data.length != 0) {
      var categoryIds = [];
      var catgoriesArray = [];
      for (var i = 0; i < data.length; i++) {
        var totalAmount = 0.0;
        var transaction = data[i];
        if (categoryIds.indexOf(transaction.categoryId) == -1) {
          var categoryobj = {};
          var headerObj = {};
          var transactionobj = {};
          var categoryArray = [];
          categoryIds.push(transaction.categoryId);
          headerObj.categoryName = transaction.categoryName;
          var configManager = applicationManager.getConfigurationManager();
          headerObj.categoryColor = configManager.PFM_CATEGORIES_COLORS[transaction.categoryName];
          for (var j = 0; j < data.length; j++) {
            if (data[j].categoryId == data[i].categoryId) {
              totalAmount += parseFloat(data[j].transactionAmount);
              headerObj.totalAmount = totalAmount;
              data[j].transactionDate = CommonUtilities.getFrontendDateString(data[j].transactionDate);
              data[j].displayAmount = CommonUtilities.formatCurrencyWithCommas(data[j].transactionAmount)
              categoryArray.push(data[j]);
            }
          }
          transactionobj.categoryArray = categoryArray;
          headerObj.totalAmount = CommonUtilities.formatCurrencyWithCommas(headerObj.totalAmount);
          categoryobj.header = headerObj;
          categoryobj.transactionobj = categoryArray;
          catgoriesArray.push(categoryobj);
        } else {}
      }
      return catgoriesArray;
    } else {}
  }

  /**
     * getMonthlyCategorizedTransactions : used to get Monthly Categorized transactions.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get monthly data
     * @returns {} 
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getMonthlyCategorizedTransactions = function(monthId) {
    var self = this;
    self.showProgressBar();
    if(!monthId)
    {
      var date = new Date();
      monthId = date.getMonth();
    }
    var reqObject = {
      'monthId': monthId
    };
    var accountManager = applicationManager.getAccountManager();
    accountManager.getMonthlyCategorizedTransactions(monthId,this.getMonthlyCategorizedTransactionsSuccess.bind(this),this.getMonthlyCategorizedTransactionsFailure.bind(this));
  };

  PersonalFinanceManagement_PresentationController.prototype.getMonthlyCategorizedTransactionsSuccess = function(response){
    var viewModel = {};
    viewModel.showMonthlyCategorizedTransactions = true;
    viewModel.monthlyCategorizedTransactions = this.formatMonthlyCategorizedTransactions(response);
    this.presentPFM(viewModel);
  };

  PersonalFinanceManagement_PresentationController.prototype.getMonthlyCategorizedTransactionsFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };

  /**
     * formatPFMBudgetChartData : used to format the data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get All monthly data
     * @returns {} get all list of months.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.formatPFMBudgetChartData = function(yearsPFMBudgetdata) {
    function addRequirePFMBudgetChartFields(yearBudget) {
      var leftBudget = (Number(yearBudget.allocatedAmount) - Number(yearBudget.amountSpent));
      yearBudget.categoryName = yearBudget.categoryName;
      yearBudget.budget = Number(yearBudget.amountSpent);
      var configManager = applicationManager.getConfigurationManager();
      yearBudget.budgetColorCode = configManager.PFM_CATEGORIES_COLORS[yearBudget.categoryName];
      yearBudget.budgetAnnotationText = "";
      yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(yearBudget.amountSpent) +"  " +kony.i18n.getLocalizedString("i18n.common.usedfrom") +"  " +CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
      yearBudget.remaningBuget = leftBudget;
      if(leftBudget<0)
      {
        yearBudget.remaningBuget = 0;
        yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(Math.abs(leftBudget)) +  "  " +kony.i18n.getLocalizedString("i18n.common.over") +"  "+ CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
      }
      else
      {
        yearBudget.remaningBuget = leftBudget;
        yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(yearBudget.amountSpent) + "  " + kony.i18n.getLocalizedString("i18n.common.usedfrom") + "  " + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
      }
      yearBudget.remainingBudgeTooltipText = CommonUtilities.formatCurrencyWithCommas(leftBudget) + "  " + kony.i18n.getLocalizedString("i18n.common.leftfrom") + "  " + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
      yearBudget.remaningBugetColorCode = "color:" + configManager.PFM_CATEGORIES_COLORS[yearBudget.categoryName] + ";" + "opacity:0.3";
      yearBudget.remaingBudgetAnnotationText = "";
      yearBudget.remainingBudgeTooltipText= CommonUtilities.formatCurrencyWithCommas(leftBudget)+"  " + kony.i18n.getLocalizedString("i18n.common.leftfrom")+"  "  + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);

      return yearBudget;
    }
    var pfmBudgetData = yearsPFMBudgetdata.map(addRequirePFMBudgetChartFields);
    return pfmBudgetData;
  };
  /**
     * getPFMBudgetChart : Method for used to draw budget chart.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {year,viewModel} /get All monthly data
     * @returns {} get all list of months.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getPFMBudgetChart = function(monthId,year) {
    var self = this;
    self.showProgressBar();
    var reqObject = {
      'year': year,
      'monthId':monthId
    };
    var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("monthId", reqObject.monthId), 
                                           kony.mvc.Expression.eq("year", reqObject.year));
    var accountManager = applicationManager.getAccountManager();
    accountManager.getYearlyBudgetData(criteria,this.getPFMBudgetChartSuccess.bind(this),this.getPFMBudgetChartFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getPFMBudgetChartSuccess = function(response){
    var viewModel = {};
        viewModel.showYearlyBudgetChart = true;
        viewModel.yearlyBudgetData = this.formatPFMBudgetChartData(response);
        this.presentPFM(viewModel);
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getPFMBudgetChartFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
         * presentPFM : Method for used to Format the Chart Data.
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {monthlyData} /get All monthly data
         * @returns {} get all list of months.
         * @throws {} 
         */
  PersonalFinanceManagement_PresentationController.prototype.formatPFMDonuctChartData = function(monthlyData) {
    function addRequireDonutChartFields(month) {
      month.label = month.categoryName;
      month.Value = Number(month.cashSpent);
      var configManager = applicationManager.getConfigurationManager();
      month.colorCode = configManager.PFM_CATEGORIES_COLORS[month.categoryName];
      return month;
    }
    var pfmData = monthlyData.map(addRequireDonutChartFields);
    return pfmData;
  }
  /**
         * presentPFM : Method for used to Format the Chart Data.
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {monthlyData} /get All Year data
         * @returns {} get all list of Years.
         * @throws {} 
         */
  PersonalFinanceManagement_PresentationController.prototype.formatPFMBarChartData = function(yearlyData) {
    function addRequireBarChartFields(yearSpending) {
      yearSpending.label = yearSpending.monthName;
      yearSpending.Value = Number(yearSpending.totalCashFlow);
      yearSpending.colorCode = "#3c6cbe";
      yearSpending.annotationText = CommonUtilities.formatCurrencyWithCommas(yearSpending.totalCashFlow);
      yearSpending.tooltipText = yearSpending.monthName +": " +CommonUtilities.formatCurrencyWithCommas(yearSpending.totalCashFlow);
      return yearSpending;
    }
    var pfmData = yearlyData.map(addRequireBarChartFields).reverse();
    return pfmData;
  }
  /**
         * presentPFM : Method for return list of years
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {data} /viewmodel for form
         * @returns {} get all list of Years.
         * @throws {} 
         */
    PersonalFinanceManagement_PresentationController.prototype.getPFMYears = function() {
        var pfmYears = [];
        var date = new Date();
        var presentYear = date.getFullYear();
        if(presentYear && date.getMonth()===0)
        {
          presentYear = presentYear - 1;
        }
        for (var i = 0; i < applicationManager.getConfigurationManager().pfmMaxYears; i++) {
          pfmYears.push(presentYear - i);
        }
        return pfmYears;
    };
    /**
     * presentPFM : Method for return List Of Months.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of months.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getPFMMonthsByYear = function(year) {
    var pfmMonthsWithYear = {};
    pfmMonthsWithYear.year = year;
    var date = new Date();
    var presentYear = date.getFullYear();
    if(presentYear && date.getMonth()===0)
    {
      pfmMonthsWithYear.pfmMonths = CommonUtilities.returnMonths();
    }else if (year == presentYear) {
      pfmMonthsWithYear.pfmMonths = CommonUtilities.returnMonths(date.getMonth());
    } else {
      pfmMonthsWithYear.pfmMonths = CommonUtilities.returnMonths();
    }
    return pfmMonthsWithYear;
  };
  /**
     * getMonthlySpending : used to get Montly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.selectYear = function(year) {
    var viewModel = {};
    viewModel.getMonths = this.getPFMMonthsByYear(year);
    this.presentPFM(viewModel);
  };
  /**
     * getPFMAccounts : used to get All PFMAccouns.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} 
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getPFMRelatedAccounts = function(monthId,year) {
    var self = this;
    self.showProgressBar();
    var reqObject = {
      'year': year,
      'monthId':monthId
    };
    var accountManager = applicationManager.getAccountManager();
    accountManager.getSelecetdPFMAccounts(reqObject,this.getPFMRelatedAccountsSuccess.bind(this),this.getPFMRelatedAccountsFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getPFMRelatedAccountsSuccess = function(response){
    var viewModel = {};
        viewModel.showPFMAccounts = true;
        viewModel.pfmAccounts = this.formatPFMAccounts(response.records);
        this.presentPFM(viewModel);
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getPFMRelatedAccountsFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
     * formatPFMAccounts : used to format the accounts.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} 
     * @returns {} get all pfm related accounts.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.formatPFMAccounts = function(accounts) {
    function formatPFMAccount(account) {
      account.accountID = kony.i18n.getLocalizedString("i18n.common.accountNumber")+" " +  CommonUtilities.getMaskedAccountNumber(account.accountID);
      account.availableBalance = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
      account.totalDebits =  CommonUtilities.getDisplayCurrencyFormat(account.totalDebitsMonth,false,account.currencyCode);
      account.totalCredits =  CommonUtilities.getDisplayCurrencyFormat(account.totalCreditMonths,false,account.currencyCode);
      account.currentBalance = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
      account.outstandingBalance = CommonUtilities.formatCurrencyWithCommas(account.outstandingBalance,false,account.currencyCode);
      return account;
    }
    var pfmAccounts = accounts.map(formatPFMAccount);
    return pfmAccounts;
  };
  /**
     * getMonthlySpending : used to get Montly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getMonthlySpending = function(showBothDonutChart,monthId,year,frmView) {
    var self = this;
    self.showProgressBar();
    var reqObject = {
      'monthId': monthId,
      'year' : year
    };
    var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("monthId", reqObject.monthId), 
                                           kony.mvc.Expression.eq("year", reqObject.year));
    var accountManager = applicationManager.getAccountManager();
    accountManager.getMonthlySpending(criteria,this.getMonthlySpendingSuccess.bind(this,showBothDonutChart,frmView),this.getMonthlySpendingFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getMonthlySpendingSuccess = function(showBothDonutChart,frmView,response){
    if (frmView === undefined) {
          var viewModel = {};
          if(showBothDonutChart)
            viewModel.showBothDonutCharts = true;
          else
            viewModel.showBothDonutCharts = false;
          viewModel.showMonthlyDonutChart = true;
          viewModel.monthlySpending = this.formatPFMDonuctChartData(response);
          if(response.length !==0)
            viewModel.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(response[0].totalCashSpent);
          this.presentPFM(viewModel);
        } else {
          frmView.monthlySpending = this.formatPFMDonuctChartData(response);
          if(response.length !==0)
            frmView.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(response[0].totalCashSpent);
          if(showBothDonutChart)
            frmView.showBothDonutCharts = true;
          else
            frmView.showBothDonutCharts = false;
          this.presentPFM(frmView);
        }
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getMonthlySpendingFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
     * getYearlySpending : used to get Yearly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {yearId} //sending particular year
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.getYearlySpending = function(year) {
    var self = this;
    self.showProgressBar();
    var reqObject = {
      'year': year
    };
    var criteria=kony.mvc.Expression.eq("year",reqObject.year);
    var accountManager = applicationManager.getAccountManager();
    accountManager.getYearlySpending(criteria,this.getYearlySpendingSuccess.bind(this,year),this.getYearlySpendingFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getYearlySpendingSuccess = function(year,response){
    var pfmYearData = response;
    var date = new Date();
        if(date.getFullYear() == year)
        {
          pfmYearData = pfmYearData.slice(0,date.getMonth());
        }
        var viewModel = {};
        viewModel.showYearlyBarChart = true;
        viewModel.yearlySpending = this.formatPFMBarChartData(pfmYearData);
        this.presentPFM(viewModel);      
  };
  
  PersonalFinanceManagement_PresentationController.prototype.getYearlySpendingFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
     * createDataModelUnCategorizedTransations : Method for create data model for uncategorized transaction
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {transaction} /transaction array for form
     * @returns {}
     * @throws {} 
     */    
  PersonalFinanceManagement_PresentationController.prototype.createDataModelUnCategorizedTransations = function (transaction) {
    return {
      transactionId : transaction.transactionId,
      categoryName : transaction.categoryName,
      categoryId : transaction.categoryId,
      toAccountName : transaction.toAccountName,
      toAccountNumber : transaction.toAccountNumber,
      fromAccountName : transaction.fromAccountName,
      fromAccountNumber : transaction.fromAccountNumber,
      transactionAmount : CommonUtilities.formatCurrencyWithCommas(transaction.transactionAmount),
      transactionDate : CommonUtilities.getFrontendDateString(transaction.transactionDate),
      transactionDescription : transaction.transactionDescription
    }
  };
  /**
     * showBulkUpdateTransaction : Method to present the viewmodel in form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} /viewmodel for form
     * @returns {}
     * @throws {} 
     */    
  PersonalFinanceManagement_PresentationController.prototype.showBulkUpdateTransaction = function(){
    this.presentPFM({bulkUpdateTransactionList : this.transactionViewModel});
  };
  /**
     * showUnCategorizedTransaction : Method to display viewmodel in form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} 
     * @throws {} 
     */    
  PersonalFinanceManagement_PresentationController.prototype.showUnCategorizedTransaction = function(transactionObj){
    this.transactionViewModel = transactionObj.data.map(this.createDataModelUnCategorizedTransations);
    this.presentPFM({
      unCategorizedIdTransactionList: {
        data : this.transactionViewModel, 
        config: transactionObj.config

      }
    });
  };
  /**
     * fetchUnCategorizedTransations : Method for load uncategorised transaction list.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} / monthId to load
     * @returns {} \
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.fetchUnCategorizedTransations = function (monthId, yearId, dataInputs) {

    var self = this;
    if (monthId) {
      this.monthId = monthId;
      this.yearId = yearId;
    }
    if (this.unCategorizedId === undefined || this.unCategorizedId === null) {
      this.fetchCategoryList();
      return;
    }
    var sortInputs = this.sortUnCatTranascionsConfig;
    if(dataInputs) {
      sortInputs= CommonUtilities.Sorting.getSortConfigObject({
        sortBy: dataInputs.sortBy ,
        order: dataInputs.order
      }, self.sortUnCatTranascionsConfig);
    }

    var reqObject = {
      monthId: self.monthId,
      yearId: self.yearId,
      categoryId: self.unCategorizedId,
      sortBy: sortInputs.sortBy,
      order: sortInputs.order
    };
    var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("monthId", reqObject.monthId),
                        kony.mvc.Expression.eq("year", reqObject.yearId),
                        kony.mvc.Expression.eq("categoryId", reqObject.categoryId),
            kony.mvc.Expression.eq("sortBy", reqObject.sortBy), 
            kony.mvc.Expression.eq("order", reqObject.order));
    var accountManager = applicationManager.getAccountManager();
    accountManager.getUncategorizedTransactions(criteria,this.fetchUnCategorizedTransationsSuccess.bind(this,reqObject),this.fetchUnCategorizedTransationsFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.fetchUnCategorizedTransationsSuccess = function(reqObject,response){
    this.showUnCategorizedTransaction({
          data: response,
          config: reqObject
        });
  };
  
  PersonalFinanceManagement_PresentationController.prototype.fetchUnCategorizedTransationsFailure = function(error){
    this.presentPFM({
          onServerDownError: true
        });
  };
  /**
     * bulkUpdateCategory : Method for update the transactions
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {transactionsList} /list of transactions
     * @returns {}
     * @throws {} 
     */    
  PersonalFinanceManagement_PresentationController.prototype.bulkUpdateCategory = function(transactionList){
    var self = this;
    var accountManager = applicationManager.getAccountManager();
    accountManager.bulkUpdateTransactions(transactionList,this.bulkUpdateCategorySuccess.bind(this),this.bulkUpdateCategoryFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.bulkUpdateCategorySuccess = function(response){
    
  };
  
  PersonalFinanceManagement_PresentationController.prototype.bulkUpdateCategoryFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
     * fetchCategoryList : Method to load categories list.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} 
     * @returns {} 
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.fetchCategoryList = function () {
    var self = this;
    var accountManager = applicationManager.getAccountManager();
    accountManager.getPFMCategories(this.fetchCategoryListSuccess.bind(this),this.fetchCategoryListFailure.bind(this));
  };
  
  PersonalFinanceManagement_PresentationController.prototype.fetchCategoryListSuccess = function(response){
    this.categoryList = response;
        this.unCategorizedId = response.filter(function(data){
          if(data.categoryName === OLBConstants.UNCATEGORISED){
            return data.categoryId;
          }
        })[0].categoryId;
        this.fetchUnCategorizedTransations();
        this.presentPFM({categoryList : this.categoryList});
  };
  
  PersonalFinanceManagement_PresentationController.prototype.fetchCategoryListFailure = function(error){
    this.presentPFM({onServerDownError : true});
  };
  /**
     * presentPFM : Method for navigate to PersonalFinanceManagement form.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
  PersonalFinanceManagement_PresentationController.prototype.presentPFM = function(data) {
    var navigationManager = applicationManager.getNavigationManager();
    navigationManager.updateForm(data,"frmPersonalFinanceManagement");
    //this.presentUserInterface("frmPersonalFinanceManagement", data);
  };
  PersonalFinanceManagement_PresentationController.prototype.showProgressBar = function(data) {
    var self = this;
    self.presentPFM({
      "showProgressBar": "showProgressBar"
    });
  };
  PersonalFinanceManagement_PresentationController.prototype.hideProgressBar = function(data) {
    var self = this;
    self.presentPFM({
      "hideProgressBar": "hideProgressBar"
    });
  };
  return PersonalFinanceManagement_PresentationController;
});