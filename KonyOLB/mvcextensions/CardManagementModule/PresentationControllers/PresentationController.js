define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {

    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;

    function CardManagement_PresentationController() {
        MDABasePresenter.call(this);
    }
    this.card = "";
      this.action = "";
    inheritsFrom(CardManagement_PresentationController, MDABasePresenter);

    CardManagement_PresentationController.prototype.initializePresentationController = function() {

    };

    
    /**
     * navigateToManageCards - Entry point to Cards Management.
     */
    CardManagement_PresentationController.prototype.navigateToManageCards = function() {
        var viewProperties = {};
        viewProperties.progressBar = true;
        applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
        this.fetchCardsList();
        this.fetchRequiredInfo();
    };


    /**
     * fetchCardsList - Issues a command to fetch all the cards associated with the current user and then presents the user interface.
     */
    CardManagement_PresentationController.prototype.fetchCardsList = function() {
        applicationManager.getCardsManager().fetchCardsList(this.fetchCardsListSuccess.bind(this), this.fetchCardsListFailure.bind(this));
    };

    /**
     * This method is used as the success call back for the fetchCardsList.
     * @param {Object} response - contains the list of cards.
     */
    CardManagement_PresentationController.prototype.fetchCardsListSuccess = function(response) {
        var viewProperties = {};
        viewProperties.progressBar = true;
        viewProperties.cards = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
        this.fetchCardsStatus(response);
    };

    /** 
     * Method used as the failure call back for the fetchCardsListFailure.
     * @param {String} errorMessage - contains the error message for the service failure of fetchCardsList.
     */
    CardManagement_PresentationController.prototype.fetchCardsListFailure = function(errorMessage) {
        var viewProperties = {};
        viewProperties.serverDown = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    /**
     * Issues a command to lock the given card and then presents the user interface.
     * @param {Object, String} - Card object.
     * @param {String} action - contains the action to be performed.
     */
    CardManagement_PresentationController.prototype.lockCard = function(card, action) {
      this.card = card;
      this.action = action;
      var params = {
        "cardId" : card.cardId,
        
      }
        applicationManager.getCardsManager().lockCard(params, this.lockCardSuccess.bind(this), this.lockCardFailure.bind(this));
    };

    /**
     * method used as success call back to lock card.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the response to lock card.
     */
    CardManagement_PresentationController.prototype.lockCardSuccess = function(response) {
       var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "LOCK_CARD",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
           
        }else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
          var viewProperties = {};
        viewProperties.card = this.card;
        viewProperties.actionAcknowledgement = this.action;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
    };

    /**
     * method used as failure call back to lock card.
     * @param {String} errorMessage - contains the errormessage to lock card.
     */
    CardManagement_PresentationController.prototype.lockCardFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Issues a command to Un-lock the given card and then presents the user interface.
     * @param {Object, String} - Card object.
     * @param {String} action - contains the action to be performed.
     */
    CardManagement_PresentationController.prototype.unlockCard = function(card, action) {
      this.card = card;
      this.action = action;
       var params = {
            "cardId": card.cardId,
            

        };
        applicationManager.getCardsManager().unLockCard(params, this.unlockCardSuccess.bind(this), this.unlockCardFailure.bind(this));
    };

    /**
     * method used as success call back to un-lock card.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the response to un-lock card.
     */
    CardManagement_PresentationController.prototype.unlockCardSuccess = function(response) {
       var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "UNLOCK_CARD",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        } else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
          var viewProperties = {};
        viewProperties.card = this.card;
        viewProperties.actionAcknowledgement = this.action;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
        
    };

    /**
     * method used as failure call back to un-lock card.
     * @param {String} errorMessage - contains the errormessage to lock card.
     */
    CardManagement_PresentationController.prototype.unlockCardFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Method used to change pin for the card.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - change pin.
     */
    CardManagement_PresentationController.prototype.changePin = function(params, action) {
      this.card = params.card;
      this.action = action;
        applicationManager.getCardsManager().changePin({
            cardId: params.card.cardId,
            Reason: params.reason,
            notes: params.notes,
            newPin:params.newPin,
           
        }, this.changePinSuccess.bind(this), this.changePinFailure.bind(this));
    };

    /**
     * method used as success call back to change pin
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - change pin.
     * @param {Object} response - contains the response to change pin.
     */
    CardManagement_PresentationController.prototype.changePinSuccess = function(response) {
      var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "CHANGE_PIN_DEBIT",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
          var viewProperties = {};
        viewProperties.card = this.card;
        viewProperties.actionAcknowledgement = this.action;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
        
    };

    /**
     * method used as failure call back to change pin
     * @param {String} errorMessage - contains the errormessage to change pin.
     */
    CardManagement_PresentationController.prototype.changePinFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * reportLost - Issues a command to report a lost card and then presents the user interface.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - report lost card.
     */
    CardManagement_PresentationController.prototype.reportLost = function(params, action) {
      this.card = params.card;
      this.action = action;
        applicationManager.getCardsManager().reportLost({
            cardId: params.card.cardId,
            Reason: params.Reason,
            notes: params.notes,
         
        }, this.reportLostSuccess.bind(this), this.reportLostFailure.bind(this));
    };

    /**
     * method used as success call back to report lost card.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - report lost card.
     * @param {Object} response - contains the response to report lost card.
     */
    CardManagement_PresentationController.prototype.reportLostSuccess = function( response) {
       var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "REPORT_LOST",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        } else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
          var viewProperties = {};
          viewProperties.card = this.card;
          viewProperties.actionAcknowledgement = this.action;
          viewProperties.progressBar = false;
          applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
    };

    /**
     * method used as failure call back to report lost card.
     * @param {String} errorMessage - contains the errormessage to report lost card.
     */
    CardManagement_PresentationController.prototype.reportLostFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    CardManagement_PresentationController.prototype.cancelCard = function(params,action){
      this.card = params.card;
      this.action = action;
        applicationManager.getCardsManager().updateCardStatus({
            cardId: params.card.cardId,
            Reason: params.Reason,
            notes: params.notes,
            Action : 'Cancel',
          
        }, this.cancelCardSuccess.bind(this), this.cancelCardFailure.bind(this));
    };

    CardManagement_PresentationController.prototype.cancelCardSuccess = function(response) {
       var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "CANCEL_CARD",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
          var viewProperties = {};
        viewProperties.card = this.card;
        viewProperties.actionAcknowledgement = this.action;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
        
    };

    CardManagement_PresentationController.prototype.cancelCardFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * sendSecureAccessCode - Issues a command to send Secure Access Code and then presents the user interface
     * @param {Object} params - Params object.
     * @param {String} action - contains the action name 
     */
    CardManagement_PresentationController.prototype.sendSecureAccessCode = function(params, action) {
        applicationManager.getUserPreferencesManager().SendSecureAccessCode(this.sendSecureAccessCodeSuccess.bind(this, params, action), this.sendSecureAccessCodeFailure.bind(this));
    };

    /**
     * method used as the success call back for the send secure access code service.
     * @param {Object} params - contains the parameters for the updation.
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the response to the send secure access service.
     */
    CardManagement_PresentationController.prototype.sendSecureAccessCodeSuccess = function(params, action, response) {
        var viewProperties = {};
        viewProperties.params = params;
        viewProperties.action = action;
        viewProperties.secureAccessCode = true;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * method used as the failure call back for the send secure access code service.
     * @param {String} errormessage - contains the error message.
     */
    CardManagement_PresentationController.prototype.sendSecureAccessCodeFailure = function(errormessage) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.serverError = errormessage;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * method to verify the secure access code.
     * @param {Object} params - contains the pin.
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the response to the verify secure access service.
     */
    CardManagement_PresentationController.prototype.verifySecureAccessCode = function(params, action) {
        applicationManager.getUserPreferencesManager().VerifySecureAccessCode({
            Otp: params.enteredAccessCode
        }, this.verifySecureAccessCodeSuccess.bind(this, params, action), this.verifySecureAccessCodeFailure.bind(this, params, action));
    };

    /**
     * method used as success call back to verify the secure access code.
     * @param {Object} params - contains the pin.
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the response to the verify secure access service.
     */
    CardManagement_PresentationController.prototype.verifySecureAccessCodeSuccess = function(params, action) {
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.LockCard")) {
                this.lockCard(params.card, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin")) {
                this.changePin(params, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")) {
                this.unlockCard(params.card, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost")) {
                this.reportLost(params, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard")) {
                this.replaceCard(params, action);
            }else if (action === kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard")) {
                this.cancelCard(params, action);
            }else if(action === "Offline_Change_Pin"){
               this.createCardRequest(params, action);
            }
    };

    /**
     * method used as failure call back to verify the secure access code.
     * @param {Object} params - contains the pin.
     * @param {String} action - contains the action to be performed.     
     * @param {Object} response - contains the errormessage to the verify secure access service.
     */
    CardManagement_PresentationController.prototype.verifySecureAccessCodeFailure = function(params, action, response) {
        if(response.isServerUnreachable) {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }else{
           var viewProperties = {};
           viewProperties.card = params.card;
           viewProperties.action = action;
           viewProperties.incorrectSecureAccessCode = true;
           applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
    };

    /**
     * replaceCard - Issues a command to replace card and then presents the user interface.
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - replace card.
     */
    CardManagement_PresentationController.prototype.replaceCard = function(params, action) {
      this.card = params.card;
      this.action = action;
        applicationManager.getCardsManager().replaceCard({
            cardId: params.card.cardId,
            reason : params.reason,
            
        }, this.replaceCardSuccess.bind(this), this.replaceCardFailure.bind(this));
    };

    /**
     * method used as success call back to replace card
     * @param {Object} card - contains card object
     * @param {String} action - contains the action to be - replace card
     * @param {Object} response - contains the response to replace card
     */
    CardManagement_PresentationController.prototype.replaceCardSuccess = function(response) {
       var mfaManager = applicationManager.getMFAManager();
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "REPLACE_CARD",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
            var viewProperties = {};
            viewProperties.card = this.card;
            viewProperties.actionAcknowledgement = this.action;
            viewProperties.progressBar = false;
            applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
        }
      
    };

    /**
     * method used as failure call back to replace card
     * @param {String} errorMessage - contains the errormessage to replace card
     */
    CardManagement_PresentationController.prototype.replaceCardFailure = function(errorMessage) {
      applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        viewProperties.progressBar = false;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Fetch already created Travel Notifications for user
     */
    CardManagement_PresentationController.prototype.fetchTravelNotifications = function() {
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "lastNinetyDays ": 1
        };
        applicationManager.getCardsManager().fetchNotificationsList(params, this.fetchTravelNotificationsSuccess.bind(this), this.fetchTravelNotificationsFailure.bind(this));
    };

    /**
     * Method used as the success call back for the success call back for fetchTravelNotificationsSuccess.
     * @param {Object} response - contains the travel notifications object.
     */
    CardManagement_PresentationController.prototype.fetchTravelNotificationsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "travelNotificationsList": response
        }, "frmCardManagement");
    };

    /**
     * Method used as the failure call back for the fetchTravelNotificationsFailure.
     * @param {String} errorMessage - contains the error message for the failure of fetchTravelNotifications.
     */
    CardManagement_PresentationController.prototype.fetchTravelNotificationsFailure = function(errorMessage) {
        applicationManager.getNavigationManager().updateForm({
            "serverError": errorMessage
        }, "frmCardManagement");
    };

    /**
     * Method used for new travel plan in cards.
     * @param {Object} scope - contains isEditFLow flag.
     */
    CardManagement_PresentationController.prototype.AddNewTravelPlan = function(scope) {
        var self = this;
        var data = {};
        if (scope)
            data.isEditFlow = scope.isEditFlow;
        data.AddNewTravelPlan = 'AddNewTravelPlan';
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'getCountryList'),
                asyncManager.asyncItem(userPrefManager, 'getStatesList'),
                asyncManager.asyncItem(userPrefManager, 'getCityList'),
            ],
            this.AddNewTravelPlanCompletionCallBack.bind(this, data)
        );
    };

    /**
     * Method used as completion call back for the async call to getCountryList, getStatesList, getCityList.
     * @param {Object} data - contains the addNewTravelPlan property.
     * @param {Object} syncResponseObject - contains the async manager respnose.
     */
    CardManagement_PresentationController.prototype.AddNewTravelPlanCompletionCallBack = function(data, syncResponseObject) {
        if (syncResponseObject.isAllSuccess()) {
            data.country = syncResponseObject.responses[0].data.records;
            data.states = syncResponseObject.responses[1].data.records;
            data.city = syncResponseObject.responses[2].data.records;
            applicationManager.getNavigationManager().updateForm(data, "frmCardManagement")
        } else {
            applicationManager.getNavigationManager().updateForm({
                "hideProgressBar": true
            });
            CommonUtilities.showServerDownScreen();
        }
    };

    /**
     * Method to create new travel notification.
     * @param {Object} notificationObj - contains the notificaion data.
     */
    CardManagement_PresentationController.prototype.createTravelNotification = function(notificationObj) {
        var formatUtil = applicationManager.getFormatUtilManager(); 
        var tavelNotification = {
            "Destinations": JSON.stringify(notificationObj.locations),
            "Channel_id": OLBConstants.Channel,
            "StartDate": formatUtil.convertToUTC(notificationObj.fromDate),
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "additionNotes": notificationObj.notes,
            "EndDate": formatUtil.convertToUTC(notificationObj.toDate),
            "phonenumber": notificationObj.phone,
            "Cards": JSON.stringify(notificationObj.selectedcards),
        };
        applicationManager.getCardsManager().createTravelNotification(tavelNotification, this.createTravelNotificationSuccess.bind(this), this.createTravelNotificationFailure.bind(this));
    };

    /**
     * Method used as the success call back for the createTravelNotification service call.
     * @param {Object} response - contains the response for the createTravelNotification
     */
    CardManagement_PresentationController.prototype.createTravelNotificationSuccess = function(response) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.notificationAcknowledgement = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Method used as the failure call back for the createTravelNotification service call.
     * @param {Object} notificationObj - contains the errorMessage for the createTravelNotification service call.
     */
    CardManagement_PresentationController.prototype.createTravelNotificationFailure = function(errorMessage) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.serverError = errorMessage.errorMessage; //errorMessage.serverErrorRes && errorMessage.serverErrorRes.dbpErrMsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Issues a command to fetch travel status  associated with the current user's cards and then presents the user interface.
     * @param {cards} - array of card ids
     */
    CardManagement_PresentationController.prototype.fetchCardsStatus = function(cards) {
        var self = this;
        var cardsViewModel = {};
        var cardsViewArray = [];
        cards.forEach(function(card) {
            cardsViewArray.push(card.maskedCardNumber);
        });
        cardsViewArray = JSON.stringify(cardsViewArray);
        cardsViewArray = cardsViewArray.replace(/"/g, "'");
        var context = {
            "CardNumbers": cardsViewArray,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };

        applicationManager.getCardsManager().fetchCardStatus(context, this.fetchCardsStatusSuccess.bind(this, cards), this.fetchCardsStatusFailure.bind(this));
    };


    /**
     * Method used as the success call back for the createTravelNotification service call.
     * @param {Object} response - contains the response for the createTravelNotification
     */
    CardManagement_PresentationController.prototype.fetchCardsStatusSuccess = function(cards, response) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.travelStatus = {
            "status": response.CardStatus,
            "data": cards
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };

    /**
     * Method used as the failure call back for the createTravelNotification service call.
     * @param {Object} notificationObj - contains the errorMessage for the createTravelNotification service call.
     */
    CardManagement_PresentationController.prototype.fetchCardsStatusFailure = function(errorMessage) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.serverDown = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmCardManagement");
    };
  
    /**
  * Method used to fetch user Addresses from userPreferences Manager.
  * @returns {Object} - returns the list of user addresses.
  */
    CardManagement_PresentationController.prototype.fetchUserAddresses = function() {
        return applicationManager.getUserPreferencesManager().getEntitlementAddresses();
    };
  
  /**
  * Method used to fetch user Phone Numbers from userPreferences Manager.
  * @returns {Object} - returns the list of user Phone numbers.
  */
    CardManagement_PresentationController.prototype.fetchUserPhoneNumbers = function() {
        return applicationManager.getUserPreferencesManager().getEntitlementPhoneNumbers();
    };

    /**
  * Method used to fetch user Email-Ids from userPreferences Manager.
  * @returns {Object} - returns the list of user Email-Ids.
  */
    CardManagement_PresentationController.prototype.fetchUserEmailIds = function() {
        return applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
    };

    /**
  * Method used to fetch user name from userPreferences Manager.
  * @returns {String} - returns user name.
  */
    CardManagement_PresentationController.prototype.getUserName = function() {
        return applicationManager.getUserPreferencesManager().getCurrentUserName();
    };

    /**
  * Method used to fetch the required info like user addresses, phone numbers and email ids.
  */
    CardManagement_PresentationController.prototype.fetchRequiredInfo = function() {
        this.fetchUserPhoneNumbers();
        this.fetchUserEmailIds();
        this.fetchUserAddresses();
    }

  /**
  * Method used to delete Travel Notification.
  * @param {Object} requestID - contains the travel notification Id.
  */
    CardManagement_PresentationController.prototype.deleteNotification = function(requestID) {
        applicationManager.getCardsManager().deleteNotification({
            request_id: requestID
        }, this.deleteNotificationSuccess.bind(this), this.deleteNotificationFailure.bind(this));
    };

    /**
  * Method used as the success call back for the delete travel notification. 
  * @param {Object} response - contains the response to delete travel notification. 
  */
    CardManagement_PresentationController.prototype.deleteNotificationSuccess = function(response) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.notificationDeleted = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };
  
    /**
  * Method used as the failure call back for the delete travel notification.
  * @param {String} errorMessage - contains the error message.
  */
    CardManagement_PresentationController.prototype.deleteNotificationFailure = function(errorMessage) {
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.serverError = errorMessage;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    /**
  * Method used to create a card request.
  * @param {Object} params - contains the pin.
    * @param {String} action - contains the action to be performed.
  */
    CardManagement_PresentationController.prototype.createCardRequest = function(params, action) {
      this.card = params;
      this.action = action;
        applicationManager.getCardsManager().createCardRequest(params, this.createCardRequestSuccess.bind(this), this.createCardRequestFailure.bind(this));
    };

    /**
  * Method used as the failure call back for the create card request service.
  * @param {String} errorMessage - contains the error message.
  */
    CardManagement_PresentationController.prototype.createCardRequestFailure = function(errorMessage) {
        applicationManager.getNavigationManager().navigateTo("frmCardManagement");    
        var viewProperties = {};
        viewProperties.serverError = errorMessage;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    /**
  * Method used as the success call back for the create card request.
  * @param {Object} response - contains the response to the create card request.
  */
    CardManagement_PresentationController.prototype.createCardRequestSuccess = function(response) {
        var self = this;
       var mfaManager = applicationManager.getMFAManager();
        var viewProperties = {};
       if (response.MFAAttributes && response.MFAAttributes.isMFARequired) {
            var mfaJSON = {
                "serviceName": mfaManager.getServiceId(),
                "flowType": "CHANGE_PIN_CREDIT",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
        }else{
          applicationManager.getNavigationManager().navigateTo("frmCardManagement");
           if (this.action === "Offline_Change_Pin") {
            viewProperties.card = this.card.card;
            viewProperties.actionAcknowledgement = this.action;
            applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
        } else if (this.action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard")) {
            self.replaceCard(this.card, this.action);
        }
        }
       
    };

    /**
     * getEligibleCards - Issues a command to get active cards and then presents the user interface.
     */
    CardManagement_PresentationController.prototype.getEligibleCards = function() {
        applicationManager.getCardsManager().fetchActiveCards(this.getEligibleCardsSuccess.bind(this), this.getEligibleCardsFailure.bind(this));
    };

  /**
  * Method used as the sucess call back for the getEligibleCards service.
  * @param {Object} data - contains the list of eligible cards.
  */
    CardManagement_PresentationController.prototype.getEligibleCardsSuccess = function(data) {
        var viewProperties = {};
        viewProperties.eligibleCards = data;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    /**
  * Method used as the failure call back for the getEligibleCards service.
  * @param {String} errorMessage - contains the error message.
  */
    CardManagement_PresentationController.prototype.getEligibleCardsFailure = function(errorMessage) {
        var viewProperties = {};
        viewProperties.serverDown = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };
  
    /**
     * updateTravelNotification - Issues a command to update travel notification.
     * @param {Object} notificationObj - Params Object
     */
    CardManagement_PresentationController.prototype.updateTravelNotifications = function(notificationObj) {
        var self = this;
        var formatUtil = applicationManager.getFormatUtilManager(); 
        var notificationData = {
            Channel_id: OLBConstants.Channel,
            request_id: notificationObj.requestId,
            Destinations: JSON.stringify(notificationObj.locations),
            userName: applicationManager.getUserPreferencesManager().getCurrentUserName(),
            additionNotes: notificationObj.notes,
            phonenumber: notificationObj.phone,
            Cards: JSON.stringify(notificationObj.selectedcards),
            StartDate: formatUtil.convertToUTC(notificationObj.fromDate),
            EndDate: formatUtil.convertToUTC(notificationObj.toDate),
        };
        applicationManager.getCardsManager().updateTravelNotifications(notificationData, this.updateTravelNotificationSuccess.bind(this), this.updateTravelNotificationFailure.bind(this));    };

    /**
     * updateTravelNotification - Issues a command to update travel notification.
     * @param {Object} - Params Object
     */
    CardManagement_PresentationController.prototype.updateTravelNotificationSuccess = function(response) {
        var self = this;
        var viewProperties = {};
        viewProperties.notificationAcknowledgement = response;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };
  
    /**
     * updateTravelNotification - Issues a command to update travel notification.
     * @param {Object} reponse - Params Object
     */
    CardManagement_PresentationController.prototype.updateTravelNotificationFailure = function(errorOject) {
        var self = this;
        var viewProperties = {};
        viewProperties.progressBar = false;
        viewProperties.serverError = errorOject.serverErrorRes && errorOject.serverErrorRes.dbpErrMsg;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    /**
     * fetchSecurityQuestions - Issues a command to fetch Security Questions and then presents the user interface
     * @param {Object} - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.fetchSecurityQuestions = function(card, action) {
        applicationManager.getUserPreferencesManager().fetchSecurityQuestions({userName:applicationManager.getUserPreferencesManager().getCurrentUserName()}, this.fetchSecurityQuestionsSuccess.bind(this, card, action), this.fetchSecurityQuestionsFailure.bind(this, card, action));
    };

  
    /**
     * Method to be used as success call back for the fetch security questions.
     * @param {Object} - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.fetchSecurityQuestionsSuccess = function(card, action, response) {
        var viewProperties = {};
        viewProperties.card = card;
        viewProperties.action = action;
        viewProperties.securityQuestions = response.records;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

  
    /**
     * Method to be used as failure call back for the fetch security questions.
     * @param {Object} - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.fetchSecurityQuestionsFailure = function(card, action, response) {
        var viewProperties = {};
        viewProperties.serverDown = true;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };
  
    /**
     * verifySecurityQuestionAnswers - Issues a command to verify Security Questions and then presents the user interface
     * @param {Object} params - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.verifySecurityQuestionAnswers = function(params, action) {
        var self = this;
        var questionAnswerParams = {};
        questionAnswerParams.userName = applicationManager.getUserPreferencesManager().getCurrentUserName();
        questionAnswerParams.securityQuestions = JSON.stringify(params.questionAnswers);
        applicationManager.getUserPreferencesManager().verifySecurityQuestions(questionAnswerParams, this.verifySecurityQuestionAnswersSuccess.bind(this,params, action), this.verifySecurityQuestionAnswersFailure.bind(this, params, action));
    };


  
    /**
     * verifySecurityQuestionAnswers - Issues a command to verify Security Questions and then presents the user interface
     * @param {Object} params - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.verifySecurityQuestionAnswersSuccess = function(params, action, response) {
            var self  = this;
          if (response.verifyStatus === "true") {
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.LockCard")) {
              self.lockCard(params.card, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin")) {
              self.changePin(params, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")) {
              self.unlockCard(params.card, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost")) {
              self.reportLost(params, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard") || action === "Offline_Change_Pin") {
              self.createCardRequest(params, action);
            } else if (action === kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard")) {
                this.cancelCard(params, action);
            }
          } else if (response.verifyStatus === "false") {
            var viewProperties = {};
            viewProperties.card = params.card;
            viewProperties.action = action;
            viewProperties.incorrectSecurityAnswers = true;
            applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
          }
    };


  
    /**
     * verifySecurityQuestionAnswers - Issues a command to verify Security Questions and then presents the user interface
     * @param {Object} params - Card object
     * @param {String} action - contains the action to be performed.
     * @param {Object} response - contains the service response for the fetch security questions.
     */
    CardManagement_PresentationController.prototype.verifySecurityQuestionAnswersFailure = function(params, action,errorMessage) {
        var viewProperties = {}
        viewProperties.serverError = errorMessage.errorMessage || errorMessage;
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
    };

    CardManagement_PresentationController.prototype.showPrintPage = function(data){
        var scopeObj = this;
        data.printKeyValueGroupModel.printCallback = function(){
               scopeObj.showAcknowlegeScreenOnPrintCancel(); 
        }
        applicationManager.getNavigationManager().navigateTo('frmPrintTransfer');
        applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
    };

    CardManagement_PresentationController.prototype.showAcknowlegeScreenOnPrintCancel = function(){
        var viewProperties = {};
        viewProperties.isPrintCancelled = true;
        applicationManager.getNavigationManager().navigateTo("frmCardManagement");
        applicationManager.getNavigationManager().updateForm(viewProperties, 'frmCardManagement');
      
    };
    return CardManagement_PresentationController;
});