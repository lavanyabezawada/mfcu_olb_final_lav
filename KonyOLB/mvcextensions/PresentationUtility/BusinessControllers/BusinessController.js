/**
*@module PresentationUtility
*/
define(['OLBConstants'], function (OLBConstants) { 
  /**
   * PresentationUtility consists of all utilities anf wrapper functions related to Presentation 
   *@alias module:PresentationUtility
   *@class
*/ 

  function PresentationUtility() { 
    /*
  A variable maintained to store row index globally on swipe 
  Note:It is maintained to delete on swipe till platform fix issue related to segment
*/

    /**@member {integer}  number to maintain index for swipe*/

    this.rowIndexforSwipe=-1;

  } 

  inheritsFrom(PresentationUtility, kony.mvc.Business.Delegator); 

  PresentationUtility.prototype.initializeBusinessController = function() { 

  }; 


  /**
  * A wrapper on kony alert message for further use
  * @param {JSON} basicConfig - same as basicConfig in kony.ui.Alert
  * @param {JSON} pspConfig - same as pspConfig in kony.ui.Alert
*/
  PresentationUtility.prototype.showAlertMessage =  function(basicConfig, pspConfig ){
    if(kony.os.deviceInfo().name === "android"){
      basicConfig.alertIcon="transparentbox.png";
    }
    kony.ui.Alert(basicConfig, pspConfig);
  }


  /**
  * Returns value of given i18n key in device's locale
  * @param {String} i18n Key - an i18n key to look out for
  * @param {String} noKeyValue(optonal) - returns this when lookout failed 
  * @returns {String} - value associated to that key if its not there noKeyValue is returned
  */
  PresentationUtility.prototype.getStringFromi18n = function (stringValue,noKeyValue)
  {
    return  kony.i18n.getLocalizedString(stringValue) ? kony.i18n.getLocalizedString(stringValue):noKeyValue;
  }



  /**
  * A UI function to show loading indicator
*/
  PresentationUtility.prototype.showLoadingScreen = function(){
    kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
  }

  /**
  * A UI function to dismiss loading indicator
*/
  PresentationUtility.prototype.dismissLoadingScreen = function(){
    kony.application.dismissLoadingScreen();
  }


  /**
  * Returns the controller of the requested form
  * @param {String} formname - Name of the form for which the controller is required
  * @param {Boolean} isForm - expects true if the requested controller is of a form 
  * @returns {object} - returns the requested controller(kony.mvc.MDAFormController)
  */
  PresentationUtility.prototype.getController = function(formname,isForm){
    var controller = _kony.mvc.GetController(formname, isForm);
    return controller;               
  };



  PresentationUtility.prototype.MFA = {
    navigateBasedOnMFAType : function(){
      var mfaManager = applicationManager.getMFAManager();
      var MFAType = mfaManager.getMFAType();
      switch(MFAType){
        case OLBConstants.MFA_FLOW_TYPES.SECURE_ACCESS_CODE : 
          this.navigateToOtpScreen();
          break;
        case OLBConstants.MFA_FLOW_TYPES.SECURITY_QUESTIONS : 
          this.navigateToSecurityQuestion();
          break;
      }
    },
    getMFAFlowType : function(){
      return applicationManager.getMFAManager().getMFAFlowType();
    },
    navigateToOtpScreen : function() {
      var mfaManager = applicationManager.getMFAManager();
      var communicationType = mfaManager.getCommunicationType();
      switch(communicationType){
        case OLBConstants.MFA_FLOW_TYPES.DISPLAY_ALL : 
          this.navigateToPhoneEmailScreen();
          break;
        case OLBConstants.MFA_FLOW_TYPES.DISPLAY_NO_VALUE : 
          this.navigateToDefaultMFAScreen();
          break;
        case OLBConstants.MFA_FLOW_TYPES.DISPLAY_PRIMARY :
          this.navigateToPrimaryMFAScreen();
          break;
      }

    },
    navigateToSecurityQuestion: function() {
      var mfaManager = applicationManager.getMFAManager();
       var MFAResponse = mfaManager.getMFAResponse();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
          applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                securityQuestions: MFAResponse
            }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                securityQuestions: MFAResponse
            }, "frmMFATransactions");
          break;
      }
        },
    
    navigateToPhoneEmailScreen : function() {
    var mfaManager = applicationManager.getMFAManager();
    var MFAResponse = mfaManager.getMFAResponse();
    var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
         applicationManager.getNavigationManager().updateForm({
            phoneEmailScreen: MFAResponse.MFAAttributes.customerCommunication
      }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
      applicationManager.getNavigationManager().updateForm({
        phoneEmailScreen: MFAResponse.MFAAttributes.customerCommunication
      }, "frmMFATransactions");
          break;
      }  
    },
    
    navigateToDefaultMFAScreen: function() {
       var mfaManager = applicationManager.getMFAManager();
         var MFAResponse = mfaManager.getMFAResponse();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                defaultPhoneEmailScreen: MFAResponse.MFAAttributes
            }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                defaultPhoneEmailScreen: MFAResponse.MFAAttributes
            }, "frmMFATransactions");
          break;
      }  
        },

    navigateToPrimaryMFAScreen : function() {
       var mfaManager = applicationManager.getMFAManager();
       var MFAResponse = mfaManager.getMFAResponse();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                primaryPhoneEmailScreen: MFAResponse.MFAAttributes
            }, "frmMFAPreLogin");
          break;
        default:
         applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                primaryPhoneEmailScreen: MFAResponse.MFAAttributes
            }, "frmMFATransactions");
          break;
      }  
    },
    
    showMFAOTPScreen : function(){
       var mfaManager = applicationManager.getMFAManager();
        var MFAResponse = mfaManager.getMFAResponse();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
           applicationManager.getNavigationManager().updateForm({
            otpReceived: MFAResponse.MFAAttributes
          }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
         applicationManager.getNavigationManager().updateForm({
          otpReceived: MFAResponse.MFAAttributes
        }, "frmMFATransactions");
          break;
      }  
    },
    
    cancelMFAFlow:function(){
      var mfaManager = applicationManager.getMFAManager();
     var flowType= mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS : 
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.showTransferScreen();
          break;
        case OLBConstants.MFA_FLOW_TYPES.WIRE_TRANSFERS :
           var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "makeTransfer"
         });
          break;
         
          case OLBConstants.MFA_FLOW_TYPES.SINGLE_BILL_PAY :
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.showBillPayData();
            break;
          case OLBConstants.MFA_FLOW_TYPES.PAY_A_PERSON :
           var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
           p2pModule.presentationController.showPayAPerson("sendMoneyTab");
            break;
          
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_EURO : 
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.showTransferScreen();
          break;
           case OLBConstants.MFA_FLOW_TYPES.BULK_BILL_PAY :
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.showBillPayData();
            break;
           case OLBConstants.MFA_FLOW_TYPES.ONE_TIME_WIRE_TRANSFERS :
           var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "makeTransfer"
         });
          break;
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_UPDATE :
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
          transferModule.presentationController.editTransactionSuccess();
          break;
        case "UPDATE_USERNAME" :
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.showUserNameAndPassword();
          break;
        case "UPDATE_PASSWORD" :
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.showUserNameAndPassword();
          break;
        case "SECURITYQUESTION_RESET":
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.checkSecurityQuestions();
          break;
        case "LOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
        case "UNLOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
        case "CHANGE_PIN_DEBIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;

        case "REPORT_LOST" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
        case "CANCEL_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
        case "REPLACE_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
            case "CHANGE_PIN_CREDIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.navigateToManageCards();
          break;
      }
    },

    navigateToTransactionScreen: function(response) {
      var mfaManager = applicationManager.getMFAManager();
      var flowType = mfaManager.getMFAFlowType();
      switch (flowType) {
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS:
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
          transferModule.presentationController.createTransferErrorCallback(response);
          break;
        case  OLBConstants.MFA_FLOW_TYPES.WIRE_TRANSFERS:
          var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
          wireTransferModule.presentationController.showMakeTransferError(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.SINGLE_BILL_PAY:
          var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
          billPayModule.presentationController.singleBillPayFailureCallBack(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.PAY_A_PERSON:
          var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
          p2pModule.presentationController.createP2PSendMoneyFailure(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_EURO:
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
          transferModule.presentationController.createTransferErrorCallback(response);
          break;
        case  OLBConstants.MFA_FLOW_TYPES.BULK_BILL_PAY:
          var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
          billPayModule.presentationController.createBulkPaymentsErrorCallBack(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.ONE_TIME_WIRE_TRANSFERS:
          var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
          wireTransferModule.presentationController.showOneTimeTransferError(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_UPDATE:
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
          transferModule.presentationController.editTransactionError(response);
          break;
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA:
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
          authModule.presentationController.onLoginFailure(response);
          break;
        case "UPDATE_USERNAME":
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.updateUsernameFailure(response);
          break;
        case "UPDATE_PASSWORD":
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.updatePasswordFailure(response);
          break;
        case "SECURITYQUESTION_RESET":
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.updateSecurityQuestionsFailure(response);
          break;
        case "LOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.lockCardFailure(response);
          break;
        case "UNLOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.unlockCardFailure(response);
          break;
        case "CHANGE_PIN_DEBIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.changePinFailure(response);
          break;

        case "REPORT_LOST" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.reportLostFailure(response);
          break;
        case "CANCEL_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.cancelCardFailure(response);
          break;
        case "REPLACE_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.replaceCardFailure(response);
          break;
            case "CHANGE_PIN_CREDIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.createCardRequestFailure(response);
          break;
      }
    },

    verifySecurityQuestions:function(data){
      var mfaManager = applicationManager.getMFAManager();
      var inputparams = {
        "MFAAttributes" : {
          "serviceName": mfaManager.getServiceId(),
          "serviceKey" : mfaManager.getServicekey(),
          "securityQuestions" : data
        }
      };
      mfaManager.verifySecurityQuestions(inputparams);
    },
    

    
     resendOTP:function(data){
       var mfaManager = applicationManager.getMFAManager();
       var inputparams = {
      "MFAAttributes" : {
       "serviceName": mfaManager.getServiceId(),
        "serviceKey" : mfaManager.getServicekey(),
        "OTP" : data
      }
    };
       mfaManager.resendOTP(inputparams);
       },
    
    requestOTP:function(data){
       var mfaManager = applicationManager.getMFAManager();
       var inputparams = {
      "MFAAttributes" : {
       "serviceName": mfaManager.getServiceId(),
        "serviceKey" : mfaManager.getServicekey(),
        "OTP" : data
      }
    };
       mfaManager.requestOTP(inputparams);
       },
    
    
  verifyOTP:function(data){
     var mfaManager = applicationManager.getMFAManager();
       var inputparams = {
      "MFAAttributes" : {
       "serviceName": mfaManager.getServiceId(),
        "serviceKey" : mfaManager.getServicekey(),
        "OTP" : data
      }
    };
       mfaManager.verifyOTP(inputparams);
  },
    
    enteredIncorrectOTP:function(error){
       var mfaManager = applicationManager.getMFAManager();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                otpIncorrect: error
            }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                otpIncorrect: error
            }, "frmMFATransactions");
          break;
      }  
    },
    
    
    enteredIncorrectAnswer: function(error){
      var mfaManager = applicationManager.getMFAManager();
      var flowType = mfaManager.getMFAFlowType();
       switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                securityAnswerIncorrect: error
            }, "frmMFAPreLogin");
          break;
        default:
          applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                securityAnswerIncorrect: error
            }, "frmMFATransactions");
          break;
      }  
    },
    navigateToAckScreen:function(response){
      var mfaManager = applicationManager.getMFAManager();
       var userpreferencesManager =  applicationManager.getUserPreferencesManager();
      var flowType = mfaManager.getMFAFlowType();
      var serviceKey = mfaManager.getServicekey();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.TRANSFERS : 
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.createTransferSuccessCallback(response);
          break;
           case OLBConstants.MFA_FLOW_TYPES.WIRE_TRANSFERS :
           var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
            wireTransferModule.presentationController.showMakeTransferAcknowledgement(response);
             break;
        case OLBConstants.MFA_FLOW_TYPES.SINGLE_BILL_PAY : 
             var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
             billPayModule.presentationController.singleBillPaySuccessCallBack(response);
          break;
           case OLBConstants.MFA_FLOW_TYPES.PAY_A_PERSON :
           var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
           p2pModule.presentationController.createP2PSendMoneySuccess(response);
            break;
       case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_EURO : 
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.createTransferSuccessCallback(response);
          break;
         case OLBConstants.MFA_FLOW_TYPES.BULK_BILL_PAY : 
             var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
             billPayModule.presentationController.createBulkPaymentsSuccessCallBack(response);
          break;
           case OLBConstants.MFA_FLOW_TYPES.ONE_TIME_WIRE_TRANSFERS :
           var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
            wireTransferModule.presentationController.showOneTimeTransferAcknowledgement(response);
             break;
          case OLBConstants.MFA_FLOW_TYPES.TRANSFERS_UPDATE :
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.editTransactionSuccess(response);
             break;
         case OLBConstants.MFA_FLOW_TYPES.LoginMFA : 
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.onLoginMFA(serviceKey);
          break;
        case "UPDATE_USERNAME" : 
           var userName = mfaManager.getUserName();
           userpreferencesManager.setCurrentUserName(userName);
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
           authModule.presentationController.doLogout({
              		"text" : "username",
                    "action": "userNamePasswordSuccessfullyChanged"
                });
          break;
        case "UPDATE_PASSWORD" : 
           var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
           authModule.presentationController.doLogout({
             		 "text" : "password",
                    "action": "userNamePasswordSuccessfullyChanged"
                });
          break;
        case "SECURITYQUESTION_RESET" : 
          var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          profileModule.presentationController.updateSecurityQuestions();
          break;

        case "LOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.lockCardSuccess(response);
          break;
        case "UNLOCK_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.unlockCardSuccess(response);
          break;
        case "CHANGE_PIN_DEBIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.changePinSuccess(response);
          break;

        case "REPORT_LOST" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.reportLostSuccess(response);
          break;
        case "CANCEL_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.cancelCardSuccess(response);
          break;
        case "REPLACE_CARD" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.replaceCardSuccess(response);
          break;
           case "CHANGE_PIN_CREDIT" : 
          var cardManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
          cardManagementModule.presentationController.createCardRequestSuccess(response);
          break;
      }
      
    },
    
    setSecureCodeScreen:function(response){
        var mfaManager = applicationManager.getMFAManager();
        var MFAResponse = mfaManager.getMFAResponse();
       if (MFAResponse.MFAAttributes.communicationType === "DISPLAY_NO_VALUE") {
                applicationManager.getPresentationUtility().MFA.navigateToDefaultMFAScreen(response);
            }else if (MFAResponse.MFAAttributes.communicationType === "DISPLAY_ALL"){
                applicationManager.getPresentationUtility().MFA.showMFAOTPScreen(response);
            } else {
                applicationManager.getPresentationUtility().MFA.navigateToPrimaryMFAScreen(response);
            }
    },

  
  getServiceIdBasedOnDisplayName : function(displayName){
     var configManager = applicationManager.getConfigurationManager();
     var mfaManager = applicationManager.getMFAManager();
        var services = configManager.getServicesListForUser();
        for (var i = 0; i < services.length; i++) {
            if(services[i].displayName === displayName){
          mfaManager.setServiceId(services[i].serviceId);              

            }
        }
  },
    
    mfaOTPError:function(error){
      
       var mfaManager = applicationManager.getMFAManager();
       var flowType = mfaManager.getMFAFlowType();
      switch(flowType){
        case OLBConstants.MFA_FLOW_TYPES.LoginMFA :  
           applicationManager.getNavigationManager().navigateTo("frmMFAPreLogin");
            applicationManager.getNavigationManager().updateForm({
                otpRequestFailed: error
            }, "frmMFAPreLogin");
          break;
        default:
         applicationManager.getNavigationManager().navigateTo("frmMFATransactions");
            applicationManager.getNavigationManager().updateForm({
                otpRequestFailed: error
            }, "frmMFATransactions");
          break;
      }  
       
    },
    
    getDisplayNameForTransfer:function(key){
      var displayName = "";
      if(key === "OWN_INTERNAL_ACCOUNTS"){
        displayName  = "KonyBankAccountsTransfer";
      }
      else if (key === "OTHER_INTERNAL_MEMBER"){
        displayName = "OtherKonyAccountsTransfer";
      }
      else if (key === "OTHER_EXTERNAL_ACCOUNT"){
        displayName = "OtherBankAccountsTransfer";
      }
      else if(key === "INTERNATIONAL_ACCOUNT"){
        displayName = "InternationalAccountsTransfer";
      }else if(key === "Domestic"){
         displayName = "DomesticWireTransfer";
      }else{
        displayName = "InternationalWireTransfer";
      }
      
      return displayName;
    },
    
    
  };

  return PresentationUtility;

});