/**
* StopPayments Presenation to handle all check related functionalties
* @module StopPaymentsPresentationController
*/

define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {

    /**
     * Stop payements Presenation to handle all check related activities
     * @class
     * @alias module:StopPaymentsPresentationController
     */
    function StopPaymentsPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
    }

    inheritsFrom(StopPaymentsPresentationController, kony.mvc.Presentation.BasePresenter);

    /**
    * initializePresentationController - Method to intialize Presentation controller data , called by constructor
    * @param {Object} dataInputs - dataInputs to configure Presentation controller. (optional) - useful when we need to customize.
    */
    StopPaymentsPresentationController.prototype.initializePresentationController = function (dataInputs) {
        var scopeObj = this;
        scopeObj.viewFormsList = {
            frmStopPayments: 'frmStopPayments'
        };
        //Handling loading progreebar
        scopeObj.totalServiceModels = 0; //flag to wait for models from service
        scopeObj.tmpSerViewModels = []; //array to store models from service
        scopeObj.serverErrorVieModel = 'serverError';
        scopeObj.serviceViewModels = {
            frmStopPayments: []
        };
        scopeObj.activeForm = scopeObj.viewFormsList.frmStopPayments;
        scopeObj.checkRequestReasons = [
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Duplicate"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.LostorStolenCheck"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.DefectiveGoods"),
            kony.i18n.getLocalizedString("i18n.funds.InsufficientBalance"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Others")
        ];
        scopeObj.disputeTransactionRequestReasons = [
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.dontRecognise"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.GoodsAndService"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.duplicate"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.recurringTransaction"),
            kony.i18n.getLocalizedString("i18n.StopCheckPayments.BillingError"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Others")
        ];
        //Stop check request sort config
        scopeObj.stopChekRequestsConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY
        };
        scopeObj.disputedTransactionRequestsConfig = {
            'sortBy': 'disputeDate',
            'defaultSortBy': 'disputeDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY
        };
        scopeObj.navManager = applicationManager.getNavigationManager();
        scopeObj.accountManager = applicationManager.getAccountManager();
        scopeObj.transactionManager = applicationManager.getTransactionManager();
        scopeObj.paginationManager = applicationManager.getPaginationManager();

    };

    /**
     * showStopPayments : Entry point for Stop payment form 
     * @param {object} dataInputs, required object to show specific view
     */
    StopPaymentsPresentationController.prototype.showStopPayments = function (dataInputs) {
        var scopeObj = this;
        scopeObj.activeForm = scopeObj.viewFormsList.frmStopPayments;
        scopeObj.navManager.navigateTo(scopeObj.viewFormsList.frmStopPayments);
        if (dataInputs && dataInputs.show) {
            switch (dataInputs.show) {
                case OLBConstants.ACTION.SHOW_STOPCHECKS_FORM:
                    scopeObj.showStopChecksForm(dataInputs);
                    break;
                case OLBConstants.ACTION.SHOW_DISPUTE_TRANSACTION_FORM:
                    scopeObj.showDisputeTransaction(dataInputs.data);
                    break;
                case OLBConstants.DISPUTED_CHECKS:
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_CHECKS
                    });
                    break;
                case OLBConstants.DISPUTED_TRANSACTIONS:
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_TRANSACTIONS
                    });
                    break;
                default: //Default form view
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_TRANSACTIONS
                    });
            }
        }
        else {
            scopeObj.showMyRequests({
                selectTab: OLBConstants.DISPUTED_TRANSACTIONS
            });
        }
    };

    

    /**
    * presentStopPayments : Update stop payments form
    * @param {Object} viewModel, view model for stop payments
    */
    StopPaymentsPresentationController.prototype.presentStopPayments = function (viewModel) {
        var scopeObj = this;
        scopeObj.navManager.updateForm(viewModel, scopeObj.viewFormsList.frmStopPayments);
        scopeObj.updateLoadingForCompletePage(viewModel);
    };

    /**
     * Method to handle Progress bar
     * @param {boolean} isLoading , loading flag true.false
     */
    StopPaymentsPresentationController.prototype.updateProgressBarState = function (isLoading) {
        var scopeObj = this;
        scopeObj.navManager.updateForm({
            "progressBar": isLoading
        }, scopeObj.activeForm);
    };

    /**
     * updateLoadingForCompletePage : Method to handle loading progress bar w.r.t expected view models.
     * @param {Object}  viewModel ,  form view model
     */
    StopPaymentsPresentationController.prototype.updateLoadingForCompletePage = function (viewModel) {
        var scopeObj = this;
        if (viewModel.isLoading === true) {
            scopeObj.tmpSerViewModels = viewModel.serviceViewModels;
            scopeObj.totalServiceModels = 0;
            scopeObj.updateProgressBarState(true);
        } else {
            if (scopeObj.isServiceViewModel(viewModel, scopeObj.tmpSerViewModels)) {
                scopeObj.totalServiceModels++;
            }
            if (scopeObj.totalServiceModels === scopeObj.tmpSerViewModels.length) {
                scopeObj.updateProgressBarState(false);
                scopeObj.totalServiceModels = 0;
            }
        }
    };

    /**
     * isServiceViewModel  Method to validate service view models.
     * @param {Object} viewModel   form view model
     * @param {Array} serviceViewModels expected serice view models.
     * @returns {object} object
     */
    StopPaymentsPresentationController.prototype.isServiceViewModel = function (viewModel, serviceViewModels) {
        var scopeObj = this;
        return Object.keys(viewModel).filter(function (key) {
            return serviceViewModels.indexOf(key) >= 0 || key === scopeObj.serverErrorVieModel; //include server error as expected
        }).length > 0;
    };


    /**
     * onServerError : Method to handle server errors.
     * @param {object} data - Service error object
     */
    StopPaymentsPresentationController.prototype.onServerError = function (data) {
        var scopeObj = this;
        if (scopeObj.activeForm) {
            scopeObj.navManager.updateForm({
                serverError: data
            }, scopeObj.activeForm);
            scopeObj.updateLoadingForCompletePage({
                serverError: data
            });
        }
    };

    /**
     * fetchAccounts: fetches accounts using Command - 'com.kony.StopPayments.getAccounts'
     * @param {function} onSuccess , success callback
     * @param {function} onError , error callback
     */
    StopPaymentsPresentationController.prototype.fetchAccounts = function (onSuccess, onError) {
        var scopeObj = this;
        scopeObj.accountManager.fetchInternalAccounts(scopeObj.fetchInternalAccountsSuccessCallBack.bind(scopeObj, onSuccess),
            scopeObj.fetchInternalAccountsFailureCallBack.bind(scopeObj, onError));
    };

    /**
     * used to handels the fetchInternalAccounts success schenario
     * @param {function} onSuccess onSuccess
     * @param {object} response response
     */
    StopPaymentsPresentationController.prototype.fetchInternalAccountsSuccessCallBack = function (onSuccess, response) {
        onSuccess(response)
    };

    /**
      * used to handels the fetchInternalAccounts success schenario
      * @param {function} onError onError
      * @param {object} response response
      */
    StopPaymentsPresentationController.prototype.fetchInternalAccountsFailureCallBack = function (onError, response) {
        onError(response);
    };

    /****************************************************************************************************************************
        * Stop Payments : Dispute transaction details
        ****************************************************************************************************************************/
    /**
 * showDisputeTransactionRequests: Method to show My Requests Disputed transaction request
 * @param {object} dataInputs config
 */
    StopPaymentsPresentationController.prototype.showDisputeTransactionRequests = function (dataInputs) {
        var scopeObj = this;
        dataInputs = dataInputs || {};
        scopeObj.paginationManager.resetValues();
        scopeObj.paginationManager.getValues(scopeObj.disputedTransactionRequestsConfig, dataInputs);
        scopeObj.getViewRequestsDisputedTransactions(scopeObj.onDisputeTransactionRequestsSuccess.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
    };


    /**
     * getViewRequestsData: Method  return dispute transaction reasons
     * @param {function} onSuccess onSuccess
     * @param {function} onError onError
     */
    StopPaymentsPresentationController.prototype.getViewRequestsDisputedTransactions = function (onSuccess, onError) {
        var scopeObj = this;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['viewDisputedRequestsResponse']
        });
        var dataInputs = applicationManager.getPaginationManager().getValues(scopeObj.disputedTransactionRequestsConfig);
        dataInputs.transactionType = OLBConstants.TRANSACTION_TYPE.DISPUTEDTRANSACTIONSREQUEST;
        scopeObj.transactionManager.getDisputedTransactions(dataInputs, scopeObj.getDisputedTransactionsSuccessCallBack.bind(scopeObj, onSuccess),
            scopeObj.getDisputedTransactionsFailureCallBack.bind(scopeObj, onError));
    };

    /** 
     * get View RequestsDisputedTransactions success schenario
     * @param {function} onSuccess onSuccess
     * @param {object} response response
    */
    StopPaymentsPresentationController.prototype.getDisputedTransactionsSuccessCallBack = function (onSuccess, response) {
        var scopeObj = this;
        var dataInputs = applicationManager.getPaginationManager().getValues(scopeObj.disputedTransactionRequestsConfig);
        dataInputs.transactionType = OLBConstants.TRANSACTION_TYPE.DISPUTEDTRANSACTIONSREQUEST;
        response.config = dataInputs;
        onSuccess(response);
    };

    /** 
   * get View RequestsDisputedTransactions success schenario
   * @param {function} onError onError
   * @param {object} response response
  */
    StopPaymentsPresentationController.prototype.getDisputedTransactionsFailureCallBack = function (onError, response) {
        onError(response);
    };

    /**
     * onDisputeTransactionRequestsSuccess: Methods that excutes as success callback for getViewRequestsDisputedTransactions
     * @param {object} responseData responseData
     */
    StopPaymentsPresentationController.prototype.onDisputeTransactionRequestsSuccess = function (responseData) {
        var scopeObj = this;
        if (responseData) {
            scopeObj.presentStopPayments({
                'viewDisputedRequestsResponse': {
                    "stopDisputedRequests": scopeObj.getDisputeRequestsViewModel(responseData),
                    "config": responseData.config
                }
            });
        } else {
            scopeObj.onServerError(responseData);
        }
    };
    /**
 * getDisputeRequestsViewModel: Methods that gets view model for dispute transactions view requests
 * @param {object} disputeViewRequests data from service
 * @returns {object} viewModel for get view requests for dispute
 */
    StopPaymentsPresentationController.prototype.getDisputeRequestsViewModel = function (disputeViewRequests) {
        var scopeObj = this;
        var disputeTransactionRequestsViewmodel = disputeViewRequests || [];
        disputeTransactionRequestsViewmodel = disputeViewRequests.map(function (requestObject) {
            var finalRequestObject = {
                disputeDate: requestObject.disputeDate === undefined || requestObject.disputeDate === null ? kony.i18n.getLocalizedString("i18n.common.none") : CommonUtilities.getFrontendDateString(requestObject.disputeDate),
                transactionDesc: requestObject.description === undefined || requestObject.description === null ? kony.i18n.getLocalizedString("i18n.common.none") : requestObject.description,
                transactionId: requestObject.transactionId,
                amount: requestObject.amount === undefined || requestObject.amount === null ? kony.i18n.getLocalizedString("i18n.common.none") : CommonUtilities.formatCurrencyWithCommas(requestObject.amount,false,requestObject.currencyCode),
                disputeStatus: requestObject.disputeStatus,
                fromAccountNumber: requestObject.fromAccountNumber,
                fromAccountNickName: requestObject.fromNickName,
                fromAccountName: requestObject.fromAccountName,
                fromAccount: CommonUtilities.mergeAccountNameNumber(requestObject.fromNickName || requestObject.fromAccountName, requestObject.fromAccountNumber),
                toAccountName: requestObject.toAccountName || requestObject.payPersonName || requestObject.payeeNickName || requestObject.payeeName,
                transactionDate: CommonUtilities.getFrontendDateString(requestObject.transactionDate),
                transactionType: requestObject.transactionType,
                disputeReason: requestObject.disputeReason,
                disputeDescription: requestObject.disputeDescription === undefined || requestObject.disputeDescription === null ? kony.i18n.getLocalizedString("i18n.common.none") : requestObject.disputeDescription,
                /**
                 * used to navigate the message 
                 */
                onSendMessageAction: function () {
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage(null, {
                        show: "CreateNewMessage",
                        cancelCallback : scopeObj.showStopPayments.bind(scopeObj, {show : OLBConstants.DISPUTED_TRANSACTIONS} )
                    });
                }
            };
            if (requestObject.disputeStatus === OLBConstants.TRANSACTION_STATUS.INPROGRESS) {
                finalRequestObject.onCancelRequest = function () {
                    scopeObj.onCancelDisputeTransactionRequest({
                        transactionId:requestObject.transactionId,
                        transactionType:requestObject.transactionType
                    });
                };
            }
            return finalRequestObject;
        });
        return disputeTransactionRequestsViewmodel;
    };


    /**
     * getdisputeTransactionReasonsListViewModel: Method  return dispute transaction reasons
     * @return {Array}, dispute transaction reasons list
     */
    StopPaymentsPresentationController.prototype.getdisputeTransactionReasonsListViewModel = function () {
        var scopeObj = this;
        return scopeObj.disputeTransactionRequestReasons.map(function (reason) {
            return {
                id: reason,
                name: reason
            };
        });
    };


    /**
   * createDisputeTransaction : Method for calling create transaction service for dispute a transaction
   * @param {object} params  params
   * @param {object} input input
   */
    StopPaymentsPresentationController.prototype.createDisputeTransaction = function (params, input) {
        var self = this;
        var dataParams = {
            "transactionId": params.transactionId,
            "disputeReason": params.disputeReason, // Platform defect for special character(ticket number 102172)
            "disputeDescription": params.disputeDescription // Platform defect for special character(ticket number 102172)

        };
        this.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['disputeTransactionResponse']
        });
        self.transactionManager.createDisputedTransaction(dataParams, self.createDisputedTransactionSuccessCallBack.bind(self, input),
            self.createDisputedTransactionFailureCallBack.bind(self));
    };

    /**
     * handels the createDisputedTransaction Success schenario
     * @param {*} input input
     * @param {*} response  response
     */
    StopPaymentsPresentationController.prototype.createDisputedTransactionSuccessCallBack = function (input, response) {
        var self = this;
        self.presentStopPayments({
            "disputeTransactionResponse": {
                data: response,
                values: input
            }
        });
    };

    /**
     * handels the createDisputedTransaction Failure schenario
     * @param {*} response  response
     */
    StopPaymentsPresentationController.prototype.createDisputedTransactionFailureCallBack = function (response) {
        var self = this;
        self.onServerError(response.data);
    };

    /**
     * showDisputeTransaction : Method to show dispute transaction pageX
     * @param {object} data transaction
     */
    StopPaymentsPresentationController.prototype.showDisputeTransaction = function (data) {
        var formatUtil = applicationManager.getFormatUtilManager();
        var transaction = data.disputeTransactionObject;
        var From = CommonUtilities.mergeAccountNameNumber(transaction.fromNickName || transaction.fromAccountName, transaction.fromAccountNumber);
        var transactionDate =formatUtil.getFormatedDateString(formatUtil.getDateObjectfromString(transaction.transactionDate, "YYYY-MM-DD HH:MM:SS"), formatUtil.getApplicationDateFormat());
        var viewModel = {
            fromAccountNumber: From,
            toAccount: transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName,
            amount: CommonUtilities.formatCurrencyWithCommas(Math.abs(transaction.amount), true),
            date: transactionDate,
            types: transaction.transactionType,
            referenceNumber: data.disputeTransactionObject.transactionId,
            notes: transaction.transactionsNotes
        };
        this.presentStopPayments({
            disputeTransactionObject: {
                data: viewModel,
                onCancel: data.onCancel,
                /**
                 * used to navigate the accountDetails
                 */
                onBacktoAccountDetails: function () {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showAgainAccountsDetails(transaction.fromAccountNumber);
                }
            }
        });
    };

    /****************************************************************************************************************************
     * Stop Payments : Checks
     ****************************************************************************************************************************/


    /**
     * isCheckAccount: Method to return whether account support checks or not 
     * @param {RBObjects.Account} account , account object  - RBObjects.Account.
     * @returns {boolean} is this Account support check?
     */
    StopPaymentsPresentationController.prototype.isCheckAccount = function (account) {
        return account.supportChecks && account.supportChecks === '1';
    };

    /**
     * showStopChecksForm: Method to show Stop Check request form
     * @param {object} dataInputs ,input values for pre popultated data
     */
    StopPaymentsPresentationController.prototype.showStopChecksForm = function (dataInputs) {
        var scopeObj = this;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['stopChecksFormAccounts']
        });
        scopeObj.fetchAccounts(scopeObj.onStopChecksFormAccountsSuccess.bind(scopeObj), CommonUtilities.showServerDownScreen);
        scopeObj.presentStopPayments({
            'stopChecksFormData': scopeObj.getStopChecksFormViewModel(dataInputs)
        });
    };

    /**
     * onStopChecksFormAccountsSuccess: Method to handle check accounts list
     * @param {object} accounts  accounts
     */
    StopPaymentsPresentationController.prototype.onStopChecksFormAccountsSuccess = function (accounts) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            'stopChecksFormAccounts': scopeObj.getStopChecksFormAccountsViewModel(accounts.filter(scopeObj.isCheckAccount))
        });
    };

    /**
     * getStopChecksFormAccountsViewModel: Method to create and return Stop check form accounts view model
     * @param {Array} accounts, RBObjects.Account array
     * @return {Array} Stop check form accounts view model
     */
    StopPaymentsPresentationController.prototype.getStopChecksFormAccountsViewModel = function (accounts) {
        /**
         * create the account view
         * @param {*} account account
         * @returns {object} accounts
         */
        var createAccountsViewModal = function (account) {
            return {
                accountName: CommonUtilities.getAccountDisplayName(account),
                accountID: account.accountID,
                type: account.accountType
            };
        };
        return accounts.map(createAccountsViewModal);
    };

    /**
     * getStopChecksFormViewModel: Method to create and return Stop check form
     * @param {object} data, pre populated form data
     * @return {object} Stop check form view model
     */
    StopPaymentsPresentationController.prototype.getStopChecksFormViewModel = function (data) {
        var scopeObj = this;
        data = data || {};
        var isSeriesChecks = data.requestType === OLBConstants.CHECK_REQUEST_TYPES.SERIES;
        scopeObj.onCancel = data.onCancel || function () {
            scopeObj.presentStopPayments({
                "myRequests": {
                    selectTab: OLBConstants.DISPUTED_CHECKS
                }
            });
        };

        return {
            accountID: data.accountID || data.fromAccountNumber || null,
            payeeName: data.payeeName || "",
            isSeriesChecks: isSeriesChecks,
            checkNumber1: data.checkNumber1,
            checkNumber2: data.checkNumber2,
            checkDateOfIssue: data.checkDateOfIssue ? CommonUtilities.getFrontendDateString(data.checkDateOfIssue) : CommonUtilities.getFrontendDateString(kony.os.date("YYYY-MM-DD")),
            checkReason: data.checkReason,
            checkAmount: Math.abs(data.amount),
            description: data.transactionsNotes || "",
            maxDesriptionLength: OLBConstants.NOTES_MAX_LENGTH,
            showStopPaymentServiceFeesAndValidity: applicationManager.getConfigurationManager().enalbeStopPaymentServiceFeesAndValidity === "true",
            checkServiceFee: applicationManager.getConfigurationManager().checkServiceFee,
            checkServiceVality: applicationManager.getConfigurationManager().checkServiceVality,
            serviceChargableText: kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable11") + " " + applicationManager.getConfigurationManager().getCurrencyCode() + applicationManager.getConfigurationManager().checkServiceFee + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable12") + " " + applicationManager.getConfigurationManager().checkServiceVality + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable13"),
            onCancel: scopeObj.onCancel
        };

    };

    /**
     * stopCheckRequest: Method to create stop check request 
     * @param {object} transactionModel, check request transaction object
     */
    StopPaymentsPresentationController.prototype.stopCheckRequest = function (transactionModel) {
        var scopeObj = this;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['successStopCheckRequest']
        });
        var checkRequestModel = scopeObj.createCheckRequestModel(transactionModel);
        scopeObj.transactionManager.createTransaction(checkRequestModel, scopeObj.createCheckPaymentRequestSuccessCallBack.bind(scopeObj, transactionModel),
            scopeObj.createCheckPaymentRequestFailureCallBack.bind(scopeObj));
    };

    /**
     * used to handels the create check request success schenario
     * @param {object} transactionModel transaction
     * @param {object} response response
     */
    StopPaymentsPresentationController.prototype.createCheckPaymentRequestSuccessCallBack = function (transactionModel, response) {
        var scopeObj = this;
        response.fromAccountNumber = transactionModel.fromAccountNumber;
        scopeObj.onCreateStopCheckRequestSuccess(response);
    };

    /**
     * used to handels the create check request Failure schenario
     * @param {object} response response
     */
    StopPaymentsPresentationController.prototype.createCheckPaymentRequestFailureCallBack = function (response) {
        var scopeObj = this;
        scopeObj.onServerError(response);
    };

    /**
     * createCheckRequestModel: Method to create Check request model object for Create check request
     * @param {object} transactionModel, check request transaction object
     * @return {object} formatted transasction model for Create check request
     */
    StopPaymentsPresentationController.prototype.createCheckRequestModel = function (transactionModel) {
        var checkRequestModel = {};
        if (transactionModel) {
            checkRequestModel = {
                transactionType: transactionModel.transactionType || OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST,
                fromAccountNumber: transactionModel.fromAccountNumber,
                payeeName: transactionModel.payeeName.trim(),
                checkNumber1: transactionModel.checkNumber1,
                amount: transactionModel.amount,
                requestValidityInMonths: applicationManager.getConfigurationManager().checkServiceVality,
                checkReason: transactionModel.checkReason,
                transactionsNotes: transactionModel.description.trim(),
            };
            if (transactionModel.checkNumber2) { //for series checks
                checkRequestModel.checkNumber2 = transactionModel.checkNumber2;
            }
            if (transactionModel.amount) { //for single check
                checkRequestModel.amount = transactionModel.amount;
            }
            if (transactionModel.checkDateOfIssue) { //for single check
                checkRequestModel.checkDateOfIssue = CommonUtilities.sendDateToBackend(transactionModel.checkDateOfIssue);
            }
        }
        else {
            CommonUtilities.ErrorHandler.onError("Invalid transaction Model");
        }
        return checkRequestModel;
    };

    /**
     * stopCheckRequest: Method to create stop check request 
     * @param {object} successData, stop check request success resposne
     */
    StopPaymentsPresentationController.prototype.onCreateStopCheckRequestSuccess = function (successData) {
        var scopeObj = this;
        if (successData && successData.referenceId) {
            scopeObj.presentStopPayments({
                "successStopCheckRequest": {
                    referenceNumber: successData.referenceId,
                    onMyRequestAction: function () {
                        scopeObj.showMyRequests({
                            selectTab: OLBConstants.DISPUTED_CHECKS
                        });
                    },
                    onBacktoAccountDetails: function () {
                        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                        accountsModule.presentationController.showAgainAccountsDetails(successData.fromAccountNumber);
                    }
                }
            });
        }
        else {
            scopeObj.onServerError(successData);
        }
    };

    /**
     * getCheckReasonsListViewModel: Method  return check reasons
     * @return {Array}, check request reasons list
     */
    StopPaymentsPresentationController.prototype.getCheckReasonsListViewModel = function () {
        var scopeObj = this;
        return scopeObj.checkRequestReasons.map(function (reason) {
            return {
                id: reason,
                name: reason
            };
        });
    };

    /******************************************************************************************************
     * Stop Payments : My Requests
     ******************************************************************************************************/

    /**
     * showMyRequests: Method to show My Requests
     * @param {object} dataInputs dataInputs
     * @throws {}
     */
    StopPaymentsPresentationController.prototype.showMyRequests = function (dataInputs) {

        var scopeObj = this;
        dataInputs = dataInputs || {};
        var selectTab = dataInputs.selectTab || OLBConstants.DISPUTED_TRANSACTIONS;
        scopeObj.presentStopPayments({
            "myRequests": {
                selectTab: selectTab,
                addNewStopCheckRequestAction: {
                    displayName: kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest"),
                    action: function () {
                        scopeObj.showStopChecksForm({
                            onCancel: function () {
                                scopeObj.presentStopPayments({
                                    "myRequests": {
                                        selectTab: OLBConstants.DISPUTED_CHECKS
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
        switch (selectTab) {
            case OLBConstants.DISPUTED_TRANSACTIONS:
                scopeObj.showDisputeTransactionRequests({
                    resetSorting: true
                });
                break;
            case OLBConstants.DISPUTED_CHECKS:
                scopeObj.showDisputeCheckRequests({
                    resetSorting: true
                });
                break;
        }
    };

    /**
     * showDisputeCheckRequests: Method to show My Requests Disputed stop check request
     * @param {object} dataInputs dataInputs
     */
    StopPaymentsPresentationController.prototype.showDisputeCheckRequests = function (dataInputs) {
        var scopeObj = this;
        scopeObj.paginationManager.resetValues();
        dataInputs = dataInputs || {};
        scopeObj.paginationManager.getValues(scopeObj.stopChekRequestsConfig, dataInputs);
        scopeObj.getStopCheckRequests(scopeObj.onStopCheckRequestsSuccess.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
    };

    /**
     * getStopCheckRequests: Method to fetch stop check request from MF/Command handler - com.kony.StopPayments.getStopCheckPaymentRequests.
     * @param {function} onSuccess, success call back
     * @param {function} onError, error call back
     */
    StopPaymentsPresentationController.prototype.getStopCheckRequests = function (onSuccess, onError) {
        var scopeObj = this;
        var dataInputs = scopeObj.paginationManager.getValues(scopeObj.stopChekRequestsConfig);
        dataInputs.transactionType = OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['stopCheckRequestsViewModel'] //expected view model object
        });
        scopeObj.transactionManager.getStopCheckPaymentRequests(dataInputs, scopeObj.getStopCheckPaymentRequestsSuccessCallBack.bind(scopeObj, onSuccess),
            scopeObj.getStopCheckPaymentRequestsFailureCallBack.bind(scopeObj, onError));
    };
    
     /**
      * get StopCheckRequests success schenario
      * @param {function} onSuccess onSuceess
      * @param {object} response  response
      */
     StopPaymentsPresentationController.prototype.getStopCheckPaymentRequestsSuccessCallBack = function (onSuccess, response) {
        var scopeObj = this;
        var dataInputs = scopeObj.paginationManager.getValues(scopeObj.stopChekRequestsConfig);
        dataInputs.transactionType = OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST;
        response.config = dataInputs;
        onSuccess(response);
    };

    /**
 * get StopCheckRequests Failure schenario
 * @param {function} onError onError
 * @param {object} response response
 */
    StopPaymentsPresentationController.prototype.getStopCheckPaymentRequestsFailureCallBack = function (onError, response) {
        onError(response);
    };

    /**
     * onStopCheckRequestsSuccess: Method to handle successful stop check requests
     * @param {object} responseData, response data
     */
    StopPaymentsPresentationController.prototype.onStopCheckRequestsSuccess = function (responseData) {
        var scopeObj = this;
        if (responseData) {
            scopeObj.presentStopPayments({
                'stopCheckRequestsViewModel': {
                    "stopchecksRequests": scopeObj.getStopCheckRequestsViewModel(responseData),
                    "config": responseData.config
                }
            });
        } else {
            scopeObj.onServerError(responseData);
        }
    };

    /*
     * getStopCheckRequestsViewModel: Method to return Stop Check Requests view model
     * @param {Array} stopChecksRequests, stop check requests data array 
     * @return {Array} stopCheckRequestsViewModel, stop check requests view model array 
     * @throws {}
     */
    StopPaymentsPresentationController.prototype.getStopCheckRequestsViewModel = function (stopChecksRequests) {
        var scopeObj = this;
        var stopCheckRequestsViewModel = stopChecksRequests || [];
        stopCheckRequestsViewModel = stopChecksRequests.map(function (requestObject) {
            var finalRequestObject = {
                transactionId: requestObject.transactionId,
                transactionDate: CommonUtilities.getFrontendDateString(requestObject.transactionDate),
                transactionType: requestObject.transactionType,
                payeeName: requestObject.payeeName,
                statusDescription: requestObject.statusDescription,
                fromAccountNumber: requestObject.fromAccountNumber,
                fromAccountName: requestObject.fromAccountName,
                fromAccountNickName: requestObject.fromNickName,
                fromAccount: CommonUtilities.mergeAccountNameNumber(requestObject.fromNickName || requestObject.fromAccountName, requestObject.fromAccountNumber),
                requestValidity: requestObject.requestValidity ? CommonUtilities.getFrontendDateString(requestObject.requestValidity) : kony.i18n.getLocalizedString("i18n.common.none"),
                checkReason: requestObject.checkReason,
                transactionsNotes: requestObject.transactionsNotes === undefined || requestObject.transactionsNotes === null ? kony.i18n.getLocalizedString("i18n.common.none") : requestObject.transactionsNotes,
                requestType: requestObject.requestType,
                amount: requestObject.amount === undefined || requestObject.amount === null ? kony.i18n.getLocalizedString("i18n.common.NA") : CommonUtilities.formatCurrencyWithCommas(requestObject.amount,false,requestObject.currencyCode),
                checkDateOfIssue: requestObject.checkDateOfIssue ? CommonUtilities.getFrontendDateString(requestObject.checkDateOfIssue) : kony.i18n.getLocalizedString("i18n.common.NA"),
                /**
                 * used to navigate the new message flow
                 */
                onSendMessageAction: function () {
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage(null, {
                        show: "CreateNewMessage",
                        cancelCallback : scopeObj.showStopPayments.bind(scopeObj, {show : OLBConstants.DISPUTED_CHECKS} )
                    });
                }
            };

            if (requestObject.requestType === OLBConstants.CHECK_REQUEST_TYPES.SINGLE) {
                finalRequestObject.checkNumber = requestObject.checkNumber1;
            } else if (requestObject.requestType === OLBConstants.CHECK_REQUEST_TYPES.SERIES) {
                finalRequestObject.checkNumber = requestObject.checkNumber1 + " " + OLBConstants.CHECK_SERIES_SEPARATOR + " " + requestObject.checkNumber2;
            } else {
                CommonUtilities.ErrorHandler.onError("getStopCheckRequestsViewModel : Invalid request type: " + requestObject.requestType);
            }

            if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.STOPPED || requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.INPROGRESS) {
                finalRequestObject.onCancelRequest = function () {
                    scopeObj.onCancelStopCheckRequest(requestObject.transactionId);
                };
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.REQUESTEXPIRED) {
                finalRequestObject.onReNewRequest = function () {
                    requestObject.onCancel = scopeObj.presentStopPayments.bind(scopeObj, {
                        "myRequests": {
                            selectTab: OLBConstants.DISPUTED_CHECKS
                        }
                    });
                    scopeObj.showStopChecksForm(requestObject);

                };
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.CLEARED) {
                //Nothing
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.FAILED) {
                //Nothing
            }

            return finalRequestObject;
        });
        return stopCheckRequestsViewModel;
    };

    /**
     * onCancelDisputeTransactionRequest: Method to handle cancel dispute transactions request action,
     * @param {String} transactionId, requested disputed transaction, transaction id.
     */
    StopPaymentsPresentationController.prototype.onCancelDisputeTransactionRequest = function (transactionParams) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            "cancelStopCheckAction": {
                headerText: kony.i18n.getLocalizedString("i18n.StopCheckPayments.CancelDisputeTransaction"),
                message: kony.i18n.getLocalizedString("i18n.StopCheckPayments.AreYouSureToCancelTheDisputeRequest"),
                /**
                 * used to delete the dispute transaction
                 */
                confirmCancelAction: function () {
                    scopeObj.cancelDisputeTransactionRequest(transactionParams, scopeObj.showDisputeTransactionRequests.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                }
            }
        });
    };

    /**
     * cancelDisputeTransactionRequest: Method to cancel/delete dispute a transaction
     * @param {String} transactionId requested disputed transaction
     * @param {function} onSuccess success call back
     * @param {function} onError error call back
     */
    StopPaymentsPresentationController.prototype.cancelDisputeTransactionRequest = function (transactionParams, onSuccess, onError) {
        var scopeObj = this;
        if (transactionParams.transactionId) {
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['cancelRequestSuccess'] //expected view model object
            });
            scopeObj.transactionManager.deleteTransaction(transactionParams, scopeObj.cancelDisputedTransactionSuccessCallBack.bind(scopeObj, onSuccess), scopeObj.cancelDisputedTransactionFailureCallBack.bind(scopeObj, onError));
        } else {
            CommonUtilities.ErrorHandler.onError("cancelDisputeTransactionRequest : Invalid transaction Id" + transactionParams.transactionId);
        }
    };

    /**
     * cancelDisputed Transaction Success Schenario
     * @param {function} onSuccess onSuccess
     * @param {object} response reponse 
     */
    StopPaymentsPresentationController.prototype.cancelDisputedTransactionSuccessCallBack = function (onSuccess, response) {
        onSuccess(response);
    };


    /**
     * cancelDisputed Transaction Failure Schenario
     * @param {function} onError onSuccess
     * @param {object} response reponse 
     */
    StopPaymentsPresentationController.prototype.cancelDisputedTransactionFailureCallBack = function (onError, response) {
        onError(response);
    };

    /**
     * onCancelStopCheckRequest: Method to handle cancel stop check request action,
     * @param {String} transactionId, requested stop check transaction id.
     */
    StopPaymentsPresentationController.prototype.onCancelStopCheckRequest = function (transactionId) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            "cancelStopCheckAction": {
                headerText: kony.i18n.getLocalizedString("i18n.StopCheckPayments.CancelStopCheckPayment"),
                message: kony.i18n.getLocalizedString("i18n.StopCheckPayments.AreYouSureToCancelTheRequest"),
                showStopPaymentServiceFeesAndValidity: applicationManager.getConfigurationManager().enalbeStopPaymentServiceFeesAndValidity === "true",
                checkServiceFee: applicationManager.getConfigurationManager().checkServiceFee,
                checkServiceVality: applicationManager.getConfigurationManager().checkServiceVality,
                serviceChargableText: kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable11") + applicationManager.getConfigurationManager().getCurrencyCode() + applicationManager.getConfigurationManager().checkServiceFee + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable12") + applicationManager.getConfigurationManager().checkServiceVality + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable13") + " " + kony.i18n.getLocalizedString("i18n.StopcheckPayments.ThisRequestWillbeProcessedWithin12hrs"),
                /**
                 * used to cancel the cheque
                 */
                confirmCancelAction: function () {
                    scopeObj.canelStopCheckRequest(transactionId, scopeObj.showDisputeCheckRequests.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                }
            }
        });
    };

    /**
     * canelStopCheckRequest: Method to cancel/delete stop check request transaction.
     * @param {String} transactionId, requested stop check transaction id.
     * @param {function} onSuccess, success call back
     * @param {function} onError error call back
     */
    StopPaymentsPresentationController.prototype.canelStopCheckRequest = function (transactionId, onSuccess, onError) {
        var scopeObj = this;
        if (transactionId) {
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['cancelRequestSuccess'] //expected view model object
            });
            var params = {
                transactionId: transactionId
            };
            scopeObj.transactionManager.deleteTransaction(params, scopeObj.canelStopCheckRequestSuccessCallBack.bind(scopeObj, onSuccess),
                scopeObj.canelStopCheckRequestFailureCallBack.bind(scopeObj, onError));
        } else {
            CommonUtilities.ErrorHandler.onError("canelStopCheckRequest : Invalid transaction Id" + transactionId);
        }
    };

    /**
     * canelStopCheckRequest success schenario
     * @param {function} onSuccess success callBack
     * @param {object} response response
     */
    StopPaymentsPresentationController.prototype.canelStopCheckRequestSuccessCallBack = function (onSuccess, response) {
        onSuccess(response);

    };

    /**
     * canelStopCheckRequest Failure schenario
     * @param {function} onError success callBack
     * @param {object} response response
     */
    StopPaymentsPresentationController.prototype.canelStopCheckRequestFailureCallBack = function (onError, response) {
        onError(response);
    };

    StopPaymentsPresentationController.prototype.showPrintPage = function(data) {
        applicationManager.getNavigationManager().navigateTo('frmPrintTransfer');
        applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
    };

    return StopPaymentsPresentationController;
});