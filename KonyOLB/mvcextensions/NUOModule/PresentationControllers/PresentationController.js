/**
* New User ON Boarding Presentation handler
* @module NUOPresentationController
*/
define([], function () {

    /**
   *New User ON Boarding Presentation handler
   * @class
   * @alias module:NUOPresentationController
   */

    var isErrorState = false;
    var progressBarCount = 0;

    function NUOPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
    }

    inheritsFrom(NUOPresentationController, kony.mvc.Presentation.BasePresenter);

    /**
     * Method to intialize NUO presentation scope data.
    */
    NUOPresentationController.prototype.initializePresentationController = function () {
        this.navManager = applicationManager.getNavigationManager();
        this.productManager = applicationManager.getProductManager();
        this.newUserBusinessManager = applicationManager.getNewUserBusinessManager();
        this.userPreferencesManager = applicationManager.getUserPreferencesManager();
        this.authManager = applicationManager.getAuthManager();
        this.frmNewUserOnboarding = "frmNewUserOnboarding";
    };

    /**
     * Entry Point method for new user onboarding
     * @param {string} context context
     */
    NUOPresentationController.prototype.showNewUserOnBoarding = function (context) {
        progressBarCount = 0;
        isErrorState = false;
        context = context || {
            NUOLanding: "true"
        }
        this.showNewUserOnBoardingBasedOnContext(context)
    };

    NUOPresentationController.prototype.showNewUserOnBoardingBasedOnContext = function (context) {
        progressBarCount = 0;
        isErrorState = false;
        if (context.NUOLanding) {
            this.navManager.navigateTo(this.frmNewUserOnboarding);
            this.presentNUO({
                NUOLanding: {}
            })
        }
    }

    /**
     * Present new user onboarding screen
     * @param {JSON} data View Model For NUO form
     */
    NUOPresentationController.prototype.presentNUO = function (data) {
        if (!isErrorState) {
            this.navManager.updateForm(data, this.frmNewUserOnboarding)
        }
    };

    /**
     * Show Error Screen
     */
    NUOPresentationController.prototype.showErrorScreen = function () {
        this.forceHideProgressBar();
        isErrorState = true;
        this.showServerDownScreen();
    };
    /** 
     * used to navigate the sever down screen
    */
    NUOPresentationController.prototype.showServerDownScreen = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        //Presenting the Login form without a viewmodel first as a work around for preShow of form being called after willUpdateUI.
        authModule.presentationController.presentUserInterface('frmLogin', {});
        this.navigateToServerDownScreen();
    };

    /** 
     * used to navigate the sever down screen
    */
    NUOPresentationController.prototype.navigateToServerDownScreen = function () {
        kony.store.setItem('OLBLogoutStatus', {
            action: "ServerDown"
        });
        this.doLogout();
    };

    /**
     * Shows Progress bar - Indicates Loading State
     */
    NUOPresentationController.prototype.showProgressBar = function () {
        var self = this;
        if (progressBarCount === 0) {
            self.presentNUO({
                showProgressBar: "showProgressBar"
            });
        }
        progressBarCount++;
    };

    /**
     * Hides Progress bar - Indicates Non Loading State
     */
    NUOPresentationController.prototype.hideProgressBar = function () {
        var self = this;
        if (progressBarCount > 0) {
            progressBarCount--;
        }
        if (progressBarCount === 0) {
            self.presentNUO({
                hideProgressBar: "hideProgressBar"
            });
        }
    };
    /**
     * Hides Progress bar - Indicates Non Loading State
     */
    NUOPresentationController.prototype.forceHideProgressBar = function () {
        var self = this;
        progressBarCount = 0;
        self.presentNUO({
            hideProgressBar: "hideProgressBar"
        });

    }

    /**
     * Show Server Error Flex on UI
     * @param {string} errorMessage error message
     */
    NUOPresentationController.prototype.showServerErrorFlex = function (
        errorMessage
    ) {
        this.hideProgressBar();
        this.presentWireTransfer({
            serverError: errorMessage
        });
    };

    /** 
     * Shows Step 2 of new user on boarding - Product Selection
     * @param {JSON} context Data from previous step.
    */
    NUOPresentationController.prototype.navigateToProductSelection = function (context) {
        context = context || {};
        this.getAllProducts(context);
    };
    /** 
     * get all available products and sends them to UI
     * @param {JSON} context Data from previous step.
     * @returns {void} - None
    */
    NUOPresentationController.prototype.getAllProducts = function (context) {
        var self = this;
        var asyncManager = applicationManager.getAsyncManager();
        if (context.isRevisiting) {
            self.showProgressBar();
        }
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(self.productManager, 'fetchProductsList'),
                asyncManager.asyncItem(self.productManager, 'getUserProductsList')
            ],
            self.getAllProductsCompleteSuccessCallBack.bind(self, context)
        );
    };

    /**
     * used to get AllProducts Completion SuccessCallBack
     * @param {JSON} context Data from previous step.
     * @param {object} syncResponseObject - responses object.
    */
    NUOPresentationController.prototype.getAllProductsCompleteSuccessCallBack = function (context, syncResponseObject) {
        var self = this;
        var userName = kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName;
        self.hideProgressBar();
        function convertToProducts(responseItem) {
            return JSON.parse(responseItem.products)
        }
        if (syncResponseObject.isAllSuccess()) {
            self.presentNUO({
                productSelection: {
                    products: syncResponseObject.responses[0].data,
                    username: userName,
                    selectedProducts: syncResponseObject.responses[1].data.map(convertToProducts),
                    isRevisiting: context.isRevisiting
                }
            })
        } else {
            self.showErrorScreen();
        }
    };
    /** 
     * Save user products
     * @param {JSON} productList data from UI
    */
    NUOPresentationController.prototype.saveUserProducts = function (productList) {
        var self = this;
        self.showProgressBar();
        var data = productList.map(function (product) {
            return {
                product: JSON.stringify({
                    productTypeId: product.productTypeId,
                    productName: product.productName,
                    productType: product.productType,
                    productId: product.productId
                })
            };
        });
        self.productManager.createSelectedProductsList(
            { productLi: JSON.stringify(data).replace(/\"/g, "'").replace(/\\\"/g, "\"").replace(/\\'/g, "\"") }
            , self.saveUserSelectedProductsSuccessCallBack.bind(self),
            self.saveUserSelectedProductsFailureCallBack.bind(self)
        );
    };

    /** 
     * save user selected products success schenario
     * @param {object} saveUserSelectedProducts userSelectedProdcuts 
    */
    NUOPresentationController.prototype.saveUserSelectedProductsSuccessCallBack = function () {
        var self = this;
        self.hideProgressBar();
        self.navigateToUserInformationStep();
    };

    /** 
     * save user selected products success schenario
     * @param {object} saveUserSelectedProducts userSelectedProdcuts 
    */
    NUOPresentationController.prototype.saveUserSelectedProductsFailureCallBack = function () {
        var self = this;
        self.showErrorScreen();
    };

    /**
     * Checks the Existance of phone number
     * @param {string} phoneNumber phone Number 
     */
    NUOPresentationController.prototype.checkPhoneNumber = function (phoneNumber) {
        var self = this;
        self.showProgressBar();
        var params = {
            "phone": phoneNumber
        };
        self.newUserBusinessManager.customGetExistingUserPhone(params, self.customGetExistingUserPhoneSuccessCallBack.bind(self, phoneNumber),
            self.customGetExistingUserPhoneFailureCallBack.bind(self, phoneNumber));
    };

    /**
      * Checks the Existance of phone number success schenario
      * @param {string} phoneNumber phone Number
      * @param {object} response response 
     */
    NUOPresentationController.prototype.customGetExistingUserPhoneSuccessCallBack = function (phoneNumber, response) {
        var self = this;
        // Check required for Already Enrolled Customer
        // If Check Available from Backend then call self.customerExists
        if (response.success) {
            self.createNewCustomer(phoneNumber);
            return;
        }
        
    };

    /**
      * Checks the Existance of phone number Error schenario
     * @param {object} response response 
     */
    NUOPresentationController.prototype.customGetExistingUserPhoneFailureCallBack = function (phoneNumber, response) {
        var self = this;
        if (response.errorMessage === "User already exists") {
                self.hideProgressBar();
                self.continueApplication(phoneNumber);
                return;
        }
        self.showErrorScreen();
    };

    /**
    * Continue the application - User exists as new applicant
    * @param {string} phoneNumber phone number
    */
    NUOPresentationController.prototype.continueApplication = function (phoneNumber) {
        this.presentNUO({
            continueApplication: {
                phoneNumber: phoneNumber
            }
        });
    }

    /**
    * Notify UI that User exists as a Fully Enrolled Customer 
    * @param {string} phoneNumber phone Number 
    */
    NUOPresentationController.prototype.customerExists = function (phoneNumber) {
        this.presentNUO({
            customerExists: {
                phoneNumber: phoneNumber
            }
        });
    }

    /**
    * User Already Exists - Requests OTP
    * @param {string} phoneNumber phone Number
    */
    NUOPresentationController.prototype.createNewCustomer = function (phoneNumber) {
        var self = this;
        self.requestOTP(function () {
            self.hideProgressBar();
            self.presentNUO({
                createNewCustomer: {
                    phoneNumber: phoneNumber
                }
            });
        })

    }

    /**
     * requests for the OTP
     * @param {function} successCallBack Optional success call back - If not Specified it will contninue new application flow
     */
    NUOPresentationController.prototype.requestOTP = function (successCallBack) {
        var self = this;
        if (!successCallBack) {
            self.showProgressBar();
        }
        self.userPreferencesManager.SendSecureAccessCode(self.completionRequestOTPSuccessCallBack.bind(self, successCallBack),
            self.completionRequestOTPFailureCallBack.bind(self));
    };

    /**
     * completion RequestOTP Success Schenario
     * @param {function} successCallBack success CallBack
     * @param  {object}  response response
     */
    NUOPresentationController.prototype.completionRequestOTPSuccessCallBack = function (successCallBack, response) {
        var self = this;
        /**
        * @param {object} response  response
        */
        var defaultOnSuccess = function (response) {
            self.hideProgressBar();
            self.presentNUO({ OTPRequested: response });
        }
        if (successCallBack) {
            successCallBack(response);
        }
        else {
            setTimeout(function () {
                defaultOnSuccess(response);
            }, 3000)
        }
    };


    /**
     * completion RequestOTP Failure Schenario
     */
    NUOPresentationController.prototype.completionRequestOTPFailureCallBack = function () {
        var self = this;
        self.showErrorScreen();
    };

    /**
     * Validates the OTP
     * @param {string} otp otp
     */
    NUOPresentationController.prototype.validateOTP = function (otp) {
        var self = this;
        var otpJSON = {
            "Otp": otp
        };
        self.showProgressBar();
        self.userPreferencesManager.VerifySecureAccessCode(otpJSON, self.verifySecureAccessCodeSuccessCallBack.bind(self), self.verifySecureAccessCodeFailureCallBack.bind(self));
    };

    /**
     * validate otp success schenario
     * @param {object} response response
     */
    NUOPresentationController.prototype.verifySecureAccessCodeSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({ OTPValidated: response });
    };

    /**
     * validate otp failure schenario
     * @param {object} response response
     */
    NUOPresentationController.prototype.verifySecureAccessCodeFailureCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            verifyOtpError: response.errorMessage
        });
    };

    /**
     * Checks user name availibility and creates if username is available.
     * @param {object} usernamePasswordJSON - JSON object of new user details
     */
    NUOPresentationController.prototype.enrollUser = function (usernamePasswordJSON) {
        var self = this;
        self.showProgressBar();
        this.checkUserName(usernamePasswordJSON.username, this.createUser.bind(this, usernamePasswordJSON), function () {
            self.hideProgressBar();
            self.presentNUO({
                enrollError: 'userNameError'
            })
        })
    };

    /**
    * Create user
    * @param {object} usernamePasswordJSON - JSON object of new user details
    */
    NUOPresentationController.prototype.createUser = function (usernamePasswordJSON) {
        var self = this;
        var params = {
            userName: usernamePasswordJSON.username,
            password: usernamePasswordJSON.password,
            phone: usernamePasswordJSON.phone,
            email: usernamePasswordJSON.email
        };
        self.newUserBusinessManager.createCredentialsForNewUser(params, self.createUserSuccessCallBack.bind(self, usernamePasswordJSON), self.createUserFailureCallBack.bind(self));
    };

    /**
     * create User Success schenario
     * @param {object} usernamePasswordJSON  usernamePassword JSON
     * @param {object} response  response
     */
    NUOPresentationController.prototype.createUserSuccessCallBack = function (usernamePasswordJSON, response) {
        var self = this;
        self.doLogin(usernamePasswordJSON, self.navigateToProductSelection.bind(self), self.showErrorScreen.bind(self));
    };

    /** 
     *  create User Failure schenario
     *  @param {object} response  response
    */
    NUOPresentationController.prototype.createUserFailureCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.showErrorScreen(response);
    };

    /**
     * check username
     * @param {object} username  JSON object of new user details
     * @param {function} successCallback successCallBack
     * @param {function} errorCallback errorCallback
     */
    NUOPresentationController.prototype.checkUserName = function (username, successCallback, usernameErrorCallback) {
        var self = this;
        var params = { UserName: username };
        self.newUserBusinessManager.checkUserName(params, successCallback,
            self.checkUserNameFailureCallBack.bind(self, usernameErrorCallback));
    };

    /**
  * check username Failure schenario
  * @param {object} response response
  */
    NUOPresentationController.prototype.checkUserNameFailureCallBack = function (usernameErrorCallback, response) {
        var self = this;
        if (response.errorMessage &&  response.errorMessage.toLowerCase() === "user name already exists") {
            usernameErrorCallback();
            return;
        }
        self.showErrorScreen();
    };


    /**
     * login for a new User
     * @param {object} usernamePasswordJSON  JSON object of new user credentials
     * @param {function} successCallBack successCallBack
     * @param {function} errorCallback errorCallback
     */
    NUOPresentationController.prototype.doLogin = function (usernamePasswordJSON, successCallBack, errorCallback) {
        var self = this;
        self.newUserBusinessManager.login(usernamePasswordJSON, self.doLoginSuccessCallBack.bind(self, successCallBack), self.doLoginFailureCallBack.bind(self, errorCallback));
    };

    /** 
     * login for a new User success schenario
     * @param {function} successCallBack successCallBack
     * @param {object} response response
    */
    NUOPresentationController.prototype.doLoginSuccessCallBack = function (successCallBack, response) {
        var self = this;
        kony.application.registerForIdleTimeout(applicationManager.getConfigurationManager().idleTimeOut, self.doLogout.bind(self));
        successCallBack(response);
    };

    /** 
  * login for a new User failure schenario
  * @param {function} errorCallback errorCallback
  * @param {object} response response
 */
    NUOPresentationController.prototype.doLoginFailureCallBack = function (errorCallback, response) {
        errorCallback(response);
    };


    /**
     * login for a new User
     * @param {string} password password
     */
    NUOPresentationController.prototype.doCustomerLogin = function (password) {
        var self = this;
        var params = {
            "username": kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName,
            "password": password
        }
        self.showProgressBar();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.onLogin(params, function (loginResponse) {
            self.hideProgressBar();
            self.presentNUO({
                customerLoginError: loginResponse.details.errmsg
            })
        });
    };

    /** 
     * used to navigate the accounts dashboard screen
    */
    NUOPresentationController.prototype.navigateToAccounts = function () {
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    }

    /**
     * Refreshes the application
     */
    NUOPresentationController.prototype.doLogout = function () {
        var self = this;
        self.newUserBusinessManager.logout(self.doLogoutSuccessCallBack.bind(self), self.doLogoutFailureCallBack.bind(self));
    };

    /** 
     * handels the logout success schenario
    */
    NUOPresentationController.prototype.doLogoutSuccessCallBack = function () {
        var self = this;
        self.deRegisterIdleTimeout();
    };

    /** 
    * handels the logout Failure  schenario
    */
    NUOPresentationController.prototype.doLogoutFailureCallBack = function () {
        var self = this;
        self.logoutErrorCallback();
    };

    /**
    * Used to show the error downtime screen when the logout fails
   */
    NUOPresentationController.prototype.logoutErrorCallback = function () {
        var context = {
            "action": "ServerDown"
        };
        kony.store.setItem('OLBLogoutStatus', context);
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        //Presenting the Login form without a viewmodel first as a work around for preShow of form being called after willUpdateUI.
        authModule.presentationController.presentUserInterface('frmLogin', {});
        authModule.presentationController.presentUserInterface('frmLogin', context);
    };

    /**
    * deRegisterIdleTimeout : used to deregister the idle TimeOut 
   */
    NUOPresentationController.prototype.deRegisterIdleTimeout = function () {
        var self = this;
        self.unRegisterForIdleTimeout(self.deregisterSuccessCallback);
    };

    /**
    * deregisterSuccessCallback : used to deregister when idle TimeOut occurs
   */
    NUOPresentationController.prototype.deregisterSuccessCallback = function () {
        window.location.reload();
    };

    /**
     * Entry method to User Information Step(Step 3)
     */
    NUOPresentationController.prototype.navigateToUserInformationStep = function () {
        this.presentNUO({
            "showUserInformationStep": "showUserInformationStep"
        });
    };

    /**
     * Entry method to Employment Information Step(Step 4)
     */
    NUOPresentationController.prototype.navigateToEmploymentInformationStep = function () {
        this.presentNUO({
            "showEmploymentInformationViewModel": "showEmploymentInformationViewModel"
        });
    };

    /**
     * Entry method to Financial Information Step(Step 5)
     */
    NUOPresentationController.prototype.navigateToFinancialInformationStep = function () {
        this.presentNUO({
            "showFinancialInformationViewModel": "showFinancialInformationViewModel"
        });
    };

    /**
     * Method to create user information 
     * @param {Object}  userInfo contains the details of the user 
     * @param {Object}  context which indicates whether to logout
     */
    NUOPresentationController.prototype.createUserInformation = function (userInfo, context) {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.customCreatePersonalInfo(userInfo, self.createUserInformationSuccessCallBack.bind(self, userInfo, context),
            self.createUserInformationFailureCallBack.bind(self));
    };

    /**
     * Method to create user information success schenario
     * @param {Object}  userInfo contains the details of the user 
     * @param {string} context context
     * @param {object} response response
     */
    NUOPresentationController.prototype.createUserInformationSuccessCallBack = function (userInfo, context, response) {
        var self = this;
        self.hideProgressBar();
        if (context && context.action === "saveAndClose") {
            self.doLogout();
        } else {
            if (userInfo.informationType === "PersonalInfo") {
                self.navigateToEmploymentInformationStep();
            } else if (userInfo.informationType === "EmploymentInfo") {
                self.navigateToFinancialInformationStep();
            } else {
                self.navigateToUploadDocumentsStep();
            }
        }
    };

    /**
   * Method to create user information failure schenario
   */
    NUOPresentationController.prototype.createUserInformationFailureCallBack = function () {
        var self = this;
        self.showErrorScreen();
    };

    /**
     * Method to get user information 
     * @member NUOPresentationController
     * @param {object} viewModelName viewModelName
     */
    NUOPresentationController.prototype.getUserInformation = function (viewModelName) {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.customGetUserPersonalInfo(self.getUserInformationSuccessCallBack.bind(self,viewModelName), self.getUserInformationFailureCallBack.bind(self));
    };

    /** 
     * Method to get user information success schenario
       * @param {object} viewModelName viewModelName 
     * @param {object} response response
    */
    NUOPresentationController.prototype.getUserInformationSuccessCallBack = function (viewModelName, response) {
        var self = this;
        var viewModel = {};
        self.hideProgressBar();
        viewModel[viewModelName] = {
            data: response[0]
        };
        self.presentNUO(viewModel);
    };

    /** 
   * Method to get user information Failure schenario
   * @param {object} response response
  */
    NUOPresentationController.prototype.getUserInformationFailureCallBack = function (response) {
        var self = this;
        self.showErrorScreen();
    };

    /**
     * Entry point function to show documents upload screen
     */
    NUOPresentationController.prototype.navigateToUploadDocumentsStep = function () {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.customGetUserPersonalInfo(self.navigateToUploadDocumentsStepSuccessCallBack.bind(self), self.navigateToUploadDocumentsStepFailureCallBack.bind(self));
    };

    /** 
     * used to handels the  upload documents Step success schenario
     * @param {object} response response
    */
    NUOPresentationController.prototype.navigateToUploadDocumentsStepSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            showUploadDocuments: {
                employementStatus: response[0].employmentInfo
            }
        })
    };

    /** 
   * used to handels the  upload documents Step error schenario
   * @param {object} response response
  */
    NUOPresentationController.prototype.navigateToUploadDocumentsStepFailureCallBack = function (response) {
        var self = this;
        self.showErrorScreen();
    };

    /**
     * Upload documents
     * @param {object} files files
     */
    NUOPresentationController.prototype.uploadDocuments = function (files) {
        var self = this;
        var addressProofManager = [];
        var employmentProofManager = [];
        var incomeProofManager = [];
        var addressProof = {
            document: files.addressProof,
            documentType: "Address"
        };
        addressProofManager.push(addressProof);
        var employmentProof = {
            document: files.employmentProof,
            documentType: "Employment"
        };
        employmentProofManager.push(employmentProof);
        var incomeProof = {
            document: files.incomeProof,
            documentType: "Income"
        };
        incomeProofManager.push(incomeProof);
        self.showProgressBar();
        //Adding comments for a known MF issue
        var asyncManager = applicationManager.getAsyncManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(self.newUserBusinessManager, 'uploadDocumentForNewUser', addressProofManager),
                asyncManager.asyncItem(self.newUserBusinessManager, 'uploadDocumentForNewUser', employmentProofManager),
                asyncManager.asyncItem(self.newUserBusinessManager, 'uploadDocumentForNewUser', incomeProofManager)
            ],
            self.uploadDocumentForNewUserCompletionCallBack.bind(self)
        );
    };
    /** 
     * used to handels the upload documents success schenario
     * @param {Object} syncResponseObject response
     */
    NUOPresentationController.prototype.uploadDocumentForNewUserCompletionCallBack = function (syncResponseObject) {
        var scopeObj = this;
        if (syncResponseObject.isAllSuccess()) {
            scopeObj.hideProgressBar();
            scopeObj.presentNUO({
                documentsSaved: "documentsSaved"
            })
        } else {
            scopeObj.showErrorScreen();
        }
    };

    /**
     * get the Identity VerificationQuestion
     * @param {string} context context 
     */
    NUOPresentationController.prototype.getIdentityVerificationQuestion = function (context) {
        var self = this;
        self.presentNUO({
            identityVerificationQuestions: "identityVerificationQuestions"
        });
        //To do - Integrate with actual service to fetch SSN security questions
        //Call service to fetch Identity Verification Questions
    };
    /**
     * used to get the userState 
     * @param {function} successCallBack  successCallBack
     */
    NUOPresentationController.prototype.getUserState = function (successCallBack) {
        var self = this;
        self.newUserBusinessManager.customRequestState(self.getUserStateSuccessCallBack.bind(self, successCallBack), self.getUserStateFailureCallBack.bind(self));
    };


    /**
     * used to get the userState  success schenario
     * @param {function} successCallBack successCallBack
     * @param {object} response response 
     */
    NUOPresentationController.prototype.getUserStateSuccessCallBack = function (successCallBack, response) {
        successCallBack(response[0]);
    };

    /**
   * used to get the userState  Failure schenario
   * @param {object} response response 
   */
    NUOPresentationController.prototype.getUserStateFailureCallBack = function (response) {
        var self = this;
        self.showErrorScreen();
    };
    /**
     * Method to get user information 
     * @param {object} userNameAndPassword userNameAndPassword
     */
    NUOPresentationController.prototype.loginAndContinueApplication = function (userNameAndPassword) {
        var self = this;
        self.showProgressBar();
        self.doLogin(userNameAndPassword, function (loginResponse) {
            self.getUserState(self.continueOrResetApplication.bind(self, loginResponse))
        },
            self.showLoginError.bind(this)
        )
    };

    /**
     * showLoginError
     * @param {string} loginErrorResponse login error response
     */
    NUOPresentationController.prototype.showLoginError = function (loginErrorResponse) {
        this.hideProgressBar();
        this.presentNUO({
            loginError: loginErrorResponse.details.message
        })
    }

    function isApplicationComplete(userState) {
        var keys = ["userProducts", "userPersonalInfo", "userEmploymentInfo", "userFinancialInfo", "userSecurityQuestions"];
        var factor = 0;
        keys.forEach(function (key) {
            if (userState[key] === "true") {
                factor++;
            }
        })
        if (factor === keys.length) {
            return true;
        }
        return false;
    }

    /**
     * continue Or Reset Application
     * @param {object} userDetails  userDetails
     * @param {string} userState user state 
     */
    NUOPresentationController.prototype.continueOrResetApplication = function (userDetails, userState) {
        this.hideProgressBar();
        if (isApplicationComplete(userState)) {
            this.presentNUO({
                uploadSignature: {}
            })
        }
        else {
            this.presentNUO({
                continueOrResetApplication: {
                    progress: this.calculateProgress(userState),
                    userState: userState,
                    userDetails: userDetails
                }
            })
        }

    }
    /**
     * findStageAndContinueApplication
     * @param {object} userDetails user Details
     * @param {string} userState user State
     */
    NUOPresentationController.prototype.findStageAndContinueApplication = function (userDetails, userState) {
        if (userState.userProducts === "false") {
            this.navigateToProductSelection({ isRevisiting: true });
        } else if (userState.userPersonalInfo === "false") {
            this.navigateToUserInformationStep();
        } else if (userState.userEmploymentInfo === "false") {
            this.navigateToEmploymentInformationStep();
        } else if (userState.userFinancialInfo === "false") {
            this.navigateToFinancialInformationStep();
        } else if (userState.userSecurityQuestions === "false") {
            this.navigateToUploadDocumentsStep();
        } else if (userState.creditCheck === "false") {
            this.navigateToCreditCheckError();
        }
        else {
            this.navigateToCreditCheckSuccess();
        }
    }
    /**
     * navigateToCreditCheckError 
     */
    NUOPresentationController.prototype.navigateToCreditCheckError = function () {
        this.presentNUO({
            userCreditCheckError: {}
        })
    }

    /** 
     * navigateToCreditCheckSuccess
    */
    NUOPresentationController.prototype.navigateToCreditCheckSuccess = function () {
        this.presentNUO({
            userCreditCheckSuccess: {}
        })
    }

    /**
     * calculateProgress 
     * @param {string} userState user State
     * @return {object} data
     */
    NUOPresentationController.prototype.calculateProgress = function (userState) {
        var factor = 1;
        var keys = ["userProducts", "userPersonalInfo", "userEmploymentInfo", "userFinancialInfo", "userSecurityQuestions", "creditCheck"];
        keys.forEach(function (key) {
            if (userState[key] === "true") {
                factor++;
            }
        })
        return (factor / (keys.length + 1)) * 100;
    }

    /**
     * reset Application
     * @param {*} userDetails userDetails
     * @param {*} userState userState
     */
    NUOPresentationController.prototype.resetApplication = function (userDetails, userState) {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.resetNewUserData(self.resetNewUserDataSuccessCallBack.bind(self),
            self.resetNewUserDataFailureCallBack.bind(self));
    };
    /** 
     * used to reset NewUserData Success Schenario
     * @param {object} response response
    */
    NUOPresentationController.prototype.resetNewUserDataSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.navigateToProductSelection();
    };
    /** 
 * used to reset NewUserData Failure Schenario
 * @param {object} response response
*/
    NUOPresentationController.prototype.resetNewUserDataFailureCallBack = function (response) {
        var self = this;
        self.showErrorScreen();
    };
    /**
     * doIdentityVerification
     * @param {object} response reponse 
     */
    NUOPresentationController.prototype.doIdentityVerification = function (response) {
        var self = this;
        self.showProgressBar();
        //As of now calling this service to update "UserSecurityQuestion" Flag
        self.newUserBusinessManager.customCreatePersonalInfo({ "informationType": "SecurityQuestions" },
            self.customCreatePersonalInfoSuccessCallBack.bind(self),
            self.customCreatePersonalInfoFailureCallBack.bind(self))
        //Call here actual service to do Identity Verification 
    };

    /** 
     * custom CreatePersonal InfoSuccessCallBack
     * @param {object} response response
    */
    NUOPresentationController.prototype.customCreatePersonalInfoSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            identityVerificationSuccess: "identityVerificationSuccess"
        })
    };

    /** 
      * custom CreatePersonal Failure Call Back
      * @param {object} response response
     */
    NUOPresentationController.prototype.customCreatePersonalInfoFailureCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            identityVerificationError: response
        })
    };

    /** 
     * used to get the credit check
    */
    NUOPresentationController.prototype.userCreditCheck = function () {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.customGetUserPersonalInfo(self.customGetUserPersonalInfoSuccessCallBack.bind(self), self.customGetUserPersonalInfoFailureCallBack.bind(self));
    }
    /** 
     * handels the customGetUserPersonalInfoSuccess schenario
     * @param {object} response response
    */
    NUOPresentationController.prototype.customGetUserPersonalInfoSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.doUserCreditCheckWithSSN(response[0].ssn);

    };
    /** 
 * handels the customGetUserPersonalInfoSuccess schenario
 * @param {object} response response
*/
    NUOPresentationController.prototype.customGetUserPersonalInfoFailureCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.showErrorScreen();

    };
    /**
     * doUserCreditCheckWithSSN
     * @param {*} ssn ssn number
     */
    NUOPresentationController.prototype.doUserCreditCheckWithSSN = function (ssn) {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.userCreditCheck({ ssn: ssn }, self.userCreditCheckSuccessCallBack.bind(self), self.userCreditCheckFailureCallBack.bind(self));
    };
    /** 
     * user CreditCheckSuccess schenario
    */
    NUOPresentationController.prototype.userCreditCheckSuccessCallBack = function () {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            userCreditCheckSuccess: "userCreditCheckSuccess"
        })
    };
    /** 
      * user CreditCheckSuccess schenario
     */
    NUOPresentationController.prototype.userCreditCheckFailureCallBack = function () {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            userCreditCheckError: "userCreditCheckError"
        })
    };

    /**
     * uploadSignature
     * @param {object} file file 
     */
    NUOPresentationController.prototype.uploadSignature = function (file) {
        var self = this;
        self.showProgressBar();
        self.newUserBusinessManager.signatureUpload({ signatureImage: "abc" }, self.uploadSignatureSuccessCallBack.bind(self),
            self.uploadSignatureFailureCallBack.bind(self));
    };

    /**
     * upload Signature Success schenario
     * @param {*} response response
     */
    NUOPresentationController.prototype.uploadSignatureSuccessCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.presentNUO({
            uploadSignatureSuccess: "uploadSignatureSuccess"
        });
    };

    /**
     * upload Signature Failure schenario 
     * @param {*} response response
     */
    NUOPresentationController.prototype.uploadSignatureFailureCallBack = function (response) {
        var self = this;
        self.hideProgressBar();
        self.showErrorScreen();
    };

    /** 
     * used to show the enroll form
    */
    NUOPresentationController.prototype.showEnrollForm = function () {
        var self = this;
        self.showProgressBar();
        self.authManager.getUserNamePoliciesForEnroll(self.getUserNamePoliciesForEnrollSuccessCallBack.bind(self),
            self.getUserNamePoliciesForEnrollFailureCallBack.bind(self));
    };
    /** 
     * getUserNamePoliciesForEnrollSuccessCallBack
     * @param {object} response response
    */
    NUOPresentationController.prototype.getUserNamePoliciesForEnrollSuccessCallBack = function (response) {
        var self = this;
        kony.mvc.MDAApplication.getSharedInstance().appContext.nuorules = response.records;
        self.hideProgressBar();
        self.presentNUO({
            usernamepasswordrules: response.records
        })
    }
    /** 
  * getUserNamePoliciesForEnrollFailureCallBack
  * @param {object} response response
 */
    NUOPresentationController.prototype.getUserNamePoliciesForEnrollFailureCallBack = function (response) {
        var self = this;
        self.showErrorScreen();
    }

    /** 
     * used to unregister For IdleTimeout
     * @param {function} completeCallBack completeCallBack
    */
    NUOPresentationController.prototype.unRegisterForIdleTimeout = function (completeCallBack) {
        kony.application.unregisterForIdleTimeout();
        completeCallBack();
    };

    return NUOPresentationController;
});
