define([],function () {

  	function NUO_BusinessController(){
      	kony.mvc.Business.Controller.call(this);
    }

  	inheritsFrom(NUO_BusinessController,kony.mvc.Business.Controller);
	
	NUO_BusinessController.prototype.initializeBusinessController = function(){

    };

    NUO_BusinessController.prototype.execute = function(command)
    {
      kony.mvc.Business.Controller.prototype.execute.call(this,command);
    };

    return NUO_BusinessController;
});
