define(['OLBConstants','CommonUtilities'], function (OLBConstants,CommonUtilities) {
  function BusinessBankingPresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
    this.initializePresentationController();
  }

  inheritsFrom(BusinessBankingPresentationController, kony.mvc.Presentation.BasePresenter);

  BusinessBankingPresentationController.prototype.initializePresentationController = function () {
    this.navManager = applicationManager.getNavigationManager();
    this.frmUserManagementForm = "frmUserManagement";
    this.frmBBUsersDashboardForm = "frmBBUsersDashboard";
    this.businessUserManager = applicationManager.getBusinessUserManager();
	this.authManager = applicationManager.getAuthManager();
    this.formatUtil = applicationManager.getFormatUtilManager();

    this.defaultSortConfig = {
      'sortBy': 'createdts',
      'order': OLBConstants.DESCENDING_KEY,
      'offset': OLBConstants.DEFAULT_OFFSET,
      'limit': OLBConstants.PAGING_ROWS_LIMIT
    };
  };


  /**
   * Entry Method for business banking module
   * @param {object} context - context object for 
   */
  BusinessBankingPresentationController.prototype.showUserManagent = function (context) {
    switch (context.show) {
      case "createNewUser":
        this.showCreateUser();
        break;
      case "showAllUsers": {
        this.navigateToUsers();
        break;
      }
      default:
      //no context.
    }
  };

  /**
   * Method to navigate crete user ui.
   * @param {object} userObj - user object /user id for update User - as per final imp .
   */
  BusinessBankingPresentationController.prototype.showCreateUser = function (userObj) {
    this.navManager.navigateTo(this.frmUserManagementForm);
    if (userObj) { //update user ui
      this.navManager.updateForm({
        "updateUser": this.businessUserManager.getUserObject(),
		    "id": this.businessUserManager.getUserAttribute("id")
      }, this.frmUserManagementForm);
    } else {
	  this.businessUserManager.clearDataMembers();
      this.businessUserManager.createUserObject();
      this.navManager.updateForm({
        "createNewUser": "createNewUser"
      }, this.frmUserManagementForm);
    }

  };
  
   /** 
     * used to show the username policies
     */
  BusinessBankingPresentationController.prototype.getUserNamePolicies = function() {
      var self = this;
      applicationManager.getUserPreferencesManager().fetchUsernameRulesAndPolicy(self.getUserNamePoliciesSuccessCallBack.bind(this), self.onServerError.bind(this));
  };
  /** 
   * getUserNamePoliciesSuccessCallBack
   * @param {object} response response
   */
  BusinessBankingPresentationController.prototype.getUserNamePoliciesSuccessCallBack = function(response) {      
      var validationUtility = applicationManager.getValidationUtilManager();
      validationUtility.createRegexForUsernameValidation(response.usernamerules);
      this.navManager.updateForm({
          "userNamePolicies": {
              usernamerules: response.usernamepolicy
          },
          progressBar: false
      }, this.frmUserManagementForm);
  };
  /**
   * Method to handle server error.
   * @param {object} errorResponse - error response object
   */
  BusinessBankingPresentationController.prototype.onServerError = function (errorResponse) {
    this.navManager.updateForm({
      serverError: true,
      errorMessage: errorResponse.errorMessage,
      progressBar: false
    })
  };
  /**
   * Method to update user details in user module.
   * @param {object} userObj - user details
   */
  BusinessBankingPresentationController.prototype.updateUserDetails = function (userObj) {
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.validateUser({
      "Ssn": userObj.ssn,
      "DateOfBirth": CommonUtilities.sendDateToBackend(userObj.dob)
    }, this.onValidateUserSuccess.bind(this, userObj), this.onServerError.bind(this));
  };

  /**
   * Method to update user details in user module on sucess of valid user.
   * @param {object} userObj - user details
   * @param {object} response - respose object
   */
  BusinessBankingPresentationController.prototype.onValidateUserSuccess = function (userObj, response) {
    this.navManager.updateForm({
      progressBar: true
    }, this.frmUserManagementForm);
    if (response.isValid) {
      
      this.businessUserManager.setUserAttribute("FirstName", userObj.firstName);
      this.businessUserManager.setUserAttribute("MiddleName", userObj.middleName);
      this.businessUserManager.setUserAttribute("LastName", userObj.lastName);
      this.businessUserManager.setUserAttribute("Email", userObj.email);
      this.businessUserManager.setUserAttribute("Ssn", userObj.ssn);
      this.businessUserManager.setUserAttribute("DrivingLicenseNumber", userObj.driverLicenseNumber);
      this.businessUserManager.setUserAttribute("Phone", userObj.phoneNumber);
      this.businessUserManager.setUserAttribute("UserName", userObj.userName);
      this.businessUserManager.setUserAttribute("DateOfBirth", CommonUtilities.sendDateToBackend(userObj.dob));
      this.fetchRoles();
    } else {
      this.navManager.updateForm({
        "invalidUserError": response,
        progressBar: false
      }, this.frmUserManagementForm);
    }
  };

  /**
   * Validate username availability errorcallback 
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUsernameError = function (response) {
    this.navManager.updateForm({
      "invalidUserError": response,
      progressBar: false
    }, this.frmUserManagementForm);
  };
  
  /**
   * Validate user name availability
   * @param {object} username  user name
   * */
  BusinessBankingPresentationController.prototype.validateUserName = function (username) {
    this.navManager.updateForm({ progressBar: true }, this.frmUserManagementForm)
    this.businessUserManager.validateUserName(username, this.onValidateUserNameSuccess.bind(this), this.onValidateUserNameError.bind(this));
  };

  /**
   * Validate user name availability success callback
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUserNameSuccess = function (response) {
    this.navManager.updateForm({
      userNameAvailability: { isAvailable: response.isAvailable },
      progressBar: false
    }, this.frmUserManagementForm);
  };
  
  /**
   * Validate user name availability errorcallback 
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUserNameError = function (response) {
    this.navManager.updateForm({
      "invalidUser": true,
      progressBar: false
    }, this.frmUserManagementForm);
  };

  /**
   * Method to know if its Edit flow or Create flow
   */
  BusinessBankingPresentationController.prototype.isEditMode = function() {
    return !(this.businessUserManager.getUserAttribute("id") === undefined || this.businessUserManager.getUserAttribute("id") === null || this.businessUserManager.getUserAttribute("id") === "");
    
  };

  /**
   * Method to fetch user roles.
   */
  BusinessBankingPresentationController.prototype.fetchRoles = function () {    
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.getUserRoles(this.onFetchUserRolesSuccess.bind(this), this.onServerError.bind(this));
  };

  /**
   * fetch user roles success scenario
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onFetchUserRolesSuccess = function (response) {
    if(this.isEditMode()){
      this.navManager.navigateTo(this.frmUserManagementForm);
    }
    this.navManager.updateForm({
      userRoles: response,
      selectedRoleId: this.businessUserManager.getUserAttribute("Role_id"),
      id : this.businessUserManager.getUserAttribute("id"),
      progressBar: false
    }, this.frmUserManagementForm);
  };

  /**
   * update user roles
   * @param {object} roleObj - response object
   * */
  BusinessBankingPresentationController.prototype.confirmRole = function (roleObj) {
    var prevRoleId = this.businessUserManager.getUserAttribute("Role_id");
    this.businessUserManager.setUserAttribute("Role_id", roleObj.id);// create user
    this.businessUserManager.setUserAttribute("Group_Name", roleObj.Name); //for user ack screen
    this.businessUserManager.setUserAttribute("Group_Description", roleObj.Description); //for user ack screen
	  if(roleObj.id !== prevRoleId){
    	this.businessUserManager.resetTransactionLimits();
		this.businessUserManager.setUserAttribute("services",[]);
    }
  };

  /**
 * Method to navigate to user management dashboard and fetch list of users
 * @param {JSON} viewModel - consists context and parameters
 */
  BusinessBankingPresentationController.prototype.navigateToUsers = function (sortingInputs) {
    this.navManager.navigateTo(this.frmBBUsersDashboardForm);
    this.navManager.updateForm({ "progressBar": true }, this.frmBBUsersDashboardForm);
    applicationManager.getPaginationManager().resetValues();
    var searchString = sortingInputs ? sortingInputs.searchString : null;
    var params = {};
    params = applicationManager.getPaginationManager().getValues(this.defaultSortConfig, sortingInputs);
    if (typeof searchString === "string" && searchString.length > 0) {
      params.searchString = searchString;
    } else if(searchString !== null && searchString !== undefined){
      if(searchString.trim().length === 0){
        params.sortBy = 'createdts';
        params.order = OLBConstants.DESCENDING_KEY;
      }
    }
    else{}
    params.limit = "";
    params.offset = "";
    this.businessUserManager.getAllUsers(params, this.fetchSubUsersSuccess.bind(this, searchString), this.onServerError.bind(this));
  };

  /** Need to add more fields when the service is called 
  * Method to handle success response of fetch sub users
  * @param {object} viewModel - which consists of response from backend
  */
  BusinessBankingPresentationController.prototype.fetchSubUsersSuccess = function (searchString, viewModel) {
    var paginationValues = applicationManager.getPaginationManager().getValues(this.defaultSortConfig);
    if(searchString !== null && searchString !== undefined){
      if(searchString.trim().length === 0){
        paginationValues.sortBy = 'createdts';
        paginationValues.order = OLBConstants.DESCENDING_KEY;
      }
    }
    paginationValues.limit = viewModel.length;
    if (viewModel.length > 0 || paginationValues.offset === 0) {
      this.navManager.updateForm({
        "allUsers": {
          users: viewModel,
          config: paginationValues,
          searchString: searchString
        }
      }, this.frmBBUsersDashboardForm);
    } else if (paginationValues.offset !== 0) {
      this.navManager.updateForm({ noMoreRecords: true });
    }
  };

  /** Need to add more fields and parameters when the service is called 
  * Method to update user status
  */
  BusinessBankingPresentationController.prototype.setUserStatus = function (params) {
    this.businessUserManager.updateUserStatus(params, this.setUserStatusSuccess.bind(this),  this.onServerError.bind(this));
  };
  /** Need to add more fields when the service is called 
    * Method to handle success response of updating user status
    * @param {object} viewModel - which consists of response from backend
    */
  BusinessBankingPresentationController.prototype.setUserStatusSuccess = function (viewModel) {
    //Need to handle the success senario based on the response, when the call is successful
    var viewProperties = {};
    viewProperties.statusSuccess = viewModel;
    viewProperties.progressBar = false;
    this.navManager.updateForm(viewProperties, this.frmBBUsersDashboardForm);

  };
  /**Method to make service call to fetch Accounts
  */
  BusinessBankingPresentationController.prototype.showAllAccounts = function () {
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.getAllAccounts(this.fetchAccountsSuccess.bind(this), this.onServerError.bind(this));
  };

  /** Method for fetch Accounts success
  * @param {object} response - which consists of response from backend
  */
	BusinessBankingPresentationController.prototype.fetchAccountsSuccess = function(response) {
		if(this.isEditMode()){
      this.navManager.navigateTo(this.frmUserManagementForm);
    }
		this.navManager.updateForm({
			progressBar: false,
			accounts: response,
			selectedAccounts: this.businessUserManager.getUserAttribute("accounts"),
			id : this.businessUserManager.getUserAttribute("id")
		}, this.frmUserManagementForm);
  };

  /** Method to update the selected accounts in the Business Manager
   * @param {object} selectedAccounts - which consists of list of selected accounts of the user
   */
  BusinessBankingPresentationController.prototype.confirmAccounts = function (selectedAccounts) {
    this.businessUserManager.setUserAttribute("accounts", selectedAccounts);
  };

  /**Method to make service call to fetch Transaction Limits
  */
  BusinessBankingPresentationController.prototype.fetchTransactionLimits = function () {
    this.navManager.updateForm({
      progressBar: true
    }, this.frmUserManagementForm);
    this.businessUserManager.getTransactionLimits(this.businessUserManager.getUserAttribute("Role_id"), this.fetchTransactionLimitsSuccess.bind(this), this.onServerError.bind(this));
  };

  /** Method for fetch Transaction Limit success
   * @param {object} response - which consists of response from backend
   */
  BusinessBankingPresentationController.prototype.fetchTransactionLimitsSuccess = function (response) {
    if (response.length) {
      this.navManager.updateForm({
        progressBar: false,
        transactionLimits: response,
		    selectedLimits: this.businessUserManager.getUserAttribute("services"),
		    id : this.businessUserManager.getUserAttribute("id")
      }, this.frmUserManagementForm);
    } else {
      this.businessUserManager.createOrUpdateUser(this.createOrUpdateUserSuccess.bind(this), this.onServerError.bind(this));
    }
  };

  BusinessBankingPresentationController.prototype.addTransactionLimitsOnBack = function (transactionLimits) {
    this.businessUserManager.setUserAttribute("services", transactionLimits);
  };

  /** Method to update the transaction limits in the Business Manager
   * @param {object} transactionLimits - which consists of list of transaction limits of the user
   */
  BusinessBankingPresentationController.prototype.confirmTransactionLimits = function (transactionLimits) {
    this.businessUserManager.setUserAttribute("services", transactionLimits);
    this.businessUserManager.createOrUpdateUser(this.createOrUpdateUserSuccess.bind(this), this.onServerError.bind(this));
  };

  /** Method for fetch Transaction Limit success
   * @param {object} response - which consists of response from backend
   */
  BusinessBankingPresentationController.prototype.createOrUpdateUserSuccess = function (response) {
    if (response) {
      var userObj = this.businessUserManager.getUserObject();
      
      this.navManager.updateForm({
        progressBar: false
      });
      if(response.errorMessage){
        this.navManager.updateForm({
          progressBar: false,
          createUserFail: true,
          errMsg: response.errorMessage
        });
      } else {
        this.navManager.navigateTo(this.frmBBUsersDashboardForm);    
        this.businessUserManager.clearDataMembers();    
        this.navManager.updateForm({
          progressBar: false,
          createUserSuccess: true,
          referenceNumber: response.id,
          userObject: userObj
        });
        
      }

     
    }
  };

  /** Method to resend Activation Link
  * @param {String} username - contains username
  */
  BusinessBankingPresentationController.prototype.resendActivationLink = function (username) {
    this.businessUserManager.resendActivationLink(username, this.resendActivationLinkSuccess.bind(this), this.resendActivationLinkFailure.bind(this));
  };

  /** Method to handle success response of resendActivationLink
  * @param {object} response - which consists of response from backend
  */
  BusinessBankingPresentationController.prototype.resendActivationLinkSuccess = function () {
    this.navManager.updateForm({
      "activationLinkSuccess": true,
      "progressBar": false
    }, this.frmBBUsersDashboardForm);

  };

  /** Method to handle failure response of resendActivationLink
  */
  BusinessBankingPresentationController.prototype.resendActivationLinkFailure = function () {
    this.navManager.updateForm({
      "serverError": true,
      "progressBar": false
    });

  };

  /** Method to fetch User details
  * @param {String} username - contains username
  */
  BusinessBankingPresentationController.prototype.fetchUserDetails = function (username) {
    this.businessUserManager.fetchUserDetails(username, this.fetchUserDetailsSuccess.bind(this), this.fetchUserDetailsFailure.bind(this));


  };

  /** Method to handle success response of fetchUserDetails
  * @param {object} userObject - which consists of response from backend
  */
  BusinessBankingPresentationController.prototype.fetchUserDetailsSuccess = function (userObject) {
    this.updateUserObjManager(userObject);
    this.navManager.navigateTo(this.frmBBUsersDashboardForm);
    this.navManager.updateForm({
      "manageUser": userObject,
      "progressBar": false
    }, this.frmBBUsersDashboardForm);

  };

  /** Method to handle failure response of fetchUserDetails
  */
  BusinessBankingPresentationController.prototype.fetchUserDetailsFailure = function () {
    this.navManager.updateForm({
      "serverError": true,
      "progressBar": false
    });

  };

  /** Method to UPDATE User object in manager
   * @param {object} userObj - which consists of the user object
  */
  BusinessBankingPresentationController.prototype.updateUserObjManager = function (userObj) {
    this.businessUserManager.createUserObject();
    this.businessUserManager.setUserAttribute("id", userObj.id);
    this.businessUserManager.setUserAttribute("FirstName", userObj.FirstName);
    this.businessUserManager.setUserAttribute("MiddleName", userObj.MiddleName);
    this.businessUserManager.setUserAttribute("LastName", userObj.LastName);
    this.businessUserManager.setUserAttribute("Email", userObj.Email);
    this.businessUserManager.setUserAttribute("Ssn", userObj.Ssn);
    this.businessUserManager.setUserAttribute("DrivingLicenseNumber", userObj.DrivingLicenseNumber);
    this.businessUserManager.setUserAttribute("Phone", userObj.Phone);
    this.businessUserManager.setUserAttribute("UserName", userObj.Username);
    //this.businessUserManager.setUserAttribute("DateOfBirth", //this.formatUtil.getFormatedDateString(this.formatUtil.getDateObjectfromString(userObj.DateOfBirth, "MM/DD/YYYY"), //this.formatUtil.getBackendDateFormat()));
	this.businessUserManager.setUserAttribute("DateOfBirth",userObj.DateOfBirth);
    this.businessUserManager.setUserAttribute("Role_id", userObj.Group_id);
    this.businessUserManager.setUserAttribute("Group_Name", userObj.Group_Name);
    this.businessUserManager.setUserAttribute("Group_Description", userObj.Group_Description);
    this.businessUserManager.setUserAttribute("accounts", userObj.accounts);
    this.businessUserManager.setUserAttribute("services", userObj.services);
  };

  BusinessBankingPresentationController.prototype.showPrintPage = function(data){
    var self = this;
    data.printKeyValueGroupModel.printCallback = function(){
         self.onPrintCancel();
    }
    applicationManager.getNavigationManager().navigateTo('frmPrintTransfer');
    applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
  };

  BusinessBankingPresentationController.prototype.onPrintCancel = function(){
      this.navManager.navigateTo(this.frmBBUsersDashboardForm);
      this.navManager.updateForm({ "isPrintCancelled": true }, this.frmBBUsersDashboardForm);
  };

  return BusinessBankingPresentationController;
});