define(["CommonUtilities"], function(CommonUtilities) {
    function PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
        this.isAccountsLoading = false;
        this.mapAccounts = false;
        this.fetchPasswordWarningAndOutageMessages = true;
    }
    var currentAccount = null;
  	var scope = this;
    inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);
    PresentationController.prototype.initializePresentationController = function() {
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        this.scheduleTransactonsConfig = {
            All: {
                sortBy: "scheduledDate",
                defaultSortBy: "scheduledDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            }
        };

        this.sortConfig = {
            All: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Checks: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Deposits: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Transfers: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Withdrawals: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Payments: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Purchases: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            },
            Interest: {
                sortBy: "transactionDate",
                defaultSortBy: "transactionDate",
                order: OLBConstants.DESCENDING_KEY,
                defaultOrder: OLBConstants.DESCENDING_KEY,
                offset: OLBConstants.DEFAULT_OFFSET,
                limit: OLBConstants.PAGING_ROWS_LIMIT
            }
        };

        var self = this;
        this.Transactions = {
            showTransactionsByType: function(dataInputs) {
                this.showSelectedAccountTransaction(null, dataInputs);
            }.bind(self),
            showAll: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.ALL
                });
            }.bind(self),
            showChecks: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.CHECKS
                });
            }.bind(self),
            showDeposits: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.DEPOSITS
                });
            }.bind(self),
            showTransfers: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.TRANSFERS
                });
            }.bind(self),
            showWithdrawals: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.WITHDRAWLS
                });
            }.bind(self),
            showInterest: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.INTEREST
                });
            }.bind(self),
            showPurchase: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.PURCHASES
                });
            }.bind(self),
            showPayment: function(dataInputs) {
                var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
                this.showSelectedAccountTransaction(null, dataInputs || {
                    resetSorting: true,
                    transactionType: OLBConstants.TRANSACTION_TYPE.PAYMENTS
                });
            }.bind(self)
        };
    };

    /**
     * Entery method for accounts landing page. This method inturns calls 3 methods to get data for different section of form
     */
    PresentationController.prototype.showAccountsDashboard = function(params) {
        params = params || {};
        applicationManager.getNavigationManager().navigateTo("frmAccountsLanding");
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsLanding");
        this.showWelcomeBanner();
        this.showAllAccounts(params.noRefresh);
        this.loadAccountsLandingComponents();
    };
    /**
     * Method takes you to server error page in case if a critical/required service call fails.
     */
    PresentationController.prototype.showOnServerError = function() {
        CommonUtilities.showServerDownScreen();
    };
    PresentationController.prototype.updateFeedBackId = function(userData) {
        if(applicationManager.getConfigurationManager().isFeedbackEnabled === "true"){
            applicationManager.getStorageManager().setStoredItem("feedbackUserId", userData.feedbackUserId);
        }
        applicationManager.getFeedbackManager().updateFeedbackId(null);
    }    
    /**
     * Method to show Welcome banner in accounts Landing page like last logged in time/User Name etc.
     */
    PresentationController.prototype.showWelcomeBanner = function() {
        var userPreferenceManager = applicationManager.getUserPreferencesManager();
        var cachedUser = userPreferenceManager.getUserObj();
        if (cachedUser) {
            this.fetchWelcomeBannerSuccess(cachedUser);
            this.updateFeedBackId(cachedUser); 
        } else {
            this.showOnServerError.bind(this);
        }
    };
    /**
     * Method to recieve backend data of the current User for populating data in welcome banner
     * @param {JSON} userData User Data containing all details of current loggedin user
     */
    PresentationController.prototype.fetchWelcomeBannerSuccess = function(userData) {
        var dateObj = applicationManager.getFormatUtilManager().getDateObjectfromString(userData.lastlogintime, "YYYY-MM-DDTHH:MM:SS");
        var lastLoggedInTime = applicationManager.getFormatUtilManager().getFormatedDateString(dateObj, "m/d/Y h:m A");
        userData.lastlogintime = lastLoggedInTime;
        applicationManager.getNavigationManager().updateForm({
            welcomeBanner: userData
        }, "frmAccountsLanding");
    };



    PresentationController.prototype.fetchAccounts = function (userName) {
        var self = this;
        function completionCallback (accountsData) {
            this.accounts = accountsData;
            this.isAccountsLoading = false;
            if (this.mapAccounts) {
                this.fetchAccountsSuccess();
            }
        }
        this.isAccountsLoading = true;
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            function completionCallback(asyncResponse) {
                if (asyncResponse.isAllSuccess()) {
                    self.accounts = asyncResponse.responses;
                    self.isAccountsLoading = false;
                    if (self.mapAccounts) {
                        self.fetchAccountsSuccess();
                    }
                } else {
                    self.showOnServerError();
                }
            }
            var username = userName || applicationManager.getUserPreferencesManager().getUserObj().userName
            var asyncManager = applicationManager.getAsyncManager();
            asyncManager.callAsync(
                [
                    asyncManager.asyncItem(applicationManager.getAccountManager(), "fetchInternalAccounts"),
                    asyncManager.asyncItem(applicationManager.getAccountManager(), "fetchExternalAccountsData", [username])
                ], completionCallback);
        } else {
            applicationManager.getAccountManager().fetchInternalAccounts(completionCallback.bind(this), this.showOnServerError.bind(this));
        }
    }

    /**
     *Method to get all accounts i.e internal and external based on aggregatedAccount flag of configurations
     */
    PresentationController.prototype.showAllAccounts = function(noRefresh) {
        var self = this;
        if (!noRefresh) {
            this.fetchAccounts();
        }
        if (this.isAccountsLoading) {
            applicationManager.getNavigationManager().updateForm({
                showLoadingIndicator: {
                    status: true
                }
            }, "frmAccountsLanding");
            this.mapAccounts = true;
        }
        else {
            this.fetchAccountsSuccess();
        }
       
      
    };
    /**
     *Method is invoked when fetching of accounts is successful. It covers both scenarios of internal and external accounts
     * @param {Collection} accounts list of all the accounts i.e internal account and external accounts based on aggregatedAccount flag of configurations
     */
    PresentationController.prototype.fetchAccountsSuccess = function() {
        var self = this;
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            var finalAccounts = [];
            this.accounts[0].data.forEach(function(internalAccount) {
                finalAccounts.push(internalAccount);
            });
            this.accounts[1].data.forEach(function(externalAccount) {
                finalAccounts.push(self.processExternalAccountsData(externalAccount));
            });
            applicationManager.getNavigationManager().updateForm({
                accountsSummary: finalAccounts
            }, "frmAccountsLanding");
        } else {
            applicationManager.getNavigationManager().updateForm({
                accountsSummary: this.accounts
            }, "frmAccountsLanding");
        }
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmAccountsLanding");
        this.callPostAccountsService();
    };


    PresentationController.prototype.callPostAccountsService = function () {
        var scope = this;
        var userName = null;
        if (this.fetchPasswordWarningAndOutageMessages) {   
            if (CommonUtilities.isCSRMode()) {
                function onGetUserAttributesSuccess(response){
                    scope.performServiceCallsOnAccountsSuccess(response.UserName);  
                }
                var configManager = applicationManager.getConfigurationManager();
                kony.sdk.getCurrentInstance().getIdentityService(configManager.constants.IDENTITYSERVICENAME).getUserAttributes(onGetUserAttributesSuccess,{});
            } else {
                userName = applicationManager.getUserPreferencesManager().getUserObj().userName;
                this.performServiceCallsOnAccountsSuccess(userName);
            }           
        }   
        else {
            scope.showPasswordExpiryMessage();
            scope.showOutageMessage();
        }       
    };

    PresentationController.prototype.performServiceCallsOnAccountsSuccess = function(userName){
        var scope = this;
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        var accountsManager = applicationManager.getAccountManager();
        var userName = null;
        var breakpoint = kony.application.getCurrentBreakpoint(); 
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'showPasswordResetWarning', [{
                    userName: userName
                }]),
                asyncManager.asyncItem(applicationManager.getConfigurationManager(), 'fetchOutageMessages')
        ],
        function (asyncResponses) {
            scope.showPasswordExpiryMessage();
            scope.showOutageMessage();
            scope.fetchPasswordWarningAndOutageMessages = false;
        }
        )
    };

  PresentationController.prototype.getAccountDashboardCampaignsOnBreakpointChange = function(){
        var scope = this;
        var asyncManager = applicationManager.getAsyncManager();
        var accountsManager = applicationManager.getAccountManager();
        var breakpoint = kony.application.getCurrentBreakpoint(); 
        asyncManager.callAsync(
            [
              asyncManager.asyncItem(accountsManager, 'getCampaigns', [{
              "screenName" : "ACCOUNT_DASHBOARD",
              "scale" : (breakpoint >= 1366) ? "1366" : breakpoint
              }])
        ],
        function (asyncResponses) {
            scope.getCampaigns(asyncResponses.responses[0].data);
        }
        )
    };
  
   /**
    *Method is used for fetching of campaigns 
    * @param {Object}- list of campaigns 
    */
  PresentationController.prototype.getCampaigns = function(response) {
     	if(response.campaignSpecifications)
          this.getCampaignsSuccess(response);
     	else
          this.getCampaignsFailure(response);
   };
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
    PresentationController.prototype.getCampaignsSuccess = function(res) {
       applicationManager.getNavigationManager().updateForm({
           "campaignRes" : res["campaignSpecifications"]
        });
    };
    /**
     * Method that gets called when there is an error in fetching unread messages for account dashboard
     * @param {Object} error Error Object
     */
    PresentationController.prototype.getCampaignsFailure = function(error) {
       applicationManager.getNavigationManager().updateForm({
            "campaignError" : error
        });
    };

    /**
     *  Process external Account Data
     * @param {Collection} rawData External accounts JSON
     * @returns {Collection} Collection of altered data
     */
    PresentationController.prototype.processExternalAccountsData = function(rawData) {
        var isError = function(error) {
            try {
                if (error && error.trim() !== "") {
                    return true;
                }
                return false;
            } catch (err) {

            }
        };
        var account = {};
        account.accountName = rawData.AccountName;
        account.nickName = rawData.NickName;
        account.accountID = rawData.Number;
        account.accountType = rawData.TypeDescription;
        account.availableBalance = rawData.AvailableBalance;
        account.currentBalance = rawData.AvailableBalance;
        account.outstandingBalance = rawData.AvailableBalance;
        account.availableBalanceUpdatedAt = (rawData.LastUpdated) ? (CommonUtilities.getTimeDiferenceOfDate(rawData.LastUpdated)) : kony.i18n.getLocalizedString('i18n.AcountsAggregation.timeDifference.justNow');
        if (String(rawData.FavouriteStatus).trim().toLowerCase() === "true") {
            account.favouriteStatus = "1";
        } else {
            account.favouriteStatus = "0";
        }
        account.bankLogo = rawData.BankLogo;
        account.isError = isError(rawData.error);
        account.isExternalAccount = true;
        account.bankName = rawData.BankName;
        account.userName = (rawData.Username) ? rawData.Username : rawData.username;
        account.bankId = rawData.Bank_id;
        account.externalAccountId = rawData.Account_id;
        account.accountHolder = rawData.AccountHolder;
        return account;
    };
    /**
     * Method to toggle favourite account status of any specific selected account
     * @param {JSON} account details of the current account whose status is to be changed
     */
    PresentationController.prototype.changeAccountFavouriteStatus = function(account) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmAccountsLanding");
        var accountModel;
        if (account.isExternalAccount === true) {
            var favoriteStatus;
            if (account.favouriteStatus === "1") {
                favoriteStatus = "false";
            } else {
                favoriteStatus = "true";
            }
            accountModel = {
                Account_id: account.externalAccountId,
                FavouriteStatus: favoriteStatus
            };
            applicationManager.getAccountManager().updateExternalAccountFavouriteStatus(accountModel, this.showAllAccounts.bind(this, false), this.showOnServerError.bind(this));
        } else {
            accountModel = {
                accountID: account.accountID
            };
            applicationManager.getAccountManager().updateFavouriteStatus(accountModel, this.showAllAccounts.bind(this, false), this.showOnServerError.bind(this));
        }
    };
    /**
     * Method which calls all the functions for different components of accounts landing page except accounts accounts and user details.
     */
    PresentationController.prototype.loadAccountsLandingComponents = function() {
        this.getUnreadMessages();
        this.fetchScheduledTransactions();
        this.fetchMessages();
        this.fetchPFMData();
    };

  
   PresentationController.prototype.showPasswordExpiryMessage = function(){
          var userPrefManager = applicationManager.getUserPreferencesManager();
          var passwordlockSettings = userPrefManager.getPasswordWarning();
            applicationManager.getNavigationManager().updateForm({
                "passwordResetWarning": {
                    "show": this.fetchPasswordWarningAndOutageMessages,
                    "message": passwordlockSettings
                }
            });
           
   };
    /**
     * Method to show outage message
     */
    PresentationController.prototype.showOutageMessage = function() {
        var outageMessage = applicationManager.getConfigurationManager().getOutageMessages();
        if (outageMessage && outageMessage.length > 0) {
            outageMessage.join("\n");
        } else {
            outageMessage = "";
        }
        if (!CommonUtilities.isEmptyString(outageMessage)) {
            applicationManager.getNavigationManager().updateForm({
                "outageMessage": {
                    "show": this.fetchPasswordWarningAndOutageMessages,
                    "message": outageMessage
                }
            });
        }
    };

    /**
     * Method to get Unread messages count from messages module
     */
    PresentationController.prototype.getUnreadMessages = function() {
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.getUnreadMessagesOrNotificationsCount(this.getUnreadMessagesCountCompletionCallback.bind(this));
    };
    /**
     * Method that receives unread message count from the Messages module
     * @param {JSON} response contains totalUnread count key for unread messages
     */
    PresentationController.prototype.getUnreadMessagesCountCompletionCallback = function(response) {
        applicationManager.getNavigationManager().updateForm({
            unreadCount: {
                count: response.totalUnreadCount
            }
        }, "frmAccountsLanding");
    };
    /**
     * Method to fetch upcomming scheduled transactions for accounts landing page
     */
    PresentationController.prototype.fetchScheduledTransactions = function() {
        applicationManager.getTransactionManager().fetchScheduledTransaction(this.fetchScheduledTransactionSuccess.bind(this), this.fetchScheduledTransactionFailure.bind(this));
    };
    /**
     * Method that receives upcomming scheduled transactions for accounts dashboard
     * @param {Collection} transactions List of transactions
     */
    PresentationController.prototype.fetchScheduledTransactionSuccess = function(transactions) {
        applicationManager.getNavigationManager().updateForm({
            UpcomingTransactions: transactions
        });
    };
    /**
     * Method that gets called when there is an error in fetching upcomming account dasboard transactions
     * @param {Object} error Error Object
     */
    PresentationController.prototype.fetchScheduledTransactionFailure = function(error) {
        applicationManager.getNavigationManager().updateForm({
            UpcomingTransactions: [],
            serviceError: true
        });
    };
    /**
     * Method to fetch all the unread messages
     */
    PresentationController.prototype.fetchMessages = function() {
        applicationManager.getMessagesManager().fetchAllRequestsForInbox(this.fetchMessagesSuccess.bind(this), this.fetchMessagesFailure.bind(this));
    };
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
    PresentationController.prototype.fetchMessagesSuccess = function(messagesObj) {
        var dashboardMessagesCount = applicationManager.getConfigurationManager().getConfigurationValue("getDashboardMessageCount");
        var messages = messagesObj.customerrequests_view.slice(0, parseInt(dashboardMessagesCount));
        applicationManager.getNavigationManager().updateForm({
            unreadMessages: messages
        });
    };
    /**
     * Method that gets called when there is an error in fetching unread messages for account dashboard
     * @param {Object} error Error Object
     */
    PresentationController.prototype.fetchMessagesFailure = function(error) {
        applicationManager.getNavigationManager().updateForm({
            unreadMessages: [],
            serviceError: true
        });
    };
    /**
     * Method to fetch Product finance Management(PFM) data
     */
    PresentationController.prototype.fetchPFMData = function() {
        if (applicationManager.getConfigurationManager().getConfigurationValue("isPFMWidgetEnabled") !== "true") {
            applicationManager.getNavigationManager().updateForm({
                PFMDisabled: true
            }, "frmAccountsLanding");
        } else {
            var date = new Date();
            var criteria;
            criteria = kony.mvc.Expression.and(criteria, kony.mvc.Expression.eq("monthId", date.getMonth()+1), kony.mvc.Expression.eq("year", date.getFullYear()));
            applicationManager.getAccountManager().getMonthlySpending(criteria, this.fetchPFMSuccess.bind(this), this.fetchPFMFailure.bind(this));
        }
    };
    /**
     * Method that called on successful fetching of PFM data
     * @param {Object} PFMData PFMData object
     */
    PresentationController.prototype.fetchPFMSuccess = function(PFMData) {
        applicationManager.getNavigationManager().updateForm({
            PFMMonthlyWidget: PFMData
        });
    };
    /**
     * Method that gets called when there is an error in fetching PFMData for account dashboard
     * @param {Object} error Error Object
     */
    PresentationController.prototype.fetchPFMFailure = function(error) {
        applicationManager.getNavigationManager().updateForm({
            PFMMonthlyWidget: [],
            serviceError: true
        });
    };

    PresentationController.prototype.showExternalBankList = function() {
        this.presentAccountsLanding();
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsLanding");
        applicationManager.getAccountManager().fetchExternalBanks(this.showExternalBankListSuccess.bind(this), this.showExternalBankListFailure.bind(this));
    };

    PresentationController.prototype.showExternalBankListSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "externalBankList": response
        });
    };

    PresentationController.prototype.showExternalBankListFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "externalBankList": []
        });
    };

    PresentationController.prototype.deleteExternalAccount = function(accountObject) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsLanding");
        applicationManager.getAccountManager().deleteExternalAccount(accountObject, this.showAccountsDashboard.bind(this), this.deleteExternalAccountFailure.bind(this));
    };
    PresentationController.prototype.deleteExternalAccountFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            serviceError: true,
            showLoadingIndicator: {
                status: false
            }
        });
    };

    PresentationController.prototype.fetchExternalBankAccounts = function(bankObj) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsLanding");
        var bankCredentials = {
            mainUser: bankObj.mainUser,
            userName: bankObj.userName,
            bankId: bankObj.bankId
        };
        applicationManager.getAccountManager().fetchAccountsFromAnExternalBank(bankCredentials, this.fetchExternalBankAccountsSuccess.bind(this, bankObj), this.fetchExternalBankAccountsFailure.bind(this));
    };

    PresentationController.prototype.fetchExternalBankAccountsSuccess = function(bankObj, response) {
        var self = this;
        var externalAccount = [];
        if (response.length > 0) {
            externalAccount = self.processOtherBankAccountsData(response, bankObj.userName);
        }
        applicationManager.getNavigationManager().updateForm({
            externalBankAccountsModel: externalAccount
        });
    };

    PresentationController.prototype.fetchExternalBankAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            serviceError: true,
            showLoadingIndicator: {
                status: false
            }
        });
    };

    PresentationController.prototype.processOtherBankAccountsData = function(data,userName) {
        var secData = [];
        var scopeObject = this;
        for (var i = 0; i < data.length; i++) {
            secData[i] = {};
            secData[i].AccountName = data[i].AccountName;
            //secData[i].checkImage= "checkboxtick.png";
            secData[i].AccountType = data[i].TypeDescription;

            secData[i].AvailableBalanceLabel = kony.i18n.getLocalizedString("i18n.accountDetail.availableBalance");        
            var availableBalance = parseFloat(data[i].AvailableBalance).toFixed(2);
            secData[i].AvailableBalanceWithCurrency = CommonUtilities.formatCurrencyWithCommas(availableBalance,false,data[i].CurrencyCode);
            secData[i].AvailableBalance = availableBalance;
            secData[i].CurrencyCode = data[i].CurrencyCode;
            secData[i].Number = data[i].Number;
            secData[i].bank_id = data[i].Bank_id;
            secData[i].Type_id = data[i].Type_id;
            secData[i].AccountHolder = data[i].AccountHolder;
            secData[i].UserName = userName;
        }
        return secData;
    };

    PresentationController.prototype.prepareInputConfigForSavingEXternalBankAccounts = function(selectedData) {
        var accData = {};
        accData.AccountName = "";
        accData.main_user = "";
        accData.bank_id = "";
        accData.username = "";
        accData.CurrencyCode = "";
        accData.AvailableBalance = "";
        accData.Number = "";
        accData.Type_id = "";
        accData.AccountHolder = "";
        var mainUser = applicationManager.getUserPreferencesManager().getCurrentUserName();
        for (var i in selectedData) {
            accData.AccountName = accData.AccountName + selectedData[i].AccountName + ",";
            accData.main_user = accData.main_user + mainUser + ",";
            accData.bank_id = accData.bank_id + selectedData[i].BankId + ",";
            accData.CurrencyCode = accData.CurrencyCode + selectedData[i].CurrencyCode + ",";
            accData.AvailableBalance = accData.AvailableBalance + selectedData[i].AvailableBalance + ",";
            accData.Number = accData.Number + selectedData[i].AccountNumber + ",";
            accData.Type_id = accData.Type_id + selectedData[i].TypeId + ",";
            accData.AccountHolder = accData.AccountHolder + selectedData[i].AccountHolder + ",";
            accData.username = accData.username + selectedData[i].UserName + ",";
        }
        accData.loop_count = (++i).toString();
        accData.AccountName = accData.AccountName.slice(0, -1);
        accData.main_user = accData.main_user.slice(0, -1);
        accData.bank_id = accData.bank_id.slice(0, -1);
        accData.username = accData.username.slice(0, -1);
        accData.CurrencyCode = accData.CurrencyCode.slice(0, -1);
        accData.AvailableBalance = accData.AvailableBalance.slice(0, -1);
        accData.Number = accData.Number.slice(0, -1);
        accData.Type_id = accData.Type_id.slice(0, -1);
        accData.AccountHolder = accData.AccountHolder.slice(0, -1);
        return accData;
    };


    PresentationController.prototype.addExternalBankAccounts = function(selectedData) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsLanding");
        var bankParameters = this.prepareInputConfigForSavingEXternalBankAccounts(selectedData);
        applicationManager.getAccountManager().addExternalAccounts(bankParameters, this.addExternalBankAccountsSuccess.bind(this), this.addExternalBankAccountsFailure.bind(this));
    };

    PresentationController.prototype.addExternalBankAccountsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            savedExteranlAccountsModel: response
        });
    };

    PresentationController.prototype.addExternalBankAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            serviceError: true
        });
    };

    PresentationController.prototype.authenticateUserInExternalBank = function(username, password, identityProvider) {
        var params = {
            "username": username,
            "password": password,
            "identityProvider": identityProvider
        };
        applicationManager.getAuthManager().loginExternalBank(params, this.authenticateUserInExternalBankSuccess.bind(this), this.authenticateUserInExternalBankFailure.bind(this));
    };

    PresentationController.prototype.authenticateUserInExternalBankSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "externalBankLogin": response
        });
    };

    PresentationController.prototype.authenticateUserInExternalBankFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            "externalBankLoginFailure": response
        });
    };
    /**
     * Method to navigate to open New Account(NAO)
     */
    PresentationController.prototype.navigateToNewAccountOpening = function() {
        var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        nuoModule.presentationController.showNewAccountOpening();
    };
    /**
     * Method to navigate and show contact us
     */
    PresentationController.prototype.showContactUs = function() {
        var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
        InformationContentModule.presentationController.showContactUsPage();
    };
    /**
     *Method to Navigate to Messages Module and loads the new Message screen
     */
    PresentationController.prototype.newMessage = function() {
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.showAlertsPage("AccountsLanding", {
            show: "CreateNewMessage"
        });
    };
    /**
     * Method to Navigate to Messages Module and show selected message
     * @param {JSON} message selected essage object
     */
    PresentationController.prototype.showSelectedMessage = function(message) {
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.showAlertsPage("AccountsLanding", {
            show: "ShowSelectedMessage",
            selectedRequestId: message.id
        });
    };
    /**
     * Method to navigate/show frmAccountsDetails
     */
    PresentationController.prototype.presentAccountDetails = function() {
        applicationManager.getNavigationManager().navigateTo("frmAccountsDetails");
    };
    /**
     * Method to navigate/show frmScheduledTransaction
     */
    PresentationController.prototype.presentScheduledTransaction = function() {
        applicationManager.getNavigationManager().navigateTo("frmScheduledTransactions");
    };
    /**
     * Method to navigate/show frmAccountsLanding
     */
    PresentationController.prototype.presentAccountsLanding = function() {
        applicationManager.getNavigationManager().navigateTo("frmAccountsLanding");
    };
    /**
     * Entry Point for Account Details, this method load Account detail components
     * @param {JSON} account account json for which yout want to get details
     */
    PresentationController.prototype.showAccountDetails = function(account) {
        if (account) {
            currentAccount = account;
        }
        this.showSelectedAccountTransaction(currentAccount);
        applicationManager.getNavigationManager().navigateTo("frmAccountsDetails");
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsDetails");
        this.getAccountDeatilsAllAccounts();
        this.updateAccountDetails(currentAccount);
    };
    /**
     * Method to get all internal accounts for account details page
     */
    PresentationController.prototype.getAccountDeatilsAllAccounts = function() {
        var internalAccounts = applicationManager.getAccountManager().getInternalAccounts();
        if (internalAccounts) {
            applicationManager.getNavigationManager().updateForm({
                accountList: internalAccounts
            }, "frmAccountsDetails");
        } else {
            this.showOnServerError();
        }
    };
    /**
     * Method to get transactions for specific selected account
     * @param {JSON} account Account for which you want transactions
     * @param {JSON} dataInputs dataInputs for getting specific type of data or for resetting sorting
     * @param {String} navigationMode Navigation Mode
     */
    PresentationController.prototype.showSelectedAccountTransaction = function(account, dataInputs, navigationMode) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsDetails");
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        if (account) {
            currentAccount = account;
        }
        if (!dataInputs) {
            dataInputs = {
                resetSorting: true,
                transactionType: OLBConstants.ALL,
                account: account
            };
        } else {
            if (dataInputs.resetSorting === undefined || dataInputs.resetSorting === null) {
                dataInputs.resetSorting = true;
            }
            dataInputs["account"] = currentAccount;
        }
        var transactionType = dataInputs.transactionType;
        var paginationManager = applicationManager.getPaginationManager();
        if (dataInputs.resetSorting === true) {
            paginationManager.resetValues();
        }
        var sortingConfig = this.sortConfig[transactionType];
        var sortParams;
        if (navigationMode !== "nextTransaction" && navigationMode !== "previousTransaction") {
            sortParams = paginationManager.getValues(sortingConfig, dataInputs);
        } else {
            sortParams = paginationManager.getValues(sortingConfig);
        }

        if (
            (dataInputs.sortBy !== undefined || dataInputs.sortBy !== null) && sortParams.sortBy !== dataInputs.sortBy) {
            sortParams.sortBy = dataInputs.sortBy;
        }
        var commandObj = {
            accountID: currentAccount.accountID,
            transactionType: transactionType,
            offset: sortParams.offset,
            limit: sortParams.limit,
            isScheduled: "false", //not getting scheduled transactions for account details page
            sortBy: sortParams.sortBy,
            order: sortParams.order
        };
        dataInputs.sortConfig = sortingConfig;
        applicationManager.getTransactionManager().fetchAccountTransactionByType(commandObj, this.fetchTrasactionsSuccess.bind(this, dataInputs), this.showAccountDetailsError.bind(this));
    };
    /**
     * Method to show account details page error
     */
    PresentationController.prototype.showAccountDetailsError = function() {
        applicationManager.getNavigationManager().updateForm({
            showOnServerError: true,
            showLoadingIndicator: {
                status: false
            }
        }, "frmAccountsDetails");
    };
    /**
     * Method gets invoked once fetching of transactions is successful
     * @param {Object} dataInputs Object containing sortConfig, transactType i.e 'All','Transfer' etc and current account
     * @param {Collection} transactions List of fetched transactions
     */
    PresentationController.prototype.fetchTrasactionsSuccess = function(dataInputs, transactions) {
        var uiDataObj = {};
        var paginationManager = applicationManager.getPaginationManager();
        if(transactions.length ===0 && dataInputs.sortConfig.offset !== 0) {
            applicationManager.getNavigationManager().updateForm({
                noMoreRecords: true
            }, "frmAccountsDetails");
            return;
        }
        paginationManager.updatePaginationValues();
        uiDataObj.transactions = transactions;
        uiDataObj.dataInputs = dataInputs;
        uiDataObj.pagination = {
            limit: paginationManager.limit,
            offset: paginationManager.offset,
            paginationRowLimit: paginationManager.paginationRowLimit
        };
        uiDataObj.pagination.limit = transactions.length;
        applicationManager.getNavigationManager().updateForm({
            transactionDetails: uiDataObj
        }, "frmAccountsDetails");
    };
    /**
     * Method to fetch next transactions on click of pagination next button
     * @param {JSON} account account for which we want to fetch transactions
     * @param {Object} dataInputs Object containing sortConfig, transactType i.e 'All','Transfer' etc and current account
     */
    PresentationController.prototype.fetchNextTransactions = function(account, dataInputs) {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.showSelectedAccountTransaction(account, dataInputs, "nextTransaction");
    };
    /**
     * Method to fetch previous transactions on click of pagination previous button
     * @param {JSON} account account for which we want to fetch transactions
     * @param {Object} dataInputs Object containing sortConfig, transactType i.e 'All','Transfer' etc and current account
     */
    PresentationController.prototype.fetchPreviousTransactions = function(account, dataInputs) {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getPreviousPage();
        this.showSelectedAccountTransaction(account, dataInputs, "previousTransaction");
    };

    /**
     * Method to fetch transactions based on serch keywords
     * @param {Object} searchParams Search Parameters
     */
    PresentationController.prototype.fetchTransactionsBySearch = function(searchParams) {
        var startDate = searchParams.dateRange.startDate;
        var endDate = searchParams.dateRange.endDate;
        var commandObj = {
            searchTransactionType: searchParams.transactionTypeSelected,
            searchDescription: searchParams.keyword,
            searchMinAmount: searchParams.fromAmount,
            searchMaxAmount: searchParams.toAmount,
            searchStartDate: startDate,
            searchEndDate: endDate,
            isScheduled: 0, //as per the reqirement from backend explicitly sending this value
            fromCheckNumber: searchParams.fromCheckNumber,
            toCheckNumber: searchParams.toCheckNumber,
            accountNumber: currentAccount.accountID
        };
        this.searchParams = commandObj;
        var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("searchTransactionType", commandObj.searchTransactionType), kony.mvc.Expression.eq("searchDescription", commandObj.searchDescription), kony.mvc.Expression.eq("searchMinAmount", commandObj.searchMinAmount), kony.mvc.Expression.eq("searchMaxAmount", commandObj.searchMaxAmount), kony.mvc.Expression.eq("searchStartDate", commandObj.searchStartDate), kony.mvc.Expression.eq("searchEndDate", commandObj.searchEndDate), kony.mvc.Expression.eq("fromCheckNumber", commandObj.fromCheckNumber), kony.mvc.Expression.eq("toCheckNumber", commandObj.toCheckNumber), kony.mvc.Expression.eq("accountNumber", commandObj.accountNumber), kony.mvc.Expression.eq("isScheduled", commandObj.isScheduled), kony.mvc.Expression.eq("searchType", "Search"));
        applicationManager.getTransactionManager().fetchTransactionsByCriteria(criteria, this.fetchTransactionsBySearchSuccess.bind(this), this.showAccountDetailsError.bind(this));
    };
    /**
     * Methods gets called once when the fetching of transaction by search is successful
     * @param {Collection} transactions List of transactions
     */
    PresentationController.prototype.fetchTransactionsBySearchSuccess = function(transactions) {
        applicationManager.getNavigationManager().updateForm({
            searchTransactions: {
                transactions: transactions,
                dataInputs: {
                    account: currentAccount
                }
            }
        }, "frmAccountsDetails");
    };
    /**
     * Method to present frmAccount details with specific account details
     * @param {JSON} account Account whose details needs to be displayed
     */
    PresentationController.prototype.updateAccountDetails = function(account) {
        applicationManager.getNavigationManager().updateForm({
            accountDetails: account
        }, "frmAccountsDetails");
    };
    /**
     * Method to show account details by updating account info
     * @param {Number} accountID Account Id of the account
     */
    PresentationController.prototype.showAgainAccountsDetails = function(accountID) {
        applicationManager.getNavigationManager().navigateTo("frmAccountsDetails");
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmAccountsDetails");
        applicationManager.getAccountManager().fetchInternalAccounts(this.fetchAccountsDetailsSuccess.bind(this, accountID), this.showOnServerError.bind(this));
    };
    /**
     * Method gets called if fetching of internal accounts is success for account details
     * @param {Number} accountID AccountID of the account
     * @param {Collection} accounts Array of accounts JSON
     */
    PresentationController.prototype.fetchAccountsDetailsSuccess = function(accountID, accounts) {
        var account;
        for (var i in accounts) {
            if (accounts[i].accountID === accountID) {
                account = accounts[i];
                break;
            }
        }
        this.showAccountDetails(account);
    };
    /**
     * Entry point method to show scheduled transactions forms and load all its dependencies
     * @param {JSON} account Account whose transactions needs to be fetched
     */
    PresentationController.prototype.showScheduledTransactionsForm = function(account) {
        if (account) {
            currentAccount = account;
        }
        applicationManager.getNavigationManager().navigateTo("frmScheduledTransactions", {
            showLoadingIndicator: {
                status: true
            }
        });
        this.showScheduledTransactionAccountInfo(currentAccount);
        applicationManager.getPaginationManager().resetValues();
        this.fetchScheduledFormTransactions(currentAccount, { resetSorting : true});
        this.loadScheduledTransctionsAllAccounts();
    };
    /**
     * Method toshow account info on frmScheduledTransaction form
     * @param {JSON} account Account whose details needs to be displayed
     */
    PresentationController.prototype.showScheduledTransactionAccountInfo = function(account) {
        if (account) {
            currentAccount = account;
        }
        applicationManager.getNavigationManager().updateForm({
            accountInfo: currentAccount
        }, "frmScheduledTransactions");
    };
    /**
     * Method to fetch scheduled transaction for selected account for frmScheduledTransaction
     * @param {JSON} account Account whose information needs to be fetched
     * @param {JSON} dataInputs Input for sorting like sortBy:Asc/Desc etc.
     */
    PresentationController.prototype.fetchScheduledFormTransactions = function(account, dataInputs) {
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmScheduledTransactions");
        if (account) {
            currentAccount = account;
        }
        var paginationManager = applicationManager.getPaginationManager();
        var sortingConfig = this.scheduleTransactonsConfig["All"];
        var sortParams = paginationManager.getValues(sortingConfig, dataInputs);
        var commandObj = {
            accountID: currentAccount.accountID,
            transactionType: OLBConstants.ALL,
            limit: sortParams.limit,
            paginationRowLimit: paginationManager.paginationRowLimit,
            offset: sortParams.offset,
            isScheduled: "true", //true to get scheduled transactions
            sortBy: sortParams.sortBy,
            order: sortParams.order
        };
        applicationManager.getTransactionManager().fetchAccountTransactionByType(commandObj, this.fetchScheduledFormTransactionsSuccess.bind(this, sortParams), this.scheduledFormError.bind(this));
    };
    /**
     * Method that gets invoked once fetching of scheduled transaction for frmScheduledTransaction for is successful
     * @param {JSON} sortConfig Sorting configurations for scheduled transactions
     * @param {Collection} transactions List of transactions
     */
    PresentationController.prototype.fetchScheduledFormTransactionsSuccess = function(sortConfig, transactions) {

        if(sortConfig.offset > 0 && transactions.length === 0) {
            applicationManager.getNavigationManager().updateForm({
                noMoreRecords: true
            }, "frmScheduledTransactions");    
            return;
        }
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var uiData = {};
        uiData.showingType = OLBConstants.ALL;
        uiData.account = currentAccount;
        applicationManager.getPaginationManager().updatePaginationValues();
        uiData.pagination = sortConfig;
        uiData.scheduledTransactions = transactions;
        uiData.pagination.limit = transactions.length;
        applicationManager.getNavigationManager().updateForm({
            transactionDetails: uiData
        }, "frmScheduledTransactions");
    };
    /**
     * Methods gets called if any of the services for scheduled form fails
     */
    PresentationController.prototype.scheduledFormError = function() {
        applicationManager.getNavigationManager().updateForm({
            serverError: true,
            showLoadingIndicator: {
                status: false
            }
        }, "frmScheduledTransactions");
    };

    /**
     * Method to fetch next scheduled transactions on click of pagination next button
     * @param {JSON} account account for which we want to fetch transactions
     */
    PresentationController.prototype.fetchNextScheduledTransactions = function(account) {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getNextPage();
        this.fetchScheduledFormTransactions(account);
    };
    /**
     * Method to fetch previous scheduled transactions on click of pagination previous button
     * @param {JSON} account account for which we want to fetch transactions
     */
    PresentationController.prototype.fetchPreviousScheduledTransactions = function(account) {
        var paginationManager = applicationManager.getPaginationManager();
        paginationManager.getPreviousPage();
        this.fetchScheduledFormTransactions(account);
    };

    /**
     * Method to fetch all internal and external cached accounts based on aggregated account flag for external accounts
     * @returns {Collection} List of accounts
     */
    PresentationController.prototype.getAllAccounts = function() {
        var finalAccounts = [],
            self = this;
        var internalAccounts = applicationManager.getAccountManager().getInternalAccounts();
        if (internalAccounts.length > 0) {
            internalAccounts.forEach(function(account) {
                finalAccounts.push(account);
            });
        }
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            var externalAccount = applicationManager.getAccountManager().getExternalAccounts();
            if (externalAccount.length > 0) {
                externalAccount.forEach(function(account) {
                    finalAccounts.push(self.processExternalAccountsData(account));
                });
            }
        }
        return finalAccounts;
    };
    /**
     * Method to load all the eligible accounts for scheduled transaction Form
     */
    PresentationController.prototype.loadScheduledTransctionsAllAccounts = function() {
        var finalAccounts = this.getAllAccounts();
        applicationManager.getNavigationManager().updateForm({
            accountList: finalAccounts
        }, "frmScheduledTransactions");
    };
    /**
     * Method to get E-StateMents for all internal accounts
     * @param {JSON} account Selected account for which you want estatement
     */
    PresentationController.prototype.showFormatEstatements = function(account) {
        applicationManager.getNavigationManager().navigateTo("frmAccountsDetails");
        var navManager = applicationManager.getNavigationManager();
        navManager.updateForm({
            showLoadingIndicator: {
                status: true
            }
        });
        var internalAccounts = applicationManager.getAccountManager().getInternalAccounts();
        var uiData = {
            estatement: {
                account: account,
                allAccounts: internalAccounts
            }
        };
        navManager.updateForm(uiData, "frmAccountsDetails");
    };
    /**
     * Method to download e-state ment for selected account
     * @param {Number} accountID Account Id for which you want e-statement
     * @param {Number} year Year for which you want e-statement
     * @param {Number} month Month for which you want e-statement
     * @param {String} format Format in which you want e-statement
     */
    PresentationController.prototype.showDownloadStatementScreen = function(accountID, year, month, format) {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        });
        var context = {
            accountID: accountID,
            year: year,
            format: format,
            StatementMonth: month
        };
        applicationManager.getAccountManager().fetchAccountStatments(context, this.showDownloadStatementScreenSuccess.bind(this), this.showDownloadStatementScreenFailure.bind(this));
    };
    /**
     * Method that gets called once fetching e-statement link is successful
     * @param {Object} downloadURL Object containg details of e-statement download link
     */
    PresentationController.prototype.showDownloadStatementScreenSuccess = function(downloadURL) {
        applicationManager.getNavigationManager().updateForm({
            showDownloadStatement: downloadURL[0].StatementLink
        }, "frmAccountsDetails");
    };
    /**
     * Method that gets called in case downloading e-statement link fails
     */
    PresentationController.prototype.showDownloadStatementScreenFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        });
    };
    /**
     * Method to show print page with required data
     * @param {Object} uiData Objects required for printing transaction
     */
    PresentationController.prototype.showTransferPrintPage = function(uiData) {
        applicationManager.getNavigationManager().navigateTo("frmPrintTransfer", {
            context: null
        });
        applicationManager.getNavigationManager().updateForm(uiData, "frmPrintTransfer");
    };
    /**
     * Method to repeat any Transfer  transaction
     * @param {JSON} transaction transaction details for repeating the transaction
     * @param {function} onCancel - callback method if transaction cancel.
     */
    PresentationController.prototype.repeatTransfer = function(transaction, onCancel) {
        var context = {};
        context.transactionObject = transaction;
        context.onCancelCreateTransfer = onCancel;
        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen(context);
    };
    /**
     * Method to modify any Transfer  transaction
     * @param {JSON} transaction transaction details for repeating the transaction
     * @param {function} onCancel - callback method if transaction cancel. 
     */
    PresentationController.prototype.modifyTransfer = function(transaction, onCancel) {
        var context = {};
        context.editTransactionObject = transaction;
        context.onCancelCreateTransfer = onCancel;
        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen(context);
    };
    /**
     * Method to repeat any BillPay  transaction
     * @param {JSON} transaction transaction details for repeating the transaction
     * @param {function} onCancel - callback method if transaction cancel.
     */
    PresentationController.prototype.repeatBillPay = function(transaction, onCancel) {
        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transaction.onCancel = onCancel;
        billPayModule.presentationController.showBillPayData("Accounts", "PayABill", true, transaction);
    };
    /**
     * Method to repeat any P2P  transaction
     * @param {JSON} transaction transaction details for repeating the transaction
     * @param {function} onCancel - callback method if transaction cancel.
     */
    PresentationController.prototype.repeatP2P = function(transaction, onCancel) {
        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
        transaction.show = "SendMoney";
        transaction.amount = CommonUtilities.formatCurrencyWithCommas(String(Math.abs(transaction.amount)), true);
        transaction.onCancel = onCancel;
        p2pModule.presentationController.showPayAPerson(transaction);
    };
    PresentationController.prototype.repeatWireTransfer = function(transaction, onCancel) {
        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('WireTransferModule');
        wireTransferModule.presentationController.showWireTransfer({
            transactionObject: transaction,
            onCancel: onCancel
        });
    };
    /**
     * Method to modify  a load transaction
     * @param {Number} toAccountID AccountId to which you want to pay loan
     * @param {Number} onCancel Method to trigger on cancel
     */
    PresentationController.prototype.modifyLoan = function(toAccountID, onCancel) {
        var toAccount;
        var fromAccount = currentAccount;
        var internalAccount = applicationManager.getAccountManager().getInternalAccounts();
        for (var i = 0; i < internalAccount.length; i++) {
            if (internalAccount[i].accountID === toAccountID) {
                fromAccount = internalAccount[i];
                break;
            }
        }
        toAccount.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(toAccount.currentAmountDue,false,toAccount.currencyCode);
        toAccount.currentAmountDue = toAccount.currentAmountDue.slice(1);
        var data = {
            accounts: toAccount,
            fromAccount: fromAccount,
            onCancel: onCancel
        };
        var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
        loanModule.presentationController.navigateToLoanDue(data);
    };
    /**
     * Method to cancel any scheduled transaction
     * @param {JSON} transaction transaction that you want to cancel
     */
    PresentationController.prototype.cancelScheduledTransaction = function(transaction) {
        applicationManager.getTransactionManager().deleteTransaction({
            transactionId: transaction.transactionId,
            transactionType: transaction.transactionType
        }, this.showScheduledTransactionsForm.bind(this, currentAccount), this.scheduledFormError.bind(this));
    };
    /**
     * Method to cancel any scheduled transactions occurances
     * @param {JSON} transaction transaction that you want to cancel occurance
     */
    PresentationController.prototype.cancelScheduledTransactionOccurrence = function(transaction) {
        applicationManager.getTransactionManager().deleteRecurrenceTransaction({
            transactionId: transaction.transactionId
        }, this.showScheduledTransactionsForm.bind(this, currentAccount), this.scheduledFormError.bind(this));
    };
    /**
     * Method to cancel any scheduled transaction series
     * @param {JSON} transaction transaction that you want to cancel
     */
    PresentationController.prototype.cancelScheduledTransactionSeries = function(transaction) {
        applicationManager.getTransactionManager().deleteTransaction({
            transactionId: transaction.transactionId,
            transactionType: transaction.transactionType
        }, this.showScheduledTransactionsForm.bind(this, currentAccount), this.scheduledFormError.bind(this));
    };
    /**
     * Method to dispute any transaction
     * @param {JSON} transaction Transaction that you want to dispute
     */
    PresentationController.prototype.onDisputeTransaction = function(transaction) {
        var self = this;
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
        stopPaymentsModule.presentationController.showStopPayments({
            show: OLBConstants.ACTION.SHOW_DISPUTE_TRANSACTION_FORM,
            data: {
                disputeTransactionObject: transaction,
                onCancel: function() {
                    self.presentAccountDetails();
                }
            }
        });
    };
    /**
     * Method to navigate Print Transactions form and upadate active form.
     * @param {object} data  view model for Print form
     */
    PresentationController.prototype.showPrintPage = function(data) {
        data.accountDetailsModel = currentAccount;
        applicationManager.getNavigationManager().navigateTo('frmPrintTransaction');
        applicationManager.getNavigationManager().updateForm({printData : data}, 'frmPrintTransaction');
    };
    /**
     * Method to view all disputed transactions request
     */
    PresentationController.prototype.onViewDisputedRequets = function() {
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
        stopPaymentsModule.presentationController.showStopPayments({
            show: OLBConstants.DISPUTED_TRANSACTIONS
        });
    };


    PresentationController.prototype.downloadTransactionFile = function(param) {
        var requestParam = {};
        if (param.isSearchParam && this.searchParams) {
            requestParam = this.searchParams;
        }else {
            requestParam.accountNumber = currentAccount.accountID;
            requestParam.searchTransactionType = param.transactionType;
        }
        requestParam.searchStartDate = param.fromDate;
        requestParam.searchEndDate = param.toDate;
        requestParam.fileType = param.format;  

        requestParam.title = kony.i18n.getLocalizedString("i18n.common.transactions");
        requestParam.generatedBy = applicationManager.getUserPreferencesManager().getUserObj().userName;
        applicationManager.getNavigationManager().updateForm({
            transactionDownloadFile: applicationManager.getAccountManager().getDownloadTransctionURL(requestParam)
        }, "frmAccountsDetails");
    };
    /**
     * Method that fetches account by accountID and navigates to account details page
     * @param {String} accountID account id for which yout want to get details
     */
    PresentationController.prototype.fetchUpdatedAccountDetails = function(accountID) {
        var self = this;
        var param = {
            "accountID": accountID
        };
        applicationManager.getAccountManager().fetchInternalAccountByID(param, this.onSuccessFetchUpdatedAccountDetails.bind(this), this.showOnServerError.bind(this));
    };

    /**
     * Method called as success callback for FetchUpdatedAccountDetails
     * @param {Object} response - response from service for FetchUpdatedAccountDetails
     */
    PresentationController.prototype.onSuccessFetchUpdatedAccountDetails = function(response) {
        this.showAccountDetails(response[0]);
    };

    return PresentationController;
});