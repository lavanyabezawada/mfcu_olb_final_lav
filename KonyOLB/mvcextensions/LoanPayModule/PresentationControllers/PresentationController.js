define(['CommonUtilities'], function (CommonUtilities) {

  function LoanPay_PresentationController() {

    kony.mvc.Presentation.BasePresenter.call(this);
    var viewModel = [];
  }

  inheritsFrom(LoanPay_PresentationController, kony.mvc.Presentation.BasePresenter);

  LoanPay_PresentationController.prototype.initializePresentationController = function () {

  };

  LoanPay_PresentationController.prototype.presentLoanPay = function (data) {
    var navManager = applicationManager.getNavigationManager();
    if(kony.application.getCurrentForm().id!=="frmPayDueAmount"){
      navManager.navigateTo("frmPayDueAmount");
    }
    navManager.updateForm(data);
  };
  /**
  * To get frontend date string
  * @member LoanPay_PresentationController
  * @param {Date} dateString
  * @returns {Date} frontend date
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.getFormattedDateString = function (dateString) {
    return CommonUtilities.getFrontendDateString(dateString);
  };
  /**
  * To get backend date string
  * @member LoanPay_PresentationController
  * @param {Date} dateString
  * @param {String} dateFormat
  * @returns {Date} backend date (yyyy-mm-dd) format
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.getBackendDate = function (dateString) {
    return CommonUtilities.getBackendDateFormat(dateString);
  };
  /**
 * Entry Function for Loan Due
 * @member LoanPay_PresentationController
 * @param {Object} response contains account details
 * @returns {void} - None
 * @throws {void} - None
 */
  LoanPay_PresentationController.prototype.navigateToLoanDue = function (response) {
    if (response)
      viewModel = response;
    this.presentLoanPay({loanDue:viewModel});
  };
  /**
 * Entry Function for Loan Pay Off
 * @member LoanPay_PresentationController
 * @param {Object} response contains account details
 * @returns {void} - None
 * @throws {void} - None
 */
  LoanPay_PresentationController.prototype.navigateToLoanPay = function (response) {
    if (response)
      viewModel = response;
    this.presentLoanPay({loanPayoff:viewModel});
  };
  
  LoanPay_PresentationController.prototype.showView = function (frm, data) {
    this.presentUserInterface(frm, data);
  };
  /**
  * Function to Show Progress Bar
  * @member LoanPay_PresentationController
  * @param {void} - None 
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.showProgressBar = function () {
    var self = this;
    self.presentLoanPay({
      "ProgressBar": {
        show: true
      }
    });
  };
  /**
  * Function to Hide Progress Bar
  * @member LoanPay_PresentationController
  * @param {void} - None 
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.hideProgressBar = function () {
    var self = this;
    self.presentLoanPay({
      "ProgressBar": {
        show: false
      }
    });
  };
  /**
  * Function to create transfer for Loan Pay Off and Loan Pay Due Amount
  * @member LoanPay_PresentationController
  * @param {Object} data 
  * @param {String} context stores context
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.payLoanOff = function (data, context) {
    var self = this;
    self.showProgressBar();
    var info = {};
    var commandData = {
      fromAccountNumber: data.fromAccountID,
      amount: data.amount,
      transactionsNotes: data.notes,
      isScheduled: data.isScheduled ? data.isScheduled : "false",
      transactionType: "Loan",
      toAccountNumber: data.toAccountID,
      scheduledDate: data.date,
      penaltyFlag: data.penaltyFlag ? data.penaltyFlag : "false",
      payoffFlag: data.payoffFlag ? data.payoffFlag : "false"

    };
    applicationManager.getTransactionManager().createTransaction(commandData,this.onSuccessPayLoanOff.bind(this,data,context),this.onFailurePayLoanOff.bind(this));
  };

  /**
    * Method called as success callback for PayLoanOff
    * @param {Object} response - response from service for PayLoanOff
    */
  LoanPay_PresentationController.prototype.onSuccessPayLoanOff = function (data,context,response) {
    this.hideProgressBar();
    var responseData = {
      "data": data,
      "referenceId": response.referenceId
    };
    if (context === "payOtherAmount") {
      this.presentLoanPay({ payOtherAmount: responseData });
    } else if (context === "payCompleteDue") {
      this.presentLoanPay({ payCompleteDue: responseData });
    } else if (context === "payCompleteMonthlyDue") {
      this.presentLoanPay({ payCompleteMonthlyDue: responseData });
    }
  };
  /**
 * Method called as failure calback for PayLoanOff
 * @param {Object} response - response from service for PayLoanOff
 */
  LoanPay_PresentationController.prototype.onFailurePayLoanOff = function (response) {
    this.presentLoanPay({ "serverError": response.errorMessage });
    this.hideProgressBar();
  };
  /**
    * Function to Navigate to Accounts module
    * @member LoanPay_PresentationController
    * @param {void} - None
    * @returns {void} - None
    * @throws {void} - None
    */
  LoanPay_PresentationController.prototype.backToAccount = function () {
    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountsModule.presentationController.showAccountsDashboard();
  };
  /**
    * Function to fetch updated account object
    * @member LoanPay_PresentationController
    * @param {Object} accountID
    * @param {String} context stores context
    * @returns {void} - None
    * @throws {void} - None
    */
  LoanPay_PresentationController.prototype.fetchUpdatedAccountDetails = function (accountID, context) {
    var self = this;
    var param = {"accountID":accountID};
    applicationManager.getAccountManager().fetchInternalAccountByID(param,this.onSuccessFetchUpdatedAccountDetails.bind(this,context),this.onFailureFetchUpdatedAccountDetails.bind(this));
  };

  /**
    * Method called as success callback for FetchUpdatedAccountDetails
    * @param {Object} response - response from service for FetchUpdatedAccountDetails
    */
    LoanPay_PresentationController.prototype.onSuccessFetchUpdatedAccountDetails = function (context,response) {
      if(context=== "newAccountSelection"){
        this.presentLoanPay({ newAccountSelection : response});
      } else if(context=== "navigationToAccountDetails"){
        var accountDetails = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
         accountDetails.presentationController.showAccountDetails(response[0]);
      } else if(context=== "validateData"){
        this.presentLoanPay({ validateData : response});
      } else if(context=== "updateFromAccount"){
        this.presentLoanPay({ updateFromAccount : response});
      } else if(context=== "updateToAccount"){
        this.presentLoanPay({ updateToAccount : response});
      } else if(context=== "populateAccountData"){
        this.presentLoanPay({ populateAccountData : response});
      }
    };
    /**
   * Method called as failure calback for FetchUpdatedAccountDetails
   * @param {Object} response - response from service for FetchUpdatedAccountDetails
   */
    LoanPay_PresentationController.prototype.onFailureFetchUpdatedAccountDetails = function (response) {
      this.hideProgressBar();
      CommonUtilities.showServerDownScreen(); 
    };

  LoanPay_PresentationController.prototype.fetchCheckingAccounts = function () {
    applicationManager.getAccountManager().fetchInternalAccounts(this.onSuccessFetchCheckingAccounts.bind(this),this.onFailureFetchCheckingAccounts.bind(this));

  };
  /**
    * Method called as success callback for FetchCheckingAccounts
    * @param {Object} response - response from service for FetchCheckingAccounts
    */
    LoanPay_PresentationController.prototype.onSuccessFetchCheckingAccounts = function (response) {
      this.presentLoanPay({"loadAccounts":response});
      this.hideProgressBar();
    };
    /**
   * Method called as failure calback for FetchCheckingAccounts
   * @param {Object} response - response from service for FetchCheckingAccounts
   */
    LoanPay_PresentationController.prototype.onFailureFetchCheckingAccounts = function (response) {
      this.hideProgressBar();
      CommonUtilities.showServerDownScreen();
    };

  return LoanPay_PresentationController;
});