define(['CommonUtilities','OLBConstants'], function(CommonUtilities,OLBConstants) {
this.securityQuestionsPayload  = "";
 this.newUserName = "";
    function Profile_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(Profile_PresentationController, kony.mvc.Presentation.BasePresenter);


    Profile_PresentationController.prototype.initializeUserProfileClass = function() {
        this.PhoneTypes = {
            'Mobile': 'Mobile',
            'Work': 'Work',
            'Home': 'Home',
            'Other': 'Other'
        };
        this.AddressTypes = {
            "ADR_TYPE_WORK": 'Work',
            "ADR_TYPE_HOME": 'Home'
        };
        this.accountTypeConfig = {};
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)]= {
                sideImage: 'accounts_sidebar_turquoise.png',
                skin: 'sknflxhex26d0cecode',
                image: 'account_change_turquoise.png'
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)]= {
            sideImage: 'accounts_sidebar_purple.png',
            skin: 'sknflxhex9060B7code',
            image: 'account_change_purple.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)]= {
            sideImage: 'accounts_sidebar_yellow.png',
            skin: 'sknflxhexf4ba22code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)]= {
            sideImage: 'accounts_sidebar_blue.png',
            skin: 'sknflxhex4a90e2code',
            image: 'account_change_turquoise.png'
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)]= {
            sideImage: 'accounts_sidebar_brown.png',
            skin: 'sknflxhex8D6429code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)]= {
            sideImage: 'accounts_sidebar_brown.png',
            skin: 'sknflxhex8D6429code',
            image: 'account_change_yellow.png',
        };
        this.accountTypeConfig['Default']= {
            sideImage: 'accounts_sidebar_turquoise.png',
            skin: 'sknflxhex26d0cecode',
            image: 'account_change_turquoise.png'

            };
        
    };
    /**
     * Method to fetch alerts and mapping to UI
     * @param {Object} addressSelection - contains if he selected country or state.
     */
    Profile_PresentationController.prototype.getSpecifiedCitiesAndStates = function(addressSelection, addressId, states) {
        var self = this;
        var data = [];
        if (addressSelection === "country") {
            var statesList = [];
            statesList.push(["lbl1", "Select a State"]);
            for (var i = 0; i < Object.keys(states).length; ++i) {
                if (states[i][2] === addressId) {
                    statesList.push([states[i][0], states[i][1]]);
                }
            }
            data = {
                "states": statesList
            };

        } else if (addressSelection === "state") {
            var cityList = [];
            cityList.push(["lbl2", "Select a City"]);
            for (var j = 0; j < Object.keys(states).length; ++j) {
                if (states[j][2] === addressId) {
                    cityList.push([states[j][0], states[j][1]]);
                }
            }
            data = {
                "cities": cityList
            };

        }
        return data;
    };

    /**
     * Method to fetch user profile info and mapping to UI
     */
    Profile_PresentationController.prototype.showProfileSettings = function() {
        this.showUserProfile();
        this.initializeUserProfileClass();
        applicationManager.getNavigationManager().updateForm({"isLoading":true});
    };

    /**
     * Method to show secure access code settings
     */
    Profile_PresentationController.prototype.showSecureAccessSettings = function() {
        this.initializeUserProfileClass();
        var viewModel = {};
        viewModel.secureAccessSettings = true;
        applicationManager.getNavigationManager().updateForm(viewModel, 'frmProfileManagement');
    };

    /**
     * Method to show setting screen
     * @param {Object} viewModel - Data to be mapped at setting's screen 
     */
    Profile_PresentationController.prototype.showSettingsScreen = function(viewModel) {
        applicationManager.getNavigationManager().updateForm(viewModel, "frmProfileManagement");
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
    };

    /**
     * Method to get External Account edit data from the post show of frmProfileSettings
     * @returns {JSON} External Account Edit Object
     */
    Profile_PresentationController.prototype.getEditExternalAccount= function(){
        if (this.editExternalAccountData) {
            var showEditExternalAccount=JSON.parse(JSON.stringify(this.editExternalAccountData));
            this.editExternalAccountData.flow='';
            return showEditExternalAccount;
        }
        return null;
    }

    /**
     * Method to edit external account
     * @param {Object} data - JSON consisting account data
     */
    Profile_PresentationController.prototype.showEditExternalAccount = function(data) {
        this.editExternalAccountData={
            flow: 'editExternalAccounts',
            data:data
        }
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
    };

    /**
     * Method used to attach phone numbers to accounts after updation of phone numbers.
     * @param {Object} phoneId - contains the communication id.
     * @param {Object} accountIds - contains the account ids.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccounts = function(phoneId, accountIds) {
        var accountsArray = [];
        var i, params;


        if (accountIds.length === 0) {
            this.attachPhoneNumberToAccountsSuccess();
        } else {
            for (i = 0; i < accountIds.length; i++) {
                var accounts = {
                    "accountNumber": accountIds[i],
                    "phone": phoneId
                };
                accountsArray[i] = accounts;
            }
            accountsArray = JSON.stringify(accountsArray);
            accountsArray = accountsArray.replace(/"/g, "'");
            params = {
                "accountli": accountsArray
            };
        }
        applicationManager.getAccountManager().updateAccountPhoneNumber(params, this.attachPhoneNumberToAccountsSuccess.bind(this), this.attachPhoneNumberToAccountsFailure.bind(this));

    };

    /**
     * Method used as success call back for the attach phone numbers to accounts.
     *@param {Object} response - contains the service resonse.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccountsSuccess = function(response) {
        applicationManager.getAccountManager().fetchInternalAccounts(function(){},function(){});
        this.fetchEntitlements("ContactNumbers");
    };

    /**
     * Method used as failure call back for the attach phone numbers to accounts service.
     *@param {String} errorMessage - contains the error message for the service.
     */
    Profile_PresentationController.prototype.attachPhoneNumberToAccountsFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            addPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to delete Phone.
     * @param {Object} phoneObj - contains the phone Obj.
     */
    Profile_PresentationController.prototype.deletePhone = function(phoneObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteCommunicationID": phoneObj.id
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "ContactNumbers"), this.deleteEmailFailure.bind(this));
    };

    /**
     *Method used to edit phone number in profile management.
     * @param {Object} id - contains the communication Id.
     * @param {Object} viewmodel - contains the viewModel.
     */
    Profile_PresentationController.prototype.editPhoneNumber = function(id, viewModel) {
        this.showProgressBar();
        var phoneNumbers = [{
            "id": id,
            "Extension": viewModel.Extension,
            "isPrimary": (viewModel.isPrimary === true) ? "1" : "0",
            "phoneNumber": viewModel.phoneNumber,
            "phoneCountryCode":viewModel.phoneCountryCode,
            "phoneExtension":viewModel.phoneExtension,
        }];
        phoneNumbers = JSON.stringify(phoneNumbers);
        phoneNumbers = phoneNumbers.replace(/"/g, "'");
        var params = {
            "phoneNumbers": phoneNumbers,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.attachPhoneNumberToAccountsSuccess.bind(this),/*this.attachPhoneNumberToAccounts.bind(this, viewModel.phoneNumber, viewModel.services),*/ this.editPhoneNumberFailure.bind(this));
    };

    /**
     *Method used as failure call back for the edit phone number.
     *@param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.editPhoneNumberFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            editPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to get the edit phone view.
     * @param {Object} phoneObj - contains the phone Obj.
     */
    Profile_PresentationController.prototype.editPhoneView = function(phoneObj) {
        var accounts = applicationManager.getAccountManager().getInternalAccounts();
       /* var services = accounts.map(function(account) {
            return {
                id: account.accountID,
                name: CommonUtilities.getAccountDisplayName(account),
            };
        });*/
        var newPhoneModel = {
            phoneTypes: this.objectToListBoxArray(this.PhoneTypes),
            phoneTypeSelected: "Mobile",
            countryType: 'domestic',
            phoneNumber: '',
            phoneCountryCode:"",
            ext: '',
            isPrimary: false,
        //    services: services,
            recievePromotions: false
        };
        newPhoneModel.Extension = phoneObj.Extension;
        newPhoneModel.value = phoneObj.Value;
        newPhoneModel.phoneNumber = phoneObj.phoneNumber;
        newPhoneModel.phoneCountryCode = phoneObj.phoneCountryCode;
        newPhoneModel.recievePromotions = phoneObj.receivePromotions === "1";
        newPhoneModel.isPrimary = phoneObj.isPrimary === "true";
      /*  newPhoneModel.services = accounts.map(function(account) {
            return {
                id: account.accountID,
                name: CommonUtilities.getAccountDisplayName(account),
                selected: account.phoneId === phoneObj.phoneNumber
            };
        });*/
        newPhoneModel.id = phoneObj.id;
        newPhoneModel.onBack = this.getPhoneDetails.bind(undefined, phoneObj);
        applicationManager.getNavigationManager().updateForm({
            isLoading: false,
            editPhoneViewModel: newPhoneModel
        }, "frmProfileManagement");
    };

    /**
     *Method used as success call back for save phone NUmber service.
     * @param {Object} viewModel - contains the context to be shown in the profile.
     * @para {Object} response - contains the service response.
     */
    Profile_PresentationController.prototype.savePhoneNumberSuccessCallBack = function(viewModel, response) {
        //this.attachPhoneNumberToAccounts(viewModel.phoneNumber);/*, viewModel.services);
        this.attachPhoneNumberToAccountsSuccess();
    };

    /**
     * Method used as failure call back for the save phone number.
     * @param {String} errorMessage - contains the error message of the service call.
     */
    Profile_PresentationController.prototype.savePhoneNumberFailureCallBack = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            addPhoneViewModel: {
                serverError: errorMessage
            }
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to save phone Number in profile.
     *@param {Object} viewModel - contains the context to be shown in profile.
     */
    Profile_PresentationController.prototype.savePhoneNumber = function(viewModel) {
        this.showProgressBar();
        var phoneNumbers = [{
            "isPrimary": (viewModel.isPrimary === true) ? "1" : "0",
            "phoneNumber": viewModel.phoneNumber,
            "phoneCountryCode":viewModel.phoneCountryCode,
            "phoneExtension":viewModel.phoneExtension,
            "Extension": viewModel.type
        }];
        phoneNumbers = JSON.stringify(phoneNumbers);
        phoneNumbers = phoneNumbers.replace(/"/g, "'");
        var params = {
            "phoneNumbers": phoneNumbers,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.savePhoneNumberSuccessCallBack.bind(this, viewModel), this.savePhoneNumberFailureCallBack.bind(this));
    };

    /**
     * Method used to show the user phone numbers in profile manangement.
     */
    Profile_PresentationController.prototype.showUserPhones = function() {
        var viewProperties = {
            phoneList: applicationManager.getUserPreferencesManager().getEntitlementPhoneNumbers(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to show the add user phone number in profile manangement.
     */
    Profile_PresentationController.prototype.getAddPhoneNumberView = function() {
        var accounts = applicationManager.getAccountManager().getInternalAccounts();
        /*var services = accounts.map(function(account) {
            return {
                id: account.accountID,
                name: CommonUtilities.getAccountDisplayName(account),
            };
        });*/
        var phoneViewModel = {
            phoneTypes: this.objectToListBoxArray(this.PhoneTypes),
            phoneTypeSelected: "Mobile",
            countryType: 'domestic',
            phoneNumber: '',
            phoneCountryCode:'',
            phoneExtension:'',
            ext: '',
            isPrimary: false,
        //    services: services,
            recievePromotions: false
        };
        applicationManager.getNavigationManager().updateForm({
            isLoading: false,
            addPhoneViewModel: phoneViewModel
        }, "frmProfileManagement");
    };

    /**
     * Method used to get phone details.
     * @param {Object} phoneObject - contains the phone object.
     */
    Profile_PresentationController.prototype.getPhoneDetails = function(phoneObject) {
      /*  var accounts = applicationManager.getAccountManager().getInternalAccounts();
        var services = accounts.map(function(account) {
            return {
                id: account.accountID,
                name: CommonUtilities.getAccountDisplayName(account),
                selected: account.phoneId === phoneObject.phoneNumber
            };
        });*/
        applicationManager.getNavigationManager().updateForm({
            phoneDetails: {
                phone: phoneObject//,
             //   services: services
            },
            isLoading: false
        }, "frmProfileManagement");
    };

    /**
     * Method used to get list box array.
     * @param {Object} obj - contains the object containing the data to be used to construct the box array.
     */
    Profile_PresentationController.prototype.objectToListBoxArray = function(obj) {
        var list = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                list.push([key, obj[key]]);
            }
        }
        return list;
    };

    /**
     * Method used to fetch entitlements.
     */
    Profile_PresentationController.prototype.fetchEntitlements = function(requiredView) {
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
        };
        applicationManager.getUserPreferencesManager().fetchEntitlementsForUser(params, this.fetchEntitlementsSuccess.bind(this, requiredView), this.fetchEntitlementsFailure.bind(this));
    };

    /**
     * Method used as the success call back for fetch entitlements service call.
     */
    Profile_PresentationController.prototype.fetchEntitlementsSuccess = function(requiredView, response) {
        var viewProperties;
        if (requiredView === "ContactNumbers") {
            viewProperties = {
                phoneList: response.ContactNumbers,
            };
        } else if (requiredView === "EmailIds") {
            viewProperties = {
                emailList: response.EmailIds
            };
        } else if (requiredView === "Addresses") {
            viewProperties = {
                addressList: response.Addresses
            };
        }
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used as failure call back for fetch entitlements service call.
     */
    Profile_PresentationController.prototype.fetchEntitlementsFailure = function(errorMessage) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };

    /**
     * Method used as the failure call back for the save email service.
     * @param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.saveEmailFailureCallBack = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            emailError: errorMessage
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to save email. 
     * @param {Object} context - contains the email id, value, description, extension .
     */
    Profile_PresentationController.prototype.saveEmail = function(context) {
        this.showProgressBar();

        function sameEmail(emailIds, emailAddress) {
            for (var i = 0; i < emailIds.length; i++) {
                var existingEmail = emailIds[i].Value;
                if (existingEmail.toUpperCase() === emailAddress.toUpperCase()) {
                    return true;
                }
            }
            return false;
        }
        var entitlementEmailIds = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
        if (sameEmail(entitlementEmailIds, context.value)) {
            this.saveEmailFailureCallBack(kony.i18n.getLocalizedString("i18n.profile.emailAlreadyExists"));
        } else {
            if (entitlementEmailIds.length < 3) {
                var emailIds = [{
                    "isPrimary": (context.isPrimary === true) ? "1" : "0",
                    "value": context.value,
                    "Extension": "Personal"

                }];

                emailIds = JSON.stringify(emailIds);
                emailIds = emailIds.replace(/"/g, "'");
                var params = {
                    "emailIds": emailIds,
                    "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
                    "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
                };

                applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "EmailIds"), this.saveEmailFailureCallBack.bind(this));
            } else {
                this.saveEmailFailureCallBack("We currently do not support adding more than three emails for a user");
            }
        }
    };

    /**
     * Method used to show the user email view of profile management. 
     */
    Profile_PresentationController.prototype.showUserEmail = function() {
        this.showProgressBar();
        var viewProperties = {
            emailList: applicationManager.getUserPreferencesManager().getEntitlementEmailIds(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to delete email.
     * @param {Object} emailObj - contains the email object.
     */
    Profile_PresentationController.prototype.deleteEmail = function(emailObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteCommunicationID": emailObj.id
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "EmailIds"), this.deleteEmailFailure.bind(this));
    };

    /**
     * Method used as the failure call back for delete email.
     * @param {String} errorMessage - contains the error message.
     */
    Profile_PresentationController.prototype.deleteEmailFailure = function(errorMessage) {
        //navigating to serverDown Screen incase of delete email failure.
        this.fetchEntitlementsFailure(errorMessage);
    };

    /**
     * Method used to show the progress bar in the profile management.
     */
    Profile_PresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": true
        }, "frmProfileManagement");
    };

    /**
     * Method used to hide the progress bar in the profile management.
     */
    Profile_PresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "isLoading": false
        }, "frmProfileManagement");
    };

    /**
     * Method used to edit email in profile management.
     * @param {Object} context - contains the email value, id, extension, isPrimary, description.
     */
    Profile_PresentationController.prototype.editEmail = function(Context) {
        this.showProgressBar();
        var emailIds = [{
            "id": Context.id,
            "Extension": Context.extension,
            "isPrimary": (Context.isPrimary === true) ? "1" : "0",
            "value": Context.email
        }];
        emailIds = JSON.stringify(emailIds);
        emailIds = emailIds.replace(/"/g, "'");
        var params = {
            "emailIds": emailIds,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "EmailIds"), this.editEmailFailure.bind(this));
    };

    /**
     * Method used as the failure call back for the edit email service call.
     * @param {String} errorMessage - contains the error message for edit service.
     */
    Profile_PresentationController.prototype.editEmailFailure = function(errorMessage) {
        var viewProperties = {
            isLoading: false,
            editEmailError: errorMessage
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     * Method used to show user addresses.
     */
    Profile_PresentationController.prototype.showUserAddresses = function() {
        applicationManager.getNavigationManager().updateForm({
            "addressList": applicationManager.getUserPreferencesManager().getEntitlementAddresses(),
            "isLoading": false
        }, "frmProfileManagement");
    };

    /**
     * Method used to delete user addresses.
     */
    Profile_PresentationController.prototype.deleteAddress = function(AddressObj) {
        this.showProgressBar();
        var params = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "deleteAddressID": AddressObj.Address_id
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "Addresses"), this.deleteAddressFailure.bind(this));
    };

    /**
     * Method used as failure call back for the delete address.
     * @param {String} errorMessage - contains the service error message.
     */
    Profile_PresentationController.prototype.deleteAddressFailure = function(errorMessage) {
        CommonUtilities.showServerDownScreen();
    };

    Profile_PresentationController.prototype.getAddNewAddressView = function() {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'getCountryList'),
                asyncManager.asyncItem(userPrefManager, 'getStatesList'),
                asyncManager.asyncItem(userPrefManager, 'getCityList'),
            ],
            this.getAddNewAddressViewSuccess.bind(this)
        );
    };

    /**
     * Method used to get the add address view model.
     * @param {Object} countries - contains the countries list.
     * @param {Object} states - contains the states list.
     * @param {Object} Cities - contains the cities list.
     */
    Profile_PresentationController.prototype.AddAddressViewModel = function(countries, states, cities) {
        var countryNew = [];
        countryNew.push(["1", "Select a Country"]);
        var stateNew = [];
        stateNew.push(["lbl1", "Select a State"]);
        var cityNew = [];
        cityNew.push(["lbl2", "Select a City"]);
        return {
            serverError: null,
            addressTypes: this.objectToListBoxArray(this.AddressTypes),
            addressTypeSelected: "Work",
            addressLine1: '',
            addressLine2: '',
            countries: countries.map(function(country) {
                return countryNew.push([country.id, country.Name]);
            }),
            countryNew: countryNew,
            states: states.map(function(state) {
                return stateNew.push([state.id, state.Name, state.Country_id]);
            }),
            stateNew: stateNew,
            cities: cities.map(function(city) {
                return cityNew.push([city.id, city.Name, city.Region_id]);
            }),
            cityNew: cityNew,
            citySelected: cities[0].id,
            stateSelected: states[0].id,
            countrySelected: countries[0].id,
            city: '',
            isPreferredAddress: false,
            zipcode: '',
        };
    };

    /**
     * Method used as success call back for the address view model.
     * @param {Object} syncResponseObject - contains the async response object.
     */
    Profile_PresentationController.prototype.getAddNewAddressViewSuccess = function(syncResponseObject) {

        if (syncResponseObject.isAllSuccess()) {
            var viewModel = this.AddAddressViewModel(syncResponseObject.responses[0].data.records, syncResponseObject.responses[1].data.records, syncResponseObject.responses[2].data.records);
            viewModel.addressTypeSelected = "ADR_TYPE_WORK";
            viewModel.countrySelected = "1";
            viewModel.stateSelected = "lbl1";
            viewModel.citySelected = "lbl2";
            applicationManager.getNavigationManager().updateForm({
                isLoading: false,
                addNewAddress: viewModel
            }, "frmProfileManagement");
        } else {
            CommonUtilities.showServerDownScreen();
        }
        this.hideProgressBar();
    };

    /**
     * Method used to save address.
     * @param {Object} addressObj - contains the address Object.
     */
    Profile_PresentationController.prototype.saveAddress = function(addressObj) {
        this.showProgressBar();
        var addresses = [{
            Addr_type: addressObj.Addr_type,
            addrLine1: addressObj.addrLine1,
            addrLine2: addressObj.addrLine2,
            City_id: addressObj.citySelected,
            ZipCode: addressObj.zipcode,
            isPrimary: (addressObj.isPreferredAddress === true) ? "1" : "0",
            Region_id: addressObj.stateSelected
        }];
        addresses = JSON.stringify(addresses);
        addresses = addresses.replace(/"/g, "'");
        var params = {
            "addresses": addresses,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "Addresses"), this.fetchEntitlementsFailure.bind(this));
    };

    /**
     * Method used to get edit address view.
     * @param{Object} address - contains the address object.
     */
    Profile_PresentationController.prototype.getEditAddressView = function(address) {
        this.showProgressBar();
        var asyncManager = applicationManager.getAsyncManager();
        var userPrefManager = applicationManager.getUserPreferencesManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(userPrefManager, 'getCountryList'),
                asyncManager.asyncItem(userPrefManager, 'getStatesList'),
                asyncManager.asyncItem(userPrefManager, 'getCityList'),
            ],
            this.getEditAddressViewSuccess.bind(this, address)
        );
    };

    /**
     * Method used as sucess call bakc for the get edit address vie.
     * @param {Object} address - contains the address object.
     * @param {Object} syncRespnoseObject - contains the async response object.
     */
    Profile_PresentationController.prototype.getEditAddressViewSuccess = function(address, syncResponseObject) {

        if (syncResponseObject.isAllSuccess()) {
            var viewModel = this.AddAddressViewModel(syncResponseObject.responses[0].data.records, syncResponseObject.responses[1].data.records, syncResponseObject.responses[2].data.records);
            viewModel.addressId = address.Address_id;
            viewModel.addressLine1 = address.AddressLine1;
            viewModel.addressLine2 = address.AddressLine2;
            viewModel.addressTypeSelected = address.AddressType;
            viewModel.city = address.CityName;
            viewModel.isPreferredAddress = address.isPrimary === "true";
            viewModel.zipcode = address.ZipCode;
            viewModel.countrySelected = address.Country_id;
            viewModel.stateSelected = address.Region_id;
            viewModel.citySelected = address.City_id;
            viewModel.hidePrefferedAddress = address.isPrimary === "true";
            applicationManager.getNavigationManager().updateForm({
                isLoading: false,
                editAddress: viewModel
            }, "frmProfileManagement");
        } else {
            CommonUtilities.showServerDownScreen();
        }
        this.hideProgressBar();
    };

    /**
     * Method used to get the current user name.
     */
    Profile_PresentationController.prototype.getUserName = function() {
        return applicationManager.getUserPreferencesManager().getCurrentUserName();
    };

    /**
     * Method used to get the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRules = function() {
        applicationManager.getUserPreferencesManager().getPasswordPolicies(this.getPasswordRulesSuccess.bind(this), this.getPasswordRulesFailure.bind(this));
    };

    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            usernamepasswordRules: response.records
        }, "frmProfileManagement");
    };

    /**
     * Method used as failure call back for the get password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesFailure = function() {
        CommonUtilities.showServerDownScreen();
    };

    
  Profile_PresentationController.prototype.getPasswordRulesAndPolicies = function() {
        applicationManager.getUserPreferencesManager().fetchPasswordRulesAndPolicy(this.getPasswordRulesAndPoliciesSuccess.bind(this), this.getPasswordRulesAndPoliciesFailure.bind(this));
    };

    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesAndPoliciesSuccess = function(response) {
       var validationUtility = applicationManager.getValidationUtilManager();
       validationUtility.createRegexForPasswordValidation(response.passwordrules);
        applicationManager.getNavigationManager().updateForm({
            usernamepasswordRules: {"passwordpolicies" : response.passwordpolicy}
        }, "frmProfileManagement");
    };

    /**
     * Method used as failure call back for the get password rules.
     */
    Profile_PresentationController.prototype.getPasswordRulesAndPoliciesFailure = function() {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Method to check exisitng password
     * @param {Object} viewModel - Password entered by user
     */
    Profile_PresentationController.prototype.checkExistingPassword = function(viewModel) {

        var password = viewModel;
        applicationManager.getUserPreferencesManager().checkExistingPassword(password, this.checkExistingPasswordSuccess.bind(this), this.checkExistingPasswordFailure.bind(this));

    };

    /**
     * Method to check exisitng password
     * @param {String} response - contains the existing password response.
     */
    Profile_PresentationController.prototype.checkExistingPasswordSuccess = function(response) {
        if (response.result === "The user is verified") {
            applicationManager.getNavigationManager().updateForm({
                showVerificationByChoice: response
            }, "frmProfileManagement");
        } else if(response.result === "Invalid Credentials"){
            applicationManager.getNavigationManager().updateForm({
                wrongPassword: "Wrong Password"
            }, "frmProfileManagement");
        } else{
            applicationManager.getNavigationManager().updateForm({
                passwordExists: "password exists"
            }, "frmProfileManagement");
        }
    };

    /**
     * Method to check exisitng password
     * @param {String} ErrorMessage - error message
     */
    Profile_PresentationController.prototype.checkExistingPasswordFailure = function(viewModel) {
        applicationManager.getNavigationManager().updateForm({
            passwordExistsServerError: "password exists"
        }, "frmProfileManagement");
    };

    /**
     * Method to request OTP
     */
    Profile_PresentationController.prototype.requestOtp = function(securityQuestionsPayload) {
      this.securityQuestionsPayload = securityQuestionsPayload;
      var mfaManager = applicationManager.getMFAManager();
      var params = {
        "MFAAttributes": {
            "serviceKey": mfaManager.getServicekey()
        }

      }
  
        mfaManager.requestUpdateSecurityQuestionsOTP(params,this.requestOtpSuccess.bind(this),this.requestOtpFailure.bind(this));
    };

    /**
     * Method to check exisitng password
     */
    Profile_PresentationController.prototype.requestOtpSuccess = function(response) {
       var mfaJSON = {
                    "flowType": "SECURITYQUESTION_RESET",
                    "response": response
                };
                applicationManager.getMFAManager().initMFAFlow(mfaJSON);
    };

    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.requestOtpFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            requestOtpError: response
        }, "frmProfileManagement");
    };

    /**
     * Method to verify otp
     * @param {String} viewModel - OTP 
     */
    Profile_PresentationController.prototype.verifyOtp = function(viewModel) {
        var otpJSON = {
            "Otp": viewModel
        };
        applicationManager.getUserPreferencesManager().VerifySecureAccessCode(otpJSON, this.verifyOtpSuccess.bind(this), this.verifyOtpFailure.bind(this));
    };

    /**
     * Method used as success call back for verify otp.
     */
    Profile_PresentationController.prototype.verifyOtpSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyOtp: response.isOtpVerified
        }, "frmProfileManagement");
    };

    /**
     * Method used as failure call back for verify otp. 
     */
    Profile_PresentationController.prototype.verifyOtpFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyOtpError: response.errorMessage
        }, "frmProfileManagement");
    };

    /**
     * Method to check existance of security questions
     */
    Profile_PresentationController.prototype.checkSecurityQuestions = function() {
      applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        var securityQuestionsStatus = applicationManager.getUserPreferencesManager().isSecurityQuestionsConfigured();
        if(securityQuestionsStatus){
            applicationManager.getNavigationManager().updateForm({
                SecurityQuestionExists: "Questions Exist"
            }, "frmProfileManagement");
        }else{
            applicationManager.getNavigationManager().updateForm({
                SecurityQuestionExists: "Questions doesnt Exist"
            }, "frmProfileManagement");
        }
    };

    /**
     * Method to verify Security Question and answers
     * @param {JSON} data - JSON consisting of question and answers
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswer = function(data) {

        data = JSON.stringify(data);
        data = data.replace(/"/g ,  "'");
        applicationManager.getUserPreferencesManager().verifySecurityQuestions(data, this.verifyQuestionsAnswerSuccess.bind(this), this.verifyQuestionsAnswerFailure.bind(this));
    };

    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswerSuccess = function(response) {
        if (response.verifyStatus === 'true') {
            applicationManager.getNavigationManager().updateForm({
                verifyQuestion: "security questions"
            }, "frmProfileManagement");
        } else {
            applicationManager.getNavigationManager().updateForm({
                verifyQuestionAnswerError: "security questions"
            }, "frmProfileManagement");
        }

    };

    /**
     * Method used as failure call back for requestOTP service.
     */
    Profile_PresentationController.prototype.verifyQuestionsAnswerFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            verifyQuestionAnswerError: "security questions"
        }, "frmProfileManagement");
    };

    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestions = function() {
        var scopeObj = this;
        var param = {
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
        };

        applicationManager.getUserPreferencesManager().fetchSecurityQuestions(param, this.getAnswerSecurityQuestionsSuccess.bind(this), this.getAnswerSecurityQuestionsFailure.bind(this));
    };

    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestionsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            answerSecurityQuestion: response.records
        }, "frmProfileManagement");
    };

    /**
     * Method to get Security questions to be answeres
     */
    Profile_PresentationController.prototype.getAnswerSecurityQuestionsFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            getAnswerSecurityQuestionError: response
        }, "frmProfileManagement");
    };

    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestions = function() {
      var data = this.securityQuestionsPayload;
        data = JSON.stringify(data);
        data = data.replace(/"/g ,  "'");
        var params = {
              "MFAAttributes": {
        		"serviceKey": applicationManager.getMFAManager().getServicekey()
    	},
          "securityQuestions": data,
          "userName" : applicationManager.getUserPreferencesManager().getCurrentUserName()
        };

        applicationManager.getUserPreferencesManager().resetSecurityQuestions(params, this.updateSecurityQuestionsSuccess.bind(this), this.updateSecurityQuestionsFailure.bind(this));
    };

    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestionsSuccess = function(response) {
         applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
          applicationManager.getNavigationManager().updateForm({
            update: "security questions"
        }, "frmProfileManagement");
    };

    /**
     * Method to change the answers of the security questions
     * @param {Object} data - questions and their answers
     */
    Profile_PresentationController.prototype.updateSecurityQuestionsFailure = function(data) {
         applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            updateSecurityQuestionError: "security questions"
        }, "frmProfileManagement");
    };

    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestions = function(context, staticSetQuestions) {
        applicationManager.getUserPreferencesManager().fetchAllSecurityQuestions(this.fetchSecurityQuestionsSuccess.bind(this, context, staticSetQuestions), this.fetchSecurityQuestionsFailure.bind(this, context, staticSetQuestions));
    };

    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestionsSuccess = function(context, staticSetQuestions, response) {
      var mfaManager  = applicationManager.getMFAManager();
       mfaManager.setMFAFlowType("SECURITYQUESTION_RESET");
      mfaManager.setServicekey(response.serviceKey);
        var i = 0;
        while (i < response.records.length) {
            context.securityQuestions[i] = response.records[i].SecurityQuestion;
            context.flagToManipulate[i] = "false";
            i++;
        }
        staticSetQuestions(context, response.records);
    };

    /**
     * Method to fetch all the security questions
     * @param {Object} context - JSON to handle the security questions
     * @param {function} staticSetQuestions - function binded to handle the response
     */
    Profile_PresentationController.prototype.fetchSecurityQuestionsFailure = function(context, staticSetQuestions, response) {
        staticSetQuestions(context, response.records);
    };

    /**
     * Method to update secure access option
     * @param {Object} viewModel - Secure access data which needs to be updated
     */
    Profile_PresentationController.prototype.updateSecureAccessOptions = function(context) {
        applicationManager.getUserPreferencesManager().updateSecureAccessSettings(context, this.updateSecureAccessOptionsSuccess.bind(this), this.updateSecureAccessOptionsFailure.bind(this));
    };

    /**
     * Method used as success call back for updateSecureAccessOptions
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.updateSecureAccessOptionsSuccess = function(response) {
        applicationManager.getNavigationManager().updateForm({
            secureAccessOption: response
        }, "frmProfileManagement");
    };

    /**
     * Method method used as failure call back for updateSecureAccessOptions.
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.updateSecureAccessOptionsFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            secureAccessOptionError: response
        }, "frmProfileManagement");
    };

    /**
     * Method used to fetch alerts.
     * @param {Object} alertType - contains the type of alert to fetch.
     */
    Profile_PresentationController.prototype.fetchAlerts = function(alertType) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        this.showProgressBar();
        var self = this;
        var params = {
            userName: applicationManager.getUserPreferencesManager().getCurrentUserName(),
            alertTypeName: alertType
        };

        applicationManager.getAlertsManager().fetchProfileAlerts(params, this.fetchAlertsSuccess.bind(this), this.fetchAlertsFailure.bind(this));
    };

    /**
     * Method method used as failure call back for updateSecureAccessOptions.
     * @param {Object} resonse - service response for updateSecureAccessOptions.
     */
    Profile_PresentationController.prototype.fetchAlertsFailure = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "AlertsError": response
        }, 'frmProfileManagement');
    };

    /**
     * updateAlerts- Method to update alerts and mapping to UI
     * @param {Object} response -  alerts list and alert type
     */
    Profile_PresentationController.prototype.updateAlerts = function(alertsData, responseData) {
        var self = this;
        this.showProgressBar();
        var params = {
            AlertCategoryId: alertsData.AlertCategoryId,
            AccountId: null,
            isSubscribed: alertsData.isSubscribed,
            channelPreference: alertsData.channelPreference,
            typePreference: alertsData.typePreference
        };
        applicationManager.getAlertsManager().setAlertPreferences(params, this.updateAlertsSuccess.bind(this,responseData), this.updateAlertsFailure.bind(this));
    };

    /**
     * updateAlertsSuccess- Method to update alerts and mapping to UI
     * @param {Object} response -  alerts list and alert type
     */

    Profile_PresentationController.prototype.updateAlertsSuccess = function(responseData) {
        applicationManager.getNavigationManager().updateForm({
            "alertsSaved": responseData
        }, 'frmProfileManagement');
        this.hideProgressBar();

    };

    /**
     * updateAlerts- Method to update alerts and mapping to UI
     * @param {Object} response -  contains the error response.
     */

    Profile_PresentationController.prototype.updateAlertsFailure = function(response) {
        this.hideProgressBar();
        CommonUtilities.showServerDownScreen();
    };

    /**
     * Method to get user emails.
     */
    Profile_PresentationController.prototype.getUserEmail = function() {
        applicationManager.getNavigationManager().updateForm({
            "emails": userObjectToEmailList(applicationManager.getUserPreferencesManager().getEntitlementEmailIds())
        }, 'frmProfileManagement');
    };

    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfile = function() {
        applicationManager.getUserPreferencesManager().fetchUserProfile(this.showUserProfileSuccess.bind(this), this.showUserProfileFailure.bind(this));
    };

    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfileSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            "userProfile": response[0],
            "isLoading": false
        }, 'frmProfileManagement');
    };

    /**
     * Method used to fetch the profile
     */
    Profile_PresentationController.prototype.showUserProfileFailure = function(response) {
        CommonUtilities.showServerDownScreen();
    };

    /**
     * Method to update the password
     * @param {Object} password - contains the password.
     */
    Profile_PresentationController.prototype.updatePassword = function(oldPassword,newPassword) {
  var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var params = {
            "oldPassword": oldPassword,
            "newPassword": newPassword
        };
        applicationManager.getUserPreferencesManager().updateUserPassword(params, this.updatePasswordSuccess.bind(this), this.updatePasswordFailure.bind(this));
    };


    /**
     * Method to update the password
     * @param {Object} response - contains the response from the service.
     */
    Profile_PresentationController.prototype.updatePasswordSuccess = function(response) {
            var mfaManager = applicationManager.getMFAManager();
        if(response && response.MFAAttributes){
          if(response.MFAAttributes.isMFARequired == "true"){
            var mfaJSON = {
                "flowType": "UPDATE_PASSWORD",
                "response": response
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
          }
       }else{
         var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
           authModule.presentationController.doLogout({
                     "text" : "password",
                    "action": "userNamePasswordSuccessfullyChanged"
                });
       }
    };

    /**
     * Method used as the failure call back for update password.
     * @param {String} viewModel - Data required to update password
     */
    Profile_PresentationController.prototype.updatePasswordFailure = function(response) {
      applicationManager.getNavigationManager().navigateTo('frmProfileManagement');  
      applicationManager.getNavigationManager().updateForm({
            passwordServerError: response.serverErrorRes
        }, "frmProfileManagement");
    };
    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccess = function() {
        applicationManager.getUserPreferencesManager().getSecureAccessCodeOptions(this.checkSecureAccessSuccess.bind(this), this.checkSecureAccessFailure.bind(this));
    };

    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccessSuccess = function(response) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        applicationManager.getNavigationManager().updateForm({
            securityAccess: response[0]
        }, "frmProfileManagement");
    };

    /**
     * Method to Check existing secure access option
     */
    Profile_PresentationController.prototype.checkSecureAccessFailure = function(response) {
        applicationManager.getNavigationManager().updateForm({
            checkSecurityAccessError: response
        }, "frmProfileManagement");
    };

    /**
     * Method to show setting screen
     * @param {String} userName - Contains the userName.
     */
  Profile_PresentationController.prototype.updateUsername = function(userName) {
         var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var params =  {
            "newUserName":userName,
            "oldUserName" : userPreferencesManager.getCurrentUserName()
        };
     this.newUserName = userName;
        applicationManager.getUserPreferencesManager().updateUserName(params, this.updateUsernameSuccess.bind(this), this.updateUsernameFailure.bind(this));
    };    
    /**
     * Method used as success call back for update username 
     * @param {Object} response - contains the response .
     */
   Profile_PresentationController.prototype.updateUsernameSuccess = function(response) {
     var mfaManager = applicationManager.getMFAManager();
     var userpreferencesManager =  applicationManager.getUserPreferencesManager();
        if(response && response.MFAAttributes){
          if(response.MFAAttributes.isMFARequired == "true"){
            var mfaJSON = {
                "flowType": "UPDATE_USERNAME",
                "response": response,
              	"userName":this.newUserName
            };
            applicationManager.getMFAManager().initMFAFlow(mfaJSON);
          }
       }  else{
          userpreferencesManager.setCurrentUserName(this.newUserName);
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
           authModule.presentationController.doLogout({
                    "text" : "username",
                    "action": "userNamePasswordSuccessfullyChanged"
            });
       }
    };

    /**
     * Method used as failure call back for update user name
     * @param {Object} response - contains the response for update user name.
     */
    Profile_PresentationController.prototype.updateUsernameFailure = function(response) {
      applicationManager.getNavigationManager().navigateTo('frmProfileManagement');  
      applicationManager.getNavigationManager().updateForm({
            updateUsernameError: response.serverErrorRes
        }, "frmProfileManagement");
    };

    /**
     * Method used to update address. 
     *@param {Object} context - contains the user address Object.
     */
    Profile_PresentationController.prototype.updateAddress = function(context) {
        var addresses = [{
                "Addr_type": context.Addr_type,
                "isPrimary": (context.isPrimary === true) ? "1" : "0",
                "addrLine1": context.addrLine1,
                "addrLine2": context.addrLine2,
                "ZipCode": context.ZipCode,
                "City_id": context.City_id,
                "Addr_id": context.addressId,
                "Region_id": context.Region_id
            }],
            addresses = JSON.stringify(addresses);
        addresses = addresses.replace(/"/g, "'");
        var params = {
            "addresses": addresses,
            "userName": applicationManager.getUserPreferencesManager().getCurrentUserName(),
            "modifiedByName": applicationManager.getUserPreferencesManager().getCurrentUserName()
        };
        applicationManager.getUserPreferencesManager().updateUserProfileDetails(params, this.fetchEntitlements.bind(this, "Addresses"), this.fetchEntitlementsFailure.bind(this));
    };

    /**
     * Method used to save the user profile pic.
     *@ param {String} base64String - contains the base 64 string. 
     */
    Profile_PresentationController.prototype.userImageUpdate = function(base64String) {
        applicationManager.getUserPreferencesManager().updateUserProfilePic(base64String, this.showUserProfile.bind(this), this.fetchEntitlementsFailure.bind(this));
    };


    /******************************************Ankit Refactored******************************************
     * ***************************************************************************************************
     * ***************************************************************************************************
     */

    /**
     * Method used to show the user email view of profile management.
     */
    Profile_PresentationController.prototype.getUserEmail = function() {
        this.showProgressBar();
        var viewProperties = {
            emails: applicationManager.getUserPreferencesManager().getEntitlementEmailIds(),
            isLoading: false
        };
        applicationManager.getNavigationManager().updateForm(viewProperties, "frmProfileManagement");
    };

    /**
     *Method to navigate frmProfileManagement and show account preference page where accounts are sorted as per their preference order
     */
    Profile_PresentationController.prototype.showPreferredAccounts = function() {
        this.getPreferredAccounts();
        applicationManager.getNavigationManager().navigateTo('frmProfileManagement');
        applicationManager.getNavigationManager().updateForm({
            showPrefferedView: true,
            isLoading: true
        }, 'frmProfileManagement');
    }

    /**
     * Method to show account preference page where accounts are sorted as per their preference order
     *
     */
    Profile_PresentationController.prototype.getPreferredAccounts = function(){
        applicationManager.getNavigationManager().updateForm({
            isLoading: true
        }, 'frmProfileManagement');
        var self = this;
        /* Commented this line as per the bug number ARB-11106. Removed the aggregate accounts call temperorily
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            function completionCallback(asyncResponse) {
                if (asyncResponse.isAllSuccess()) {
                    self.fetchAccountsSuccess(asyncResponse.responses);
                } else {
                    self.fetchAccountsFailure();
                }
            }
            var username = applicationManager.getUserPreferencesManager().getUserObj().userName;
            var asyncManager = applicationManager.getAsyncManager();
            asyncManager.callAsync([
                asyncManager.asyncItem(
                    applicationManager.getAccountManager(),
                    "fetchInternalAccounts"
                ),
                asyncManager.asyncItem(
                    applicationManager.getAccountManager(),
                    "fetchExternalAccountsData", [username]
                )
            ], completionCallback);
        } else {
            applicationManager.getAccountManager().fetchInternalAccounts(this.fetchAccountsSuccess.bind(this), this.fetchAccountsFailure.bind(this));
        }
        */
        applicationManager.getAccountManager().fetchInternalAccounts(this.fetchAccountsSuccess.bind(this), this.fetchAccountsFailure.bind(this));
    }

    /**
     *Method is invoked when fetching of accounts is successful. It covers both scenarios of internal and external accounts
     * @param {Collection} accounts list of all the accounts i.e internal account and external accounts based on aggregatedAccount flag of configurations
     */
    Profile_PresentationController.prototype.fetchAccountsSuccess = function(accounts) {
        var finalAccounts = [],
            self = this;
        /* Commented this line as per the bug number ARB-11106. Removed the aggregate accounts call temperorily
        if (applicationManager.getConfigurationManager().getConfigurationValue("isAggregatedAccountsEnabled") === "true") {
            accounts[0].data.forEach(function(internalAccount) {
                finalAccounts.push(internalAccount);
            });
            accounts[1].data.forEach(function(externalAccount) {
                finalAccounts.push(self.processExternalAccountsData(externalAccount));
            });
        } else {
            finalAccounts = accounts;
        }*/
        finalAccounts = accounts;
        applicationManager.getNavigationManager().updateForm({
            getPreferredAccountsList: finalAccounts
        }, 'frmProfileManagement');
    };

    /**
     *  Process external Account Data
     * @param {Collection} rawData External accounts JSON
     * @returns Collection of altered data
     */
    Profile_PresentationController.prototype.processExternalAccountsData = function(rawData) {
        var isError = function(error) {
            try {
                if (error && error.trim() !== "") {
                    return true;
                }
                return false;
            } catch (error) {

            }
        }
        var account = {};
        account.accountName = rawData.AccountName;
        account.nickName = rawData.NickName;
        account.accountID = rawData.Number;
        account.accountType = rawData.TypeDescription;
        account.availableBalance = rawData.AvailableBalance;
        account.currentBalance = rawData.AvailableBalance;
        account.outstandingBalance = rawData.AvailableBalance;
        account.availableBalanceUpdatedAt = (rawData.LastUpdated) ? (CommonUtilities.getTimeDiferenceOfDate(rawData.LastUpdated)) : kony.i18n.getLocalizedString('i18n.AcountsAggregation.timeDifference.justNow');
        if (String(rawData.FavouriteStatus).trim().toLowerCase() === "true") {
            account.favouriteStatus = "1";
        } else {
            account.favouriteStatus = "0";
        }
        account.bankLogo = rawData.BankLogo;
        account.isError = isError(rawData.error);
        account.israwData = true; 
        account.bankName = rawData.BankName;
        account.userName = (rawData.Username) ? rawData.Username : rawData.username;
        account.bankId = rawData.Bank_id;
        account.externalAccountId = rawData.Account_id;
        account.accountHolder = rawData.AccountHolder;
        account.isExternalAccount= true;
        return account;
    };

    /**
     *Method that gets called in case fetching of accounts fails
     */
    Profile_PresentationController.prototype.fetchAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            "getPreferredAccountsList": {
                "errorCase": true
            }
        }, 'frmProfileManagement');
    };


    /**
     * Method to save preferred account data
     * @param {JSON} data -Json containing account info like favouriteStatus, NickName, E-statements etc
     */
    Profile_PresentationController.prototype.savePreferredAccountsData = function(data) {
        if (data.external) {
            var newdata = {
                "NickName": data.NickName,
                "FavouriteStatus": data.FavouriteStatus,
                "Account_id": data.Account_id,
            }
            applicationManager.getAccountManager().updateExternalAccountFavouriteStatus(newdata,this.updateAccountPreferenceSuccess.bind(this), this.updateAccountPreferenceFailure.bind(this));
        } else {
            applicationManager.getAccountManager().updateUserAccountSettingsForEstatements(data, this.updateAccountPreferenceSuccess.bind(this), this.updateAccountPreferenceFailure.bind(this));
        }
    };

    /**
     * Method that gets called on success of saving account preferences
     */
    Profile_PresentationController.prototype.updateAccountPreferenceSuccess = function() {
        this.getPreferredAccounts();
    }

    /**
     * Method that gets called in case of saving account preferences failure
     */
    Profile_PresentationController.prototype.updateAccountPreferenceFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            errorEditPrefferedAccounts: true
        }, 'frmProfileManagement');
    }

    /**
     * * Method to change the preference Number of the accounts
     * @param {JSON} updatedAccounts Accounts with account number and their preference number
     */
    Profile_PresentationController.prototype.setAccountsPreference = function(updatedAccounts) {
        var data = JSON.stringify(updatedAccounts);
        data = data.replace(/"/g ,  "'");
        var final = {};
        final['accountli'] = data;
        applicationManager.getAccountManager().setAccountsPreference(final, this.getPreferredAccounts.bind(this), this.getPreferredAccounts.bind(this));
    };

    /**
     * Method to get default user accounts from user object
     */
    Profile_PresentationController.prototype.getDefaultUserProfile = function() {
        var userObj = applicationManager.getUserPreferencesManager().getUserObj();
        var data = {
            defaultTransferAccount: userObj['default_account_transfers'],
            defaultBillPayAccount: userObj['default_account_billPay'],
            defaultP2PAccount: userObj['default_from_account_p2p'],
            defaultCheckDepositAccount: userObj['default_account_deposit']
        };
        this.getDefaultAccountNames(data);
    };


    /**
     * Method to get Default account name for the default user accounts
     * @param {JSON} accountNumbers Default account numbers of logged in user   
     */
    Profile_PresentationController.prototype.getDefaultAccountNames = function(accountNumbers) {
        var defaultNames = [];
        this.defaultAccounts = accountNumbers;
        var accounts = applicationManager.getAccountManager().getInternalAccounts()
        for (var keys in accountNumbers) {
            if (accountNumbers[keys] && accountNumbers[keys] !== "-1") {
                for (var i in accounts) {
                    if (accountNumbers[keys] === accounts[i].accountID) {
                        defaultNames[keys] = accounts[i].accountName;
                        break;
                    }
                }
            } else {
                defaultNames[keys] = "None";
            }
        }
        applicationManager.getNavigationManager().updateForm({
            "showDefautUserAccounts": defaultNames
        }, 'frmProfileManagement');
    };

    /**
     * Method to get List of accounts for selecting default accounts for different transaction types
     */
    Profile_PresentationController.prototype.getAccountsList = function() {
        var defaultAccounts = this.defaultAccounts;
        var getDefaultSelectedKey = function(data) {
            if (data && data !== "-1") {
                return data;
            } else {
                return 'undefined';
            }
        };
        var getAccountsListViewModel = {};
        getAccountsListViewModel['TransfersAccounts'] = applicationManager.getAccountManager().getFromTransferSupportedAccounts();
        getAccountsListViewModel['defaultTransfersAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultTransferAccount);
        getAccountsListViewModel['BillPayAccounts'] = applicationManager.getAccountManager().getBillPaySupportedAccounts();
        getAccountsListViewModel['defaultBillPayAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultBillPayAccount);
        getAccountsListViewModel['P2PAccounts'] = applicationManager.getAccountManager().getFromTransferSupportedAccounts();
        getAccountsListViewModel['defaultP2PAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultP2PAccount);
        getAccountsListViewModel['CheckDepositAccounts'] = applicationManager.getAccountManager().getDepositSupportedAccounts();
        getAccountsListViewModel['defaultCheckDepositAccounts'] = getDefaultSelectedKey(defaultAccounts.defaultCheckDepositAccount);
        applicationManager.getNavigationManager().updateForm({
            "getAccountsList": getAccountsListViewModel
        }, 'frmProfileManagement');
    };


    /**
     * Method to save default accounts for different type of transactions
     * @param {JSON} defaultAccounts Accounts with default account numbers
     */
    Profile_PresentationController.prototype.saveDefaultAccounts = function(defaultAccounts) {
        applicationManager.getUserPreferencesManager().updateUserDetails(defaultAccounts, this.saveDefaultAccountsSuccess.bind(this), this.saveDefaultAccountsFailure.bind(this));
    };

    /**
     * Method that gets called when saving default accounts is successful
     */
    Profile_PresentationController.prototype.saveDefaultAccountsSuccess = function() {
        applicationManager.getUserPreferencesManager().fetchUser(this.getDefaultUserProfile.bind(this), this.saveDefaultAccountsFailure.bind(this))
    }

    /**
     * Method that gets called when saving default accounts is failed
     */
    Profile_PresentationController.prototype.saveDefaultAccountsFailure = function() {
        applicationManager.getNavigationManager().updateForm({
            "showDefautUserAccounts": {
                errorCase: true
            }
        }, 'frmProfileManagement');
    }

     /**
  * Method to save external account data
  * @member of Profile_PresentationController
  * @param {JSON} data - accounts data
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.saveExternalAccountsData=function(data){
    var newdata = {
      "NickName": data.NickName,
      "FavouriteStatus": data.FavouriteStatus,
      "Account_id": data.Account_id,
    }
    applicationManager.getAccountManager().updateExternalAccountFavouriteStatus(newdata, this.saveExternalAccountsDataSuccess.bind(this), this.saveExternalAccountsDataFailure.bind(this));
  };

  Profile_PresentationController.prototype.saveExternalAccountsDataSuccess=function(){ 
    applicationManager.getNavigationManager().updateForm({errorSaveExternalAccounts:{error:false}});
    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountsModule.presentationController.showAccountsDashboard();
  }
  
  Profile_PresentationController.prototype.saveExternalAccountsDataFailure=function(){
    applicationManager.getNavigationManager().updateForm({errorSaveExternalAccounts:true});
  }
  
  /**
     * Method to to fetch alerts data and channels basing on category to be shown.
     * @param {String} alertID - ID of the Alert Type
     */
  Profile_PresentationController.prototype.fetchAlertsDataById = function(alertID) {
    var scopeObj = this;
    applicationManager.getNavigationManager().updateForm({
        isLoading: true
    }, 'frmProfileManagement');
     function completionCallback (asyncResponse){
        if (asyncResponse.isAllSuccess()) {
            scopeObj.fetchAlertsSuccess(asyncResponse.responses, alertID);
        } else {
            scopeObj.fetchEntitlementsFailure(); // basically, sever down screen function
        }
    }
    var params ={ 
        "AlertCategoryId": alertID
      };
    var asyncManager = applicationManager.getAsyncManager();
    asyncManager.callAsync(
        [
            asyncManager.asyncItem(applicationManager.getAlertsManager(), "fetchAlertChannels", [params]),
            asyncManager.asyncItem(applicationManager.getAlertsManager(), "fetchAlertsInCategory",[params] ),
        ], completionCallback);
 
};
  /**Success callback for fetch alerts by id
  * @param {Object} response - contains the service response for the Alerts channel and category.
  * @param {String} AlertId - ID of the Alert
     */
    Profile_PresentationController.prototype.fetchAlertsSuccess = function(responses, alertId) {
        var alertsData = {
            "channels":responses[0].data.records,
            "alertsData":responses[1].data.records,
            "isAlertTypeSelected": responses[1].data.categorySubscription.isSubscribed,
            "isInitialLoad": responses[1].data.categorySubscription.isInitialLoad,
            "AlertCategoryId": alertId
        };
        applicationManager.getNavigationManager().updateForm({
            AlertsDataById: alertsData
        }, 'frmProfileManagement'); 
    
      
    };
  /**
     * Method to to fetch the type of customer alert category to be shown.
     * @param {String} entryPoint - Dicides which flow to be called
     */
    Profile_PresentationController.prototype.fetchAlertsCategory = function(entryPoint) {
        applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
        this.showProgressBar();
        applicationManager.getAlertsManager().fetchAlertsCategory(this.fetchAlertsCategorySuccess.bind(this,entryPoint), this.fetchAlertsCategoryFailure.bind(this));
    };
    /**
     * Success callback for customer alert category to be shown.
     * @param {String} entryPoint - Dicides which flow to be called
     * @param {Object} response - contains the service response for the Alerts Category.
     */
    Profile_PresentationController.prototype.fetchAlertsCategorySuccess=function(entryPoint , response){ 
        applicationManager.getNavigationManager().updateForm({
            Alerts: response
        }, 'frmProfileManagement'); 
      if (entryPoint === "profileSettings") {
            this.showProfileSettings();
        }
        if (entryPoint === "accountSettings") {
            this.showPreferredAccounts();
        }
        if (entryPoint === "alertSettings") {
            this.fetchFirstAlerttype();
        }
       if(entryPoint=="securityQuestions"){
           this.showSecurityQuestionsScreen();
       } 
    };
   /**
     * Error callback for customer alert category to be shown.
     */
    Profile_PresentationController.prototype.fetchAlertsCategoryFailure=function(response){
    
    }
  /**
     * Entry method to profile settings
     * @param {String} entryPoint - Dicides which flow to be called
     */
    Profile_PresentationController.prototype.enterProfileSettings = function(entryPoint) {
      this.showProgressBar();
      this.initializeUserProfileClass();
      this.fetchAlertsCategory(entryPoint);
    };
  /**
     * Method to navigate to profile form
     */
    Profile_PresentationController.prototype.fetchFirstAlerttype = function() {
      applicationManager.getNavigationManager().updateForm({
            fetchFirstAlert: {}
        }, 'frmProfileManagement'); 
    };
  /**
     * Method to fetch the username rules and policies
     */  
  Profile_PresentationController.prototype.getUsernameRulesAndPolicies = function() {
     applicationManager.getUserPreferencesManager().fetchUsernameRulesAndPolicy(this.getUsernameRulesAndPoliciesSuccess.bind(this), this.getUsernameRulesAndPoliciesFailure.bind(this));
    };

    /**
     * Method used as success call back for the password rules.
     * @param {Object} response - contains the service response for the password rules.
     */
    Profile_PresentationController.prototype.getUsernameRulesAndPoliciesSuccess = function(response) {
       var validationUtility = applicationManager.getValidationUtilManager();
       validationUtility.createRegexForUsernameValidation(response.usernamerules);
        applicationManager.getNavigationManager().updateForm({
            usernamepolicies: {"usernamepolicies" : response.usernamepolicy}
        }, "frmProfileManagement");
    };
  
  /**
     *failure callback of method getUsernameRulesAndPolicies
     */
  Profile_PresentationController.prototype.getUsernameRulesAndPoliciesFailure = function() {
    
 };
  
   Profile_PresentationController.prototype.showUserNameAndPassword = function() {
     applicationManager.getNavigationManager().navigateTo("frmProfileManagement");
      applicationManager.getNavigationManager().updateForm({
            usernameAndPasswordLanding: {}
        }, 'frmProfileManagement'); 
 };
  
   Profile_PresentationController.prototype.showSecurityQuestionsScreen = function() {
        applicationManager.getNavigationManager().updateForm({
            showSecurityQuestion: true,
            isLoading: true
        }, 'frmProfileManagement');
    };

    /********************************************************************** */
    return Profile_PresentationController;
});