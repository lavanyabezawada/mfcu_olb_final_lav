define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_a4e5a713566840db979d5ff8dc1bd3ad: function AS_FlexContainer_a4e5a713566840db979d5ff8dc1bd3ad(eventobject) {
        var self = this;
        this.ShowDomesticAccount();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_e8548f1abafa4153806d165548994d3e: function AS_FlexContainer_e8548f1abafa4153806d165548994d3e(eventobject) {
        var self = this;
        this.showInternationalAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_bc41c3b8bc1d4576b53acb90a8ef149d: function AS_FlexContainer_bc41c3b8bc1d4576b53acb90a8ef149d(eventobject) {
        var self = this;
        this.showOneTimeTransfer();
    },
    /** preShow defined for frmWireTransfer **/
    AS_Form_c3ba9435f751460a9db3346daf50c19d: function AS_Form_c3ba9435f751460a9db3346daf50c19d(eventobject) {
        var self = this;
        this.formPreshow();
    },
    /** onTouchEnd defined for frmWireTransfer **/
    AS_Form_a3f85960f6584452949f79666367e4e9: function AS_Form_a3f85960f6584452949f79666367e4e9(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});