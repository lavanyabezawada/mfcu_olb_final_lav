define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnDomesticAccountKA **/
    AS_Button_g2a9fb21282d4ce18a20720c907f3fce: function AS_Button_g2a9fb21282d4ce18a20720c907f3fce(eventobject) {
        var self = this;
        //this.addDomesticAccount();
        this.presenter.showDomesticAccounts(this);
    },
    /** onClick defined for btnInternationalAccountKA **/
    AS_Button_b72f02dbcd9e4a33b75335ff8d89544b: function AS_Button_b72f02dbcd9e4a33b75335ff8d89544b(eventobject) {
        var self = this;
        //this.addInternationalAccount();
        this.presenter.showInternationalAccounts(this);
    },
    /** onKeyUp defined for tbxRoutingNumberKA **/
    AS_TextField_g8da09fedddb4d4da4ce6548705a811b: function AS_TextField_g8da09fedddb4d4da4ce6548705a811b(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBankNameKA **/
    AS_TextField_ccefffac87b74286bd259509d55df73b: function AS_TextField_ccefffac87b74286bd259509d55df73b(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_f41816e12da442a58f2ac2c108aca45e: function AS_TextField_f41816e12da442a58f2ac2c108aca45e(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_cc6837cd982746debee5dab3a241eff9: function AS_TextField_cc6837cd982746debee5dab3a241eff9(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_ad851b32f8aa4d20a0c86179965f93ba: function AS_TextField_ad851b32f8aa4d20a0c86179965f93ba(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_de1d4d3b2af1446facf8a4219fafbb23: function AS_TextField_de1d4d3b2af1446facf8a4219fafbb23(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onClick defined for btnCancelKA **/
    AS_Button_f360c5e34c69406fa2c86fae24ace2a8: function AS_Button_f360c5e34c69406fa2c86fae24ace2a8(eventobject) {
        var self = this;
        this.presenter.cancelTransaction();
    },
    /** onClick defined for btnAddAccountKA **/
    AS_Button_f5a672211dd6450596707e4131aa4a6c: function AS_Button_f5a672211dd6450596707e4131aa4a6c(eventobject) {
        var self = this;
        this.addDomesticAccount();
    },
    /** onKeyUp defined for tbxIntSwiftCodeKA **/
    AS_TextField_ha986d22a14840dda0c3998fcbc0c316: function AS_TextField_ha986d22a14840dda0c3998fcbc0c316(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBankNameKA **/
    AS_TextField_a892a0332988490d9ac6d609f7cfc6cc: function AS_TextField_a892a0332988490d9ac6d609f7cfc6cc(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberKA **/
    AS_TextField_a44523c6bfb24d9dbcf990c1ba926c6e: function AS_TextField_a44523c6bfb24d9dbcf990c1ba926c6e(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberAgainKA **/
    AS_TextField_a4c5a63018c74f2496cd5a6e62035580: function AS_TextField_a4c5a63018c74f2496cd5a6e62035580(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBeneficiaryNameKA **/
    AS_TextField_b188aa342fdc4deba1c8b3b9a074bff1: function AS_TextField_b188aa342fdc4deba1c8b3b9a074bff1(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNickNameKA **/
    AS_TextField_ba54143a059a4961affa6cbd1385e961: function AS_TextField_ba54143a059a4961affa6cbd1385e961(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onClick defined for btnIntCancelKA **/
    AS_Button_e4bed968b34641e7ab839b69b400edfe: function AS_Button_e4bed968b34641e7ab839b69b400edfe(eventobject) {
        var self = this;
        this.presenter.cancelTransaction();
    },
    /** onClick defined for btnIntAddAccountKA **/
    AS_Button_bfb4c62952ec43dc8dfc80e0dd5359ff: function AS_Button_bfb4c62952ec43dc8dfc80e0dd5359ff(eventobject) {
        var self = this;
        this.addInternationalAccount();
    },
    /** onClick defined for flxMainContainer **/
    AS_FlexContainer_f910953ee2b540948040785353e93bfb: function AS_FlexContainer_f910953ee2b540948040785353e93bfb(eventobject) {
        var self = this;
        //test
    },
    /** preShow defined for frmAddExternalAccount **/
    AS_Form_c6250d3a49a245c7a2c28d26b7debdd7: function AS_Form_c6250d3a49a245c7a2c28d26b7debdd7(eventobject) {
        var self = this;
        this.preshowFrmAddAccount();
    },
    /** postShow defined for frmAddExternalAccount **/
    AS_Form_b366a82cbd174f63884727df2d0ac2e8: function AS_Form_b366a82cbd174f63884727df2d0ac2e8(eventobject) {
        var self = this;
        this.postshowAddExternalAccount();
    },
    /** onDeviceBack defined for frmAddExternalAccount **/
    AS_Form_h5873d8114a44ef4a2d91f29507df0fc: function AS_Form_h5873d8114a44ef4a2d91f29507df0fc(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAddExternalAccount **/
    AS_Form_df5526807c524e76bb92247c42a8d64d: function AS_Form_df5526807c524e76bb92247c42a8d64d(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});