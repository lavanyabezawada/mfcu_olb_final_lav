define({ 
	
 showSelectedRow: function(){
		var index = kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.selectedIndex;
		var rowIndex = index[1];
		var data = kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.data;
		data[rowIndex].imgDropdown = "arrow_down.png";
        data[rowIndex].template = "flxManageRecipientsWireTransfersMobile";
		kony.application.getCurrentForm().WireTransferContainer.segWireTransfers.setDataAt(data[rowIndex], rowIndex);
        kony.application.getCurrentForm().forceLayout();
        this.AdjustScreen();
	},
  DeleteAction: function() {
     var currForm = kony.application.getCurrentForm();
     currForm.flxPopup.setVisibility(true);
     currForm.CustomPopup.lblPopupMessage.text = "Are you sure you want to Delete the Recipient ?";
     currForm.CustomPopup.lblHeading.text = "DELETE";
     currForm.forceLayout();
  },
  showWireTransferActivityScreen: function() {
     var currForm = kony.application.getCurrentForm();
     this.hideAll();
     currForm.view.flxWireTransferActivityWindow.setVisibility(true);
	 this.showRightBar("WireTransfersWindow");
	 currForm.forceLayout();
  },
  hideAll: function() {
    var currForm = kony.application.getCurrentForm();
	currForm.view.flxWireTransfersWindow.setVisibility(false);
	currForm.view.flxWireTransferActivityWindow.setVisibility(false);
	currForm.view.flxAddRecipientsWindow.setVisibility(false);
	currForm.view.flxNoRecipients.setVisibility(false);
	currForm.view.flxActivateWireTransfer.setVisibility(false);
	currForm.view.flxConfirmDetails.setVisibility(false);
	currForm.view.flxRightBar.setVisibility(false);
	currForm.view.flxOneTimeTransfer.setVisibility(false);
	currForm.forceLayout();
},
  MakeTransferAction: function() {
    var currForm = kony.application.getCurrentForm();
	this.showWireTransfersScreen();
	currForm.view.WireTransferContainer.btnMakeTransfer.skin = "sknBtnAccountSummarySelectedmod";
	currForm.view.WireTransferContainer.btnRecent.skin = "sknBtnAccountSummaryUnselectedTransfer";
	currForm.view.WireTransferContainer.btnManageRecipient.skin = "sknBtnAccountSummaryUnselectedTransfer";
	this.showAllSeperatorsWireTransfer();
	currForm.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
	currForm.view.breadcrumb.btnBreadcrumb2.text = "MAKE TRANSFER";
	currForm.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(true);
	currForm.view.WireTransferContainer.flxSortRecent.setVisibility(false);
	currForm.showRightBar("WireTransfersWindow");
    currForm.forceLayout();
},
  showWireTransfersScreen: function() 
  {
    var currForm = kony.application.getCurrentForm();
    this.hideAll();
	currForm.view.flxWireTransfersWindow.setVisibility(true);
	this.showRightBar("WireTransfersWindow");
	currForm.forceLayout();
  },
  showAllSeperatorsWireTransfer: function() {
    var currForm = kony.application.getCurrentForm();
    currForm.view.WireTransferContainer.flxTabsSeperator1.setVisibility(true);
	currForm.view.WireTransferContainer.flxTabsSeperator2.setVisibility(true);
	currForm.view.WireTransferContainer.flxTabsSeperator3.setVisibility(true);
	currForm.view.WireTransferContainer.flxTabsSeperator4.setVisibility(true);
    currForm.forceLayout();
},
  showRightBar: function() {
    var currForm = kony.application.getCurrentForm();
    currForm.view.flxRightBar.setVisibility(true);
	if(screenName == "WireTransfersWindow") {
		currForm.view.flxRightBar.top = "45dp";
	}
  },
  editAction: function() {
  var currForm = kony.application.getCurrentForm();
  currForm.showOneTimeTransfer();
  currForm.oneTimeTransfer.lblHeader.text = "EDIT RECIPIENT";
  currForm.oneTimeTransfer.flxAccountType.setVisibility(false);
  currForm.setOneTimeTransferStep(1);
  currForm.breadcrumb.btnBreadcrumb2.text = "EDIT RECIPIENT";
  currForm.detailsJson.isDomesticVar = "false";
  currForm.detailsJson.isInternationalVar = "true";
  currForm.forceLayout();
  },
  //UI Code
    AdjustScreen: function () {
        this.view.forceLayout();
        var currForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currForm.customheader.frame.height + currForm.flxMain.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.frame.height;
            if (diff > 0)
                currForm.flxFooter.top = mainheight + diff + 40 + "dp";
            else
                currForm.flxFooter.top = mainheight + 40 + "dp";
        } else {
            currForm.flxFooter.top = mainheight + 40 + "dp";
        }
        currForm.forceLayout();
    },    
 });