define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_ea5c6bf666b4499cacce8541dba8e7b0: function AS_FlexContainer_ea5c6bf666b4499cacce8541dba8e7b0(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_bc85717b509e4024be651f7e19e71443: function AS_Button_bc85717b509e4024be651f7e19e71443(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_d3281287a1484821a07da08f76ea39ce: function AS_Button_d3281287a1484821a07da08f76ea39ce(eventobject, context) {
        var self = this;
        this.viewBillPayActivity();
    },
    /** onClick defined for btnCancel **/
    AS_Button_h3557a44a5e84cab935df574868ea442: function AS_Button_h3557a44a5e84cab935df574868ea442(eventobject, context) {
        var self = this;
        kony.print("Cancel clicked");
    },
    /** onClick defined for btnSave **/
    AS_Button_d3758a21bbfa42478efd7c4d0f3e8d8e: function AS_Button_d3758a21bbfa42478efd7c4d0f3e8d8e(eventobject, context) {
        var self = this;
        kony.print("Save clicked");
    }
});