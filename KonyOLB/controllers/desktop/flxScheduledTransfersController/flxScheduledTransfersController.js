define({
    showUnselectedRow: function() {
        var index = kony.application.getCurrentForm().transfermain.segmentTransfers.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().transfermain.segmentTransfers.data;
        for (i = 0; i < data.length; i++) {
            if (i == rowIndex) {
                kony.print("index:" + index);
                data[i].imgDropdown = "chevron_up.png";
                data[i].template = "flxScheduledTransfersSelected";
            } else {
                data[i].imgDropdown = "arrow_down.png";
                data[i].template = "flxScheduledTransfers";
            }
        }
        kony.application.getCurrentForm().transfermain.segmentTransfers.setData(data);
        this.AdjustScreen(0);
    }, 
      AdjustScreen: function(data) {
    var currentForm = kony.application.getCurrentForm();
    currentForm.forceLayout();    
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    currentForm.forceLayout();
    mainheight = currentForm.customheader.frame.height + currentForm.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - currentForm.flxFooter.frame.height;
        if (diff > 0) 
            currentForm.flxFooter.top = mainheight + diff + data + "dp";
        else
            currentForm.flxFooter.top = mainheight + data + "dp";
        currentForm.forceLayout();
     } else {
        currentForm.flxFooter.top = mainheight + data + "dp";
        currentForm.forceLayout();
     }

  },
});