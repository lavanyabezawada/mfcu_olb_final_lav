define(['CommonUtilities', 'ViewConstants', 'FormControllerUtility', 'OLBConstants'], function(CommonUtilities, ViewConstants, FormControllerUtility, OLBConstants) {

    var orientationHandler = new OrientationHandler();
    return {
      transactionsTemplate: "",
      transactionsSelectedTemplate: "",
      checkImagesTemplate:"",
      /**
       * Method to patch update any section of form
       * @param {Object} uiData Data for updating form sections
       */
      updateFormUI: function(uiData) {
          if (uiData) {
              if (uiData.showLoadingIndicator) {
                  if (uiData.showLoadingIndicator.status === true) {
                      FormControllerUtility.showProgressBar(this.view)
                  } else {
                      FormControllerUtility.hideProgressBar(this.view)
                  }
              }
              if (uiData.showOnServerError) {
                  this.showServerError(uiData.showOnServerError)
              }
              if (uiData.accountList) {
                  this.updateAccountList(uiData.accountList);
              }
              if (uiData.accountDetails) {
                  this.showAccountSummary();
                  this.showAccountDetailsScreen();
                  this.updateAccountTransactionsTypeSelector(uiData.accountDetails.accountType).call(this);
                  this.updateAccountDetails(uiData.accountDetails);
                  this.updateAccountInfo(uiData.accountDetails);
                  this.setSecondayActions(uiData.accountDetails); //Set Secondary actions
                  this.setRightSideActions(uiData.accountDetails);
              }
              if (uiData.transactionDetails) {
                  this.hideSearchFlex();
                  this.highlightTransactionType(uiData.transactionDetails.dataInputs.transactionType);
                  this.updateTransactions(uiData.transactionDetails);
                  this.updatePaginationBar(uiData.transactionDetails);
                  FormControllerUtility.setSortingHandlers(this.transactionsSortMap, this.onTransactionsSortClickHandler, this);
                  FormControllerUtility.updateSortFlex(this.transactionsSortMap, uiData.transactionDetails.dataInputs.sortConfig);
              }
              if(uiData.noMoreRecords){
                  this.showNoMoreRecords()
              }
              if (uiData.searchTransactions) {
                  this.showSearchedTransaction(uiData.searchTransactions);
                  this.updateSearchTransactionView()
              }
              if (uiData.estatement) {
                  this.showViewStatements(uiData.estatement.account)
                  this.initViewStatements(uiData.estatement)
              }
              if (uiData.showDownloadStatement) {
                  this.downloadUrl(uiData.showDownloadStatement);
              }
              if(uiData.transactionDownloadFile){
                this.downLoadTransactionFile(uiData.transactionDownloadFile)
              }
          }
      },
      /**
       * Method to load and return Messages and Alerts Module.
       * @returns {object} Messages and Alerts Module object.
       */
      loadAccountModule: function() {
          return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      },
      /**
       * Method to initialize frmAccountDetails
       */
      initFrmAccountDetails: function() {
          var self = this;
          var formatUtil=applicationManager.getFormatUtilManager();
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          this.transactionsSortMap = [{
            name: 'transactionDate',
            imageFlx: this.view.transactions.imgSortDate,
            clickContainer: this.view.transactions.flxSortDate
        }, {
            name: 'amount',
            imageFlx: this.view.transactions.imgSortAmount,
            clickContainer: this.view.transactions.flxSortAmount
        }];
          this.view.transactions.imgSortType.setVisibility(false);
          this.view.transactions.imgSortDescription.setVisibility(false);
          this.view.transactions.imgSortBalance.setVisibility(false);
          this.view.imgCloseDowntimeWarning.onTouchEnd = function() {
              self.showServerError(false);
          };
          var today = new Date();
          this.searchViewModel = {
              searchPerformed: false,
              visible: false,
              searchResults: [],
              keyword: '',
              transactionTypes: self.objectToListBoxArray(self.transactionTypes),
              transactionTypeSelected: OLBConstants.BOTH,
              timePeriods: self.objectToListBoxArray(self.timePeriods),
              timePeriodSelected: OLBConstants.ANY_DATE,
              fromCheckNumber: '',
              toCheckNumber: '',
              fromAmount: '',
              toAmount: '',
              fromDate: [today.getDate(), today.getMonth()+1, today.getFullYear()],
              toDate: [today.getDate(), today.getMonth()+1, today.getFullYear()]
          }
      },
      /**  Returns height of the page
       * @returns {String} height height of the page
       */
      getPageHeight: function() {
          var height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT;
          return height + ViewConstants.POSITIONAL_VALUES.DP;
      },
      /**
       * Method that gets called on preshow of frmAccountDetails
       */
      preshowFrmAccountDetails: function() {
          var scopeObj = this;
          var currBreakpoint = kony.application.getCurrentBreakpoint();
          if(currBreakpoint === 640  || orientationHandler.isMobile){
            this.transactionsTemplate = "flxSegTransactionsRowSavingsMobile";
            this.transactionsSelectedTemplate = "flxSegTransactionRowSelectedMobile";
            this.checkImagesTemplate = "flxSegCheckImagesMobile";
          }else {
            this.transactionsTemplate = "flxSegTransactionRowSavings";
            this.transactionsSelectedTemplate = "flxSegTransactionRowSelected";
            this.checkImagesTemplate = "flxSegCheckImages";
          }
          this.view.onBreakpointChange = function(){
            scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
          }

          scopeObj.view.transactions.calDateFrom.hidePreviousNextMonthDates = true;
          scopeObj.view.transactions.calDateTo.hidePreviousNextMonthDates = true; 
 
          this.view.accountActionsMobile.isVisible = false;
          //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
          this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController;
          this.view.customheader.forceCloseHamburger();
          this.view.accountTypes.isVisible = false;
          this.view.moreActions.isVisible = false;
          this.view.flxEditRule.isVisible = false;
          //resetting Masterdata
          this.view.transactions.flxNoTransactions.isVisible = false;
          //setting serach visiblity off~first visit of form
          this.searchViewModel.visible = false;
          this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
          this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
          this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
          this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
          this.view.customheader.topmenu.lblFeedback.skin = ViewConstants.SKINS.FEEDBACK_TOP_MENU_BLANK;
          if (CommonUtilities.isPrintEnabled()) {
              this.view.transactions.imgPrintIcon.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.accounts.print"));
            if(( kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) || (kony.application.getCurrentBreakpoint()==1024 || orientationHandler.isTablet)){
              this.view.transactions.flxPrint.setVisibility(false);
            }
            else{this.view.transactions.flxPrint.setVisibility(true);}
              if (CommonUtilities.isCSRMode()) {
                  this.view.transactions.imgPrintIcon.onTouchStart = CommonUtilities.disableButtonActionForCSRMode();
              } else {
                  this.view.transactions.imgPrintIcon.onTouchStart = this.onClickPrint;
              }
          } else {
              this.view.transactions.flxPrint.setVisibility(false);
          }
          //initializing search 
          this.renderSearchForm(this.searchViewModel);
          this.clearSearch();
          this.updateSearchTransactionView();
          //breadcrumb data
          this.view.breadcrumb.setBreadcrumbData([{
              text: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
          }, {
              text: kony.i18n.getLocalizedString("i18n.transfers.accountDetails")
          }]);
          this.view.breadcrumb.btnBreadcrumb1.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.topmenu.accounts"));
          this.view.breadcrumb.lblBreadcrumb2.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.transfers.accountDetails"));
          //actions for buttons and flex
          this.view.accountSummary.btnAccountInfo.onClick = function () {
              accactflag = 0;
              scopeObj.showAccountInfo();
          };
          this.view.flxAccountTypes.onTouchStart = function(){
            if(scopeObj.view.accountTypes.isVisible){
              scopeObj.view.accountTypes.origin = true;  
              if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
                scopeObj.view.imgAccountTypes.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.accountTypes.isVisible = false;
              }
            }
          }
          this.view.flxAccountTypes.onClick = function() {
            if(currBreakpoint === 640  || orientationHandler.isMobile){  
                flag = 0; 
            } 
            scopeObj.view.accountActionsMobile.setVisibility(false);  
            scopeObj.alignAccountTypesToAccountSelectionImg();
              scopeObj.showAccountTypes();
          };
          this.view.btnSecondaryActionsMobile.onClick = function(){
            scopeObj.view.accountTypes.setVisibility(false);  
            scopeObj.showMobileActions();
          }
          this.view.flxSecondaryActions.onTouchStart = function(){
            if(scopeObj.view.moreActions.isVisible){
              scopeObj.view.moreActions.origin = true;  
              if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
                scopeObj.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.moreActions.isVisible = false;
              }
            }
          }
          this.view.flxSecondaryActions.onClick = function() {
                if(event.target.id !==  "frmAccountsDetails_imgSecondaryActions" ) {
                    scopeObj.showMoreActions();
                }
          };
          this.view.imgSecondaryActions.onTouchEnd = function() {
                scopeObj.showMoreActions();
          };
          this.view.accountSummary.btnAccountSummary.onClick = function() {
              scopeObj.showAccountSummary();
          };
          this.view.accountSummary.btnBalanceDetails.onClick = function() {
              scopeObj.showBalanceDetails();
          };
          this.view.transactions.flxDownload.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.common.Download"));
          if (CommonUtilities.isCSRMode()) {
              this.view.transactions.flxDownload.onClick = CommonUtilities.disableButtonActionForCSRMode();
          } else {
             if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
              this.view.transactions.flxDownload.isVisible = false;
             }
            else{ this.view.transactions.flxDownload.isVisible = true;}
              this.view.transactions.flxDownload.onClick = scopeObj.showDownloadPopup;
          }
          this.view.downloadTransction.imgClose.onTouchEnd = function() {
              scopeObj.view.flxDownloadTransaction.isVisible = false;
          };
          this.view.CheckImage.flxImgCancel.onClick = function() {
            scopeObj.view.flxCheckImage.setVisibility(false);
          };
          this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "My Accounts");          
          this.view.accountActionsMobile.setVisibility(false);    
          this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_DOWN;
          applicationManager.getNavigationManager().applyUpdates(this);
        },
        /**
         * Method to show Download popup
         */
        showDownloadPopup: function () {
            var maxDateRange = [new Date().getDate(), new Date().getMonth() + 1, new Date().getFullYear()];
            this.view.downloadTransction.flxDowntimeWarning.setVisibility(false);
            this.view.flxDownloadTransaction.isVisible = true;
            this.view.flxDownloadTransaction.height = this.getPageHeight();
            this.view.downloadTransction.lblHeader.setFocus(true);
            //this.view.downloadTransction.calFromDate.date = [];
            this.view.downloadTransction.CopycalFromDate0if75756a9b0b46.date = maxDateRange;
        
            this.view.downloadTransction.calFromDate.validEndDate = maxDateRange;
            this.view.downloadTransction.CopycalFromDate0if75756a9b0b46.validEndDate = maxDateRange;
            this.view.downloadTransction.calFromDate.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            this.view.downloadTransction.CopycalFromDate0if75756a9b0b46.dateFormat = applicationManager.getFormatUtilManager().getDateFormat()  
            this.view.downloadTransction.lbxSelectFormat.masterData = [["csv", "CSV"], ["xls", "Excel"], ["pdf","PDF"],["qfx","QFX"],["qbo","QBO"]];
            this.view.downloadTransction.btnDownload.onClick = this.initDownLoadFile;
            this.view.customheader.headermenu.btnLogout.setFocus(true);
          },
          /**
           * Method to download transaction file
           * @param {String} fileUrl
           */
          downLoadTransactionFile : function(fileUrl){
            var data={"url":fileUrl};
            CommonUtilities.downloadFile(data);
            this.view.downloadTransction.flxDowntimeWarning.setVisibility(false);
            FormControllerUtility.hideProgressBar(this.view);
          },
          /**
           * Method to init download file functionality
           */
          initDownLoadFile: function () {
            var message;
            if (!(message = this.getFileDownloadErrorMessage())){
              FormControllerUtility.showProgressBar(this.view);
              var downloadFileParams = {};
              downloadFileParams.fromDate = CommonUtilities.sendDateToBackend(this.view.downloadTransction.calFromDate.date);
              downloadFileParams.toDate = CommonUtilities.sendDateToBackend(this.view.downloadTransction.CopycalFromDate0if75756a9b0b46.date);
              downloadFileParams.format = this.view.downloadTransction.lbxSelectFormat.selectedkeyvalue[0];
              downloadFileParams.isSearchParam = this.view.transactions.flxSearchResults.isVisible ? true : false;
              downloadFileParams.transactionType = this.transactionType === OLBConstants.ALL ? OLBConstants.BOTH : this.transactionType;
              this.loadAccountModule().presentationController.downloadTransactionFile(downloadFileParams);
              this.view.flxDownloadTransaction.isVisible = false;
            } else{
              this.view.downloadTransction.flxDowntimeWarning.setVisibility(true);
              this.view.downloadTransction.lblDowntimeWarning.text = message;
              this.view.downloadTransction.lblDowntimeWarning.setFocus(true);
              this.view.lblDowntimeWarning.setFocus(true);
            }
          },
        
          /**
           * Method to show error message if Download is not completed
           * @param {Object} obj
           * @returns {Boolean} true/false
           */
          getFileDownloadErrorMessage : function (obj) {
            var fromDateObj = this.view.downloadTransction.calFromDate;
            var toDateObj = this.view.downloadTransction.CopycalFromDate0if75756a9b0b46;
        
            if (!fromDateObj.date || fromDateObj.date === "") {
              return kony.i18n.getLocalizedString("i18n.Calendar.startDateEmpty");
            }
            if (!toDateObj.date || toDateObj.date === "") {
              return kony.i18n.getLocalizedString("i18n.Calendar.endDateEmpty");
            }
        
            var fromDate = new Date(fromDateObj.year, fromDateObj.month - 1, fromDateObj.day);
            var toDate = new Date(toDateObj.year, toDateObj.month - 1, toDateObj.day);
            
            if (fromDate > new Date()){
              return kony.i18n.getLocalizedString("i18n.Calendar.futureStartDate");
            }
            if (toDate > new Date()){
              return kony.i18n.getLocalizedString("i18n.Calendar.futureEndDate");
            }
            if (fromDate > toDate) {
              return kony.i18n.getLocalizedString("i18n.Calendar.startDateGreater");
            }
        
            var maxYearDate = fromDate;
            maxYearDate.setFullYear(maxYearDate.getFullYear() + 1);
            maxYearDate.setDate(maxYearDate.getDate() - 1);
        
            if(toDate > maxYearDate){
              return kony.i18n.getLocalizedString("i18n.Calendar.maximumRange");
            }
            return false;
          },        
      /**
       * Method that gets called on postshow of frmAccountDetails
       */
      postShowFrmAccountDetails: function() {
        var scopeObj = this;
        this.view.customheader.forceCloseHamburger();
        this.view.AllForms.flxCross.onClick = function() {
            scopeObj.view.AllForms.setVisibility(false);
        };
        this.view.accountSummary.flxInfo.onClick = function() {
          if (scopeObj.view.AllForms.isVisible === false) {
            scopeObj.view.AllForms.setVisibility(true);
            if(scopeObj.view.flxDowntimeWarning.isVisible === true)
              scopeObj.view.AllForms.top = "360dp";
            else   
              scopeObj.view.AllForms.top = "280dp"; 
              if (kony.application.getCurrentBreakpoint() > 1024) {
                //To Fix ARB-10347
                scopeObj.view.AllForms.left = scopeObj.view.flxMain.frame.x +  scopeObj.view.accountSummary.frame.x + scopeObj.view.accountSummary.flxMainDesktop.frame.x +  scopeObj.view.accountSummary.flxRight.frame.x +  scopeObj.view.accountSummary.flxInfo.frame.x - 125 + "dp";
              }else{
                scopeObj.view.AllForms.right="10dp";
                scopeObj.view.AllForms.left = "";
              }
              if(orientationHandler.isMobile || orientationHandler.isTablet){
                scopeObj.view.AllForms.right="10dp";
                scopeObj.view.AllForms.left = ""; 
              }
          } else {
            scopeObj.view.AllForms.setVisibility(false);
          }
        };
        this.AdjustScreen();
      },
      /**
       * Method to show server error on frmAccountDetails
       * @param {Boolean} status true/false
       */
      showServerError: function(status) {
          this.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
          if (status === true) {
              this.view.flxDowntimeWarning.setVisibility(true);
              this.view.lblDowntimeWarning.setFocus(true);
          } else {
              this.view.flxDowntimeWarning.setVisibility(false);
          }
          this.AdjustScreen();
      },
      /**
       * Method to handle transactions sorting
       * @param {Object} event event like click etc
       * @param {JSON} data data of sortType value
       */
      onTransactionsSortClickHandler: function(event, data) {
          data['transactionType'] = this.transactionType;
          this.loadAccountModule().presentationController.Transactions.showTransactionsByType(data);
      },
      /**
       * Method to update the layout of the page
       */
      AdjustScreen: function() {
          this.view.forceLayout();
          var mainheight = 0;
          var screenheight = kony.os.deviceInfo().screenHeight;
          mainheight = this.view.customheader.frame.height + this.view.flxMainWrapper.frame.height;
          var diff = screenheight - mainheight;
          if (mainheight < screenheight) {
              diff = diff - this.view.flxFooter.frame.height;
              if (diff > 0) {
                  this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
              } else {
                  this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
              }
          } else {
              this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
          }
          this.view.forceLayout();
      },
      /**
       * Method to clear search component for frmAccountDetails
       */
      clearSearch: function() {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var today = new Date();
          var formatUtil= applicationManager.getFormatUtilManager();
          this.view.transactions.txtKeyword.text = ""
          this.view.transactions.lstbxTransactionType.selectedKey = OLBConstants.BOTH;
          this.view.transactions.lstbxTimePeriod.selectedKey = OLBConstants.ANY_DATE;
          this.onTimePeriodChange();
          this.view.transactions.txtCheckNumberTo.text = ""
          this.view.transactions.txtCheckNumberFrom.text = ""
          this.view.transactions.txtAmountRangeTo.text = ""
          this.view.transactions.txtAmountRangeFrom.text = ""
          this.view.transactions.calDateFrom.dateComponents = [today.getDate(), today.getMonth()+1, today.getFullYear()];
          this.view.transactions.calDateTo.dateComponents = [today.getDate(), today.getMonth()+1, today.getFullYear()];
      },
      /**
       * Method to get list box items for transaction type
       * @param {String} obj  object for transactiontype or timeperiod
       * @returns {Collection} List of transaction/time period
       */
      objectToListBoxArray: function(obj) {
          var list = [];
          for (var key in obj) {
              if (obj.hasOwnProperty(key)) {
                  list.push([key, kony.i18n.getLocalizedString(obj[key])]);
              }
          }
          return list;
      },
      transactionTypes: {
          Both: 'i18n.accounts.allTransactions',
          Deposit: 'i18n.accounts.deposits',
          Withdrawal: 'i18n.accounts.withdrawls',
          Checks: 'i18n.accounts.checks',
          Transfers: "i18n.hamburger.transfers",
          BillPay: "i18n.billPay.BillPay",
          P2PDebits: 'i18n.accounts.p2pDebits',
          P2PCredits: 'i18n.accounts.p2pCredits'
      },
      
      timePeriods: {
          ANY_DATE: 'i18n.accounts.anyDate',
          LAST_SEVEN_DAYS: 'i18n.accounts.lastSevenDays',
          LAST_THIRTY_DAYS: 'i18n.accounts.lastThirtyDays',
          LAST_SIXTY_DAYS: 'i18n.accounts.lastSixtyDays',
          CUSTOM_DATE_RANGE: 'i18n.accounts.customDateRange'
      },
      /**
       * Method to update Search ViewModel
       * @returns {JSON} Search View Model
       */
      updateSearchViewModel: function() {
          this.searchViewModel.keyword = this.view.transactions.txtKeyword.text.trim();
          this.searchViewModel.transactionTypeSelected = this.view.transactions.lstbxTransactionType.selectedKey;
          this.searchViewModel.timePeriodSelected = this.view.transactions.lstbxTimePeriod.selectedKey;
          this.searchViewModel.fromCheckNumber = this.view.transactions.txtCheckNumberFrom.text.trim();
          this.searchViewModel.toCheckNumber = this.view.transactions.txtCheckNumberTo.text.trim();
          this.searchViewModel.fromDate = this.getDateComponent(this.view.transactions.calDateFrom.date);
          this.searchViewModel.fromAmount = (this.view.transactions.txtAmountRangeFrom.text.trim() !== "") ?applicationManager.getFormatUtilManager().deFormatAmount(this.view.transactions.txtAmountRangeFrom.text.trim()) : (this.view.transactions.txtAmountRangeFrom.text.trim());
          this.searchViewModel.toAmount = (this.view.transactions.txtAmountRangeTo.text.trim() !== "") ?applicationManager.getFormatUtilManager().deFormatAmount(this.view.transactions.txtAmountRangeTo.text.trim()) : (this.view.transactions.txtAmountRangeTo.text.trim());
          this.searchViewModel.toDate = this.getDateComponent(this.view.transactions.calDateTo.date);
          return this.searchViewModel;
      },
      /**
       * Method to validate the user for search
       * @param {JSON} formData Search View Model
       * @returns {Boolean} tru/false
       */
      userCanSearch: function(formData) {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;

          function checkIfEmpty(value) {
              if (value === null || value === "") {
                  return true;
              }
              return false;
          }
          if (!checkIfEmpty(formData.keyword)) {
              return true;
          } else if (formData.transactionTypeSelected !== OLBConstants.BOTH) {
              return true;
          } else if (formData.timePeriodSelected !== OLBConstants.ANY_DATE) {
              return true;
          } else if (!checkIfEmpty(formData.fromCheckNumber) && !checkIfEmpty(formData.toCheckNumber)) {
              return true;
          } else if (!checkIfEmpty(formData.fromAmount) && !checkIfEmpty(formData.toAmount)) {
              return true;
          } else {
              return false;
          }
      },
      /**
       * Method to update the Search Button State
       */
      checkSearchButtonState: function() {
          var formData = this.updateSearchViewModel({});
          if (this.userCanSearch(formData)) {
              this.view.transactions.enableSearchButton();
          } else {
              this.view.transactions.disableSearchButton();
          }
      },
      /**
       * Method to handle Time Period Change
       */
      onTimePeriodChange: function() {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          if (this.view.transactions.lstbxTimePeriod.selectedKey === OLBConstants.CUSTOM_DATE_RANGE) {
              this.view.transactions.showByDateWidgets();
          } else {
              this.view.transactions.hideByDateWidgets();
          }
          this.checkSearchButtonState();
      },
      /**
       * Method to get the date Range
       * @param {JSON} searchviewmodel JSON of search view
       * @returns {JSON} JSON with start and end date
       */
      getDateRangeForTimePeriod: function(searchviewmodel) {
        var startDate = null;
        var endDate = null;
        var formatUtil=applicationManager.getFormatUtilManager();
        var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
        if (searchviewmodel.timePeriodSelected === OLBConstants.ANY_DATE) {
            startDate = "";
            endDate = "";
        } else if (searchviewmodel.timePeriodSelected === 'CUSTOM_DATE_RANGE') {
            function padDigits(n) {
                    n = n + '';
                    return n.length >= 2 ? n : new Array(2 - n.length + 1).join('0') + n;
            }
            var dd_mm_yyyy_startDate = padDigits(searchviewmodel.fromDate[0])  + "/" + padDigits(searchviewmodel.fromDate[1]) + "/" + searchviewmodel.fromDate[2];
            var dd_mm_yyyy_endDate = padDigits(searchviewmodel.toDate[0])  + "/" + padDigits(searchviewmodel.toDate[1]) + "/" + searchviewmodel.toDate[2];
            startDate = formatUtil.getFormatedDateString(formatUtil.getDateObjectFromCalendarString(dd_mm_yyyy_startDate, "DD/MM/YYYY"), formatUtil.getBackendDateFormat());
            endDate = formatUtil.getFormatedDateString(formatUtil.getDateObjectFromCalendarString(dd_mm_yyyy_endDate, "DD/MM/YYYY"), formatUtil.getBackendDateFormat());
        } else {
            var dateConfig = {
                LAST_SEVEN_DAYS: 7,
                LAST_THIRTY_DAYS: 30,
                LAST_SIXTY_DAYS: 60 
            };
            var today = new Date();
            endDate = formatUtil.getFormatedDateString(today, formatUtil.getBackendDateFormat());
            today.setDate(today.getDate() - dateConfig[searchviewmodel.timePeriodSelected]);
            startDate = formatUtil.getFormatedDateString(today, formatUtil.getBackendDateFormat());
        }
        return {
            startDate: startDate,
            endDate: endDate
        };
    },
    /**
     * Method to make search call
     */
    startSearch:function(){
        FormControllerUtility.showProgressBar(this.view);
        this.searchViewModel.searchPerformed = true;
        //this.updateSearchViewModel();
        this.searchViewModel.dateRange= this.getDateRangeForTimePeriod(this.searchViewModel);
        this.loadAccountModule().presentationController.fetchTransactionsBySearch(this.searchViewModel);
    },
      /**
       * Method to bind search actions
       */
      bindSearchFormActions: function() {
          this.view.transactions.btnSearch.onClick = function() {
              this.updateSearchViewModel();
              this.startSearch();
          }.bind(this);
          this.view.transactions.btnCancel.onClick = function() {
              this.clearSearch();
              this.searchViewModel.visible = false;
              this.view.transactions.setSearchVisible(false);
              this.presenter.showAccountDetails()
              this.AdjustScreen();
          }.bind(this);
          this.view.transactions.txtKeyword.onKeyUp = this.checkSearchButtonState.bind(this);
          this.view.transactions.lstbxTransactionType.onSelection = this.checkSearchButtonState.bind(this);
          this.view.transactions.txtCheckNumberFrom.onKeyUp = this.checkSearchButtonState.bind(this);
          this.view.transactions.lstbxTimePeriod.onSelection = this.onTimePeriodChange.bind(this);
          this.view.transactions.txtCheckNumberTo.onKeyUp = this.checkSearchButtonState.bind(this);
          FormControllerUtility.wrapAmountField(this.view.transactions.txtAmountRangeFrom).onKeyUp(this.checkSearchButtonState.bind(this));
          FormControllerUtility.wrapAmountField(this.view.transactions.txtAmountRangeTo).onKeyUp(this.checkSearchButtonState.bind(this));
          var scopeObj = this
          this.view.transactions.calDateFrom.onSelection = function() {
              var fromdate = scopeObj.view.transactions.calDateFrom.dateComponents;
              var today = new Date();
              scopeObj.view.transactions.calDateTo.enableRangeOfDates([fromdate[0], fromdate[1], fromdate[2]], [today.getDate(), today.getMonth() + 1, today.getFullYear()], "skn", true);
          };
      },
      /**
       * Method to update the value in search form
       * @param {JSON} searchViewModel Search View Model
       */
      renderSearchForm: function(searchViewModel) {
          this.view.transactions.setSearchVisible(true);
          this.view.transactions.setSearchResultsVisible(false);
          this.view.transactions.txtKeyword.text = searchViewModel.keyword;
          this.view.transactions.lstbxTransactionType.masterData = searchViewModel.transactionTypes;
          this.view.transactions.lstbxTransactionType.selectedKey = searchViewModel.transactionTypeSelected;
          this.view.transactions.lstbxTimePeriod.masterData = searchViewModel.timePeriods;
          this.view.transactions.lstbxTimePeriod.selectedKey = searchViewModel.timePeriodSelected;
          this.view.transactions.txtCheckNumberFrom.text = searchViewModel.fromCheckNumber;
          this.view.transactions.txtCheckNumberTo.text = searchViewModel.toCheckNumber;
          this.view.transactions.txtAmountRangeFrom.text = searchViewModel.fromAmount;
          this.view.transactions.txtAmountRangeTo.text = searchViewModel.toAmount;
          this.view.transactions.calDateFrom.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
          this.view.transactions.calDateTo.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
          this.view.transactions.calDateFrom.dateComponents = searchViewModel.fromDate;
          this.view.transactions.calDateTo.dateComponents = searchViewModel.toDate;
          this.onTimePeriodChange();
          this.checkSearchButtonState();
      },
      /**
         * Method to get date component
         * @param {string} - Date string
         * @returns {Object} - dateComponent Object
        */
        getDateComponent: function(dateString) {
            var dateObj  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase());
            return [dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear()];
        },
      /**
       * Method to hide Search Flex
       */
      hideSearchFlex:function(){
        this.view.transactions.setSearchVisible(false);
        this.view.transactions.setSearchResultsVisible(false);
      },
      /**
       * Method to update search View model
       */
       updateSearchTransactionView: function() {
          var self = this;
          this.view.transactions.flxSearch.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Search");
          this.view.transactions.flxSearch.onClick = function() {
              if (self.searchViewModel.visible === true) {
                  self.searchViewModel.visible = false;
                  self.view.transactions.setSearchVisible(false);
              } else {
                  self.searchViewModel.visible = true;
                  self.view.transactions.setSearchVisible(true);
              }
              self.AdjustScreen();
          };
          
          if (this.searchViewModel.visible && !this.searchViewModel.searchPerformed) {
            this.renderSearchForm(this.searchViewModel);
          }else if (this.searchViewModel.visible && this.searchViewModel.searchPerformed) {
            this.showSearchResults(this.searchViewModel);
          }else{
            this.hideSearchFlex();
          }
          this.view.transactions.flxSearch.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.billPay.Search"));
          this.bindSearchFormActions();
          this.AdjustScreen();
      },
      
      /**
       * Method to modify search
       */
      modifySearch:function(){
        this.searchViewModel.visible = true;
        this.searchViewModel.searchPerformed = false;
        this.renderSearchForm(this.searchViewModel);
        this.AdjustScreen();
      },
      /**
       * Method to show Search Results
       * @param {JSON} searchViewModel searchViewModel - search view model
       */
      showSearchResults: function (searchViewModel) {
        var self=this;
        this.view.transactions.btnModifySearch.onClick = function () {
            self.modifySearch();
        };
        this.view.transactions.btnClearSearch.onClick = function () {
            self.presenter.showAccountDetails()
        };
        this.view.transactions.setSearchVisible(false);
        this.view.transactions.setSearchResultsVisible(true);
        this.configureActionsForTags(searchViewModel);
      },
      /**
       * Method to handle footer alignment
       */
      footeralignment: function() {
          if (kony.os.deviceInfo().screenHeight >= "900") {
              var mainheight = 0;
              var screenheight = kony.os.deviceInfo().screenHeight;
              mainheight = this.view.customheader.frame.height;
              mainheight = mainheight + this.view.flxMain.frame.height;
              var diff = screenheight - mainheight;
              if (diff > 0) {
                  this.view.flxFooter.top = diff + ViewConstants.POSITIONAL_VALUES.DP;
              }
          }
      },
      /**
       * Method to show or hide account info
       */
      showAccountInfo: function() {
        
              this.view.accountSummary.btnAccountInfo.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
              this.view.accountSummary.btnAccountInfo.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
              this.view.accountSummary.btnAccountSummary.skin = ViewConstants.SKINS.TAB_INACTIVE;
              this.view.accountSummary.btnAccountSummary.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
              this.view.accountSummary.btnBalanceDetails.skin = ViewConstants.SKINS.TAB_INACTIVE;
              this.view.accountSummary.btnBalanceDetails.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
              this.view.accountSummary.flxSummaryDesktop.isVisible = false;
              this.view.accountSummary.flxBalanceDetailDesktop.isVisible = false;
              this.view.accountSummary.flxAccountInfoDesktop.isVisible = true;
              this.view.accountSummary.lblAccountNumber.setFocus(true);
              this.view.accountSummary.height = "";
              this.view.forceLayout();
      },
      /**
       * Method to show Account Types popup
       */
      showAccountTypes: function() {
        if (this.view.accountTypes.origin) {
          this.view.accountTypes.origin = false;
          return;
        }
        if(this.view.accountTypes.isVisible==false){
          this.view.imgAccountTypes.src = ViewConstants.IMAGES.ARRAOW_UP;
          this.view.accountTypes.imgToolTip.setFocus(true);
          if (this.view.flxDowntimeWarning.isVisible === true) {
            this.view.accountTypes.top = 60 + this.view.flxDowntimeWarning.frame.height + 10 + ViewConstants.POSITIONAL_VALUES.DP;
          }else{
            this.view.accountTypes.top = "50dp";
          }
          if( kony.application.getCurrentBreakpoint() > 640 && !(orientationHandler.isMobile) )  {
            this.view.accountTypes.left = (this.view.flxMain.frame.x + this.view.accountSummary.frame.x + 0) + "dp";
          }          
          this.view.accountTypes.isVisible = true;
        }else{
          this.view.accountTypes.isVisible = false;
          this.view.imgAccountTypes.src = ViewConstants.IMAGES.ARRAOW_DOWN;
        }
        
        this.view.forceLayout();
      },
      /**
       * Method to handle show More Actions
       */
      showMoreActions: function() {
          var scopeObj = this;
          setTimeout((function(){
            if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                scopeObj.showTabletActions()
                return;
            }

            if (showMoreActionsFlag === 1 && flag === 0) {
                this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                this.view.moreActions.isVisible = false;
                showMoreActionsFlag = 0;
            } else {
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                    this.view.moreActions.isVisible = true;
                    this.view.moreActions.imgToolTip.setFocus(true);
                } else {
                    this.view.moreActions.width = this.view.flxSecondaryActions.frame.width + "dp";
                    this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_UP;
                    var top = this.view.flxSecondaryActions.frame.y + 120;;
                    if (this.view.flxDowntimeWarning.isVisible) {
                        top = top + 80;
                    }
                    this.view.moreActions.top = top + "dp";
                    var right = (this.view.flxMainWrapper.frame.width - this.view.flxMain.frame.width) / 2;
                    right += 0.06 * this.view.flxMain.frame.width;
                    if (kony.application.getCurrentBreakpoint() >= 1380) {
                        right -= 24;
                    } else {
                        right = right;
                    }
                    this.view.moreActions.right = right + "dp";
                    this.view.moreActions.imgToolTip.setFocus(true);
                    this.view.moreActions.setVisibility(true);
                    if (kony.application.getCurrentBreakpoint() === 1024 || orientationHandler.isTablet) {
                        this.view.moreActions.right = "2%";
                        this.view.moreActions.top = "410dp";
                    }
                }
                showMoreActionsFlag = 1;
                this.view.forceLayout();
            }
           }).bind(this),0);
      },
      /**
       * Method to show balance details
       */
      showBalanceDetails: function() {
          this.view.accountSummary.flxSummaryDesktop.isVisible = false;
          this.view.accountSummary.lblTotalCreditsTitle.setFocus(true);
          this.view.accountSummary.flxAccountInfoDesktop.isVisible = false;
          this.view.accountSummary.flxBalanceDetailDesktop.isVisible = true;
          this.view.accountSummary.btnAccountInfo.skin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED;
          this.view.accountSummary.btnAccountInfo.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
          this.view.accountSummary.btnAccountSummary.skin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED;
          this.view.accountSummary.btnAccountSummary.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
          this.view.accountSummary.btnBalanceDetails.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
          this.view.accountSummary.btnBalanceDetails.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
          this.view.accountSummary.height = "";
      },
      /**
       * Method to create Accounts List segment view model
       * @param {JSON} account Account for which you want to create view Model
       * @returns {JSON} View model
       */
      createAccountListSegmentsModel: function(account) {
          return {
              "lblUsers": {
                  "text": CommonUtilities.getAccountName(account),
                  "toolTip": CommonUtilities.changedataCase(CommonUtilities.getAccountName(account))
              },
              "lblSeparator": "Separator",
              "flxAccountTypes": {
                  "onClick": this.loadAccountModule().presentationController.showAccountDetails.bind(this.loadAccountModule().presentationController, account)
              },
              "flxAccountTypesMobile": {
                  "onClick": this.loadAccountModule().presentationController.showAccountDetails.bind(this.loadAccountModule().presentationController, account)
              },            
              template: kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile ? "flxAccountTypesMobile" : "flxAccountTypes"
          };
      },
      /**
       * Method to update accounts list segment
       * @param {Collection} accountList List of accounts
       */
      updateAccountList: function(accountList) {
          var accounts = accountList.map(this.createAccountListSegmentsModel);
  		  var dataMap = {
    			"flxAccountTypes": "flxAccountTypes",
                "flxAccountTypesMobile": "flxAccountTypesMobile",
    			"lblSeparator": "lblSeparator",
    			"lblUsers": "lblUsers"
  		  };
		  this.view.accountTypes.segAccountTypes.widgetDataMap = dataMap;
          this.view.accountTypes.segAccountTypes.setData(accounts);
          this.view.accountTypes.forceLayout();
          this.AdjustScreen();
      },
      /**
       * Method to show account summary
       */
      showAccountSummary: function() {
          this.view.accountSummary.flxSummaryDesktop.isVisible = true;
          this.view.accountSummary.lblExtraFieldTitle.setFocus(true);
          this.view.accountSummary.flxBalanceDetailDesktop.isVisible = false;
          this.view.accountSummary.flxAccountInfoDesktop.isVisible = false;
          this.view.accountSummary.btnAccountSummary.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
          this.view.accountSummary.btnAccountSummary.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
          this.view.accountSummary.btnBalanceDetails.skin = ViewConstants.SKINS.TAB_INACTIVE;
          this.view.accountSummary.btnBalanceDetails.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
          this.view.accountSummary.btnAccountInfo.skin = ViewConstants.SKINS.TAB_INACTIVE;
          this.view.accountSummary.btnAccountInfo.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
          this.view.accountSummary.height = "";
      },
      /**
       * Method to show account details screen and hide estatement flexes
       */
      showAccountDetailsScreen: function() {
          this.view.flxHeader.isVisible = true;
          this.view.flxMainWrapper.isVisible = true;
          this.view.flxMain.isVisible = true;
          this.view.flxEditRule.isVisible = false;
          this.view.flxCheckImage.isVisible = false;
          this.view.flxDownloadTransaction.isVisible = false;
          var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
          var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
          this.view.breadcrumb.setBreadcrumbData([{
              text: text1
          }, {
              text: text2
          }]);
          this.view.flxLogout.isVisible = true;
          this.view.CustomPopup.lblHeading.setFocus(true);
          this.view.flxAccountTypesAndInfo.isVisible = true;
          this.view.flxAccountSummaryAndActions.isVisible = true;
          this.view.flxViewStatements.isVisible = false;
          this.view.flxTransactions.isVisible = true;
          this.view.transactions.flxSortDate.left = "8.9%";
          this.view.flxFooter.isVisible = true;
          this.view.accountTypes.isVisible = false;
          this.view.moreActions.isVisible = false;
          this.view.moreActionsDup.isVisible = false;
          this.view.breadcrumb.isVisible = false;
          this.AdjustScreen();
      },
      /**
       * Method to show/ hide flexes and sorting header base d on account type
       * @param {String} accountType Account type of the account
       */
      updateAccountTransactionsTypeSelector: function(accountType) {
        switch(accountType){
             case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING):
              return function() {
                  this.view.transactions.flxTabsChecking.isVisible = true;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = false;
                  this.view.transactions.flxSortType.isVisible = true;
                  this.view.transactions.lblSortType.isVisible = true;
                  this.view.accountSummary.flxExtraField.isVisible = false;
              }
             case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CURRENT):
              return function() {
                  this.view.transactions.flxTabsChecking.isVisible = true;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = false;
                  this.view.transactions.flxSortType.isVisible = false;
                  this.view.accountSummary.flxExtraField.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = false;
                  this.view.transactions.imgSortType.isVisible = false;
              }
             case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD):
              return function() {
                  this.view.transactions.flxTabsChecking.isVisible = false;
                  this.view.transactions.flxTabsCredit.isVisible = true;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = false;
                  this.view.transactions.flxSortType.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = false;
                  this.view.transactions.imgSortType.isVisible = false;
              }
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN):
               return function() {
                  this.view.transactions.flxTabsChecking.isVisible = false;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = true;
                  this.view.transactions.flxSortType.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = false;
                  this.view.transactions.imgSortType.isVisible = false;
              }
             case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT):
              return function() {
                  this.view.transactions.flxTabsChecking.isVisible = false;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = true;
                  this.view.transactions.flxTabsLoan.isVisible = false;
                  this.view.transactions.flxSortType.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = false;
                  this.view.transactions.imgSortType.isVisible = false;
              }
             case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING):
              return function() {
                  this.view.transactions.flxTabsChecking.isVisible = true;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = false;
                  this.view.transactions.flxSortType.isVisible = true;
                  this.view.accountSummary.flxExtraField.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = true;
              }
              default :
               return function() {
                  this.view.transactions.flxTabsChecking.isVisible = false;
                  this.view.transactions.flxTabsCredit.isVisible = false;
                  this.view.transactions.flxTabsDeposit.isVisible = false;
                  this.view.transactions.flxTabsLoan.isVisible = true;
                  this.view.transactions.flxSortType.isVisible = false;
                  this.view.accountSummary.flxExtraField.isVisible = false;
                  this.view.transactions.lblSortType.isVisible = false;
                  this.view.transactions.imgSortType.isVisible = false;
              }
            };
          
      },
      /**
       * Method to gets sking for selected account type of accounts
       * @param {String} type Account type of account
       * @returns {String} Skin for selected account type
       */
      getSkinForAccount: function(type) {
        switch(type){
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING) :
                 return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_SAVINGS
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING) :
                 return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_CHECKING
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD): 
                return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_CREDIT_CARD
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT): 
                return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_DEPOSIT
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE): 
                return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_MORTGAGE
              case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN): 
                return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_LOAN
              default : 
                return ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_UNCONFIGURED
          }
          
      },
      /**
       * Method to update account details- account details section
       * @param {JSON} account Account whose details needs to be updated
       */
      updateAccountDetails: function(account) {
          var scopeObj=this;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          this.view.accountSummary.flxIdentifier.skin = this.getSkinForAccount(account.accountType);
          this.view.lblAccountTypes.text = CommonUtilities.getAccountName(account) +" "+ CommonUtilities.accountNumberMask(account.accountID);
          this.view.lblAccountTypes.toolTip = CommonUtilities.getAccountName(account) +" "+ CommonUtilities.accountNumberMask(account.accountID);
          this.view.AllForms.lblInfo.text = kony.i18n.getLocalizedString("i18n.WireTransfers.Information");
          this.view.lblAccountTypes.parent.forceLayout();
        this.view.imgAccountTypes.src = ViewConstants.IMAGES.ARRAOW_DOWN;
          this.view.downloadTransction.btnCancel.onClick = function(accountObj) {
            scopeObj.view.flxDownloadTransaction.isVisible = false;
            scopeObj.loadAccountModule().presentationController.showAccountDetails(accountObj);
          }.bind(this,account);
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)) {
              this.view.accountSummary.flxExtraField.isVisible = false;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = true;
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.formatCurrencyWithCommas(account.pendingDeposit,false,account.currencyCode);
              this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
              this.view.accountSummary.lblPendingWithdrawalsValue.text = CommonUtilities.formatCurrencyWithCommas(account.pendingWithdrawal,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendRate,false,account.currencyCode) + '%';
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendPaidYTD,false,account.currencyCode);
              this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
              this.view.accountSummary.lblLastDividentPaidValue.text = CommonUtilities.formatCurrencyWithCommas(account.lastDividendPaidAmount,false,account.currencyCode);
              this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
              this.view.accountSummary.lblPaidOnValue.text = CommonUtilities.getFrontendDateString(account.lastPaymentDate);
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.totalCreditMonths,false,account.currencyCode);
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.totalDebitsMonth,false,account.currencyCode);
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
              this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");
              this.view.accountSummary.flxDummyOne.isVisible = false;
              this.view.accountSummary.flxDummyTwo.isVisible = false;
          }
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)) {
              this.view.accountSummary.flxExtraField.isVisible = false;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = true;
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndBillDetail");
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue,false,account.currencyCode);
              this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit");
              this.view.accountSummary.lblPendingWithdrawalsValue.text = CommonUtilities.formatCurrencyWithCommas(account.creditLimit,false,account.currencyCode);
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableCredit,false,account.currencyCode);
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue,false,account.currencyCode);
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.minimumDueAmount");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.minimumDue,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paymentDueDate");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.getFrontendDateString(account.dueDate);
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastStatementBalance");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.formatCurrencyWithCommas(account.lastStatementBalance,false,account.currencyCode);
              this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
              this.view.accountSummary.lblLastDividentPaidValue.text = CommonUtilities.formatCurrencyWithCommas(account.lastPaymentAmount,false,account.currencyCode);
              this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
              this.view.accountSummary.lblPaidOnValue.text = CommonUtilities.getFrontendDateString(account.lastPaymentDate);
              this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.rewardBalance");
              this.view.accountSummary.lblDummyOneValue.text = account.availableCredit;
              this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
              this.view.accountSummary.lblDummyTwoValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestRate,false,account.currencyCode) + '%';
              this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.CreditCard");
              this.view.accountSummary.flxDummyOne.isVisible = true;
              this.view.accountSummary.flxDummyTwo.isVisible = true;
          }
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN) || account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)) {
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestDetail");
              this.view.accountSummary.flxExtraField.isVisible = true;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = false;
              this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalBalance");
              this.view.accountSummary.lblExtraFieldValue.text = CommonUtilities.formatCurrencyWithCommas(account.principalBalance,false,account.currencyCode);
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalAmount");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.principalValue,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue,false,account.currencyCode);
              this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.nextPaymentDueDate");
              this.view.accountSummary.lblPendingWithdrawalsValue.text = CommonUtilities.getFrontendDateString(account.dueDate);
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestRate,false,account.currencyCode) + '%';
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidYTD");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestPaidYTD,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidLastYear");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestPaidLastYear,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.formatCurrencyWithCommas(account.lastPaymentAmount,false,account.currencyCode);
              this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
              this.view.accountSummary.lblLastDividentPaidValue.text = CommonUtilities.getFrontendDateString(account.lastPaymentDate);
              this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalAmount");
              this.view.accountSummary.lblPaidOnValue.text = CommonUtilities.formatCurrencyWithCommas(account.originalAmount,false,account.currencyCode);
              this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalDate");
              this.view.accountSummary.lblDummyOneValue.text = CommonUtilities.getFrontendDateString(account.openingDate);
              this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.loan.payOffAmount");
              this.view.accountSummary.lblDummyTwoValue.text = CommonUtilities.formatCurrencyWithCommas(account.payoffAmount,false,account.currencyCode);
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.outstandingBalance");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.outstandingBalance,false,account.currencyCode);
              this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Loan");
              this.view.accountSummary.flxDummyOne.isVisible = true;
              this.view.accountSummary.flxDummyTwo.isVisible = true;
          }
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)) {
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestAndMaturityDetail");
              this.view.accountSummary.flxExtraField.isVisible = true;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = false;
              this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
              this.view.accountSummary.lblExtraFieldValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestEarned");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestEarned,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.getFrontendDateString(account.maturityDate);
              this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.term");
              this.view.accountSummary.lblPendingWithdrawalsValue.text = account.paymentTerm;
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendRate,false,account.currencyCode) + '%';
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendPaidYTD,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendLastPaidAmount,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.getFrontendDateString(account.dividendLastPaidDate);
              this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
              this.view.accountSummary.lblLastDividentPaidValue.text = CommonUtilities.getFrontendDateString(account.maturityDate);
              this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityAmount");
              this.view.accountSummary.lblPaidOnValue.text = CommonUtilities.formatCurrencyWithCommas(account.maturityAmount,false,account.currencyCode);
              this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityOption");
              this.view.accountSummary.lblDummyOneValue.text = account.maturityOption;
              this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Deposit");
              this.view.accountSummary.flxDummyOne.isVisible = true;
              this.view.accountSummary.flxDummyTwo.isVisible = false;
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
          }
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING) || account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CURRENT)) {
              this.view.accountSummary.flxExtraField.isVisible = false;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = true;
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.formatCurrencyWithCommas(account.pendingDeposit,false,account.currencyCode);
              this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
              this.view.accountSummary.lblPendingWithdrawalsValue.text = CommonUtilities.formatCurrencyWithCommas(account.pendingWithdrawal,false,account.currencyCode);
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.totalCreditMonths,false,account.currencyCode);
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.totalDebitsMonth,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.bondInterestLastYear");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.formatCurrencyWithCommas(account.bondInterestLastYear,false,account.currencyCode) + '%';
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidLastYear");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendPaidYTD,false,account.currencyCode);
              this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
              this.view.accountSummary.lblLastDividentPaidValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendRate,false,account.currencyCode) + '%';
              this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
              this.view.accountSummary.lblPaidOnValue.text = CommonUtilities.formatCurrencyWithCommas(account.dividendPaidYTD,false,account.currencyCode);
              this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
              this.view.accountSummary.lblDummyOneValue.text = CommonUtilities.formatCurrencyWithCommas(account.lastDividendPaidAmount,false,account.currencyCode);
              this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
              this.view.accountSummary.lblDummyTwoValue.text = CommonUtilities.getFrontendDateString(account.lastPaymentDate);
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
              this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");
              this.view.accountSummary.flxDummyTwo.isVisible = true;
              this.view.accountSummary.flxDummyTwo.isVisible = true;
          }
          if (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LINE_OF_CREDIT)) {
              this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
              this.view.accountSummary.flxExtraField.isVisible = false;
              this.view.accountSummary.flxPendingWithdrawals.isVisible = false;
              this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
              this.view.accountSummary.lblCurrentBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
              this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
              this.view.accountSummary.lblPendingDepositsValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentBalance,false,account.currencyCode);
              this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
              this.view.accountSummary.lblTotalCreditsValue.text = CommonUtilities.formatCurrencyWithCommas(account.interestRate,false,account.currencyCode) + '%';
              this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.regularPaymentAmount");
              this.view.accountSummary.lblTotalDebtsValue.text = CommonUtilities.formatCurrencyWithCommas(account.regularPaymentAmount,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
              this.view.accountSummary.lblDividentRateValue.text = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue,false,account.currencyCode);
              this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dueDate");
              this.view.accountSummary.lblDividentRateYTDValue.text = CommonUtilities.getFrontendDateString(account.dueDate);
              this.view.accountSummary.flxBalanceDetailsRight.isVisible = false;
              this.view.accountSummary.lblAsOf.text = kony.i18n.getLocalizedString("i18n.accounts.AsOf") +" "+ CommonUtilities.getFrontendDateString(new Date().toISOString());
              this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
              this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance,false,account.currencyCode);
              this.view.accountSummary.flxDummyOne.isVisible = false;
              this.view.accountSummary.flxDummyTwo.isVisible = false;
              this.view.accountSummary.flxPaidOn.isVisible = false;
          }
          this.view.accountSummary.forceLayout();
      },
      /**
       * Method to align account types to AccountSelection images
       */
      alignAccountTypesToAccountSelectionImg: function() {
          var getNumber = function(str) {
              if (str.length > 2) {
                  return Number(str.substring(0, str.length - 2));
              }
              return 0;
          };
          var topImgCenter = this.view.imgAccountTypes.frame.x + (this.view.imgAccountTypes.frame.width / 2);
          var bottomImgLeftPos = (topImgCenter - (getNumber(this.view.accountTypes.imgToolTip.width) / 2));
          this.view.accountTypes.imgToolTip.left = bottomImgLeftPos + ViewConstants.POSITIONAL_VALUES.DP;
      },
      /**
       * Method to update account information
       * @param {JSON} account Account whose information needs to be updated
       */
      updateAccountInfo: function(account) {
          var controller = this;
          var LabelToDisplayMap = {
              accountNumber: kony.i18n.getLocalizedString("i18n.common.accountNumber"),
              routingNumber: kony.i18n.getLocalizedString("i18n.accounts.routingNumber"),
              swiftCode: kony.i18n.getLocalizedString("i18n.accounts.swiftCode"),
              accountName: kony.i18n.getLocalizedString("i18n.accounts.primaryholder"),
              primaryAccountHolder: kony.i18n.getLocalizedString("i18n.accounts.primaryholder"),
              jointHolder: kony.i18n.getLocalizedString("i18n.accounts.jointHolder"),
              creditcardNumber: kony.i18n.getLocalizedString("i18n.accountDetail.creditCardNumber"),
              creditIssueDate: kony.i18n.getLocalizedString("i18n.accountDetail.creditIssueDate"),
              iban : kony.i18n.getLocalizedString("i18n.Accounts.IBAN")
          };
          var accountHolderFullName = function(accountHolder){
                try{
                    var accountHolder = JSON.parse(accountHolder);
                    return accountHolder.fullname;
                }
                catch (exception){
                    return accountHolder;
                }

            };
            var jointAccountHolders = function(accountHolderslist){
                var accountHoldersNameList = "";
                try{
                    accountHolders = JSON.parse(accountHolderslist);
                    for( var index in accountHolders){
                        accountHoldersNameList = accountHoldersNameList + accountHolders[index].fullname + ", ";
                    }
                    accountHoldersNameList = accountHoldersNameList.slice(0,-2);
                    return accountHoldersNameList;
                } catch(exception){
                    return accountHolderslist
                }
                
            };
            var accountInfo = {
                accountName: CommonUtilities.getAccountName(account),
                accountNumber: CommonUtilities.getMaskedAccountNumber(account.accountID),
                accountID: account.accountID,
                accountType: account.accountType,
                routingNumber: account.routingNumber,
                swiftCode: account.swiftCode,
                primaryAccountHolder: accountHolderFullName(account.accountHolder),
                jointHolder: jointAccountHolders(account.jointHolders),
                creditIssueDate: CommonUtilities.getFrontendDateString(account.openingDate),
                creditcardNumber: account.creditCardNumber,
                iban: account.IBAN ? applicationManager.getFormatUtilManager().formatIBAN(account.IBAN) : ""
            }
            var AccountTypeConfig = {};
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)] = ['accountNumber', 'routingNumber', 'swiftCode', 'primaryAccountHolder', 'jointHolder', 'iban'];
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)]= ['creditcardNumber', 'primaryAccountHolder', 'creditIssueDate', '', '', 'iban'];
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)]= ['accountNumber', 'primaryAccountHolder', 'jointHolder', '', '', 'iban'];
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)]= ['accountNumber', 'primaryAccountHolder', 'jointHolder', '', '', 'iban'];
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)]= ['accountNumber', 'primaryAccountHolder', 'jointHolder', '', '', 'iban'];
            AccountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)]= ['accountNumber', 'routingNumber', 'swiftCode', 'primaryAccountHolder', 'jointHolder', 'iban'];
        
          var AccountInfoUIConfigs = [{
              flx: 'flx' + 'AccountNumber',
              label: 'lbl' + 'AccountNumber',
              value: 'lbl' + 'AccountNumber' + 'Value'
          }, {
              flx: 'flx' + 'RoutingNumber',
              label: 'lbl' + 'RoutingNumber',
              value: 'lbl' + 'RoutingNumber' + 'Value'
          }, {
              flx: 'flx' + 'SwiftCode',
              label: 'lbl' + 'SwiftCode',
              value: 'lbl' + 'SwiftCode' + 'Value'
          }, {
              flx: 'flx' + 'PrimaryHolder',
              label: 'lbl' + 'PrimaryHolder',
              value: 'lbl' + 'PrimaryHolder' + 'Value'
          }, {
              flx: 'flx' + 'JointHolder',
              label: 'lbl' + 'JointHolder' + 'Title',
              value: 'lbl' + 'JointHolder' + 'Value'
          }, {
            flx: 'flx' + 'IBAN',
            label: 'lbl' + 'IBANCode',
            value: 'lbl' + 'IBANCode' + 'Value'
          }];
          AccountInfoUIConfigs.map(function(uiConfig, i) {
              var toShow = null;
              var key = AccountTypeConfig[account.accountType][i];
              if (key) {
                if(accountInfo[key]){
                    toShow = {
                        label: key,
                        value: accountInfo[key]
                    };
                }
              }
              return {
                  uiConfig: uiConfig,
                  toShow: toShow
              };
          }).forEach(function(config) {
              if (config.toShow === null) {
                  controller.view.accountSummary[config.uiConfig.flx].isVisible = false;
                  controller.view.accountSummary[config.uiConfig.label].text = '';
                  controller.view.accountSummary[config.uiConfig.value].text = '';
              } else {
                  controller.view.accountSummary[config.uiConfig.flx].isVisible = true;
                  controller.view.accountSummary[config.uiConfig.label].text = LabelToDisplayMap[config.toShow.label];
                  controller.view.accountSummary[config.uiConfig.value].text = config.toShow.value;
                  if(config.uiConfig.flx==="flxIBAN"){
                    CommonUtilities.enableCopy( controller.view.accountSummary[config.uiConfig.value]);
                  }
              }
          });
          this.view.accountSummary.forceLayout();
      },
      /**
       * Method to get quick actions for accounts
       * @param {Object} dataInput Data inputs like onCancel/accountType etc
       * @returns {Object} quick action for selected account
       */
      getQuickActions: function(dataInput) {
          var onCancel = dataInput.onCancel;
          var quickActions = [{
              actionName: ViewConstants.ACTION.SCHEDULED_TRANSACTIONS,
              displayName: dataInput.scheduledDisplayName || kony.i18n.getLocalizedString("i18n.accounts.scheduledTransactions"),
              action: function(account) {
                  if (dataInput.showScheduledTransactionsForm) {
                      dataInput.showScheduledTransactionsForm(account);
                  }
              }
          }, {
              actionName: ViewConstants.ACTION.MAKE_A_TRANSFER, //MAKE A TRANSFER
              displayName: dataInput.makeATransferDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
              action: function(account) {
                  //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                  applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                      accountObject: account,
                      onCancelCreateTransfer: onCancel
                  });
              }
          }, {
              actionName: ViewConstants.ACTION.TRANSFER_MONEY, //MAKE A TRANSFER
              displayName: dataInput.tranferMoneyDisplayName || kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
              action: function(account) {
                  //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                  applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                      accountObject: account,
                      onCancelCreateTransfer: onCancel
                  });
              }
          }, {
              actionName: ViewConstants.ACTION.PAY_A_BILL,
              displayName: dataInput.payABillDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom"),
              action: function(account) {
                  //Function call to open bill pay screen
                  var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                  billPayModule.presentationController.showBillPayData("Accounts","PayABillWithContext",true,{
                      "fromAccountNumber": account.accountID,
                      "show": 'PayABill',
                      "onCancel": onCancel
                  });
              }
          }, {
              actionName: ViewConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY,
              displayName: dataInput.sendMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Pay.SendMoney"),
              action: function(account) {
                  var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                  var dataItem = account;
                  dataItem.onCancel = onCancel;
                  p2pModule.presentationController.showPayAPerson("sendMoneyTab", dataItem);
              }
          }, {
              actionName: ViewConstants.ACTION.PAY_DUE_AMOUNT,
              displayName: dataInput.payDueAmountDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount"),
              action: function(account) {
                  account.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue);
                  account.currentAmountDue = account.currentAmountDue.slice(1);
                  var data = {
                      "accounts": account
                  };
                  var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                  loanModule.presentationController.navigateToLoanDue(data);
              }
          }, {
              actionName: ViewConstants.ACTION.PAYOFF_LOAN,
              displayName: dataInput.payoffLoanDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
              action: function(account) {
                  account.principalBalance = CommonUtilities.formatCurrencyWithCommas(account.principalBalance);
                  account.payOffCharge = CommonUtilities.formatCurrencyWithCommas(account.payOffCharge);
                  var data = {
                      "accounts": account
                  };
                  var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                  loanModule.presentationController.navigateToLoanPay(data);
              }
          }, {
              actionName: ViewConstants.ACTION.STOPCHECKS_PAYMENT,
              displayName: dataInput.stopCheckPaymentDisplayName || kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
              action: function(account) {
                  var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                  stopPaymentsModule.presentationController.showStopPayments({
                      onCancel: onCancel,
                      accountID: account.accountID,
                      "show": ViewConstants.ACTION.SHOW_STOPCHECKS_FORM
                  });
              }
          }, {
              actionName: ViewConstants.ACTION.VIEW_STATEMENTS,
              displayName: dataInput.viewStatementsDisplayName || kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS"),
              action: function(account) {
                  var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                  accountsModule.presentationController.showFormatEstatements(account);
              }
          }, {
              actionName: ViewConstants.ACTION.UPDATE_ACCOUNT_SETTINGS,
              displayName: dataInput.updateAccountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
              action: function() {
                  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                  profileModule.presentationController.enterProfileSettings("accountSettings");
              }
          }, {
              actionName: ViewConstants.ACTION.REMOVE_ACCOUNT,
              displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount"),
              action: function(account) {
                  var accountData = account;
                  var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                  accountModule.presentationController.showAccountDeletionPopUp(accountData);
              }
          }, {
              actionName: ViewConstants.ACTION.ACCOUNT_PREFERENCES,
              displayName: kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountPreferences"),
              action: function() {
                  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                  profileModule.presentationController.enterProfileSettings("accountSettings");
              }
          }, {
              actionName: ViewConstants.ACTION.EDIT_ACCOUNT,
              displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.editAccount"),
              action: function(account) {
                  var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                  profileModule.presentationController.initializeUserProfileClass();
                  profileModule.presentationController.showEditExternalAccount(account);
              }
          }];
          return quickActions;
      },
      /**
       * Method to check if action is valid or not
       * @param {String} actionName Name of action
       * @param {JSON} account List of accounts
       * @returns {Boolean} true/false
       */
      isValidAction: function(actionName, account) {
          var isValid = false;
          var principalBalance, currentAmountDue;
          switch (actionName) {
              case ViewConstants.ACTION.PAY_A_BILL:
                  isValid = applicationManager.getConfigurationManager().getConfigurationValue('isBillPayEnabled') === "true" && account.supportBillPay === "1";
                  break;
              case ViewConstants.ACTION.MAKE_A_TRANSFER:
              case ViewConstants.ACTION.TRANSFER_MONEY:
                  isValid = ((applicationManager.getConfigurationManager().getConfigurationValue('isKonyBankAccountsTransfer') === "true") || (applicationManager.getConfigurationManager().getConfigurationValue('isOtherKonyAccountsTransfer') === "true") || (applicationManager.getConfigurationManager().getConfigurationValue('isOtherBankAccountsTransfer') === "true") || (applicationManager.getConfigurationManager().getConfigurationValue('isInternationalAccountsTransfer') === "true") || (applicationManager.getConfigurationManager().getConfigurationValue('isInternationalWireTransferEnabled') === "true") || (applicationManager.getConfigurationManager().getConfigurationValue('isDomesticWireTransferEnabled') === "true"))
                  break;
              case ViewConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY:
                  isValid = applicationManager.getConfigurationManager().getConfigurationValue('ispayAPersonEnabled') === "true";
                  break;
              case ViewConstants.ACTION.PAY_DUE_AMOUNT:
                  principalBalance = Number(account.principalBalance) ? Number(account.principalBalance) : 0;
                  currentAmountDue = Number(account.currentAmountDue) ? Number(account.currentAmountDue) : 0;
                  isValid = applicationManager.getConfigurationManager().getConfigurationValue('loanPaymentEnabled') === "true" && (principalBalance > 0 && currentAmountDue > 0);
                  break;
              case ViewConstants.ACTION.PAYOFF_LOAN:
                  principalBalance = Number(account.principalBalance) ? Number(account.principalBalance) : 0;
                  isValid = applicationManager.getConfigurationManager().getConfigurationValue('payOffLoanPaymentEnabled') === "true" && (principalBalance > 0);
                  break;
              case ViewConstants.ACTION.STOPCHECKS_PAYMENT:
                  isValid = applicationManager.getConfigurationManager().getConfigurationValue('enableStopPayments') === "true";
                  break;
              case OLBConstants.ACTION.UPDATE_ACCOUNT_SETTINGS:
                  return !(orientationHandler.isMobile || kony.application.getCurrentBreakpoint() == 640);
              case OLBConstants.ACTION.ACCOUNT_PREFERENCES:
                  return !(orientationHandler.isMobile || kony.application.getCurrentBreakpoint() == 640);
              case OLBConstants.ACTION.EDIT_ACCOUNT:
                  return !(orientationHandler.isMobile || kony.application.getCurrentBreakpoint() == 640);
              default:
                  isValid = true;
          }
          return isValid;
      },
      /**
       * Method to get action for specific account
       * @param {Collection} Actions List of Actions
       * @param {String} actionName Name of action
       * @param {JSON} account Account for which action is required
       * @returns {Object} matched action for the account from liat of actions
       */
      getAction: function(Actions, actionName, account) {
          var actionItem, matchedAction;
          for (var i = 0; i < Actions.length; i++) {
              actionItem = Actions[i];
              if (actionItem.actionName === actionName) {
                  matchedAction = {
                      actionName: actionItem.actionName,
                      displayName: actionItem.displayName,
                      action: actionItem.action.bind(null, account)
                  };
                  break;
              }
          }
          if (!matchedAction) {
              CommonUtilities.ErrorHandler.onError("Action :" + actionName + " is not found, please validate with Contextial actions list.");
              return false;
          }
          return matchedAction;
      },
      /**
       * Method to set secondary actions for selected account type
       * @param {JSON} account Account for which secondary actions are required
       */
      setSecondayActions: function(account) {
          var self = this;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var onCancel = function() {
              self.loadAccountModule().presentationController.presentAccountDetails();
          };
          //Secondary action object array
          var SecondaryActions = this.getQuickActions({
              onCancel: onCancel
          });
          var accountType = account.accountType,
              validActions,
              finalActions = [];
          if (accountType) {
              var SecondaryActionsConfig = OLBConstants.CONFIG.ACCOUNTS_SECONDARY_ACTIONS;
              var actions = SecondaryActionsConfig[account.accountType];
              if (actions.length) {
                  validActions = actions.filter(function(action) {
                      return self.isValidAction(action, account);
                  });
                  finalActions = validActions.map(function(action) { //get action object.
                      return self.getAction(SecondaryActions, action, account);
                  });
              }
          }
          var dataMap = {
              "lblUsers": "lblUsers",
              "lblSeparator": "lblSeparator ",
              "flxAccountTypes": "flxAccountTypes"
          };
          var secondaryActions = finalActions.map(function(dataItem) {
              return {
                  "lblUsers": {
                      "text": dataItem.displayName,
                      "toolTip": CommonUtilities.changedataCase(dataItem.displayName)
                  },
                  "lblSeparator": " ",
                  "flxAccountTypes": {
                      "onClick": dataItem.action
                  }
              };
          });
          this.view.moreActions.segAccountTypes.widgetDataMap = dataMap;
          this.view.moreActions.segAccountTypes.setData(secondaryActions);
          this.view.moreActions.forceLayout();
          this.AdjustScreen();
      },
      /**
       * Reset Right side Actions UI
       */
      resetRightSideActions: function() {
          var scopeObj = this;
          for (var i = 0; i < scopeObj.rightActionButtons.length; i++) {
              scopeObj.rightActionButtons[i].setVisibility(false);
              scopeObj.rightActionButtons[i].onClick = null;
              if (i >= 0 && i < scopeObj.rightActionSeparator.length) {
                  scopeObj.rightActionSeparator[i].setVisibility(false);
              }
          }
      },
      /**
       * Method to set actions for the right top section of frmAccountDetails
       * @param {JSON} account Account for which actions are needed
       */
      setRightSideActions: function(account) {
          var self = this;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var onCancel = function() {
              self.loadAccountModule().presentationController.presentAccountDetails();
          };
          //Right side action object array
          var RightSideActions = this.getQuickActions({
              onCancel: onCancel,
              showScheduledTransactionsForm: self.loadAccountModule().presentationController.showScheduledTransactionsForm.bind(self.loadAccountModule().presentationController),
              payABillDisplayName: kony.i18n.getLocalizedString("i18n.hamburger.payABill"),
              makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
              payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount"),
              payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
              viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS"),
              viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
              updateAccountSettingsDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
              stopCheckPaymentDisplayName: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
          });
          var accountType = account.accountType,
              validActions,
              finalActions = [];
          if (accountType) {
              var RightSideActionsConfig = OLBConstants.CONFIG.ACCOUNTS_RIGHTSIDE_ACTIONS;
              var actions = RightSideActionsConfig[account.accountType];
              if (actions.length) {
                  validActions = actions.filter(function(action) {
                      return self.isValidAction(action, account);
                  });
                  finalActions = validActions.map(function(action) { //get action object.
                      return self.getAction(RightSideActions, action, account);
                  });
              }
          }
          var dataItem, j;
          var viewModel = finalActions || [];
          // Right action buttonas
          self.rightActionButtons = [
              self.view.btnScheduledTransfer,
              self.view.btnMakeTransfer,
              self.view.btnPayABill
          ];
          // Right action saparators
          self.rightActionSeparator = [
              self.view.flxSeparatorPrimaryActions,
              self.view.flxSeparatorPrimaryActionsTwo
          ];
          self.resetRightSideActions(); //Reset Actions
          if (viewModel.length === 0) {
              self.view.flxPrimaryActions.setVisibility(false);
          } else {
              self.view.flxPrimaryActions.setVisibility(true);
              for (var i = 0; i < viewModel.length && viewModel.length <= self.rightActionButtons.length; i++) {
                  dataItem = viewModel[i];
                  self.rightActionButtons[i].text = dataItem.displayName;
                  self.rightActionButtons[i].setVisibility(true);
                  self.rightActionButtons[i].onClick = dataItem.action;
                  self.rightActionButtons[i].toolTip = CommonUtilities.changedataCase(dataItem.displayName);
                  j = i - 1;
                  if (j >= 0 && j < self.rightActionSeparator.length) {
                      self.rightActionSeparator[j].setVisibility(true);
                  }
              }
          }
      },
      /**
       *  Method to handle highlight Transaction Type
       * @param {String} transactionType transaction type of the transaction
       */
      highlightTransactionType: function(transactionType) {
          var skins = {
              selected: ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED,
              unselected: ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED,
              hover: ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER,
              hovered: ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER
          };
          var transactionTypeToButtonIdMap = {
              'All': ['btnAllChecking', 'btnAllCredit', 'btnAllDeposit', 'btnAllLoan'],
              'Checks': ['btnChecksChecking'],
              'Deposits': ['btnDepositsChecking', 'btnDepositDeposit'],
              'Transfers': ['btnTransfersChecking'],
              'Withdrawals': ['btnWithdrawsChecking', 'btnWithdrawDeposit'],
              'Payments': ['btnPaymentsCredit'],
              'Purchases': ['btnPurchasesCredit'],
              'Interest': ['btnInterestDeposit']
          };
          var tabs = [this.view.transactions.flxTabsChecking,
              this.view.transactions.flxTabsCredit,
              this.view.transactions.flxTabsDeposit,
              this.view.transactions.flxTabsLoan
          ];
          var currentTab = tabs.filter(function(tab) {
              return tab.isVisible === true;
          })[0];
          if (transactionTypeToButtonIdMap[transactionType]) {
              var isForTransactionType = function(button) {
                  return transactionTypeToButtonIdMap[transactionType].some(function(buttonId) {
                      return buttonId === button.id;
                  });
              };
              var updateButtonSkin = function(button) {
                  if (isForTransactionType(button)) {
                      if(kony.application.getCurrentBreakpoint() == 640)
                      	button.top = "0dp";
                      else
                        button.top = "-1dp";
                      button.skin = skins.selected;
                      button.hoverSkin = skins.hover;
                  } else {
                      if(kony.application.getCurrentBreakpoint() == 640)
                      	button.top = "0dp";
                      else
                        button.top = "-1dp";
                      button.skin = skins.unselected;
                      button.hoverSkin = skins.hovered;
                  }
              };
              var getButtonsFrom = function(tab) {
                  var hasText = function(text) {
                      return function(fullText) {
                          return fullText.indexOf(text) > -1;
                      };
                  };
                  var getValueFrom = function(object) {
                      return function(key) {
                          return object[key];
                      };
                  };
                  return Object.keys(tab).filter(hasText('btn')).map(getValueFrom(tab));
              };
              getButtonsFrom(currentTab).forEach(updateButtonSkin);
          }
      },
      /**
       * Method to check for pending transaction
       * @param {JSON} transaction transactions
       * @returns {Boolean} true/false
       */
      isPendingTransaction: function(transaction) {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          return typeof transaction.statusDescription === 'string' && transaction.statusDescription.toLowerCase() === OLBConstants.PENDING;
      },
      /**
       * Method to check for successful transaction
       * @param {JSON} transaction transactions
       * @returns {Boolean} true/false
       */
      isSuccessfulTransacion: function(transaction) {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          return typeof transaction.statusDescription === 'string' && transaction.statusDescription.toLowerCase() === OLBConstants.SUCCESSFUL;
      },
      /**
       * Method to get mapping for different type of transactions
       * @param {String} context context for which mappings are required
       * @param {JSON} transaction current transaction
       * @param {JSON} account account for which mapping is required
       * @param {Object} transactionProperties  transactionProperties like isTransactionDisputed etc
       * @returns {JSON} mappings for requested type
       */
      getMappings: function(context, transaction, account, transactionProperties) {
          var self = this;
          var isTransactionDisputed = transactionProperties.isTransactionDisputed
          if (context === "imgError") {
              if (isTransactionDisputed === true) {
                  return {
                      "isVisible": true
                  };
              } else {
                  return {
                      "isVisible": false
                  };
              }
          }
          if (context === "imgWarning") {
              if (isTransactionDisputed === true) {
                  return {
                      "isVisible": true
                  };
              } else {
                  return {
                      "isVisible": false
                  };
              }
          }
          if (context === "btnViewRequests") {
              if (isTransactionDisputed === true) {
                  return {
                      "isVisible": true,
                      "text": kony.i18n.getLocalizedString('i18n.StopCheckPayments.ViewRequests'),
                      "onClick": function() {
                          self.loadAccountModule().presentationController.onViewDisputedRequets(transaction);
                      }
                  };
              } else {
                  return {
                      "isVisible": false
                  };
              }
          }
          if (context === "btnDisputeTransaction") {
              if (applicationManager.getConfigurationManager().editDisputeATransaction === "true") {
                  if (isTransactionDisputed === true) {
                    return {
                        "isVisible": false
                    };
                  } else {
                    if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.DEPOSIT) 
                        || transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTEREST) 
                        || transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.RECEIVEDREQUEST)) {
                        return {
                            "isVisible": false
                        };
                    } else {
                        return {
                            "text": kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                            "isVisible": true,
                            "onClick": function() {
                                self.loadAccountModule().presentationController.onDisputeTransaction(transaction);
                            }
                        };
                    }
                      
                  }
              }
          }
          if (context === "lblDisputedWarning") {
              if (isTransactionDisputed === true) {
                  return {
                      "text": kony.i18n.getLocalizedString('i18n.StopCheckPayments.DisputedWarning'),
                      "isVisible": true
                  };
              }
          }
          if (context === "btnPrint") {
              if (CommonUtilities.isPrintEnabled()) {
                  return {
                      "text": kony.i18n.getLocalizedString('i18n.accounts.print'),
                      "isVisible" :(kony.application.getCurrentBreakpoint() == 1024|| orientationHandler.isTablet) ? false : true,
                      "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString('i18n.common.PrintThisTransaction')),
                      "onClick": function() {
                          self.onClickPrintRow();
                      }
                  };
              } else {
                  return {
                      "isVisible": false
                  };
              }
          }
          if (context === "nickName") {
              return self.getDataByType(account.accountType, transaction).ToValue || "";
          }
      },
      /**
       * Method to get actions for repeat button for each transaction
       * @param {Object} transactionProperties transactionProperties like isTransactionDisputed etc
       * @param {JSON} transaction transaction for which repeat actions are required
       * @returns {Object} repeat button properties like text tooltip etc
       */
      btnRepeatActions: function (transactionProperties, transaction) {
          var self = this;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var isModuleEnabled = self.isTransactionTypeEnabled(transaction);
          if (isModuleEnabled && transactionProperties.isRepeatSupportAccount && transactionProperties.isRepeatableTransaction && !transactionProperties.isTransactionDisputed && !transactionProperties.isFeesOrInterestTransaction && transaction.statusDescription.toLowerCase() === OLBConstants.SUCCESSFUL) {
              return {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
                  "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString('i18n.common.repeatThisTransaction')),
                  "isVisible": true,
                  "onClick": function () {
                      self.onRepeatTransaction(transaction)
                  }
              };
          } else {
              return {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
                  "isVisible": false,
                  "onClick": function () { }
              };
          }
      },

      /**
       * Return the given transaction type supports the module or not based on config.
       * @param {JSON} transaction transaction for which repeat actions are required
       * @returns {boolean} whether the transaction type enalbed or not 
       */
      isTransactionTypeEnabled: function (transaction) {
          var isModuleEnabled = false;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var configurationManager = applicationManager.getConfigurationManager();
          if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER) || transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER)) {
              isModuleEnabled = !(configurationManager.isKonyBankAccountsTransfer === "false" &&
                  configurationManager.isOtherKonyAccountsTransfer === "false" &&
                  configurationManager.isOtherBankAccountsTransfer === "false" &&
                  configurationManager.isInternationalAccountsTransfer === "false");
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.BILLPAY)) {
              isModuleEnabled = configurationManager.isBillPayEnabled === "true";
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.P2P)) {
              isModuleEnabled = configurationManager.ispayAPersonEnabled === "true";
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.LOAN)) {
              isModuleEnabled = true;
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.WIRE)) {
              isModuleEnabled = applicationManager.getUserPreferencesManager().getWireTransferEligibleForUser();
          }

          return isModuleEnabled;
      },
      /**
       * Method that gets called on click of repeat transaction
       * @param {JSON} transaction transaction that needs to be repeated
       */
      onRepeatTransaction: function(transaction) {
          var transactionData;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var onCancel=this.presenter.presentAccountDetails.bind(this.presenter);
          if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER) || transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER)) {
              transactionData = {
                  "amount": Math.abs(transaction.amount),
                  "frequencyEndDate": transaction.frequencyEndDate,
                  "frequencyStartDate": transaction.frequencyStartDate,
                  "frequencyType": transaction.frequencyType,
                  "fromAccountNumber": transaction.fromAccountNumber,
                  "isScheduled": "false",
                  "numberOfRecurrences": transaction.numberOfRecurrences,
                  "scheduledDate": transaction.scheduledDate,
                  "toAccountNumber": transaction.toAccountNumber,
                  "transactionDate": transaction.transactionDate,
                  "ExternalAccountNumber": transaction.ExternalAccountNumber,
                  "transactionId": transaction.transactionId,
                  "notes": transaction.transactionsNotes,
                  "transactionType": transaction.transactionType,
                  "category": transaction.category,
                  "isInternationalAccount" : transaction.isInternationalAccount,
                  "serviceName" : transaction.serviceName
              };
              if(transaction.isInternationalAccount === "false" && transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER))
              {
                transactionData.IBAN = transaction.IBAN ? transaction.IBAN :"",
                transactionData.toAccountName = transaction.toAccountName ? transaction.toAccountName :"";
              }else if(transaction.isInternationalAccount === "true" &&  transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER)){
                transactionData.swiftCode = transaction.swiftCode ? transaction.swiftCode :"";
                transactionData.toAccountName = transaction.toAccountName ? transaction.toAccountName :"";
                transactionData.bankName = transaction.bankName ? transaction.bankName :"";
                transactionData.ExternalAccountNumber = transaction.ExternalAccountNumber ? transaction.ExternalAccountNumber :"";
              }
              this.loadAccountModule().presentationController.repeatTransfer(transactionData, onCancel);
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.BILLPAY))) {
              transactionData = {
                  "payeeNickname": transaction.payeeNickName || transaction.payeeName,
                  "dueAmount": transaction.billDueAmount,
                  "payeeId": transaction.payeeId,
                  "billid": transaction.billid,
                  "sendOn": transaction.scheduledDate,
                  "notes": transaction.transactionsNotes,
                  "amount": String(Math.abs(transaction.amount)),
                  "fromAccountName": transaction.fromAccountName,
                  "fromAccountNumber": transaction.fromAccountNumber,
                  "lastPaidAmount": transaction.billPaidAmount || transaction.lastPaidAmount,
                  "lastPaidDate": transaction.billPaidDate || transaction.lastPaidDate,
                  "nameOnBill": transaction.nameOnBill,
                  "eBillSupport": transaction.eBillSupport,
                  "eBillStatus": transaction.eBillEnable,
                  "billDueDate": transaction.billDueDate,
                  "billCategory": transaction.billCategoryId,
                  "billCategoryName": transaction.billCategory,
                  "billGeneratedDate": transaction.billGeneratedDate,
                  "ebillURL": transaction.ebillURL,
                  "frequencyEndDate": transaction.frequencyEndDate,
                  "frequencyStartDate": transaction.frequencyStartDate,
                  "frequencyType": transaction.frequencyType,
                  "numberOfRecurrences": transaction.numberOfRecurrences,
                  "isScheduled": transaction.isScheduled,
                "serviceName" : transaction.serviceName
              }
              this.loadAccountModule().presentationController.repeatBillPay(transactionData, onCancel);
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.P2P)) {
              this.loadAccountModule().presentationController.repeatP2P(transaction, onCancel);
          } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.WIRE)) {
                transactionData = {
                    payeeNickName: transaction.payeeNickName,
                    payeeName: transaction.payeeName,
                    payeeCurrency: transaction.payeeCurrency,
                    type: transaction.payeeType,
                    wireAccountType: transaction.wireAccountType,
                    accountNumber: transaction.payeeAccountNumber,
                    routingCode: transaction.routingNumber,
                    country: transaction.country,
                    IBAN: transaction.IBAN ? applicationManager.getFormatUtilManager().formatIBAN(transaction.IBAN) :"",
                    fromAccountNumber: transaction.fromAccountNumber,
                    swiftCode: transaction.swiftCode,
                    internationalRoutingCode: transaction.internationalRoutingCode,
                    payeeId: transaction.payeeId,
                    bankAddressLine1: transaction.bankAddressLine1,
                    bankAddressLine2: transaction.bankAddressLine2,
                    bankCity: transaction.bankCity,
                    bankState: transaction.bankState,
                    bankZip: transaction.bankZip,
                    bankName: transaction.bankName,
                    addressLine1: transaction.payeeAddressLine1,
                    addressLine2: transaction.payeeAddressLine2,
                    cityName: transaction.cityName,
                    state: transaction.state,
                    transactionsNotes: transaction.transactionsNotes,
                    amount: Math.abs(transaction.amount),
                    amountRecieved: Math.abs(transaction.amountRecieved),
                    zipCode: transaction.zipCode,
                  	serviceName : transaction.serviceName
                };        
                this.loadAccountModule().presentationController.repeatWireTransfer(transactionData, onCancel);
          }
      },
      /**
       * Method to handle onClick of the transaction segment list to toggle
       */
      onClickToggle: function() {
          var scopeObj = this;
          var index = kony.application.getCurrentForm().transactions.segTransactions.selectedIndex;
          var sectionIndex = index[0];
          var rowIndex = index[1];
          var data = kony.application.getCurrentForm().transactions.segTransactions.data;
          var collapseAll = function(segments) {
              segments.forEach(function(segment, i) {
                  if (segment.template === scopeObj.checkImagesTemplate || segment.template === scopeObj.transactionsSelectedTemplate) {
                      segment.template = scopeObj.transactionsTemplate;
                      segment.imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
                      kony.application.getCurrentForm().transactions.segTransactions.setDataAt(segment, i, sectionIndex);
                  }
              });
          };
          if (data[sectionIndex][1]) {
              if ((data[sectionIndex][1][rowIndex].lblTypeValue === "CheckWithdrawal" || data[sectionIndex][1][rowIndex].lblTypeValue === "CheckDeposit") && data[sectionIndex][1][rowIndex].template === this.transactionsTemplate) {
                  collapseAll(data[sectionIndex][1]);
                  data[sectionIndex][1][rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_UP;
                  data[sectionIndex][1][rowIndex].template = this.checkImagesTemplate;
              } else if (data[sectionIndex][1][rowIndex].template === this.transactionsTemplate) {
                  collapseAll(data[sectionIndex][1]);
                  data[sectionIndex][1][rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_UP;
                  data[sectionIndex][1][rowIndex].template = this.transactionsSelectedTemplate;
              } else {
                  data[sectionIndex][1][rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
                  data[sectionIndex][1][rowIndex].template = this.transactionsTemplate;
              }
              kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
              var data1 =  kony.application.getCurrentForm().transactions.segTransactions.clonedTemplates[sectionIndex][1][rowIndex].frame.height;
              data[sectionIndex][1][rowIndex].flxIdentifier.height= data1 +"dp";
              kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
          } else {
              if ((data[sectionIndex][1][rowIndex].lblTypeValue === "CheckWithdrawal" || data[sectionIndex][1][rowIndex].lblTypeValue === "CheckDeposit") && data[rowIndex].template === this.transactionsTemplate) {
                  collapseAll(data);
                  data[rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_UP;
                  data[rowIndex].template = this.checkImagesTemplate;
              } else if (data[rowIndex].template === this.transactionsTemplate) {
                  collapseAll(data);
                  data[rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_UP;
                  data[rowIndex].template = this.transactionsSelectedTemplate;
              } else {
                  data[rowIndex].imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
                  data[rowIndex].template = this.transactionsTemplate;
              }
              kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[rowIndex], rowIndex, sectionIndex);
          }
          this.view.transactions.forceLayout();
          this.AdjustScreen();
      },
      /**
       * Method to assign action for print row click
       */
      onClickPrintRow: function() {
          var rowIndex = this.view.transactions.segTransactions.selectedIndex[1];
          var secIndex = this.view.transactions.segTransactions.selectedIndex[0];
          var data = this.view.transactions.segTransactions.data[secIndex][1][rowIndex];
          this.loadAccountModule().presentationController.showTransferPrintPage({
              transactionRowData: data,
              accountDisplayName: this.view.lblAccountTypes.text
          });
      },
      /**
       * onClickPrint : Method to assign action for print
       */ 
      onClickPrint : function(){
            var accountDisplayName = this.view.lblAccountTypes.text;
            this.presenter.showPrintPage({
            transactions : this.view.transactions.segTransactions.data,
            accountDisplayName : accountDisplayName
            });
        },
      /**
       * Method to show Check Image
       * @param {String} frontImage front image of check
       * @param {String} backImage back image of check
       * @param {String} postDate post date of check
       * @param {String} amount amount of check
       * @param {String} memo memo on check
       */
      showCheckImage: function(frontImage, backImage, postDate, amount, memo) {
          var self = this;
          this.view.CheckImage.lblPostDateValue.text = CommonUtilities.getFrontendDateString(postDate);
          this.view.CheckImage.lblAmountValue.text = amount;
          this.view.CheckImage.lblMemoValue.text = (memo === undefined || memo === null || memo === "") ? kony.i18n.getLocalizedString("i18n.common.none") : memo;
          this.view.CheckImage.imgCheckImage.src = frontImage;
          this.view.flxCheckImage.height = self.getPageHeight();
          this.view.flxCheckImage.setVisibility(true);
          this.view.CheckImage.lblCheckDetails.setFocus(true);
          this.view.CheckImage.flxFlip.onClick = function() {
              if (self.view.CheckImage.imgCheckImage.src === frontImage) {
                  self.view.CheckImage.imgCheckImage.src = backImage;
              } else {
                  self.view.CheckImage.imgCheckImage.src = frontImage;
              }
          };
          this.view.CheckImage.flxPrint.onClick = function () {
              self.downloadUrl(self.view.CheckImage.imgCheckImage.src);
          };
          this.view.CheckImage.forceLayout();
      },
      /**
       * Method to get transactions value based on the account type
       * @param {String} accountType account type of transaction
       * @param {JSON} transaction current transaction
       * @returns {Object} required values for transaction type
       */
      getDataByType: function(accountType, transaction) {
          var nickname;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          if (transaction) {
              nickname = transaction.payPersonName || transaction.payeeNickName || transaction.payeeName || transaction.toAccountName||" ";
          }
              switch(accountType){
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING):
                  {
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
                        ToValue: transaction !== undefined ? nickname : " ",
                        withdrawFlag: true,
                        rememberFlag: false, // Made false as it's Not in scope R4
                        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
                    }
                  }
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING):
                  {
                        return {
                            editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
                            transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                            ToText: kony.i18n.getLocalizedString('i18n.common.To'),
                            ToValue: transaction !== undefined ? nickname : " ",
                            withdrawFlag: true,
                            rememberFlag: false, // Made false as it's Not in scope R4
                            typeText: kony.i18n.getLocalizedString('i18n.common.Type')
                        }
                  }
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD):
                  {
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.common.Download'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
                        ToText: kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                        ToValue: transaction !== undefined ? transaction.transactionId : " ",
                        withdrawFlag: false,
                        rememberFlag: false,
                        typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
                    }
                  }
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CURRENT):{
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
                        ToValue: transaction !== undefined ? nickname : " ",
                        withdrawFlag: true,
                        rememberFlag: false, // Made false as it's Not in scope R4
                        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
                    }
                  }
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN):
                  {
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.common.Download'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
                        ToText: kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                        ToValue: transaction !== undefined ? transaction.transactionId : " ",
                        withdrawFlag: false,
                        rememberFlag: false,
                        typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
                    }
                  }
                  case applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT):
                  {
                        return {
                            editText: kony.i18n.getLocalizedString('i18n.common.Download'),
                            transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
                            ToText: kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                            ToValue: transaction !== undefined ? transaction.transactionId : " ",
                            withdrawFlag: false,
                            rememberFlag: false,
                            typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
                        }
                  }
                  case applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.WIRE):
                  {
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
                        ToValue: transaction !== undefined ? nickname : " ",
                        withdrawFlag: false,
                        rememberFlag: false,
                        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
                    } 
                  }
                  default : {
                      return {
                        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
                        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
                        ToText: kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                        ToValue: transaction !== undefined ? transaction.transactionId : " ",
                        withdrawFlag: true,
                        rememberFlag: false, // Made false as it's Not in scope R4
                        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
                    }
                  }
              }
      },
      /**
       * Method to get data map for checks
       * @param {JSON} transaction transaction for which data is required
       * @param {String} transactionType transactionType for which data is required
       * @param {JSON} account Account for which data is required
       * @param {String} isTransactionTypeIconVisible check for isTransactionTypeIconVisible
       * @param {Object} transactionProperties transactionProperties like istransactionrepeatable etc
       * @returns {JSON} view Model for checks
       */
      getDataMapForChecks: function(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties) {
          var self = this;
          return {
              "lblIdentifier": "",
              "flxCheck2": {
                  "isVisible": transaction.frontImage2 ? true : false
              },
              "lblTransactionFeeKey":{
                "text": "Transaction Fee",
                "isVisible":false,
                },
                "lblTransactionFeeValue":{
                        "text": "5.00",
                        "isVisible":false,
                },
                "lblCurrencyAmountTitle":{
                        "text":"Currency",
                        "isVisible":false,
                },
                "lblCurrencyAmountValue":{
                        "text":"US Dollar($)",
                        "isVisible":false,
                },
              "flxCash": {
                  isVisible: transaction.cashAmount ? true : false
              },
              "btnPrint": self.getMappings("btnPrint", transaction, account, transactionProperties),
              "btnEditRule": {
                  "isVisible": "false"
              },
              "btnRepeat": {
                  "isVisible": "false"
              },
              "lblTypeTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Bank'),
              "lblTypeTitle2": kony.i18n.getLocalizedString('i18n.CheckImages.Bank'),
              "lblWithdrawalAmountTitle": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
              "lblWithdrawalAmountTitle2": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
              "CopylblToTitle0a2c47b22996e4f": kony.i18n.getLocalizedString('i18n.CheckImages.Cash'),
              "imgCheckImage1Icon": {
                  "text": ViewConstants.FONT_ICONS.CHECK_BOX_ICON
              },
              "imgCheckImage2Icon": {
                  "text": ViewConstants.FONT_ICONS.CHECK_BOX_ICON
              },
              "lblType": {
                "text": (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType)!=null?
                applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType):
                transactionType),
                  "isVisible": isTransactionTypeIconVisible()
              },
              "lblSeparator": "lblSeparator",
              "lblSeparator2": "lblSeparator2",
              "lblSeparatorActions": "lblSeparatorActions",
              "lblSeperatorhor1": "lblSeperatorhor1",
              "lblSeperatorhor2": "lblSeperatorhor2",
              "lblSeperatorhor3": "lblSeperatorhor3",
              "lblSeperatorhor4": "lblSeperatorhor4",
              "lblSeperatorhor5": "lblSeperatorhor5",
              "lblTotalValue": kony.i18n.getLocalizedString('i18n.CheckImages.Total'),
              "lblToTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Checks/cash'),
              "lblToTitle2": kony.i18n.getLocalizedString('i18n.CheckImages.Checks/cash'),
              "lblCheck1Ttitle": transaction.checkNumber1,
              "lblCheck2Ttitle": transaction.checkNumber2,
              "flxCheckImageIcon": {
                  "onClick": function() {
                      self.showCheckImage(transaction.frontImage1, transaction.backImage1, transaction.transactionDate, transaction.withdrawlAmount1, transaction.memo);
                  }
              },
              "flxCheckImage2Icon": {
                  "onClick": function() {
                      self.showCheckImage(transaction.frontImage2, transaction.backImage2, transaction.transactionDate, transaction.withdrawlAmount2, transaction.memo);
                  }
              },
              "lblBankName1": transaction.bankName1,
              "lblBankName2": transaction.bankName2,
              "lblWithdrawalAmountCheck1": transaction.withdrawlAmount1 ? self.getDisplayCurrencyFormatWrapper(transaction, transaction.withdrawlAmount1, false) : "",
              "lblWithdrawalAmountCheck2": transaction.withdrawlAmount2 ? self.getDisplayCurrencyFormatWrapper(transaction, transaction.withdrawlAmount2, false) : "",
              "lblWithdrawalAmountCash": transaction.cashAmount ? self.getDisplayCurrencyFormatWrapper(transaction, transaction.cashAmount, false) : "",
              "lblWithdrawalAmount": transaction.totalCheckAmount ? self.getDisplayCurrencyFormatWrapper(transaction, transaction.totalCheckAmount, false) : "",
              "txtFieldMemo": {
                  "placeholder": kony.i18n.getLocalizedString('i18n.CheckImages.MemoOptional'),
                  "isVisible": true,
                  "text": transaction.memo ? transaction.memo : ""
              },
              "imgDropdown": {
                  "text": ViewConstants.FONT_ICONS.CHEVRON_DOWN,
                  "accessibilityconfig":{
                    "a11yLabel":"View Transaction Details"
                  }
              },
              "flxDropdown": "flxDropdown",
              "lblDate": CommonUtilities.getFrontendDateString(transaction.transactionDate),
              "lblTypeValue": transactionType,
              "lblDescription": transaction.description || kony.i18n.getLocalizedString('i18n.common.none'),
              "lblAmount": self.getDisplayCurrencyFormatWrapper(transaction,transaction.amount, true),
              "lblBalance": transaction.fromAccountBalance ? self.getDisplayCurrencyFormatWrapper(transaction,transaction.fromAccountBalance, false) : kony.i18n.getLocalizedString('i18n.common.none'),
              "template": this.transactionsTemplate
          };
      },
      getDisplayCurrencyFormatWrapper: function(transactionObj, amount, considerTransactionCurrency){
            var self = this;
            if(considerTransactionCurrency){
                if(transactionObj.transactionCurrency != undefined && transactionObj.transactionCurrency != "" && transactionObj.transactionCurrency != null){
                    var amount= applicationManager.getConfigurationManager().getCurrency(transactionObj.transactionCurrency) + CommonUtilities.formatCurrencyWithCommas(amount, true);
                }
                else{
                    var amount = CommonUtilities.getDisplayCurrencyFormat(amount);
                }
            }
            else{
                var amount = CommonUtilities.getDisplayCurrencyFormat(amount);
            }
            if (amount) {
                if (amount.match(/-/)) {
                    amount = "-" + amount.replace(/-/, "");
                }
            }
            return amount;
        },
      /**
       * Method to get data map for wire transfer
       * @param {JSON} transaction transaction for which data is required
       * @param {String} transactionType transactionType for which data is required
       * @param {JSON} account Account for which data is required
       * @param {String} isTransactionTypeIconVisible check for isTransactionTypeIconVisible
       * @param {Object} transactionProperties transactionProperties like istransactionrepeatable etc
       * @returns {JSON} view Model for checks
       */
      getDataMapForWire: function(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties) {
          var self = this;
          return {
              "lblNoteTitle": kony.i18n.getLocalizedString('i18n.accounts.Note'),
              "lblNoteValue": (transaction.transactionsNotes) ? transaction.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
              "lblFrequencyTitle": {
                  "text": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                  "isVisible": false
               },
              "lblTransactionFeeKey":{
                "text": "Transaction Fee",
                "isVisible":false,
                },
                "lblTransactionFeeValue":{
                        "text": "$5.00",
                        "isVisible":false,
                },
                "lblCurrencyAmountTitle":{
                        "text":"Currency",
                        "isVisible":false,
                },
                "lblCurrencyAmountValue":{
                        "text":"US Dollar($)",
                        "isVisible":false,
                },
              "lblFrequencyValue": {
                  "text": transaction.frequencyType || kony.i18n.getLocalizedString('i18n.transfers.frequency.once'),
                  "isVisible": false
              },
              "lblRecurrenceTitle": {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.recurrence'),
                  "isVisible": false
              },
              "lblDescription": transaction.description || kony.i18n.getLocalizedString('i18n.common.none'),
              "lblRecurrenceValue": {
                  "text": transaction.numberOfRecurrences,
                  "isVisible": false
              },
              "lblIdentifier": ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                 "flxIdentifier":{
                     "height":"100dp"
                 },
              "imgDropdown": {
                  "text": ViewConstants.FONT_ICONS.CHEVRON_DOWN,               
                  "accessibilityconfig":{
                    "a11yLabel":"View Transaction Details"
                  }
              },
              "flxDropdown": "flxDropdown",
              "lblDate": CommonUtilities.getFrontendDateString(transaction.transactionDate),
              "lblType": {
                "text": (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType)!=null?
                applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType):
                transactionType),
                  "isVisible": true
              },
              "flxRememberCategory": {
                  "isVisible": false
              },
              "lblDisputedWarning": self.getMappings("lblDisputedWarning", transaction, account, transactionProperties),
              "imgError": self.getMappings("imgError", transaction, account, transactionProperties),
              "imgWarning": self.getMappings("imgWarning", transaction, account, transactionProperties),
              "btnViewRequests": self.getMappings("btnViewRequests", transaction, account, transactionProperties),
              "lblCategory": " ",
              "imgCategoryDropdown": " ",
              "lblAmount": self.getDisplayCurrencyFormatWrapper(transaction,transaction.amount, true),
              "lblBalance": transaction.fromAccountBalance ? CommonUtilities.formatCurrencyWithCommas(transaction.fromAccountBalance,false,account.currencyCode) : kony.i18n.getLocalizedString('i18n.common.none'),
              "lblSeparator": "lblSeparator",
              "lblSeparator2": "lblSeparator2",
              "btnPrint": self.getMappings("btnPrint", transaction, account, transactionProperties),
              "btnDisputeTransaction": self.getMappings("btnDisputeTransaction", transaction, account, transactionProperties),
              "btnRepeat": this.btnRepeatActions(transactionProperties, transaction),
              "lblSeparatorActions": "lblSeparatorActions",
              "lblTypeTitle": this.getDataByType(account.accountType).typeText,
              "lblToTitle": {
                  "text": this.getDataByType(account.accountType).ToText,
                  "isVisible": true
              },
              "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
              "lblTypeValue": transactionType,
              "lblToValue": {
                  "text": self.getMappings("nickName", transaction, account, transactionProperties),
                  "isVisible": true
              },
              "lblWithdrawalAmountTitle": {
                "isVisible": false,
            },
            "lblWithdrawalAmountValue": {
                "isVisible": false,
            },
              "lblSeparatorDetailData": "lblSeparatorDetailData",
              "template": this.transactionsTemplate
          }
      },
      /**
       * Method to get data map for common accounts like savings/checking etc
       * @param {JSON} transaction transaction for which data is required
       * @param {String} transactionType transactionType for which data is required
       * @param {JSON} account Account for which data is required
       * @param {String} isTransactionTypeIconVisible check for isTransactionTypeIconVisible
       * @param {Object} transactionProperties transactionProperties like istransactionrepeatable etc
       * @returns {JSON} view Model for checks
       */
      getDataMapForCommonAccounts: function(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties) {
          var self = this;
          var numberOfRecurrences;
          if (transaction.numberOfRecurrences === undefined || transaction.numberOfRecurrences === null || transaction.numberOfRecurrences === "0") {
            numberOfRecurrences = kony.i18n.getLocalizedString('i18n.common.none');
          } else {
            numberOfRecurrences = transaction.numberOfRecurrences;
          }
          return {
              "lblNoteTitle": {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.Note'),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblNoteValue": {
                  "text": (transaction.transactionsNotes) ? transaction.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblTransactionFeeKey":{
                   "text": kony.i18n.getLocalizedString("i18n.TransfersEur.TransactionFee"),
              },
              "lblTransactionFeeValue":{
                   "text": (transaction.fee != undefined && transaction.fee!= null && transaction.fee != "" ) == true ? CommonUtilities.formatCurrencyWithCommas(transaction.fee) : "",
              },
              "lblCurrencyAmountTitle": {
                "text": transaction.transactionCurrency ? kony.i18n.getLocalizedString("i18n.common.Currency") : ""
              },
              "lblCurrencyAmountValue": {
                "text": transaction.transactionCurrency ? transaction.transactionCurrency : ""
              },
              "lblFrequencyTitle": {
                  "text": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblFrequencyValue": {
                  "text": transaction.frequencyType || kony.i18n.getLocalizedString('i18n.transfers.frequency.once'),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblRecurrenceTitle": {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.recurrence'),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblDescription": transaction.description || kony.i18n.getLocalizedString('i18n.common.none'),
              "lblRecurrenceValue": {
                  "text": numberOfRecurrences,
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblIdentifier": ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                "flxIdentifier":{
                     "height":"100dp"
                 },
              "imgDropdown": {
                  "text": ViewConstants.FONT_ICONS.CHEVRON_DOWN,
                  "accessibilityconfig":{
                    "a11yLabel":"View Transaction Details"
                  }
              },
              "flxDropdown": "flxDropdown",
              "lblDate": CommonUtilities.getFrontendDateString(transaction.transactionDate),
              "lblType": {
                "text": (applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType)!=null?
                applicationManager.getTypeManager().getTransactionTypeDisplayValue(transactionType):
                transactionType),
                  "isVisible": isTransactionTypeIconVisible()
              },
              "flxRememberCategory": {
                  "isVisible": this.getDataByType(account.accountType).rememberFlag
              },
              "imgRememberCategory": {
                  "src": ViewConstants.IMAGES.UNCHECKED_IMAGE,
                  "isVisible": this.getDataByType(account.accountType).rememberFlag
              },
              "lblRememberCategory": {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.rememberCategory'),
                  "isVisible": this.getDataByType(account.accountType).rememberFlag
              },
              "lblCategory": " ",
              "imgCategoryDropdown": " ",
              "lblAmount": self.getDisplayCurrencyFormatWrapper(transaction,transaction.amount, true),
              "lblBalance": transaction.fromAccountBalance ? CommonUtilities.formatCurrencyWithCommas(transaction.fromAccountBalance,false,account.currencyCode) : kony.i18n.getLocalizedString('i18n.common.none'),
              "lblSeparator": "lblSeparator",
              "lblSeparator2": "lblSeparator2",
              "btnPrint": self.getMappings("btnPrint", transaction, account, transactionProperties),
              "btnEditRule": {
                  "text": this.getDataByType(account.accountType).editText
              },
              "btnDisputeTransaction": self.getMappings("btnDisputeTransaction", transaction, account, transactionProperties),
              "btnViewRequests": self.getMappings("btnViewRequests", transaction, account, transactionProperties),
              "imgError": self.getMappings("imgError", transaction, account, transactionProperties),
              "imgWarning": self.getMappings("imgWarning", transaction, account, transactionProperties),
              "lblDisputedWarning": self.getMappings("lblDisputedWarning", transaction, account, transactionProperties),
              "btnRepeat": this.btnRepeatActions(transactionProperties, transaction),
              "lblfrmAccountNumber": transaction.fromAcc,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblTypeTitle": this.getDataByType(account.accountType).typeText,
              "lblToTitle": {
                  "text": this.getDataByType(account.accountType).ToText,
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "fromAccountName": transaction.fromAccountName,
              "lblWithdrawalAmountTitle": {
                  "isVisible": this.getDataByType(account.accountType).withdrawFlag,
                  "text": this.transactionTypeLabelName(transaction)
              },
              "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
              "lblTypeValue": transactionType,
              "lblToValue": {
                  "text": self.getMappings("nickName", transaction, account, transactionProperties),
                  "isVisible": transactionProperties.isFeesOrInterestTransaction ? false : true
              },
              "lblWithdrawalAmountValue": {
                  "isVisible": this.getDataByType(account.accountType).withdrawFlag,
                  "text": self.getDisplayCurrencyFormatWrapper(transaction,transaction.amount, false)
              },
              "lblExternalAccountNumber": transaction.externalAccountNumber,
              "lblSeparatorDetailData": "lblSeparatorDetailData",
              "txtMemo": (!transaction.transactionsNotes) ? transaction.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
              "toAcc": transaction.toAcc,
              "externalAcc": transaction.externalAccountNumber,
              "template": this.transactionsTemplate
          };
      },

       /**
       * Method to Label Name based on Transaction Type
       * @param {object} transaction transaction for which data is required
       * @returns {string} Label Name
       */
      transactionTypeLabelName :  function (transaction) {
        if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.DEPOSIT)){
             return kony.i18n.getLocalizedString('i18n.common.depositedAmount');
        } else if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.CREDIT)){
             return kony.i18n.getLocalizedString('i18n.common.creditedAmount');
        } else if (transaction.transactionCurrency) {
            if (applicationManager.getConfigurationManager().getBaseCurrency() === transaction.transactionCurrency ){
                return kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount') ;
            } else {
                return kony.i18n.getLocalizedString('i18n.common.convertedAmount');
            }
        }				 
        return kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount');
    },
      /**
       * Method to create segment model for the transactions
       * @param {Object} dataInputs data inputs like account, transactiontype etc
       * @param {JSON} transaction Current transaction
       * @returns {JSON} dataMap for the segment
       */
      createTransactionSegmentModel: function(dataInputs, transaction) {
          var dataMap;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          var account = dataInputs.account;
          var transactionType = transaction.transactionType; //[Checking, Saving, Loan] etc
          var requestedTransactionType = dataInputs.transactionType; // [All, Deposit, Loan] etc  
          var isTransactionTypeIconVisible = function() {
              return requestedTransactionType === OLBConstants.ALL && (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING) || account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING));
          }
          var isRepeatSupportAccount = (account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING) || account.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING));
          var isRepeatableTransaction = (transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER) || transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER) || transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.BILLPAY) || transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.P2P)) || (transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.WIRE) && !(transaction.isPayeeDeleted === "true"));
          var isTransactionDisputed = (transaction.isDisputed === true || transaction.isDisputed === "true"); //MF has issue with so we are checking both boolean and string values.
          var isFeesOrInterestTransaction = (transactionType == OLBConstants.TRANSACTION_TYPE.TAX || transactionType == OLBConstants.TRANSACTION_TYPE.POS || transactionType == OLBConstants.TRANSACTION_TYPE.INTERNETTRANSACTION || transactionType == OLBConstants.TRANSACTION_TYPE.CARDPAYMENT || transactionType == OLBConstants.TRANSACTION_TYPE.FEE || transactionType == OLBConstants.TRANSACTION_TYPE.CREDIT || transactionType == OLBConstants.TRANSACTION_TYPE.FEES || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTCREDIT || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTDEBIT);
          var transactionProperties = {
              transactionType: transactionType,
              requestedTransactionType: requestedTransactionType,
              isTransactionTypeIconVisible: isTransactionTypeIconVisible,
              isRepeatSupportAccount: isRepeatSupportAccount,
              isRepeatableTransaction: isRepeatableTransaction,
              isTransactionDisputed: isTransactionDisputed,
              isFeesOrInterestTransaction: isFeesOrInterestTransaction
          }
          if (isTransactionTypeIconVisible()) {
              this.view.transactions.flxSortType.setVisibility(true);
          } else {
              this.view.transactions.flxSortType.setVisibility(false);
          }
          if (transactionType === "Checks" || transactionType === "CheckWithdrawal" || transactionType === "CheckDeposit") {
              dataMap = this.getDataMapForChecks(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties);
          } else if (transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.WIRE)) {
              dataMap = this.getDataMapForWire(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties);
          } else {
              dataMap = this.getDataMapForCommonAccounts(transaction, transactionType, account, isTransactionTypeIconVisible, transactionProperties);
          }
          dataMap.flxDropdown = {
              "onClick": this.onClickToggle
          };
          var breakpoint = kony.application.getCurrentBreakpoint();
          if(breakpoint === 640){
            dataMap.flxAmount = {
              right : "15dp"
            };
          }
          else {
            var amtLeft = "64%";
            if(breakpoint === 1024)
            	amtLeft = "62%";
			dataMap.flxAmount = {
              	left : amtLeft
            };
          }
          return dataMap;
      },
      /**
       * Method to handle adjust ui for transactions
       * @param {Boolean} isPresent true/false
       */
      adjustUIForTransactions: function(isPresent) {
          if (isPresent) {
              this.view.transactions.flxNoTransactions.isVisible = false;
              this.view.transactions.flxSeparatorNoResults.isVisible = true;
              this.view.transactions.flxSort.isVisible = true;
              if(kony.application.getCurrentBreakpoint() !== 640){
                this.view.transactions.flxSegmentContainer.top = "-40dp";
              }
              else
                this.view.transactions.flxSegmentContainer.top = "0dp";
          } else {
              this.view.transactions.flxNoTransactions.isVisible = true;
              this.view.transactions.flxSeparatorNoResults.isVisible = false;
              this.view.transactions.flxSegmentContainer.top="0dp";
              this.view.transactions.flxSort.isVisible = false;
          }
      },
      /**
       * Method to update transaction segment
       * @param {JSON} transactionDetails transactionDetails like transactions, transactiontype, accounts etc
       */
      updateTransactions: function(transactionDetails) {
          var controller = this;
          var transactions = transactionDetails.transactions;
          var dataInputs = transactionDetails.dataInputs;
          this.transactionType = dataInputs.transactionType;
          if (transactions.length > 0) {
              this.view.transactions.flxPagination.setVisibility(true);
          } else {
              this.view.transactions.flxPagination.setVisibility(false);
          }
          this.adjustUIForTransactions(transactionDetails.transactions.length > 0);
          var createSegmentSection = function(transactions, sectionHeaderText) {
              return [{
                      "lblTransactionHeader": sectionHeaderText,
                      "lblSeparator": "."
                  },
                  transactions.map(controller.createTransactionSegmentModel.bind(this, dataInputs))
              ];
          };
          var pendingTransactionsSection = createSegmentSection(transactionDetails.transactions.filter(this.isPendingTransaction), {
            "text" :kony.i18n.getLocalizedString('i18n.accounts.pending'),
            "skin":"sknLblPendingTransactions"
          });
          var postedTransactionsSection = createSegmentSection(transactionDetails.transactions.filter(this.isSuccessfulTransacion), {
            "text":kony.i18n.getLocalizedString('i18n.accounts.posted'),
            "skin":"sknLblPostedTransactions"
          });
          var transactionsExistInSection = function(section) {
              return section[1] && section[1].length && section[1].length > 0;
          };
          this.view.transactions.segTransactions.widgetDataMap = {
              "btnRepeat": "btnRepeat",
              "lblNoteTitle": "lblNoteTitle",
              "lblNoteValue": "lblNoteValue",
              "lblFrequencyTitle": "lblFrequencyTitle",
              "lblFrequencyValue": "lblFrequencyValue",
              "lblRecurrenceTitle": "lblRecurrenceTitle",
              "lblRecurrenceValue": "lblRecurrenceValue",
              "btnDisputeTransaction": "btnDisputeTransaction",
              "btnViewRequests": "btnViewRequests",
              "imgError": "imgError",
              "imgWarning": "imgWarning",
              "lblDisputedWarning": "lblDisputedWarning",
              "btnEditRule": "btnEditRule",
              "btnPrint": "btnPrint",
              "cbxRememberCategory": "cbxRememberCategory",
              "flxActions": "flxActions",
              "flxActionsWrapper": "flxActionsWrapper",
              "flxAmount": "flxAmount",
              "flxBalance": "flxBalance",
              "flxCategory": "flxCategory",
              "flxDate": "flxDate",
              "flxDescription": "flxDescription",
              "flxDetail": "flxDetail",
              "flxDetailData": "flxDetailData",
              "flxDetailHeader": "flxDetailHeader",
              "flxDropdown": "flxDropdown",
              "flxIdentifier": "flxIdentifier",
              "flxInformation": "flxInformation",
              "flxLeft": "flxLeft",
              "flxMemo": "flxMemo",
              "flxRight": "flxRight",
              "flxSegTransactionHeader": "flxSegTransactionHeader",
              "flxSegTransactionRowSavings": this.transactionsTemplate,
              "flxSegTransactionRowSelected": this.transactionsSelectedTemplate,
              "flxSegTransactionRowWrapper": "flxSegTransactionRowWrapper",
              "flxSelectedRowWrapper": "flxSelectedRowWrapper",
              "flxToData": "flxToData",
              "flxToHeader": "flxToHeader",
              "flxType": "flxType",
              "flxTypeData": "flxTypeData",
              "flxTypeHeader": "flxTypeHeader",
              "flxWithdrawalAmountData": "flxWithdrawalAmountData",
              "flxWithdrawalAmountHeader": "flxWithdrawalAmountHeader",
              "flxWrapper": "flxWrapper",
              "imgCategoryDropdown": "imgCategoryDropdown",
              "imgDropdown": "imgDropdown",
              "imgType": "imgType",
              "lblType": "lblType",
              "lblAmount": "lblAmount",
              "lblBalance": "lblBalance",
              "lblCategory": "lblCategory",
              "lblDate": "lblDate",
              "lblDescription": "lblDescription",
              "lblIdentifier": "lblIdentifier",
              "lblSeparator": "lblSeparator",
              "lblSeparatorActions": "lblSeparatorActions",
              "lblSeparatorDetailData": "lblSeparatorDetailData",
              "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
              "lblToTitle": "lblToTitle",
              "lblToValue": "lblToValue",
              "lblTransactionHeader": "lblTransactionHeader",
              "lblTypeTitle": "lblTypeTitle",
              "lblTypeValue": "lblTypeValue",
              "lblWithdrawalAmountTitle": "lblWithdrawalAmountTitle",
              "lblWithdrawalAmountValue": "lblWithdrawalAmountValue",
              "lblCurrencyAmountTitle":"lblCurrencyAmountTitle",
              "lblCurrencyAmountValue":"lblCurrencyAmountValue",
              "txtMemo": "txtMemo",
              "CopyflxToHeader0g61ceef5594d41": "CopyflxToHeader0g61ceef5594d41",
              "CopylblToTitle0a2c47b22996e4f": "CopylblToTitle0a2c47b22996e4f",
              "flxBankName1": "flxBankName1",
              "flxBankName2": "flxBankName2",
              "flxCash": "flxCash",
              "flxCheck1": "flxCheck1",
              "flxCheck1Ttitle": "flxCheck1Ttitle",
              "flxCheck2": "flxCheck2",
              "flxCheck2Ttitle": "flxCheck2Ttitle",
              "flxCheckImage": "flxCheckImage",
              "flxCheckImage2Icon": "flxCheckImage2Icon",
              "flxCheckImageIcon": "flxCheckImageIcon",
              "flxRememberCategory": "flxRememberCategory",
              "flxSegCheckImages": this.checkImagesTemplate,
              "flxTotal": "flxTotal",
              "flxTotalValue": "flxTotalValue",
              "flxTransactionFee":"flxTransactionFee",
              "flxFrequencyTitle":"flxFrequencyTitle",
              "flxRecurrenceTitle":"flxRecurrenceTitle",
              "flxCurrency":"flxCurrency",
              "flxWithdrawalAmount": "flxWithdrawalAmount",
              "flxWithdrawalAmountCash": "flxWithdrawalAmountCash",
              "flxWithdrawalAmountCheck1": "flxWithdrawalAmountCheck1",
              "flxWithdrawalAmountCheck2": "flxWithdrawalAmountCheck2",
              "imgCheckimage": "imgCheckimage",
              "imgCheckImage1Icon": "imgCheckImage1Icon",
              "imgCheckImage2Icon": "imgCheckImage2Icon",
              "imgRememberCategory": "imgRememberCategory",
              "lblTransactionFeeKey":"lblTransactionFeeKey",
              "lblTransactionFeeValue":"lblTransactionFeeValue",
              "lblBankName1": "lblBankName1",
              "lblBankName2": "lblBankName2",
              "lblCheck1Ttitle": "lblCheck1Ttitle",
              "lblCheck2Ttitle": "lblCheck2Ttitle",
              "lblRememberCategory": "lblRememberCategory",
              "lblSeparator2": "lblSeparator2",
              "lblSeperatorhor1": "lblSeperatorhor1",
              "lblSeperatorhor2": "lblSeperatorhor2",
              "lblSeperatorhor3": "lblSeperatorhor3",
              "lblSeperatorhor4": "lblSeperatorhor4",
              "lblSeperatorhor5": "lblSeperatorhor5",
              "lblTotalValue": "lblTotalValue",
              "lblWithdrawalAmount": "lblWithdrawalAmount",
              "lblWithdrawalAmountCash": "lblWithdrawalAmountCash",
              "lblWithdrawalAmountCheck1": "lblWithdrawalAmountCheck1",
              "lblWithdrawalAmountCheck2": "lblWithdrawalAmountCheck2",
              "segCheckImages": "segCheckImages",
              "txtFieldMemo": "txtFieldMemo",
             "lblToTitle2":"lblToTitle2",
             "lblTypeTitle2":"lblTypeTitle2",
             "lblWithdrawalAmountTitle2":"lblWithdrawalAmountTitle2"
          };
          this.view.transactions.segTransactions.setData([pendingTransactionsSection, postedTransactionsSection].filter(transactionsExistInSection));
          this.view.transactions.forceLayout();
          FormControllerUtility.hideProgressBar(this.view);
          this.AdjustScreen();
      },
      /**
       * Method to show searched transactions
       * @param {JSON} searchUIData Contains transactions and accounts inside datainputs
       */
      showSearchedTransaction: function(searchUIData) {
          var controller = this;
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          this.highlightTransactionType(OLBConstants.ALL);
          this.view.transactions.flxPagination.setVisibility(false);
          var transactions = searchUIData.transactions;
          var dataInputs = searchUIData.dataInputs;
          
          this.adjustUIForTransactions(transactions.length > 0);
          var createSegmentSection = function(transactions, sectionHeaderText) {
              return [{
                      "lblTransactionHeader": sectionHeaderText,
                      "lblSeparator": "."
                  },
                  transactions.map(controller.createTransactionSegmentModel.bind(this, dataInputs))
              ];
          };
          var pendingTransactionsSection = createSegmentSection(transactions.filter(this.isPendingTransaction), {
            "text":kony.i18n.getLocalizedString('i18n.accounts.pending'),
            "skin":"sknLblPendingTransactions" 
          });
          var postedTransactionsSection = createSegmentSection(transactions.filter(this.isSuccessfulTransacion), {
            "text":kony.i18n.getLocalizedString('i18n.accounts.posted'),
            "skin":"sknLblPostedTransactions"
          });
          var transactionsExistInSection = function(section) {
              return section[1] && section[1].length && section[1].length > 0;
          };
          this.view.transactions.segTransactions.setData([pendingTransactionsSection, postedTransactionsSection].filter(transactionsExistInSection));
          this.view.transactions.forceLayout();
          FormControllerUtility.hideProgressBar(this.view);
          this.AdjustScreen();
      },
      /**
       *  Method to count tag state
       */
      tagState: {
          visible: 0,
          NUMBER_OF_TAGS: 5,
          decrement: function() {
              if (this.visible > 0) {
                  this.visible--;
              }
          },
          increment: function() {
              if (this.visible < this.NUMBER_OF_TAGS) {
                  this.visible++;
              }
          }
      },
      
 getTagConfig : function(searchViewModel) {
   var scopeObj = this;
   var config;
   var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
   if( kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
     config =  [{
       actionOn: 'flxCancelKeywordM',
       hide: ['flxKeywordWrapper'],
       clearPropertiesFromViewModel: [{
         propertyName: 'keyword',
         resetValue: ''
       }],
       value: {
         label: 'lblKeywordValueM',
         computedValue: function() {
           if (searchViewModel.keyword === "") {
             return null;
           }
           return searchViewModel.keyword;
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelTypeM',
       hide: ['flxTypeWrapper'],
       clearPropertiesFromViewModel: [{
         propertyName: 'transactionTypeSelected',
         resetValue: OLBConstants.BOTH
       }],
       value: {
         label: 'lblTypeValueM',
         computedValue: function() {
           if (searchViewModel.transactionTypeSelected === OLBConstants.BOTH) {
             return null;
           }
           return kony.i18n.getLocalizedString(this.transactionTypes[searchViewModel.transactionTypeSelected]);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelAmountRangeM',
       hide: ['flxAmountRangeWrapper'],
       clearPropertiesFromViewModel: [{
         propertyName: 'fromAmount',
         resetValue: ''
       }, {
         propertyName: 'toAmount',
         resetValue: ''
       }],
       value: {
         label: 'lblAmountRangeValueM',
         computedValue: function() {
           if (searchViewModel.fromAmount === "" || searchViewModel.toAmount === "") {
             return null;
           }
           return CommonUtilities.formatCurrencyWithCommas(searchViewModel.fromAmount) + " to " + CommonUtilities.formatCurrencyWithCommas(searchViewModel.toAmount);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelDateRangeM',
       hide: ['flxDateRangeWrapper'],
       clearPropertiesFromViewModel: [{
         propertyName: 'timePeriodSelected',
         resetValue: OLBConstants.ANY_DATE
       }],
       value: {
         label: 'lblDateRangeValueM',
         computedValue: function() {
           if (searchViewModel.timePeriodSelected === OLBConstants.ANY_DATE) {
             return null;
           } else if (searchViewModel.timePeriodSelected === OLBConstants.CUSTOM_DATE_RANGE) {
             var fromDate=this.view.transactions.calDateFrom.date;
             var toDate=this.view.transactions.calDateTo.date;
             return fromDate + " " + kony.i18n.getLocalizedString("i18n.transfers.lblTo") + " " + toDate;
           }
           return kony.i18n.getLocalizedString(this.timePeriods[searchViewModel.timePeriodSelected]);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelCheckNumberM',
       hide: ['flxCheckNumberWrapper'],
       clearPropertiesFromViewModel: [{
         propertyName: 'fromCheckNumber',
         resetValue: ''
       }, {
         propertyName: 'toCheckNumber',
         resetValue: ''
       }],
       value: {
         label: 'lblCheckNumberValueM',
         computedValue: function() {
           if (searchViewModel.fromCheckNumber === "" || searchViewModel.toCheckNumber === "") {
             return null;
           }
           return searchViewModel.fromCheckNumber + " " + kony.i18n.getLocalizedString("i18n.transfers.lblTo") + " " + searchViewModel.toCheckNumber;
         }.bind(scopeObj)
       }
     }];     
   }
   else {
     config =  [{
       actionOn: 'flxCancelKeyword',
       hide: ['lblKeywordTitle', 'lblKeywordValue'],
       clearPropertiesFromViewModel: [{
         propertyName: 'keyword',
         resetValue: ''
       }],
       value: {
         label: 'lblKeywordValue',
         computedValue: function() {
           if (searchViewModel.keyword === "") {
             return null;
           }
           return searchViewModel.keyword;
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelType',
       hide: ['lblTypeValue', 'lblTypeTitle'],
       clearPropertiesFromViewModel: [{
         propertyName: 'transactionTypeSelected',
         resetValue: OLBConstants.BOTH
       }],
       value: {
         label: 'lblTypeValue',
         computedValue: function() {
           if (searchViewModel.transactionTypeSelected === OLBConstants.BOTH) {
             return null;
           }
           return kony.i18n.getLocalizedString(this.transactionTypes[searchViewModel.transactionTypeSelected]);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelAmountRange',
       hide: ['lblAmountRangeTitle', 'lblAmountRangeValue'],
       clearPropertiesFromViewModel: [{
         propertyName: 'fromAmount',
         resetValue: ''
       }, {
         propertyName: 'toAmount',
         resetValue: ''
       }],
       value: {
         label: 'lblAmountRangeValue',
         computedValue: function() {
           if (searchViewModel.fromAmount === "" || searchViewModel.toAmount === "") {
             return null;
           }
           return CommonUtilities.formatCurrencyWithCommas(searchViewModel.fromAmount) + " to " + CommonUtilities.formatCurrencyWithCommas(searchViewModel.toAmount);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelDateRange',
       hide: ['lblDateRangeTitle', 'lblDateRangeValue'],
       clearPropertiesFromViewModel: [{
         propertyName: 'timePeriodSelected',
         resetValue: OLBConstants.ANY_DATE
       }],
       value: {
         label: 'lblDateRangeValue',
         computedValue: function() {
           if (searchViewModel.timePeriodSelected === OLBConstants.ANY_DATE) {
             return null;
           } else if (searchViewModel.timePeriodSelected === OLBConstants.CUSTOM_DATE_RANGE) {
             var fromDate=this.view.transactions.calDateFrom.date;
             var toDate=this.view.transactions.calDateTo.date;
             return fromDate + " " + kony.i18n.getLocalizedString("i18n.transfers.lblTo") + " " + toDate;
           }
           return kony.i18n.getLocalizedString(this.timePeriods[searchViewModel.timePeriodSelected]);
         }.bind(scopeObj)
       }
     }, {
       actionOn: 'flxCancelCheckNumber',
       hide: ['lblCheckNumberTitle', 'lblCheckNumberValue'],
       clearPropertiesFromViewModel: [{
         propertyName: 'fromCheckNumber',
         resetValue: ''
       }, {
         propertyName: 'toCheckNumber',
         resetValue: ''
       }],
       value: {
         label: 'lblCheckNumberValue',
         computedValue: function() {
           if (searchViewModel.fromCheckNumber === "" || searchViewModel.toCheckNumber === "") {
             return null;
           }
           return searchViewModel.fromCheckNumber + " " + kony.i18n.getLocalizedString("i18n.transfers.lblTo") + " " + searchViewModel.toCheckNumber;
         }.bind(scopeObj)
       }
     }];
   }
   
   return config;
 }, 
      
      setFlxSearchResultsHeight :  function( numberOfVisibleTags ) {
        if( numberOfVisibleTags == 0)return;
        
        var x = (numberOfVisibleTags * 22) + (numberOfVisibleTags * 10);
        var y =  x + 37;
        var z = y + 43;
        
        this.view.transactions.flxSearchItemsMobile.height = x + "dp";
        this.view.transactions.flxWrapper.height = y + "dp";
        this.view.transactions.flxSearchResults.height = z + "dp";
        
        this.AdjustScreen();
      },    
      /**
       * Method to show searched tags
       * @param {Object} searchViewModel search view model
       */
      configureActionsForTags: function(searchViewModel) {
          var scopeObj = this;
          var tagConfig = scopeObj.getTagConfig(searchViewModel);

          function generateClickListenerForTag(config) {
              return function() {
                  hideTag(config);
                  scopeObj.tagState.decrement();
                  config.clearPropertiesFromViewModel.forEach(function(property) {
                    scopeObj.searchViewModel[property.propertyName] = property.resetValue;
                  });
                  FormControllerUtility.showProgressBar(this.view);
                  if (scopeObj.tagState.visible === 0) {
                      scopeObj.presenter.showAccountDetails();
                  } else {
                      scopeObj.startSearch();
                  }
                  scopeObj.view.transactions.forceLayout();
              };
          }

          function hideTag(config) {
              scopeObj.view.transactions[config.actionOn].setVisibility(false);
              config.hide.forEach(function(widgetToHide) {
                  scopeObj.view.transactions[widgetToHide].setVisibility(false);
              });
              scopeObj.view.transactions.forceLayout();
          }

          function showTag(config) {
              scopeObj.view.transactions[config.actionOn].setVisibility(true);
              config.hide.forEach(function(widgetToHide) {
                  scopeObj.view.transactions[widgetToHide].setVisibility(true);
              });
              scopeObj.view.transactions.forceLayout();
              scopeObj.tagState.increment();
          }
          this.tagState.visible = 0;
          tagConfig.forEach(function(config) {
              if (config.value.computedValue() === null) {
                  hideTag(config);
              } else {
                  showTag(config);
                  scopeObj.view.transactions[config.actionOn].onClick = generateClickListenerForTag(config);
                  scopeObj.view.transactions[config.value.label].text = config.value.computedValue();
              }
          });
          if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile ) {
            scopeObj.setFlxSearchResultsHeight(scopeObj.tagState.visible);
          }
          this.AdjustScreen();
      },
    /**
     * showNoMoreRecords - Handles zero records scenario in navigation.
     */
    showNoMoreRecords : function(){
        this.view.transactions.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
        this.view.transactions.flxPaginationNext.setEnabled(false);
        alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
        FormControllerUtility.hideProgressBar(this.view);
    },

      /**
       * Method to update Pagination Bar
       * @param {Object} transactionDetails transaction details like pagination, transactions etc
       */
      updatePaginationBar: function(transactionDetails) {
          var pagination = transactionDetails.pagination;
          var dataInputs = transactionDetails.dataInputs;
          dataInputs.resetSorting = false;
          var account = dataInputs.account;
          this.view.transactions.lblPagination.text = (pagination.offset + 1) + " - " + (pagination.offset + pagination.limit) + " " + kony.i18n.getLocalizedString('i18n.common.transactions');
          this.view.transactions.flxPaginationPrevious.onClick = this.loadAccountModule().presentationController.fetchPreviousTransactions.bind(this.loadAccountModule().presentationController, account, dataInputs);
          this.view.transactions.flxPaginationNext.onClick = this.loadAccountModule().presentationController.fetchNextTransactions.bind(this.loadAccountModule().presentationController, account, dataInputs);
          this.view.transactions.imgPaginationFirst.isVisible = false;
          this.view.transactions.imgPaginationLast.isVisible = false;
          if (pagination.offset >= pagination.paginationRowLimit) {
              this.view.transactions.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
              this.view.transactions.flxPaginationPrevious.setEnabled(true);
          } else {
              this.view.transactions.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
              this.view.transactions.flxPaginationPrevious.setEnabled(false);
          }
          if (pagination.limit < pagination.paginationRowLimit) {
              this.view.transactions.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
              this.view.transactions.flxPaginationNext.setEnabled(false);
          } else {
              this.view.transactions.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
              this.view.transactions.flxPaginationNext.setEnabled(true);
          }
          this.AdjustScreen();
      },
      /**
       * Method to show E-Statements
       * @param {JSON} account account for which e-statements are required
       */
      showViewStatements: function(account) {
          this.view.downloadTransction.lblHeader.text = kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS");
          var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
          var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
          var text3 = kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS");
          this.view.breadcrumb.setBreadcrumbData([{
              text: text1
          }, {
              text: text2,
              callback: this.loadAccountModule().presentationController.showAccountDetails.bind(this.loadAccountModule().presentationController, account)
          }, {
              text: text3
          }]);
          this.view.ViewStatements.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.GoToAccountSummary");
          this.view.ViewStatements.confirmButtons.btnConfirm.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails"));
          this.view.flxHeader.setVisibility(true);
          this.view.flxMainWrapper.setVisibility(true);
          this.view.flxMain.setVisibility(true);
          this.view.flxLogout.setVisibility(true);
          this.view.CustomPopup.lblHeading.setFocus(true);
          this.view.flxFooter.setVisibility(true);
          this.view.breadcrumb.setVisibility(false);
          this.view.flxViewStatements.setVisibility(true);
          this.view.downloadTransction.lblPickDateRange.setVisibility(true);
          this.view.downloadTransction.lblTo.setVisibility(true);
          this.view.downloadTransction.flxFromDate.setVisibility(true);
          this.view.downloadTransction.flxToDate.setVisibility(true);
          this.view.flxEditRule.setVisibility(false);
          this.view.flxCheckImage.setVisibility(false);
          this.view.flxAccountTypesAndInfo.setVisibility(false);
          this.view.flxAccountSummaryAndActions.setVisibility(false);
          this.view.flxTransactions.setVisibility(false);
          this.view.accountTypes.setVisibility(false);
          this.view.moreActions.setVisibility(false);          
          this.view.accountActionsMobile.setVisibility(false);    
          this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_DOWN;
          this.view.moreActionsDup.setVisibility(false);
          this.view.flxDownloadTransaction.setVisibility(false);
          this.view.ViewStatements.Segment0d986ba7141b544.setData([]);
          this.format = "pdf";
          this.view.ViewStatements.forceLayout();
          this.AdjustScreen();
      },
      /**
       * map function for months
       * @param {Number} month month number
       * @returns {String} Month
       */
      fetchMonth: function(month) {
          var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
          switch (month) {
              case 1:
                  return OLBConstants.MONTHS_FULL.January;
              case 2:
                  return OLBConstants.MONTHS_FULL.February;
              case 3:
                  return OLBConstants.MONTHS_FULL.March;
              case 4:
                  return OLBConstants.MONTHS_FULL.April;
              case 5:
                  return OLBConstants.MONTHS_FULL.May;
              case 6:
                  return OLBConstants.MONTHS_FULL.June;
              case 7:
                  return OLBConstants.MONTHS_FULL.July;
              case 8:
                  return OLBConstants.MONTHS_FULL.August;
              case 9:
                  return OLBConstants.MONTHS_FULL.September;
              case 10:
                  return OLBConstants.MONTHS_FULL.October;
              case 11:
                  return OLBConstants.MONTHS_FULL.November;
              case 12:
                  return OLBConstants.MONTHS_FULL.December;
              default:
                  return "None";
          }
      },
      /**
       * Populates month in view Statements flex basing on year, accountID
       */
      setMonthsData: function() {
          var noOfMonths = 0;
          var rows = 1;
          var year = parseInt(this.view.ViewStatements.lblSelectYear.selectedKey || this.view.ViewStatements.lblSelectYear.masterData[0][1]);
          var date = new Date();
          var month = date.getMonth();
          if (year === date.getFullYear()) {
              noOfMonths = month;
          } else {
              noOfMonths = 12;
          }
          var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
          var statementsWidgetDataMap = {
              "btnStatement1": "btnStatement1",
              "btnStatement2": "btnStatement2",
              "flxMonthStatements": "flxMonthStatements",
              "lblMonth1": "lblMonth1",
              "lblMonth2": "lblMonth2",
              "lblSeparator": "lblSeparator"
          };
          var data = [];
          this.view.ViewStatements.Segment0d986ba7141b544.widgetDataMap = statementsWidgetDataMap;
          for (var i = noOfMonths; rows < 7 && i>0; i--) {
              var rightMonth = i - 6;
              var month1 = (this.fetchMonth(i));//.toUpperCase();
              var month2 = (this.fetchMonth(rightMonth));//.toUpperCase();
              var list = {
                  "btnStatement1": {
                      "text": month1 + " " + year,
                      "onClick": this.loadAccountModule().presentationController.showDownloadStatementScreen.bind(this.loadAccountModule().presentationController, accountID, year, i, this.format)
                  },
                  "btnStatement2": {
                      "text": month2 + " " + year,
                      "isVisible": rightMonth <= 0 ? false : true,
                      "onClick": this.loadAccountModule().presentationController.showDownloadStatementScreen.bind(this.loadAccountModule().presentationController, accountID, year, rightMonth, this.format)
                  },
                  "flxMonthStatements": "flxMonthStatements",
                  "lblMonth1": this.fetchMonth(i),
                  "lblMonth2": {
                      "text": this.fetchMonth(rightMonth),
                      "isVisible": rightMonth <= 0 ? false : true
                  },
                  "lblSeparator": "lblSeparator"
              };
              data.push(list);
              rows++;
          }
          this.view.ViewStatements.Segment0d986ba7141b544.setData(data);
          this.view.ViewStatements.forceLayout();
          this.AdjustScreen();
      },
      /**
       * populating years to listbox
       */
      setYearsToListBox: function() {
          var date = new Date();
          var yy = date.getMonth() === 0 ? date.getFullYear()-1 : date.getFullYear(); 
          var list = [];
          for (var i = 0; i < 2; i++, yy--) {
              var tempList = [];
              tempList.push(yy);
              tempList.push(yy);
              list.push(tempList);
          }
          this.view.ViewStatements.lblSelectYear.masterData = list;
          this.view.ViewStatements.lblSelectYear.onSelection = this.setMonthsData;
      },
      /**
       * Method to show accounts for selection
       * @param {JSON} presentAccounts present accounts
       * @returns {Object} List of accounts
       */
      showAccountsForSelection: function(presentAccounts) {
          var list = [];
          for (var i = 0; i < presentAccounts.length; i++) {
              var tempList = [];
              tempList.push(presentAccounts[i].accountID);
              var name = CommonUtilities.getAccountDisplayName(presentAccounts[i]);
              tempList.push(name);
              list.push(tempList);
          }
          return list;
      },
      /**
       * On selection of new account from list box
       */
      onSelectionOfNewAccount: function() {
          var date = new Date();
        //   var year = date.getFullYear();
        //   this.view.ViewStatements.lblSelectYear.selectedKey = year;
          this.setMonthsData();
      },
      /**
       * Method to update accounts list
       * @param {Object} uiData list of accounts
       */
      updateListBox: function(uiData) {
          var allAccounts = uiData.allAccounts;
          var currentAccount = uiData.account;
          this.view.ViewStatements.lstSelectAccount.masterData = this.showAccountsForSelection(allAccounts);
          this.view.ViewStatements.lstSelectAccount.selectedKey = currentAccount.accountID;
          this.view.ViewStatements.lstSelectAccount.onSelection = this.onSelectionOfNewAccount;
          this.view.ViewStatements.forceLayout();
      },
      /**
       * Method to Init e-statements view
       * @param {Object} uiData UI data containing current account and all accounts
       */
      initViewStatements: function(uiData) {
          var scopeObj = this;
          this.setYearsToListBox();
          this.updateListBox(uiData);
          this.view.ViewStatements.confirmButtons.btnModify.onClick = function(){
            scopeObj.loadAccountModule().presentationController.showAccountsDashboard();
          }.bind(this)
          this.view.ViewStatements.confirmButtons.btnConfirm.onClick = function(){
            var accountID = scopeObj.view.ViewStatements.lstSelectAccount.selectedKey;
            scopeObj.loadAccountModule().presentationController.fetchUpdatedAccountDetails(accountID);
          }.bind(this)
          this.setMonthsData();
          FormControllerUtility.hideProgressBar(this.view);
          this.AdjustScreen();
      },
      /**
       * Method to download any given URL data
       * @param {String} downloadUrl Download URL
       */
      downloadUrl: function(downloadUrl) {
          var data = {
              "url": downloadUrl
          };
          CommonUtilities.downloadFile(data);
          FormControllerUtility.hideProgressBar(this.view);
      },
      /**
      * onBreakpointChange : Handles ui changes on .
      * @member of {frmAccountDetailsController}
      * @param {integer} width - current browser width
      * @return {} 
      * @throws {}
      */
      detailsButtonText: "",
      flagDetailsBtn: false,
      onBreakpointChange: function(width) {
        kony.print('on breakpoint change');
        orientationHandler.onOrientationChange(this.onBreakpointChange);
        this.view.customheader.onBreakpointChange(width);
        this.setupFormOnTouchEnd(width);

        this.view.moreActions.setVisibility(false);
        this.view.accountActionsMobile.setVisibility(false);
        var scope = this;
        var responsiveFonts = new ResponsiveFonts();
        this.view.flxSecondaryActions.zindex = 100;
        if(this.flagDetails === false){
          this.detailsButtonText = this.view.accountSummary.btnBalanceDetails.text;
          this.flagDeatils = true;
        }

        if (width === 640|| orientationHandler.isMobile) {
          this.detailsButtonText = this.view.accountSummary.btnBalanceDetails.text;
          this.view.accountSummary.btnBalanceDetails.text = "Details";
          responsiveFonts.setMobileFonts();
          this.view.flxActions.isVisible = false;
          this.view.transactions.flxSort.height = "0dp";
          this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
          this.view.btnSecondaryActionsMobile.zIndex = 100;
          this.view.btnSecondaryActionsMobile.isVisible = true;
          this.view.transactions.flxTabsChecking.skin = "CopysknFlxffffffBorder1";
          this.view.transactions.flxTabsCredit.skin = "CopysknFlxffffffBorder1";
          this.view.transactions.flxTabsDeposit.skin = "CopysknFlxffffffBorder1";
          this.view.transactions.flxTabsLoan.skin = "CopysknFlxffffffBorder1";
          this.view.transactions.flxSegmentContainer.skin = "sknflxBordere3e3e3";
          this.transactionsTemplate = "flxSegTransactionsRowSavingsMobile";
          this.transactionsSelectedTemplate = "flxSegTransactionRowSelectedMobile";
          this.checkImagesTemplate = "flxSegCheckImagesMobile";
          var data = this.view.transactions.segTransactions.data;
          if(data == undefined) return;
          for(var i=0; i<data.length; i++){
            data[i][1].map(function(e){
              if(e.template==="flxSegCheckImages"){
                e.lblToTitle2 = e.lblToTitle;
                e.lblTypeTitle2 = e.lblTypeTitle;
                e.lblWithdrawalAmountTitle2 = e.lblWithdrawalAmountTitle;
                e.template=scope.checkImagesTemplate;
              }else{
                e.lblToTitle2 = e.lblToTitle;
                e.lblTypeTitle2 = e.lblTypeTitle;
                e.lblWithdrawalAmountTitle2 = e.lblWithdrawalAmountTitle;
                e.template = scope.transactionsTemplate;
                e.imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_DOWN
              }
            });
          }
          this.view.transactions.segTransactions.setData(data);
        }else if(width === 1024 || width === 1366 || width === 1380){
          this.detailsButtonText = this.view.accountSummary.btnBalanceDetails.text
          responsiveFonts.setDesktopFonts();
          this.view.flxActions.isVisible = true;
          this.view.transactions.flxSort.height = "40dp";
          this.view.transactions.flxTabsChecking.skin = "CopysknFlxffffffShadowdddcdcTopBorder0fff39127a66a46";
          this.view.transactions.flxTabsCredit.skin = "CopysknFlxffffffShadowdddcdcTopBorder0fff39127a66a46";
          this.view.transactions.flxTabsDeposit.skin = "CopysknFlxffffffShadowdddcdcTopBorder0fff39127a66a46";
          this.view.transactions.flxTabsLoan.skin = "CopysknFlxffffffShadowdddcdcTopBorder0fff39127a66a46";
          this.view.transactions.flxSegmentContainer.skin = "sknFlxffffffShadowdddcdcBottomRadius";
          this.view.customheader.lblHeaderMobile.text = "";
          this.view.btnSecondaryActionsMobile.isVisible = false;
          this.transactionsTemplate = "flxSegTransactionRowSavings";
          this.transactionsSelectedTemplate = "flxSegTransactionRowSelected";
          this.checkImagesTemplate = "flxSegCheckImages";
          var data = this.view.transactions.segTransactions.data;
          if(data == undefined) return;
          for(var i=0; i<data.length; i++){
            data[i][1].map(function(e){
              if(e.template==="flxSegCheckImagesMobile"){
                e.lblToTitle2 = e.lblToTitle;
                e.lblTypeTitle2 = e.lblTypeTitle;
                e.lblWithdrawalAmountTitle2 = e.lblWithdrawalAmountTitle;
                e.template = scope.checkImagesTemplate
              }else{
                e.lblToTitle2 = e.lblToTitle;
                e.lblTypeTitle2 = e.lblTypeTitle;
                e.lblWithdrawalAmountTitle2 = e.lblWithdrawalAmountTitle;
                e.template = scope.transactionsTemplate;
                e.imgDropdown = ViewConstants.FONT_ICONS.CHEVRON_DOWN
              }
            });
          }
          this.view.transactions.segTransactions.setData(data);
        }
        this.AdjustScreen();
      },
      setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();   
              } 
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
        this.view.flxAccountTypes.onTouchStart = null;
        this.view.flxSecondaryActions.onTouchStart = null;
      },
      /**
      * showMobileActions : Mobile only dialog for actions on accounts
      * @member of {frmAccountDetailsController}
      * @param {}
      * @return {} 
      * @throws {}
      */
      showMobileActions: function(){
        if(flag == 1){
          this.view.accountActionsMobile.isVisible = false;
          flag = 0;
          this.view.forceLayout();
          return;
        }
          flag = 1;
          var data = [];
          var primary = this.view.flxPrimaryActions.widgets();
          for(var i=0; i<primary.length; i+=2){
            if(primary[i].isVisible){
              var temp = {
                "flxAccountTypes":{
                  "onClick": primary[i].onClick
                },
                "lblUsers":{
                  "text": primary[i].text,
                  "tooltip": primary[i].text
                }
              };
              data.push(temp);
            }
          }
          var secondaryActions = this.view.moreActions.segAccountTypes.data;
          secondaryActions.forEach(function(e){
            data.push(e);
          });
          if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile){
            data = data.filter(function(value, index, arr){
              return value["lblUsers"]["text"] !== kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings");
            });
          }
          this.view.accountActionsMobile.segAccountTypes.setData(data);
          this.view.accountActionsMobile.isVisible = true;
          this.view.accountActionsMobile.imgToolTip.setFocus(true);
        
        this.AdjustScreen();
      },
      showTabletActions: function(){
        if(this.view.accountActionsMobile.isVisible){
          this.view.accountActionsMobile.isVisible = false;
          this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_DOWN;
        }else{
          var data = [];
          var primary = this.view.flxPrimaryActions.widgets();
          if(primary[2].isVisible){
            var temp = {
              "flxAccountTypes":{
                onClick : primary[2].onClick
              },
              "lblUsers":{
                "text" : primary[2].text,
                "tooltip":primary[2].text
              }
            }
            data.push(temp);
          }
          var secondaryActions = this.view.moreActions.segAccountTypes.data;
          secondaryActions.forEach(function(e){
              if (e.lblUsers.text !== primary[2].text) {
                  data.push(e);
              }
          });
          this.view.accountActionsMobile.segAccountTypes.setData(data);
          this.view.accountActionsMobile.right = "28dp";
          this.view.accountActionsMobile.width = this.view.flxSecondaryActions.frame.width+"dp";
          this.view.accountActionsMobile.isVisible = true;
          this.view.imgSecondaryActions.src = ViewConstants.IMAGES.ARRAOW_UP;
          this.view.accountActionsMobile.imgToolTip.setFocus(true);
        }
      }

  };
});