define(['CommonUtilities', 'OLBConstants'],function(CommonUtilities,OLBConstants){

    return{
        
        preShow: function () {
            
        },
        postShow: function () {
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        
        /**
         * Method to start binding
         * @param {JsonObject} viewModel, model to display data
         */
    updateFormUI: function (viewModel) {
        if(viewModel.printData)this.updatePrintData(viewModel.printData)
    },

    updatePrintData: function(viewModel){
        this.setDataForTransaction(viewModel.transactions);
        this.setAccountDetailsInformation(viewModel.accountDetailsModel, viewModel.accountDisplayName);
        this.view.forceLayout();
        window.print();
        this.loadAccountsDetails();
    },

  	/**
     * Method to set Account Details Information
     * @param {JsonObject} accountDetailsModel, account model
     */
    setAccountDetailsInformation: function (accountDetailsModel, accountDisplayName) {
        var accountNumberMask = function(accountNumber) {
            var stringAccNum = '' + accountNumber;
            var isLast4Digits = function(index) {
                return index > (stringAccNum.length - 5);
            };
            return stringAccNum.split('').map(function(c, i) {
                return isLast4Digits(i) ? c : 'X';
            }).join('');
        };
        accountDetailsModel=JSON.parse(JSON.stringify(accountDetailsModel));
        this.view.lblMyCheckingAccount.text = accountDisplayName;
        this.view.lblKonyBank.text = accountDisplayName;

        this.view.lblTransactionsFrom.setVisibility(false);
        this.view.btnBack.setVisibility(false);
        this.view.btnBackBottom.setVisibility(false);

        this.view.keyValueAccHolderName.lblValue.text = JSON.parse(accountDetailsModel.accountHolder).fullname;
        this.view.keyValueAccNumber.lblValue.text = accountNumberMask(accountDetailsModel.accountID);
        this.view.keyValueBank.lblValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.bankName || "";

        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)) {

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.availableBalance');

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.currentBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.pendingDeposit,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.pendingDeposit');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.pendingWithdrawal,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.pendingWithdrawals');
        }
        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)) {

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableCredit,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.availableCredit');

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.currentBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentAmountDue,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.currentDueAmount');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.creditLimit,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.creditLimit');
        }
        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)) {

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.outsandingBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString("i18n.accounts.outstandingBalance");

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.principalBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.principalBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.principalAmount,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.principalAmount');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentAmountDue,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.currentDueAmount');

        }
        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)) {

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.availableBalance');

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.currentBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.interestEarned,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.interestEarned');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.maturityDate,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.maturityDate');
        }
        
        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING) || accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CURRENT)) {

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.availableBalance');

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.currentBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.pendingDeposit,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.pendingDeposit');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.pendingWithdrawal,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.pendingWithdrawals');
        }
        if (accountDetailsModel.accountType === applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LINE_OF_CREDIT)) {

            this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString('i18n.accounts.availableBalance');
            this.view.accountSummary.lblAvailableBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableBalance,false,accountDetailsModel.currencyCode);

            this.view.keyValueAvailableBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.availableBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueAvailableBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.availableBalance');

            this.view.keyValueCurrentBalance.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.currentBalance,false,accountDetailsModel.currencyCode);
            this.view.keyValueCurrentBalance.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.currentBalance');

            this.view.keyValuePendingDeposits.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.interestRate,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingDeposits.lblKey.text = kony.i18n.getLocalizedString('i18n.accountDetail.interestRate');

            this.view.keyValuePendingWithdrawal.lblValue.text = CommonUtilities.formatCurrencyWithCommas(accountDetailsModel.regularPaymentAmount,false,accountDetailsModel.currencyCode);
            this.view.keyValuePendingWithdrawal.lblKey.text = "Regular Payment Amount";
        }
        this.view.forceLayout();
    },

	/**
     * setDataForTransaction : Method set data in transaction segment
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} transactions, transactions object
     * @return {}
     * @throws {}
     */  
    setDataForTransaction: function (transactions) {
        var firstTable = transactions[0];
        var secondTable = transactions[1];
        if (firstTable) {
            this.view.lblPendingTransactions.text = firstTable[0].lblTransactionHeader.text;
            this.view.segPendingTransaction.setData(firstTable[1].map(this.createTransactionSegmentsModel));
            this.view.flxPendingTransactions.setVisibility(true);
        } else {
            this.view.flxPendingTransactions.setVisibility(false);
        }
        if (secondTable) {
            this.view.CopylblPendingTransactions0a979ad50321149.text = secondTable[0].lblTransactionHeader.text;
            this.view.segPostedTransactions.setData(secondTable[1].map(this.createTransactionSegmentsModel));
            this.view.flxPostedTransactions.setVisibility(true);
        } else {
            this.view.flxPostedTransactions.setVisibility(false);
        }
    },
	/**
     * createTransactionSegmentsModel : Method to create Transaction SegmentsModel
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} transaction object to map
     * @return {JsonObject} transaction object with segment mapping
     * @throws {}
     */  
    createTransactionSegmentsModel: function (transaction) {
        return {
            lblDate: transaction.lblDate,
            lblDescription: transaction.lblDescription,
            lblType: transaction.lblTypeValue,
            lblCategory: transaction.lblCategory,
            lblAmount: transaction.lblAmount,
            lblBalance: transaction.lblBalance,
            lblSeparator: transaction.lblSeparator
        };
    },
	/**
     * loadAccountsDetails : Method to accounts details
     * @member of {frmPrintTransactionController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    loadAccountsDetails : function(){
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.presentAccountDetails();
    }
}
});