define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274: function AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_g0db6ac9e74540de9db3c0e123f05090: function AS_Image_g0db6ac9e74540de9db3c0e123f05090(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c: function AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_g69571ded423405b9dc951ca48f115e1: function AS_TextField_g69571ded423405b9dc951ca48f115e1(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_e078a62143e14ffa9b70000f80946fa6: function AS_TextField_e078a62143e14ffa9b70000f80946fa6(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_c2223a86ea724db7a29054904f7e4c0e: function AS_TextField_c2223a86ea724db7a29054904f7e4c0e(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_ca3b63cfaebb49329d8e4d7f2ab2a689: function AS_Button_ca3b63cfaebb49329d8e4d7f2ab2a689(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_f67a44d570bf4722b5071962ff199656: function AS_FlexContainer_f67a44d570bf4722b5071962ff199656(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** init defined for frmTransfers **/
    AS_Form_j65feee81194476c97341e7afd896acc: function AS_Form_j65feee81194476c97341e7afd896acc(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmTransfers **/
    AS_Form_b61534baeb4a46729f7a7c0bb24ffab8: function AS_Form_b61534baeb4a46729f7a7c0bb24ffab8(eventobject) {
        var self = this;
        this.initTabsActions()
        this.loadMyPaymentAccountsData();
    },
    /** postShow defined for frmTransfers **/
    AS_Form_f61a533a94744161a486552a0668316d: function AS_Form_f61a533a94744161a486552a0668316d(eventobject) {
        var self = this;
        this.postShowtransfers();
        this.loadMyPaymentAccountsData();
        this.presenter.loadHamburgerTransfers('frmTransfers');
    },
    /** onDeviceBack defined for frmTransfers **/
    AS_Form_e1bde3d3274840838ac1e5cb73816fa3: function AS_Form_e1bde3d3274840838ac1e5cb73816fa3(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmTransfers **/
    AS_Form_j82b124f81af40f0b31ef0ec43e42ebe: function AS_Form_j82b124f81af40f0b31ef0ec43e42ebe(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});