define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_be5c1e4670354b6db0ce974fd81e2912: function AS_FlexContainer_be5c1e4670354b6db0ce974fd81e2912(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnCancelRequests **/
    AS_Button_b24db0c8a5f14bfdac2ba41f89bd9528: function AS_Button_b24db0c8a5f14bfdac2ba41f89bd9528(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        currForm.flxLogoutStopCheckPayment.setVisibility(true);
        currForm.forceLayout();
    },
    /** onClick defined for btnSendAMessage **/
    AS_Button_cd03cbaab92b4b4b9fbb29288ea7823a: function AS_Button_cd03cbaab92b4b4b9fbb29288ea7823a(eventobject, context) {
        var self = this;
        var nav = new kony.mvc.Navigation("frmNotificationsAndMessages");
        nav.navigate();
    }
});