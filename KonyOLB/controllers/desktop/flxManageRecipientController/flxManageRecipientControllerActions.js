define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a39045ca27fd46f7b4b01cb17b66b1e8: function AS_FlexContainer_a39045ca27fd46f7b4b01cb17b66b1e8(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnEdit **/
    AS_Button_ib11bfaf80044d33a4cb32e6f1a98f78: function AS_Button_ib11bfaf80044d33a4cb32e6f1a98f78(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnDelete **/
    AS_Button_d3152d288d2d4401866482334bb801d6: function AS_Button_d3152d288d2d4401866482334bb801d6(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    }
});