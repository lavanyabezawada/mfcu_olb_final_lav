define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_j530cc13caed4d3a886aaaf96a5ee271: function AS_FlexContainer_j530cc13caed4d3a886aaaf96a5ee271(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_ac97bc4b6e334e2d9ab422dd95680583: function AS_FlexContainer_ac97bc4b6e334e2d9ab422dd95680583(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    }
});