define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnDone **/
    AS_Button_j912d2860bbd4e7ebac09789ccdd101c: function AS_Button_j912d2860bbd4e7ebac09789ccdd101c(eventobject) {
        var self = this;
        this.btnAckDoneAction();
    },
    /** preShow defined for frmCustomerFeedbackSurvey **/
    AS_Form_c1a50c3f78094d83905d740169e0e2d3: function AS_Form_c1a50c3f78094d83905d740169e0e2d3(eventobject) {
        var self = this;
        this.preShowCustomerFeedbackSurvey();
    },
    /** postShow defined for frmCustomerFeedbackSurvey **/
    AS_Form_e987f36c12644412b4ec08b9729ce71e: function AS_Form_e987f36c12644412b4ec08b9729ce71e(eventobject) {
        var self = this;
        this.postShowCustomerFeedbackSurvey();
    }
});