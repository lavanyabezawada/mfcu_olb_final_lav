define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxSearch **/
    AS_FlexContainer_eeca1425129f4b568a977fce19d0d67e: function AS_FlexContainer_eeca1425129f4b568a977fce19d0d67e(eventobject) {
        var self = this;
        //this.flxSearchOnClick();
    },
    /** onClick defined for flxSchedulePayment **/
    AS_FlexContainer_h157340439b843c9a13cf324da991d5e: function AS_FlexContainer_h157340439b843c9a13cf324da991d5e(eventobject) {
        var self = this;
        this.presenter.showPayABill([], 'quickAction');
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274: function AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_g0db6ac9e74540de9db3c0e123f05090: function AS_Image_g0db6ac9e74540de9db3c0e123f05090(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c: function AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_g69571ded423405b9dc951ca48f115e1: function AS_TextField_g69571ded423405b9dc951ca48f115e1(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_e078a62143e14ffa9b70000f80946fa6: function AS_TextField_e078a62143e14ffa9b70000f80946fa6(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_c2223a86ea724db7a29054904f7e4c0e: function AS_TextField_c2223a86ea724db7a29054904f7e4c0e(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onClick defined for btnConfirm1 **/
    AS_Button_bb6353e767814b70abc23342b0e1ff52: function AS_Button_bb6353e767814b70abc23342b0e1ff52(eventobject) {
        var self = this;
    },
    /** onClick defined for flxPaginationPrevious **/
    AS_FlexContainer_icf4141b3f6044a69260ef1f8368616e: function AS_FlexContainer_icf4141b3f6044a69260ef1f8368616e(eventobject) {
        var self = this;
        this.paginationPrevious();
    },
    /** onClick defined for flxPaginationNext **/
    AS_FlexContainer_c536c7bd3200405e9dd88750df380111: function AS_FlexContainer_c536c7bd3200405e9dd88750df380111(eventobject) {
        var self = this;
        this.paginationNext();
    },
    /** onClick defined for btnClose **/
    AS_Button_i63195b6fd94464a8a1825d5f2d5af30: function AS_Button_i63195b6fd94464a8a1825d5f2d5af30(eventobject) {
        var self = this;
        this.view.flxBillPayManagePayeeActivity.setVisibility(false);
        this.view.forceLayout();
    },
    /** init defined for frmBillPay **/
    AS_Form_f674c7925b57473a8e5ec9bd29e377b0: function AS_Form_f674c7925b57473a8e5ec9bd29e377b0(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmBillPay **/
    AS_Form_a00e65fb4fc2403794a65587f7be5587: function AS_Form_a00e65fb4fc2403794a65587f7be5587(eventobject) {
        var self = this;
        this.frmBillPayPreShow();
    },
    /** postShow defined for frmBillPay **/
    AS_Form_a293816c47da417ca7c5976e9824e097: function AS_Form_a293816c47da417ca7c5976e9824e097(eventobject) {
        var self = this;
        this.frmBillPayPostShow();
    },
    /** onDeviceBack defined for frmBillPay **/
    AS_Form_b9a7bbef43ca4bfa9534d09d928852a6: function AS_Form_b9a7bbef43ca4bfa9534d09d928852a6(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmBillPay **/
    AS_Form_c0b71de19f3440d790f7522027d43434: function AS_Form_c0b71de19f3440d790f7522027d43434(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});