define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants','OLBConstants'], function (FormControllerUtility, CommonUtilities, ViewConstants,OLBConstants) {
  return {
    /*loadLoanPayModule- function to load LoanPayModule
      * @member of frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} -None
      */
    loadLoanPayModule: function () {
      return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
    },
    /*updateFormUI- function to update View
      * @member of frmPayDueAmountController
      * @param {viewModel} - viewModel
      * @returns {void} - None
      * @throws {void} -None
      */
    updateFormUI: function (viewModel) {
      if (viewModel.ProgressBar) {
        if (viewModel.ProgressBar.show) {
          FormControllerUtility.showProgressBar(this.view);
        }
        else {
          FormControllerUtility.hideProgressBar(this.view);
        }
      }
      if (viewModel.serverError) {
        this.showServerError(viewModel.serverError);
      }
      if (viewModel.sideMenu) {
        this.updateHamburgerMenu(viewModel.sideMenu);
      }
      if (viewModel.loadAccounts) {
        this.accountsListCallback(viewModel.loadAccounts);
      }
      if (viewModel.loanPayoff) {
        this.loanPayOffInit(viewModel.loanPayoff);
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.loanDue) {
        this.loanPayDueAmountInit(viewModel.loanDue);
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.payCompleteMonthlyDue) {
        this.successPayDueAmount(viewModel.payCompleteMonthlyDue.data, viewModel.payCompleteMonthlyDue.referenceId);
      }
      if (viewModel.payCompleteDue) {
        this.successPayment(viewModel.payCompleteDue.data, viewModel.payCompleteDue.referenceId);
      }
      if (viewModel.payOtherAmount) {
        this.successPayOtherAmount(viewModel.payOtherAmount.data, viewModel.payOtherAmount.referenceId);
      }
      if (viewModel.populateAccountData) {
        this.populateFromAccountValues(viewModel.populateAccountData);
      }
      if (viewModel.updateToAccount) {
        this.updateToAccountDetails(viewModel.updateToAccount);
      }
      if (viewModel.updateFromAccount) {
        this.updateAccountDetails(viewModel.updateFromAccount);
      }
      if (viewModel.validateData) {
        this.validateData(viewModel.validateData);
      }
      if (viewModel.newAccountSelection) {
        viewModel.newAccountSelection.principalBalance = CommonUtilities.formatCurrencyWithCommas(viewModel.newAccountSelection.payoffAmount, false, viewModel.newAccountSelection.currencyCode);
        viewModel.newAccountSelection.payOffCharge = CommonUtilities.formatCurrencyWithCommas(viewModel.newAccountSelection.payOffCharge, false, viewModel.newAccountSelection.currencyCode);
        this.setDataToForm(viewModel.newAccountSelection);
      }
      this.view.breadcrumb.btnBreadcrumb2.skin = ViewConstants.SKINS.BREADCRUM;
      this.view.breadcrumb.btnBreadcrumb2.hoverSkin = ViewConstants.SKINS.BREADCRUM;
      this.AdjustScreen();
    },
    /**
      * Show UI for Loan Pay Off
      * @member frmPayDueAmountController
      * @param {data}  data data
      */
    loanPayOffInit: function (data) {
      this.loadAccounts();
      this.view.lblConfirmationTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
      this.view.lblAcknowledgementTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
      this.loanContext = "Loan Payoff";
      this.setDataToForm(data.accounts);
      if (data.fromAccount) {
        this.fromAccount = data.fromAccount;
      }
      this.view.flxDownTimeWarning.setVisibility(false);
      this.view.flxAcknowledgement.setVisibility(false);
      this.view.flxConfirmation.setVisibility(false);
      this.view.LoanPayOff.flxError.setVisibility(false);
      this.view.flxPayDueAmount.setVisibility(false);
      this.view.flxLoanPayOff.setVisibility(true);
      this.view.LoanPayOff.tbxOptional.text = " ";
      this.view.flxTermsAndConditions.setVisibility(true);
      this.view.lblHeading1.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
    },
    /**
     * Show UI for Pay Due Amount Flow
     * @member frmPayDueAmountController
     * @param {data}  data data
     */
    loanPayDueAmountInit: function (data) {
      this.loadAccounts();
      this.setDataToForm(data.accounts);
      this.initActionsLoanDue();
      if (data.fromAccount) {
        this.fromAccount = data.fromAccount;
      }
      this.loanPayDueAmountUI();
      this.loanContext = "Loan Due";
      this.view.forceLayout();
    },
    /**
     * Set UI for Loan Pay Due Amount
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    loanPayDueAmountUI: function () {
      this.view.flxDownTimeWarning.setVisibility(false);
      this.view.flxTermsAndConditions.setVisibility(true);
      this.view.flxAcknowledgement.setVisibility(false);
      this.view.flxConfirmation.setVisibility(false);
      this.view.flxPayDueAmount.setVisibility(true);
      this.view.flxLoanPayOff.setVisibility(false);
      this.view.PayDueAmount.lblDueAmount.isVisible = false;
      this.view.PayDueAmount.tbxOptional.text = " ";
      this.view.lblConfirmationTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanDuePayment");
      this.view.lblAcknowledgementTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanDuePayment");
      this.view.lblHeading.text = kony.i18n.getLocalizedString("i18n.loan.loanDuePayment");
      if (applicationManager.getConfigurationManager().modifyLoanPaymentAmount === "false") {
        this.view.PayDueAmount.flxRadioPayOtherAmount.setVisibility(false);
      }
      else {
        this.view.PayDueAmount.flxRadioPayOtherAmount.setVisibility(true);
      }
      this.showPayDueAmount();
      FormControllerUtility.enableButton(this.view.PayDueAmount.btnPayAmount);
    },
    /**
     * Handle server error, shows serverFlex
     * @member frmPayDueAmountController
     * @param {object} serverError error
     * @returns {void} - None
     * @throws {void} - None
     */
    showServerError: function (serverError) {
      this.view.flxDownTimeWarning.setVisibility(true);
      this.view.rtxDowntimeWarning.setVisibility(true);
      this.view.rtxDowntimeWarning.text = serverError;
      this.view.rtxDowntimeWarning.setFocus(true);
    },
    /**
     * Function initialize actions for Loans flow
     * @member frmPayDueAmountController
     * @param {void}  - None
     * @returns {void} - None
     * @throws {void} - None
     */
    initActionsLoanDue: function () {
      this.view.PayDueAmount.btnPayAmount.onClick = this.payDueAmountPay;
      this.view.PayDueAmount.btnCancel.onClick = this.payDueAmountCancel;
      this.view.PayDueAmount.tbxAmount.onBeginEditing = this.hideAmountError;
      this.view.PayDueAmount.CalendarSendDate.onTouchStart = this.hideCalendarError;
    },
    /**
     * Function to register Actions for Loans
     * @member frmPayDueAmountController
     */
    initActions: function () {
      var scopeObj = this;
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
    
      FormControllerUtility.wrapAmountField(this.view.PayDueAmount.tbxAmount)
        .onKeyUp(this.amountTextboxAction);

      this.view.CustomPopup1.btnNo.onClick = this.quitPopUpNo;
      this.view.CustomPopup1.btnYes.onClick = this.quitPopUpYes;
      this.view.btnViewAccountDetail.onClick = this.backToAccountDetails;
      this.view.btnBackToAccountSummary.onClick = this.loadLoanPayModule().presentationController.backToAccount;
      this.view.btnBackToAccountDeatil.onClick = this.backToAccountDetails;
      this.view.LoanPayOff.listbxTo.onSelection = this.setDataOnNewSelection;
      this.view.LoanPayOff.listbxFrom.onSelection = this.setSkinToListbox.bind(this, scopeObj.view.LoanPayOff.listbxFrom, scopeObj.view.LoanPayOff.flxError);
      this.view.LoanPayOff.flxCheckbox.onClick = this.onSelectionOfCheckbox;
      this.view.LoanPayOff.btnCancel.onClick = this.payDueAmountCancel;
      this.view.CustomPopup.btnYes.onClick = this.performLogout;
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.LoanPayOff.btnCancel.height = "40dp";
      this.view.LoanPayOff.btnConfirm.height = "40dp";
    },
    /**
     * Function to hide Amount for Pay Due Amount flow
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    hideAmountError: function () {
      this.view.PayDueAmount.lblDueAmount.setVisibility(true);
      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);
      CommonUtilities.removeDelimitersForAmount(this.view.PayDueAmount.tbxAmount);
    },
    /**
     * Function to Hide Calendar Error
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    hideCalendarError: function () {
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.left = "50.8%";
      this.view.PayDueAmount.lblDueDate.setVisibility(true);
    },
    /**
     * Function to enable and disable Confirm button for Loan Pay Off
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    onSelectionOfCheckbox: function () {
      if (!CommonUtilities.isChecked(this.view.LoanPayOff.imgCheckbox)) {
        CommonUtilities.setCheckboxState(true, this.view.LoanPayOff.imgCheckbox);
        CommonUtilities.enableButton(this.view.LoanPayOff.btnConfirm);
      }
      else {
        CommonUtilities.setCheckboxState(false, this.view.LoanPayOff.imgCheckbox);
        CommonUtilities.disableButton(this.view.LoanPayOff.btnConfirm);
      }
    },
    /**
     * Function to show view based on parameter
     * @member frmPayDueAmountController
     * @param {Array} views 
     * @returns {void} - None
     * @throws {void} - None
     */
    showView: function (views) {
      this.view.flxMyPaymentAccounts.isVisible = false;
      this.view.flxPayDueAmount.isVisible = false;
      this.view.flxLoanPayOff.isVisible = false;
      this.view.flxPrimaryActions.isVisible = false;
      this.view.flxConfirmation.isVisible = false;
      this.view.flxAcknowledgement.isVisible = false;
      this.view.flxQuitPayment.isVisible = false;
      this.view.flxDownTimeWarning.isVisible = false;
      for (var i = 0; i < views.length; i++) {
        this.view[views[i]].isVisible = true;
      }
      if (kony.application.getCurrentBreakpoint() == 640) {
        this.view.flxPrimaryActions.isVisible = false;
      }
      this.initializeResponsiveViews();
    },
    /**
     * Pre-show for frmPayDueAmount form
     * @member frmPayDueAmountController
     * @param {void} None  
     * @returns {void} - None
     * @throws {void} - None
     */
    frmPayDueAmountPreShow: function () {
      var scopeObj = this;
      applicationManager.getLoggerManager().setCustomMetrics(this, false, "Loans");
      this.view.LoanPayOff.skin = ViewConstants.SKINS.LOANPAYOFF_SKIN;
      this.view.LoanPayOff.clipBounds = false;
      this.view.PayDueAmount.skin = ViewConstants.SKINS.LOANPAYOFF_SKIN;
      this.view.PayDueAmount.clipBounds = false;
      this.view.PayDueAmount.listbxFrom.skin = ViewConstants.SKINS.LOANS_LISTBOX_NOERROR;
      this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
      this.view.PayDueAmount.flxCalender.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
      this.view.confirmation.flxError.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.flxDownTimeWarning.setVisibility(false);
      this.view.PayDueAmount.CalendarSendDate.hidePreviousNextMonthDates = true;
      this.view.LoanPayOff.CalendarPyOn.hidePreviousNextMonthDates = true;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      CommonUtilities.setCheckboxState(false, this.view.LoanPayOff.imgCheckbox);
      CommonUtilities.disableButton(this.view.LoanPayOff.btnConfirm);
      this.view.customheader.forceCloseHamburger();
      this.view.confirmation.lblKeyAmount.text=kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+applicationManager.getConfigurationManager().getCurrencyCode()+")";
      this.view.confirmDialog.lblKeyAmount.text=kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+applicationManager.getConfigurationManager().getCurrencyCode()+")";
      this.initActions();
      applicationManager.getNavigationManager().applyUpdates(this);
    },
    /**
     * Function to change UI when radio buttons are toggled,Pay Due Amount
     * @member frmPayDueAmountController
     * @param {String} obj stores context for Pay Due amount - {Due/Other}
     * @returns {void} - None
     * @throws {void} - None
     */
    payDueAmountRadioButton: function (obj) {
      this.view.PayDueAmount.flxError.setVisibility(false);
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.setVisibility(true);
      this.view.PayDueAmount.listbxFrom.skin = ViewConstants.SKINS.LOANS_LISTBOX_NOERROR;
      this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
      this.view.PayDueAmount.flxCalender.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);

      this.view.PayDueAmount.CalendarSendDate.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();     
      var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
      var endDate  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(todayDate, applicationManager.getFormatUtilManager().getDateFormat());
      this.view.PayDueAmount.CalendarSendDate.dateComponents = [endDate.getDate(), endDate.getMonth()+1, endDate.getFullYear()];

      if (obj === "Due") {
        if (this.view.PayDueAmount.imgRadioPayDueAmount.src === ViewConstants.IMAGES.ICON_RADIOBTN) {
          FormControllerUtility.enableButton(this.view.PayDueAmount.btnPayAmount);
          this.view.PayDueAmount.imgRadioPayDueAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
          this.view.PayDueAmount.imgRadioPayOtherAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN;
          //this.view.PayDueAmount.tbxAmount.skin = ViewConstants.SKINS.PAYDUE_DUE_AMOUNT;
          this.view.PayDueAmount.tbxAmount.setEnabled(false);
          this.view.PayDueAmount.lblDueAmount.isVisible = false;
          this.view.PayDueAmount.tbxAmount.text = this.dueAmount;
        }
      } else if (obj === "Other") {
        if (this.view.PayDueAmount.imgRadioPayOtherAmount.src === ViewConstants.IMAGES.ICON_RADIOBTN) {
          FormControllerUtility.disableButton(this.view.PayDueAmount.btnPayAmount);
          this.view.PayDueAmount.imgRadioPayDueAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN;
          this.view.PayDueAmount.imgRadioPayOtherAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
          //this.view.PayDueAmount.tbxAmount.skin = ViewConstants.SKINS.PAYDUE_OTHER_AMOUNT;
          this.view.PayDueAmount.tbxAmount.setEnabled(true);
          this.view.PayDueAmount.lblDueAmount.isVisible = true;
          this.view.PayDueAmount.tbxAmount.text = " ";
          this.view.PayDueAmount.lblDueAmount.text = (kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ":" + applicationManager.getConfigurationManager().configurations.getItem("CURRENCYCODE") + this.dueAmount);
        }
      }
      this.AdjustScreen();
    },
    /**
      * Function to show Quit pop up on click of cancel for Pay Due Amount 
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    payDueAmountCancel: function () {
      var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height;
      this.view.flxQuitPayment.height = height + "dp";
      this.view.flxQuitPayment.setVisibility(true);
      this.view.CustomPopup1.lblHeading.setFocus(true);
    },
    /**
     * Triggers on click of Pay Amount Button for Pay Due Amount, Show confirmation after validating data
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    payDueAmountPay: function () {
      var scopeObj = this;
      var isAmountValid = true;

      var isDateBeforeDue = true;
      var data = {};
      isAmountValid = this.amountValidation(this.view.PayDueAmount.tbxAmount.text.trim(), this.balanceInFromAccount, this.dueAmount);
      isDateBeforeDue = this.dateValidation(this.view.PayDueAmount.CalendarSendDate.date, this.dueDate);
      this.view.forceLayout();
      if (isAmountValid && isDateBeforeDue) {
        if (this.view.PayDueAmount.imgRadioPayDueAmount.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
          data = {
            "fromAccount": scopeObj.view.PayDueAmount.listbxFrom.selectedKeyValue[1],
            "fromAccountID": scopeObj.view.PayDueAmount.listbxFrom.selectedKey,
            "toAccount": scopeObj.view.PayDueAmount.listbxTo.selectedKeyValue[1],
            "toAccountID": scopeObj.view.PayDueAmount.listbxTo.selectedKey,
            "amount": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.dueAmount)),
            "date": scopeObj.view.PayDueAmount.CalendarSendDate.date,
            "description": scopeObj.view.PayDueAmount.tbxOptional.text.trim(),
            "payment": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.view.PayDueAmount.tbxAmount.text.trim())),
            "penalty": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.penalty))
          };
          var tdate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
          if (this.getDateObject(data.date).getTime() <= this.getDateObject(tdate).getTime()) {
            data.isScheduled = "false";
          }
          else {
            data.isScheduled = "true";
          }
          if (scopeObj.getDateObject(data.date).getTime() > scopeObj.getDateObject(this.dueDate).getTime()) {
            this.showConfirmationPayOtherAmount(data);
          }
          else {
            this.showConfirmationPayDueAmount(data);
            this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
            this.view.confirmation.confirmButtons.top = "0px";
          }
        }
        else {
          data = {
            "fromAccount": scopeObj.view.PayDueAmount.listbxFrom.selectedKeyValue[1],
            "fromAccountID": scopeObj.view.PayDueAmount.listbxFrom.selectedKey,
            "toAccount": scopeObj.view.PayDueAmount.listbxTo.selectedKeyValue[1],
            "toAccountID": scopeObj.view.PayDueAmount.listbxTo.selectedKey,
            "amount": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.dueAmount)),
            "date": scopeObj.view.PayDueAmount.CalendarSendDate.date,
            "description": scopeObj.view.PayDueAmount.tbxOptional.text.trim(),
            "dueDate": scopeObj.dueDate,
            "payment": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.view.PayDueAmount.tbxAmount.text.trim())).toFixed(2),
            "penalty": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.penalty)),
            "remainingAmount": parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.dueAmount)) - parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(scopeObj.view.PayDueAmount.tbxAmount.text))
          };
          data.remainingAmount = data.remainingAmount.toFixed(2);
          if (data.payment === data.amount) {
            this.view.confirmation.flxError.setVisibility(false);
            this.view.confirmation.confirmButtons.top = "0px";
          }
          else {
            this.view.confirmation.flxError.setVisibility(true);
            this.view.confirmation.lblError.text = kony.i18n.getLocalizedString("i18n.Error.partialPayment");
            this.view.confirmation.confirmButtons.top = "20px";
            this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
            this.view.confirmation.flxError.setVisibility(true);
          }
          var currDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
          if (this.getDateObject(data.date).getTime() <= this.getDateObject(currDate).getTime()) {
            data.isScheduled = "false";
          }
          else {
            data.isScheduled = "true";
          }
          this.showConfirmationPayOtherAmount(data);
          this.AdjustScreen();
          this.view.forceLayout();
        }
      }
    },
    /**
      *  Validate date and show error for Pay Due Amount
      * @member frmPayDueAmountController
      * @param {Date} date
      * @param {Date} dateDue
      * @returns {boolean} true/false
      * @throws {void} - None
      */
    dateValidation: function (date, dateDue) {
      var scopeObj = this;
      if (scopeObj.getDateObject(date).getTime() > scopeObj.getDateObject(dateDue).getTime()) {
        if (applicationManager.getConfigurationManager().loanPaymentAfterDueDateEnabled === "false") {
          scopeObj.view.PayDueAmount.flxError.setVisibility(true);
          scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.scheduledDateError");
          scopeObj.view.PayDueAmount.flxCalender.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
          scopeObj.view.confirmation.flxError.setVisibility(true);
          scopeObj.AdjustScreen();
          return false;
        }
        if (scopeObj.view.PayDueAmount.flxInfoDueDate.isVisible === false) {
          scopeObj.view.PayDueAmount.flxInfoDueDate.setVisibility(true);
          scopeObj.view.PayDueAmount.lblDueDate.setVisibility(false);
          return false;
        }
        else {
          return true;
        }
      }
      else {
        scopeObj.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
        scopeObj.view.PayDueAmount.lblDueDate.setVisibility(true);
        return true;
      }
    },
    /**
     *  Validate amount and show error for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {String} enteredAmount
     * @param {String} accountBalance
     * @param {String} dueAmount
     * @returns {boolean} - true/false
     * @throws {void} - None
     */
    amountValidation: function (enteredAmount, accountBalance, dueAmount) {
      this.currencySymbol = applicationManager.getConfigurationManager().configurations.getItem("CURRENCYCODE");
      if (kony.application.getCurrentBreakpoint() !== 640) {
      this.view.PayDueAmount.flxInfoDueDate.width = "48%";}
      this.view.PayDueAmount.lblDueDate1.width = "100%";
      this.view.PayDueAmount.lblDueDate2.width = "100%";
      var scopeObj = this;
      if (enteredAmount === "" || enteredAmount === undefined  || parseFloat(enteredAmount) <= 0 || enteredAmount.length < 1) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.login.EnrollAlert");
        this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        this.view.confirmation.flxError.setVisibility(true);
        scopeObj.AdjustScreen();
        return false;
      }
      enteredAmount = parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(enteredAmount));
      accountBalance = parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(accountBalance));
      dueAmount = parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(dueAmount));
      if (enteredAmount > dueAmount) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.amountMoreThanDueAmount");
        this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        this.view.confirmation.flxError.setVisibility(true);
        scopeObj.AdjustScreen();
        return false;
      } else if (enteredAmount > accountBalance) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.common.errorInsufficientFunds");
        scopeObj.view.PayDueAmount.listbxFrom.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERRORSKIN;
        scopeObj.view.confirmation.flxError.setVisibility(true);
        scopeObj.AdjustScreen();
        return false;
      } else if ((enteredAmount + parseFloat(scopeObj.penalty)) > accountBalance) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.insufficientFunds") + " (" + kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + this.currencySymbol + dueAmount + " , " + kony.i18n.getLocalizedString("i18n.loan.Penalty") + " : " + this.currencySymbol + scopeObj.penalty + ")";
        this.view.PayDueAmount.flxtxtbox.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        this.view.confirmation.flxError.setVisibility(true);
        scopeObj.AdjustScreen();
        return false;
      } else {
        scopeObj.setSkinToFlex(scopeObj.view.PayDueAmount.flxtxtbox, scopeObj.view.PayDueAmount.flxError);
        if (scopeObj.view.PayDueAmount.flxInfoDueAmount.isVisible === false && (dueAmount - enteredAmount) > 0) {
          scopeObj.view.PayDueAmount.flxInfoDueAmount.setVisibility(true);
          scopeObj.view.PayDueAmount.flxInfoDueAmount.width = "48%";
          scopeObj.view.PayDueAmount.lblInfoAmount.width = "100%";
          scopeObj.view.PayDueAmount.lblDueAmount.setVisibility(false);
          var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
          var dueDate = this.loadLoanPayModule().presentationController.getFormattedDateString(this.dueDate);
          var tempDue = CommonUtilities.formatCurrencyWithCommas(scopeObj.view.PayDueAmount.tbxAmount.text);
          scopeObj.view.PayDueAmount.tbxAmount.text = tempDue.slice(1);
          this.view.PayDueAmount.lblInfoAmount.isVisible = true;
          if(new Date(dueDate)>new Date(todayDate)){
            scopeObj.view.PayDueAmount.lblInfoAmount.text = kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + CommonUtilities.formatCurrencyWithCommas(dueAmount.toFixed(2)) + "." + kony.i18n.getLocalizedString("i18n.Error.payRemainingBalance") + " " + CommonUtilities.formatCurrencyWithCommas(parseFloat(dueAmount - enteredAmount).toFixed(2)) + " " + kony.i18n.getLocalizedString("i18n.common.onBefore") + " " + dueDate + " " + kony.i18n.getLocalizedString("i18n.Error.avoidPenalty");
          }else{
          scopeObj.view.PayDueAmount.lblInfoAmount.text = kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + CommonUtilities.formatCurrencyWithCommas(dueAmount.toFixed(2)) + ".";
          }
         scopeObj.view.forceLayout()
        } else {
          return true;
        }
      }
      scopeObj.AdjustScreen();
    },
    /**
      *  Set confirmation values
      * @member frmPayDueAmountController
      * @param {JSON} data for left container
      * @param {JSON} extraData for right container
      * @param {JSON} actions for buttons
      * @returns {void} - None
      * @throws {void} - None
      */
    setConfirmationValues: function (data, extraData, actions) {
      var target = this.view.confirmation.flxLeft.widgets();
      if (data.Description) {
        this.view.confirmation.flxContainerPaymentDate.setVisibility(true);
      }
      else {
        this.view.confirmation.flxContainerPaymentDate.setVisibility(false);
      }
      var i = 0;
      var prop, key, value;
      for (prop in data) {
        key = target[i].widgets()[0].widgets()[0];
        value = target[i].widgets()[2].widgets()[0];
        key.text = prop + ":";
        value.text = data[prop];
        i++;
      }
      if (Object.keys(extraData).length !== 0) {
        this.view.confirmation.flxPartialPayment.isVisible = true;
        target = this.view.confirmation.flxPartialPaymentdetails.widgets();
        i = 1;
        for (prop in extraData) {
          key = target[i].widgets()[0].widgets()[0];
          value = target[i].widgets()[1].widgets()[0];
          key.text = prop;
          value.text = extraData[prop];
          i++;
        }
        this.view.confirmation.confirmButtons.top = "0px";
      } else {
        this.view.confirmation.flxPartialPayment.isVisible = false;
        this.view.confirmation.confirmButtons.top = "20px";
      }

      this.view.confirmation.confirmButtons.btnCancel.onClick = actions.cancel;
      this.view.confirmation.confirmButtons.btnModify.onClick = actions.modify;
      if (CommonUtilities.isCSRMode()) {
        this.view.confirmation.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
        this.view.confirmation.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
        this.view.confirmation.confirmButtons.btnConfirm.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
      }
      else {
        if (this.loanContext === "Loan Payoff") {
          this.view.confirmation.confirmButtons.btnConfirm.onClick = this.loanPayOffPayment;
        }
        else {
          this.view.confirmation.confirmButtons.btnConfirm.onClick = actions.confirm;
        }
      }
    },
    /**
    *  Function to set footer height
    * @member frmPayDueAmountController
    * @param {void} - None 
    * @returns {void} - None
    * @throws {void} - None
    */
    postShowPayDueAmount: function () {
      this.view.customheader.imgKony.setFocus(true);
      this.AdjustScreen();
      this.setFlowActions();
      var context1 = { "widget": this.view.PayDueAmount.CalendarSendDate, "anchor": "bottom" };
      this.view.PayDueAmount.CalendarSendDate.setContext(context1);
      context1 = { "widget": this.view.LoanPayOff.CalendarPyOn, "anchor": "bottom" };
      this.view.LoanPayOff.CalendarPyOn.setContext(context1);
      //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
    },
    /**
     *  Set Screen height
     * @member frmPayDueAmountController
     * @param {void} - None 
     * @returns {void} - None
     * @throws {void} - None
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0)
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
      this.initializeResponsiveViews();
    },
    /**
    	Depending on the locale this method returns the magic number that is required 
        to align info popup
    **/
    getMagicNumberForPopup : function(){
      var locale = kony.i18n.getCurrentLocale();
      switch(locale){
        case "es_ES" : return -3;
        case "en_US" : return -2;
        case "en_GB" : return -2;
        case "de_DE" : return 1;
        case "fr_FR" : return 4;
        default : return 10;
      }
    },
    /**
    *  Set flow actions
     * @member frmPayDueAmountController
     * @param {void} - None 
     * @returns {void} - None
     * @throws {void} - None
     */
    setFlowActions: function () {
      var scopeObj = this;
      this.view.LoanPayOff.flxImageInfo.onClick = function () {
        if (scopeObj.view.LoanPayOff.AllForms.isVisible === false){
	      scopeObj.view.LoanPayOff.AllForms.left = (scopeObj.view.LoanPayOff.flxImageInfo.frame.x - scopeObj.view.LoanPayOff.lblPayOffPenality.frame.width)+scopeObj.getMagicNumberForPopup()+"%";
          scopeObj.view.LoanPayOff.AllForms.isVisible = true;
      	}
        else
          scopeObj.view.LoanPayOff.AllForms.isVisible = false;
      };
      this.view.LoanPayOff.AllForms.flxCross.onClick = function () {
        scopeObj.view.LoanPayOff.AllForms.isVisible = false;
      };
    },
    /**
    *  Set Acknowledgement Values
     * @member frmPayDueAmountController
     * @param {JSON} data
     * @param {JSON} details 
     * @param {JSON} extraData 
     * @returns {void} - None
     * @throws {void} - None
     */
    setAcknowledgementValues: function (data, details, extraData) {
      var i,prop,value,key;
      this.view.acknowledgment.lblTransactionMessage.text = data.message;
      this.view.acknowledgment.ImgAcknowledged.src = data.image;
      this.view.acknowledgment.lblRefrenceNumberValue.text = data.referenceNumber;
      this.view.acknowledgment.lblAccType.text = data.accountType;
      this.view.acknowledgment.lblBalance.text = data.amount;
      if (details.Description) {
        this.view.confirmDialog.flxContainerDescription.setVisibility(true);
      }
      else {
        this.view.confirmDialog.flxContainerDescription.setVisibility(false);
      }
      this.view.forceLayout();
      var target = this.view.confirmDialog.flxMain.widgets();
      i = 2;
      for (prop in details) {
        key = target[i].widgets()[0].widgets()[0];
        value = target[i].widgets()[1].widgets()[0];
        key.text = prop;
        value.text = details[prop];
        i++;
      }
      if (Object.keys(details).length > 5) {
        this.view.confirmDialog.flxContainerPenalty.isVisible = true;
        this.view.confirmDialog.flxContainerPartial.isVisible = true;
      } else {
        this.view.confirmDialog.flxContainerPenalty.isVisible = false;
        this.view.confirmDialog.flxContainerPartial.isVisible = false;
      }
      if (Object.keys(extraData).length !== 0) {
        this.view.confirmDialog.flxPartial.isVisible = true;
        target = this.view.confirmDialog.flxPartial.widgets();
        i = 1;
        for (prop in extraData) {
          key = target[i].widgets()[0].widgets()[0];
          value = target[i].widgets()[1].widgets()[0];
          key.text = prop;
          value.text = extraData[prop];
          i++;
        }
      } else {
        this.view.confirmDialog.flxPartial.isVisible = false;
      }
      this.view.forceLayout();
    },
    /**
    *  Show confirmation for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {Object} obj stores transaction details
     * @returns {void} - None
     * @throws {void} - None
     */
    showConfirmationPayDueAmount: function (obj) {
      var scopeObj = this;
      this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      this.view.confirmation.flxContainerDescription.setVisibility(false);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountConfirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": obj.date,
        "Description": obj.description,
      };
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showPayDueAmount();
        },
        "confirm": function () {
          scopeObj.successData = obj;
          scopeObj.confirmPayDueAmount(obj, "payCompleteMonthlyDue");
        }
      };
      this.setConfirmationValues(data, {}, actions);
      this.showView(["flxConfirmation"]);
    },
    /**
    *  Confirm Pay Due Amount/Pay Other Amount
     * @member frmPayDueAmountController
     * @param {Object} obj stores transaction details
     * @param {function} action stores success callback 
     * @returns {void} - None
     * @throws {void} - None
     */
    confirmPayDueAmount: function (obj, action) {
      var data = {};
      FormControllerUtility.showProgressBar(this.view);
      data.fromAccountID = obj.fromAccountID;
      data.toAccountID = obj.toAccountID;
      data.fromAccount = obj.fromAccount;
      data.toAccount = obj.toAccount;
      data.notes = obj.description;
      data.date = obj.date;
      if (this.view.PayDueAmount.imgRadioPayDueAmount.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
        data.amount = obj.amount;
      }
      else {
        data.amount = obj.payment;
      }
      if (this.getDateObject(data.date).getTime() > this.getDateObject(this.dueDate).getTime()) {
        data.penaltyFlag = "true";
        data.payoffFlag = "false";
      }
      data.isScheduled = obj.isScheduled;
      this.loadLoanPayModule().presentationController.payLoanOff(data, action);
    },
    /**
    *  Success Callback for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {Object} data
     * @param {String} response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayDueAmount: function (data, response) {
      var scopeObj = this;
      data = scopeObj.successData;
      var scheduledFlag = data.isScheduled;
      this.view.flxMainAcknowledgment.setVisibility(true);
      scopeObj.showAcknowledgementPayDueAmount(data, scheduledFlag);
      if (applicationManager.getConfigurationManager().showLoanUpdateDisclaimer === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      else {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(false);
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
    },
    /**
    *  Success Callback for Pay Other Amount
     * @member frmPayDueAmountController
     * @param {Object} data
     * @param {String} response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayOtherAmount: function (data, response) {
      var scopeObj = this;
      this.view.flxMainAcknowledgment.setVisibility(true);
      data = scopeObj.successData;
      var scheduledFlag = data.isScheduled;
      scopeObj.showAcknowledgementPayOtherAmount(data, scheduledFlag);
      if (applicationManager.getConfigurationManager().showLoanUpdateDisclaimer === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      else {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(false);
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
    },
    /**
     *  Set updated account value for From Account on acknowledgement screen 
      * @member frmPayDueAmountController
      * @param {Object} account - from Account
      * @returns {void} - None
      * @throws {void} - None
      */
    updateAccountDetails: function (account) {
      account = account[0];
      this.view.acknowledgment.lblAccType.text = account.accountName;
      this.view.acknowledgment.lblBalance.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance);
      this.view.confirmDialog.lblValue.text = account.accountName + " ..." + account.accountID.slice(-4) + " " + this.getDisplayBalance(account);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    /**
    *  Set updated account value for To Account on acknowledgement screen  
     * @member frmPayDueAmountController
     * @param {Object} account - to Account
     * @returns {void} - None
     * @throws {void} - None
     */
    updateToAccountDetails: function (account) {
      account = account[0];
      this.view.confirmDialog.lblValueTo.text = account.accountName + " ..." + account.accountID.slice(-4) + " " + this.getDisplayToBalance(account);
      this.view.forceLayout();
    },
    /**
     *  Show confirmation for Pay other amount  
      * @member frmPayDueAmountController
      * @param {Object} obj stores trannsaction details
      * @returns {void} - None
      * @throws {void} - None
      */
    showConfirmationPayOtherAmount: function (obj) {
      var scopeObj = this;
      var extraData = {};
      this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      this.view.confirmation.flxContainerDescription.setVisibility(false);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountConfirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": scopeObj.loadLoanPayModule().presentationController.getFormattedDateString(scopeObj.dueDate),
        "Description": obj.description,
      };
      if (scopeObj.getDateObject(obj.date).getTime() > scopeObj.getDateObject(this.dueDate).getTime()) {
        scopeObj.view.confirmation.flxError.setVisibility(true);
        scopeObj.view.confirmation.lblError.text = kony.i18n.getLocalizedString("i18n.loan.payingAfterDueDate");
        extraData = {
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(obj.payment),
          "Due Date:": scopeObj.loadLoanPayModule().presentationController.getFormattedDateString(scopeObj.dueDate),
          "Your Payment date:": obj.date,
          "Late Payment Penalty:": CommonUtilities.formatCurrencyWithCommas(obj.penalty),
        };
      }
      else {
        extraData = {
          "Your Bill Amount:": CommonUtilities.formatCurrencyWithCommas(obj.amount),
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(obj.payment),
          "Balance Amount:": CommonUtilities.formatCurrencyWithCommas(obj.remainingAmount),
          "Date:": obj.date,
        };
      }
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showPayDueAmount();
        },
        "confirm": function () {
          scopeObj.successData = obj;
          scopeObj.confirmPayDueAmount(obj, "payOtherAmount");
        }
      };
      this.setConfirmationValues(data, extraData, actions);
      this.showView(["flxConfirmation"]);
    },
    /**
     *  Show acknowledgement for Pay Due amount
      * @member frmPayDueAmountController
      * @param {Object} obj stores trannsaction details
      * @param {boolean} scheduledFlag 
      * @returns {void} - None
      * @throws {void} - None
      */
    showAcknowledgementPayDueAmount: function (obj, scheduledFlag) {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountAcknowledgement");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "message": kony.i18n.getLocalizedString("i18n.loanpay.transactionSuccess"),
        "image": ViewConstants.IMAGES.SUCCESS_GREEN,
        "referenceNumber": " ",
        "accountType": " ",
        "amount": " ",
      };
      var details = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": scopeObj.dueAmount,
        "Date": obj.date,
        "Description": obj.description,
      };
      if (scheduledFlag === "true") {
        data.message = kony.i18n.getLocalizedString("i18n.loanpay.scheduledTransaction") + obj.date;
      }
      this.setAcknowledgementValues(data, details, {});

      this.showView(["flxAcknowledgement"]);
    },
    /**
      *  Show acknowledgement for Pay other amount
       * @member frmPayDueAmountController
       * @param {Object} obj stores trannsaction details
       * @param {boolean} scheduledFlag 
       * @returns {void} - None
       * @throws {void} - None
       */
    showAcknowledgementPayOtherAmount: function (obj, scheduledFlag) {
      var scopeObj = this;
      var extraData = {};
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountAcknowledgement");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "message": kony.i18n.getLocalizedString("i18n.loanpay.transactionSuccess"),
        "image": ViewConstants.IMAGES.SUCCESS_GREEN,
        "referenceNumber": " ",
        "accountType": " ",
        "amount": " ",
      };
      var details = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": scopeObj.loadLoanPayModule().presentationController.getFormattedDateString(scopeObj.dueDate),
        "Description": obj.description,
      };
      if (scopeObj.getDateObject(obj.date).getTime() < scopeObj.getDateObject(this.dueDate).getTime()) {
        extraData = {
          "Your Bill Amount:": CommonUtilities.formatCurrencyWithCommas(obj.amount),
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(parseFloat(obj.payment)),
          "Balance Amount:": CommonUtilities.formatCurrencyWithCommas(obj.remainingAmount),
          "Date:": obj.date,
        };
      }
      else {
        extraData = {
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(parseFloat(obj.payment)),
          "Due Date:": scopeObj.loadLoanPayModule().presentationController.getFormattedDateString(scopeObj.dueDate),
          "Your Payment date:": obj.date,
          "Late Payment Penalty:": CommonUtilities.formatCurrencyWithCommas(obj.penalty),
        };
      }
      if (scheduledFlag === "true") {
        data.message = kony.i18n.getLocalizedString("i18n.loanpay.scheduledTransaction") + obj.date;
      }
      this.setAcknowledgementValues(data, details, extraData);

      this.showView(["flxAcknowledgement"]);
    },
    /**
   *  Show Loan Pay Off Confirmation
    * @member frmPayDueAmountController
    * @param {void} -None
    * @returns {void} - None
    * @throws {void} - None
    */
    showConfirmationLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loanpay.Confirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.view.flxBottom.setVisibility(false);
      var data = {
        "From": "Personal Checking….1234",
        "To": "My Car Loan Account…XXXX9870",
        "PayOff Amount": "$1000",
        "Actual Loan End Date": "07/15/2017",
        "Payment Date": "07/15/2017",
        "Penalty Amount": "$1000",
        "Note": "Car Loan EMI for July"
      };
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showLoanPayOff();
        },
        "confirm": function () {
          scopeObj.showAcknowledgementLoanPayOff();
        }
      };
      this.setConfirmationValues(data, {}, actions);
      this.showView(["flxConfirmation"]);
      this.AdjustScreen();
    },
    /**
     *  Show Loan Pay Off Acknowledgement
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showAcknowledgementLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loanpay.Confirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.showView(["flxAcknowledgement"]);
    },
    /**
     *  Show Pay Due Amount
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showPayDueAmount: function () {
      this.showView(["flxPayDueAmount", "flxPrimaryActions"]);
      if (kony.application.getCurrentBreakpoint() === 640) {
        this.view.flxPrimaryActions.isVisible = false;
      }
      else {
        this.view.flxPrimaryActions.isVisible = true;
      }
      this.AdjustScreen();
    },
    /**
     *  Show loan pay off
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = "LOAN PAYOFF";
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.view.flxBottom.setVisibility(true);
      this.showView(["flxLoanPayOff", "flxPrimaryActions"]);
      this.view.forceLayout();
      this.AdjustScreen();
    },
    /**
    *  Show quit popup
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    showQuitPopUp: function () {
      var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height;
      this.view.flxQuitPayment.height = height + "dp";
      this.view.flxQuitPayment.isVisible = true;
    },
    /**
    *  Triggered on No of Quit Pop up
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    quitPopUpNo: function () {
      this.view.flxQuitPayment.setVisibility(false);
    },
    /**
    * Triggered on Yes of Quit Pop up
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    quitPopUpYes: function () {
      this.quitPopUpNo();
      this.backToAccountDetails();
    },
    /**
     * Function to load all accounts
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    loadAccounts: function () {
      var scopeObj = this;
      scopeObj.loadLoanPayModule().presentationController.fetchCheckingAccounts();
    },
    /**
     * Callback to set masterdata for TO FROM account, to set payment accounts
      * @member frmPayDueAmountController
      * @param {Array} response
      * @returns {void} - None
      * @throws {void} - None
      */
    accountsListCallback: function (response) {
      this.initListboxSelectionAction(response);
      this.updateListBox(response);
      this.updateToListBox(response);
      this.updateTransferAccountList(response);
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(this.view.PayDueAmount.listbxFrom.selectedKey, "populateAccountData");
    },
    initListboxSelectionAction: function (accounts) {
      this.view.PayDueAmount.listbxFrom.onSelection = this.updateFromAccountValue.bind(this, accounts);
      this.view.PayDueAmount.listbxTo.onSelection = this.updateToAccountValue.bind(this, accounts);
      this.view.PayDueAmount.CalendarSendDate.onSelection = this.setSkinToFlex.bind(this, this.view.PayDueAmount.flxCalender, this.view.PayDueAmount.flxError);
    },

    /**
     * Function to set skin to calendar widget,
      * @member frmPayDueAmountController
      * @param {}
      * @returns {void} - None
      * @throws {void} - None
      */
    setSkinToListbox: function (widgetId, errorWidget) {
      widgetId.skin = ViewConstants.SKINS.LOANS_LISTBOX_NOERROR;
      errorWidget.setVisibility(false);
      this.AdjustScreen();
    },
    /**
     * Function to set skin to calendar widget,
     * @member frmPayDueAmountController
     * @param {}
     * @returns {void} - None
     * @throws {void} - None
     */
    setSkinToFlex: function (widgetId, errorWidget) {
      widgetId.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERROR;
      errorWidget.setVisibility(false);
      this.AdjustScreen();
    },
    /**
     * Function to filter accounts for FROM ListBox,
      * @member frmPayDueAmountController
      * @param {Array} fromAccounts
      * @returns {void} - None
      * @throws {void} - None
      */
    updateListBox: function (fromAccounts) {
      var accounts = [];
      for (var i = 0, j = 0; i < fromAccounts.length; i++)
        if (fromAccounts[i].accountType !== "Loan" && fromAccounts[i].accountType !== "Mortgage" && fromAccounts[i].accountType !== "CreditCard") {
          accounts[j] = fromAccounts[i];
          j++;
        }
      this.view.LoanPayOff.listbxFrom.masterData = this.showAccountsForSelection(accounts);
      this.view.PayDueAmount.listbxFrom.masterData = this.showAccountsForSelection(accounts);
      if (this.fromAccount) {
        this.view.LoanPayOff.listbxFrom.selectedKey = this.fromAccount;
        this.view.PayDueAmount.listbxFrom.selectedKey = this.fromAccount;
        this.updateFromAccountValue(fromAccounts);
      }else{
        this.view.LoanPayOff.listbxFrom.selectedKey = accounts[0].accountID;
        this.view.PayDueAmount.listbxFrom.selectedKey = accounts[0].accountID;
        this.updateFromAccountValue(fromAccounts);
      }
    },
    /**
     * Function to filter accounts for TO ListBox
      * @member frmPayDueAmountController
      * @param {Array} fromAccounts
      * @returns {void} - None
      * @throws {void} - None
      */
    updateToListBox: function (fromAccounts) {
      var accounts = [];
      for (var i = 0, j = 0; i < fromAccounts.length; i++)
        if (fromAccounts[i].accountType === "Loan") {
          if (fromAccounts[i].principalBalance > 0) {
            accounts[j] = fromAccounts[i];
            j++;
          }
        }
      if (accounts.length > 0) {
        this.view.LoanPayOff.listbxTo.masterData = this.showAccountsForSelectionToBox(accounts);
        this.view.LoanPayOff.listbxTo.selectedKey = this.savedAccountID;
      }
      var accounts2 = [];
      for (var k = 0, l = 0; k < fromAccounts.length; k++)
        if (fromAccounts[k].accountType === "Loan" || fromAccounts[k].accountType === "Mortgage") {
          if (fromAccounts[k].currentAmountDue > 0 && fromAccounts[k].principalBalance > 0) {
            accounts2[l] = fromAccounts[k];
            l++;
          }
        }
      if (accounts2.length > 0) {
        this.view.PayDueAmount.listbxTo.masterData = this.showAccountsForSelectionToBox(accounts2);
        this.view.PayDueAmount.listbxTo.selectedKey = this.savedAccountID;

      }
    },
    /**
     * Function to return master data for "For" listBox
      * @member frmPayDueAmountController
      * @param {Array} presentAccounts eligible accounts
      * @returns {void} - None
      * @throws {void} - None
      */
    showAccountsForSelection: function (presentAccounts) {
      var list = [];
      for (var i = 0; i < presentAccounts.length; i++) {
        var tempList = [];
        tempList.push(presentAccounts[i].accountID);
        var tempAccountNumber = presentAccounts[i].accountID;
        tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4) + " " + this.getDisplayBalance(presentAccounts[i]));
        list.push(tempList);
      }
      return list;
    },
    /**
     * Get display balance for To ListBox, based on context
      * @member frmPayDueAmountController
      * @param {Object} account account
      * @returns {void} - None
      * @throws {void} - None
      */
    getDisplayToBalance: function (account) {
      if (this.loanContext === "Loan Payoff") {
        return "(" + kony.i18n.getLocalizedString('i18n.loan.payOffAmount') + ":" + CommonUtilities.formatCurrencyWithCommas(account.payoffAmount, false, account.currencyCode) + ")";
      }
      else if (this.loanContext === "Loan Due") {
        return "(" + kony.i18n.getLocalizedString('i18n.loan.dueAmount') + ":" + CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue, false, account.currencyCode) + ")";
      }
    },
    /**
     * Function to return master data for To listBox
      * @member frmPayDueAmountController
      * @param {Array} presentAccounts eligible accounts
      * @returns {void} - None
      * @throws {void} - None
      */
    showAccountsForSelectionToBox: function (presentAccounts) {
      var list = [];
      for (var i = 0; i < presentAccounts.length; i++) {
        var tempList = [];
        tempList.push(presentAccounts[i].accountID);
        var tempAccountNumber = presentAccounts[i].accountID;
        tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4) + " " + this.getDisplayToBalance(presentAccounts[i]));
        list.push(tempList);
      }
      return list;
    },
    /**
     * Function to get display balance in (Available: $123) format
      * @member frmPayDueAmountController
      * @param {Object} account account
      * @returns {void} - None
      * @throws {void} - None
      */
    getDisplayBalance: function (account) {
      return "(" + kony.i18n.getLocalizedString('i18n.common.available') + ":" + CommonUtilities.formatCurrencyWithCommas(account.availableBalance, false, account.currencyCode) + ")";
    },
    /**
     * Function to set Data initially
      * @member frmPayDueAmountController
      * @param {Object} data to initialize from account
      * @returns {void} - None
      * @throws {void} - None
      */
    setDataToForm: function (data) {
      var scopeObj = this;
      this.view.LoanPayOff.tbxOptional.maxTextLength = 150;
      this.view.PayDueAmount.tbxOptional.maxTextLength = 150;
      this.savedAccountID = data.accountID;
      this.flagPayOff = true;
      if (data.closingDate === null || data.closingDate === undefined || data.principalBalance === null || data.principalBalance === undefined || data.payOffCharge === null || data.payOffCharge === undefined) {
        this.flagPayOff = false;
      }
      this.view.LoanPayOff.tbxActualLoanEndDate.text = scopeObj.loadLoanPayModule().presentationController.getFormattedDateString(data.closingDate);
      this.view.LoanPayOff.tbxLoanPayOffAmount.text = data.payoffAmount;
      this.view.LoanPayOff.tbxPayoffPenality.text = data.payOffCharge;
      this.view.PayDueAmount.imgRadioPayDueAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
      this.view.PayDueAmount.imgRadioPayOtherAmount.src = ViewConstants.IMAGES.ICON_RADIOBTN;
      this.view.PayDueAmount.tbxAmount.text = data.currentAmountDue;
      //this.view.PayDueAmount.tbxAmount.skin = ViewConstants.SKINS.PAYDUE_DUE_AMOUNT;
      this.view.PayDueAmount.tbxAmount.setEnabled(false);
      this.view.PayDueAmount.CalendarSendDate.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();     
      var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
      var endDate  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(todayDate, applicationManager.getFormatUtilManager().getDateFormat());
      this.view.PayDueAmount.CalendarSendDate.dateComponents = [endDate.getDate(), endDate.getMonth()+1, endDate.getFullYear()];

      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);
      this.penalty = data.lateFeesDue;
      this.dueAmount = data.currentAmountDue;
      this.dueDate = data.dueDate;
      this.availableBalance = data.availableBalance;
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.flxError.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + this.loadLoanPayModule().presentationController.getFormattedDateString(data.dueDate) + ")";
      var amount1 = applicationManager.getFormatUtilManager().deFormatAmount(data.payoffAmount);
      var amount2 = applicationManager.getFormatUtilManager().deFormatAmount(data.payOffCharge);
      this.fullPayOffAmount = Number(amount1) + Number(amount2);
      this.view.LoanPayOff.tbxActualLoanEndDate.setEnabled(false);
      this.view.LoanPayOff.tbxLoanPayOffAmount.setEnabled(false);
      this.view.LoanPayOff.tbxPayoffPenality.setEnabled(false);
      this.disableDaySelectionLoanPayOff(this.view.LoanPayOff.CalendarPyOn, this.view.LoanPayOff.tbxActualLoanEndDate.text);
      CommonUtilities.disableOldDaySelection(this.view.PayDueAmount.CalendarSendDate);
      this.view.LoanPayOff.CalendarPyOn.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
      var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
      var startDate = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(todayDate, applicationManager.getFormatUtilManager().getDateFormat());
      this.view.LoanPayOff.CalendarPyOn.dateComponents = [startDate.getDate(), startDate.getMonth() + 1, startDate.getFullYear()];
      this.AdjustScreen();
      this.view.forceLayout();
    },
    /**
    * Function to disable date selectionm, Loan Pay Off
     * @member frmPayDueAmountController
     * @param {Object} widgetId 
     * @param {Date} toDate 
     * @returns {void} - None
     * @throws {void} - None
     */
    disableDaySelectionLoanPayOff: function (widgetId, toDate) {
      var date = new Date();
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var yy = date.getFullYear();
      var date1 = toDate.split("/");
      if (date1.length === 1) {
        var maxDate = new Date(toDate);
        date1[1] = maxDate.getDate();
        date1[0] = maxDate.getMonth() + 1;
        date1[2] = maxDate.getFullYear();
      }
      widgetId.enableRangeOfDates([dd, mm, yy], [date1[1], date1[0], date1[2]], "skn", true);
    },
    /**
     * Pay Off enable button
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    enableButtonConfirm: function () {
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(this.view.LoanPayOff.listbxFrom.selectedKey, "validateData");
    },
    /**
    * Validate data for Pay Off
     * @member frmPayDueAmountController
     * @param {Object} account account
     * @returns {void} - None
     * @throws {void} - None
     */
    validateData: function (account) {
      if (this.flagPayOff === false) {
        this.view.LoanPayOff.flxError.setVisibility(true);
        this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.loan.detailsIncorrectMsg");
        this.view.LoanPayOff.listbxFrom.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERRORSKIN;
        this.view.confirmation.flxError.setVisibility(true);
        this.AdjustScreen();
      }
      else {
        if (account.availableBalance < this.fullPayOffAmount) {
          this.view.LoanPayOff.flxError.setVisibility(true);
          this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.common.errorInsufficientFunds");
        } else {
          if (!CommonUtilities.isChecked(this.view.LoanPayOff.imgCheckbox)) {
            this.view.LoanPayOff.flxError.setVisibility(true);
            this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.loan.termsandconditions");
          } else {
            this.view.LoanPayOff.flxError.setVisibility(false);
            this.navigateToConfirmation();
          }
        }
      }
      this.view.forceLayout();
    },
    /**
     * Funtion to get account object for selected account ID of From Listbox
      * @member frmPayDueAmountController
      * @param {frmAccounts} frmAccounts frmAccounts
      * @returns {void} - None
      * @throws {void} - None
      */
    updateFromAccountValue: function (frmAccounts) {
      var selectedKey = this.view.PayDueAmount.listbxFrom.selectedKey;
      var selectedAccount = frmAccounts.filter(function (accounts) {
        return accounts.accountID === selectedKey;
      })[0];
      this.populateFromAccountValues(selectedAccount);
      this.setSkinToListbox(this.view.PayDueAmount.listbxFrom, this.view.PayDueAmount.flxError);
    },
    /**
    * Function to update available balance for validation, when From account is selected
     * @member frmPayDueAmountController
     * @param {Object} account account
     * @returns {void} - None
     * @throws {void} - None
     */
    populateFromAccountValues: function (account) {
      //get amount for validation
      this.balanceInFromAccount = account.availableBalance||account[0].availableBalance;
    },
    /**
     * Function to get updated account object for an accountID based on current context, callback to show account details
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    backToAccountDetails: function () {
      FormControllerUtility.showProgressBar(this.view);
      var selectedKey;
      if (this.loanContext === "Loan Due") {
        selectedKey = this.view.PayDueAmount.listbxTo.selectedKey;
      }
      else if (this.loanContext === "Loan Payoff") {
        selectedKey = this.view.LoanPayOff.listbxTo.selectedKey;
      }
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(selectedKey, "navigationToAccountDetails");
    },
    /**
     * Function to get account object when To account is selected, Pay Due Amount
      * @member frmPayDueAmountController
      * @param {frmAccounts} frmAccounts frmAccounts
      */
    updateToAccountValue: function (frmAccounts) {
      var selectedKey = this.view.PayDueAmount.listbxTo.selectedKey;
      var selectedAccount = frmAccounts.filter(function (accounts) {
        return accounts.accountID === selectedKey;
      })[0];
      selectedAccount.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(selectedAccount.currentAmountDue, false, selectedAccount.currencyCode);
      selectedAccount.currentAmountDue = selectedAccount.currentAmountDue.slice(1);
      this.populateToAccountValues(selectedAccount);
    },
    /**
    * Function to populate and update values when To account is selected, Pay Due Amount
     * @member frmPayDueAmountController
     * @param {object} account account
     * @returns {void} - None
     * @throws {void} - None
     */
    populateToAccountValues: function (account) {
      //due amount //due date //store due amount
      this.availableBalance = account.availableBalance;
      this.dueAmount = account.currentAmountDue;
      this.view.PayDueAmount.lblDueAmount.text = (kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + CommonUtilities.formatCurrencyWithCommas(this.dueAmount, false, account.currencyCode));
      this.dueDate = account.dueDate;
      this.view.PayDueAmount.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + this.loadLoanPayModule().presentationController.getFormattedDateString(this.dueDate) + ")";
      this.penalty = account.lateFeesDue;

      this.view.PayDueAmount.CalendarSendDate.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();     
      var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
      var endDate  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(todayDate, applicationManager.getFormatUtilManager().getDateFormat());
      this.view.PayDueAmount.CalendarSendDate.dateComponents = [endDate.getDate(), endDate.getMonth()+1, endDate.getFullYear()];

      if (this.view.PayDueAmount.imgRadioPayDueAmount.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
        this.view.PayDueAmount.tbxAmount.text = account.currentAmountDue;
      }
      else {
        this.payDueAmountRadioButton("Due");
      }
    },
    /**
     * Function on click of Pay amount Button, Loan Pay Off
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    loanDataAfterConfirmation: function () {
      this.enableButtonConfirm();
    },
    /**
     * Navigate to Pay Off confirmation
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    navigateToConfirmation: function () {
      this.showConfirmationLoanPayOff();
      this.view.confirmation.lblValueAmount.text = this.view.LoanPayOff.tbxLoanPayOffAmount.text;
      this.view.confirmation.lblValueDate.text = this.view.LoanPayOff.tbxActualLoanEndDate.text;
      if (this.view.LoanPayOff.tbxPayoffPenality.text !== null && this.view.LoanPayOff.tbxPayoffPenality.text !== "") {
        this.view.confirmation.flxContainerPenaltyAmount.setVisibility(true);
        this.view.confirmation.lblValuePenaltyAmount.text = this.view.LoanPayOff.tbxPayoffPenality.text;
      }
      else {
        this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      }
      this.view.confirmation.lblValuePaymentDate.text = this.view.LoanPayOff.CalendarPyOn.date;
      if (this.view.LoanPayOff.tbxOptional.text !== null && this.view.LoanPayOff.tbxOptional.text !== "" && this.view.LoanPayOff.tbxOptional.text !== " ") {
        this.view.confirmation.flxContainerDescription.setVisibility(true);
        this.view.confirmation.lblValueDescription.text = this.view.LoanPayOff.tbxOptional.text;
      }
      else {
        this.view.confirmation.flxContainerDescription.setVisibility(false);
      }
      this.view.confirmation.lblValue.text = this.view.LoanPayOff.listbxFrom.selectedKeyValue[1];
      this.view.confirmation.lblValueTo.text = this.view.LoanPayOff.listbxTo.selectedKeyValue[1];
      this.view.forceLayout();
    },
    /**
     * Function to form data and confirm Pay Off
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    loanPayOffPayment: function () {
      FormControllerUtility.showProgressBar(this.view);
      var data = {};
      data.isScheduled = "true";
      var tdate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
      if (this.getDateObject(this.view.confirmation.lblValuePaymentDate.text).getTime() <= this.getDateObject(tdate).getTime()) {
        data.isScheduled = "false";
      }
      data.fromAccountID = this.view.LoanPayOff.listbxFrom.selectedKey;
      data.toAccountID = this.view.LoanPayOff.listbxTo.selectedKey;
      data.fromAccount = this.view.confirmation.lblValue.text;
      data.toAccount = this.view.confirmation.lblValueTo.text;
      data.notes = this.view.confirmation.lblValueDescription.text;
      data.principalBalance = this.view.confirmation.lblValueAmount.text;
      data.actualLoanEndDate = this.view.confirmation.lblValueDate.text;
      data.payOffCharge = this.view.confirmation.lblValuePenaltyAmount.text;
      data.date = this.loadLoanPayModule().presentationController.getBackendDate(this.view.confirmation.lblValuePaymentDate.text);
      data.amount = this.fullPayOffAmount;
      data.penaltyFlag = "true";
      data.payoffFlag = "true";
      var action = "payCompleteDue";
      this.loadLoanPayModule().presentationController.payLoanOff(data, action);
    },

    /**
    * Callback for Pay Off Success
     * @member frmPayDueAmountController
     * @param {void} data data
     * @param {String} response response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayment: function (data, response) {
      this.showAcknowledgementLoanPayOff();
      this.view.flxMainAcknowledgment.setVisibility(true);
      if (data.notes !== null && data.notes !== "" && data.notes !== " ") {
        this.view.confirmDialog.flxContainerDescription.setVisibility(true);
      }
      else
        this.view.confirmDialog.flxContainerDescription.setVisibility(false);
      this.view.confirmDialog.flxPartial.setVisibility(false);
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
      if (data.isScheduled === "true") {
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.loan.transactionMsg") + " " + data.date;
        this.view.acknowledgment.flxBalance.setVisibility(false);
        if (kony.application.getCurrentBreakpoint() === 640) 
          this.view.confirmDialog.top = "20dp";
      } else {
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.PayDueAmount.AcknowlwdgmentMessage");
        this.view.acknowledgment.flxBalance.setVisibility(true);
        if (kony.application.getCurrentBreakpoint() === 640) 
          this.view.confirmDialog.top = "30dp";
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.view.acknowledgment.lblBalance.text = response.balance;
      this.view.acknowledgment.lblAccType.text = response.accountName;
      this.view.confirmDialog.lblValue.text = data.fromAccount;
      this.view.confirmDialog.lblValueTo.text = data.toAccount;
      this.view.confirmDialog.lblValueDescription.text = data.notes;
      this.view.confirmDialog.lblValueAmount.text = data.principalBalance.slice(1);
      this.view.confirmDialog.lblValueDate.text = data.date;
      this.view.confirmDialog.lblValuePenaltyAmount.text = data.payOffCharge;
      this.view.confirmDialog.lblValuePayDate.text = data.date;
      if (applicationManager.getConfigurationManager().showLoanUpdateDisclaimer === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    /**
      * Data Map based on account type
       * @member frmPayDueAmountController
       * @param {object} accounts accounts
       * @returns {void} - None
       * @throws {void} - None
       */
    createAccountSegmentsModel: function (accounts) {
      var accountTypeConfig = {};
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)]= {
          skin: ViewConstants.SKINS.PAYDUE_ACCOUNT_SAVINGS,
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        };
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)]= {
          skin: ViewConstants.SKINS.PAYDUE_ACCOUNT_CHECKING,
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        };
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)]= {
          skin: ViewConstants.SKINS.PAYDUE_ACCOUNT_CREDITCARD,
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        };
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)]= {
          skin: ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_DEPOSIT,
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        };
        accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)]= {
          skin: ViewConstants.SKINS.ACCOUNT_DETAILS_IDENTIFIER_DEPOSIT,
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        };
        accountTypeConfig['Default']= {
          skin: ViewConstants.SKINS.PAYDUE_ACCOUNT_SAVINGS,
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        };
      
      var balanceInDollars = function (amount) {
        return CommonUtilities.formatCurrencyWithCommas(amount);
      };
      return Object.keys(accountTypeConfig).map(function (type) {
        return accounts.filter(function (account) {
          return account.accountType === type;
        });
      }).map(function (accounts) {
        return accounts.map(function (account) {
          return {
            "flxLeft": {
              "skin": accountTypeConfig[account.accountType].skin
            },
            "lblLeft": " ",
            "lblAccountName": account.nickName || account.accountName,
            "lblBalance": balanceInDollars(account[accountTypeConfig[account.accountType].balanceKey]),
            "lblAvailableBalance": accountTypeConfig[account.accountType].balanceTitle
          };
        });
      }).reduce(function (p, e) {
        return p.concat(e);
      }, []);
    },
    /**
     * Function to populate Payment accounts
      * @member frmPayDueAmountController
      * @param {Object} accountModel account
      * @returns {void} - None
      * @throws {void} - None
      */
    updateTransferAccountList: function (accountModel) {
      this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = {
        "lblLeft": "lblLeft",
        "lblAccountName": "lblAccountName",
        "flxLeft": "flxLeft",
        "lblBalance": "lblBalance",
        "lblAvailableBalance": "lblAvailableBalance"
      };
      this.view.mypaymentAccounts.segMypaymentAccounts.setData(this.createAccountSegmentsModel(accountModel));
      this.view.forceLayout();
    },
    /**
    * Update data for Pay Off when TO account selection is changed
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    setDataOnNewSelection: function () {
      this.loadLoanPayModule().presentationController.fetchUpdatedAccountDetails(this.view.LoanPayOff.listbxTo.selectedKey, "newAccountSelection");
    },
    /**
     * Return Date object for timestamp date
      * @member frmPayDueAmountController
      * @param {Date} dateString dateString
      * @returns {Array} date array
      */
    getDateObject: function (dateString) {
      var index = -1;
      index = dateString.indexOf("T");
      if (index !== -1) {
        return applicationManager.getFormatUtilManager().getDateObjectfromString(dateString);
      }else{
        return applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
      } 
    },
    /**
     * Function to activate/deactivate Pay Due Amount button 
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    changeButtonState: function () {
      if (this.view.PayDueAmount.tbxAmount.text.trim() !== "") {
        FormControllerUtility.enableButton(this.view.PayDueAmount.btnPayAmount);
      }
      else {
        FormControllerUtility.disableButton(this.view.PayDueAmount.btnPayAmount);
      }
    },
    
    /**
      * Function to perform action onKeyUp of PayDueAmount textbox
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    amountTextboxAction: function () {
      this.changeButtonState();
      CommonUtilities.validateAmountFieldKeyPress(this.view.PayDueAmount.tbxAmount);
      this.setSkinToFlex(this.view.PayDueAmount.flxtxtbox, this.view.PayDueAmount.flxError);
      this.AdjustScreen();
    },
    /**
     * Function to perform Logout, on confirmation
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    performLogout: function () {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      var context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      this.view.flxLogout.left = "-100%";
    },
    responsiveViews: {},
    initializeResponsiveViews: function () {
      this.responsiveViews["flxDownTimeWarning"] = this.isViewVisible("flxDownTimeWarning");
      this.responsiveViews["flxPayDueAmount"] = this.isViewVisible("flxPayDueAmount");
      this.responsiveViews["flxLoanPayOff"] = this.isViewVisible("flxLoanPayOff");
      this.responsiveViews["flxConfirmation"] = this.isViewVisible("flxConfirmation");
      this.responsiveViews["flxAcknowledgement"] = this.isViewVisible("flxAcknowledgement");
      this.responsiveViews["flxQuitPayment"] = this.isViewVisible("flxQuitPayment");
    },
    isViewVisible: function (container) {
      if (this.view[container].isVisible) {
        return true;
      } else {
        return false;
      }
    },
    /**
    * onBreakpointChange : Handles ui changes on .
    * @param {integer} width - current browser width
    */
    orientationHandler:null,
    onBreakpointChange: function (width) {
      kony.print('on breakpoint change');
      if (this.orientationHandler === null) {
       this.orientationHandler = new OrientationHandler();
      }
      this.orientationHandler.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChange(width);
      this.setupFormOnTouchEnd(width);

      var views;
      this.AdjustScreen();
      var scope = this;
      var responsiveFonts = new ResponsiveFonts();
      if (width === 640) {        
        responsiveFonts.setMobileFonts();
        views = Object.keys(this.responsiveViews);
        views.forEach(function (e) {
          scope.view[e].isVisible = scope.responsiveViews[e];
        });
        if (this.loanContext == "Loan Payoff") {
          this.view.customheader.lblHeaderMobile.text = "Loan Pay Off";
        } else {
          this.view.customheader.lblHeaderMobile.text = "Pay Due Amount";
        }
      } else {
        responsiveFonts.setDesktopFonts();
        views = Object.keys(this.responsiveViews);
        views.forEach(function (e) {
          scope.view[e].isVisible = scope.responsiveViews[e];
        });
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.PayDueAmount.lblDueDate.left = "50.8%";
      }
    },
        setupFormOnTouchEnd: function(width){
          if(width==640){
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }else{
            if(width==1024){
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }else{
                this.view.onTouchEnd = function(){
                  hidePopups();   
                } 
              }
              var userAgent = kony.os.deviceInfo().userAgent;
              if (userAgent.indexOf("iPad") != -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }
          }
        },
        nullifyPopupOnTouchStart: function(){
        }
  };
});