define({ 

	showSelectedRow: function(){
		var index = kony.application.getCurrentForm().tableView.segP2P.selectedIndex;
		var rowIndex = index[1];
		var data = kony.application.getCurrentForm().tableView.segP2P.data;
		data[rowIndex].template = "flxReceived";
		kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[rowIndex], rowIndex);
	    this.AdjustScreen(30);
    },
	showRequestMoney: function(){
		kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin="sknBtnAccountSummarySelected";
		kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(true);
		kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
		kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
        this.AdjustScreen(30);
	},
  //UI Code
  AdjustScreen: function(data) {
    var currForm = kony.application.getCurrentForm();
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = currForm.customheader.frame.height + currForm.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - currForm.flxFooter.frame.height;
        if (diff > 0) 
            currForm.flxFooter.top = mainheight + diff + data + "dp";
        else
            currForm.flxFooter.top = mainheight + data + "dp";        
     } else {
        currForm.flxFooter.top = mainheight + data + "dp";
     }
    currForm.forceLayout();
  },  


 });