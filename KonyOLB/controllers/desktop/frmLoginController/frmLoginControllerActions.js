define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onBeginEditing defined for tbxUserName **/
    AS_TextField_ed806356b3314d86b37761258f426fca: function AS_TextField_ed806356b3314d86b37761258f426fca(eventobject, changedtext) {
        var self = this;
        this.showUserNamesBasedOnlength()
    },
    /** onEndEditing defined for tbxUserName **/
    AS_TextField_ba5381fb58bc4e69a7e5366199a2aecb: function AS_TextField_ba5381fb58bc4e69a7e5366199a2aecb(eventobject, changedtext) {
        var self = this;
        this.hideUserNames();
    },
    /** onKeyUp defined for tbxUserName **/
    AS_TextField_cfa0d44f49e44ed09ae7f7c35af168f4: function AS_TextField_cfa0d44f49e44ed09ae7f7c35af168f4(eventobject) {
        var self = this;
        this.checkifUserNameContainsMaskCharacter();
    },
    /** onRowClick defined for segUsers **/
    AS_Segment_i76f4b293b0a49e9adc3d42e03760f00: function AS_Segment_i76f4b293b0a49e9adc3d42e03760f00(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectUserName();
    },
    /** onDone defined for tbxPassword **/
    AS_TextField_bcdc10ae8b1a403c82fa05d79cf6b6df: function AS_TextField_bcdc10ae8b1a403c82fa05d79cf6b6df(eventobject, changedtext) {
        var self = this;
        this.toEnableOrDisableLogin();
    },
    /** onBeginEditing defined for tbxPassword **/
    AS_TextField_e48f6a4124bf4de286d0c4a912983d38: function AS_TextField_e48f6a4124bf4de286d0c4a912983d38(eventobject, changedtext) {
        var self = this;
        if (this.view.main.flxPassword.skin == "sknBorderFF0101") {
            this.credentialsMissingUIChangesAnti();
        }
    },
    /** onKeyUp defined for tbxPassword **/
    AS_TextField_d95ee59d841c471cb206a8bf70db7295: function AS_TextField_d95ee59d841c471cb206a8bf70db7295(eventobject) {
        var self = this;
        this.enableLogin(this.view.main.tbxUserName.text.trim(), this.view.main.tbxPassword.text);
    },
    /** onClick defined for flxClick **/
    AS_FlexContainer_d182f34b5edc46929be1cbb0d6a10db9: function AS_FlexContainer_d182f34b5edc46929be1cbb0d6a10db9(eventobject) {
        var self = this;
        this.rememberMe();
    },
    /** onClick defined for btnForgotPassword **/
    AS_Button_h44fea635e804886ab7d28fdf194cd62: function AS_Button_h44fea635e804886ab7d28fdf194cd62(eventobject) {
        var self = this;
        this.verifyUser();
    },
    /** onClick defined for btnLogin **/
    AS_Button_d6245c2a909d4dcaad06d0912e45fb28: function AS_Button_d6245c2a909d4dcaad06d0912e45fb28(eventobject) {
        var self = this;
        this.onLoginClick();
    },
    /** onClick defined for btnOnlineAccessEnroll **/
    AS_Button_a92df3783b20499aaf1ba139e65eecd5: function AS_Button_a92df3783b20499aaf1ba139e65eecd5(eventobject) {
        var self = this;
        this.verifyUser();
    },
    /** onClick defined for btnOpenNewAccount **/
    AS_Button_fc3e3184ebb74a5e92ab1fda8b85ec4a: function AS_Button_fc3e3184ebb74a5e92ab1fda8b85ec4a(eventobject) {
        var self = this;
        this.navigateToNewUserOnBoarding();
    },
    /** onKeyUp defined for tbxLastName **/
    AS_TextField_ab492a75ee104b79bcef227862bd1dd1: function AS_TextField_ab492a75ee104b79bcef227862bd1dd1(eventobject, changedtext) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onKeyUp defined for tbxSSN **/
    AS_TextField_fecb63274ea6496298bc7eb0399a4e65: function AS_TextField_fecb63274ea6496298bc7eb0399a4e65(eventobject, changedtext) {
        var self = this;
        this.allFieldsCheck();
        this.ssnCheck();
    },
    /** onSelection defined for lbxYear **/
    AS_ListBox_gcfe9c71f0534d908d98a6f965b3fd53: function AS_ListBox_gcfe9c71f0534d908d98a6f965b3fd53(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxMonth **/
    AS_ListBox_f02eb01ff527451f94db938c8addb1e6: function AS_ListBox_f02eb01ff527451f94db938c8addb1e6(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onSelection defined for lbxDate **/
    AS_ListBox_fbceb21f4ddd4de1a9a20791e2251754: function AS_ListBox_fbceb21f4ddd4de1a9a20791e2251754(eventobject) {
        var self = this;
        this.allFieldsCheck();
    },
    /** onClick defined for btnProceed **/
    AS_Button_df12693ac5b44c3b9864b1091128b959: function AS_Button_df12693ac5b44c3b9864b1091128b959(eventobject) {
        var self = this;
        this.verifyUserDetails();
    },
    /** onClick defined for btnNext **/
    AS_Button_je1a71e9e3a441829c006de52cf56f16: function AS_Button_je1a71e9e3a441829c006de52cf56f16(eventobject) {
        var self = this;
        this.requestOTPValue();
    },
    /** onClick defined for btnUseCVV **/
    AS_Button_b014ab53d3c6421cb0d39f83ae5e147f: function AS_Button_b014ab53d3c6421cb0d39f83ae5e147f(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onTouchStart defined for imgViewCVV **/
    AS_Image_a77ff3d5f598477d96ba8a7a74a917ea: function AS_Image_a77ff3d5f598477d96ba8a7a74a917ea(eventobject, x, y) {
        var self = this;
        this.showOTP();
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_j0019f86f656471d85690a0d9c52a0fe: function AS_TextField_j0019f86f656471d85690a0d9c52a0fe(eventobject, changedtext) {
        var self = this;
        this.reTypeOTP();
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_g6943b93f48a40b4b3253ac5cc4bacc3: function AS_TextField_g6943b93f48a40b4b3253ac5cc4bacc3(eventobject) {
        var self = this;
        this.otpCheck();
    },
    /** onClick defined for btnNext **/
    AS_Button_e674540514d44d46a690c2ba43e3d1bf: function AS_Button_e674540514d44d46a690c2ba43e3d1bf(eventobject) {
        var self = this;
        this.isOTPCorrect();
    },
    /** onClick defined for btnUseCVV **/
    AS_Button_bdb7d79246ab43e38d477d71090512cd: function AS_Button_bdb7d79246ab43e38d477d71090512cd(eventobject) {
        var self = this;
        this.useCVVForReset();
    },
    /** onTouchStart defined for lstbxCards **/
    AS_ListBox_c790c4d06bea4977bc373f4d4d1d535c: function AS_ListBox_c790c4d06bea4977bc373f4d4d1d535c(eventobject, x, y) {
        var self = this;
        // this.presenter.showCVVCards(this);
    },
    /** onTouchStart defined for imgViewCVV **/
    AS_Image_jdbb4394f1144542a7f3b2f91a6381cc: function AS_Image_jdbb4394f1144542a7f3b2f91a6381cc(eventobject, x, y) {
        var self = this;
        this.showCVV();
    },
    /** onBeginEditing defined for tbxCVV **/
    AS_TextField_c2d9de5f0b544570a165739090cb744c: function AS_TextField_c2d9de5f0b544570a165739090cb744c(eventobject, changedtext) {
        var self = this;
        this.reEnterCVV();
    },
    /** onKeyUp defined for tbxCVV **/
    AS_TextField_e1039fff3fb44a11822e0ea10aa5484e: function AS_TextField_e1039fff3fb44a11822e0ea10aa5484e(eventobject) {
        var self = this;
        this.cvvCheck();
    },
    /** onClick defined for btnNext **/
    AS_Button_ec982f6c3e4d43dda32d288ea7cb89ea: function AS_Button_ec982f6c3e4d43dda32d288ea7cb89ea(eventobject) {
        var self = this;
        this.isCVVCorrect();
    },
    /** onClick defined for btnUseOTP **/
    AS_Button_da99080919b041c4ac365b2690a28094: function AS_Button_da99080919b041c4ac365b2690a28094(eventobject) {
        var self = this;
        this.useOTPForReset();
    },
    /** onTouchStart defined for imgValidPassword **/
    AS_Image_fd08ca4c74b94e93b7bf00984af3b4f3: function AS_Image_fd08ca4c74b94e93b7bf00984af3b4f3(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgValidPassword **/
    AS_Image_h482f0f304b04288a5767a79c99c6f29: function AS_Image_h482f0f304b04288a5767a79c99c6f29(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onBeginEditing defined for tbxNewPassword **/
    AS_TextField_ff4ed44c808349aea23a3cdb203e1b63: function AS_TextField_ff4ed44c808349aea23a3cdb203e1b63(eventobject, changedtext) {
        var self = this;
        this.passwordEditing();
    },
    /** onKeyUp defined for tbxNewPassword **/
    AS_TextField_j3d7e710b1c447708db8934acdc6d121: function AS_TextField_j3d7e710b1c447708db8934acdc6d121(eventobject) {
        var self = this;
        this.validateNewPassword(this.view.newpasswordsetting.tbxNewPassword.text);
    },
    /** onKeyUp defined for tbxMatchPassword **/
    AS_TextField_fef60f6dfd0b4e02928bb2a0fa24be8d: function AS_TextField_fef60f6dfd0b4e02928bb2a0fa24be8d(eventobject) {
        var self = this;
        this.matchPwdKeyUp();
    },
    /** onClick defined for btnNext **/
    AS_Button_g8926370565949d99200dc018568c10c: function AS_Button_g8926370565949d99200dc018568c10c(eventobject) {
        var self = this;
        this.showResetConfirmationPage();
    },
    /** onClick defined for btnProceed **/
    AS_Button_c3f3b614ef974d01822ba57f93ce1714: function AS_Button_c3f3b614ef974d01822ba57f93ce1714(eventobject) {
        var self = this;
        this.letsGetStarted();
    },
    /** onClick defined for btnEnroll **/
    AS_Button_e5560a494d924458bc9240c27758a826: function AS_Button_e5560a494d924458bc9240c27758a826(eventobject) {
        var self = this;
        this.loginLater(this.view.flxEnrollOrServerError);
    },
    /** onTouchStart defined for lblHowToEnroll **/
    AS_Label_hbeccaed576f4fe2b9a931795bd748aa: function AS_Label_hbeccaed576f4fe2b9a931795bd748aa(eventobject, x, y) {
        var self = this;
        this.returnToLogin();
    },
    /** onClick defined for btnBackToLogin **/
    AS_Button_hb4e02e809db4e1bae0ba4b9556b99ea: function AS_Button_hb4e02e809db4e1bae0ba4b9556b99ea(eventobject) {
        var self = this;
        this.loginLater(this.view.flxEnrollOrServerError);
    },
    /** onRowClick defined for segLanguagesList **/
    AS_Segment_a7f920ba00b74a61829eea1648992834: function AS_Segment_a7f920ba00b74a61829eea1648992834(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectYourLanguage();
    },
    /** onTouchEnd defined for lblLanguage **/
    AS_Label_bf8f6b3c9e744371b2d69642561a70dc: function AS_Label_bf8f6b3c9e744371b2d69642561a70dc(eventobject, x, y) {
        var self = this;
        this.showLanguageOptions();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_dd1259ed31c44c9ca451566656049a34: function AS_FlexContainer_dd1259ed31c44c9ca451566656049a34(eventobject) {
        var self = this;
        this.showLanguageOptions();
    },
    /** onClick defined for btnYes **/
    AS_Button_f30ad6790c514d8b9e3a3b6642ab8841: function AS_Button_f30ad6790c514d8b9e3a3b6642ab8841(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNo **/
    AS_Button_ja24bde46bf44018b58917cff76e85aa: function AS_Button_ja24bde46bf44018b58917cff76e85aa(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_f40184f7a43448b0ab98aa871fa5f37b: function AS_FlexContainer_f40184f7a43448b0ab98aa871fa5f37b(eventobject) {
        var self = this;
        this.flxCrossTakeSurvey();
    },
    /** preShow defined for frmLogin **/
    AS_Form_a00d69b6a6fe4e8f982b0de8159a5f3b: function AS_Form_a00d69b6a6fe4e8f982b0de8159a5f3b(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** postShow defined for frmLogin **/
    AS_Form_df5e80bd9a794da2b4e45afaa435f2e4: function AS_Form_df5e80bd9a794da2b4e45afaa435f2e4(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** onDeviceBack defined for frmLogin **/
    AS_Form_c876da882d124a0aa022ca5dc35ec6d6: function AS_Form_c876da882d124a0aa022ca5dc35ec6d6(eventobject) {
        var self = this;
        kony.print("User pressed back button");
    },
    /** onTouchEnd defined for frmLogin **/
    AS_Form_he4ff28aa4df4130a957effb7bd85ce0: function AS_Form_he4ff28aa4df4130a957effb7bd85ce0(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});