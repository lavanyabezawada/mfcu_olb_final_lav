/**
 * Module representing a StopCheckPayements.
 * @module frmStopPaymentsController
 */

define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {

    return {
        /**
         * frmStopPaymentsInitAction : Form Init action handler
         */
        frmStopPaymentsInitAction: function () {
            var scopeObj = this;
            this.view.onBreakpointChange = function () {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.view.ConfirmDetailsSingleMultiple.skin="sknFlxffffffShadowdddcdc";
            this.view.MyRequestsTabs.skin="slFbox";
            this.view.flxSuccessMessageStopCheck.clipBounds=false;
            this.setFormActions();
            this.setFooter();
            this.clearData();
            this.view.StopCheckPaymentSeriesMultiple.lblSingleMultipleChecks.text = kony.i18n.getLocalizedString("i18n.StopPayments.SingleCheck");
            this.view.MyRequestsTabs.lblStatusDC.text = kony.i18n.getLocalizedString("i18n.billPay.Status");
            this.view.ConfirmDetailsSingleMultiple.btnViewRequests.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests"));
            this.view.ConfirmDetailsSingleMultiple.btnBackToAccoutnDetails.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails");
          	this.view.StopCheckPaymentSeriesMultiple.btnCancel.height = "40dp";
          	this.view.StopCheckPaymentSeriesMultiple.btnProceed.height = "40dp";
          	this.view.ConfirmDetailsSingleMultiple.btnConfirm.height = "40dp";
          	this.view.ConfirmDetailsSingleMultiple.btnModify.height = "40dp";
          	this.view.ConfirmDetailsSingleMultiple.btnCancel.height = "40dp";
          	this.view.MyRequestsTabs.flxBody.minHeight = "500dp";
            this.AdjustScreen();
        },

        /**
        * getPageHeight : Return Page height.
        * @return {string} height 
        */
        getPageHeight: function () {
            var footerHeight = this.view.flxFooter.frame.height +  (this.view.flxFooter.frame.y-(this.view.flxHeader.frame.height + this.view.flxContainer.frame.height));
            var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + footerHeight;
            return height;

            return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height;
        },

        /**
         * clearData : clear dummy/sample data.
         */
        clearData: function () {
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.segTransactions.setData([]);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData([]);
        },
        /**
        * setFooter : set form footer.
        */
        setFooter: function () {
            this.view.flxFooter.setVisibility(true);
        },

        /**
   * Method to load and return Stop payements 
   * @returns {object} Stop Payements Module object.
   */
        loadStopPaymentsModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
        },
        /**
        * postShowfrmStopCheckPayments : Form postShow action handler
        */
        postShowfrmStopCheckPayments: function () {
            this.AdjustScreen();
            this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "Stop Payment Requests");
            this.view.customheader.forceCloseHamburger();
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.lblPrintfontIconConfirm.onClick = this.showPrintPage.bind(this);
            this.view.lblPrintACK.onClick = this.showStopChecksPrintPage.bind(this);
          	var scopeObj = this;
          	this.view.btnNew.setVisibility(true);
        	if(kony.application.getCurrentBreakpoint() === 640){
              this.view.customheader.topmenu.flxMenu.onTouchStart = function () {
                scopeObj.view.btnNew.setVisibility(false);
              };
              this.view.customheader.customhamburger.flxClose.onTouchStart = function () {
                scopeObj.view.btnNew.setVisibility(true);
              };
            }
        },
        showPrintPage : function(){
            var stopTransctionData = [];
            stopTransctionData.push({
                key: kony.i18n.getLocalizedString("i18n.common.status"),
              value: this.view.acknowledgmentMyRequests.lblTransactionMessage.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueBankName.lblKey.text,
              value: this.view.confirmDialog.keyValueBankName.lblValue.text 
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueCountryName.lblKey.text,
              value: this.view.confirmDialog.keyValueCountryName.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueAccountType.lblKey.text,
              value: this.view.confirmDialog.keyValueAccountType.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueAccountNumber.lblKey.text,
              value: this.view.confirmDialog.keyValueAccountNumber.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueRoutingNumber.lblKey.text,
              value: this.view.confirmDialog.keyValueRoutingNumber.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueBenificiaryName.lblKey.text,
              value: this.view.confirmDialog.keyValueBenificiaryName.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.keyValueAccountNickName.lblKey.text,
              value: this.view.confirmDialog.keyValueAccountNickName.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.flxDileveryBy.lblKey.text,
              value: this.view.confirmDialog.flxDileveryBy.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.confirmDialog.flxDescription.lblKey.text,
              value: this.view.confirmDialog.flxDescription.lblValue.text
            });
            stopTransctionData.push({
              key: this.view.acknowledgmentMyRequests.lblRefrenceNumber.text,
              value: this.view.acknowledgmentMyRequests.lblRefrenceNumberValue.text
            });

            var printCallback = function(){
                applicationManager.getNavigationManager().navigateTo("frmStopPayments");    
                applicationManager.getNavigationManager().updateForm({disputeTransactionResponse:{isDataBindedAlready: true}}, "frmStopPayments");
            }
            var viewModel = {
                moduleHeader : this.view.lblBillPayAcknowledgement.text,
                tableList  : [
                        { 
                            tableHeader : this.view.confirmDialog.confirmHeaders.lblHeading.text,
                            tableRows : stopTransctionData
                        }
                    ],
                printCallback  : printCallback
            }
            this.loadStopPaymentsModule().presentationController.showPrintPage({printKeyValueGroupModel:viewModel});            
        },

        showStopChecksPrintPage : function(){
            var stopChecksData = [];
            stopChecksData.push({
                key: kony.i18n.getLocalizedString("i18n.common.status"),
                value: this.view.lblSuccessAck.text
            });
            stopChecksData.push({
              key: this.view.lblreferenceno.text,
              value:  this.view.lblReferenceNumber2.text
            });
            stopChecksData.push({
              key: this.view.ConfirmDetailsSingleMultiple.lblReason.text,
              value:  this.view.ConfirmDetailsSingleMultiple.rtxReason.text
            });
            stopChecksData.push({
              key: this.view.ConfirmDetailsSingleMultiple.lblDescription.text,
              value:  this.view.ConfirmDetailsSingleMultiple.rtxDescription.text
            });

            var row = this.view.ConfirmDetailsSingleMultiple.segConfirmDetails.data[0];
            stopChecksData.push({
                key: row.lblFrom1,
                value:  row.rtxFrom1
            });
            stopChecksData.push({
                key: row.lblPayee1,
                value:  row.rtxPayee1
            });
            stopChecksData.push({
                key: row.lblDate1,
                value:  row.rtxDate1.text || row.rtxDate1
            });
            if(row.rtxAmount1){
                stopChecksData.push({
                    key: row.lblAmount1,
                    value:  row.rtxAmount1.text || row.rtxAmount1
                });
            }
            if(row.rtxNotes1){
                stopChecksData.push({
                    key: row.lblNotes1,
                    value:  row.rtxNotes1.text || row.rtxNotes1
                });
            }

            var printCallback = function(){
                applicationManager.getNavigationManager().navigateTo("frmStopPayments");    
                applicationManager.getNavigationManager().updateForm({successStopCheckRequest:{isDataBindedAlready: true}}, "frmStopPayments");
            }
            var viewModel = {
                moduleHeader : this.view.lblHeader.text,
                tableList  : [
                        { 
                            tableHeader : this.view.ConfirmDetailsSingleMultiple.lblConfirmHeader.text,
                            tableRows : stopChecksData
                        }
                    ],
                printCallback  : printCallback
            }
            this.loadStopPaymentsModule().presentationController.showPrintPage({printKeyValueGroupModel:viewModel});
        },

         /**
         * Form preShow action handler
         */
        preShowStopPayments:function(){
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Stop Payments");
            this.view.ConfirmDetailsSingleMultiple.skin="sknFlxffffffShadowdddcdc";
            this.view.MyRequestsTabs.skin="slFbox";
            this.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.hidePreviousNextMonthDates = true;
          applicationManager.getNavigationManager().applyUpdates(this);
        },
        /**
         * AdjustScreen : Ui team proposed method to handle screen aligment
         */
        AdjustScreen: function () {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },

        /** 
        * setBreadcrumb :Resets Bread crumb state
        * @param {Array} viewModel , Breadcrumb lables object array { lable , toolTip}
        */
        setBreadcrumb: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.length > 1) {
                scopeObj.view.breadcrumb.setBreadcrumbData([{
                    text: viewModel[0].label
                }, {
                    text: viewModel[1].label
                }]);
                scopeObj.view.breadcrumb.btnBreadcrumb1.toolTip = viewModel[0].toolTip;
                scopeObj.view.breadcrumb.lblBreadcrumb2.toolTip = viewModel[1].toolTip;
            }
        },

        /**
         * setFormActions : Method to bind all action in form
         */
        setFormActions: function () {
            var scopeObj = this;

            scopeObj.view.btnViewDisputedTransactions.onClick = scopeObj.btnViewDisputedTransactionsClickHandler.bind(scopeObj);
            scopeObj.view.btnViewDisputedTransactions.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewDisputedTransactions"));
            scopeObj.view.btnViewDisputedChecks.onClick = scopeObj.btnViewDisputedChecks.bind(scopeObj);
            scopeObj.view.btnViewDisputedChecks.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewDisputedChecks"));

            scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.onClick = scopeObj.onSingleOrMultipleRadioButtonClick.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.onClick = scopeObj.onSeriesRadioButtonClick.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxTCContentsCheckbox.onClick = scopeObj.onTCCClickHanlder.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnTermsAndConditions.onClick = scopeObj.onTandCBtnClickHandler.bind(scopeObj);
            scopeObj.view.flxTCContentsCheckbox.onClick = scopeObj.onTandContentCheckBoxClickHandler.bind(scopeObj);
            scopeObj.view.btnSave.onClick = scopeObj.onTandContentSaveClickHandler.bind(scopeObj);
            scopeObj.view.btnCancel.onClick = scopeObj.setTermsAndConditionsPopupState.bind(scopeObj, false);
            scopeObj.view.flxClose.onClick = scopeObj.setTermsAndConditionsPopupState.bind(scopeObj, false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.onTextChange = scopeObj.onSeriesLastCheck2TextChange.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.onTextChange = scopeObj.onSeriesLastCheckTextChange.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.onTextChange = scopeObj.onSeriesLastCheck2TextChange.bind(scopeObj);

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            FormControllerUtility.wrapAmountField(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount)
                                 .onKeyUp(scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj));
            scopeObj.view.imgCloseDowntimeWarning.onTouchEnd = function () {
                scopeObj.setServerError(false);
            };
            this.view.CancelStopCheckPayments.flxCross.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
            };
            this.view.CancelStopCheckPayments.btnNo.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
            };
            this.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.onClick = function () {
                scopeObj.noOfClonedChecks++;
                var newCheck = scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.clone(scopeObj.noOfClonedChecks);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxStopCheckPayments.addAt(newCheck, 3);
            };
        },

        /**
         * setDisputedTransactionTabUI: Method to set My requests Disputed Transactions tab UI
         */
        setDisputedTransactionTabUI: function () {
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.skin = OLBConstants.SKINS.STOPPAYMENTS_SELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.skin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.hoverSkin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_HOVER;
            scopeObj.ShowAllSeperators();
            scopeObj.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(false);
            scopeObj.view.MyRequestsTabs.lblSortDate.setFocus(true);
            scopeObj.view.forceLayout();
        },

        /**
         * setDisputedChecksTabUI: Method to set My requests Disputed Checks tab UI
         */
        setDisputedChecksTabUI: function () {
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.skin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.hoverSkin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_HOVER;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.skin = OLBConstants.SKINS.STOPPAYMENTS_SELECT_TAB;
            scopeObj.ShowAllSeperators();
            scopeObj.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(false);
            scopeObj.view.MyRequestsTabs.lblSortDateDC.setFocus(true);
            scopeObj.view.forceLayout();
        },
        /**
         * ShowAllSeperators
         */
        ShowAllSeperators: function () {
            // this.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(true);
            // this.view.MyRequestsTabs.flxTabsSeperator2.setVisibility(true);
            // this.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(true);
            this.view.forceLayout();
        },

        /**
         * showDisputeTransactionDetail : update flexes visibility w.r.t dispute transaction form
         */
        showDisputeTransactionDetail: function () {
            this.hideAll();

            this.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.accounts.disputeTransaction"),
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.disputeTransaction")
                },
            ]);
            this.view.flxDisputedTransactionDetail.setVisibility(true);
            this.view.lblStopPayments.setVisibility(true);
            this.view.flxActionsDisputeTransactionDetails.setVisibility(true);
            this.view.flxDisputedTransactionDetails.setVisibility(true);
            this.view.DisputeTransactionDetail.setVisibility(true);
            this.view.StopCheckPaymentSeriesMultiple.setVisibility(false);
            this.view.DisputeTransactionDetail.confirmButtons.btnModify.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.view.DisputeTransactionDetail.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputeTransactionDetails");
            this.view.DisputeTransactionDetail.confirmButtons.btnModify.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.transfers.Cancel"));
            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.common.proceed"));

            this.AdjustScreen();
        },

        /**
        * showStopCheckPaymentsSeriesMultiple : update flexes visibility w.r.t stop checks form
        */
        showStopCheckPaymentsSeriesMultiple: function () {
            var scopeObj = this;
            scopeObj.hideAll();
            scopeObj.view.lblStopPayments.setVisibility(true);
            scopeObj.view.flxDisputedTransactionDetail.setVisibility(true);
            scopeObj.view.flxDisputedTransactionDetails.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.setVisibility(true);
            scopeObj.view.flxActionsDisputeTransactionDetails.setVisibility(true);
            scopeObj.view.DisputeTransactionDetail.setVisibility(false);
            scopeObj.setValidationErrorMessageState(false);
            scopeObj.view.forceLayout();
        },

        /**
         * hideAll : Method to show hide all flexes in form
         */
        hideAll: function () {
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.flxDisputedTransactionDetail.setVisibility(false);
            this.view.StopCheckPaymentSeriesMultiple.setVisibility(false);
            this.view.DisputeTransactionDetail.setVisibility(false);
            this.view.lblStopPayments.setVisibility(false);           
            this.view.flxMyRequestsTabs.setVisibility(false);
            this.view.flxSinglePayConfirm.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxPrintACK.setVisibility(false);
            this.view.forceLayout();
        },

        /**
         * shouldUpdateUI : Method to decide whether view model is defined or not 
         * @param {object} viewModel, view model object
         * @return {bolean} true/false , whether view model is defined or not 
         */
        shouldUpdateUI: function (viewModel) {
            return viewModel !== undefined && viewModel !== null;
        },

        /**
         * updateFormUI : Method to update form UI w.r.t view data called by presentUserInterface
         * @param {obejct} stopPayementsView  view model object
         */
        updateFormUI: function (stopPayementsView) {
            var scopeObj = this;
            if (stopPayementsView.progressBar !== undefined) {
                if (stopPayementsView.progressBar) {
                    scopeObj.setServerError(false);
                    FormControllerUtility.showProgressBar(scopeObj.view);
                } else {
                    FormControllerUtility.hideProgressBar(scopeObj.view);
                }
            } else {

                if (stopPayementsView.serverError) {
                    scopeObj.setServerError(stopPayementsView.serverError);
                } else if (stopPayementsView.stopChecksFormData) {
                    scopeObj.showStopChecksForm(stopPayementsView.stopChecksFormData);
                } else if (stopPayementsView.stopChecksFormAccounts) {
                    scopeObj.setStopChecksFormAccountsDropdown({
                        "ddnList": stopPayementsView.stopChecksFormAccounts
                    });
                } else if (stopPayementsView.disputeTransactionObject) {
                    scopeObj.showDisputeTransactionDetailPage(stopPayementsView.disputeTransactionObject);
                } else if (stopPayementsView.successStopCheckRequest) {
                    scopeObj.showStopChekRequestAcknowledgment(stopPayementsView.successStopCheckRequest);
                } else if (stopPayementsView.disputeTransactionResponse) {
                    scopeObj.showConfirmationDisputeTransaction(stopPayementsView.disputeTransactionResponse);
                } else if (stopPayementsView.myRequests) {
                    scopeObj.showMyRequests(stopPayementsView.myRequests);
                } else if (stopPayementsView.stopCheckRequestsViewModel) {
                    scopeObj.showStopCheckRequests(stopPayementsView.stopCheckRequestsViewModel);
                } else if (stopPayementsView.viewDisputedRequestsResponse) {
                    scopeObj.showDisputeTransactionsRequests(stopPayementsView.viewDisputedRequestsResponse);
                } else if (stopPayementsView.cancelStopCheckAction) {
                    scopeObj.showCancelStopRequestUI(stopPayementsView.cancelStopCheckAction);
                }
            }
            scopeObj.AdjustScreen();
        },

        /**
         * bindDisputeTransactionRequestsData : set widget data map for dispute transactions view requests
         * @param {Array} disputeTransactionRequests transactions view model
         */
        bindDisputeTransactionRequestsData: function (disputeTransactionRequests) {
            var scopeObj = this;
            var break_point = kony.application.getCurrentBreakpoint();
            var widgetDataMap = {
                "lblIdentifier": "lblIdentifier",
                "lblSeparator2": "lblSeparator2",
                "lblSeperator2": "lblSeperator2",
                "lblSeparator": "lblSeparator",
                "lblSeparatorActions": "lblSeparatorActions",
                "imgDropDown": "imgDropDown",
                "lblDate": "lblDate",
                "lblDate1": "lblDate1", 
                "btnCancelRequests": "btnCancelRequests",
                "btnSendAMessage": "btnSendAMessage",
                "lblDescription": "lblDescription",
                "lblFromAccount": "lblFromAccount",
                "lblFromAccountData": "lblFromAccountData",
                "lblDateOfDescriptionKey": "lblDateOfDescriptionKey",
                "lblDateOfDescriptionValue": "lblDateOfDescriptionValue",
                "lblReasonKey": "lblReasonKey",
                "lblReasonValue": "lblReasonValue",
                "lblReferenceNo": "lblReferenceNo",
                "lblToAccount": "lblToAccount",
                "lblToAccountData": "lblToAccountData",
                "CopylblFrequencyTitle0c4e7bef1ab0c44": "CopylblFrequencyTitle0c4e7bef1ab0c44",
                "lblTransactionTypeValue": "lblTransactionTypeValue",
                "lblDesciptionKey": "lblDesciptionKey",
                "lblDescriptionValue": "lblDescriptionValue",
                "lblAmountKey": "lblAmountKey", 
                "lblStatus": "lblStatus",
                "flxDropdown": "flxDropdown",
                "lblDescriptionHeading":"lblDescriptionHeading",
                "lblToAccount2":"lblToAccount2",
                "lblToAccountData2": "lblToAccountData2",
                "lblToAccountData1": "lblToAccountData1",
                "lblAmount":"lblAmount",
                "lblTransactionDetails":"lblTransactionDetails",
                "lblToAccount1":"lblToAccount1"
            };
            var dataMap = disputeTransactionRequests.map(function (requestObj) {
                return {
                    "template": "flxDisputedTransactionsUnSelected",
                    "imgDropDown": OLBConstants.IMAGES.ARRAOW_DOWN,
                    "flxDropdown": "flxDropdown",
                    "lblDate": requestObj.disputeDate,

                    "btnSendAMessage": {
                        "text": kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage"),
                        "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage")),
                        "onClick": requestObj.onSendMessageAction ? requestObj.onSendMessageAction : null,
                        "isVisible": requestObj.onSendMessageAction ? true : false
                    },

                    "btnCancelRequests": {
                        "isVisible": requestObj.onCancelRequest ? true : false,
                        "text": kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST"),
                        "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST")),
                        "onClick": requestObj.onCancelRequest

                    },
                    "lblDescription": requestObj.transactionDesc,
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator2": "lblSeperator2",
                    "lblSeparator": "lblSeparator",
                    "lblSeparatorActions": "lblSeparatorActions",

                    "lblFromAccount": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                    "lblFromAccountData": requestObj.fromAccount,
                    "lblDateOfDescriptionKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.DateOfTransaction"),
                    "lblDateOfDescriptionValue": requestObj.transactionDate,
                    "lblReasonKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                    "lblReasonValue": requestObj.disputeReason,
                    "lblReferenceNo": requestObj.transactionId,
                    "lblToAccount": kony.i18n.getLocalizedString("i18n.PayAPerson.ToAccount"),
                    "lblToAccountData": requestObj.toAccountName,
                    "CopylblFrequencyTitle0c4e7bef1ab0c44": kony.i18n.getLocalizedString("i18n.accounts.TransactionType"),
                    "lblTransactionTypeValue": requestObj.transactionType,
                    "lblDesciptionKey": kony.i18n.getLocalizedString("i18n.StopPayments.Description"),
                    "lblDescriptionValue": requestObj.disputeDescription,
                    "lblAmount": requestObj.amount,
                    "lblStatus": requestObj.disputeStatus,
                    "lblDate1": requestObj.disputeDate,
                    "lblDescriptionHeading": "Description",
                    "lblAmountKey": "Amount",
                    "lblToAccount1": "To:",
                    "lblToAccount2": "Billpay-",
                    "lblToAccountData2": requestObj.toAccountName,
                    "lblToAccountData1": requestObj.toAccountName,
                    "lblTransactionDetails":"Transaction Details"
                };
            });
            scopeObj.view.MyRequestsTabs.segTransactions.widgetDataMap = widgetDataMap;
            if (break_point == 640) {
                for (var i = 0; i < dataMap.length; i++) {
                    dataMap[i].template = "flxDisputedTransactionsUnSelectedMobile";
                }
            }
            scopeObj.view.MyRequestsTabs.segTransactions.setData(dataMap);
            if (CommonUtilities.isPrintEnabled()) {
                scopeObj.view.flxPrintACK.setVisibility(true);
              } else {
                scopeObj.view.flxPrintACK.setVisibility(false);
              }
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(true);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
          * bindDisputeTransactionReasonsDropdown : Method to bind Stop checks form reasons dropdown
          * @param {object} viewModel dropdown view model
          */
        bindDisputeTransactionReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.list.length) {
                scopeObj.view.DisputeTransactionDetail.lstBoxSelectReasonForDispute.masterData = viewModel.list.map(function (reason) {
                    return [reason.id, reason.name];
                });
            }
        },

        /**
        * setDisputeTransactionReasonsDropdown : Method to set dispute transaction reasons dropdown 
        * @param {object} viewModel - dropdown view model
        */
        setDisputeTransactionReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindDisputeTransactionReasonsDropdown({
                    list: viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKey = viewModel.selectedValue || scopeObj.savedSeriesCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
                * showConfirmationDisputeTransaction : Method to show showConfirmationDisputeTransaction
                * @param {object} viewModel response
                */
        showConfirmationDisputeTransaction: function (viewModel) {
            this.hideAll();
            this.view.flxAcknowledgement.setVisibility(true);
            this.view.flxAcknowledgementMain.setVisibility(true);
            if(viewModel.isDataBindedAlready){
                return;
            }
            this.view.customheader.lblHeaderMobile.text=this.view.lblBillPayAcknowledgement.text;
            this.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
            this.view.confirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
            this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails");
            this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
            this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails");
            this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
            this.view.acknowledgmentMyRequests.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
            this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck");

            this.view.acknowledgmentMyRequests.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
            this.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck")
                }
            ]);

            this.view.confirmDialog.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)");
            this.view.confirmDialog.keyValueRoutingNumber.lblValue.text = viewModel.values.amount || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.flxDescription.lblValue.text = viewModel.values.description || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueAccountNumber.lblValue.text = viewModel.values.notes || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueBankName.lblValue.text = viewModel.values.fromAccountNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueAccountNickName.lblValue.text = viewModel.values.referenceNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.flxDileveryBy.lblValue.text = viewModel.values.reason || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueCountryName.lblValue.text = viewModel.values.toAccount || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueAccountType.lblValue.text = viewModel.values.date || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.confirmDialog.keyValueBenificiaryName.lblValue.text = viewModel.values.types || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.btnMakeTransfer.onClick = viewModel.values.onBacktoAccountDetails;
            var self = this;
            this.view.btnAddAnotherAccount.onClick = function () {
                var viewModel = { selectTab: OLBConstants.DISPUTED_TRANSACTIONS };
                self.loadStopPaymentsModule().presentationController.showMyRequests(viewModel);

            };
			this.view.confirmDialog.height="450dp";
            if (CommonUtilities.isPrintEnabled()) {
                this.view.flxPrint.setVisibility(true);
            }else{
                this.view.flxPrint.setVisibility(false);
            }
        },
        /**
         * showDisputeTransactionDetailPage : Method to show showDisputeTransactionDetailPage
         * @param {object} viewModel object
         */
        showDisputeTransactionDetailPage: function (viewModel) {
            var self = this;
            this.showDisputeTransactionDetail();
            Disputeflag = 1;
            this.setDisputeTransactionReasonsDropdown({
                ddnList: self.loadStopPaymentsModule().presentationController.getdisputeTransactionReasonsListViewModel(),
                selectedValue: viewModel.checkReason || null
            });
            this.view.DisputeTransactionDetail.txtBoxDescription.text = "";
            this.view.DisputeTransactionDetail.keyValue3.lblKey.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)");
            this.view.DisputeTransactionDetail.keyValue1.lblValue.text = viewModel.data.fromAccountNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue2.lblValue.text = viewModel.data.toAccount || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue3.lblValue.text = viewModel.data.amount || kony.i18n.getLocalizedString("i18n.common.none");
            var date = viewModel.data.date;
            this.view.DisputeTransactionDetail.keyValue4.lblValue.text = date || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue5.lblValue.text = viewModel.data.types || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue6.lblValue.text = viewModel.data.referenceNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue7.lblValue.text = viewModel.data.notes || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.txtBoxDescription.maxTextLength = OLBConstants.NOTES_MAX_LENGTH;

            this.view.DisputeTransactionDetail.confirmButtons.btnModify.onClick = viewModel.onCancel;

            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.onClick = function () {

                var input = {
                    fromAccountNumber: viewModel.data.fromAccountNumber,
                    toAccount: viewModel.data.toAccount,
                    amount: viewModel.data.amount,
                    date: viewModel.data.date,
                    types: viewModel.data.types || kony.i18n.getLocalizedString("i18n.common.none"),
                    referenceNumber: viewModel.data.referenceNumber,
                    notes: viewModel.data.notes || kony.i18n.getLocalizedString("i18n.common.none"),
                    reason: self.view.DisputeTransactionDetail.lstBoxSelectReasonForDispute.selectedkeyvalue[1],
                    description: self.view.DisputeTransactionDetail.txtBoxDescription.text,
                    onBacktoAccountDetails: viewModel.onBacktoAccountDetails || null
                };
                self.onBtnContinueDisputeTransaction(input, viewModel.onCancel);
                self.AdjustScreen();
            };
        },
        /**
         * onBtnContinueDisputeTransaction : Method to show confirmation page for dispute a transaction
         * @param {object} input input
         * @param {function} onCancelAction onCancel Action
         */
        onBtnContinueDisputeTransaction: function (input, onCancelAction) {
            this.view.lblStopPayments.setVisibility(false);
            this.view.customheader.lblHeaderMobile.text=this.view.lblHeader.text;
            this.view.flxDisputedTransactionDetail.setVisibility(false);
            this.view.flxSinglePayConfirm.setVisibility(true);
            this.view.flxTopHeader.setVisibility(true);
            this.view.flxSuccessMessageStopCheck.setVisibility(false);
            this.view.ConfirmDetailsSingleMultiple.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.lblReason.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason");
            this.view.ConfirmDetailsSingleMultiple.lblDescription.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description");
            this.view.ConfirmDetailsSingleMultiple.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputeTransactionDetails");
            this.view.ConfirmDetailsSingleMultiple.flxAckButtons.setVisibility(false);
          this.view.flxHeader.setFocus(true);
            this.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction")
                },
            ]);
            this.view.ConfirmDetailsSingleMultiple.rtxReason.text = input.reason;
            this.view.ConfirmDetailsSingleMultiple.rtxDescription.text = input.description;
            this.view.flxPrintACK.setVisibility(false);
            this.view.flxDownloadACK.setVisibility(false);
            this.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction");
            var break_point = kony.application.getCurrentBreakpoint();
            var dataMap = {
                "rtxFrom1": "rtxFrom1",
                "rtxPayee1": "rtxPayee1",
                "rtxDate1": "rtxDate1",
                "rtxAmount1": "rtxAmount1",
                "rtxNotes1": "rtxNotes1",
                "rtxType1": "rtxType1",
                "rtxReferenceNumber1": "rtxReferenceNumber1",
                "lblFrom1": "lblFrom1",
                "lblPayee1": "lblPayee1",
                "lblDate1": "lblDate1",
                "lblAmount1": "lblAmount1",
                "lblNotes1": "lblNotes1",
                "lblType1": "lblType1",
                "lblReferenceNumber1": "lblReferenceNumber1"
            };
            var data = [];
            var segData = {
                "rtxFrom1": input.fromAccountNumber,
                "rtxPayee1": input.toAccount || kony.i18n.getLocalizedString("i18n.common.none"),
                "rtxDate1": input.date,
                "rtxAmount1": input.amount,
                "rtxNotes1": input.notes || kony.i18n.getLocalizedString("i18n.common.none"),
                "rtxType1": input.types,
                "rtxReferenceNumber1": input.referenceNumber,
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.from"),
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Payee"),
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Date"),
                "lblAmount1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)"),
                "lblNotes1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Notes"),
                "lblType1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Types"),
                "lblReferenceNumber1": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                "template": "flxDetails"
            };
            data.push(segData);
            this.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            if(break_point == 640 || break_point == 1024){
                for (var i = 0; i < data.length; i++) {
                    data[i].template = "flxDisputeTransactionConfirmDetails";
                }
            }
            this.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(data);

            var self = this;
            this.view.ConfirmDetailsSingleMultiple.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.btnCancel.onClick = onCancelAction;
            this.view.ConfirmDetailsSingleMultiple.btnModify.onClick = function () {
                var viewModel = { data: input };
                self.showDisputeTransactionDetail(viewModel);
            };
            this.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = function () {
                var params = {
                    transactionId: input.referenceNumber,
                    disputeReason: input.reason,
                    disputeDescription: input.description
                };
                self.loadStopPaymentsModule().presentationController.createDisputeTransaction(params, input);
            };
            self.AdjustScreen();
        },

        /**
         * showStopChecksForm : Method to show stop cheks form view
         * @param {object} formDataVieModel - form prepopulated view model   
         */
        showStopChecksForm: function (formDataVieModel) {
            var scopeObj = this;
            Disputeflag = 0;
            scopeObj.stopCheckRequestData = {};
            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
                },
            ]);
            if (formDataVieModel.showStopPaymentServiceFeesAndValidity) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.text = formDataVieModel.serviceChargableText || "";
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.setVisibility(true);
            } else {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.setVisibility(false);
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateformat = applicationManager.getFormatUtilManager().getDateFormat();
            var today = new Date();
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateComponents = [today.getDate(), today.getMonth()+1, today.getFullYear()];
            scopeObj.showStopCheckPaymentsSeriesMultiple();
            scopeObj.setStopChecksFormData(formDataVieModel);
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed.onClick = scopeObj.onStopCheckFormConfirmHandler.bind(scopeObj);
            scopeObj.onStopCheckCancel = formDataVieModel.onCancel || null;
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnCancel.onClick = scopeObj.onStopCheckCancel;
            scopeObj.AdjustScreen();
        },

        /**
         * onStopCheckFormConfirmHandler : Stop check request confirm button handler
         */
        onStopCheckFormConfirmHandler: function () {
            var scopeObj = this;
            var hasInvaidInputs = true;
            scopeObj.view.btnNew.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAmount.skin =  ViewConstants.SKINS.COMMON_FLEX_NOERRORSKIN;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.setValidationErrorMessageState(false);
            if (FormControllerUtility.isRadioBtnSelected(scopeObj.view.StopCheckPaymentSeriesMultiple.imgRadioBtn1)) {
                if (scopeObj.isValidStopCheckForm(OLBConstants.CHECK_REQUEST_TYPES.SINGLE)) {
                    hasInvaidInputs = false;
                    scopeObj.setSingleOrMultipleCheckRequestConfirmPage();
                }
            } else {
                if (scopeObj.isValidStopCheckForm(OLBConstants.CHECK_REQUEST_TYPES.SERIES)) {
                    hasInvaidInputs = false;
                    scopeObj.setSeriesCheckRequestConfirmPage();
                }
            }
            if (hasInvaidInputs) {
                scopeObj.setValidationErrorMessageState(true);
            }
            scopeObj.AdjustScreen();
        },

        /**
         * setValidationErrorMessageState : Set Stop checks Error message
         * @param {boolean} visible show or hide flag
         * @param {string} i18nKey, i18n key
         */
        setValidationErrorMessageState: function (visible, i18nKey) {
            var scopeObj = this;
            if (visible) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblWarning.text = kony.i18n.getLocalizedString(i18nKey || scopeObj.validationErrorMsgI18nKey || "i18n.StopPayments.errormessages.InvalidDetails");
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lblWarning.setVisibility(visible);
            scopeObj.AdjustScreen();
        },
        /**
         * isValidStopCheckForm : Stop check request form vaidation handler
         * @param {string} requestType, check request type single/series
         * @return {boolean} isvalid form or not.
         */
        isValidStopCheckForm: function (requestType) {
            var scopeObj = this;
            scopeObj.validationErrorMsgI18nKey = "";
            var isValidCheckNumber = function (checkumber) {
                var checkNumberReg = /^\d+$/;
                return checkNumberReg.test(checkumber);
            };
            var payeeName = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text;
            if (CommonUtilities.isEmptyString(payeeName.toString())) {
                scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidPayeeName";
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                return false; 
            }

            switch (requestType) {
                case OLBConstants.CHECK_REQUEST_TYPES.SINGLE:
                    var amount = CommonUtilities.deFormatAmount(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text);
                    var checkNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text;
                    var checkDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text;

                    if (CommonUtilities.isEmptyString(amount.toString()) || !CommonUtilities.isValidAmount(amount) || amount <= 0) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidAmount";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.flxAmount.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
                        return false;
                    }
                    if (!isValidCheckNumber(checkNumber1)) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidCheckNumber";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        return false;
                    }
                    if (CommonUtilities.isEmptyString(checkDescription.toString())) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidDescription";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        return false;
                    }
                    break;
                case OLBConstants.CHECK_REQUEST_TYPES.SERIES:
                    var serCheckNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text;
                    var sercheckNumber2 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
                    var serDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text;
                    if (!isValidCheckNumber(serCheckNumber1) || !isValidCheckNumber(sercheckNumber2)) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidCheckNumber";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        return false;
                    }
                    var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
                    if (noOfChecks <= 1 || noOfChecks > OLBConstants.MAX_CHECKS_COUNT) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidSeriesCheckNumbers";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        return false;
                    }
                    if (CommonUtilities.isEmptyString(serDescription.toString())) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidDescription";
                        scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                        return false;
                    }
                    break;
                default:
                    CommonUtilities.ErrorHandler.onError("Invalid Check request type : " + requestType);
                    return false;
            }
            scopeObj.AdjustScreen();
            return true;
        },

        /**
         * showStopCheckRequestCofirmPage : Method to show Stop check single request confirmation form.
         */
        showStopCheckRequestCofirmPage: function () {
            var scopeObj = this;
            scopeObj.hideAll();
            scopeObj.view.flxSinglePayConfirm.setVisibility(true);
            scopeObj.view.flxTopHeader.setVisibility(true);
            scopeObj.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment");
            scopeObj.view.flxDownloadACK.setVisibility(false);
            scopeObj.view.flxSuccessMessageStopCheck.setVisibility(false);
            scopeObj.view.customheader.lblHeaderMobile.text=scopeObj.view.lblHeader.text;
            var confirmDetailsSingleMultiple = scopeObj.view.ConfirmDetailsSingleMultiple;
            confirmDetailsSingleMultiple.setVisibility(true);
            confirmDetailsSingleMultiple.skin="sknFlxffffffShadowdddcdc";
            confirmDetailsSingleMultiple.clipBounds=false;
            confirmDetailsSingleMultiple.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.stopCheckPaymentDetails");
            confirmDetailsSingleMultiple.flxTransactionDetails.setVisibility(true);
            confirmDetailsSingleMultiple.flxTransactionDetailsRow1.setVisibility(true);
            confirmDetailsSingleMultiple.lblReason.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.from");
            scopeObj.stopCheckRequestData.fromAccountNumber = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKeyValue[0];
            confirmDetailsSingleMultiple.rtxReason.text = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKeyValue[1];

            confirmDetailsSingleMultiple.flxTransactionDetailsRow2.setVisibility(true);
            confirmDetailsSingleMultiple.lblDescription.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Payee");
            scopeObj.stopCheckRequestData.payeeName = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text.toString().trim();
            confirmDetailsSingleMultiple.rtxDescription.text = scopeObj.stopCheckRequestData.payeeName;
            confirmDetailsSingleMultiple.flxTransactionDetailsRow3.setVisibility(false);
            confirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(true);
            confirmDetailsSingleMultiple.flxAckButtons.setVisibility(false);
            confirmDetailsSingleMultiple.btnModify.onClick = scopeObj.onStopCheckModifyClickHandler.bind(scopeObj);
            confirmDetailsSingleMultiple.btnCancel.onClick = scopeObj.onStopCheckCancel || scopeObj.onStopCheckModifyClickHandler.bind(scopeObj);
            scopeObj.view.flxHeader.setFocus(true);
            scopeObj.AdjustScreen();
        },

        /**
         * onStopCheckModifyClickHandler : Stop check request confim page Modify click handler.
         */
        onStopCheckModifyClickHandler: function () {
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
                },
            ]);
            scopeObj.showStopCheckPaymentsSeriesMultiple();
            scopeObj.view.flxSinglePayConfirm.setVisibility(false);
            scopeObj.view.flxTopHeader.setVisibility(false);
            scopeObj.view.flxPrintACK.setVisibility(false);
            scopeObj.view.flxDownloadACK.setVisibility(false);
            scopeObj.view.flxSuccessMessageStopCheck.setVisibility(false);
            scopeObj.view.ConfirmDetailsSingleMultiple.setVisibility(false);
            scopeObj.AdjustScreen();
        },

        /**
         * showStopCheckRequestCofirmPage : Method to set single or mutliple check request page
         */
        setSingleOrMultipleCheckRequestConfirmPage: function () {
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment")
                },
            ]);
            scopeObj.showStopCheckRequestCofirmPage();
            scopeObj.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = scopeObj.onSingleOrMultipeCheckRequestConfirmBtnClick.bind(scopeObj);
            scopeObj.setStopCheckSingleOrMutlipleTranSegment();
        },

        /**
         * setStopCheckSingleOrMutlipleTranSegment : Method to set single or mutliple check request transaction deatils in Stop payment confimatin
         */
        setStopCheckSingleOrMutlipleTranSegment: function () {
            var scopeObj = this;
            var break_point = kony.application.getCurrentBreakpoint();
            var dataMap = {
                "flxRecipientDetails1": "flxRecipientDetails1",
                "flxFrom1": "flxFrom1",
                "lblFrom1": "lblFrom1",
                "rtxFrom1": "rtxFrom1",
                "flxPayee1": "flxPayee1",
                "lblPayee1": "lblPayee1",
                "rtxPayee1": "rtxPayee1",
                "flxDate1": "flxDate1",
                "lblDate1": "lblDate1",
                "rtxDate1": "rtxDate1",
                "flxAmount1": "flxAmount1",
                "lblAmount1": "lblAmount1",
                "rtxAmount1": "rtxAmount1",
                "flxNotes1": "flxNotes1",
                "lblNotes1": "lblNotes1",
                "rtxNotes1": "rtxNotes1"
            };

            scopeObj.stopCheckRequestData.checkNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text;
            scopeObj.stopCheckRequestData.amount = CommonUtilities.deFormatAmount(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text);
            scopeObj.stopCheckRequestData.checkDateOfIssue = scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.formattedDate;
            scopeObj.stopCheckRequestData.checkReason = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKeyValue[0];
            scopeObj.stopCheckRequestData.description = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text;


            var viewModel = [{
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopPayments.Checkno"),
                "rtxFrom1": scopeObj.stopCheckRequestData.checkNumber1,
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)"),
                "rtxPayee1": CommonUtilities.formatCurrencyWithCommas(scopeObj.stopCheckRequestData.amount, true),
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopPayments.DateOfIssueWithColon"),
                "rtxDate1": scopeObj.stopCheckRequestData.checkDateOfIssue,
                "lblAmount1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                "rtxAmount1": scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKeyValue[1],
                "lblNotes1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description"),
                "rtxNotes1": {
                    "bottom": "20dp",
                    "text": scopeObj.stopCheckRequestData.description
                },
                "template": "flxDetails"
            }];

            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setVisibility(true);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            if(break_point == 640 || break_point == 1024){
                for (var i = 0; i < viewModel.length; i++) {
                    viewModel[i].template = "flxDisputeTransactionConfirmDetails";
                }
            }
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(viewModel);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
         * onSingleOrMultipeCheckRequestConfirmBtnClick : Stop checks single/ multiple check confirmation page confirm button click handler
         */
        onSingleOrMultipeCheckRequestConfirmBtnClick: function () {
            var scopeObj = this;
            var requestModel = {
                transactionType: applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST),
                fromAccountNumber: scopeObj.stopCheckRequestData.fromAccountNumber,
                payeeName: scopeObj.stopCheckRequestData.payeeName,
                checkNumber1: scopeObj.stopCheckRequestData.checkNumber1,
                amount: scopeObj.stopCheckRequestData.amount,
                checkDateOfIssue: scopeObj.stopCheckRequestData.checkDateOfIssue,
                checkReason: scopeObj.stopCheckRequestData.checkReason,
                description: scopeObj.stopCheckRequestData.description
            };
            scopeObj.loadStopPaymentsModule().presentationController.stopCheckRequest(requestModel);
        },

        /**
         * onSeriesCheckRequestConfirmBtnClick : Stop checks series check confirmation page confirm button click handler
         */
        onSeriesCheckRequestConfirmBtnClick: function () {
            var scopeObj = this;
            var requestModel = {
                transactionType: applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST),
                fromAccountNumber: scopeObj.stopCheckRequestData.fromAccountNumber,
                payeeName: scopeObj.stopCheckRequestData.payeeName,
                checkNumber1: scopeObj.stopCheckRequestData.checkNumber1,
                checkNumber2: scopeObj.stopCheckRequestData.checkNumber2,
                checkReason: scopeObj.stopCheckRequestData.checkReason,
                description: scopeObj.stopCheckRequestData.description
            };
            scopeObj.loadStopPaymentsModule().presentationController.stopCheckRequest(requestModel);
        },

        /**
         * setSeriesCheckRequestConfirmPage : Method to set series check request page
         */
        setSeriesCheckRequestConfirmPage: function () {
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment")
                },
            ]);
            scopeObj.showStopCheckRequestCofirmPage();
            scopeObj.setStopCheckSeriesTranSegment();
            scopeObj.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = scopeObj.onSeriesCheckRequestConfirmBtnClick.bind(scopeObj);
        },

        /**
         * setStopCheckSeriesTranSegment : Method to set series check request transaction deatils in Stop payment confimatin
         */
        setStopCheckSeriesTranSegment: function () {
            var scopeObj = this;
            var break_point = kony.application.getCurrentBreakpoint();
            var dataMap = {
                "flxRecipientDetails1": "flxRecipientDetails1",
                "flxFrom1": "flxFrom1",
                "lblFrom1": "lblFrom1",
                "rtxFrom1": "rtxFrom1",
                "flxPayee1": "flxPayee1",
                "lblPayee1": "lblPayee1",
                "rtxPayee1": "rtxPayee1",
                "flxDate1": "flxDate1",
                "lblDate1": "lblDate1",
                "rtxDate1": "rtxDate1"
            };

            scopeObj.stopCheckRequestData.checkNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text;
            scopeObj.stopCheckRequestData.checkNumber2 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
            scopeObj.stopCheckRequestData.checkReason = scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKeyValue[0];
            scopeObj.stopCheckRequestData.description = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text;

            var viewModel = [{
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopPayments.Checkno"),
                "rtxFrom1": scopeObj.getSeriesCheckNumber() + "&nbsp;&nbsp;&nbsp;&nbsp;" + scopeObj.getSeriesCheckNumbersCount() + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected"),
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                "rtxPayee1": scopeObj.stopCheckRequestData.checkReason,
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description"),
                "rtxDate1": {
                    "bottom": "20dp",
                    "text": scopeObj.stopCheckRequestData.description
                },
                "template": "flxDetails"
            }];
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setVisibility(true);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            if(break_point == 640 || break_point == 1024){
                for (var i = 0; i < viewModel.length; i++) {
                    viewModel[i].template = "flxDisputeTransactionConfirmDetails";
                }
            }
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(viewModel);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
         * getSeriesCheckNumber : Method to create and return series check number 
         * @return {String} formatted check number for series
         */
        getSeriesCheckNumber: function () {
            return this.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text + " " + OLBConstants.CHECK_SERIES_SEPARATOR + " " + this.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
        },

        /**
         * getSeriesCheckNumbersCount : Method to return total check number in series
         * @return {Number} total checks count
         */
        getSeriesCheckNumbersCount: function () {
            var firstCheckNumber = Number(this.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text);
            var lastCheckNumber = Number(this.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text);
            var count = (lastCheckNumber - firstCheckNumber) + 1;
            if (isNaN(count)) {

                return -1;
            }
            return count;
        },

        /**
        * setStopChecksFormData : Method to set Stop checks form input values
        * @param {object} viewModel - form prepopulated view model
        */
        setStopChecksFormData: function (viewModel) {
            var scopeObj = this;
            scopeObj.setStopChecksFormAccountsDropdown({
                selectedValue: viewModel.accountID
            });
            scopeObj.savedSelectedAccount = viewModel.accountID;

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text = viewModel.payeeName || "";
            if (viewModel.isSeriesChecks) {
                scopeObj.onSeriesRadioButtonClick();
                scopeObj.setStopChecksFormSingleOrMultipleReasonsDropdown({
                    selectedValue: viewModel.checkReason
                });
                scopeObj.savedSeriesCheckReason = viewModel.checkReason;
                scopeObj.setSeriesChecksFormValues(viewModel);
            } else {
                scopeObj.onSingleOrMultipleRadioButtonClick();
                scopeObj.setStopChecksFormSeriesChecksReasonsDropdown({
                    selectedValue: viewModel.checkReason
                });
                scopeObj.savedSingleCheckReason = viewModel.checkReason;
                scopeObj.setSingleOrMultipleChecksFormValues(viewModel);
            }
        },

        /**
        * setStopChecksFormAccountsDropdown : Method to set Stop checks form accounts dropdown 
        * @param {object} viewModel - dropdown view model
        */
        setStopChecksFormAccountsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormAccountsDropdown({
                    list: viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKey = viewModel.selectedValue || scopeObj.savedSelectedAccount || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.masterData[0][0];
        },


        /**
         * onSingleOrMultipleRadioButtonClick : Stop Payments signle or multiple radion buttn click hanlder
         * @param {object} event click event JS  object
         */
        onSingleOrMultipleRadioButtonClick: function (event) {
            var scopeObj = this;
            if (scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src === OLBConstants.IMAGES.ICON_RADIOBTN) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src = OLBConstants.IMAGES.RADIOBTN_ACTIVE_SMALL;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src = OLBConstants.IMAGES.ICON_RADIOBTN;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
                scopeObj.setSingleOrMultipleChecksFormValues();
                scopeObj.setValidationErrorMessageState(false);
                scopeObj.AdjustScreen();
            }
        },

        /**
         * onSeriesRadioButtonClick : Stop Payments series radion buttn click hanlder
         * @param {object} event, click event JS  object
         */
        onSeriesRadioButtonClick: function (event) {
            var scopeObj = this;
            if (scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src === OLBConstants.IMAGES.ICON_RADIOBTN) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src = OLBConstants.IMAGES.RADIOBTN_ACTIVE_SMALL;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src = OLBConstants.IMAGES.ICON_RADIOBTN;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
                scopeObj.setSeriesChecksFormValues();
                scopeObj.setValidationErrorMessageState(false);
                scopeObj.AdjustScreen();
            }
        },

        /**
        * resetTCC: Stop Payments Term and Condition check box click handler
        * @param {object} event, click event JS  object
        */
        resetTCC: function () {
            var scopeObj = this;
            CommonUtilities.setLblCheckboxState(false, scopeObj.view.StopCheckPaymentSeriesMultiple.lblFavoriteEmailCheckBox);
        },

        /**
         * updateStopChekFormContinueBtnState: Update Stop check request Continue button state w.r.t all mandatory fields and T&C Checkbox state
         */
        updateStopChekFormContinueBtnState: function () {
            var scopeObj = this;
            var tccCheckBox = scopeObj.view.StopCheckPaymentSeriesMultiple.lblFavoriteEmailCheckBox;
            var tbxPayee = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text.toString();
            var tbxFirstCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text.toString();
            var tbxLastCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text.toString();
            var tbxCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text.toString();
            var tbxDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text.toString();
            var tbxCheckNumber = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text.toString();
            var tbxAmount = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text.toString();
            var tbxDescriptionNew = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text.toString();
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAmount.skin =  ViewConstants.SKINS.COMMON_FLEX_NOERRORSKIN;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;       
            if (FormControllerUtility.isRadioBtnSelected(scopeObj.view.StopCheckPaymentSeriesMultiple.imgRadioBtn1)) {
                if (FormControllerUtility.isFontIconChecked(tccCheckBox) &&
                    !CommonUtilities.isEmptyString(tbxPayee) &&
                    !CommonUtilities.isEmptyString(tbxCheckNumber) &&
                    !CommonUtilities.isEmptyString(tbxAmount) &&
                    !CommonUtilities.isEmptyString(tbxDescriptionNew)) {
                    FormControllerUtility.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                } else {
                    FormControllerUtility.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                }
            } else {
                if (FormControllerUtility.isFontIconChecked(tccCheckBox) &&
                    !CommonUtilities.isEmptyString(tbxPayee) &&
                    !CommonUtilities.isEmptyString(tbxFirstCheckNo) &&
                    !(CommonUtilities.isEmptyString(tbxLastCheckNo) || CommonUtilities.isEmptyString(tbxCheckNo)) &&
                    !CommonUtilities.isEmptyString(tbxDescription)) {
                    FormControllerUtility.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                } else {
                    FormControllerUtility.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                }
            }
            scopeObj.AdjustScreen();
        },
        /**
         * onTCCClickHanlder : Stop Payments Term and Condition check box click handler
         */
        onTCCClickHanlder: function () {
            var scopeObj = this;
            var tccCheckBox = scopeObj.view.StopCheckPaymentSeriesMultiple.lblFavoriteEmailCheckBox;
            FormControllerUtility.toggleFontCheckbox(tccCheckBox);
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * setSingleOrMultipleChecksFormValues : Method to set Stop payments single/mutliple checks form values
         * @param {object} viewModel, view model object
         */
        setSingleOrMultipleChecksFormValues: function (viewModel) {
            viewModel = viewModel || {};
            var scopeObj = this;
            var checkDateOfIssueObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(viewModel.checkDateOfIssue, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text = viewModel.checkNumber1 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateComponents = checkDateOfIssueObj ? [checkDateOfIssueObj.getDate(), checkDateOfIssueObj.getMonth() + 1, checkDateOfIssueObj.getFullYear()] : scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateComponents;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text = viewModel.checkAmount ? CommonUtilities.formatCurrencyWithCommas(viewModel.checkAmount, true) : "";
            scopeObj.setStopChecksFormSingleOrMultipleReasonsDropdown({
                ddnList: scopeObj.loadStopPaymentsModule().presentationController.getCheckReasonsListViewModel(),
                selectedValue: viewModel.checkReason || null
            });
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.placeholder = "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.maxTextLength = viewModel.maxDesriptionLength || OLBConstants.NOTES_MAX_LENGTH;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text = viewModel.description || "";
            scopeObj.resetTCC();
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * setSeriesChecksFormValues : Method to set Stop payments series checks form values
         * @param {object} viewModel, view model object
         */
        setSeriesChecksFormValues: function (viewModel) {
            var scopeObj = this;
            viewModel = viewModel || {};
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text = viewModel.checkNumber1 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text = viewModel.checkNumber2 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text = viewModel.checkNumber2 || "";
            var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
            if (noOfChecks > 1) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = noOfChecks + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(true);
            } else {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = "";
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(false);
            }
            scopeObj.setStopChecksFormSeriesChecksReasonsDropdown({
                ddnList: scopeObj.loadStopPaymentsModule().presentationController.getCheckReasonsListViewModel(),
                selectedValue: viewModel.checkReason || null
            });
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.placeholder = "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.placeholder = kony.i18n.getLocalizedString("i18n.StopcheckPayments.LastCheckNumber");
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.maxTextLength = viewModel.maxDesriptionLength || OLBConstants.NOTES_MAX_LENGTH;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text = viewModel.description || "";
            scopeObj.resetTCC();
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * bindStopChecksFormAccountsDropdown : Method to bind Stop checks form accounts dropdown
         * @param {object} viewModel - dropdown view model
         */
        bindStopChecksFormAccountsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.masterData = viewModel.list.map(function (account) {
                    return [account.accountID, account.accountName];
                });
            }
        },

        /**
        * setStopChecksFormSingleOrMultipleReasonsDropdown : Method to set Stop checks form single or multiple checks reasons dropdown 
        * @param {object} viewModel - dropdown view model
        */
        setStopChecksFormSingleOrMultipleReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormSingleOrMultipleReasonsDropdown({
                    list: viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKey = viewModel.selectedValue || scopeObj.savedSingleCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
         * bindStopChecksFormSingleOrMultipleReasonsDropdown : Method to bind Stop checks form reasons dropdown
         * @param {object} viewModel - dropdown view model
         */
        bindStopChecksFormSingleOrMultipleReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData = viewModel.list.map(function (reason) {
                    return [reason.id, reason.name];
                });
            }
        },

        /**
         * setStopChecksFormSingleOrMultipleReasonsDropdown : Method to set Stop checks form single or multiple checks reasons dropdown 
         * @param {object} viewModel - dropdown view model
         */
        setStopChecksFormSeriesChecksReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormSeriesChecksReasonsDropdown({
                    list: viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKey = viewModel.selectedValue || scopeObj.savedSeriesCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
         * bindStopChecksFormSingleOrMultipleReasonsDropdown : Method to bind Stop checks form reasons dropdown
         * @param {object} viewModel - dropdown view model
         */
        bindStopChecksFormSeriesChecksReasonsDropdown: function (viewModel) {
            var scopeObj = this;
            if (viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.masterData = viewModel.list.map(function (reason) {
                    return [reason.id, reason.name];
                });
            }
        },

        /**
         * onTandCBtnClickHandler : Terms and Conditions button click handler.
         */
        onTandCBtnClickHandler: function () {
            var scopeObj = this;
            scopeObj.setTermsAndConditionsPopupState({
                visible: true
            });
        },

        /**
         * setTermsAndConditionsPopupState : set Terms and Conditions popup.
         * @param {boolean} visible  is visibility true/false
         */
        setTermsAndConditionsPopupState: function (visible) {
            var scopeObj = this;
            visible = visible || false;
            scopeObj.view.flxTermsAndConditions.height = scopeObj.getPageHeight() + "dp";
            scopeObj.view.flxTermsAndConditions.setVisibility(visible);
            if(visible === true)
            scopeObj.view.lblTermsAndConditions.setFocus(true);
            scopeObj.view.flxClose.setFocus(true);
            FormControllerUtility.setLblCheckboxState(FormControllerUtility.isFontIconChecked(scopeObj.view.StopCheckPaymentSeriesMultiple.lblFavoriteEmailCheckBox), scopeObj.view.lblFavoriteEmailCheckBox);
        },

        /**
         * onTandContentSaveClickHandler : Terms and Conditions popup Save button click handler.
         */
        onTandContentSaveClickHandler: function () {
            var scopeObj = this;
            FormControllerUtility.setLblCheckboxState(FormControllerUtility.isCheckboxChecked(scopeObj.view.lblFavoriteEmailCheckBox), scopeObj.view.StopCheckPaymentSeriesMultiple.lblFavoriteEmailCheckBox);
            scopeObj.updateStopChekFormContinueBtnState();
            scopeObj.setTermsAndConditionsPopupState(false);
        },

        /**
         * onTandContentCheckBoxClickHandler : Terms and Conditions popup checkbox click handler.
         */
        onTandContentCheckBoxClickHandler: function () {
            FormControllerUtility.toggleFontCheckbox(this.view.lblFavoriteEmailCheckBox);
        },

        /**
         * onSuccessCreateStopCheckRequest : Method to handle successful sstop check request.
         * @param {object} viewModel, success request view model
         */
        onSuccessCreateStopCheckRequest: function (viewModel) {
            var scopeObj = this;
            scopeObj.showStopChekRequestAcknowledgment({
                referenceNumber: viewModel.referenceNumber
            });
        },

        /**
         * showStopChekRequestAcknowledgment : Method to show Stop check request Acknowledgement form.
         * @param {object} viewModel view
         */
        showStopChekRequestAcknowledgment: function (viewModel) {
            var scopeObj = this;
            var confirmDetailsSingleMultiple = scopeObj.view.ConfirmDetailsSingleMultiple;
            this.hideAll();
            confirmDetailsSingleMultiple.setVisibility(true);
            this.view.flxSuccessMessageStopCheck.setVisibility(true);
            confirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(false);
            confirmDetailsSingleMultiple.flxAckButtons.setVisibility(true);
            if (CommonUtilities.isPrintEnabled()) {
                scopeObj.view.flxPrintACK.setVisibility(true);
              } else {
                scopeObj.view.flxPrintACK.setVisibility(false);
              }
            this.view.flxSinglePayConfirm.setVisibility(true);
          this.view.flxHeader.setFocus(true);
            if(viewModel.isDataBindedAlready){
                this.view.forceLayout();
                return;
            }

            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement"),
                    toolTip: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
                },
            ]);
            scopeObj.view.lblReferenceNumber2.text = viewModel.referenceNumber;

            scopeObj.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.StopCheckPaymentAck");

            confirmDetailsSingleMultiple.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.stopCheckPaymentDetails");
            scopeObj.view.ConfirmDetailsSingleMultiple.btnBackToAccoutnDetails.onClick = viewModel.onBacktoAccountDetails;
            scopeObj.view.ConfirmDetailsSingleMultiple.btnViewRequests.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
            scopeObj.view.ConfirmDetailsSingleMultiple.btnViewRequests.onClick = viewModel.onMyRequestAction || scopeObj.onViewRequestsBtnClick.bind(scopeObj);
        },

        /**
         * onViewRequestsBtnClick : Stop Payment Acknowledgemtn My request button click hanlder.
         */
        onViewRequestsBtnClick: function () {
            var scopeObj = this;
            scopeObj.loadStopPaymentsModule().presentationController.showMyRequests({
                selectTab: OLBConstants.DISPUTED_CHECKS
            });
        },

        /**
         * onSeriesLastCheckTextChange : Method to handle Series last check number stop editing
         */
        onSeriesLastCheckTextChange: function () {
            var scopeObj = this;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.setValidationErrorMessageState(false);                         
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text;
            var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
            if (noOfChecks > 1) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = noOfChecks + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(true);
                scopeObj.view.forceLayout();
            }
            scopeObj.AdjustScreen();
        },

        /**
         * onSeriesLastCheck2TextChange : Method to handle Series last check number first / 2nd text box stop editing
         */
        onSeriesLastCheck2TextChange: function () {
            var scopeObj = this;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            scopeObj.setValidationErrorMessageState(false);  
            var checksCount = scopeObj.getSeriesCheckNumbersCount();
            if (checksCount > 1 && checksCount <= OLBConstants.MAX_CHECKS_COUNT) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = scopeObj.getSeriesCheckNumbersCount() + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.setValidationErrorMessageState(false);
            }
            else {
                if (scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.isVisible) {
                    scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = "";
                    scopeObj.setValidationErrorMessageState(true, "i18n.StopPayments.errormessages.InvalidSeriesCheckNumbers");
                    scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                    scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                    scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                }
            }
          scopeObj.AdjustScreen();
        },

        /**
         * showMyRequests : Method to show My Request View
         * @param {object} viewModel, view model object
         */
        showMyRequests: function (viewModel) {
            var scopeObj = this;
            if (viewModel) {
                scopeObj.setMyRequestsUI(viewModel);
            } else {
                CommonUtilities.ErrorHandler.onError("showMyRequests - Invalid View Model : " + viewModel);
            }
        },

        /**
         * setMyRequestsUI : Method to set flexes visibility for My Requests UI
         * @param {object} viewModel, view model object
         */
        setMyRequestsUI: function (viewModel) {
            var scopeObj = this;
            Disputeflag = 0;
            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    toolTip: kony.i18n.getLocalizedString("i18n.topmenu.accounts")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests")
                },
            ]);

            var selectTab = viewModel.selectTab || OLBConstants.DISPUTED_TRANSACTIONS;
            scopeObj.hideAll();
            scopeObj.view.flxMyRequestsTabs.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxMyRequestsHeader.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxMyRequestsHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests");
            //scopeObj.view.MyRequestsTabs.flxTabs.setVisibility(true);
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.setVisibility(true);
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputedTransactions");
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.onClick = scopeObj.disputedTransactionsTabClickHandler.bind(scopeObj);
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputedChecks");
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.onClick = scopeObj.disputedChecksTabClickHandler.bind(scopeObj);
            scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.setVisibility(true);

            if (viewModel && viewModel.addNewStopCheckRequestAction) {
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.text = viewModel.addNewStopCheckRequestAction.displayName;
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.toolTip = CommonUtilities.changedataCase(viewModel.addNewStopCheckRequestAction.displayName);
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = viewModel.addNewStopCheckRequestAction.action;
                scopeObj.view.btnNew.onClick = function () {
                    viewModel.addNewStopCheckRequestAction.action();
                    scopeObj.view.lblStopPayments.isVisible = false;
                    scopeObj.view.customheader.lblHeaderMobile.text = "Stop Check Payment";
                };
            } else {
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest");
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest"));
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = scopeObj.btnAddNewStopCheckPaymentsClickHandler.bind(scopeObj);
                scopeObj.view.btnNew.onClick = function () {
                    scopeObj.btnAddNewStopCheckPaymentsClickHandler.bind(scopeObj);
                    scopeObj.view.lblStopPayments.isVisible = false;
                    scopeObj.view.customheader.lblHeaderMobile.text = "Stop Check Payment";
                };
            }
            switch (selectTab) {
                case OLBConstants.DISPUTED_TRANSACTIONS:
                    scopeObj.setDisputedTransactionTabUI();
                    break;
                case OLBConstants.DISPUTED_CHECKS:
                    scopeObj.setDisputedChecksTabUI();
                    break;
                default:
                    scopeObj.setDisputedTransactionTabUI();
            }
            scopeObj.AdjustScreen();
        },

        /**
         * showStopCheckRequests : Method to show Stop check requests
         * @param {Array} stopCheckRequestsViewModel, stop check requests array
         */
        showStopCheckRequests: function (stopCheckRequestsViewModel) {
            var scopeObj = this;
            scopeObj.setDisputedChecksTabUI();
            if (stopCheckRequestsViewModel && stopCheckRequestsViewModel.stopchecksRequests && stopCheckRequestsViewModel.stopchecksRequests.length) {
                scopeObj.bindStopCheckRequestsData(stopCheckRequestsViewModel.stopchecksRequests);
                var sortMap = [{
                    name: 'transactionDate',
                    imageFlx: this.view.MyRequestsTabs.imgSortDateDC,
                    clickContainer: this.view.MyRequestsTabs.flxSortDateDC
                },
                {
                    name: 'amount',
                    imageFlx: this.view.MyRequestsTabs.imgSortAmountDC,
                    clickContainer: this.view.MyRequestsTabs.flxSortAmountDC
                },
                {
                    name: 'statusDesc',
                    imageFlx: this.view.MyRequestsTabs.imgStatusDC,
                    clickContainer: this.view.MyRequestsTabs.flxStatusDC
                }];
                FormControllerUtility.setSortingHandlers(sortMap, scopeObj.onDistiputedChecksSorting.bind(scopeObj), scopeObj);
                FormControllerUtility.updateSortFlex(sortMap, stopCheckRequestsViewModel.config);
                scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(false);
            } else {
                scopeObj.showNoTransactionsUI({
                    noTransactionsMessageI18Key: "i18n.StopPayments.NoStopPaymentRequests"
                });
            }
            scopeObj.AdjustScreen();
        },


        /**
         * showDisputeTransactionsRequests : Method to show dispute transaction requests
         * @param {Array} disputeTransactionsRequestsViewModel transactions view model
         */
        showDisputeTransactionsRequests: function (disputeTransactionsRequestsViewModel) {
            var scopeObj = this;
            scopeObj.setDisputedTransactionTabUI();
            if (disputeTransactionsRequestsViewModel && disputeTransactionsRequestsViewModel.stopDisputedRequests && disputeTransactionsRequestsViewModel.stopDisputedRequests.length) {
                scopeObj.bindDisputeTransactionRequestsData(disputeTransactionsRequestsViewModel.stopDisputedRequests);
                var sortMap = [{
                    name: 'disputeDate',
                    imageFlx: this.view.MyRequestsTabs.imgSortDate,
                    clickContainer: this.view.MyRequestsTabs.flxSortDate
                },
                {
                    name: 'amount',
                    imageFlx: this.view.MyRequestsTabs.imgSortAmount,
                    clickContainer: this.view.MyRequestsTabs.flxSortAmount
                },
                {
                    name: 'disputeStatus',
                    imageFlx: this.view.MyRequestsTabs.imgStatus,
                    clickContainer: this.view.MyRequestsTabs.flxStatus
                }];
                FormControllerUtility.setSortingHandlers(sortMap, scopeObj.onDistiputedTransactionsSorting.bind(scopeObj), scopeObj);
                FormControllerUtility.updateSortFlex(sortMap, disputeTransactionsRequestsViewModel.config);
                scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(false);
            } else {
                scopeObj.showNoTransactionsUI({
                    noTransactionsMessageI18Key: "i18n.StopPayments.NoStopPaymentRequests"
                });
            }
            scopeObj.AdjustScreen();
        },


        /**
         * onDistiputedTransactionsSorting: Handler for Make Transfer tab Sorting
         * @param {object} event - Event Object
         * @param {object} data - Updated Config 
         */
        onDistiputedTransactionsSorting: function (event, data) {
            this.loadStopPaymentsModule().presentationController.showDisputeTransactionRequests(data);
        },
        /**
         * onDistiputedChecksSorting: Handler for Make Transfer tab Sorting
         * @param {object} event - Event Object
         * @param {object} data - Updated Config 
         */
        onDistiputedChecksSorting: function (event, data) {
            this.loadStopPaymentsModule().presentationController.showDisputeCheckRequests(data);
        },

        /**
        * bindStopCheckRequestsData : Method to bind Stop check requests data to Segments
        * @param {Array} stopCheckRequests, stop check requests array
        */
        bindStopCheckRequestsData: function (stopCheckRequests) {
            var scopeObj = this;
            var break_point = kony.application.getCurrentBreakpoint();
            var widgetDataMap = {
                "flxDisputedChecksRowWrapper": "flxDisputedChecksRowWrapper",
                "flxSegDisputedChecksRowWrapper": "flxSegDisputedChecksRowWrapper",
                "flxIdentifier": "flxIdentifier",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
                "flxDropdown": "flxDropdown",
                "flxSegDisputedTransactionRowWrappers": "flxSegDisputedTransactionRowWrappers",
                "flxWrapper": "flxWrapper",
                "flxLeft": "flxLeft",
                "flxRight": "flxRight",
                "flxDate": "flxDate",
                "flxReferenceNo": "flxReferenceNo",
                "flxAmount": "flxAmount",
                "flxStatus": "flxStatus",
                "lblSeparator": "lblSeparator",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator2": "lblSeparator2",
                "imgDropDown": "imgDropDown",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblReferenceNo": "lblReferenceNo",
                "lblAmount": "lblAmount",
                "lblStatus": "lblStatus",
                "lblSepeartor3": "lblSepeartor3",
                "btnCancelRequests": "btnCancelRequests",
                "btnSendAMessage": "btnSendAMessage",
                "lblFromAccount": "lblFromAccount",
                "lblToAccount": "lblToAccount",
                "lblExpiresOnKey": "lblExpiresOnKey",
                "lblFromAccountData": "lblFromAccountData",
                "lblToAccountData": "lblToAccountData",
                "lblExpiresOnData": "lblExpiresOnData",
                "lblDateOfDescriptionKey": "lblDateOfDescriptionKey",
                "CopylblFrequencyTitle0c4e7bef1ab0c44": "CopylblFrequencyTitle0c4e7bef1ab0c44",
                "lblDateOfDescriptionValue": "lblDateOfDescriptionValue",
                "lblTransactionTypeValue": "lblTransactionTypeValue",
                "lblSeparatorLineActions":"lblSeparatorLineActions",
                "lblToAccount1":"lblToAccount1",
                "lblToAccountData1":"lblToAccountData1",
                "lblSeparatorActions":"lblSeparatorActions",
                "lblTransactionDetails":"lblTransactionDetails",
                "lblAmountKey":"lblAmountKey",
            };
            var dataMap = stopCheckRequests.map(function (requestObj) {
                return {
                    "template": "flxDisputedChecksUnSelectedWrapper",
                    "lblSeparator": "lblSeparator",
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator2": "lblSeparator2",
                    "imgDropDown": OLBConstants.IMAGES.ARRAOW_DOWN,
                    "lblDate": requestObj.transactionDate,
                    "lblDescription": requestObj.payeeName,
                    "lblReferenceNo": requestObj.checkNumber,
                    "lblAmount": requestObj.amount,
                    "lblStatus": requestObj.statusDescription,
                    "lblSepeartor3": "",
                    "lblFromAccount": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                    "lblToAccount": kony.i18n.getLocalizedString("i18n.StopCheckPayments.DateOnCheck"),
                    "lblExpiresOnKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.ExpiresOn"),
                    "lblFromAccountData": requestObj.fromAccount,
                    "lblToAccountData": requestObj.checkDateOfIssue,
                    "lblExpiresOnData": requestObj.requestValidity,
                    "lblDateOfDescriptionKey": kony.i18n.getLocalizedString("i18n.StopPayments.Reason"),
                    "CopylblFrequencyTitle0c4e7bef1ab0c44": kony.i18n.getLocalizedString("i18n.StopPayments.Description"),
                    "lblDateOfDescriptionValue": requestObj.checkReason,
                    "lblTransactionTypeValue": requestObj.transactionsNotes,
                    "btnSendAMessage": {
                        "text": kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage"),
                        "onClick": requestObj.onSendMessageAction ? requestObj.onSendMessageAction : null,
                        "isVisible": requestObj.onSendMessageAction ? true : false
                    },
                    "btnCancelRequests": {
                        "text": requestObj.onCancelRequest ? kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST") : kony.i18n.getLocalizedString("i18n.StopPayments.RENEWREQUEST"),
                        "toolTip": CommonUtilities.changedataCase(requestObj.onCancelRequest ? kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST") : kony.i18n.getLocalizedString("i18n.StopPayments.RENEWREQUEST")),
                        "onClick": requestObj.onCancelRequest ? requestObj.onCancelRequest : requestObj.onReNewRequest,
                        "isVisible": requestObj.onCancelRequest || requestObj.onReNewRequest ? true : false
                    },
                    "lblToAccount1":"To:",
                    "lblToAccountData1":requestObj.payeeName,
                    "lblTransactionDetails":"Transaction Details",
                    "lblAmountKey":"Amount:",
                };
            });
            scopeObj.view.MyRequestsTabs.segTransactions.widgetDataMap = widgetDataMap;
            if (break_point == 640) {
                for (var i = 0; i < dataMap.length; i++) {
                    dataMap[i].template = "flxDisputedTransactionsUnSelectedMobile";
                }
            }
            scopeObj.view.MyRequestsTabs.segTransactions.setData(dataMap);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(true);
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(true);
            if (CommonUtilities.isPrintEnabled()) {
                scopeObj.view.flxPrintACK.setVisibility(true);
              } else {
                scopeObj.view.flxPrintACK.setVisibility(false);
              }
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
         * showNoTransactionsUI : Method to handle Empty requests/transactions.
         * @param {object} viewModel,view model
         */
        showNoTransactionsUI: function (viewModel) {
            var scopeObj = this;
            viewModel = viewModel || {};
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(false);
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(true);
            scopeObj.view.MyRequestsTabs.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString(viewModel.noTransactionsMessageI18Key || "i18n.StopPayments.NoStopPaymentRequests");
        },

        /**
         * btnAddNewStopCheckPaymentsClickHandler : My Requests Add New Stop Check Payments button click handler
         */
        btnAddNewStopCheckPaymentsClickHandler: function () {
            this.loadStopPaymentsModule().presentationController.showStopChecksForm();
        },

        /**
        * btnViewDisputedTransactionsClickHandler : View Disputed Transactions button Click Handler
        */
        btnViewDisputedTransactionsClickHandler: function () {
            this.loadStopPaymentsModule().presentationController.showMyRequests({
                selectTab: OLBConstants.DISPUTED_TRANSACTIONS
            });
        },


        /**
        * btnViewDisputedChecks : View Disputed Checks button click handler 
        */
        btnViewDisputedChecks: function () {
            var scopeObj = this;
            scopeObj.loadStopPaymentsModule().presentationController.showMyRequests({
                selectTab: OLBConstants.DISPUTED_CHECKS
            });
        },

        /**
         * disputedTransactionsTabClickHandler : My Requests Disputed Transactions Tab Click Handler
         */
        disputedTransactionsTabClickHandler: function () {
            var scopeObj = this;
            scopeObj.setDisputedTransactionTabUI();
            scopeObj.loadStopPaymentsModule().presentationController.showDisputeTransactionRequests({
                resetSorting: true
            });
        },

        /**
         * disputedChecksTabClickHandler : My Requests Disputed Checks Tab Click Handler
         */
        disputedChecksTabClickHandler: function () {
            var scopeObj = this;
            scopeObj.setDisputedChecksTabUI();
            scopeObj.loadStopPaymentsModule().presentationController.showDisputeCheckRequests({
                resetSorting: true
            });
        },

        /**
         * setServerError : Method to handle Server error 
         * @param {boolean} isError, error flag to show/hide error flex.
         */
        setServerError: function (isError) {
            var scopeObj = this;
            scopeObj.view.flxDowntimeWarning.setVisibility(isError);
            if (isError) {
                scopeObj.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
            }
            scopeObj.view.forceLayout();
        },

        /** 
         * showCancelStopRequestUI : show cancel stop request UI
         * @param {object} viewModel , view model object.
         */
        showCancelStopRequestUI: function (viewModel) {
            var scopeObj = this;
            var cancelStopCheckPopup = scopeObj.view.CancelStopCheckPayments;
            var height = scopeObj.getPageHeight();
            cancelStopCheckPopup.lblHeading.text = viewModel.headerText;
            cancelStopCheckPopup.lblPopupMessage.text = viewModel.message;
            if (viewModel.showStopPaymentServiceFeesAndValidity) {
                cancelStopCheckPopup.lblThisServicesIsChargeble.text = viewModel.serviceChargableText || "";
                cancelStopCheckPopup.flxPleaseNoteTheFollowingPoints.setVisibility(true);
            } else {
                cancelStopCheckPopup.flxPleaseNoteTheFollowingPoints.setVisibility(false);
            }

            cancelStopCheckPopup.flxPleaseNoteTheFollowingPoints.setVisibility(false);
            scopeObj.view.flxLogoutStopCheckPayment.height = height + "dp";
            scopeObj.view.flxLogoutStopCheckPayment.left = "0%";
            scopeObj.view.flxLogoutStopCheckPayment.setVisibility(true);
            scopeObj.view.flxLogoutStopCheckPayment.setFocus(true);
            scopeObj.view.CancelStopCheckPayments.lblPopupMessage.setFocus(true);
            scopeObj.view.CustomPopup.lblHeading.setFocus(true);
            if(CommonUtilities.isCSRMode()){
                cancelStopCheckPopup.btnYes.onClick = CommonUtilities.disableButtonActionForCSRMode();  
                cancelStopCheckPopup.btnYes.skin = CommonUtilities.disableButtonSkinForCSRMode();
                cancelStopCheckPopup.btnYes.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                cancelStopCheckPopup.btnYes.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
            cancelStopCheckPopup.btnYes.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
                viewModel.confirmCancelAction();
            };
        }
        },
        /**
  * used to convert the CalenderFormat Date
  * @param {String} dateString string formated date 
  * @param {string} inputFormat input format
  * @returns {string} outputDate output date
  */
        getDateFromDateString: function (dateString, inputFormat) {
            var fu = applicationManager.getFormatUtilManager();
            var dateObj = fu.getDateObjectFromCalendarString(dateString, inputFormat);
            var outputDate = fu.getFormatedDateString(dateObj, fu.APPLICATION_DATE_FORMAT);
            return outputDate;
        },
        //UI Code
        /**
        * onBreakpointChange : Handles ui changes on .
        * @member of {frmStopPaymentsController}
        * @param {integer} width - current browser width
        * @return {} 
        * @throws {}
        */
        orientationHandler:null,
        onBreakpointChange: function (width) {
            kony.print('on breakpoint change');
            if (this.orientationHandler === null) {
                this.orientationHandler = new OrientationHandler();
            }
            this.orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChange(width);
            this.setupFormOnTouchEnd(width);

          var responsiveFonts = new ResponsiveFonts();  
          this.view.lblSuccessAck.text=kony.i18n.getLocalizedString("i18n.StopCheckPayment.SuccessMessage");
            if (Disputeflag == 1) {
                this.view.DisputeTransactionDetail.isVisible = true;
            }
            else {
                this.view.DisputeTransactionDetail.isVisible = false;
            }
            this.view.skin = "sknFrmf8f7f8";
            if (width == 640) {
                this.view.flxTopHeader.height="0dp";
                this.view.btnNew.isVisible=true;
                this.view.btnNew.zIndex=1000;
                this.view.StopCheckPaymentSeriesMultiple.flxPleaseNoteTheFollowingPoints.top="20dp";
              	this.view.StopCheckPaymentSeriesMultiple.flxPleaseNoteTheFollowingPoints.left="2%"
                this.view.StopCheckPaymentSeriesMultiple.flxPleaseNoteTheFollowingPoints.width="96%"
                this.view.MyRequestsTabs.flxSort.isVisible = false;
                this.view.MyRequestsTabs.flxSortDisputedChecks.isVisible = false;
                this.view.customheader.lblHeaderMobile.isVisible = true;
                this.view.lblStopPayments.isVisible = false;
                this.view.lblBillPayAcknowledgement.isVisible = false;
                this.view.lblHeader.isVisible = false;
                this.view.MyRequestsTabs.flxMyRequestsHeader.isVisible = false;
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.left="2%";
              	this.view.StopCheckPaymentSeriesMultiple.btnCancel.width="96%"
                this.view.ConfirmDetailsSingleMultiple.btnCancel.left="10dp";
                this.view.ConfirmDetailsSingleMultiple.btnCancel.width = this.view.ConfirmDetailsSingleMultiple.btnModify.width;
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.top="85dp"
                this.view.ConfirmDetailsSingleMultiple.btnCancel.width = this.view.ConfirmDetailsSingleMultiple.btnModify.width;
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.left="2%";
                 this.view.StopCheckPaymentSeriesMultiple.btnCancel.width="96%";
                 this.view.StopCheckPaymentSeriesMultiple.btnCancel.centerY="";
              if(this.view.lblDowntimeWarning.frame.height>50){
               	this.view.flxDowntimeWarning.height=this.view.lblDowntimeWarning.frame.height+10+"dp";
              }
              else{
                this.view.flxDowntimeWarning.height="60dp";
              }
              responsiveFonts.setMobileFonts();
            }
            else {
                this.view.btnNew.isVisible = false;
                this.view.flxTopHeader.height = "50dp";
                this.view.MyRequestsTabs.flxSort.isVisible = false;
                this.view.MyRequestsTabs.flxSortDisputedChecks.isVisible = false;
                this.view.customheader.lblHeaderMobile.isVisible = false;
                this.view.lblBillPayAcknowledgement.isVisible = true;
              	this.view.lblStopPayments.isVisible = false;
                this.view.lblHeader.isVisible = true;
                this.view.MyRequestsTabs.flxMyRequestsHeader.isVisible = true;
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.right="302dp";
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.left="";
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.width="258dp";
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.centerY="50%";
                this.view.StopCheckPaymentSeriesMultiple.btnCancel.top="";
                responsiveFonts.setDesktopFonts();
            }
            if (width == 1366 || 1380) {
                this.view.MyRequestsTabs.lblMyRequestsHeader.skin = "sknSSP42424220Px";
            }
            if (width == 1024) {
                this.view.MyRequestsTabs.lblMyRequestsHeader.skin = "sknSSP42424217Px";
                this.view.ConfirmDetailsSingleMultiple.btnCancel.left="23.2%"
                this.view.ConfirmDetailsSingleMultiple.btnCancel.width="21.6%"
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width){
          if(width==640){
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }else{
            if(width==1024){
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }else{
                this.view.onTouchEnd = function(){
                  hidePopups();   
                } 
              }
              var userAgent = kony.os.deviceInfo().userAgent;
              if (userAgent.indexOf("iPad") != -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }
          }
        },
        nullifyPopupOnTouchStart: function(){
        }
    }

});