
/**
 * Description of Module representing a Confirm form.
 * @module frmConfirmController
 */

define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function (commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
  return /** @alias module:frmConfirmController */ {
    
    /** updates the present Form based on required function.
       * @param {object} viewModel model on which new pathe should decide
       */
    updateFormUI: function (viewModel) {
      if (viewModel.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
      } else {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.transferConfirm) {
        this.updateTransferConfirmDetails(viewModel.transferConfirm);
      }
      if (viewModel.ProgressBar) {
        if (viewModel.ProgressBar.show) {
          FormControllerUtility.showProgressBar(this.view);
        }
        else {
          FormControllerUtility.hideProgressBar(this.view);
        }
      }
    },

    /**
     * used perform the initialize activities.
     * 
     */
    initActions: function () {
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      var scopeObj = this;
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      this.AdjustScreen();
    },

    /**
     * used to perform the postShow Activiities.
     * 
     */
    postShowFrmConfirm: function () {
      var self = this;
      this.view.customheader.forceCloseHamburger();
      this.view.confirmButtons.skin = "sknFlxe9ebeeop100";
      this.customiseHeaderComponentforAcknowledgement();
      this.AdjustScreen();
      this.view.confirmDialog.keyValueFrom.flxInfo.onClick = function () {
        self.view.AllForms.left = self.view.confirmDialog.keyValueFrom.flxValue.frame.x + self.view.confirmDialog.keyValueFrom.lblValue.frame.width - 106 + "dp";
        if (self.view.AllForms.isVisible === true) {
          self.view.AllForms.isVisible = false;
        }
        else {
          self.view.AllForms.isVisible = true;
        }
      };
      this.view.AllForms.flxCross.onClick = function () {
        self.view.AllForms.isVisible = false;
      };
      this.AdjustScreen();
    },

    /**
     * breadcrumb hiding logic
     */
    customiseHeaderComponentforAcknowledgement: function () {
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
    },


    /**
     * used to perform UI Activities like fith the UI in Screen.
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },

    /** Shows Transfer Confirmation
       * @param {object} transferConfirm Transfer Details
       */
    updateTransferConfirmDetails: function (transferConfirm) {
      transferConfirm.transferData.type = transferConfirm.makeTransferViewModel.type;
      var viewModel = transferConfirm.transferData;
      var presenter = applicationManager.getModulesPresentationController("TransferModule");
      if (commonUtilities.isCSRMode() && transferConfirm.makeTransferViewModel.isEdit) {
        this.view.confirmDialog.confirmButtons.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
      }
      this.view.flxPopup.setVisibility(false);
      this.view.confirmDialog.flxdeliverby.setVisibility(false);

      this.view.confirmDialog.keyValueFrom.lblValue.text = commonUtilities.getAccountDisplayName(viewModel.accountFrom);
      this.view.confirmDialog.keyValueFrequency.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency");
      this.view.confirmDialog.keyValueFrequency.lblValue.text = viewModel.frequencyKey;

      this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString('i18n.transfer.QuitTransfe');
      this.view.lblConfirmBillPay.text = kony.i18n.getLocalizedString('i18n.transfers.confirmTransfer');

      //Note 
      this.view.confirmDialog.keyValue2.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.Note');
      this.view.confirmDialog.keyValue2.lblValue.text = viewModel.notes ? viewModel.notes : "";

      //Need to write the domestic and international login
      if (viewModel.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        this.view.confirmDialog.flxTransactionFee.setVisibility(false);
        this.view.confirmDialog.keyValue.flxContainer.setVisibility(false);
        this.view.confirmDialog.keyValueAddress.setVisibility(false);
        this.view.confirmDialog.keyValue1.setVisibility(false);
        this.view.confirmDialog.flxaddress.setVisibility(false);
        this.view.confirmDialog.keyValuePaymentDate.setVisibility(true);
        this.view.confirmDialog.keyValue.setVisibility(false);
        this.view.confirmDialog.keyValueNote.setVisibility(false);

        this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.accountTo.beneficiaryName ? viewModel.accountTo.nickName : (commonUtilities.getAccountDisplayName(viewModel.accountTo));
        this.view.confirmDialog.keyValueAmount.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialog.keyValueAmount.lblValue.text = this.formatCurrency(viewModel.amount);
        this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString('i18n.billPay.PaymentDate');
        this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.sendOnDate;

      } else if (viewModel.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        this.view.confirmDialog.keyValueAddress.setVisibility(true);
        this.view.confirmDialog.flxTransactionFee.setVisibility(false);
        this.view.confirmDialog.keyValue.setVisibility(false);
        this.view.confirmDialog.keyValue1.setVisibility(false);
        this.view.confirmDialog.keyValuePaymentDate.setVisibility(true);
        this.view.confirmDialog.keyValueNote.setVisibility(false);

        this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.reciepientName;
        this.view.confirmDialog.keyValueAddress.lblKey.text = kony.i18n.getLocalizedString('i18n.TransferEur.reciepientIBAN');
        this.view.confirmDialog.keyValueAddress.lblValue.text = applicationManager.getFormatUtilManager().formatIBAN(viewModel.IBAN);
        commonUtilities.enableCopy(this.view.confirmDialog.keyValueAddress.lblValue); 
        this.view.confirmDialog.keyValueAmount.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialog.keyValueAmount.lblValue.text = this.formatCurrency(viewModel.amount);
        this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString('i18n.billPay.PaymentDate');
        this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.sendOnDate;

      } else if (viewModel.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        this.view.confirmDialog.keyValueAddress.setVisibility(true);
        this.view.confirmDialog.keyValue1.setVisibility(true);
        this.view.confirmDialog.keyValue.setVisibility(true);
        this.view.confirmDialog.keyValueNote.setVisibility(true);
        this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.reciepientName;
        this.view.confirmDialog.keyValueAddress.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.swiftCode');
        this.view.confirmDialog.keyValueAddress.lblValue.text = viewModel.swiftCode;
        commonUtilities.enableCopy(this.view.confirmDialog.keyValueAddress.lblValue);  
        this.view.confirmDialog.keyValueAmount.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.bankName');
        this.view.confirmDialog.keyValueAmount.lblValue.text = viewModel.bankName;
        this.view.confirmDialog.keyValuePaymentDate.setVisibility(true);
        this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString('i18n.WireTransfer.recipientAccountNumber');
        this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.accountToKey;
        this.view.confirmDialog.keyValue1.lblKey.text = kony.i18n.getLocalizedString('i18n.billPay.PaymentDate');
        this.view.confirmDialog.keyValue1.lblValue.text = viewModel.sendOnDate;
        this.view.confirmDialog.keyValue.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialog.keyValue.lblValue.text = applicationManager.getConfigurationManager().currencyCode[viewModel.transactionCurrency]+""+viewModel.amount;
        this.view.confirmDialog.keyValueNote.lblKey.text = kony.i18n.getLocalizedString('i18n.common.Currency');
        this.view.confirmDialog.keyValueNote.lblValue.text = viewModel.transactionCurrency; 


        //ConfigurationDriven
        if (applicationManager.getConfigurationManager().internationalTransactionFeeEnabled === "true") {
          this.view.confirmDialog.flxTransactionFee.setVisibility(true);
          this.view.confirmDialog.lblFee.text = applicationManager.getConfigurationManager().currencyCode[applicationManager.getConfigurationManager().getBaseCurrency()] + "" + applicationManager.getConfigurationManager().internationalTransactionFee;
          this.view.confirmDialog.flxRadio1.onClick = this.internationalfeeSelection.bind(this);
          this.view.confirmDialog.flxRadio2.onClick = this.internationalfeeSelection.bind(this);
        } else {
          this.view.confirmDialog.flxTransactionFee.setVisibility(false);
        }
      }

      if (viewModel.frequencyKey !== kony.i18n.getLocalizedString("i18n.transfers.frequency.once") && viewModel.howLongKey === "ON_SPECIFIC_DATE") {
        this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
        this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.DeliverBy");
        this.view.confirmDialog.keyValueFrequencyType.lblValue.text = viewModel.endOnDate;
      } else if (viewModel.frequencyKey !== kony.i18n.getLocalizedString("i18n.transfers.frequency.once") && viewModel.howLongKey === "NO_OF_RECURRENCES") {
        this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
        this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
        this.view.confirmDialog.keyValueFrequencyType.lblValue.text = viewModel.noOfRecurrences;
      } else {
        this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
      }

      this.view.confirmDialog.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
      if (commonUtilities.isCSRMode() && transferConfirm.makeTransferViewModel.isEdit) {
        this.view.confirmDialog.confirmButtons.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
      }
      else {
        this.view.confirmDialog.confirmButtons.btnConfirm.onClick = this.saveTransfer.bind(this, transferConfirm);
      }
      this.view.confirmDialog.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
      this.view.confirmDialog.confirmButtons.btnCancel.onClick = this.showTransferCancelPopup.bind(this, transferConfirm.makeTransferViewModel.onCancelCreateTransfer || presenter.showTransferScreen.bind(presenter))
      this.view.confirmDialog.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
      this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
        presenter.modifyTransaction(transferConfirm);
      }.bind(this);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.customheader.customhamburger.activateMenu("Transfers")
      this.AdjustScreen();
    },

    /**
     * used to set the fee selection
     * @returns {boolean} gives the fee selection
     */
    internationalfeeSelection: function () {
      if (this.view.confirmDialog.imgRadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED) {
        this.view.confirmDialog.imgRadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED;
        this.view.confirmDialog.imgRadioBtn1.skin = "sknC0C0C020pxNotFontIcons";
        this.view.confirmDialog.imgRadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
        this.view.confirmDialog.imgRadioBtn2.skin = "sknLblFontTypeIcon3343e820px";
      } else {
        this.view.confirmDialog.imgRadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
        this.view.confirmDialog.imgRadioBtn1.skin = "sknLblFontTypeIcon3343e820px";
        this.view.confirmDialog.imgRadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED;
         this.view.confirmDialog.imgRadioBtn2.skin = "sknC0C0C020pxNotFontIcons";
      }
    },

    /**
     * get the feePaidByReceipent
      * @returns {boolean} gives the fee selection
     */
    getPaidByReceipent: function () {
      var feePaidByReceipent = true;
      if (this.view.confirmDialog.imgRadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED) {
        feePaidByReceipent = false;
      } else {
        feePaidByReceipent = true;
      }
      return feePaidByReceipent;
    },

    /**Formats the Currency
     * @param  {String} amount amount string 
     * @param  {boolean} currencySymbolNotRequired currency symbol
     * @param {String} currencySymbolCode currencySymbolCode
     * @returns  {String} required currency
     */
    formatCurrency: function (amount, currencySymbolNotRequired,currencySymbolCode) {
      return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired,currencySymbolCode);
    },

    /** Callback for confirm transfer button
       * @param {object} transferConfirm Transfer Details
       */
    saveTransfer: function (transferConfirm) {
      var presenter = applicationManager.getModulesPresentationController("TransferModule");
      FormControllerUtility.showProgressBar();
      if (transferConfirm.makeTransferViewModel.editTransactionObject) {
        presenter.editTransfer(this.updateTransactionObject(transferConfirm.makeTransferViewModel.editTransactionObject, transferConfirm.transferData));
      }
      else {
        this.filterTransferData(transferConfirm.transferData);
      }
    },
    /** Filter the Data
       * @param {date} date date from components
       * @return {String} date in string
       */
    handleDateFormat: function (date) {
      if (date === undefined || date === null || date === '') {
        return null;
      }
      if (date instanceof Date) {
        return ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '/' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + date.getFullYear();
      } else {
        var dateObj = new Date(date);
        return ((dateObj.getMonth() + 1) < 10 ? '0' + (dateObj.getMonth() + 1) : dateObj.getMonth() + 1) + '/' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate()) + '/' + dateObj.getFullYear();
      }
    },
    /** Filter the Data
       * @param {object} transferData Transfer Details
       */
    filterTransferData: function (transferData) {
      var accountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
      var transactionType = transferData.accountTo instanceof accountsModel ? 'InternalTransfer' : 'ExternalTransfer';
      var toAccountNumber = transferData.accountTo instanceof accountsModel ? transferData.accountTo.accountID : transferData.accountToKey;
      var externalAccountNumber = null;
      var ibanNumber = null;
      if (transferData.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        ibanNumber = transferData.IBAN ? applicationManager.getFormatUtilManager().deFormatIBAN(transferData.IBAN) :""
      } else {
        externalAccountNumber = transactionType === "ExternalTransfer" ? transferData.accountToKey : null;
      }

      var commandData = {
        fromAccountNumber: transferData.accountFrom,
        accountFrom: transferData.accountFrom,
        accountTo: transferData.accountTo,
        amount: applicationManager.getFormatUtilManager().deFormatAmount(transferData.amount),
        notes: transferData.notes,
        ExternalAccountNumber: externalAccountNumber,
        isScheduled: this.isFutureDate(transferData.sendOnDateComponents) || transferData.frequencyKey !== 'Once' ? "1" : "0",
        transactionType: transactionType,
        frequencyType: transferData.frequencyKey,
        numberOfRecurrences: transferData.howLongKey === 'NO_OF_RECURRENCES' ? transferData.noOfRecurrences : null,
        frequencyEndDate: this.handleDateFormat(transferData.howLongKey === 'ON_SPECIFIC_DATE' ? this.getDateObj(transferData.endOnDateComponents) : null),
        scheduledDate: this.handleDateFormat(this.getDateObj(transferData.sendOnDateComponents)),
        sendOnDateComponents: transferData.sendOnDateComponents,
        endOnDateComponents: transferData.endOnDateComponents,
        sendOnDate: transferData.sendOnDate,
        endOnDate: transferData.endOnDate,
        frequencyKey: transferData.frequencyKey,
        howLongKey: transferData.howLongKey,
        IBAN: ibanNumber,
        type: transferData.type,
        newDomesticRecipient: transferData.newDomesticRecipient,
        newInternationalRecipient: transferData.newInternationalRecipient,
        reciepientName: transferData.reciepientName,
        swiftCode: transferData.swiftCode,
        bankName: transferData.bankName,
        transactionCurrency: transferData.transactionCurrency,
        feeCurrency : applicationManager.getConfigurationManager().getBaseCurrency(),
        toAccountNumber : toAccountNumber
      }

      if (transferData.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        if (applicationManager.getConfigurationManager().internationalTransactionFeeEnabled === "true") {
          commandData.fee = applicationManager.getConfigurationManager().internationalTransactionFee;
          commandData.feePaidByReceipent = this.getPaidByReceipent();
          if (!commandData.feePaidByReceipent) {
            commandData.amount = parseFloat(applicationManager.getFormatUtilManager().deFormatAmount(transferData.amount)) + parseFloat(commandData.fee) + "";
          }
        }
      }

      var presentationController = applicationManager.getModulesPresentationController("TransferModule");
      presentationController.createTransfer(commandData);
    },
    /** Compares Date with todays and tell is its future or not
       * @param  {object} dateComponents object
       * @returns {boolean} True for future else false
       */
    isFutureDate: function (dateComponents) {
      var dateObj = this.getDateObj(dateComponents)
      var endTimeToday = new Date();
      var minutes = ViewConstants.MAGIC_NUMBERS.MAX_MINUTES;
      endTimeToday.setHours(ViewConstants.MAGIC_NUMBERS.MAX_HOUR, minutes, minutes, minutes);
      if (dateObj.getTime() > endTimeToday.getTime()) {
        return true;
      }
      return false;
    },
    /** Show Transfer Cancel popup for Cancel Button
       * @param {function} onCancelListener function call on click of cancel button
       */
    showTransferCancelPopup: function (onCancelListener) {
      var self = this;
      this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfer.QuitTransfer");
      this.view.flxPopup.setVisibility(true);
      this.view.CustomPopup.lblHeading.setFocus(true);
      var height = self.view.flxFooter.frame.height + self.view.flxFooter.frame.y;
      self.view.flxPopup.height = height + ViewConstants.POSITIONAL_VALUES.DP;
      this.view.CustomPopup.btnYes.onClick = onCancelListener;
      this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
      this.view.CustomPopup.flxCross.onClick = function () {
        self.view.flxPopup.setVisibility(false);
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        self.view.flxPopup.setVisibility(false);
      }
      this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.dontCancelTransaction');
      this.view.forceLayout();
    },

    /** If editing a transaction, updates transaction object
       * @param {object} transaction Existing transaction
       * @param {object} viewModel Transfer Data
       * @returns {object} transaction
       */
    updateTransactionObject: function (transaction, viewModel) {
      transaction.fromAccountNumber = viewModel.accountFrom.accountID;
      transaction.amount = viewModel.amount;
      transaction.transactionsNotes = viewModel.notes;
      transaction.frequencyType = viewModel.frequencyKey;
      transaction.scheduledDate = this.getDateObj(viewModel.sendOnDateComponents);
      transaction.numberOfRecurrences = viewModel.noOfRecurrences === "" && viewModel.howLongKey === 'NO_OF_RECURRENCES' ? null : viewModel.noOfRecurrences;
      transaction.frequencyStartDate = this.getDateObj(viewModel.sendOnDateComponents);
      transaction.frequencyEndDate = this.getDateObj(viewModel.endOnDateComponents);
      return transaction;
    },

    /** Get date from date components
       * @param {object} dateComponents Date Components from calendar object
       * @returns {object} date 
       */
    getDateObj: function (dateComponents) {
      var date = new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1]) - 1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0, 0, 0, 0)
      return date;
    },


    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @param {integer} width - current browser width
    */
    onBreakpointChange: function (width) {
      this.view.confirmButtons.skin = "slFbox";
      if (width === 640) {
        this.view.customheader.lblHeaderMobile.text = "Confirm";
      }
      else {
        this.view.customheader.lblHeaderMobile.text = "";
        if (width == 1024) {
          this.view.lblAmount.left = "";
          this.view.lblAmount.right = "1dp";
          this.view.confirmButtons.btnConfirm.left = "";
          this.view.confirmButtons.btnConfirm.right = "0dp";
          this.view.confirmButtons.btnConfirm.width = "150dp";
          this.view.confirmButtons.btnModify.left = "";
          this.view.confirmButtons.btnModify.right = "170dp";
          this.view.confirmButtons.btnCancel.left = "";
          this.view.confirmButtons.btnCancel.right = "340dp";
          this.view.confirmButtons.btnModify.width = "150dp";
        }

      }
      this.AdjustScreen();
    }
  }
});