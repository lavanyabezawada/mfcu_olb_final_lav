define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_dd411cbfc33b4cb891eded6c936f160c: function AS_FlexContainer_dd411cbfc33b4cb891eded6c936f160c(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_c5abde421f4d43b89978f41fa1581d86: function AS_Button_c5abde421f4d43b89978f41fa1581d86(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_c9fc75a0213342f786db420511429d78: function AS_Button_c9fc75a0213342f786db420511429d78(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_j49e3a602007497a9917fe821ca09b54: function AS_Button_j49e3a602007497a9917fe821ca09b54(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_gc29acbb0b0e4cc4ab58c52d49bad353: function AS_Button_gc29acbb0b0e4cc4ab58c52d49bad353(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_cfb68b5b533f4d9781c6721ca5483021: function AS_Button_cfb68b5b533f4d9781c6721ca5483021(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_a0c6e1863304415ea350e14532d37ba9: function AS_Button_a0c6e1863304415ea350e14532d37ba9(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_d1a5b04b7d0d4ef8b7d6359dcd121fca: function AS_Button_d1a5b04b7d0d4ef8b7d6359dcd121fca(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_ja63832e3526498d9513d42df75fbe7e: function AS_Button_ja63832e3526498d9513d42df75fbe7e(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_fb2220eb7fe546b3819f9876fe8d88d9: function AS_Button_fb2220eb7fe546b3819f9876fe8d88d9(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_f171630311174e629241931cf074883c: function AS_Button_f171630311174e629241931cf074883c(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_a0830fb10c804c1eb2202af7282e50e5: function AS_Button_a0830fb10c804c1eb2202af7282e50e5(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_a0674f2c289b490c9f38ff9b6318becf: function AS_Button_a0674f2c289b490c9f38ff9b6318becf(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmConfirmEur **/
    AS_Form_de3bbb10d1cc42929aaecdac08a9bcee: function AS_Form_de3bbb10d1cc42929aaecdac08a9bcee(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmConfirmEur **/
    AS_Form_j2b8212d5d464514bb2d712eaddd6790: function AS_Form_j2b8212d5d464514bb2d712eaddd6790(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmConfirmEur **/
    AS_Form_ae32e8e47b4949be8dae736689bba147: function AS_Form_ae32e8e47b4949be8dae736689bba147(eventobject) {
        var self = this;
        this.postShowFrmConfirm();
    },
    /** onDeviceBack defined for frmConfirmEur **/
    AS_Form_e01b6a05a2914f47a4e4900b20f110d1: function AS_Form_e01b6a05a2914f47a4e4900b20f110d1(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmConfirmEur **/
    AS_Form_c9de87469a404cebb84c09f83e5bec51: function AS_Form_c9de87469a404cebb84c09f83e5bec51(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});