define(['CommonUtilities','CSRAssistUI','ViewConstants','FormControllerUtility', 
'OLBConstants'],function(CommonUtilities,CSRAssistUI,ViewConstants,FormControllerUtility,OLBConstants){
  var radioBtnSelectedKey = "rbg1";
  var acknowledge = "acknowledge";
  var acknowledgementInternalAccount = "acknowledgementInternalAccount";
  var verifyAccount = "verifyAccount";
  var invertedComma = "'";
  var frmConfirmAccount = "frmConfirmAccount";
  return { 
    
    /** Manages the upcomming flow
     * @param  {object} viewModel object consisting data based on which new flow has to drive  
     */
    updateFormUI: function(viewModel){
      if (viewModel.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
        } else { 
            FormControllerUtility.hideProgressBar(this.view);
        }
      if(viewModel.verifyAccounts) {this.showVerifyAccountScreen(viewModel.verifyAccounts);}
      if(viewModel.invalidCredential) {this.errorWhileValidating();}
      if(viewModel.errorVerifyAccount) {this.errorWhileValidating(viewModel.errorVerifyAccount);}
      if(viewModel.verifyAccounts) {this.showVerifyAccountScreen(viewModel.verifyAccounts);}
      if (viewModel.validateByCredential) {this.showAcknowledgment();}
      if (viewModel.validateByTrialDeposit) {this.showNextSteps();}
      if(viewModel.internalAccount){this.setInternalAccount(viewModel);}
      if(viewModel.domesticAccount){this.setDomesticAccount(viewModel);}
      if(viewModel.internationalAccount){this.setInternationalAccount(viewModel);}
      this.AdjustScreen();
    },
    /** Sets the value for international Account
     * @param {object} viewModel data to set
     */
    setInternationalAccount: function(viewModel){
      var self = this;
      var Verifydata= viewModel.internationalAccount;
      this.setValues(Verifydata);
      this.checkSkin();
      this.view.verifyByCredential.flxCheckbox.onClick=function(){
        CommonUtilities.toggleFontCheckbox(self.view.verifyByCredential.lblChecbox);
        self.checkSkin();
      };
      this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
      this.normalSkin();
      this.view.btnConfirm.onClick = function () {
        self.checkVerificationOptions(viewModel);
      };
      this.view.btnCancel.onClick=function(){
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.showTransferScreen();
      }
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
      this.setBreadCrumbs(Verifydata);
      this.view.btnAddAnotherAccount.onClick = this.addAnotherAccount.bind(this, viewModel);
      this.view.btnMakeTransfer.onClick = this.makeTransfer.bind(this, viewModel);
    },
    /** Sets data to show and gives verification options
     */
    setDataAndShowVerifyOptions: function(viewModel){
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");
      this.normalSkin();
      this.showVerificationOptions(viewModel);
      this.view.forceLayout();  
    },
    /** Sets common data like breadcrumbs
     */
    setBreadCrumbs: function(Verifydata){
      this.internalAccountAcknowledgement(Verifydata.beneficiaryName);
      this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo;
      this.view.acknowledgment.lblRefrenceNumber.setVisibility(true);
      this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]); 
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");
    },
    /** Sets the value for domestic Account
     * @param {object} viewModel data to set
     */
    setDomesticAccount: function(viewModel){
      var self = this;
      var Verifydata= viewModel.domesticAccount;
      this.setValues(Verifydata);
      this.checkSkin();
      this.view.verifyByCredential.flxCheckbox.onClick=function(){
        CommonUtilities.toggleFontCheckbox(self.view.verifyByCredential.lblChecbox);
        self.checkSkin();
      };

      this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
      //this.normalSkin();
      this.view.btnConfirm.onClick = function () {
        self.checkVerificationOptions(viewModel);
      };
      this.view.btnCancel.toolTip= kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.btnCancel.onClick=function(){
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.showTransferScreen();
      }
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount'); 
      this.setBreadCrumbs(Verifydata);
      this.view.btnAddAnotherAccount.onClick = this.addAnotherAccount.bind(this, viewModel);
      this.view.btnMakeTransfer.onClick = this.makeTransfer.bind(this, viewModel);
    },
    /** Sets the value for internal Account
     * @param {object} viewModel data to set
     */
    setInternalAccount: function(viewModel){
      var Verifydata= viewModel.internalAccount;
      this.setValues(Verifydata);
      this.internalAccountAcknowledgement(Verifydata.beneficiaryName);
      this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo;
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.add_kony_account");
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountAcknowledge')}]); 
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountAcknowledge");
      this.view.btnAddAnotherAccount.onClick = this.addAnotherAccount.bind(this, viewModel);
    },
    /** Sets the value of Account
     * @param {object} Verifydata data to set
     */
    setValues: function(Verifydata){
      this.view.lblBillerValue.text = Verifydata.bankName;
      this.view.lblAccountTypeValue.text = Verifydata.accountType;
      this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
      this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
      if(Verifydata.IBAN) {
        this.view.flxAccountType.setVisibility(false);
        this.view.lblAccountNumber.text = kony.i18n.getLocalizedString("i18n.Accounts.IBAN");
        this.view.lblAccountNumberValue.text = applicationManager.getFormatUtilManager().formatIBAN(Verifydata.IBAN);
      } else {
        this.view.flxAccountType.setVisibility(true);
        this.view.lblAccountType.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
        this.view.lblAccountTypeValue.text = Verifydata.swiftCode;
        this.view.lblAccountNumber.text = kony.i18n.getLocalizedString("i18n.common.accountNumber");
        this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
      }
      this.view.rtxBillerAddress.isVisible=false;
      this.view.lblBankAddress.isVisible=false;
      this.view.flxsemicolon.isVisible=false;
    },
    /** Manages the post show of the
     */
    postShowVerifyAccount: function(){   
      this.AdjustScreen();
    },
    /** UI code to adjust screen
     */
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
        {this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;}
        else
        {this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;}
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
        this.view.forceLayout();
      }
    },
    /**
     * Method to show verify account screen using amounts sent by bank
     * @param {object} selectedRow Details of selected account
     */
    showVerifyAccountScreen: function(selectedRow) {
      var self=this;
      this.hideAll();
      this.view.flxAcknowledgeActions.setVisibility(false);
      this.view.flxVerifyActions.setVisibility(true);
      this.blockedSkin();
      this.setAmountValidation(this.view.verifyByCredential.tbxUsername);
      this.setAmountValidation(this.view.verifyByCredential.tbxPassword);      
      CommonUtilities.setLblCheckboxState(false,this.view.verifyByCredential.lblChecbox);
      this.view.verifyByCredential.flxCheckbox.onClick=function(){
        CommonUtilities.toggleFontCheckbox(self.view.verifyByCredential.lblChecbox);
        self.validateDepositInput();
      };

      this.view.verifyByCredential.setVisibility(true);
      this.view.lblBillerValue.text = selectedRow.lblBankName;
      this.view.lblAccountTypeValue.text = selectedRow.lblAccountTypeValue;
      this.view.lblAccountNumberValue.text = selectedRow.txtAccountNumber.text;
      this.view.lblBeneficiaryNameValue.text = selectedRow.lblSeparator;
      this.view.lblAccountNickNameValue.text = selectedRow.lblAccountName;
      this.view.verifyByCredential.tbxPassword.secureTextEntry = false;
      this.view.verifyByCredential.lblUsername.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount1");
      this.view.verifyByCredential.lblPassword.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount2");
      this.view.verifyByCredential.tbxUsername.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
      this.view.verifyByCredential.tbxPassword.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyAccount");
      this.view.verifyByCredential.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyByProvidingDeposits");
      this.view.verifyByCredential.Label0f96e5b8496f040.text = selectedRow.lblBankName;
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
        callback: function() {
          var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
          transferModule.presentationController.showExternalAccounts();
        }
      }, {
        text: kony.i18n.getLocalizedString('i18n.transfers.verifyAccount')
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.btnBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccount");
      this.view.verifyByCredential.tbxUsername.left = 0;
      this.view.verifyByCredential.tbxUsername.left = "40" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
      this.view.verifyByCredential.tbxPassword.left = 0;
      this.view.verifyByCredential.tbxPassword.left = "40" + ViewConstants.POSITIONAL_VALUES.PERCENTAGE;
      this.view.lblHeadingVerify.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
      this.view.verifyByCredential.lblUsername.skin = ViewConstants.SKINS.USERNAME;
      this.view.verifyByCredential.lblPassword.skin = ViewConstants.SKINS.USERNAME;
      this.view.breadcrumb.btnBreadcrumb2.skin = ViewConstants.SKINS.BREADCRUM;
      this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
      this.view.btnConfirm.onClick = function() {
        self.onBtnConfirmVerifyAndAdd({
          "accountNumber": selectedRow.txtAccountNumber.text,
          "firstDeposit": self.view.verifyByCredential.tbxUsername.text,
          "secondDeposit": self.view.verifyByCredential.tbxPassword.text,
          "nickName": selectedRow.lblAccountName,
          "accountType": selectedRow.lblAccountTypeValue
        });
      };
      this.view.btnCancel.toolTip= kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.btnCancel.onClick=function(){
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.showExternalAccounts();
      };
    },
    /**Method to hide widgets
     */
    hideAll: function () {
      this.view.verifyBankAccount.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.view.flxPrint.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(false);
      this.view.acknowledgment.setVisibility(false);
    },
    /**Method to apply blocked skin
     */
    blockedSkin: function(){
      FormControllerUtility.disableButton(this.view.btnConfirm);
    },
    /**Method to validate amount fields
     * @param {object} widget widget reference
     */
    setAmountValidation:function(widget){
      var self=this;
      this.amountFieldWrapper = FormControllerUtility.wrapAmountField(widget)
        .onKeyUp(this.validateDepositInput.bind(this));
    },
    /**Method to Validate the inputs by user
     */
    validateDepositInput: function() {
      var deposit1 = this.view.verifyByCredential.tbxUsername.text;
      var deposit2 = this.view.verifyByCredential.tbxPassword.text;
      var chkBoxState=FormControllerUtility.isFontIconChecked(this.view.verifyByCredential.lblChecbox);
      if (this.isBlank(deposit1) || this.isBlank(deposit2)|| !deposit1 || !deposit2 || !chkBoxState ) {
        this.blockedSkin();
      } else {
        this.normalSkin();
      }
    },
    /**Method to show verify account screen using amounts sent by bank
     * @param {String} input ENtered field
     */
    isBlank:function(input){
      var pattern=/\s/;   //to check blank inputs
      return input.match(pattern);
    },
    /** Method gets triggered at preshow of form to set data
     * @param {object} selectedRow data of the seleted row
     */
    onBtnConfirmVerifyAndAdd: function(selectedRow){
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.confirmVerifyAndAdd(selectedRow);
      this.view.breadcrumb.btnBreadcrumb2.skin=ViewConstants.SKINS.BREADCRUM;
    },
    /** Method gets triggered at preshow of form to set data
     */
    preshowfrmVerifyAccount: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.flxPrint.setVisibility(false);
      if(CommonUtilities.isCSRMode()){
        CSRAssistUI.setCSRAssistConfigurations(scopeObj,'frmVerifyAccount');
      }
      this.view.verifyByCredential.flxWarning.setVisibility(false);
      var blankText = "";
      this.view.verifyByCredential.tbxUsername.text=blankText;
      this.view.verifyByCredential.tbxPassword.text=blankText;
      this.view.verifyBankAccount.lblRadioType1.onTouchEnd = function(){
        scopeObj.RadioBtnAction(scopeObj.view.verifyBankAccount.lblRadioType1, scopeObj.view.verifyBankAccount.lblRadioType2);
      };
      this.view.verifyBankAccount.lblRadioType2.onTouchEnd = function(){
       scopeObj.RadioBtnAction(scopeObj.view.verifyBankAccount.lblRadioType1, scopeObj.view.verifyBankAccount.lblRadioType2);
      };
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = radioBtnSelectedKey;
      this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.verifyBankAccount.rbtVerifyByDeposits.setVisibility(false);
      this.view.verifyBankAccount.rbtVerifyBankCredential.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
      CommonUtilities.setLblCheckboxState(false,this.view.verifyByCredential.lblChecbox);
      applicationManager.getNavigationManager().applyUpdates(this);      
    },
    /**
     * Method to show error if validation of username and password fails
     * @param {String} errmsg error message to show
     */
    errorWhileValidating: function(errmsg){
      this.view.verifyByCredential.tbxUsername.text="";
      this.view.verifyByCredential.tbxPassword.text="";
      if(errmsg){
        this.view.verifyByCredential.rtxWarning.text=errmsg;
      }else{
        this.view.verifyByCredential.rtxWarning.text=kony.i18n.getLocalizedString("i18n.transfers.incorrectCredentials");
      }
      CommonUtilities.setLblCheckboxState(false,this.view.verifyByCredential.lblChecbox);
      this.view.verifyByCredential.flxWarning.setVisibility(true);
      this.blockedSkin();
      this.view.forceLayout();
    },
    
            /**
         * Toggles the Radio Button selection state for Image Labels rendered by font icons
         * @param {object} RadioBtn1 Refernce to lbl widget 1
         * @param {object} RadioBtn2 Refernce to lbl widget 2
         */
    RadioBtnAction: function (RadioBtn1, RadioBtn2) {
            if (RadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED) {
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED;
                RadioBtn1.skin = ViewConstants.SKINS.RADIO_BUTTON_NOT_FONT_ICON;
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
                RadioBtn2.skin = ViewConstants.SKINS.RADIO_BUTTON_FONT_ICON;
            } else {
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED;
                RadioBtn2.skin = ViewConstants.SKINS.RADIO_BUTTON_NOT_FONT_ICON;;
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
                RadioBtn1.skin = ViewConstants.SKINS.RADIO_BUTTON_FONT_ICON;
            }
    },
    /**
     * Method to show acknowledgment
     */
    showAcknowledgment: function(){
      this.internalAccountAcknowledgement(this.view.lblAccountNickNameValue.text);//Call to set an Acknowledgement 
      this.view.verifyByCredential.flxWarning.setVisibility(false);
      this.view.acknowledgment.setVisibility(true);
      this.view.verifyByCredential.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(false);
      this.view.verifyBankAccount.setVisibility(false);
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.transferToThatPayee');
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayAddAnotherAccount");
      this.view.flxVerifyActions.setVisibility(false);
      this.view.flxAcknowledgeActions.setVisibility(true);
      this.view.acknowledgment.lblRefrenceNumber.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(false);
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.add_kony_account");
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountAcknowledge')}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountAcknowledge");	
      FormControllerUtility.hideProgressBar(this.view);
    },
    /**
     * Method to show acknowledgment for internal Account
     * @param {String} AccountName Name of the Account
     */
    internalAccountAcknowledgement: function (AccountName) {
      this.view.acknowledgment.lblTransactionMessage.text = invertedComma+AccountName+invertedComma+kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully");
      this.acknowledge();
      if (CommonUtilities.isPrintEnabled()) {
        this.view.flxPrint.setVisibility(true);
        this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
      } else {
        this.view.flxPrint.setVisibility(false);
      }
         },
    /**
     * Method to set visibility in acknowledgement
     */
    showNextSteps: function(){
      this.view.acknowledgment.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(true);
      this.view.verifyBankAccount.setVisibility(false);
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayAddAnotherAccount");
      this.view.flxVerifyActions.setVisibility(false);
      this.view.flxAcknowledgeActions.setVisibility(true); 
      FormControllerUtility.hideProgressBar(this.view);
    },
    /**
     * Method to assign normal skin
     */
    normalSkin: function(){
      FormControllerUtility.enableButton(this.view.btnConfirm);
    },
    /**
     * Method to show acknowledgment
     */
    acknowledge: function () {
      this.hideAll();
      this.view.acknowledgment.setVisibility(true);
      this.switchActions(acknowledgementInternalAccount);
    },
    /**
     * Method to toggle acknowledgment Flow
     */
    switchActions: function (type) {
      switch (type) {
        case acknowledge:
          this.view.flxAcknowledgeActions.setVisibility(true);
          this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
          this.view.flxVerifyActions.setVisibility(false);
          break;
        case acknowledgementInternalAccount:
          this.view.flxAcknowledgeActions.setVisibility(true);
          this.view.flxVerifyActions.setVisibility(false);
          break;
        case verifyAccount:
          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          break;
        default : break;
      }
    },
    /**
     * Method to Verify by deposit
     */
    verifyByDeposits: function () {
      this.hideAll();
      this.view.verifyNextSteps.setVisibility(true);
      this.switchActions(acknowledge);
    },
    /**
     * Method to show Verify by credentials
     */
    verifyByCredentials: function () {
      this.hideAll();
      this.blockedSkin();
      this.view.verifyByCredential.setVisibility(true);
      this.switchActions(verifyAccount);
    },
    /**
     * Method to decide whether to verify by deposit or credentials 
     */
    nextStep: function () {
      if (this.view.verifyBankAccount.lblRadioType2.text !== ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED) {
        this.view.verifyBankAccount.lblRadioType2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
        this.view.lblRadioType2.skin = ViewConstants.SKINS.RADIO_BUTTON_FONT_ICON;
        this.verifyByDeposits();
        return;
      }
      if (this.view.verifyBankAccount.lblRadioType1.text !== ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED) {
        this.view.verifyBankAccount.lblRadioType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED;
        this.view.verifyBankAccount.lblRadioType1.skin = ViewConstants.SKINS.RADIO_BUTTON_FONT_ICON;
        this.verifyByCredentials();
        return;
      }
      if (this.view.verifyByCredential.isVisible) {
        this.acknowledge();
        return;
      }
    },  
    /**
     * Method to set actions on credentials flow
     */
    setActionsOnCredential: function (){
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = radioBtnSelectedKey;
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
    },
    /**
     * Method to set Actions on verify by deposit flow
     */
    setActionsOnTrialDeposit: function(){
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = null;
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = radioBtnSelectedKey;
    },
    /**
     * Method to set initial action
     */
    setActions: function () {
      this.view.verifyBankAccount.rbtVerifyBankCredential.onSelection = function () {
        this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
      }.bind(this);
      this.view.verifyBankAccount.rbtVerifyByDeposits.onSelection = function () {
        this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = null;
      }.bind(this);
    },
    /**
     * Method to make transfer from Add External Account Acknowledgement screen
     */
    makeTransfer : function(viewModel) {
      var obj = {};
      obj.repeatTransactionObject = {}
      if (viewModel.internationalAccount) {
          obj.repeatTransactionObject.ExternalAccountNumber = viewModel.internationalAccount.accountNumber;
          obj.repeatTransactionObject.toAccountName = viewModel.internationalAccount.beneficiaryName;
          obj.repeatTransactionObject.swiftCode = viewModel.internationalAccount.swiftCode;
          obj.repeatTransactionObject.bankName = viewModel.internationalAccount.bankName;   
      } else {
          obj.repeatTransactionObject.IBAN = viewModel.domesticAccount.IBAN;
          obj.repeatTransactionObject.toAccountName = viewModel.domesticAccount.beneficiaryName;
      }
      obj.repeatTransactionObject.transactionType = applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER);
      obj.repeatTransactionObject.type = viewModel.internationalAccount ? OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT: OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.showTransferScreen({
          accountTo: obj
      });
    },
    /**
     * Method to make New Transfer
     */
    makeNewTransfer: function(){
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      if(this.view.btnMakeTransfer.text === kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"))
      {transferModule.presentationController.showTransferScreen({accountTo:this.view.lblAccountNumberValue.text});}
      else
      {transferModule.presentationController.showTransferScreen();}
    },
    /**
     * Method to check username and password validation for enable/disable of button
     */
    checkSkin: function(){
      if (!this.view.verifyByCredential.isVisible) {
        return;
      }
      if (this.view.verifyByCredential.tbxUsername.text !== "") {
        if (this.view.verifyByCredential.tbxPassword.text !== "") {
          if(CommonUtilities.isFontIconChecked(this.view.verifyByCredential.lblChecbox))
            {this.normalSkin();}
          else 
            {this.blockedSkin();}
        }
        else 
          {this.blockedSkin();}
      }
      else 
        {this.blockedSkin();}

    },

    /**
     * Method to handle verify by credentials using username and password
     */
    checkVerificationOptions: function(viewModel){
      var data;
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      this.view.verifyByCredential.lblHeading.text=kony.i18n.getLocalizedString("i18n.transfers.nextStepsToVerifyAccount");
      this.view.verifyByCredential.Label0f96e5b8496f040.text=kony.i18n.getLocalizedString("i18n.transfers.verifyCredentials");
      this.view.verifyByCredential.tbxUsername.onKeyUp=function(){};
      this.view.verifyByCredential.tbxPassword.onKeyUp=function(){};
      this.view.verifyByCredential.tbxUsername.onEndEditing=function(){};
      this.view.verifyByCredential.tbxPassword.onEndEditing=function(){};
      if (this.view.verifyBankAccount.isVisible) {
        if (this.view.verifyBankAccount.lblRadioType1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED) {
          this.view.acknowledgment.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");	
          this.blockedSkin();
          this.view.verifyByCredential.setVisibility(true);
          this.view.verifyByCredential.tbxPassword.secureTextEntry = true;
          this.view.verifyByCredential.lblUsername.text=kony.i18n.getLocalizedString("i18n.common.EnterUsername")+":";
          this.view.verifyByCredential.lblPassword.text=kony.i18n.getLocalizedString("i18n.common.EnterPassword")+":";
          this.view.verifyByCredential.tbxUsername.placeholder=kony.i18n.getLocalizedString("i18n.common.EnterUsername");
          this.view.verifyByCredential.tbxPassword.placeholder=kony.i18n.getLocalizedString("i18n.common.EnterPassword");
          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.verifyNextSteps.setVisibility(false);
          this.view.verifyBankAccount.setVisibility(false);
        } else if (this.view.verifyBankAccount.lblRadioType2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED) {
          data = {
            ExternalAccountNumber: this.view.lblAccountNumberValue.text
          };
          transferModule.presentationController.addToVerifyAccount(data, viewModel);
          this.view.acknowledgment.setVisibility(false);
          this.view.verifyByCredential.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]);   
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");
          this.view.verifyNextSteps.setVisibility(true);
          this.view.verifyBankAccount.setVisibility(false);
          if (CommonUtilities.isPrintEnabled()) {
            this.view.flxPrint.setVisibility(true);
            this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
          } else {
            this.view.flxPrint.setVisibility(false);
          }
        } 

      }
      else if (this.view.verifyByCredential.isVisible) {
        if (this.view.verifyByCredential.tbxUsername.text !== null) {
          if (this.view.verifyByCredential.tbxPassword.text !== null) {
            if(CommonUtilities.isFontIconChecked(this.view.verifyByCredential.lblChecbox))
              {data = {
                userName: this.view.verifyByCredential.tbxUsername.text,
                password: this.view.verifyByCredential.tbxPassword.text
              };}
              transferModule.presentationController.verifyCredentials(data, viewModel);
          }
        }
      }
    },
    addAnotherAccount: function (data) {
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.addAnotherAccount(data);
    },
    /**
     * Method to show Verify by Credentials or verify by Trial Deposit
     */
    showVerificationOptions: function(viewModel){
      this.view.acknowledgment.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.normalSkin();
      if(applicationManager.getConfigurationManager().verifyByCredentials==="true"){
        this.view.verifyNextSteps.setVisibility(false);
        this.view.verifyBankAccount.setVisibility(true);
        this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
        this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");	
        this.view.verifyBankAccount.setVisibility(true);
        this.view.flxVerifyActions.setVisibility(true);
        this.view.flxAcknowledgeActions.setVisibility(false);

      } 
      else{
        var data;
        data = {
          ExternalAccountNumber: this.view.lblAccountNumberValue.text,
        };
        this.presenter.addToVerifyAccount(data, viewModel);
      }
    },

          /**
         * onClick action of print button
         * @returns {void} - None
         */
        onClickPrint: function () {
          var scopeObj = this;
          var subData = [], tableList = [];
          if (this.view.acknowledgment.isVisible) {
              subData.push({
                  key: this.view.acknowledgment.lblRefrenceNumber.text,
                  value: this.view.acknowledgment.lblRefrenceNumberValue.text
              });
          }
          subData.push({
              key: this.view.lblBankName.text,
              value: this.view.lblBillerValue.text
          });
          subData.push({
              key: this.view.lblAccountType.text,
              value: this.view.lblAccountTypeValue.text
          });
          subData.push({
              key: this.view.lblAccountNumber.text,
              value: this.view.lblAccountNumberValue.text
          });  
          subData.push({
              key: this.view.lblBeneficiaryName.text,
              value: this.view.lblBeneficiaryNameValue.text
          });
          subData.push({
              key: this.view.lblAccountNickNameKey.text,
              value: this.view.lblAccountNickNameValue.text
          });
          tableList.push({
            tableHeader  : this.view.lblHeadingVerify.text,
            tableRows  : subData
          });
          subData=[];
          if(this.view.verifyNextSteps.isVisible){
            subData.push({
                key: "1.",
                value: this.view.verifyNextSteps.lblStep1.text
            });
            subData.push({
                key: "2.",
                value: this.view.verifyNextSteps.lblStep2.text
            });
            subData.push({
                key: "3.",
                value: this.view.verifyNextSteps.CopylblStep0g47625bb21d744.text
            });
            tableList.push({
              tableHeader  : this.view.verifyNextSteps.lblHeading.text,
              tableRows  : subData
          });
          }
         
          applicationManager.getModulesPresentationController("TransferModule").showPrintPage({ 
            printKeyValueGroupModel : { 
            "moduleHeader": scopeObj.view.lblAddAccountHeading.text, 
            "printCallback": scopeObj.navigateToAcknowledgement, 
            "tableList" : tableList
           } });
      },
      /**
       * callback function binded to cancel button of print view
       * @returns {void} - None
       */
      navigateToAcknowledgement: function () {
          applicationManager.getNavigationManager().navigateTo("frmVerifyAccount");
          applicationManager.getNavigationManager().updateForm({}, "frmVerifyAccount");
          if (CommonUtilities.isPrintEnabled()) {
            this.view.flxPrint.setVisibility(true);
            this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
          } else {
            this.view.flxPrint.setVisibility(false);
          }
      },
  };
});