define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function (commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
  var offset = OLBConstants.DEFAULT_OFFSET;
  var KonyBankAccountsTransfer = "KonyBankAccountsTransfer";
  var OtherKonyAccountsTransfer = "OtherKonyAccountsTransfer";
  var OtherBankAccountsTransfer = "OtherBankAccountsTransfer";
  var DomesticWireTransfer = "DomesticWireTransfer";
  var InternationalAccountsTransfer = "InternationalAccountsTransfer";
  var InternationalWireTransfer = "InternationalWireTransfer";
  var CONFIG_WIRE_TRANSFER = 'WIRE_TRANSFER';
  var newDomesticRecipient = null;
  var newInternationalRecipient = null;

  var frequencies = {
    'Once': "i18n.transfers.frequency.once",
    'Daily': "i18n.Transfers.Daily",
    'Weekly': "i18n.Transfers.Weekly",
    'BiWeekly': "i18n.Transfers.EveryTwoWeeks",
    'Monthly': "i18n.Transfers.Monthly",
    'Quarterly': "i18n.Transfers.Quaterly",
    'Half Yearly': "i18n.Transfers.HalfYearly",
    'Yearly': "i18n.Transfers.Yearly"
  };
  var forHowLong = {
    ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
    NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
  };
  var transferToTypes = {
    OWN_INTERNAL_ACCOUNTS: {
      external: false,
      filterFunction: function (userAccount) {
        return userAccount.supportTransferTo === "1";
      }
    },
    OTHER_INTERNAL_MEMBER: {
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isSameBankAccount === 'true';
      }
    },
    OTHER_EXTERNAL_ACCOUNT: {
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isSameBankAccount === 'false' || externalAccount.isSameBankAccount === null;
      }
    },
    INTERNATIONAL_ACCOUNT: {
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isInternationalAccount === 'true';
      }
    },
    WIRE_TRANSFER: {
      external: true,
      filterFunction: function (externalAccount) {
        return false;
      }
    }
  };
  return {
    initialLoadingDone: false,
    transfersViewModel: {
      transactionsData: [],
      first: 0,
      last: 10
    },
    
    /** Manages the upcomming flow
     * @param  {object} viewModel object consisting data based on which new flow has to drive  
     */
    updateFormUI: function (viewModel) {
      if (viewModel.serverError) {
        this.showServerError(viewModel.serverError);
      } else {
        if (viewModel.isLoading === true) {
          FormControllerUtility.showProgressBar(this.view);
        } else if (viewModel.isLoading === false) {
          FormControllerUtility.hideProgressBar(this.view);
        }
        if (viewModel.resetAndShowProgressBar) { this.hideAll(); }
        if (viewModel.transferError) { this.showtransferError(viewModel.transferError); }
        if (viewModel.modifyTransaction) { this.modifyTransfer(viewModel.modifyTransaction) }
        if (viewModel.gateway) { this.updateGateWay(viewModel.gateway); }
        if (viewModel.frequencyModel) { this.frequencies = viewModel.frequencyModel; }
        if (viewModel.showRecentTransfers) { this.resetFormForRecentTransfers(); }
        if (viewModel.accountsValue) { this.filterAccounts(viewModel.accountsValue); }
        if (viewModel.repeatTransaction) { this.filterRepeatTransactionAccounts(viewModel.repeatTransaction); }
        if (viewModel.noTransaction) { this.showNoTransactions() };
        if (viewModel.pendingAccounts) { this.showPendingAccounts(viewModel.pendingAccounts); }
        if (viewModel.recentTransfers) { this.showRecentsData(viewModel.recentTransfers, viewModel.config, viewModel.pagination); }
        if (viewModel.scheduledTransfers) { this.showScheduledData(viewModel.scheduledTransfers, viewModel.config, viewModel.pagination); }
        if (viewModel.externalAccounts) { this.showExternalAccounts(viewModel.externalAccounts, viewModel.config, viewModel.pagination); }
        if (viewModel.viewSelectedExternalAccount) { this.showSelectedExternalAccount(viewModel.viewSelectedExternalAccount); }
        if (viewModel.viewExternalAccountTransactionActivity) { this.showExternalAccountTransactionActivity(viewModel.viewExternalAccountTransactionActivity); }
        if (viewModel.searchTransferPayees) {
          this.showSearchTransferPayees(viewModel.searchTransferPayees);
        }
        if (viewModel.editTransaction) { this.filterDataToEditTransaction(viewModel.editTransaction); }
        if (viewModel.noMoreRecords) {
          this.showNoMoreRecords();
        }
        if (viewModel.externalAccountsByIbanorName) {
          this.selectIbanorName(viewModel.externalAccountsByIbanorName);
        }
        if (viewModel.interNationalExternalAccounts) {
          this.selectInterNationalAccountNumber(viewModel.interNationalExternalAccounts);
        }
        if (viewModel.swiftCodeSuccess) {
          this.setInternationalSwiftCodeFlow(viewModel.bankName);
        }
      }
      this.AdjustScreen();
      this.updateHamburgerMenu();
      this.view.forceLayout();

    },
    /**
     * showNoMoreRecords - Handles zero records scenario in navigation.
     */
    showNoMoreRecords: function () {
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = null;
      alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
      FormControllerUtility.hideProgressBar(this.view);
    },
    // Workaround for Responsive issue. 
    modifyTransfer: function (modifyTransaction) {
      this.hideAll();
      this.showMainTransferWindow();
      this.showMakeTransfer();
      this.onToAccountChange(modifyTransaction);
      this.setChangeTransferTypeView(this.getValidTransferTypes(), { headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType") });
    },

    /**Preshow for Transfers 
     */
    preShowTransfers: function () {
      this.view.customheader.forceCloseHamburger();
      applicationManager.getLoggerManager().setCustomMetrics(this, false, "Transfers");
      this.view.flxVerifyAccount.setVisibility(false);
      this.view.customheader.headermenu.btnLogout.isVisible = true;
      this.view.lblVerifyButton.hoverSkin = "skncursor";
      this.view.transfermain.Search.txtSearch.text = "";
      this.view.transfermain.maketransfer.lblCurrencySymbolEur.text = applicationManager.getFormatUtilManager().getCurrencySymbol();
      applicationManager.getNavigationManager().applyUpdates(this);      
    },

    /**Converts account or external Account object to a list box item
   * @param  {object} fromAccount Account Model or ExternalAccount Model
   * @returns {array} Key Value Pair
   */
    toAccountListBoxMapper: function (toAccount) {
      var cardInfo = {};
      if (toAccount.accountType === "CreditCard") {
        cardInfo.CreditCard = {
          currentBalance: toAccount.currentBalance,
          lastStatementBalance: toAccount.lastStatementBalance,
          minimumDue: toAccount.minimumDue,
          dueDate: toAccount.dueDate
        }
      }
      return toAccount.accountID ? [toAccount.accountID, commonUtilities.getAccountDisplayName(toAccount), cardInfo] : [toAccount.accountNumber, toAccount.nickName, cardInfo];
    },
    /**Prefills a transfer form from view model
     * @param  {object} transferData View Model containing context and data
     */
    preFillTransfersForm: function (transferData) {

      this.view.transfermain.maketransfer.lbxFrequency.selectedKey = transferData.frequencyKey;
      var sendOnDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(transferData.sendOnDate, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
      this.view.transfermain.maketransfer.calSendOn.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
	  this.view.transfermain.maketransfer.calSendOn.dateComponents = sendOnDateObj ? [sendOnDateObj.getDate(), sendOnDateObj.getMonth() + 1, sendOnDateObj.getFullYear()] : this.view.transfermain.maketransfer.calSendOn.dateComponents;
      var endOnDateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(transferData.endOnDate, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
      this.view.transfermain.maketransfer.calEndingOn.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
	  this.view.transfermain.maketransfer.calEndingOn.dateComponents = endOnDateObj ? [endOnDateObj.getDate(), endOnDateObj.getMonth() + 1, endOnDateObj.getFullYear()] : this.view.transfermain.maketransfer.calEndingOn.dateComponents;
      this.view.transfermain.maketransfer.lbxForHowLong.selectedKey = transferData.howLongKey;
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = transferData.noOfRecurrences;
      this.view.transfermain.maketransfer.txtNotes.text = transferData.notes; //UI textbox widget name is change to txtNotes
      this.view.transfermain.maketransfer.lbxFromAccount.selectedKey = this.getFromAccountKey(transferData);
      this.view.transfermain.maketransfer.tbxAmountEur.text = transferData.amount;
      this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text = transferData.swiftCode ? transferData.swiftCode : "";
      this.view.transfermain.maketransfer.tbxIntBankNameKA.text = transferData.bankName ? transferData.bankName :"";
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmountEur);
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
      this.onToAccoutPopulation(transferData);
    },

    /**
     * used to get the from acount number
     * @param {object} transferData transferData
     * @returns {string} fromKey
     */
    getFromAccountKey: function (transferData) {
      if (transferData.accountFromKey) {
        return transferData.accountFromKey
      } else {
        if (this.view.transfermain.maketransfer.lbxFromAccount.masterData !== null) {
          return this.view.transfermain.maketransfer.lbxFromAccount.masterData[0][0]
        }
      }
    },

    /**
     * used to populate the toAccount details
     * @param {object} transferData transfer object
     */
    onToAccoutPopulation: function (transferData) {
      if (transferData.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER)) {
        this.setOwnInternalTransferUI(transferData);
        this.view.transfermain.maketransfer.lbxToAccount.selectedKey = transferData.accountToKey;
      } else if (transferData.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER) && transferData.IBAN) {
        this.setDomesticTransferUI(transferData);
      } else if (transferData.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER) && transferData.ExternalAccountNumber) {
        this.setInternationalTransferUI(transferData)
      }
    },

    /**Filters from account if to field is same. Needs to be called if there is only one item in to field
     * @param  {object} makeTransferViewModel Make Transfer Context
     */
    filterFromAccount: function (makeTransferViewModel) {
      var scopeObj = this;
      var toAccount = scopeObj.getToAccount(makeTransferViewModel, scopeObj.view.transfermain.maketransfer.lbxToAccount.selectedKey);

      if (makeTransferViewModel.toAccounts.length === 1 && makeTransferViewModel.fromAccounts.length > 1) {
        var fromAccountMasterData = makeTransferViewModel.fromAccounts.filter(function (fromAccountPair) {
          return toAccount !== fromAccountPair
        }).map(scopeObj.fromAccountsListBoxMapper)
        if (fromAccountMasterData.length < makeTransferViewModel.fromAccounts.length) {
          scopeObj.view.transfermain.maketransfer.lbxFromAccount.masterData = fromAccountMasterData;
        }
      }
    },
    /**Converts account object to listbox item
   * @param  {object} fromAccount Account Model Object
   * @returns {array}  key value pair
   */
    fromAccountsListBoxMapper: function (fromAccount) {
      return [fromAccount.accountID, commonUtilities.getAccountDisplayNameWithBalance(fromAccount)];
    },
    /**Filter Accounts To if from account is same
     * @param  {array} makeTransferViewModel Make Transfer Context
     */
    filterAccountsTo: function (makeTransferViewModel) {
      var scopeObj = this;
      var fromAccount = scopeObj.getFromAccount(makeTransferViewModel, scopeObj.view.transfermain.maketransfer.lbxFromAccount.selectedKey);
      if (makeTransferViewModel.toAccounts.length > 1) {
        var toAccountMasterData = makeTransferViewModel.toAccounts.filter(function (account) {
          return fromAccount !== account
        }).map(scopeObj.toAccountListBoxMapper);
        if (toAccountMasterData.length < makeTransferViewModel.toAccounts.length) {
          scopeObj.view.transfermain.maketransfer.lbxToAccount.masterData = toAccountMasterData;
          this.onToAccountChange();
        }
      }
    },
    /**Shows Make Transfer Screen
     */
    showMakeTransfer: function () {
      this.hideAll();
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.hamburger.transfers") }, { text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.transfermain.btnMakeTransfer.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED_MODIFIED;
      this.view.transfermain.flxMakeTransferForm.setVisibility(true);
      this.view.transfermain.flxRowSeperator.setVisibility(false);
      this.setSearchFlexVisibility(false);

      this.view.forceLayout();
    },
    /**Configure No Accounts Available UI
     * @param  {string} transferType Type constant of Transfer 
     */
    showNoAccountsAvailableUI: function (transferType) {
      function isExternal(type) {
        var externalAccountTypes = ['OTHER_EXTERNAL_ACCOUNT', 'WIRE_TRANSFER'];
        return externalAccountTypes.indexOf(type) > 0;
      }

      function getButtonConfig() {
        if (transferType === "OWN_INTERNAL_ACCOUNTS") {
          return {
            textKey: 'i18n.transfers.add_kony_account',
            description: 'i18n.transfers.noMyInternalAccount',
            navigator: function () {

            }
          }
        }
        if (transferType === "OTHER_INTERNAL_MEMBER") {
          return {
            textKey: 'i18n.transfers.add_kony_account',
            description: 'i18n.transfers.noOtherInternalAccounts',
            navigator: function () {
              applicationManager.getModulesPresentationController("TransferModule").showSameBankAccounts();
            }
          }
        }
        if (transferType === "INTERNATIONAL_ACCOUNT") {
          return {
            textKey: 'i18n.transfers.add_international_account',
            description: 'i18n.transfers.noInternationalAccount',
            navigator: function () {
              applicationManager.getModulesPresentationController("TransferModule").showInternationalAccounts();
            }
          }
        }
        if (transferType === "OTHER_EXTERNAL_ACCOUNT") {
          return {
            textKey: 'i18n.transfers.add_non_kony_account',
            description: 'i18n.transfers.noExternalAccount',
            navigator: function () {
              applicationManager.getModulesPresentationController("TransferModule").showDomesticAccounts();
            }
          }
        }
        else {
          return {
            textKey: 'i18n.transfers.add_kony_account',
            description: 'i18n.transfers.noOtherInternalAccounts',
            navigator: function () {
              applicationManager.getModulesPresentationController("TransferModule").showSameBankAccounts();
            }
          }
        }
      }


      this.view.flxTrasfersWindow.setVisibility(false);
      this.view.flxNoAccounts.setVisibility(true);
      this.view.NoAccounts.btnBack.onClick = function () {
        this.view.flxTrasfersWindow.setVisibility(true);
        this.view.flxNoAccounts.setVisibility(false);
      }.bind(this);
      var buttonConfig = getButtonConfig();
      this.view.NoAccounts.btnAddAccount.text = kony.i18n.getLocalizedString(buttonConfig.textKey);

      this.view.NoAccounts.txtMessage.text = kony.i18n.getLocalizedString(buttonConfig.description);
      this.view.NoAccounts.btnAddAccount.onClick = buttonConfig.navigator;

    },
    /** sets the Frequency with i18 value
     */
    getFrequencies: function () {
      var list = [];
      for (var key in frequencies) {
        if (frequencies.hasOwnProperty(key)) {
          list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
        }
      }
      return list;
    },
    /** sets the duration of the transaction with i18 value
     */
    getForHowLong: function () {
      var list = []
      for (var key in forHowLong) {
        if (forHowLong.hasOwnProperty(key)) {
          list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
        }
      }
      return list;
    },
    /**Resets the Transfer Form
     */
    resetTransfersForm: function () {
      newDomesticRecipient = null;
      newInternationalRecipient = null;
      this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text = "";
      this.view.transfermain.maketransfer.tbxIntBankNameKA.text = "";
      this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
      this.view.transfermain.maketransfer.flxToInternationalAccountsWrapper.setVisibility(false);
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = [];
      this.view.transfermain.maketransfer.lbxToAccount.masterData = [];
      this.view.transfermain.maketransfer.lbxFrequency.masterData = this.getFrequencies();
      var date = new Date();
      this.view.transfermain.maketransfer.calSendOn.dateComponents = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
      this.view.transfermain.maketransfer.calEndingOn.dateComponents = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
      this.view.transfermain.maketransfer.lbxForHowLong.masterData = this.getForHowLong();
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = "";
      this.view.transfermain.maketransfer.txtNotes.text = "";
      this.view.transfermain.maketransfer.tbxAmountEur.text = "";
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmountEur);
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
    },
    /**
   * getValidTransferTypes : Method to filter Transfer type actions w.r.t configurtions
   * @param {Array} actionKeys, action keys
   * @return {Array} transferTypes - eligible Transfer type actions
   */
    getValidTransferTypes: function (actionKeys) {
      var scopeObj = this;
      var actionObj;
      var transferTypes = [];
      actionKeys = actionKeys || [
        OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
        OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
        OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT
      ];

      actionKeys.forEach(function (actionKey) {
        actionObj = {
          key: actionKey
        };
        if (scopeObj.getTransferTypeVisibility(actionKey)) {
          if (actionKey === OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER) {
            actionObj.action = function () {
              var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
              WireTransferModule.presentationController.showWireTransfer();
            };
          } else {
            actionObj.action = function (fromAccount) {
              applicationManager.getModulesPresentationController("TransferModule").loadAccountsByTransferType(actionKey, null, fromAccount);
            };
          }
          transferTypes.push(actionObj);
        }
      });

      return transferTypes;
    },
    /**
   * setChangeTransferTypeView : Method to bind Right Transfer Type Actions 'Change Transfer Type' View/Flex
   * @param {Array} actions, action object {key, action} array
   * @param {Number} fromAccount, from Account for all transfer actions
   */
    setChangeTransferTypeView: function (actions, dataInputs) {
      var scopeObj = this;
      var transferTypeData = [];
      var actionObj;
      var widgetDataMap = null;

      dataInputs = dataInputs || {
        headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChangeTransferType")
      };

      var UIMap = {
        OWN_INTERNAL_ACCOUNTS: {
          displayName: kony.i18n.getLocalizedString("i18n.transfers.toMyKonyBankAccounts"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.toMyKonyBankAccounts")
        },
        OTHER_INTERNAL_MEMBER: {
          displayName: kony.i18n.getLocalizedString("i18n.transfers.toOtherKonyBankAccounts"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.toOtherKonyBankAccounts")
        },
        OTHER_EXTERNAL_ACCOUNT: {
          displayName: kony.i18n.getLocalizedString("i18n.transfers.toOtherBankAccounts"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.toOtherBankAccounts")
        },
        INTERNATIONAL_ACCOUNT: {
          displayName: kony.i18n.getLocalizedString("i18n.transfers.toInternationalAccounts"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.toInternationalAccounts")
        },
        WIRE_TRANSFER: {
          displayName: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
        }
      };

      if (actions.length > 0) {
        widgetDataMap = {
          "flxTransferType": "flxTransferType",
          "lblTransferType": "lblTransferType",
          "lblSeperator": "lblSeperator"
        };

        transferTypeData = actions.map(function (actionObj, i) {
          return {
            "template": "flxChangeTransferType",
            "lblTransferType": {
              "text": UIMap[actionObj.key].displayName,
              "toolTip": UIMap[actionObj.key].toolTip,
              "accessibilityconfig": {
                "a11yLabel": UIMap[actionObj.key].displayName,
              }
            },
            "flxTransferType": {
              "toolTip": UIMap[actionObj.key].toolTip,
              "onClick": actionObj.action.bind(null, dataInputs.fromAccount)
            },
            "lblSeperator": "lblSeperator"
          };
        });

        scopeObj.view.lblChangeTransferTypeHeader.text = dataInputs.headerText;
        scopeObj.view.lblChangeTransferTypeHeader.toolTip = dataInputs.headerText;
        scopeObj.view.segChangeTransferType.widgetDataMap = widgetDataMap;
        scopeObj.view.segChangeTransferType.setData(transferTypeData);
        scopeObj.view.segChangeTransferType.setVisibility(true);
        scopeObj.view.flxChangeTransferType.setVisibility(true); //show Change Tranfers form.
      } else {
        //Hide Complete Flex/UI
        scopeObj.view.flxChangeTransferType.setVisibility(false);
      }
    },
    /**Show main transfer window - contains tabs etc
     */
    showMainTransferWindow: function () {
      this.view.flxTrasfersWindow.setVisibility(true);
      this.view.flxNoAccounts.setVisibility(false);
    },
    /** filters the data from the backend response
     * @param {object} viewModel data comming from backend
     */
    filterAccounts: function (viewModel) {
      var makeTransfer = {
        fromAccounts: viewModel.userAccounts.filter(this.userAccountsFilter),
        toAccounts: viewModel.type ? this.getToAccountsByType(viewModel.type, viewModel.userAccounts, viewModel.externalAccounts) : viewModel.externalAccounts.filter(function (externalAccount) { return externalAccount.accountNumber === viewModel.accountTo }),
        defaultFromAccountNumber: viewModel.accountFrom ? viewModel.accountFrom : viewModel.userProfile.default_account_transfers,
        type: viewModel.type,
        limit: this.getMinMaxTransfersLimit(viewModel.type)
      };
      if (viewModel.accountTo) {
        if (viewModel.accountTo.repeatTransactionObject) {
          makeTransfer.repeatTransactionObject = viewModel.accountTo.repeatTransactionObject;
          makeTransfer.type = viewModel.accountTo.repeatTransactionObject.type;
          makeTransfer.repeatTransactionObject.accountFromKey = viewModel.userProfile.default_account_transfers;
        }
      }
      this.updateMakeTransferForm(makeTransfer);
    },
    /**Needs to be called when gateway need to be shown initially
   * @param  {object} gateway View Model containing from account number for preselection
   */
    updateGateWay: function (gateway) {
      this.showTransfersGateway();
      this.configureTransferGateway(gateway.overrideFromAccount);
    },
    /**Shows Transfer Gateway View
     */
    showTransfersGateway: function () {
      this.hideAll();
      this.view.flxTrasfersWindow.setVisibility(true);
      this.view.flxNoAccounts.setVisibility(false);
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.hamburger.transfers") }, { text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.transfermain.btnMakeTransfer.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED_MODIFIED;
      this.view.flxTrasfersWindow.setVisibility(true);
      this.view.transfermain.flxTransfersGateway.setVisibility(true);
      this.view.transfermain.maketransfer.flxToInternationalAccountsWrapper.setVisibility(false);
      this.setSearchFlexVisibility(false);
      this.view.transfermain.flxRowSeperator.setVisibility(false);
      this.view.flxAddAccountWindow.setVisibility(true);
      this.view.flxChangeTransferType.setVisibility(false); //Hide change tranfer.
      this.view.forceLayout();
    },
    /** Decides whether to show a Transfer Type
         * @returns {boolean} true if type needs to be shown , false otherwise
         */
    getTransferTypeVisibility: function (transferType) {
      switch (transferType) {
        case OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS:
          return applicationManager.getConfigurationManager().getConfigurationValue("isKonyBankAccountsTransfer") === "true";

        case OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT:
          return applicationManager.getConfigurationManager().getConfigurationValue("isOtherBankAccountsTransfer") === "true";

        case OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT:
          return applicationManager.getConfigurationManager().getConfigurationValue("isInternationalAccountsTransfer") === "true";

        default: return false;
      }
    },

    /**getDisplayDescription : This function gets the display description to be displayed 
         * @param  {String} DisplayName description to be displayed
         */
    getDisplayDescription: function (displayName) {
      var description = "";
      var i18nKey = "";
      var servicesForUser = applicationManager.getConfigurationManager().getServicesListForUser();
      if (servicesForUser) {
        servicesForUser.forEach(function (dataItem) {
          if (dataItem.displayName === displayName) {
            description = dataItem.serviceDesc;
          }
        });
        switch (displayName) {
          case "KonyBankAccountsTransfer":
            i18nKey = "i18n.transfers.toMyKonyBankAccountsDescription";
            break;
          case "OtherBankAccountsTransfer":
            i18nKey = "i18n.transfers.toOtherBankAccountsDescription";
            break;
          case "InternationalAccountsTransfer":
            i18nKey = "i18n.transfers.toInternationalAccountsDescription";
            break;
        }
        var bundle = {};
        bundle[i18nKey] = description;
        kony.i18n.updateResourceBundle(bundle, "en");
      }
      return i18nKey;
    },
    /**Configure Transfer Gateway using a config
     * @param  {array} overrideFromAccount AccountID for preselection
     */
    configureTransferGateway: function (overrideFromAccount) {
      var transferGatewayConfig = [{
        key: OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toMyKonyBankAccounts",
        displayName: KonyBankAccountsTransfer,
        image: ViewConstants.IMAGES.KONYBANK_ACC
      },
      {
        key: OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toOtherBankAccounts",
        displayName: OtherBankAccountsTransfer,
        image: ViewConstants.IMAGES.OTHERBANK_ACC
      },
      {
        key: OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toInternationalAccounts",
        displayName: InternationalAccountsTransfer,
        image: ViewConstants.IMAGES.INTERNATIONALBANK_ACC,
        external: true
      }
      ]
      var self = this;
      var transferToViews = this.view.transfermain.flxTransfersGateway.widgets();

      function createCallback(config, visible) {
        if (config.key === CONFIG_WIRE_TRANSFER) {
          return function () {
            var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
            WireTransferModule.presentationController.showWireTransfer();
          }
        }
        return function () {
          applicationManager.getModulesPresentationController("TransferModule").loadAccountsByTransferType(config.key, null, overrideFromAccount);
        }

      }
      var visibleCounter = 0;
      for (var index = 0; index < transferGatewayConfig.length; index++) {
        var transferToView = transferToViews[visibleCounter + 1];
        var gatewayConfig = transferGatewayConfig[index];
        if (this.getTransferTypeVisibility(gatewayConfig.key)) {
          transferToView.setVisibility(true);
          var infoWidgets = transferToView.widgets()[0].widgets();
          infoWidgets[0].src = gatewayConfig.image;
          infoWidgets[1].text = kony.i18n.getLocalizedString(gatewayConfig.title);
          infoWidgets[2].text = kony.i18n.getLocalizedString(this.getDisplayDescription(gatewayConfig.displayName));
          var buttonProceed = transferToView.widgets()[1].widgets()[0];
          var buttonProceed1 = transferToView.widgets()[2];
          buttonProceed.onClick = createCallback(gatewayConfig);
          buttonProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
          buttonProceed1.onClick = createCallback(gatewayConfig);
          transferToView.onClick = createCallback(gatewayConfig);
          visibleCounter++;
        }
        this.setVisibilityOfAddRecipients(gatewayConfig.displayName);
      }

      for (var i = visibleCounter + 1; i < transferToViews.length; i++) {
        var view = transferToViews[i];
        view.setVisibility(false);
      }
      applicationManager.getModulesPresentationController("TransferModule").assignFrequency();
    },
    /** Decides whether to show a Add Accounts window
     * @param  {String} type - type of Transfer
    */
    setVisibilityOfAddRecipients: function (type) {
      if (type === OtherKonyAccountsTransfer) {
        if (applicationManager.getConfigurationManager().getConfigurationValue("isOtherKonyAccountsTransfer") === "false") {
          this.view.lblAddKonyAccount.setVisibility(false);
        }
      } else if (type === OtherBankAccountsTransfer) {
        if (applicationManager.getConfigurationManager().getConfigurationValue("isOtherBankAccountsTransfer") === "false") {
          this.view.lblAddNonKonyAccount.setVisibility(false);
        }
      } else if (type === InternationalAccountsTransfer) {
        if (applicationManager.getConfigurationManager().getConfigurationValue("isInternationalAccountsTransfer") === "false") {
          this.view.lblAddInternationalAccount.setVisibility(false);
        }
      }

    },
    /**Make Transfer Form Configuration based context - handles make transfer, repeats and edit transfer  
   * @param  {object} makeTransferViewModel - View Model containing context and data
   */
    updateMakeTransferForm: function (makeTransferViewModel) {
      this.showMainTransferWindow();
      var scopeObj = this;
      this.view.transfermain.maketransfer.lblCurrencySymbolEur.text = applicationManager.getFormatUtilManager().getCurrencySymbol();
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.skin = ViewConstants.SKINS.TRANSFERS_TEXTBOX_NOERROR;
      this.view.transfermain.maketransfer.calEndingOn.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
      this.view.transfermain.maketransfer.calSendOn.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
      this.view.transfermain.maketransfer.lbxFromAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.view.transfermain.maketransfer.lbxToAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.COMMON_FLEX_NOERRORSKIN;
      this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
      this.view.transfermain.maketransfer.flxTransferForm.setVisibility(true);
      this.view.transfermain.maketransfer.flxContainer3.setVisibility(false);
      this.view.transfermain.maketransfer.flxCurrencyAmountEur.setVisibility(true);
      this.AdjustScreen();
      this.setChangeTransferTypeView(this.getValidTransferTypes(), { fromAccount: makeTransferViewModel.defaultFromAccountNumber, headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChangeTransferType") });
      commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calSendOn);
      commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calEndingOn);
      this.view.transfermain.maketransfer.calSendOn.dateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
      this.view.transfermain.maketransfer.calEndingOn.dateComponents = this.view.transfermain.maketransfer.calEndingOn.dateComponents;
      this.resetTransfersForm();
      if (makeTransferViewModel.toAccounts.length === 0 && makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        this.showNoAccountsAvailableUI(makeTransferViewModel.type)
      }
      else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        this.setOwnInternalTransferUI(makeTransferViewModel);
      } else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        this.setDomesticTransferUI(makeTransferViewModel)
      } else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        this.setInternationalTransferUI(makeTransferViewModel)
      }
      this.showMainTransferWindow();
      this.showMakeTransfer();
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = makeTransferViewModel.fromAccounts.map(this.fromAccountsListBoxMapper);
      this.view.transfermain.maketransfer.lbxFromAccount.selectedKey = makeTransferViewModel.defaultFromAccountNumber;
      this.filterFromAccount(makeTransferViewModel);
      if (makeTransferViewModel.editTransactionObject) {
        this.preFillTransfersForm(this.generateTransferData(makeTransferViewModel, makeTransferViewModel.editTransactionObject));
        if (!makeTransferViewModel.type) {
          makeTransferViewModel.type = this.getTransactionType(makeTransferViewModel.editTransactionObject);
        }
      }
      if (makeTransferViewModel.repeatTransactionObject) {
        this.preFillTransfersForm(this.generateTransferData(makeTransferViewModel, makeTransferViewModel.repeatTransactionObject));
        if (!makeTransferViewModel.type) {
          makeTransferViewModel.type = this.getTransactionType(makeTransferViewModel.repeatTransactionObject);
        }
      }
      if (makeTransferViewModel.transferData) {
        this.preFillTransfersForm(makeTransferViewModel.transferData)
      }
      var transactionObj;
      if (makeTransferViewModel.editTransactionObject || makeTransferViewModel.repeatTransactionObject) {
        if (makeTransferViewModel.editTransactionObject !== undefined) {
          transactionObj = makeTransferViewModel.editTransactionObject
        } else {
          transactionObj = makeTransferViewModel.repeatTransactionObject
        }
        if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
          this.view.transfermain.maketransfer.lbxToAccount.onSelection(transactionObj);
        }
      } else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        this.view.transfermain.maketransfer.lbxToAccount.onSelection();
      }
      this.view.transfermain.maketransfer.btnConfirm.onClick = this.confirmTransfer.bind(this, makeTransferViewModel);
      this.view.transfermain.maketransfer.btnModify.onClick = makeTransferViewModel.onCancelCreateTransfer || function () {
        this.showTransfersGateway();
      }.bind(this);
      this.view.flxMakeTransferError.setVisibility(false);
    },

    /**
     * used to get the transaction type
     * @param {object} trans trans
     * @returns {String} type
     */
    getTransactionType: function (trans) {
      if (trans.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER)) {
        if (trans.IBAN) {
          return OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT;
        } else if (trans.ExternalAccountNumber) {
          return OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT;
        }
      } else if (trans.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER)) {
        return OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS
      }
    },

    /**
     * used to set the internal accounts view
     * @param {object} makeTransferViewModel makeTransferViewModel
     */
    setOwnInternalTransferUI: function (makeTransferViewModel) {
      var scopeObj = this;
      this.view.flxAddAccountWindow.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.setVisibility(false);
      this.view.transfermain.maketransfer.flxContainer2.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientName.setVisibility(false);
      this.view.transfermain.maketransfer.flxCurrencyEur.setVisibility(false);
      this.view.transfermain.maketransfer.lblCurrencySymbolEur.setVisibility(true);
      this.view.transfermain.maketransfer.lbxToAccount.masterData = makeTransferViewModel.toAccounts.map(this.toAccountListBoxMapper);
      this.filterAccountsTo(makeTransferViewModel);
      this.view.transfermain.maketransfer.lbxToAccount.onSelection = this.onToAccountChange;
      this.view.transfermain.maketransfer.lbxFromAccount.onSelection = function () {
        scopeObj.filterAccountsTo(makeTransferViewModel);
        scopeObj.view.transfermain.maketransfer.lbxFromAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
        scopeObj.view.transfermain.maketransfer.lbxToAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
        scopeObj.hideFieldError();
      }
    },

    /**
     * used to set the domestic fllow Ui
     * @param {object} transferObj makeTransferViewModel
     */
    setDomesticTransferUI: function (transferObj) {
      var scopeObj = this;
      this.view.flxAddAccountWindow.setVisibility(true);
      this.view.transfermain.maketransfer.flxContainer2.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.setVisibility(true);
      this.view.transfermain.maketransfer.flxCurrencyEur.setVisibility(false);
      this.view.transfermain.maketransfer.lblCurrencySymbolEur.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.flxCancelFilter.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.flxCancelFilter.onClick = scopeObj.clearIbanOrAccountNumber.bind(scopeObj);
      this.view.transfermain.maketransfer.flxRecipientName.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblRecipientIBAN.text = kony.i18n.getLocalizedString('i18n.TransferEur.reciepientIBAN');
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.placeholder = kony.i18n.getLocalizedString('i18n.common.searchByNameOrIBAN');
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.onKeyUp = commonUtilities.debounce(scopeObj.fetchRecipientbyIbanorName.bind(scopeObj),OLBConstants.FUNCTION_WAIT,false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.onEndEditing =  scopeObj.closeIBANAccountNumberPopup.bind(scopeObj);
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.onClick = scopeObj.getIbanValue.bind(scopeObj);
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text = transferObj.IBAN ? transferObj.IBAN : "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = transferObj.toAccountName ? transferObj.toAccountName : "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
    },

    /**
     * used to set the international flow UI
     * @param {object} transferObj makeTransferViewModel
     */
    setInternationalTransferUI: function (transferObj) {
      var scopeObj = this;
      this.view.flxAddAccountWindow.setVisibility(true);
      //Need to set form back end
      var supportedCurrencies = applicationManager.getConfigurationManager().getSupportedCurrencies();
      this.view.transfermain.maketransfer.lbxCurrencyEur.masterData = supportedCurrencies;
      this.view.transfermain.maketransfer.flxCurrencyEur.setVisibility(true);
      this.view.transfermain.maketransfer.lblCurrencySymbolEur.setVisibility(false);
      this.view.transfermain.maketransfer.flxContainer2.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientName.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.flxCancelFilter.setVisibility(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.flxCancelFilter.onClick = scopeObj.clearIbanOrAccountNumber.bind(scopeObj);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblRecipientIBAN.text = kony.i18n.getLocalizedString('i18n.WireTransfer.recipientAccountNumber');
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.placeholder = kony.i18n.getLocalizedString('i18n.common.searchByNameOrAccountNumber');
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.onClick = scopeObj.getIbanValue.bind(scopeObj);
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text = transferObj.ExternalAccountNumber ? transferObj.ExternalAccountNumber : "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = transferObj.toAccountName ? transferObj.toAccountName : "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.onKeyUp = commonUtilities.debounce(scopeObj.fetchInternationalRecipientsbyAccountNumber.bind(scopeObj),OLBConstants.FUNCTION_WAIT,false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.onEndEditing =  scopeObj.closeIBANAccountNumberPopup.bind(scopeObj);
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.onClick = scopeObj.getInternationalAccountNumber.bind(scopeObj);
    },

    /**Show Transfer error in the UI 
   * @param  {String} errorMsg  Transfer error message
   */
    showtransferError: function (errorMsg) {
      this.view.flxMakeTransferError.setVisibility(true);
      this.view.rtxMakeTransferError.text = errorMsg;
    },
    /**showPendingAccounts  - Shows wether is there any pending account for verification
   * @param  {object} pendingAccounts Array of pending Accounts 
   */
    showPendingAccounts: function (pendingAccounts) {
      var self = this;
      if (pendingAccounts.error === true) {
        this.view.flxVerifyAccount.setVisibility(false);
      } else {
        var count = 0;
        for (var i in pendingAccounts) {
          if (pendingAccounts[i].isVerified !== "true") {
            count += 1;
            break;
          }
        }
        if (count === 0) {
          this.view.flxVerifyAccount.setVisibility(false);
        } else {
          this.view.lblVerifyWarning.text = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount");
          this.view.lblVerifyWarning.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount");
          this.view.lblVerifyButton.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
          this.view.lblVerifyButton.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
          this.view.lblVerifyButton.onClick = function () {
            applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts();
          }
          this.view.flxVerifyAccount.setVisibility(false);
        }
      }
    },
    /**Called when post show is called
  */
    postShowtransfers: function () {
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.showPendingAccountsCount();
      this.view.customheader.forceCloseHamburger();
      var scope = this;
      this.view.onBreakpointChange = function () {
        scope.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      this.view.transfermain.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.lblAddKonyAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_kony_accountMod");
      this.view.lblAddNonKonyAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_non_kony_accountMod");
      this.view.lblAddInternationalAccount.toolTip = kony.i18n.getLocalizedString("i18n.transfers.add_international_accountMod");
      this.view.transfermain.lblScheduleAPayment.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.transfermain.maketransfer.calSendOn.hidePreviousNextMonthDates = true;
      this.view.transfermain.maketransfer.calEndingOn.hidePreviousNextMonthDates = true;  
      this.AdjustScreen();
    },
    /**Form Preshow actions to be done
     */
    initActions: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.transfermain.maketransfer.calSendOn.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
      this.view.transfermain.maketransfer.calEndingOn.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
      if (applicationManager.getConfigurationManager().getConfigurationValue("canSearchTransfers") === "true") {
        scopeObj.view.transfermain.flxSearchImage.setVisibility(true);
        scopeObj.view.transfermain.flxSearchImage.onClick = scopeObj.toggleSearchBox.bind(scopeObj);
        scopeObj.view.transfermain.Search.btnConfirm.onClick = scopeObj.onSearchBtnClick.bind(scopeObj);
        scopeObj.view.transfermain.Search.flxClearBtn.onClick = scopeObj.onSearchClearBtnClick.bind(scopeObj);
        scopeObj.view.transfermain.Search.txtSearch.onKeyUp = scopeObj.onTxtSearchKeyUp.bind(scopeObj);

      } else {
        scopeObj.view.transfermain.flxSearchImage.setVisibility(false);
      }
      //External Accounts
      scopeObj.externalAccountsSortMap = [
        { name: 'nickName', imageFlx: scopeObj.view.transfermain.imgSortDateExternal, clickContainer: scopeObj.view.transfermain.flxSortDateExternal },
        { name: 'bankName', imageFlx: scopeObj.view.transfermain.imgSortDescriptionExternal, clickContainer: scopeObj.view.transfermain.flxSortDescriptionExternal },
        { name: 'isVerified', imageFlx: scopeObj.view.transfermain.imgSortTypeExternal, clickContainer: scopeObj.view.transfermain.flxSortAmountExternal }
      ];
      FormControllerUtility.setSortingHandlers(scopeObj.externalAccountsSortMap, scopeObj.onExternalAccountsSortClickHandler, scopeObj);

      //Recent AND Scheduled Transactions.
      scopeObj.recentSortMap = [
        { name: 'transactionDate', imageFlx: scopeObj.view.transfermain.imgSortDate, clickContainer: scopeObj.view.transfermain.flxSortDate },
        { name: 'toaccountname', imageFlx: scopeObj.view.transfermain.imgSortDescription, clickContainer: scopeObj.view.transfermain.flxSortDescription },
        { name: 'amount', imageFlx: scopeObj.view.transfermain.imgSortType, clickContainer: scopeObj.view.transfermain.flxSortAmount }
      ];
      scopeObj.scheduledSortMap = [
        { name: 'scheduledDate', imageFlx: scopeObj.view.transfermain.imgSortDate, clickContainer: scopeObj.view.transfermain.flxSortDate },
        { name: 'toaccountname', imageFlx: scopeObj.view.transfermain.imgSortDescription, clickContainer: scopeObj.view.transfermain.flxSortDescription },
        { name: 'amount', imageFlx: scopeObj.view.transfermain.imgSortType, clickContainer: scopeObj.view.transfermain.flxSortAmount }
      ];

      //Hiding Error flex
      this.view.transfermain.onClick = scopeObj.hideIBANAccountNumberPopup.bind(scopeObj);
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.transfermain.flxSearchSortSeparator.setVisibility(false);
    
    },
    /**Adjusts screen - Places footer on correct place
     * @param  {number} data Offset needs to be added 
     */
    AdjustScreen: function (data) {
      this.view.forceLayout();
      if (data === undefined) {
        data = 0;
      }
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) { this.view.flxFooter.top = mainheight + diff + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP; }
        else { this.view.flxFooter.top = mainheight + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP; }
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT + data + ViewConstants.POSITIONAL_VALUES.DP;
        this.view.forceLayout();
      }
      this.initializeResponsiveViews();
    },
    /**Toggles Search Box
     */
    toggleSearchBox: function () {
      this.setSearchFlexVisibility(!this.view.transfermain.flxSearch.isVisible);
      if (this.view.transfermain.btnExternalAccounts.skin === ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED || this.searchView === true) {
        if (this.searchView === true) {
          applicationManager.getModulesPresentationController("TransferModule").getExternalAccounts(); //show external tab.
        } else {
          applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts(); //show external tab.          
        }
      }
      if (!this.view.transfermain.flxSearch.isVisible) {
        this.searchView = false;
        this.prevSearchText = '';
      }
      this.view.forceLayout();
      this.AdjustScreen();
    },
    /** On Search Text Key Up
     * @param  {object} event object
     */
    onTxtSearchKeyUp: function (event) {
      var scopeObj = this;
      var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
      if (searchKeyword.length > 0) {
        scopeObj.view.transfermain.Search.txtSearch.onDone = scopeObj.onSearchBtnClick.bind(scopeObj);
        scopeObj.enableSearch();
      } else {
        scopeObj.view.transfermain.Search.txtSearch.onDone = function () { };
        scopeObj.disableSearch();
      }
    },
    /** Searches for a payee
     */
    onSearchBtnClick: function () {
      var scopeObj = this;
      var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
      if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
        applicationManager.getModulesPresentationController("TransferModule").searchTransferPayees({
          'searchKeyword': searchKeyword
        });
        scopeObj.searchView = true;
        scopeObj.prevSearchText = searchKeyword;
      }
    },
    /** clears the text on search textbox
     */
    onSearchClearBtnClick: function () {
      //this.toggleSearchBox();
      this.setSearchFlexVisibility(true);
      if (this.searchView === true) {
        applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts(); //show external tab.
      }
      this.searchView = false;
      this.prevSearchText = '';
      this.view.forceLayout();
    },
    /** On External Account  Sort click handler.
     * @param  {object} event object
     * @param  {object} data New Sorting Data
     */
    onExternalAccountsSortClickHandler: function (event, data) {
      var scopeObj = this;
      scopeObj.first = 0;
      applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts(data);
    },
    /**Initial Actions mapping - called on preshow
     */
    initTabsActions: function () {
      var scopeObj = this;
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
      this.view.transfermain.Search.txtSearch.width = "100%";
      this.view.transfermain.imgSearch.src = ViewConstants.IMAGES.SEARCH_BLUE;
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
      this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
      this.view.flxTransferViewReport.isVisible = false;
      this.fixBreadcrumb();
      this.view.transfermain.maketransfer.flxNUORadioBtn1.onClick = this.onClickRadioButton;
      this.view.transfermain.maketransfer.flxNUORadioBtn2.onClick = this.onClickRadioButton;
      this.view.transfermain.maketransfer.flxNUORadioBtn3.onClick = this.onClickRadioButton;
      this.view.transfermain.maketransfer.flxNUORadioBtn4.onClick = this.onClickRadioButton;
      this.view.flxVerifyAccount.setVisibility(false);

      this.view.transfermain.maketransfer.tbxAmountEur.onTouchStart = function () {
        scopeObj.onClickRadioButton(scopeObj.view.transfermain.maketransfer.flxNUORadioBtn4);
      }
      this.view.transfermain.btnMakeTransfer.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
        this.view.transfermain.lblGatewayHeader.setFocus(true);
      }.bind(this);
      this.view.transfermain.btnExternalAccounts.onClick = function () {
        this.view.transfermain.Search.txtSearch.text = "";
        applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts();
        this.view.transfermain.Search.txtSearch.setFocus(true);
      }.bind(this);

      this.view.transfermain.btnExternalAccounts.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
      this.view.transfermain.btnRecent.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showRecentUserTransactions();
        this.view.transfermain.Search.txtSearch.setFocus(true);
      }.bind(this);
      this.view.transfermain.btnScheduled.onClick = function () {
        this.getUserScheduledTransactions();
        this.view.transfermain.Search.txtSearch.setFocus(true);
      }.bind(this);

      //Setting Actions for Add Kony Accounts
      this.view.flxAddKonyAccount.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showDomesticAccounts();
      }.bind(this);
      this.view.flxAddNonKonyAccount.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showDomesticAccounts();
      }.bind(this);
      this.view.flxAddInternationalAccount.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showInternationalAccounts();
      }.bind(this);
      this.view.transfermain.maketransfer.lbxForHowLong.onSelection = this.onHowLongChange.bind(this);
      this.view.transfermain.maketransfer.lbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
      this.view.transfermain.maketransfer.calSendOn.onSelection = this.setSkinToCalendar(scopeObj.view.transfermain.maketransfer.calSendOn);
      this.view.transfermain.maketransfer.calEndingOn.onSelection = this.setSkinToCalendar(scopeObj.view.transfermain.maketransfer.calEndingOn);
      this.makeTransferAmountField = FormControllerUtility.wrapAmountField(this.view.transfermain.maketransfer.tbxAmountEur)
        .onKeyUp(this.checkValidityMakeTransferForm);
      this.makeTransferCreditCardAmountField = FormControllerUtility.wrapAmountField(this.view.transfermain.maketransfer.tbxAmountEur)
        .onKeyUp(this.checkValidityMakeTransferForm);
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.onKeyUp = this.checkValidityMakeTransferForm.bind(this);
      this.view.transfermain.maketransfer.btnModify.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.transfermain.maketransfer.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.transfermain.maketransfer.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
      this.view.transfermain.flxSearchSortSeparator.setVisibility(false);
      this.view.transfermain.flxToMyKonyBankInfo.hoverSkin = "sknFlxffffffBottomCursor";
      this.view.transfermain.flxToOtherKonyBankInfo.hoverSkin = "sknFlxffffffBottomCursor";
      this.view.transfermain.flxToOtherBankInfo.hoverSkin = "sknFlxffffffBottomCursor";
      this.view.transfermain.flxToInternationalInfo.hoverSkin = "sknFlxffffffBottomCursor";
      this.view.transfermain.flxWireTransferInfo.hoverSkin = "sknFlxffffffBottomCursor";
      this.view.flxSperator2.top = "57dp";
      this.view.flxSeperator3.top = "57dp";
      this.view.transfermain.btnMakeTransfer.hoverSkin = "sknBtn72727215pxSSPBgf8f7f8";
      this.view.transfermain.btnRecent.hoverSkin = "sknBtn72727215pxSSPBgf8f7f8";
      this.view.transfermain.btnScheduled.hoverSkin = "sknBtn72727215pxSSPBgf8f7f8";
      this.view.transfermain.btnExternalAccounts.hoverSkin = "sknBtn72727215pxSSPBgf8f7f8";

    },

    /**Callback for calendar Field OnSelection
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
    setSkinToCalendar: function (widgetId) {
      widgetId.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
      this.hideFieldError();
    },

    /**
    * Set search flex visibility.
     * @param  {boolean} flag visibility value 
     */
    setSearchFlexVisibility: function (flag) {
      if (typeof flag === "boolean") {
        this.view.transfermain.imgSearch.src = flag ? ViewConstants.IMAGES.SELECTED_SEARCH : ViewConstants.IMAGES.SEARCH_BLUE;
        this.view.transfermain.flxSearch.setVisibility(flag);
        //this.view.transfermain.flxSearchSortSeparator.setVisibility(flag);
        if (flag === true) {
          this.view.transfermain.Search.txtSearch.text = '';
          this.view.transfermain.Search.txtSearch.setFocus();
          this.disableSearch();
        }
      }
    },
    /**Resets Bread crumb state
     */
    fixBreadcrumb: function () {
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
    },
    // COMMON
    onClickRadioButton: function (radioButton) {
      var self = this;
      var selectedButton;
      var allRadioButtions = ["lblRadioBtn1", "lblRadioBtn2", "lblRadioBtn3", "lblRadioBtn4"];
      if (radioButton && radioButton.children) {
        selectedButton = radioButton.children[0];
      } else {
        return;
      }
      var selectRadioButton = function (button) {
        var RadioBtn = self.view.transfermain.maketransfer[button];
        RadioBtn.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
      }
      var unSelectRadioButton = function (button) {
        var RadioBtn = self.view.transfermain.maketransfer[button];
        RadioBtn.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED;
      }
      allRadioButtions.forEach(function (button) {
        if (button === selectedButton) {
          selectRadioButton(button);
        } else {
          unSelectRadioButton(button);
        }
      });
      this.checkValidityMakeTransferForm();
    },
    /**Callback for Amount Field onEndEditing
     */
    onAmountChanged: function (obj) {
      commonUtilities.validateAndFormatAmount(obj);
      this.checkValidityMakeTransferForm();
      this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.COMMON_FLEX_NOERRORSKIN;
      this.hideFieldError();
    },
    /**  Returns form Data of Make Transfer Component
     * @param {object} transferObj transfer object
     * @returns {object} JSON containing all the form data
     */
    getFormData: function (transferObj) {
      var viewModel = {};
      viewModel.accountFromKey = this.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
      if (transferObj.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        viewModel.accountToKey = this.view.transfermain.maketransfer.lbxToAccount.selectedKey;
      } else if (transferObj.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        viewModel.IBAN = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.flxTransferForm.flxRecipientIBAN.txtRecipientIBAN.text;
        viewModel.reciepientName = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text;
        viewModel.newDomesticRecipient = newDomesticRecipient;
      } else if (transferObj.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        viewModel.accountToKey = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.flxTransferForm.flxRecipientIBAN.txtRecipientIBAN.text;
        viewModel.reciepientName = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text;
        viewModel.transactionCurrency = this.view.transfermain.maketransfer.lbxCurrencyEur.selectedKey;
        viewModel.newInternationalRecipient = newInternationalRecipient;
        if (newInternationalRecipient) {
          viewModel.swiftCode = this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text;
          viewModel.bankName = this.view.transfermain.maketransfer.tbxIntBankNameKA.text;
        } else {
          viewModel.swiftCode = this.view.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems ? this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].Lbl1 : this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text;
          viewModel.bankName = this.view.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems ? this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].Lbl3 : this.view.transfermain.maketransfer.tbxIntBankNameKA.text;
        }
      }

      if (this.isCreditCardSelected !== true) {
        viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.tbxAmountEur.text);
      } else {
        if (this.view.transfermain.maketransfer.lblRadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
          viewModel.radioButtonSelected = this.view.transfermain.maketransfer.lblRadioBtn1;
          viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblCurrentBalanceValue.text);
        } else if (this.view.transfermain.maketransfer.lblRadioBtn2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
          viewModel.radioButtonSelected = this.view.transfermain.maketransfer.lblRadioBtn2;
          viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblStatementBalanceValue.text);
        } else if (this.view.transfermain.maketransfer.lblRadioBtn3.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
          viewModel.radioButtonSelected = this.view.transfermain.maketransfer.lblRadioBtn3;
          viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblMinimumDueBalanceValue.text);
        } else {
          viewModel.radioButtonSelected = this.view.transfermain.maketransfer.lblRadioBtn4;
          viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.tbxAmountEur.text);
        }
      }

      viewModel.frequencyKey = this.view.transfermain.maketransfer.lbxFrequency.selectedKey;
      viewModel.howLongKey = this.view.transfermain.maketransfer.lbxForHowLong.selectedKey;
      viewModel.sendOnDate = this.view.transfermain.maketransfer.calSendOn.date;
      viewModel.sendOnDateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
      viewModel.endOnDate = this.view.transfermain.maketransfer.calEndingOn.date;
      viewModel.endOnDateComponents = this.view.transfermain.maketransfer.calEndingOn.dateComponents;
      viewModel.noOfRecurrences = this.view.transfermain.maketransfer.tbxNoOfRecurrences.text.trim();
      viewModel.notes = this.view.transfermain.maketransfer.txtNotes.text.trim();
      viewModel.langAmount = this.view.transfermain.maketransfer.tbxAmountEur.text;
      return viewModel;
    },
    /**Checks Validity For Make Transfer - Call on every field changes - Changes button state
   */
    checkValidityMakeTransferForm: function () {
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.skin = "CopysknSSP0e30a26ff0c5f44";
      var formData = this.getFormData({});
      if (formData.amount === null || formData.amount === "" || formData.amount === "NaN") {
        commonUtilities.disableButton(this.view.transfermain.maketransfer.btnConfirm);
        return;
      }
      if (formData.frequencyKey !== "Once" && formData.howLongKey === "NO_OF_RECURRENCES" && formData.noOfRecurrences === "") {
        commonUtilities.disableButton(this.view.transfermain.maketransfer.btnConfirm);
        return;
      }
      commonUtilities.enableButton(this.view.transfermain.maketransfer.btnConfirm);
      this.hideFieldError();
    },
    /** Disables Search Button
     */
    disableSearch: function () {
      //FormControllerUtility.disableButton(this.view.transfermain.Search.btnConfirm)
      this.view.transfermain.Search.flxClearBtn.setVisibility(false);
    },
    /** Enable Search Button
     */
    enableSearch: function () {
      // FormControllerUtility.disableButton(this.view.transfermain.Search.btnConfirm)
      this.view.transfermain.Search.flxClearBtn.setVisibility(true);
      this.view.forceLayout();
    },
    /**Hides all the main flexes and  Resets UI
     */
    hideAll: function () {
      this.view.transfermain.btnMakeTransfer.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELETED_MODIFIED;
      this.view.transfermain.btnRecent.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED;
      this.view.transfermain.btnScheduled.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED;
      this.view.transfermain.btnExternalAccounts.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED;
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.transfermain.flxMakeTransferForm.setVisibility(false);
      this.view.transfermain.segmentTransfers.setVisibility(false);
      this.view.transfermain.flxNoTransactions.setVisibility(false);
      this.view.transfermain.tablePagination.setVisibility(false);
      this.view.transfermain.flxTransfersGateway.setVisibility(false);
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transferactivity.isVisible = false;
      this.closeIBANAccountNumberPopup();
    },
    /**call when frequency is changed in make transfer form - Resets the UI
   */
    onFrequencyChanged: function () {
      this.view.transfermain.maketransfer.getFrequencyAndFormLayout(this.view.transfermain.maketransfer.lbxFrequency.selectedKey,
        this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
      this.checkValidityMakeTransferForm();
    },
    /**Call Back when for how long listbox value is changed - Resets UI based on selection
   */
    onHowLongChange: function () {
      this.view.transfermain.maketransfer.getForHowLongandFormLayout(this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
      this.checkValidityMakeTransferForm();
    },
    /**  Returns height of the page
     * @returns {String} height height of the page
     */
    getPageHeight: function () {
      var height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + ViewConstants.MAGIC_NUMBERS.FRAME_HEIGHT;
      return height + ViewConstants.POSITIONAL_VALUES.DP;
    },
    /**Hamburger Menu COnfiguration
     * @param  {object} sideMenuModel Side Menu viewModel 
     */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
      if (this.view.transfermain.btnMakeTransfer.skin === ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED) {
        this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
      }
      else if (this.view.transfermain.btnRecent.skin === ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED) {
        this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer History");
      }
      else if (this.view.transfermain.btnExternalAccounts.skin === ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED) {
        this.view.customheader.customhamburger.activateMenu("Transfers", "External Accounts");
      }
    },
    /** Tell if transfer is allowed from account
     * @param  {object} userAccount Details of the Account
     * @returns {boolean} if allowed the true is returned
     */
    userAccountsFilter: function (userAccount) {
      return userAccount.supportTransferFrom === "1";
    },
    /** Filter To Accounts based on type string
     * @param  {object} type Type constant  
     * @param  {array} userAccounts User Accounts
     * @param  {array} externalAccounts External Accounts
     * @returns {boolean} Array of Accounts or External Accounts
     */
    getToAccountsByType: function (type, userAccounts, externalAccounts) {
      var filterConfig = transferToTypes[type];
      return filterConfig.external ? externalAccounts.filter(filterConfig.filterFunction) : userAccounts.filter(filterConfig.filterFunction);
    },
    /** getMinMaxTransfersLimit  - Shows Confirmation for transfer
     * @param {String} type type of the account
     */
    getMinMaxTransfersLimit: function (type) {
      var limit = {
        minimum: "",
        maximum: ""
      };
      switch (type) {
        case "OWN_INTERNAL_ACCOUNTS":
          limit.minimum = applicationManager.getConfigurationManager().getConfigurationValue("minKonyBankAccountsTransferLimit");
          limit.maximum = applicationManager.getConfigurationManager().getConfigurationValue("maxKonyBankAccountsTransferLimit");
          break;
        case "OTHER_INTERNAL_MEMBER":
          limit.minimum = applicationManager.getConfigurationManager().getConfigurationValue("minOtherKonyAccountsTransferLimit");
          limit.maximum = applicationManager.getConfigurationManager().getConfigurationValue("maxOtherKonyAccountsTransferLimit");
          break;
        case "OTHER_EXTERNAL_ACCOUNT":
          limit.minimum = applicationManager.getConfigurationManager().getConfigurationValue("minOtherBankAccountsTransferLimit");
          limit.maximum = applicationManager.getConfigurationManager().getConfigurationValue("maxOtherBankAccountsTransferLimit");
          break;
        case "INTERNATIONAL_ACCOUNT":
          limit.minimum = applicationManager.getConfigurationManager().getConfigurationValue("minInternationalAccountsTransferLimit");
          limit.maximum = applicationManager.getConfigurationManager().getConfigurationValue("maxInternationalAccountsTransferLimit");
          break;
        default:
          limit.minimum = applicationManager.getConfigurationManager().getConfigurationValue("minInternationalAccountsTransferLimit");
          limit.maximum = applicationManager.getConfigurationManager().getConfigurationValue("maxInternationalAccountsTransferLimit");
      }
      return limit;
    },
    /**Returns Account Model Object for given account number
     * @param  {array} makeTransferViewModel Make Transfer Context
     * @param  {array} selectedKey AccountID 
     * @returns {object} Account Model Object
     */
    getFromAccount: function (makeTransferViewModel, selectedKey) {
      return makeTransferViewModel.fromAccounts.filter(function (fromAccount) {
        return fromAccount.accountID === selectedKey
      })[0]

    },
    /**Returns Account Model Object or ExternalAccount Object for given account number
     * @param  {array} makeTransferViewModel Make Transfer Context
     * @param  {array} selectedKey AccountID 
     * @returns {object} Account Model Object or ExternalAccount Object
     */
    getToAccount: function (makeTransferViewModel, selectedKey) {
      return makeTransferViewModel.toAccounts.filter(function (toAccount) {
        if (toAccount.accountID) {
          return toAccount.accountID === selectedKey;
        }
        else {
          return toAccount.accountNumber === selectedKey;
        }
      })[0]
    },
    /**Validated some of the business rules on transfer form data and shows field errors
     * @param  {object} formData Form Data
     * @returns {boolean} True if form data is correct and false if its not
     */
    validateTransferData: function (formData, makeTransferViewModel) {
      var currTime = new Date();
      var minMaxLimit = {};
      minMaxLimit = makeTransferViewModel.limit;
      var deFormatAmount = this.removeCurrencyWithCommas(formData.amount);
      var amountFloats = parseFloat(deFormatAmount);
      currTime.setHours(0, 0, 0, 0); // Sets to midnight.
      var sendOnDate = this.getDateObj(formData.sendOnDateComponents);
      var endOnDate = this.getDateObj(formData.endOnDateComponents);
      if (formData.accountFrom === formData.accountTo) {
        this.showFieldError("i18n.transfers.error.cannotTransferToSame");
        this.view.transfermain.maketransfer.lbxFromAccount.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERRORSKIN;
        this.view.transfermain.maketransfer.lbxToAccount.skin = ViewConstants.SKINS.LOANS_LISTBOX_ERRORSKIN;
        return false;
      }
      else if (!commonUtilities.isValidAmount(deFormatAmount)) {
        this.showFieldError("i18n.transfers.error.enterAmount");
        this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        return false;
      }
      else if (amountFloats < minMaxLimit.minimum) {
        this.showFieldErrorMinMaxError(kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + commonUtilities.formatCurrencyWithCommas(minMaxLimit.minimum));
        this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        return false;
      }
      else if (amountFloats > minMaxLimit.maximum) {
        this.showFieldErrorMinMaxError(kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + commonUtilities.formatCurrencyWithCommas(minMaxLimit.maximum));
        this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
        return false;
      }
      else if (sendOnDate.getTime() < currTime.getTime()) {
        this.showFieldError("i18n.transfers.error.invalidSendOnDate");
        this.view.transfermain.maketransfer.calSendOn.skin = ViewConstants.SKINS.SKNFF0000CAL;
        return false;
      }
      else if (formData.frequencyKey !== 'Once') {
        if (formData.howLongKey === 'ON_SPECIFIC_DATE') {
          if (endOnDate.getTime() < currTime.getTime()) {
            this.showFieldError("i18n.transfers.errors.invalidEndOnDate");
            this.view.transfermain.maketransfer.calEndingOn.skin = ViewConstants.SKINS.SKNFF0000CAL;
            return false;
          }
          if (endOnDate.getTime() === sendOnDate.getTime()) {
            this.showFieldError("i18n.transfers.errors.sameEndDate");
            this.view.transfermain.maketransfer.calEndingOn.skin = ViewConstants.SKINS.SKNFF0000CAL;
            return false;
          }
          if (endOnDate.getTime() < sendOnDate.getTime()) {
            this.showFieldError("i18n.transfers.errors.beforeEndDate");
            this.view.transfermain.maketransfer.calEndingOn.skin = ViewConstants.SKINS.SKNFF0000CAL;
            return false;
          }
        }
        if (formData.howLongKey === 'NO_OF_RECURRENCES') {
          if (formData.noOfRecurrences === null || formData.noOfRecurrences === "" || formData.noOfRecurrences === '0' || (isNaN(formData.noOfRecurrences) || !Number.isInteger(Number.parseFloat(formData.noOfRecurrences)))) {
            this.showFieldError("i18n.transfers.error.invalidNoOfRecurrences");
            this.view.transfermain.maketransfer.tbxNoOfRecurrences.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
            return false;
          }
        }

      } else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT && !applicationManager.getValidationUtilManager().isValidIBAN(formData.IBAN)) {
        this.showFieldError("i18n.TransferEur.invalidIBAN");
        return false;
      } else if (makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT ||
        makeTransferViewModel.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        if (!formData.reciepientName) {
          this.showFieldError("i18n.common.InvalidRecipientName");
          return false;
        }
      }
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.skin = ViewConstants.SKINS.TRANSFERS_TEXTBOX_NOERROR;
      this.view.transfermain.maketransfer.calEndingOn.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
      this.view.transfermain.maketransfer.calSendOn.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
      this.view.transfermain.maketransfer.lbxFromAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.view.transfermain.maketransfer.lbxToAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.view.transfermain.maketransfer.flxAmount.skin = ViewConstants.SKINS.COMMON_FLEX_NOERRORSKIN;
      this.hideFieldError();

      return true;
    },
    /**Send Form Model to P.C for showing confirmation to user
     * @param  {object} makeTransferViewModel OBject containing form data
     */
    confirmTransfer: function (makeTransferViewModel) {
      var formData = this.getFormData(makeTransferViewModel);
      formData.accountFrom = this.getFromAccount(makeTransferViewModel, formData.accountFromKey);
      formData.accountTo = this.getToAccount(makeTransferViewModel, formData.accountToKey);
      if (this.validateTransferData(formData, makeTransferViewModel)) {
        if (newInternationalRecipient) {
          this.showNewInternationalRecipient(makeTransferViewModel, formData);
        } else {
          applicationManager.getModulesPresentationController("TransferModule").confirmTransfer(makeTransferViewModel, formData);
        }
      }
    },

    /**Send Form Model to P.C for showing confirmation to user
     * @param  {object} makeTransferViewModel OBject containing form data
     */
    confirmTransferViaInternationalFlow: function (makeTransferViewModel) {
      var formData = this.getFormData(makeTransferViewModel);
      formData.accountFrom = this.getFromAccount(makeTransferViewModel, formData.accountFromKey);
      formData.accountTo = this.getToAccount(makeTransferViewModel, formData.accountToKey);
      applicationManager.getModulesPresentationController("TransferModule").confirmTransfer(makeTransferViewModel, formData);
    },

    /**
     * used to show the new recipient form
     * @param {object} makeTransferViewModel makeTransferViewModel
     * @param {object} formData formData
     */
    showNewInternationalRecipient: function (makeTransferViewModel, formData) {
      var scopeObj = this;
      FormControllerUtility.disableButton(this.view.transfermain.maketransfer.btnConfirm);
      this.view.transfermain.maketransfer.btnConfirm.onClick = this.confirmTransferViaInternationalFlow.bind(this, makeTransferViewModel);
      this.view.transfermain.maketransfer.btnModify.onClick = this.prevInternationalStep.bind(this, makeTransferViewModel);
      this.view.transfermain.maketransfer.flxTransferForm.setVisibility(false);
      this.view.transfermain.maketransfer.flxToInternationalAccountsWrapper.setVisibility(true);
      this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text = "";
      this.view.transfermain.maketransfer.tbxIntBankNameKA.text = "";
      this.view.transfermain.maketransfer.tbxIntBankNameKA.setEnabled(false);
      this.view.transfermain.maketransfer.tbxIntAccountNumberKA.text = formData.accountToKey;
      this.view.transfermain.maketransfer.tbxIntAccountNumberKA.setEnabled(false);
      this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.onEndEditing = scopeObj.swiftCodeSearch.bind(this, makeTransferViewModel, formData);
    },

    /**
     * used to navigate the preveious international step
     * @param {obejct} makeTransferViewModel makeTransferViewModel
     */
    prevInternationalStep: function (makeTransferViewModel) {
      if (newInternationalRecipient) {
        this.view.transfermain.maketransfer.flxToInternationalAccountsWrapper.setVisibility(false);
        this.view.transfermain.maketransfer.flxTransferForm.setVisibility(true);
        this.view.transfermain.maketransfer.btnConfirm.onClick = this.confirmTransfer.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.btnModify.onClick = makeTransferViewModel.onCancelCreateTransfer || function () {
          this.showTransfersGateway();
        }.bind(this);
        this.AdjustScreen();
        this.view.forceLayout();
      } else {
        this.showTransfersGateway();
      }
    },


    /**
     * used to search the swiftCode
     * @param {obejct} makeTransferViewModel makeTransferViewModel
     * @param {object} formData formData
     */
    swiftCodeSearch: function (makeTransferViewModel, formData) {
      var searchObj = {};
      searchObj.searchKeyword = this.view.transfermain.maketransfer.tbxIntSwiftCodeKA.text;
      if(searchObj.searchKeyword.length > 0)
      {
        FormControllerUtility.showProgressBar(this.view);
        var presentationController = applicationManager.getModulesPresentationController("TransferModule");
        presentationController.fetchExternalAccountsBySwiftCode(searchObj);
      }
      
    },

    /**
     * set the bank name
     * @param {string} bankName bank Name
     */
    setInternationalSwiftCodeFlow: function (bankName) {
      this.view.transfermain.maketransfer.tbxIntBankNameKA.text = bankName;
      FormControllerUtility.enableButton(this.view.transfermain.maketransfer.btnConfirm);
      FormControllerUtility.hideProgressBar(this.view);
    },


    /**Returns Date Object from Date Components
     * @param  {array} dateComponents Date Components returned from Calendar Widget
     * @return {date} date
     */
    getDateObj: function (dateComponents) {
      var date = new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1]) - 1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0, 0, 0, 0)
      return date;
    },
    /**Show field error for transfer form 
   * @param  {string} errorKey i18n key for Make Transfer form
   */
    showFieldError: function (errorKey) {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
      this.view.transfermain.maketransfer.lblWarning.text = kony.i18n.getLocalizedString(errorKey);
      this.AdjustScreen();
    },
    /**Validated some of the business rules on transfer form data and shows field errors
     * @param  {String} errorMsg Error message to be displayed
     */
    showFieldErrorMinMaxError: function (errorMsg) {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
      this.view.transfermain.maketransfer.lblWarning.text = errorMsg;
      this.AdjustScreen();
    },
    /**Hides The Field Error from make Transfer Form
     */
    hideFieldError: function () {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
      this.AdjustScreen();
    },
    /**onToAccountChange : methord to change cridit card actions.
   * @param  {object} editTransaction Details of the Transaction
   */
    onToAccountChange: function (editTransaction) {
      var selectedCard = this.view.transfermain.maketransfer.lbxToAccount.selectedkeyvalue ?
        this.view.transfermain.maketransfer.lbxToAccount.selectedkeyvalue[2] : {};
      if (selectedCard.CreditCard) {
        this.isCreditCardSelected = true;
        this.view.transfermain.flxSearchSortSeparator.setVisibility(false);
        this.view.transfermain.maketransfer.flxContainer4.setVisibility(false);
        this.view.transfermain.maketransfer.flxAmount.setVisibility(false);
        this.view.transfermain.maketransfer.lblAmount.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption1.setVisibility(true);
        this.view.transfermain.maketransfer.flxOption2.setVisibility(true);
        this.view.transfermain.maketransfer.flxOption3.setVisibility(true);
        this.view.transfermain.maketransfer.flxOption4.setVisibility(true);
        this.view.transfermain.maketransfer.lblDueDate.setVisibility(true);
        this.view.transfermain.maketransfer.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + " : " + this.getDateFromDateStr(selectedCard.CreditCard.dueDate) + ")";
        this.view.transfermain.maketransfer.lblCurrentBalanceValue.text = commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.currentBalance,false,selectedCard.CreditCard.currencyCode);
        this.view.transfermain.maketransfer.lblStatementBalanceValue.text = commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.lastStatementBalance,selectedCard.CreditCard.currencyCode);
        this.view.transfermain.maketransfer.lblMinimumDueBalanceValue.text = commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.minimumDue,selectedCard.CreditCard.currencyCode);
        this.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Once";
        if (editTransaction && editTransaction.amount && editTransaction.radioButtonSelected) {
          if (editTransaction.radioButtonSelected !== this.view.transfermain.maketransfer.lblRadioBtn4) {
            this.view.transfermain.maketransfer.tbxAmountEur.text = "";
            this.onClickRadioButton(editTransaction.radioButtonSelected);
          }
          else {
            this.view.transfermain.maketransfer.tbxAmountEur.text = editTransaction.amount + "";
            this.onClickRadioButton(editTransaction.radioButtonSelected);
          }
        } else {
          this.view.transfermain.maketransfer.tbxAmountEur.text = "";
          this.onClickRadioButton(this.view.transfermain.maketransfer.flxNUORadioBtn1);
        }
        this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmountEur);
        this.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
      } else {
        this.isCreditCardSelected = false;
        this.view.transfermain.maketransfer.flxOption1.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption2.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption3.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption4.setVisibility(false);
        this.view.transfermain.maketransfer.flxAmount.setVisibility(true);
        this.view.transfermain.maketransfer.lblAmount.setVisibility(true);
        this.view.transfermain.maketransfer.flxContainer4.setVisibility(true);
        this.view.transfermain.maketransfer.lblDueDate.setVisibility(false);
        this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(true);
        this.onFrequencyChanged();
        this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmountEur);
      }
      this.view.transfermain.maketransfer.lbxFromAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.view.transfermain.maketransfer.lbxToAccount.skin = ViewConstants.SKINS.COMMON_LISTBOX_NOERROR;
      this.hideFieldError();
      this.view.transfermain.maketransfer.forceLayout();
    },
    /**Entry Point Method for Recent Tab
   */
    getUserRecentTransactions: function () {
      //this.first = 0;
      //this.last = 10;
      applicationManager.getModulesPresentationController("TransferModule").fetchRecentUserTransactions(this, {
        "offset": this.first,
        "limit": this.last,
        'resetSorting': true
      });
    },
    /**Show Recent Tab Data  - Entry Point Method After Will Update UI 
   * @param  {Array} data Array of transactions model 
   * @param  {config} Sorting Config for the sorting
   */
    showRecentsData: function (data, config) {
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.recent")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.recent");
      this.setChangeTransferTypeView(this.getValidTransferTypes(), { headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType") });
      var scopeObj = this;
      this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.recent"));
      //this.view.transfermain.flxRowSeperator.setVisibility(true);
      this.showSegment();
      this.view.transfermain.btnRecent.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
      this.getNewTransfersData(data, this.getUserRecentTransactions);
      this.setRecentTransactionsPagination(config);
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(true);
      this.setSearchFlexVisibility(false);
      var break_point = kony.application.getCurrentBreakpoint();
      var recentsDataMap = {
        "btnRepeat": "btnRecentRepeat",
        "btnAction": "btnAction",
        "flxDropdown": "flxDropdown",
        "imgDropdown": "imgDropdown",
        "lblAmount": "lblAmount",
        "template": "recentTemplate",
        "lblDate": "lblDate",
        "lblFromAccountTitle": "lblFromAccountTitle",
        "lblFromAccountValue": "lblFromAccountValue",
        "lblIdentifier": "lblIdentifier",
        "lblNoteTitle": "lblNoteTitle",
        "lblNoteValue": "lblNoteValue",
        "lblRecurrenceTitle": "lblRecurrenceTitle",
        "lblRecurrenceValue": "newRecurrenceValue",
        "lblReferenceNumberTitle": "lblReferenceNumberTitle",
        "lblReferenceNumberValue": "lblReferenceNumberValue",
        "lblSendTo": "lblSendTo",
        "lblSeparator": "lblSeparator",
        "lblStatusTitle": "lblStatusTitle",
        "lblStatusValue": "lblStatusValue",
        "lblSeparatorLineActions": "lblSeparatorLineActions",
        "flxRowOne": "flxRowOne",
        "flxIdentifier": "flxIdentifier",
        "flxRow": "flxRow",
        "flxDetail": "flxDetail",
        "flxSentTo": "flxSentTo",
        "flxAmount": "flxAmount",
        "flxNoteTitle": "flxNoteTitle",
        "flxStatusTitle": "flxStatusTitle",
        "flxReferenceNumberTitle": "flxReferenceNumberTitle",
        "flxDate": "flxDate",
        "flxFromAccountTitle": "flxFromAccountTitle",
        "flxCurrency": "flxCurrency",
        "lblCurrencyValue": "lblCurrencyValue",
        "flxTransactionFee": "flxTransactionFee",
        "lblTransactionFeeValue": "lblTransactionFeeValue",
        "lblCurrencyTitle": "lblCurrencyTitle",
        "lblTransactionFeeTitle": "lblTransactionFeeTitle"
      };
      this.view.transfermain.segmentTransfers.widgetDataMap = recentsDataMap;
      if (break_point == 640) {
        for (var i = 0; i < this.transfersViewModel.transactionsData.length; i++) {
          this.transfersViewModel.transactionsData[i].template = "flxRecentTransfersMobile";
        }
        this.view.transfermain.flxSort.setVisibility(false);
      }
      this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
      this.view.flxAddAccountWindow.setVisibility(true);
      FormControllerUtility.setSortingHandlers(this.recentSortMap, this.onRecentSortClickHandler, this);
      commonUtilities.Sorting.updateSortFlex(this.recentSortMap, config);
      this.AdjustScreen();
    },
    /**Show Empty View
     * @param {String} context Value which is not present
     */
    showNoTransactions: function (context) {
      this.hideAll();
      var self = this;
      this.view.transfermain.flxNoTransactions.setVisibility(true);
      if (context === kony.i18n.getLocalizedString("i18n.transfers.externalAccount")) {
        this.view.transfermain.lblScheduleAPayment.setVisibility(false);
        this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.Transfer.noExternalAccount');
      } else {
        this.view.transfermain.lblScheduleAPayment.setVisibility(true);
        this.view.transfermain.lblScheduleAPayment.onTouchEnd = function () {
          applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
        };
        this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.noTransactions');
      }
      this.view.forceLayout();
    },
    /**Configure Sort Flex
     * @param  {string} tab Type of tab and shows sort flex 
     */
    sortFlex: function (tab) {
      if (tab === kony.i18n.getLocalizedString("i18n.transfers.externalAccount")) {
        this.view.transfermain.flxSortExternal.setVisibility(true);
        this.view.transfermain.flxSort.setVisibility(false);
        this.view.transfermain.lblSortDateExternal.text = kony.i18n.getLocalizedString("i18n.transfers.accountName");
        this.view.transfermain.lblSortDescriptionExternal.text = kony.i18n.getLocalizedString("i18n.transfers.bankName");
        this.view.transfermain.lblSortTypeExternal.text = kony.i18n.getLocalizedString("i18n.billPay.Status");
      } else if (tab === kony.i18n.getLocalizedString("i18n.transfers.recent")) {
        this.view.transfermain.flxSortExternal.setVisibility(false);
        this.view.transfermain.flxSort.setVisibility(true);
        this.view.transfermain.lblSortDate.text = kony.i18n.getLocalizedString("i18n.transfers.transactionDate");
        this.view.transfermain.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.billPay.sendTo");
        this.view.transfermain.lblSortType.text = kony.i18n.getLocalizedString("i18n.transfers.amountlabel");
      }
    },
    /**Shows Segment for Tabs
     */
    showSegment: function () {
      this.hideAll();
      this.view.transfermain.flxSort.setVisibility(true);
      this.view.transfermain.segmentTransfers.setVisibility(true);
      this.view.transfermain.tablePagination.setVisibility(true);
      this.view.forceLayout();
    },
    /**Create View Model For Transactions for a segment
   * @param  {Array} data Array of transactions model 
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   */
    getNewTransfersData: function (data, onCancelCreateTransfer) {
      this.transfersViewModel.transactionsData = data.map(this.createNewTransfersData.bind(this, onCancelCreateTransfer))
    },
    /**Configure Paginations for Recent Transfers
   * @param {object} config configuration to show pagination
   */
    setRecentTransactionsPagination: function (config) {
      this.view.transfermain.tablePagination.flxPagination.setVisibility(true);
      offset = config.offset;
      this.setPaginationPreviousRecent(config);
      this.setPaginationNextRecent();
      this.view.transfermain.tablePagination.lblPagination.text = (config.offset + 1) + '-' + (config.offset + this.transfersViewModel.transactionsData.length) + ' ' + kony.i18n.getLocalizedString("i18n.common.transactions");
      if (this.transfersViewModel.transactionsData.length < ViewConstants.MAGIC_NUMBERS.LIMIT) {
        this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () { };
        this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
      }
    },
    /**Calls Repeat Transaction Method forming a transaction object
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   */
    repeatTransaction: function (onCancelCreateTransfer) {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var selectedData = data[index];
      var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      var transactionObject = new transactionsModel({
        "transactionId": selectedData.lblReferenceNumberValue
      });
      transactionObject.amount = applicationManager.getFormatUtilManager().deFormatAmount((selectedData.lblAmount).replace(applicationManager.getConfigurationManager().getCurrencyCode(), ""));
      transactionObject.scheduledDate = selectedData.lblDate;
      transactionObject.fromAccountNumber = selectedData.fromAccountNumber;
      transactionObject.notes = selectedData.lblNoteValue;
      transactionObject.numberOfRecurrences = selectedData.lblRecurrenceValue;
      transactionObject.toAccountNumber = selectedData.toAccountNumber;
      transactionObject.toAccountName = selectedData.lblSendTo;
      transactionObject.fromAccountName = selectedData.lblFromAccountValue;
      transactionObject.transactionType = selectedData.transactionType;
      transactionObject.frequencyType = selectedData.frequencyType;
      transactionObject.ExternalAccountNumber = selectedData.externalAccountNumber;
      transactionObject.IBAN = selectedData.IBAN;
      transactionObject.transactionCurrency = selectedData.transactionCurrency;
      transactionObject.toAccountName = selectedData.toAccountName;
      transactionObject.isInternationalAccount = selectedData.isInternationalAccount;
      transactionObject.swiftCode = selectedData.swiftCode;
      transactionObject.bankName = selectedData.bankName;
      if(selectedData.externalAccountNumber && selectedData.transactionCurrency && (selectedData.isInternationalAccount === "true" || selectedData.isInternationalAccount === null))
      {
        transactionObject.amount = applicationManager.getFormatUtilManager().deFormatAmount((selectedData.lblAmount).replace(applicationManager.getConfigurationManager().currencyCode[selectedData.transactionCurrency] , ""));
      }
      applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
        transactionObject: transactionObject,
        onCancelCreateTransfer: onCancelCreateTransfer
      })
      this.AdjustScreen();
      this.view.customheader.setFocus(true);
    },
    /**function to View Report
   */
    viewTransactionReport: function () {
      this.view.viewReport.lblNote.text =kony.i18n.getLocalizedString("i18n.BillPay.TermsAndConditionsPoint1")+kony.i18n.getLocalizedString("i18n.BillPay.TermsAndConditionsPoint2");
      this.view.flxTransferViewReport.setVisibility(true);
      this.view.viewReport.lblHeader.setFocus(true);
      this.view.viewReport.btnClose.onClick = function () {
        this.view.flxTransferViewReport.setVisibility(false);
      }.bind(this);
      this.view.flxTransferViewReport.height = this.getPageHeight();
      this.AdjustScreen();
    },
    /**Shows Make Transfer View For Editing a  Trancaction
   * @param  {object} transaction Transaction Model
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   */
    editScheduledTransaction: function (transaction, onCancelCreateTransfer) {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var selectedData = data[index];
      var record = {
        "amount": transaction.amount,
        "frequencyEndDate": transaction.frequencyEndDate,
        "frequencyStartDate": selectedData.frequencyStartDate,
        "frequencyType": selectedData.frequencyType,
        "fromAccountNumber": selectedData.fromAccountNumber,
        "isScheduled": "1",
        "numberOfRecurrences": selectedData.lblRecurrenceValue,
        "scheduledDate": transaction.scheduledDate,
        "toAccountNumber": selectedData.toAccountNumber,
        "transactionDate": selectedData.lblDate,
        "ExternalAccountNumber": selectedData.externalAccountNumber,
        "transactionId": selectedData.lblReferenceNumberValue,
        "transactionsNotes": selectedData.lblNoteValue,
        "transactionType": selectedData.transactionType,
        "category": selectedData.category
        };
        record.IBAN = selectedData.IBAN;
        record.transactionCurrency = selectedData.transactionCurrency;
        record.toAccountName = selectedData.beneficiaryName;
        record.isInternationalAccount = selectedData.isInternationalAccount;
        record.swiftCode = selectedData.swiftCode;
        record.bankName = selectedData.bankName;
        record.toAccountName = selectedData.toAccountName;

      applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
        editTransactionObject: record,
        onCancelCreateTransfer: onCancelCreateTransfer
      });
      this.AdjustScreen();
      this.view.customheader.setFocus(true);
    },
    /** Handler for cancel occurrence button
   * @param {object} transaction Details of the transaction
   */
    onCancelOccurrence: function (transaction) {
      var scopeObj = this;
      this.view.deletePopup.setVisibility(true);
      scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.cancelOccurrenceMessage");
      scopeObj.view.flxDelete.height = this.getPageHeight();
      scopeObj.view.flxDelete.left = "0%";
      scopeObj.view.deletePopup.lblHeading.setFocus(true);
      this.view.deletePopup.btnYes.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
        FormControllerUtility.showProgressBar(scopeObj.view);
        applicationManager.getModulesPresentationController("TransferModule").cancelTransactionOccurrence(transaction);
      };
      this.view.deletePopup.btnNo.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      };
      this.view.deletePopup.flxCross.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      };
      this.AdjustScreen();
    },
    /**Formats the Currency
   * @param  {Array} amount Array of transactions model 
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   * @param {String} currencySymbol currencySymbol
   */
    formatCurrency: function (amount, currencySymbolNotRequired, currencySymbol) {
      return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired, currencySymbol);
    },

    /**Executes on delete transaction button
  * @returns {void} None
  * @throws {void} None
  */
    deleteTransaction: function (transactionObject) {
      this.view.deletePopup.setVisibility(true);
      var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      var transObject = new transactionsModel({
        "transactionId": transactionObject.transactionId,
        "transactionType": transactionObject.transactionType
      });
      var scopeObj = this;
      scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
      scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
      scopeObj.view.flxDelete.height = this.getPageHeight();
      scopeObj.view.flxDelete.left = "0%";
      this.view.deletePopup.btnYes.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
        applicationManager.getModulesPresentationController("TransferModule").deleteTransfer(transObject);
      }
      this.view.deletePopup.btnNo.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      }
      this.view.deletePopup.flxCross.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      }
      this.AdjustScreen();
    },


    /**Create View Model For Transactions for a segment
   * @param  {Array} data Array of transactions model 
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   */
    createNewTransfersData: function (onCancelCreateTransfer, transaction) {
      var scopeObj = this;
      if (transaction.transactionsNotes === undefined || transaction.transactionsNotes === null) { transaction.transactionsNotes = kony.i18n.getLocalizedString("i18n.common.none"); }
      var dataObject = {
        btnAction: {
          text: kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
          onClick: this.viewTransactionReport.bind(this)
        },
        btnRecentRepeat: {
          text: kony.i18n.getLocalizedString("i18n.accounts.repeat"),
          toolTip: kony.i18n.getLocalizedString("i18n.accounts.repeat"),
          onClick: this.repeatTransaction.bind(this, onCancelCreateTransfer)
        },
        btnScheduledRepeat: {
          text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
          toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
          onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer)
        },
        btnScheduledDelete: {
          text: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
          toolTip: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
          onClick: commonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : this.deleteTransaction.bind(this, transaction)
        },
        btnCancelScheduledOccurrence: {
          isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true,
          text: kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
          toolTip: kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
          onClick: commonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : this.onCancelOccurrence.bind(this, transaction)
        },
        "flxDropdown": "flxDropdown",
        "imgDropdown": {
          "src": ViewConstants.IMAGES.ARROW_DOWN,
          "accessibilityconfig": {
            "a11yHidden": false,
            "a11yLabel": "View Transaction Details"
          }
        },
        "recentTemplate": "flxRecentTransfers",
        "scheduledTemplate": "flxScheduledTransfers",
        "lblFromAccountTitle": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
        "lblRecurrenceTitle": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
        "lblReferenceNumberTitle": kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
        "lblSeparator": "lblSeparator",
        "lblStatusTitle": kony.i18n.getLocalizedString("i18n.billPay.Status"),
        "lblNoteTitle": kony.i18n.getLocalizedString("i18n.transfers.Description"),
        "category": transaction.category,
        "frequencyType": transaction.frequencyType,
        "transactionType": transaction.transactionType,
        "fromAccountNumber": transaction.fromAccountNumber,
        "toAccountNumber": transaction.toAccountNumber,
        "lblAmount": scopeObj.formatCurrency(transaction.amount, false, transaction.transactionCurrency),
        "externalAccountNumber": transaction.ExternalAccountNumber,
        "lblSendTo": transaction.toAccountName,
        "lblLatestScheduledTransaction": this.getDateFromDateStr(transaction.transactionDate),
        "newRecurrenceValue": (transaction.recurrenceDesc || kony.i18n.getLocalizedString("i18n.common.none")),
        "lblDate": transaction.isScheduled === "true" ? this.getDateFromDateStr(transaction.scheduledDate) : this.getDateFromDateStr(transaction.transactionDate),
        "scheduledDate": this.getDateFromDateStr(transaction.transactionDate),
        "lblStatusValue": transaction.statusDescription,
        "lblReferenceNumberValue": transaction.transactionId,
        "lblRecurrenceValue": transaction.numberOfRecurrences,
        "recurrenceDescription": transaction.recurrenceDesc ? transaction.recurrenceDesc : "-",
        "lblFromAccountValue": transaction.fromAccountName,
        "lblNoteValue": transaction.transactionsNotes,
        "IBAN": transaction.IBAN,
        "transactionCurrency": transaction.transactionCurrency,
        "isInternationalAccount": transaction.isInternationalAccount,
        "toAccountName": transaction.toAccountName,
        "swiftCode": transaction.swiftCode,
        "bankName": transaction.bankName,
        "flxIdentifier": "flxIdentifier",
        "lblIdentifier": "lblIdentifier",
        "lblSeparatorLineActions": "lblSeparatorLineActions",
        "flxRowOne": "flxRowOne",
        "lblRowSeperator": "lblRowSeperator",
        "lblFrequencyTitle": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
        "flxCurrency": {
          "isVisible": transaction.transactionCurrency ? true : false
        },
        "lblCurrencyTitle": kony.i18n.getLocalizedString("i18n.common.Currency"),
        "lblTransactionFeeTitle": kony.i18n.getLocalizedString("i18n.TransfersEur.TransactionFee"),
        "lblCurrencyValue": {
          "text": transaction.transactionCurrency
        },
        "flxTransactionFee": {
          "isVisible": transaction.fee ? true : false
        },
        "lblTransactionFeeValue": {
          "text": transaction.fee ? scopeObj.formatCurrency(transaction.fee) : transaction.fee
        }
      };
      if (commonUtilities.isCSRMode()) {
        dataObject.btnScheduledDelete.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(13);
      }
      return dataObject;
    },
    /**Get Front End Date String
   * @param  {string} dateStr Date String from backend
   */
    getDateFromDateStr: function (dateStr) {
      if (dateStr) {
        return commonUtilities.getFrontendDateString(dateStr);
      }
      else {
        return "";
      }
    },
    /** On Recent Transactions Sort click handler.
     * @param  {object} event object
     * @param  {object} data New Sorting Data
     */
    onRecentSortClickHandler: function (event, data) {
      var scopeObj = this;
      scopeObj.first = 0;
      applicationManager.getModulesPresentationController("TransferModule").showRecentUserTransactions(data);
    },
    /**Entry Point Method of Scheduled Tab
   */
    getUserScheduledTransactions: function () {
      this.first = 0;
      this.last = 10;
      applicationManager.getModulesPresentationController("TransferModule").getScheduledUserTransactions();
    },
    /**Show Scheduled Tab Data  - Entry Point Method After WillUpdateUI
   * @param  {Array} data Array of transactions model 
   * @param  {config} Sorting Configuration for sorting
   */
    showScheduledData: function (data, config) {
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.scheduled")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.scheduled");
      this.setChangeTransferTypeView(this.getValidTransferTypes(), {
        headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType")
      });
      if ((data === undefined) || ((data instanceof Array) && data.length === 0)) { this.showNoTransactions(); }
      else { this.showSegment(); }
      this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.recent"));
      this.view.transfermain.btnScheduled.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () { };
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () { };
      //this.view.transfermain.flxRowSeperator.setVisibility(true);
      this.getNewTransfersData(data, this.getUserScheduledTransactions);
      this.setScheduledTransactionsPagination(config);
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(true);
      this.setSearchFlexVisibility(false);
      var break_point = kony.application.getCurrentBreakpoint();
      var scheduledDataMap = {
        "btnRepeat": "btnScheduledDelete",
        "btnAction": "btnScheduledRepeat",
        "btnCancelThisOccurence": "btnCancelScheduledOccurrence",
        "flxDropdown": "flxDropdown",
        "imgDropdown": "imgDropdown",
        "lblAmount": "lblAmount",
        "template": "scheduledTemplate",
        "lblDate": "lblDate",
        "lblFromAccountTitle": "lblFromAccountTitle",
        "lblFromAccountValue": "lblFromAccountValue",
        "lblIdentifier": "lblIdentifier",
        "lblNotesTitle": "lblNoteTitle",
        "lblNotesValue": "lblNoteValue",
        "lblFrequencyTitle": "lblFrequencyTitle",
        "lblFrequencyValue": "frequencyType",
        "lblRecurrenceTitle": "lblRecurrenceTitle",
        "lblRecurrenceValue": "recurrenceDescription",
        "lblReferenceNumberTitle": "lblReferenceNumberTitle",
        "lblReferenceNumberValue": "lblReferenceNumberValue",
        "lblSendTo": "lblSendTo",
        "lblSeparator": "lblSeparator",
        "lblStatusTitle": "lblStatusTitle",
        "lblStatusValue": "lblStatusValue",
        "lblSeparatorLineActions": "lblSeparatorLineActions",
        "flxRowOne": "flxRowOne",
        "flxIdentifier": "flxIdentifier",
        "flxRow": "flxRow",
        "flxDetail": "flxDetail",
        "flxAmount": "flxAmount",
        "flxDate": "flxDate",
        "flxNoteTitle": "flxNoteTitle",
        "flxReferenceNumberTitle": "flxReferenceNumberTitle",
        "flxRecurrenceTitle": "flxRecurrenceTitle",
        "lblRowSeperator": "lblRowSeperator",
        "flxStatusTitle": "flxStatusTitle",
        "flxCurrency": "flxCurrency",
        "lblCurrencyValue": "lblCurrencyValue",
        "flxTransactionFee": "flxTransactionFee",
        "lblTransactionFeeValue": "lblTransactionFeeValue",
        "lblCurrencyTitle": "lblCurrencyTitle",
        "lblTransactionFeeTitle": "lblTransactionFeeTitle"
      };
      this.view.transfermain.segmentTransfers.widgetDataMap = scheduledDataMap;
      if (break_point == 640) {
        for (var i = 0; i < this.transfersViewModel.transactionsData.length; i++) {
          this.transfersViewModel.transactionsData[i].template = "flxRecentTransfersMobile";
        }
        this.view.transfermain.flxSort.setVisibility(false);
      }
      this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
      this.view.flxAddAccountWindow.setVisibility(true);
      FormControllerUtility.setSortingHandlers(this.scheduledSortMap, this.onScheduledSortClickHandler, this);
      commonUtilities.Sorting.updateSortFlex(this.scheduledSortMap, config);
      this.view.forceLayout();
    },
    /** On Scheduled Transactions Sort click handler.
     * @param  {object} event object
     * @param  {object} data New Sorting Data
     */
    onScheduledSortClickHandler: function (event, data) {
      var scopeObj = this;
      scopeObj.first = 0;
      applicationManager.getModulesPresentationController("TransferModule").getScheduledUserTransactions(data);
    },
    /**Show Server error in the UI 
   * @param  {object} viewModel ViewModel containing server error message
   */
    showServerError: function (viewModel) {
      var scopeObj = this;
      scopeObj.view.flxMakeTransferError.setVisibility(true);
      scopeObj.view.rtxMakeTransferError.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
    },
    /**Callback for Make Transfer Button
     */
    onBtnMakeTransfer: function () {
      var self = this;
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data[index];
      var onCancelCreateTransfer = function () {
        self.setSearchFlexVisibility(false);
        applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts();
      };
      var obj = {};
      obj.repeatTransactionObject = {};
      if(data.isInternationalAccount === "true") {
        obj.repeatTransactionObject.ExternalAccountNumber = data.txtAccountNumber.text;
        obj.repeatTransactionObject.type = OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT;
        obj.repeatTransactionObject.swiftCode = data.lblRoutingNumberValue;
      } else {
        obj.repeatTransactionObject.IBAN = data.txtAccountNumber.text;
        obj.repeatTransactionObject.type = OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT;
      }
      obj.repeatTransactionObject.bankName = data.lblBankName;
      obj.repeatTransactionObject.toAccountName = data.lblAccountName;
      obj.repeatTransactionObject.transactionType = applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER);
      applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({ "accountTo": obj, onCancelCreateTransfer: onCancelCreateTransfer });
      this.AdjustScreen();
    },
    /**Call back for Verify Account Button
     */
    onBtnVerifyAccount: function () {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      applicationManager.getModulesPresentationController("TransferModule").showVerifyAccounts(data[index]);
      this.AdjustScreen();
    },
    /**UI entrypoint for external accounts
     * @param  {object} viewModel Data of External accounts
     * @param  {object} config Configuration for the Pagination
     */
    showExternalAccounts: function (viewModel, config, pagination) {
      this.setChangeTransferTypeView(this.getValidTransferTypes(), { headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType") });
      if (viewModel === "errorExternalAccounts") {
        this.hideAll();
        this.showServerError();
        this.view.transfermain.flxNoTransactions.setVisibility(true);
        this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.transfers.errorExternalAccounts");
        this.view.transfermain.lblScheduleAPayment.isVisible = false;
        this.view.forceLayout();
      } else if ((viewModel === undefined) || ((viewModel instanceof Array) && viewModel.length === 0)) {
        this.showNoTransactions(kony.i18n.getLocalizedString("i18n.transfers.externalAccount"));
      }
      else {
        var scopeObj = this;
        this.view.flxMakeTransferError.setVisibility(false);
        this.view.flxTrasfersWindow.isVisible = true;
        this.view.transferactivity.isVisible = false;

        this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.externalAccount"));
        this.showSegment();
        //this.view.transfermain.flxRowSeperator.setVisibility(true);
        this.view.transfermain.btnExternalAccounts.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
        this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
        }, {
          text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts")
        }]);
        this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
        this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");

        this.view.transfermain.lblSortDate.text = kony.i18n.getLocalizedString("i18n.transfers.accountName");
        this.view.transfermain.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.transfers.bankName");
        this.view.transfermain.lblSortType.text = kony.i18n.getLocalizedString("i18n.transfers.amountlabel");
        this.view.transfermain.flxSortExternal.setVisibility(true);
        this.view.transfermain.flxSort.setVisibility(false);
        this.view.forceLayout();
        var break_point = kony.application.getCurrentBreakpoint();
        var externalAccountDataMap = {
          "btnBankDetails": "btnBankDetails",
          "btnDelete": "btnDelete",
          "btnEdit": "btnEdit",
          "btnViewActivity": "btnViewActivity",
          "btnAction": "btnAction",
          "btnCancel": "btnCancel",
          "btnMakeTransfer": "btnMakeTransfer",
          "btnSave": "btnSave",
          "flxDropdown": "flxDropdown",
          "flxAccountTypeTitle" :"flxAccountTypeTitle",
          "flxAccountNumberTitle":"flxAccountNumberTitle",
          "imgDropdown": "imgDropdown",
          "lblAccountHolderTitle": "lblAccountHolderTitle",
          "lblAccountHolderValue": "lblAccountHolderValue",
          "lblAccountName": "lblAccountName",
          "lblAccountNumberTitle": "lblAccountNumberTitle",
          "lblAccountNumberValue": "lblAccountNumberValue",
          "lblAccountTypeTitle": "lblAccountTypeTitle",
          "lblAccountTypeValue": "lblAccountTypeValue",
          "lblAddedOnTitle": "lblAddedOnTitle",
          "lblAddedOnValue": "lblAddedOnValue",
          "lblBankDetailsTitle": "lblBankDetailsTitle",
          "lblBankName": "lblBankName",
          "lblIdentifier": "lblIdentifier",
          "lblRoutingNumberTitle": "lblRoutingNumberTitle",
          "lblRoutingNumberValue": "lblRoutingNumberValue",
          "lblSeparator": "lblSeparator",
          "lblSeparatorActions": "lblSeparatorActions",
          "lblStatus": "lblStatus",
          "txtAccountName": "txtAccountName",
          "txtAccountNumber": "txtAccountNumber",
          "txtAccountType": "txtAccountType",
          "lblSeparatorLineActions": "lblSeparatorLineActions",
          "lblSeparatorLineActions1": "lblSeparatorLineActions1",
          "lblSeparatorLineActions2": "lblSeparatorLineActions2",
          "lblRowSeperator": "lblRowSeperator"
        };
        var len = viewModel.length;
        function getMappings(context) {
          if (context.routingNumberDetails) {
            context = context.routingNumberDetails;
            if (context.lblRoutingNumberTitle) {
              if (context.lblRoutingNumberTitle.isInternationalAccount === "true") {
                return kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
              } else {
                return {
                  "text": kony.i18n.getLocalizedString("i18n.accounts.routingNumber"),
                  "isVisible": false
                };
              }
            } if (context.lblRoutingNumberValue) {
              if (context.lblRoutingNumberValue.isInternationalAccount === "true") {
                return context.lblRoutingNumberValue.swiftCode;
              } else {
                return {
                  "text": context.lblRoutingNumberValue.routingNumber,
                  "isVisible": false
                };
              }
            }
          }
          if (context.accountStatus) {
            if (context.accountStatus.isVerified === "true" || context.accountStatus.isVerified === "1") {
              return kony.i18n.getLocalizedString("i18n.transfers.verified");
            }
            else {
              return kony.i18n.getLocalizedString("i18n.accounts.pending");
            }
          }
          if (context.viewActivity) {
            if (applicationManager.getConfigurationManager().getConfigurationValue("fundTransferHistory") === 'true' && context.viewActivity.isVerified === "true") {
              return {
                "text": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                "onClick": function () {
                  scopeObj.onBtnViewActivity();
                }
              }
            } else {
              return {
                "isVisible": false
              }
            }
          }
          if (context.btnActivityAccountStatus) {
            if (context.btnActivityAccountStatus.isVerified === "true" || context.btnActivityAccountStatus.isVerified === "1") {
              return {
                "text": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                "toolTip": kony.i18n.getLocalizedString("i18n.common.transferToThatPayee"),

                "onClick": function () {
                  scopeObj.onBtnMakeTransfer();
                }
              };
            }
            else {
              return {
                "text": kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),
                "toolTip": kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),

                "onClick": function () {
                  scopeObj.onBtnVerifyAccount();
                }
              };
            }
          }
          if (context.congigkeyPlug) {
            if (applicationManager.getConfigurationManager().getConfigurationValue('addExternalAccount') === 'true') {
              return true;
            } else if (applicationManager.getConfigurationManager().getConfigurationValue('addExternalAccount') === 'false' && context.congigkeyPlug.isVerified === "true") {
              return true;
            } else {
              return false;
            }
          }
        }
        this.setExternalAccountsPagination(viewModel, pagination);          //new lOC                

        var data = [];
        var i = 0;
        for (i = 0; i < len; i++) {
          if (viewModel[i] !== undefined && getMappings({ "congigkeyPlug": viewModel[i] })) {
            var accName = viewModel[i].nickName;
            var dataObject = {
              "isInternationalAccount": viewModel[i].isInternationalAccount,
              "btnDelete": {
                "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                "toolTip": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                "onClick": commonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : function () {
                  scopeObj.onExternalAccountDelete();
                }
              },
              "btnEdit": {
                "toolTip": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                "onClick": scopeObj.externalAccountsSegmentRowClickEdit.bind(scopeObj, accName)
              },
              "btnViewActivity": getMappings({ "viewActivity": viewModel[i] }),
              "btnAction": getMappings({ "btnActivityAccountStatus": viewModel[i] }),
              "btnCancel": {
                "text": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
                "toolTip": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
                "onClick": function () {
                  scopeObj.showUnselectedRow();
                }
              },
              "btnSave": {
                "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.Save'),
                "toolTip": kony.i18n.getLocalizedString('i18n.common.saveChanges'),
                "onClick": commonUtilities.isCSRMode() ? FormControllerUtility.disableButtonActionForCSRMode() : scopeObj.saveChangedExternalAccount.bind(scopeObj, accName)
              },
              "imgDropdown": {
                "src": ViewConstants.IMAGES.ARROW_DOWN,
                "accessibilityconfig": {
                  "a11yHidden": false,
                  "a11yLabel": "View Transaction Details"
                }
              },
              "flxAccountTypeTitle" : {
                "isVisible":false,
              },
              "flxAccountNumberTitle":{
                "width": "57%",
              },
              "lblAccountHolderTitle": kony.i18n.getLocalizedString("i18n.WireTransfer.AccountHolder"),
              "lblAccountHolderValue": kony.i18n.getLocalizedString('i18n.common.individualAccount'),
              "lblAccountName": viewModel[i].nickName,
              "lblAccountNumberTitle": kony.i18n.getLocalizedString('i18n.common.accountNumber'),
              "lblAccountNumberValue": {
                "text" : viewModel[i].accountNumber,
                "textCopyable" :true
              },
              "lblAccountTypeTitle": " ",
              "lblAccountTypeValue": " ",
              "lblAddedOnTitle": kony.i18n.getLocalizedString('i18n.common.addedOn'),
              "lblAddedOnValue": commonUtilities.getFrontendDateString(viewModel[i].createdOn),
              "lblBankDetailsTitle": kony.i18n.getLocalizedString('i18n.transfers.bankDetails'),
              "lblBankName": viewModel[i].bankName,
              "lblIdentifier": "lblIdentifier",
              "lblRoutingNumberTitle": getMappings({ routingNumberDetails: { lblRoutingNumberTitle: viewModel[i] } }),    //new lOC
              "lblRoutingNumberValue": getMappings({ routingNumberDetails: { lblRoutingNumberValue: viewModel[i] } }) || "NA",    //new lOC
              "lblSeparator": viewModel[i].beneficiaryName,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblStatus": getMappings({ accountStatus: viewModel[i] }),
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": viewModel[i].nickName,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": viewModel[i].accountNumber,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": viewModel[i].accountType,
                "placeholder": "",
                "isVisible": false
              }
            };

            if (viewModel[i].isInternationalAccount === "false") {
              dataObject.lblAccountNumberTitle = kony.i18n.getLocalizedString('i18n.Accounts.IBAN');
              dataObject.lblAccountNumberValue = applicationManager.getFormatUtilManager().formatIBAN(viewModel[i].IBAN);
              dataObject.txtAccountNumber = {
                "text": viewModel[i].IBAN
              };
            }

            if (commonUtilities.isCSRMode()) {
              dataObject.btnDelete.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(13);
              dataObject.btnSave.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            }
            data.push(dataObject);
          }
        }
        this.view.transfermain.segmentTransfers.widgetDataMap = externalAccountDataMap;
        if (break_point == 640) {
          for (i = 0; i < data.length; i++) {
            data[i].template = "flxExternalAccountUnselected";
          }
          this.view.transfermain.flxSortExternal.setVisibility(false);

          //this.view.transfermain.flxSearchSortSeparator.setVisibility(true);    
        }
        this.view.transfermain.flxSearch.setVisibility(true);
        this.view.transfermain.segmentTransfers.setData(data);
        this.view.flxAddAccountWindow.setVisibility(true);
        FormControllerUtility.updateSortFlex(this.externalAccountsSortMap, config);
      }
    },

    /** On Search is complete show external accounts
     * @member  frmTransfersController
     * @param  {array} viewModel Array of recipients
     */
    showSearchTransferPayees: function (viewModel) {
      var scopeObj = this;
      scopeObj.view.transfermain.tablePagination.flxPagination.setVisibility(false);
      if (viewModel.error) {
        scopeObj.showExternalAccounts("errorExternalAccounts");
        return;
      }
      if (viewModel.externalAccounts.length === 0) {
        scopeObj.view.transfermain.flxSortExternal.setVisibility(false);
        scopeObj.view.transfermain.segmentTransfers.setVisibility(false);
        scopeObj.view.transfermain.flxNoTransactions.setVisibility(true);
        scopeObj.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.searchNoPayees');
        scopeObj.view.transfermain.lblScheduleAPayment.setVisibility(false);
        scopeObj.view.forceLayout();
        return;
      }
      scopeObj.showExternalAccounts(viewModel.externalAccounts, viewModel.searchInputs, {});
    },

    /**Callbacks for External Account Delete Button
     */
    onExternalAccountDelete: function () {
      var scopeObj = this;
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccountMsg");
      scopeObj.view.flxLogout.height = this.getPageHeight();
      scopeObj.view.flxLogout.left = "0%";
      scopeObj.view.CustomPopup.lblHeading.setFocus(true);

      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var accountNumber = data[index].txtAccountNumber.text;
      this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString('i18n.common.deleteTheAccount');
      this.view.CustomPopup.btnYes.onClick = function () {
        //function
        var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        var param = {};
        if (data[index].isInternationalAccount === "true") {
          param.accountNumber = accountNumber;
        } else {
          param.IBAN = accountNumber;
        }
        transfersModule.presentationController.deleteExternalAccount(param);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.noDontDelete');
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.AdjustScreen();
    },
    /**Callback for Edit Button on external accounts
     */
    externalAccountsSegmentRowClickEdit: function (accName) {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data[index];
      data.txtAccountName.text = accName;
      data.txtAccountName.skin = "sknTxtSSP15pxBorder727272Op201px"
      var break_point = kony.application.getCurrentBreakpoint();
      if (break_point == 640) {
        if (data.template === "flxExternalAccountSelected") {
          data.imgDropdown = "chevron_up.png";
          data.template = "flxExternalAccountsEdit";
        } else {
          data.imgDropdown = "arrow_down.png";
          data.template = "flxExternalAccountSelected";
        }
      }
      else {
        if (data.template === "flxExternalAccountsTransfersSelected") {
          data.imgDropdown = ViewConstants.IMAGES.CHEVRON_UP;
          data.template = "flxExternalAccountsTransfersEdit";
        } else {
          data.imgDropdown = ViewConstants.IMAGES.ARROW_DOWN;
          data.template = "flxExternalAccountsTransfersSelected";
        }
      }
      this.view.transfermain.segmentTransfers.setDataAt(data, index);
      this.AdjustScreen(150);
    },
    /**Shows the unselected row of segment
     */
    showUnselectedRow: function () {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      this.showSelectedRow(index);
    },
    /**Saves the changes of inline edit of external account
     */
    saveChangedExternalAccount: function (accountName) {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data[index];
      var params;
      if (data.txtAccountName.text === null || data.txtAccountName.text.trim().toLowerCase() === "null" || data.txtAccountName.text === NaN || data.txtAccountName.text === undefined || data.txtAccountName.text.trim() === "" || data.txtAccountName.text.trim().toLowerCase() === "undefined" || data.txtAccountName.text.trim().toLowerCase() === "nan" || data.txtAccountName.text.trim().length > 15) {
        data.txtAccountName.skin = "skntxtSSP424242BorderFF0000Op100Radius2px";
        data.txtAccountName.text = accountName;
        this.view.transfermain.segmentTransfers.setDataAt(data, index);
      }
      else {
        params = {
          "nickName": data.txtAccountName.text,
          "isVerified": 1
        };
        if (data.isInternationalAccount === "true") {
          params.accountNumber = data.txtAccountNumber.text;
        } else {
          params.IBAN = data.txtAccountNumber.text;
        }
        applicationManager.getModulesPresentationController("TransferModule").saveChangedExternalAccount(params, data);
        this.AdjustScreen();
      }
    },
    /**Show Selected Row of the external account
     * @param  {number} index Index of Row Needs to be expanded 
     */
    showSelectedRow: function (index) {
      var data = this.view.transfermain.segmentTransfers.data;
      for (var i = 0; i < data.length; i++) {
        if (i === index) {
          data[i].imgDropdown = ViewConstants.IMAGES.CHEVRON_UP;
          data[i].template = "flxExternalAccountsTransfersSelected";
        }
        else {
          data[i].imgDropdown = ViewConstants.IMAGES.ARROW_DOWN;
          data[i].template = "flxExternalAccountsTransfersUnselected";
        }
      }
      this.view.transfermain.segmentTransfers.setData(data);
    },
    /**Configure pagination for previous button for recent transfers
   * @param {object} config pagination values
   */
    setPaginationPreviousRecent: function (config) {
      var self = this;
      if (offset <= 0) {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () { };
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      } else {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () {
          self.getPreviousRecentTransactions(config);
        }
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      }
    },
    /**Configure Pagination for Next Button Of Recent Transfers
   */
    setPaginationNextRecent: function () {
      var self = this;
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () {
        self.getNextRecentTransactions();
      }
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
    },
    /**Called when previos button is triggered from pagination
   * @returns {object} config configuration values for pagination
   */
    getPreviousRecentTransactions: function (config) {
      if (offset >= ViewConstants.MAGIC_NUMBERS.LIMIT) {
        applicationManager.getModulesPresentationController("TransferModule").fetchPreviousRecentUserTransactions();
      }
      else {
        this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      }
    },

    /**Called when Pagination is triggered for next Recent Tranctions
   */

    getNextRecentTransactions: function () {
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transfersModule.presentationController.fetchNextRecentUserTransactions();
    },
    /**Configure Pagination for Previous Button for Scheduled Transactions
   */
    setPaginationPreviousScheduled: function () {
      var self = this;
      if (offset <= 0) {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () { };
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      } else {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () {
          self.getPreviousScheduledTransactions();
        }
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      }
    },
    /**Configure Pagination for Next Button of Scheduled Transactions
   */
    setPaginationNextScheduled: function () {
      var self = this;
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () {
        self.getNextScheduledTransactions();
      }
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
    },
    /**Configure Pagination for Scheduled Transactions
   */
    setScheduledTransactionsPagination: function (config) {
      this.view.transfermain.tablePagination.flxPagination.setVisibility(true);
      offset = config.offset;
      this.setPaginationPreviousScheduled();
      this.setPaginationNextScheduled();
      this.view.transfermain.tablePagination.lblPagination.text = (config.offset + 1) + '-' + (config.offset + this.transfersViewModel.transactionsData.length) + ' ' + kony.i18n.getLocalizedString("i18n.common.transactions");
      if (this.transfersViewModel.transactionsData.length < ViewConstants.MAGIC_NUMBERS.LIMIT) {
        this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () { };
        this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
      }
    },
    /**Called when Next Button is triggered from pagination from scheduled tab
   */
    getNextScheduledTransactions: function () {
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      applicationManager.getModulesPresentationController("TransferModule").fetchNextScheduledUserTransactions();
    },
    /**Called when Previous Button is called from Scheduled Transaction Pagination
   */
    getPreviousScheduledTransactions: function () {
      if (offset >= ViewConstants.MAGIC_NUMBERS.LIMIT) {
        applicationManager.getModulesPresentationController("TransferModule").fetchPreviousScheduledUserTransactions();
      }
      else {
        this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      }
    },
    /**Configure Pagination for External Accounts
     * @param  {object} viewModel viewModel of external accounts 
     * @param  {object} config configuration for pagination
     */
    setExternalAccountsPagination: function (viewModel, config) {
      if (typeof config.offset === "number") {
        this.view.transfermain.tablePagination.flxPagination.setVisibility(true);
        offset = config.offset;
        this.setNextExternalAccounts();
        this.setPreviousExternalAccounts();
        this.view.transfermain.tablePagination.lblPagination.text = (config.offset + 1) + '-' + (config.offset + config.limit) + ' ' + kony.i18n.getLocalizedString("i18n.topmenu.accounts");
        if (viewModel.length < ViewConstants.MAGIC_NUMBERS.LIMIT) {
          this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () { };
          this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
        }
      }

    },
    /**Configure Pagination for previous External Accounts
     */
    setPreviousExternalAccounts: function () {
      var self = this;
      if (offset <= 0) {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () { };
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      } else {
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () {
          applicationManager.getModulesPresentationController("TransferModule").fetchPreviousExternalAccounts();
        }
        this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      }
    },
    /**Configure Pagination for Next Button of External Accounts
   */
    setNextExternalAccounts: function () {
      var self = this;
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () {
        self.getNextExternalAccounts();
      }
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
    },
    /**Shows Next External Accounts - From pagination
     */
    getNextExternalAccounts: function () {
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
      applicationManager.getModulesPresentationController("TransferModule").fetchNextExternalAccounts();
    },
    /**Shows Previous External Accounts
     */
    getPreviousExternalAccounts: function () {
      if (offset >= ViewConstants.MAGIC_NUMBERS.LIMIT) {
        applicationManager.getModulesPresentationController("TransferModule").fetchPreviousExternalAccounts();
      }
      else {
        this.view.transfermain.tablePagination.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
      }
    },
    /**Returns the type based on transaciton object or external account
     * @param  {object} transactionObject viewModel of external accounts 
     * @param  {object} externalAccounts configuration for pagination
     * @returns {String} type of transaciton oraccount number of account
     */
    getTypeByTransactionObject: function (transactionObject, externalAccounts) {
      if (transactionObject.transactionType === "InternalTransfer") {
        return OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS;
      }

    },
    /**Filters the details from transaction
     * @param  {object} viewModel viewModel of Transaction
     */
    filterRepeatTransactionAccounts: function (viewModel) {
      var makeTransfer = {
        fromAccounts: viewModel.userAccounts.filter(function (userAccount) { return userAccount.accountID === viewModel.transactionObject.fromAccountNumber }),
        toAccounts: this.filterToAccounts(viewModel.userAccounts, viewModel.externalAccounts, viewModel.transactionObject),
        defaultFromAccountNumber: viewModel.transactionObject.fromAccountNumber,
        repeatTransactionObject: viewModel.transactionObject,
        onCancelCreateTransfer: viewModel.onBackPressed,
        limit: this.getMinMaxTransfersLimit(this.getTypeByTransactionObject(viewModel.transactionObject, viewModel.externalAccounts))
      };
      this.updateMakeTransferForm(makeTransfer);
    },
    /**Returns the type based on transaciton object or external account
     * @param  {object} userAccounts viewModel of user account
     * @param  {object} externalAccounts view model of the external accounts
     * @param  {object} transactionObject view model of the transaction object
     * @returns {String} accountID or account number of the account
     */
    filterToAccounts: function (userAccounts, externalAccounts, transactionObject) {
      if (transactionObject.transactionType === 'InternalTransfer') {
        return userAccounts.filter(function (userAccount) {
          return userAccount.accountID === transactionObject.toAccountNumber;
        })
      }
      else {
        return externalAccounts.filter(function (externalAccount) {
          return externalAccount.accountNumber === transactionObject.ExternalAccountNumber;
        })
      }
    },
    /** Converts Transaction Model Object to a view model
   * @param  {object} makeTransferViewModel View Model containing context and data
   * @param  {object} transaction Transaction Model Object
   * @returns {object} viewModel
   */
    generateTransferData: function(makeTransferViewModel, transaction) {
            var viewModel = {};
            viewModel.accountFromKey = transaction.fromAccountNumber;
            viewModel.accountToKey = transaction.transactionType === 'InternalTransfer' ? transaction.toAccountNumber : transaction.ExternalAccountNumber
            viewModel.amount = transaction.amount || "";
            viewModel.frequencyKey = transaction.frequencyType || 'Once';
            viewModel.noOfRecurrences = transaction.numberOfRecurrences || "";
            viewModel.notes = transaction.transactionsNotes || "";
            viewModel.howLongKey = (transaction.numberOfRecurrences && transaction.numberOfRecurrences !== "0") ? "NO_OF_RECURRENCES" : "ON_SPECIFIC_DATE";
            var dateFormat =  applicationManager.getFormatUtilManager().getDateFormat();
			if(!transaction.isModifiedTransferFlow){
				if (transaction.scheduledDate && (transaction.isScheduled === "1" || transaction.isScheduled === "true") && this.getDateObjectFromServer(transaction.scheduledDate).getTime() < new Date().getTime()) {
					viewModel.sendOnDate = this.getCurrDateString();
				} else {
					viewModel.sendOnDate = (transaction.isScheduled === "1" || transaction.isScheduled === "true") ? commonUtilities.getFrontendDateString(transaction.scheduledDate, dateFormat) : this.getCurrDateString();
				}
				if (transaction.frequencyEndDate && (transaction.isScheduled === "1" || transaction.isScheduled === "true") && this.getDateObjectFromServer(transaction.frequencyEndDate).getTime() < new Date().getTime()) {
					viewModel.endOnDate = this.getCurrDateString();
				} else {
					viewModel.endOnDate = (transaction.isScheduled === "1" || transaction.isScheduled === "true")? commonUtilities.getFrontendDateString(transaction.frequencyEndDate, dateFormat) : this.getCurrDateString();
				}
			} else {
				if (transaction.scheduledDate && this.getDateObject(transaction.scheduledDate).getTime() < new Date().getTime()) {
					viewModel.sendOnDate = this.getCurrDateString();
				} else {
					viewModel.sendOnDate = transaction.scheduledDate;
				}
				if (transaction.frequencyEndDate && this.getDateObject(transaction.frequencyEndDate).getTime() < new Date().getTime()) {
					viewModel.endOnDate = this.getCurrDateString();
				} else{
					viewModel.endOnDate = transaction.frequencyEndDate;
				}
			}
            viewModel.transactionType = transaction.transactionType;
            if (transaction.transactionType === applicationManager.getTypeManager().getTransactionTypeBackendValue(OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER)) {
                viewModel.toAccounts = makeTransferViewModel.toAccounts;
                viewModel.type = OLBConstants.TRANSFER_TYPES.INTERNATIONALBANK_ACC;
            }
            viewModel.fromAccounts = makeTransferViewModel.fromAccounts;
            viewModel.IBAN = transaction.IBAN ? transaction.IBAN : "";
            viewModel.ExternalAccountNumber = transaction.ExternalAccountNumber ? transaction.ExternalAccountNumber : "";
            viewModel.toAccountName = transaction.toAccountName ? transaction.toAccountName : ""
            viewModel.isInternationalAccount = transaction.isInternationalAccount ? transaction.isInternationalAccount : "";
            viewModel.swiftCode = transaction.swiftCode ? transaction.swiftCode : "";
            viewModel.bankName = transaction.bankName ? transaction.bankName : "";
            return viewModel;
        },
    /** Returns Current Date in string format 
    * @returns {string} Date String
     */
    getCurrDateString: function () {
      return commonUtilities.getFrontendDateString(new Date());
    },
    /**Returns date object from a dateString
     * @param  {object} dateString String of the Date
     * @returns {object} Date Object
     */
    getDateObject: function (dateString) {
      var formatUtilManager = applicationManager.getFormatUtilManager();
      return formatUtilManager.getDateObjectFromCalendarString(dateString, (formatUtilManager.getDateFormat()).toUpperCase());
    },

     /**Returns date object from a dateString
     * @param  {object} dateString String of the Date
     * @returns {object} Date Object
     */
    getDateObjectFromServer: function (dateString) {
      var formatUtilManager = applicationManager.getFormatUtilManager();
      return formatUtilManager.getDateObjectfromString(dateString, "YYYY-MM-DD");
    },

    /**Returns date object from a dateString
     * @param  {object} viewModel view Model of the transaction 
     */
    filterDataToEditTransaction: function (viewModel) {
      var makeTransfer = {
        fromAccounts: viewModel.userAccounts.filter(this.userAccountsFilter),
        toAccounts: this.filterToAccounts(viewModel.userAccounts, viewModel.externalAccounts, viewModel.editTransactionObject),
        defaultFromAccountNumber: viewModel.editTransactionObject.fromAccountNumber,
        editTransactionObject: viewModel.editTransactionObject,
        onCancelCreateTransfer: viewModel.onCancelCreateTransfer,
        isEdit: true,
        limit: this.getMinMaxTransfersLimit(this.getTypeByTransactionObject(viewModel.editTransactionObject, viewModel.externalAccounts))
      };
      this.updateMakeTransferForm(makeTransfer);
    },

    /**Removes commans from currency
    * @param  {number} amount amount entered
    * @returns {number} amount
    */
    removeCurrencyWithCommas: function (amount) {
      if (amount === undefined || amount === null) {
        return;
      }
      return applicationManager.getFormatUtilManager().deFormatAmount(amount);
    },

    /**Call back function for View Activity Button
     */
    onBtnViewActivity: function () {
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data[index];
      applicationManager.getModulesPresentationController("TransferModule").showSelectedAccountTransactions(data);
      this.AdjustScreen();
    },
    /**Shows Activity of a external account
     * @param  {array} viewModel Array of transaction objects  
     */
    showExternalAccountTransactionActivity: function (viewModel) {
      var self = this;
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.transferActivitymod.lblFromTitle.text = kony.i18n.getLocalizedString("i18n.common.To");
      this.view.transferActivitymod.flximgdatemod.setVisibility(false);
      this.view.transferActivitymod.flxfromaccountimgmod.setVisibility(false);
      this.view.flxTrasfersWindow.isVisible = false;
      this.view.transferactivity.isVisible = true;
      this.view.transferActivitymod.tablePagination.isVisible = false;
      this.view.transferActivitymod.flxSort.isVisible = false;
      this.view.transferActivitymod.flxsorttransfers.isVisible = true;
      this.view.transferActivitymod.flxSegmentBillPay.isVisible = false;
      this.view.transferActivitymod.flxSegment.isVisible = true;
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers"),
        callback: function () {
          applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
        }
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
        callback: function () {
          applicationManager.getModulesPresentationController("TransferModule").getExternalAccounts()
        }
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity")
      }]);
      var len = viewModel.length;
      var break_point = kony.application.getCurrentBreakpoint();
      this.view.transferActivitymod.lblAccountName.text = viewModel[len - 1].accountNumber;
      this.view.transferActivitymod.lblAccountHolder.text = viewModel[len - 1].nickName;
      var widgetDataMap = {
        "flxAmount": "flxAmount",
        "flxDate": "flxDate",
        "flxFrom": "flxFrom",
        "flxRunningBalance": "flxRunningBalance",
        "flxSort": "flxSort",
        "flxStatus": "flxStatus",
        "lblAmount": "lblAmount",
        "lblDate": "lblDate",
        "lblFrom": "lblFrom",
        "lblpaiddate": "lblpaiddate",
        "lblRunningBalance": "lblRunningBalance",
        "lblStatus": "lblStatus",
        "lblAmount1": "lblAmount1",
        "lblAmountHeader": "lblAmountHeader",
        "lblSeparator": "lblSeparator"
      };

      var data = [];
      if (len < 2) {
        this.view.transferActivitymod.flxSegment.isVisible = false;
        this.view.transferActivitymod.flxNoRecords.isVisible = true;
        this.view.transferActivitymod.rtxNoRecords.text = kony.i18n.getLocalizedString("i18n.transfers.noTransactions");
        this.view.transferActivitymod.lblAmountDeducted.text = kony.i18n.getLocalizedString("i18n.common.NA");
      } else {
        var i = 0;
        for (i = 0; i < len - 1; i++) {
          data.push({
            "lblAmount": commonUtilities.formatCurrencyWithCommas(Math.abs(viewModel[i].amount)),
            "lblDate": viewModel[i].transactionDate.slice(),
            "lblFrom": viewModel[i].fromNickName,
            "lblStatus": viewModel[i].statusDescription,
            "lblpaiddate": commonUtilities.getFrontendDateString(viewModel[i].transactionDate),
            "lblAmount1": commonUtilities.formatCurrencyWithCommas(Math.abs(viewModel[i].amount)),
            "lblAmountHeader": "Running Balance",
            "lblSeparator": "lblSeparator",
            "lblFromHeader": "From:",
            "template": "CopyflxSort0ga4b47ecec1840"
          });
        }
        this.view.transferActivitymod.lblAmountDeducted.text = commonUtilities.formatCurrencyWithCommas(viewModel[0].totalAmount);
        this.view.transferActivitymod.segTransferActivity.widgetDataMap = widgetDataMap;
        if (break_point == 640) {
          for (i = 0; i < data.length; i++) {
            data[i].template = "flxtransferactivitymobile";
          }
        }
        this.view.transferActivitymod.segTransferActivity.setData(data);
        this.view.transferActivitymod.flxSegment.isVisible = true;
        this.view.transferActivitymod.flxNoRecords.isVisible = false;
      }
      this.view.transferActivitymod.btnbacktopayeelist.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showExternalAccounts();
      }
    },
    /**Shows Selected External Account
     * @param  {object} viewModel  data of externalAccount 
     */
    showSelectedExternalAccount: function (viewModel) {
      this.showExternalAccounts(viewModel.accounts, viewModel.config, viewModel.pagination);   //showExternalAccount in case of bhawna
      this.showSelectedRow(viewModel.config.index);
    },
    responsiveViews: {},
    initializeResponsiveViews: function () {
      this.responsiveViews["transferactivity"] = this.isViewVisible("transferactivity");
      this.responsiveViews["flxTrasfersWindow"] = this.isViewVisible("flxTrasfersWindow");
   //   this.responsiveViews["flxTrasfersWindow"] = this.isViewVisible("flxTrasfersWindow");
      this.responsiveViews["flxNoAccounts"] = this.isViewVisible("flxNoAccounts");
      this.responsiveViews["flxTransfersGateway"] = this.isViewVisible("flxTransfersGateway");
      this.responsiveViews["segmentTransfers"] = this.isViewVisible("segmentTransfers");
      this.responsiveViews["flxMakeTransferForm"] = this.isViewVisible("flxMakeTransferForm");
      this.responsiveViews["flxNoTransactions"] = this.isViewVisible("flxNoTransactions");
      this.responsiveViews["flxChangeTransferType"] = this.isViewVisible("flxChangeTransferType");
      this.responsiveViews["flxAddAccountWindow"] = this.isViewVisible("flxAddAccountWindow");   
    },
    isViewVisible: function (container) {
      if (this.view[container] == undefined) {
        return this.view.transfermain[container].isVisible;
      } else {
        return this.view[container].isVisible
      }
    },
    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmTransfersController}
    * @param {integer} width - current browser width
    * @return {} 
    * @throws {}
    */
    onBreakpointChange: function (width) {
      var scope = this;
      this.view.flxFooter.isVisible = true;
      this.view.transfermain.flxHeader.isVisible = false;
      this.view.transfermain.maketransfer.flxOption1.isVisible = false;
      this.view.transfermain.maketransfer.flxOption2.isVisible = false;
      this.view.transfermain.maketransfer.flxOption3.isVisible = false;
      this.view.transfermain.maketransfer.flxOption4.isVisible = false;
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.isVisible = false;
      this.view.transfermain.maketransfer.flxCalEndingOn.isVisible = false;
      var views;
      if (width === 640) {
        var scopeObj = this;
        views = Object.keys(this.responsiveViews);
        views.forEach(function (e) {
          if (scope.view[e] == undefined) {
            scope.view.transfermain[e].isVisible = scope.responsiveViews[e];
          } else {
            scope.view[e].isVisible = scope.responsiveViews[e];
          }
        });
        this.view.transferActivitymod.flxtransferActivityWrapper.skin = "slFbox";
        this.view.transferActivitymod.flxRight.skin = "sknFlxffffffborderr0pxe3e3e3";
        this.view.transferActivitymod.flxHeader.skin = "sknFlxffffffborderr0pxe3e3e3";
        this.view.transfermain.flxTabsChecking.skin = "sknFlxffffffShadowdddcdcnoradius";
        this.view.customheader.lblHeaderMobile.text = "Transfers";
        this.view.transfermain.Search.skin = "slFbox";
        this.view.transfermain.flxTransferType.skin = "sknFlxffffff";
        this.view.transfermain.flxToMyKonyBank.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToOtherKonyBank.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToOtherBank.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToInternational.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxWireTransfer.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.maketransfer.skin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxMakeTransferForm.skin = "slFbox";
        this.view.transfermain.maketransfer.flxTransferForm.skin = "slFbox";
        this.view.transfermain.flxArrow1.isVisible = true;
        this.view.transfermain.flxArrow2.isVisible = true;
        this.view.transfermain.flxArrow3.isVisible = true;
        this.view.transfermain.flxArrow4.isVisible = true;
        this.view.transfermain.flxArrow5.isVisible = true;
        this.view.transfermain.flxToMyKonyBankInfo.skin = "slFbox";
        this.view.transfermain.flxToOtherKonyBankInfo.skin = "slFbox";
        this.view.transfermain.flxToOtherBankInfo.skin = "slFbox";
        this.view.transfermain.flxToInternationalInfo.skin = "slFbox";
        this.view.transfermain.flxWireTransferInfo.skin = "slFbox";
        this.view.transfermain.flxMainContainerTable.skin = "slFbox";
        this.view.forceLayout();
      }
      else {
        views = Object.keys(this.responsiveViews);
        views.forEach(function (e) {
          if (scope.view[e] == undefined) {
            scope.view.transfermain[e].isVisible = scope.responsiveViews[e];
          } else {
            scope.view[e].isVisible = scope.responsiveViews[e];
          }
        });
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.transferActivitymod.flxtransferActivityWrapper.skin = "sknFlxffffffShadowdddcdc";
        this.view.transferActivitymod.flxHeader.skin = "slFbox";
        this.view.transfermain.maketransfer.flxTransferForm.skin = "sknFFFFFFnoBor";
        this.view.transfermain.Search.skin = "sknFlxffffff";
        this.view.transfermain.flxTabsChecking.skin = "slFbox";
        this.view.transfermain.flxMainContainerTable.skin = "sknFlxffffffShadowdddcdc"
        this.view.transfermain.flxToMyKonyBank.skin = "sknBorderE3E3E3";
        this.view.transfermain.flxToOtherKonyBank.skin = "sknBorderE3E3E3";
        this.view.transfermain.flxToOtherBank.skin = "sknBorderE3E3E3";
        this.view.transfermain.flxToInternational.skin = "sknBorderE3E3E3";
        this.view.transfermain.flxWireTransfer.skin = "sknBorderE3E3E3";
        this.view.transferActivitymod.flxRight.skin = "slFbox";
        this.view.transfermain.flxToMyKonyBankInfo.skin = "sknflxffffffBottomBorder";
        this.view.transfermain.flxToOtherKonyBankInfo.skin = "sknflxffffffBottomBorder";
        this.view.transfermain.flxToOtherBankInfo.skin = "sknflxffffffBottomBorder";
        this.view.transfermain.flxToInternationalInfo.skin = "sknflxffffffBottomBorder";
        this.view.transfermain.flxWireTransferInfo.skin = "sknflxffffffBottomBorder";
        this.view.transfermain.flxToMyKonyBank.hoverSkin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToOtherKonyBank.hoverSkin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToOtherBank.hoverSkin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxToInternational.hoverSkin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxWireTransfer.hoverSkin = "sknFlxffffffShadowdddcdc";
        this.view.transfermain.flxArrow1.isVisible = false;
        this.view.transfermain.flxArrow2.isVisible = false;
        this.view.transfermain.flxArrow3.isVisible = false;
        this.view.transfermain.flxArrow4.isVisible = false;
        this.view.transfermain.flxArrow5.isVisible = false;
        this.view.forceLayout();
      }
      if (width === 1388) {
        this.view.lblTransfers.skin = "sknSSP42424220Px";
      }
      this.AdjustScreen();
    },

    /**
     * used to fetch the recipeint by iban
     */
    fetchRecipientbyIbanorName: function () {
      var param = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.flxTransferForm.flxRecipientIBAN.txtRecipientIBAN.text;
      var data = {
        searchKeyword: param
      }
      if(param.length > 0)
      {
        var presentationController = applicationManager.getModulesPresentationController("TransferModule");
        presentationController.fetchExternalAccountsByIbanOrName(data);
      }else{
        this.clearIbanOrAccountNumber();
      }
    },

    /**
    * used to fetch the recipeint by iban
    */
    fetchInternationalRecipientsbyAccountNumber: function () {
      var param = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.flxTransferForm.flxRecipientIBAN.txtRecipientIBAN.text;
      var data = {
        searchKeyword: param
      }
      if(param.length > 0)
      {
        var presentationController = applicationManager.getModulesPresentationController("TransferModule");
        presentationController.fetchExternalAccountsByAccountNumber(data);
      }else{
        this.clearIbanOrAccountNumber();
      }
    },

    /** Shows Selected External Account
      * @param  {object} response  data of externalAccount 
    */
    selectIbanorName: function (response) {
      if (response.length === 0) {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = "";
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.forceLayout();

        var isValidIBAN = applicationManager.getValidationUtilManager().isValidIBAN(this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text);

        if (isValidIBAN) {
          this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(true);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(true);
          this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
          newDomesticRecipient = true;
        } else {
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
          newDomesticRecipient = null;
        }
        this.AdjustScreen();
      } else {
        var widgetDataMap = {
          "lblIBANKey": "lblIBANKey",
          "lblIBANValue": "lblIBANValue",
          "lblNickNameKey": "lblNickNameKey",
          "lblNickNameValue": "lblNickNameValue",
          "lblSeperator": "lblSeperator"
        };
        var ibanAccounts = response.map(function (record) {
          return {
            "lblIBANKey": kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN"),
            "lblIBANValue": record.IBAN,
            "lblNickNameKey": kony.i18n.getLocalizedString("i18n.TransferEur.nickName"),
            "lblNickNameValue": record.nickName ? record.nickName : record.beneficiaryName,
            "lblSeperator": {
              "text": "'"
            }
          }
        });
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.widgetDataMap = widgetDataMap;
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setData(ibanAccounts);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setVisibility(true);
        this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
        this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.forceLayout();
        this.AdjustScreen();
      }
    },

    /**
     * Shows Selected External Account
      * @param  {object} response  data of external International Account
     */
    selectInterNationalAccountNumber: function (response) {
      if (response.length === 0) {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.forceLayout();
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = "";

        //Need to check valid International Account
        var isValidAccountNumber = true;
        if (isValidAccountNumber) {
          this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(true);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(true);
          this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
          newInternationalRecipient = true;
        } else {
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
          this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
          newInternationalRecipient = null;
        }

        this.AdjustScreen();
      } else {
        var widgetDataMap = {
          "lblIBANKey": "lblIBANKey",
          "lblIBANValue": "lblIBANValue",
          "lblNickNameKey": "lblNickNameKey",
          "lblNickNameValue": "lblNickNameValue",
          "lblSeperator": "lblSeperator",
          "Lbl1": "Lbl1",
          "Lbl3": "Lbl3"
        };
        var ibanAccounts = response.map(function (record) {
          return {
            "lblIBANKey": kony.i18n.getLocalizedString("i18n.ProfileManagement.Name"),
            "lblIBANValue": record.nickName ? record.nickName : record.beneficiaryName,
            "lblNickNameKey": kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountNumber"),
            "lblNickNameValue": record.accountNumber,
            "lblSeperator": {
              "text": "'"
            },
            "Lbl1": record.swiftCode,
            "Lbl3": record.bankName
          }
        });
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.widgetDataMap = widgetDataMap;
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setData(ibanAccounts);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(true);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.setVisibility(true);
        this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
        this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.forceLayout();
        this.AdjustScreen();
      }
    },


    /**
     * used to set the IBan value
     */
    getIbanValue: function () {
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblIBANValue;
      if (this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblNickNameValue) {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblNickNameValue;
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(false);
      } else {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
      }
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
      this.AdjustScreen();
    },

    /**
     * used to set the IBan value
     */
    getInternationalAccountNumber: function () {
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblNickNameValue;
      if (this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblIBANValue) {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.segToAccounts.selectedRowItems[0].lblIBANValue;
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(false);
      } else {
        this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
      }
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
      this.AdjustScreen();
    },

    /**
    * used to clear the IBAN and Account Number
    */
    clearIbanOrAccountNumber: function () {
      this.view.transfermain.maketransfer.flxRecipientIBAN.txtRecipientIBAN.text = "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.text = "";
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.transfermain.flxMainContainerTable.flxMakeTransferForm.maketransfer.tbxRecipientNameDomestic.setEnabled(true);
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
    },

    /**
     * used to hide the IBAN Poup selection
    */
    closeIBANAccountNumberPopup: function () {
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipient.setVisibility(false);
      this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
      this.view.transfermain.maketransfer.flxRecipientIBAN.lblNewRecipientTickMark.setVisibility(false);
    },

     /**
     * used to hide the IBAN Poup selection
    */
   hideIBANAccountNumberPopup: function () {
    this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.flxNoMatchesFound.setVisibility(false);
    this.view.flxMain.flxMainContainer.flxMainWrapper.flxTrasfersWindow.flxRecipientsSegment.setVisibility(false);
  }


  }
});