/**
 * Description of Module representing a Confirm form.
 * @module frmAckowledgementController
 */

define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function (commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
  return  /** @alias module:frmAckowledgementController */{
    
    
  
    
    /** updates the present Form based on required function.
       * @param {uiDataMap[]} uiDataMap 
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
      } else {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (uiDataMap.sideMenu) {
        this.updateHamburgerMenu(uiDataMap.sideMenu);
      }
      if (uiDataMap.topBar) {
        this.updateTopBar(uiDataMap.topBar);
      }

      if (uiDataMap.transferAcknowledge) {
        this.updateTransferAcknowledgeUI(uiDataMap.transferAcknowledge);
      }

      this.AdjustScreen();
    },

    /**
     * used to perform UI Activities like fith the UI in Screen.
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },

    /**
     * used perform the initialize activities.
     * 
    */
    initActions: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
      this.AdjustScreen();
      if (commonUtilities.isPrintEnabled()) {
        this.view.imgPrintBulk.setVisibility(true);
        this.view.lblPrintfontIcon.setVisibility(true);
        this.view.imgPrintBulk.onTouchStart = this.onClickPrintBulk;
        this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
      } else {
        this.view.imgPrintBulk.setVisibility(false);
        this.view.lblPrintfontIcon.setVisibility(false);
      }
    },

    /**
     * used to perform the post show activities
     * 
     */
    postShowFrmAcknowledgement: function () {
      this.view.customheader.forceCloseHamburger();
      this.view.confirmDialogAccounts.confirmButtons.setVisibility(false);
      this.customiseHeaderComponentforAcknowledgement();
      this.onBreakpointChange(kony.application.getCurrentBreakpoint());
      this.view.forceLayout();
      this.view.btnMakeTransfer.top = "1390dp";
      applicationManager.getNavigationManager().applyUpdates(this);
      this.AdjustScreen();
    },

    /**
     * used to perform Ui Activities
     */
    customiseHeaderComponentforAcknowledgement: function () {
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
    },
    /**used to convert the CalenderFormat Date
     * @param {String} dateString string formated date 
     * @param {String} inputFormat input format of the date
     * @return {date} Date object
     */
    getDateFromDateString: function (dateString, inputFormat) {
      var fu = applicationManager.getFormatUtilManager();
      var dateObj = fu.getDateObjectfromString(dateString, inputFormat);
      var outputDate = fu.getFormatedDateString(dateObj, fu.APPLICATION_DATE_FORMAT);
      return outputDate;
    },
    /**Returns Date Object from Date Components
   * @param  {array} dateComponents Date Components returned from Calendar Widget
   * @return {date} date
   */
    getDateObj: function (dateComponents) {
      var date = new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1]) - 1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0, 0, 0, 0)
      return date;
    },
    /** Compares Date with todays and tell is its future or not
   * @param  {object} dateComponents object
   * @returns {boolean} True for future else false
   */
    isFutureDate: function (dateComponents) {
      var dateObj = this.getDateObj(dateComponents)
      var endTimeToday = new Date();
      var minutes = ViewConstants.MAGIC_NUMBERS.MAX_MINUTES;
      endTimeToday.setHours(ViewConstants.MAGIC_NUMBERS.MAX_HOUR, minutes, minutes, minutes);
      if (dateObj.getTime() > endTimeToday.getTime()) {
        return true;
      }
      return false;
    },
    /**Formats the Currency
       * @param  {Array} amount Array of transactions model 
       * @param  {function} currencySymbolNotRequired Needs to be called when cancel button is called
       * @param {String} currencySymbolCode currencySymbolCode
       * @return {function} function to put comma
       */
    formatCurrency: function (amount, currencySymbolNotRequired,currencySymbolCode) {
      return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired,currencySymbolCode);
    },
    /**Entry Point for Transfer Acknowledgement
    * @param {object} viewModel Transfer Data
    */
    updateTransferAcknowledgeUI: function (viewModel) {
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");
      this.customizeUIForTransferAcknowledege();
      if (this.isFutureDate(viewModel.transferData.sendOnDateComponents) || viewModel.transferData.frequencyKey !== "Once") {
        this.showTransferAcknowledgeForScheduledTransaction(viewModel);
      }
      else {
        this.showTransferAcknowledgeForRecentTransaction(viewModel);
      }
      // this.onBreakpointChange(kony.application.getCurrentBreakpoint());
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = commonUtilities.getAccountDisplayName(viewModel.transferData.accountFrom);
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = kony.i18n.getLocalizedString("i18n.accounts.Note");
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = viewModel.transferData.notes;
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(false);
      this.view.confirmDialogAccounts.flxDescription.setVisibility(false);
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);

      if (viewModel.transferData.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        this.view.confirmDialogAccounts.flxTransactionFee.setVisibility(false);
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue1.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue2.setVisibility(false);
        this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.accountTo.beneficiaryName ? viewModel.transferData.accountTo.nickName : (commonUtilities.getAccountDisplayName(viewModel.transferData.accountTo));
        this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = this.formatCurrency(viewModel.transferData.amount);
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.TransfersEur.SendOn');
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.sendOnDate;
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency");
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = viewModel.transferData.frequencyType;
        this.adjustAckScreen(0);
      } else if (viewModel.transferData.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        this.view.confirmDialogAccounts.flxTransactionFee.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue1.setVisibility(false);
        this.view.confirmDialogAccounts.keyValue2.setVisibility(false);
		    this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(true);
        this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.reciepientName;
        this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text = kony.i18n.getLocalizedString('i18n.TransferEur.reciepientIBAN');
        this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = applicationManager.getFormatUtilManager().formatIBAN(viewModel.transferData.IBAN);
        commonUtilities.enableCopy(this.view.confirmDialogAccounts.keyValueAccountType.lblValue); 
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = this.formatCurrency(viewModel.transferData.amount);
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency");
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = viewModel.transferData.frequencyType;
        this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text = kony.i18n.getLocalizedString('i18n.TransfersEur.SendOn');
        this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text = viewModel.transferData.sendOnDate;
        if (viewModel.transferData.newDomesticRecipient) {
          this.view.btnSavePayee.setVisibility(true);
          this.view.btnSavePayee.onClick = function () {
            var domesticAccount = {
              "IBAN": viewModel.transferData.IBAN,
              "beneficiaryName": viewModel.transferData.reciepientName
            }
            var presenter = applicationManager.getModulesPresentationController("TransferModule");
            presenter.showDomesticAccountsFromTransferFlow(domesticAccount);
          }.bind(this);
        } else {
          this.view.btnSavePayee.setVisibility(false);
        }
		this.adjustAckScreen(0);
      } else if (viewModel.transferData.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {

        if (applicationManager.getConfigurationManager().internationalTransactionFeeEnabled === "true") {
          this.view.confirmDialogAccounts.flxTransactionFee.lblFee.text = applicationManager.getConfigurationManager().currencyCode[applicationManager.getConfigurationManager().getBaseCurrency()] + "" + applicationManager.getConfigurationManager().internationalTransactionFee;
          this.view.confirmDialogAccounts.flxTransactionFee.lblPaidByRecipient.text = this.getfeePayementName(viewModel.transferData.feePaidByReceipent);
          this.view.confirmDialogAccounts.flxTransactionFee.setVisibility(true);
        }else{
			 this.view.confirmDialogAccounts.flxTransactionFee.setVisibility(false);
		}
		this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(true);
        this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.reciepientName;
        this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.swiftCode');
        this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = viewModel.transferData.swiftCode;
        commonUtilities.enableCopy(this.view.confirmDialogAccounts.keyValueAccountType.lblValue);
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.bankName');
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.bankName;
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountNumber");
        this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = viewModel.transferData.ExternalAccountNumber;
        this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text = kony.i18n.getLocalizedString('i18n.common.Currency');
        this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text = viewModel.transferData.transactionCurrency + "(" + applicationManager.getConfigurationManager().currencyCode[viewModel.transferData.transactionCurrency] + ")";
        this.view.confirmDialogAccounts.keyValue.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
        this.view.confirmDialogAccounts.keyValue.lblValue.text = applicationManager.getConfigurationManager().currencyCode[viewModel.transferData.transactionCurrency]+""+ this.formatCurrency(viewModel.transferData.amount,true);
        this.view.confirmDialogAccounts.keyValue1.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency");
        this.view.confirmDialogAccounts.keyValue1.lblValue.text = viewModel.transferData.frequencyType;
        this.view.confirmDialogAccounts.keyValue2.lblKey.text = kony.i18n.getLocalizedString('i18n.TransfersEur.SendOn');
        this.view.confirmDialogAccounts.keyValue2.lblValue.text = viewModel.transferData.sendOnDate;
        this.view.confirmDialogAccounts.keyValue.setVisibility(true);
        this.view.confirmDialogAccounts.keyValue1.setVisibility(true);
        this.view.confirmDialogAccounts.keyValue2.setVisibility(true);
        if (viewModel.transferData.newInternationalRecipient) {
          this.view.btnSavePayee.setVisibility(true);
          this.view.btnSavePayee.text = kony.i18n.getLocalizedString('i18n.payaperson.savereciepient');
          this.view.btnSavePayee.onClick = function () {
            var internationalAccount = {
              "swiftCode": viewModel.transferData.swiftCode,
              "bankName": viewModel.transferData.bankName,
              "accountNumber": viewModel.transferData.ExternalAccountNumber,
              "beneficiaryName": viewModel.transferData.reciepientName
            }
            var presenter = applicationManager.getModulesPresentationController("TransferModule");
            presenter.showInternationalAccounts(internationalAccount);
          }.bind(this);
        } else {
          this.view.btnSavePayee.setVisibility(false);
        }
        this.adjustAckScreen(1);
      }
      this.view.btnMakeTransfer.onClick = function () {
        var presenter = applicationManager.getModulesPresentationController("TransferModule");
        presenter.showTransferScreen();
      }.bind(this);
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
      this.view.btnAddAnotherAccount.onClick = function () {
        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({ initialView: 'recent' });
      }.bind(this);
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.viewTransferActivity');
      this.view.customheader.customhamburger.activateMenu("Transfers")
      this.view.forceLayout();
    },
    /**
     * used to adjust the acknowledgememnt screen 
     */
    adjustAckScreen: function(flag) {
          if(kony.application.getCurrentBreakpoint() === 1366 || kony.application.getCurrentBreakpoint() === 1380){
            if(flag === 1){
                this.view.flxAcknowledgementMain.height = "700dp";
                this.view.flxTransactionDetails.height = "700dp";
                this.view.btnAddAnotherAccount.top = "800dp";
                this.view.btnMakeTransfer.top = "800dp";
                this.view.btnSavePayee.top = "800dp";
            }
            else {
                this.view.flxAcknowledgementMain.height = "513dp";
                this.view.flxTransactionDetails.height = "513dp";
                this.view.btnAddAnotherAccount.top = "615dp";
                this.view.btnMakeTransfer.top = "615dp";
                this.view.btnSavePayee.top = "615dp";
            }
      		this.AdjustScreen();
          }
        },
    /**
     * used to return the getfeePayementName
     * @param {booelean} feePaidByReceipent feePaidByReceipent
     * @returns {string} feePaidByReceipent
     */
    getfeePayementName : function(feePaidByReceipent)
    {
      if(feePaidByReceipent)
      {
        return "(" + kony.i18n.getLocalizedString('i18n.TransferEur.ToBePaidByRecipient') + ")"
      }else{
        return kony.i18n.getLocalizedString('i18n.TransferEur.ToBePaidByRecipient') // Need to get proper I18n value
      }
    },

    /**UI Logic for Transfers
       */
    customizeUIForTransferAcknowledege: function () {
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(false);
      this.view.flxContainer.setVisibility(true);
      this.onBreakpointChange(kony.application.getCurrentBreakpoint());
      this.view.btnSavePayee.setVisibility(false);
      this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl");
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.transfers.btnMakeTransfer");
      this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity");
      this.view.btnSavePayee.text = kony.i18n.getLocalizedString('i18n.payaperson.savereciepient');
      this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl");
      this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
    },
    /**Entry Point for Transfer Acknowledgement for Scheduled Transaction
       * @param {object} viewModel Transfer Data
       */
    showTransferAcknowledgeForScheduledTransaction: function (viewModel) {
      function getDateFromDateComponents(dateComponents) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return [dateComponents[0], monthNames[dateComponents[1] - 1], dateComponents[2]].join(" ");
      }
      this.view.acknowledgmentModify.setVisibility(true);
      this.view.acknowledgment.setVisibility(false);
      this.view.Balance.setVisibility(false);
      this.view.acknowledgmentModify.btnModifyTransfer.text = kony.i18n.getLocalizedString("i18n.common.modifyTransaction");
      this.view.acknowledgmentModify.btnModifyTransfer.onClick = this.modifyTransfer.bind(this, viewModel.transferData);
      this.view.acknowledgmentModify.btnModifyTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
      this.view.acknowledgmentModify.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
      this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourTransactionHasBeenScheduledfor");
      this.view.acknowledgmentModify.lblTransactionDate.text = getDateFromDateComponents(viewModel.transferData.sendOnDateComponents);
      this.view.acknowledgmentModify.lblTransactionDate.setVisibility(true); 
      if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "ON_SPECIFIC_DATE") {
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);        
        this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourRecurringTransferScheduled");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.endby");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.endOnDate;
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);

      }
      else if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "NO_OF_RECURRENCES") {
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);  
        this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourRecurringTransferScheduled");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.numberOfRecurrences;
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
      else {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      }
    },

    /**Modify A Scheduled Transfer
       * @param {object} transferData Transfer Data
       */
    modifyTransfer: function (transferData) {
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");


      var record = {
        transactionType: applicationManager.getModulesPresentationController("TransferModule").getTransferType(transferData.accountTo),
        fromAccountNumber: transferData.accountFrom.accountID,
        amount: transferData.amount,
        frequencyType: transferData.frequencyKey,
        numberOfRecurrences: transferData.numberOfRecurrences,
        transactionsNotes: transferData.notes,
        scheduledDate: transferData.sendOnDate,
        frequencyEndDate: transferData.endOnDate,
        frequencyStartDate: transferData.sendOnDate,
        isScheduled: "1",
        transactionDate: transferData.sendOnDate,
        transactionId: transferData.referenceId,
		isModifiedTransferFlow: true
      }

      if (transferData.type === OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS) {
        record.toAccountNumber = transferData.accountTo.accountID;
      } else if (transferData.type === OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT) {
        record.IBAN = transferData.IBAN;
        record.reciepientName = transferData.reciepientName;
        record.toAccountName = transferData.reciepientName;
        record.newDomesticRecipient = transferData.newDomesticRecipient ? transferData.newDomesticRecipient : null
      } else if (transferData.type === OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT) {
        record.ExternalAccountNumber = transferData.ExternalAccountNumber;
        record.bankName = transferData.bankName;
        record.feeCurrency = transferData.feeCurrency;
        record.transactionCurrency = transferData.transactionCurrency;
        record.swiftCode = transferData.swiftCode;
        record.reciepientName = transferData.reciepientName;
        record.newInternationalRecipient = transferData.newInternationalRecipient ? transferData.newInternationalRecipient : null
        record.reciepientName = transferData.reciepientName;
        record.toAccountName = transferData.reciepientName;
      }
      record.type = transferData.type;
      record.transactionType = transferData.transactionType;
      
      applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
        editTransactionObject: record
      });
    },
    /**Entry Point for Transfer Acknowledgement for Recent Transaction 
       * @param {object} viewModel Transfer Data
       */
    showTransferAcknowledgeForRecentTransaction: function (viewModel) {
      this.view.acknowledgment.setVisibility(true);
      this.view.Balance.setVisibility(true);
      this.view.acknowledgmentModify.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      this.view.Balance.lblSavingsAccount.text = viewModel.transferData.accountFrom.accountName;
      this.view.Balance.lblBalanceValue.text = this.formatCurrency(viewModel.transferData.accountFrom.availableBalance,false,viewModel.transferData.accountFrom.currencyCode);
    },

    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @param {integer} width - current browser width
    */
    onBreakpointChange: function (width) {
      if (width == 640) {
        this.view.acknowledgmentModify.lblTransactionMessage.skin = "sknSSPLight42424218Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin = "sknSSPLight42424218Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin = "sknBtnSSP0273e315px";
        this.view.btnTransferActivity.isVisible = false;
        this.view.confirmDialogAccounts.flxMain.skin = "sknFlexRoundedBorderFFFFFF3Pxshadowd9d9d9noshadow";
        this.view.customheader.lblHeaderMobile.text = "Acknowledgement";
        this.view.flxMain.top = 50 + "dp";
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.frame.height + 60 + "dp";
        //         this.view.btnMakeTransfer.top = 510 + this.view.flxTransactionDetails.frame.height + "dp";
        if(this.view.flxDowntimeWarning.isVisible  === true){
           this.view.btnAddAnotherAccount.top = 560 + this.view.flxTransactionDetails.frame.height + 160 + "dp";
        this.view.btnMakeTransfer.top = 560 + this.view.flxTransactionDetails.frame.height + 220 + "dp";
        }else{
        this.view.btnAddAnotherAccount.top = 560 + this.view.flxTransactionDetails.frame.height + 60 + "dp";
        this.view.btnMakeTransfer.top = 560 + this.view.flxTransactionDetails.frame.height + 120 + "dp";
        }
        this.view.btnAddAnotherAccount.isVisible = true;
        this.view.flxTransactionDetails.left = "10dp";
        this.view.flxTransactionDetails.width = this.view.flxAcknowledgementMain.width;
        //         this.view.flxMain.height = 510 + this.view.flxTransactionDetails.frame.height + 180 + "dp";
      } else {
        this.view.acknowledgmentModify.lblTransactionMessage.skin = "sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin = "sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin = "sknBtnSSP0273e317px";
        this.view.flxMain.top = 120 + "dp";
        this.view.btnTransferActivity.isVisible = false;
        //         this.view.btnMakeTransfer.top = 525 + "dp";
        this.view.btnAddAnotherAccount.skin = "sknBtnSSPBg0273e3Border0273e3";
        this.view.customheader.lblHeaderMobile.text = "";
      }
      if (width == 1024) {
        //         this.view.btnMakeTransfer.top = 973 + "dp";
        //         this.view.flxMain.height = 510 + this.view.flxTransactionDetails.frame.height + 180 + "dp";
      }
      this.AdjustScreen();
    },
    onClickPrintBulk: function () {
      this.presenter.showPrintPage({ transactionList: { "details": this.view.segBill.data, "module": this.view.lblBulkBillPayAcknowledgement.text}});
    },
    onClickPrint: function () {
      var printData = [];
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.common.status"),
        value: this.view.acknowledgment.lblTransactionMessage.text
      });
      printData.push({
        key: this.view.acknowledgment.lblRefrenceNumber.text,
        value: this.view.acknowledgment.lblRefrenceNumberValue.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.transfers.accountName"), 
        value: this.view.Balance.lblSavingsAccount.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.accounts.availableBalance"),
        value: this.view.Balance.lblBalanceValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueBankName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueBankName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueCountryName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text
      });
      if(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text
        });
      }
      if(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
        });
      }
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text
      });
      if (this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text !== "") {
        printData.push({
          key: this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text,
          value: this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text
        });
      }
      var printCallback = function(){
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.navigateToAcknowledgementForm();
      }
      var viewModel = {
        moduleHeader: this.view.lblBillPayAcknowledgement.text,
        tableList: [{
            tableHeader: this.view.confirmDialogAccounts.confirmHeaders.lblHeading.text,
            tableRows: printData
        }],
        printCallback: printCallback
      }
      this.presenter.showPrintPage({
        printKeyValueGroupModel: viewModel
      });
    }
  }
});
