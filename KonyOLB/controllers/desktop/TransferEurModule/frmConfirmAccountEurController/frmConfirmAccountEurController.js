define(['commonUtilities','FormControllerUtility','OLBConstants','ViewConstants','CSRAssistUI'],function(commonUtilities,FormControllerUtility,OLBConstants,ViewConstants,CSRAssistUI){
  var frmConfirmAccount = "frmConfirmAccount";
  return{
    
    
    /** Manages the upcomming flow
     * @param  {object} viewModel object consisting data based on which new flow has to drive  
     */
    updateFormUI: function (viewModel) {
      if (viewModel.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
    } else { 
        FormControllerUtility.hideProgressBar(this.view);
    }
      if(viewModel.internalAccount){
        this.setInternalAccount(viewModel)
      }
      if(viewModel.domesticAccount){
        this.setDomesticAccount(viewModel);
      }
      if(viewModel.internationalAccount){
        this.setInternationalAccount(viewModel);
      }
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.dataForNextForm(viewModel);
      this.AdjustScreen();
      this.view.forceLayout();
    },
    /** Sets the value for internal Account
     * @param {object} viewModel data to set
     */
    setInternalAccount: function(viewModel){
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);      
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.add_kony_account");
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")}, {text:kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm")}]);
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm");
      this.mapDetails(viewModel.internalAccount);
      this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.showCancelPopup.bind(this);
      this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, "internal");
      this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
    },
    /** Sets the value for domestic Account
     * @param {object} viewModel data to set
     */
    setDomesticAccount: function(viewModel){
      var data= viewModel.domesticAccount;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
      this.mapDetails(viewModel.domesticAccount);
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.Accounts.IBAN');
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = applicationManager.getFormatUtilManager().formatIBAN(data.IBAN);
      this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.cancelAddAccount.bind(this);
      this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, "domestic");
      this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
    },
    /** Sets the value for international Account
     * @param {object} viewModel data to set
     */
    setInternationalAccount: function(viewModel){
      var data= viewModel.internationalAccount;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);                  
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.swiftCode;
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.hamburger.transfers")}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
      this.mapDetails(viewModel.internationalAccount);
      this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.cancelAddAccount.bind(this);
      this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, 'international');
      this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
    },
    /** maps the dafault fields
     * @param {object} data data to set
     */
    mapDetails: function(data){
      this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
      this.view.flxEditHolisticAccount.isVisible=false;
      this.view.flxConfirmContainer.isVisible=true;
      this.view.confirmDialogAccounts.keyValueAccountType.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.accountNumber;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
      this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
    },
/** Actions to be done at preshow
     */
    preShowfrmConfirmAccount: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
      this.view.confirmDialogAccounts.confirmButtons.btnConfirm.setEnabled(true);
      this.view.confirmDialogAccounts.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.transfers.confirmAccountDetails");
      this.view.confirmDialogAccounts.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyAccountDetails');
      this.view.confirmDialogAccounts.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.CancelAccount');  
      this.view.flxCancelPopup.setVisibility(false);
      this.view.customheader.forceCloseHamburger();
      if(commonUtilities.isCSRMode()){
        CSRAssistUI.setCSRAssistConfigurations(scopeObj,frmConfirmAccount);
      }
      applicationManager.getNavigationManager().applyUpdates(this);
    },
/** Methods to be done at post show
     */
    postShowfrmConfirmAccount: function(){
      this.AdjustScreen();
    },
    /** UI code to adjust screen
     */
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          {this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;}
        else
          {this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;}        
      } else {
        this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
      }
      this.view.forceLayout();
    },         
/** handles the cancel flow
     */
    showCancelPopup:function()
    {
      var heightToSet = 140 + this.view.flxMainContainer.frame.height + this.view.flxFooter.frame.height;
      this.view.flxCancelPopup.height = heightToSet + ViewConstants.POSITIONAL_VALUES.DP;
      this.view.flxCancelPopup.isVisible = true;
      this.view.flxCancelPopup.setFocus(true);
      this.view.CustomPopupCancel.btnYes.onClick = function () {
        this.cancelAddAccount();
      }.bind(this);
      this.view.forceLayout();
    },
    /** hides cancel popup
     */
    hideCancelPopup:function()
    {
      this.view.flxCancelPopup.isVisible = false;
      this.view.forceLayout();
    },
    /** populates the data to verify form
     */
    populateInVerify: function(){
    var newPayeeData= { 
      "bankName": this.view.confirmDialogAccounts.keyValueBankName.lblValue.text,
      "accountType": this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text,
      "accountNumber": this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text,
      "beneficiaryName": this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text,
      "nickName":this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
    }; 
    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.createBankPayee(newPayeeData);
    },

    cancelAddAccount: function () {
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.showTransferScreen();
    },

    modifyAccount: function (type) {
      var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.modifyAccountInfo(type);
    },

  /** Adds Account
     */
  addAccount: function (data) {
    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferModule.presentationController.navigateToVerifyAccount(data);
  }
 

  };
});