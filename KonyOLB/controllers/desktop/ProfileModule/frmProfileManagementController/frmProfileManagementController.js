define(['CommonUtilities', 'CSRAssistUI','FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(CommonUtilities, CSRAssistUI, FormControllerUtility, OLBConstants, ViewConstants) {

    var orientationHandler = new OrientationHandler();
    var flag = 0;
    var update = "";
    var showOTP = 1;
    var alert = "Account"; 
    var finalData = [];
    var widgetsMap = [{
            menu: "flxProfileSettings",
            subMenu: {
                parent: "flxProfileSettingsSubMenu",
                children: [{
                        "configuration": "enableProfileSettings",
                        "widget": "flxProfile"
                    },
                    {
                        "configuration": "enablePhoneSettings",
                        "widget": "flxPhone"
                    },
                    {
                        "configuration": "enableEmailSettings",
                        "widget": "flxEmail"
                    },
                    {
                        "configuration": "enableAddressSettings",
                        "widget": "flxAddress"
                    },
                    {
                        "configuration": "enableUsernameAndPasswordSettings",
                        "widget": "flxUsernameAndPassword"
                    },
                    {
                        "configuration": "",
                        "widget": "flxChangeLanguage"
                    },       
                           
                ]
            },
            image: "lblProfileSettingsCollapse"
        },
        {
            menu: "flxSecuritySettings",
            subMenu: {
                parent: "flxSecuritySettingsSubMenu",
                children: [{
                        "configuration": "enableSecurityQuestionsSettings",
                        "widget": "flxSecurityQuestions"
                    },
                    {
                        "configuration": "enableSecureAccessCodeSettings",
                        "widget": "flxSecureAccessCode"
                    }
                ]
            },
            image: "lblSecuritySettingsCollapse"
        },
        {
            menu: "flxAccountSettings",
            subMenu: {
                parent: "flxAccountSettingsSubMenu",
                children: [{
                        "configuration": "enablePreferredAccountSettings",
                        "widget": "flxAccountPreferences"
                    },
                    {
                        "configuration": "enableDefaultAccounts",
                        "widget": "flxSetDefaultAccount"
                    }
                ]
            },
            image: "lblAccountSettingsCollapse"
        },
        {
            menu: "flxAlerts",
            subMenu: {
                parent: "flxAlertsSubMenu",
                children: [{
                        "configuration": "enableAlertSettings",
                        "widget": "flxTransactionAndPaymentsAlerts"
                    },
                    {
                        "configuration": "",
                        "widget": "flxSecurityAlerts"
                    },
                    {
                        "configuration": "",
                        "widget": "flxPromotionalAlerts"
                    },
                ]
            },
            image: "lblAlertsCollapse"
        }
    ];
    return {
        /**
         * @function
         *
         */
        willUpdateUI: function() {
            kony.print("still in the old navigation form");
        },
        showUsernameVerificationByChoice: function() {
            this.view.settings.imgUsernameVerificationcheckedRadio.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            this.view.settings.imgUsernameVerificationcheckedRadioOption2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            this.showViews(["flxUsernameVerificationWrapper"]);
        },
        updateFormUI: function(viewModel) {
            if (viewModel !== undefined) {
                this.view.flxDowntimeWarning.setVisibility(false);      
                if (viewModel.showDefautUserAccounts) this.showDefaultUserAccount(viewModel.showDefautUserAccounts);
                if (viewModel.showPrefferedView) this.showViews(['flxAccountsWrapper']);
                if (viewModel.getAccountsList) this.showAccountsList(viewModel.getAccountsList);
                if (viewModel.getPreferredAccountsList) this.showPreferredAccountsList(viewModel.getPreferredAccountsList);
                if (viewModel.errorSaveExternalAccounts) this.onSaveExternalAccount(viewModel.errorSaveExternalAccounts);
                if (viewModel.errorEditPrefferedAccounts) this.onPreferenceAccountEdit(viewModel.errorEditPrefferedAccounts);
                if (viewModel.userProfile) this.updateUserProfileSetttingsView(viewModel.userProfile);
                if (viewModel.emailList) this.updateEmailList(viewModel.emailList);
                if (viewModel.emails) this.setEmailsToLbx(viewModel.emails);
                if (viewModel.phoneList) this.updatePhoneList(viewModel.phoneList);
                if (viewModel.addressList) this.updateAddressList(viewModel.addressList);
                if (viewModel.addPhoneViewModel) this.updateAddPhoneViewModel(viewModel.addPhoneViewModel);
                if (viewModel.editPhoneViewModel) this.updateEditPhoneViewModel(viewModel.editPhoneViewModel);
                if (viewModel.showVerificationByChoice) {
                    this.updateRequirements();
                }
                if (viewModel.requestOtp) this.enterOtp();
                if (viewModel.verifyOtp) this.updateRequirements();
                if (viewModel.verifyQuestion) this.updateRequirements();
                if (viewModel.update) this.acknowledge();
                if (viewModel.securityAccess) this.showSecureAccessOptions(viewModel.securityAccess);
                if (viewModel.secureAccessOption) kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
                if (viewModel.SecurityQuestionExists) this.showSecurityQuestions(viewModel.SecurityQuestionExists);
                if (viewModel.answerSecurityQuestion) this.showSecurityVerification(viewModel.answerSecurityQuestion);
                if (viewModel.passwordExists) this.showExistingPasswordError();
                if (viewModel.wrongPassword) this.showWrongPasswordError();
                if (viewModel.requestOtpError) this.showRequestOtpError();
                if (viewModel.verifyOtpServerError) this.showVerifyOtpServerError(); //validate this statement.
                if (viewModel.verifyQuestionAnswerError) this.showVerifyQuestionAnswerError();
                if (viewModel.verifyQuestionServerAnswerError) this.showVerifyQuestionAnswerServerError(); //validate this statement.
                if (viewModel.getAnswerSecurityQuestionError) this.showGetAnswerSecurityQuestionError();
                if (viewModel.updateUsernameError) this.showUpdateUsernameError(viewModel.updateUsernameError);
                if (viewModel.updateUsernameServerError) this.showUpdateUsernameServerError(); //validate this statement.
                if (viewModel.SecurityQuestionExistsError) this.showSecurityQuestionExistsError();
                if (viewModel.updateSecurityQuestionError) this.showUpdateSecurityQuestionError();
                if (viewModel.fetchSecurityQuestionsError) this.showFetchSecurityQuestionsError(); //validate this statement.
                if (viewModel.checkSecurityAccessError) this.showCheckSecurityAccessError();
                if (viewModel.verifyOtpError) this.showVerifyOtpError();
                if (viewModel.addNewAddress) this.showAddNewAddressForm(viewModel.addNewAddress);
                if (viewModel.editAddress) this.showEditAddressForm(viewModel.editAddress);
                if (viewModel.secureAccessOptionError) this.showSecureAccessOptionError();
                if (viewModel.passwordExistsServerError) this.showPasswordExistsServerError();
                if (viewModel.passwordServerError) this.showPasswordServerError(viewModel.passwordServerError);
                if (viewModel.emailError) this.showEmailError(viewModel.emailError);
                if (viewModel.phoneDetails) this.showPhoneDetails(viewModel.phoneDetails);
                if (viewModel.editEmailError) this.showEditEmailError(viewModel.editEmailError);
                if (viewModel.Alerts) this.showAlerts(viewModel.Alerts.records);
                if (viewModel.AlertsError) this.showAlertsError(viewModel.AlertsError);
                if (viewModel.AlertsDataById) this.showAlertsDataByID(viewModel.AlertsDataById);
                if (viewModel.editExternalAccounts) this.goToEditExternalAccounts(viewModel.editExternalAccounts);
                if (viewModel.secureAccessSettings) this.showSecureAccessSettings();
                if (viewModel.usernamepasswordRules) this.getPasswordRules(viewModel.usernamepasswordRules);
                if (viewModel.isLoading !== undefined) this.changeProgressBarState(viewModel.isLoading);
                if (viewModel.usernamepolicies) {
                  this.getUsernameRules(viewModel.usernamepolicies);
                }
                if (viewModel.usernamepasswordRules) {
                    this.getPasswordRules(viewModel.usernamepasswordRules);
                };              
                if (viewModel.fetchFirstAlert) this.fetchFirstAlertType();
               if (viewModel.usernameAndPasswordLanding) this.showusernameAndPasswordLanding();
                if(viewModel.alertsSaved) this.saveAlertSuccess(viewModel.alertsSaved.alertWidget,viewModel.alertsSaved.alertsViewModel);
                if(viewModel.showSecurityQuestion) this.showSecurityQuestionsScreen();
                this.AdjustScreen();
                this.view.forceLayout();
            }
        },
      
      showusernameAndPasswordLanding:function(){
          this.showUsernameAndPassword();
          this.setSelectedSkin("UsernameAndPassword");
         FormControllerUtility.hideProgressBar(this.view);
      },
      /**
          * Calls the method assigned to first alert comming from backend.
          */
        fetchFirstAlertType: function() {
            this.view.settings.flxAccountAlerts.onClick();
        },
        setLanguages: function() {
            var langlist = this.getLanguageMasterData();
            var scopeObj = this;
            var languages = [];
            for (var lang in langlist) {
                if (langlist.hasOwnProperty(lang)) {
                    var temp = ([langlist[lang], lang]);
                    languages.push(temp);
                }
            }
            this.view.settings.lbxSelectLanguage.masterData = languages;
            this.view.settings.lbxSelectLanguage.selectedKey = langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)];
            if(this.view.settings.lbxSelectLanguage.selectedKey === langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)]){
            	this.disableButton(this.view.settings.btnChangeLanguageSave);
            }else{
            	this.enableButton(this.view.settings.btnChangeLanguageSave);
            }        
        
        },
      
      disableorEnableSaveButton:function(){
        	var scopeObj= this;
        	var selectedLang = this.view.settings.lbxSelectLanguage.selectedKey;
        	var langlist = this.getLanguageMasterData();
        	if(this.view.settings.lbxSelectLanguage.selectedKey === langlist[scopeObj.getFrontendLanguage(applicationManager.getStorageManager().getStoredItem("langObj").language)]){
            	this.disableButton(this.view.settings.btnChangeLanguageSave);
            }else{
            	this.enableButton(this.view.settings.btnChangeLanguageSave);
            }
        },
      
        /**
         * Method to change the selected language to backend language string
         * @param {String} lang - selected language
         */
        getFrontendLanguage : function(lang){
            var languageData = this.getLanguageMasterData();
            var configManager = applicationManager.getConfigurationManager();
            var langObject = configManager.locale;
            for(var key in langObject) {
               if (langObject.hasOwnProperty(key)) {
                   if(key===lang){
                     return this.getValueFromKey(langObject[key],languageData);
                   }
              }
           }
       },
         /**
         * Method to fetch languages JSON
         */
        getLanguageMasterData: function() {
            return {
               "US English" : "en_US", 
               "UK English" : "en_GB", 
               "Spanish" : "es_ES",
               "German" : "de_DE",
               "French" : "fr_FR"    
            }
        },
        /**
         * Method to fetch language from key
         * @param {String} value - selected language
         * @param {Object} langObject - language Object
         */
        getValueFromKey : function(value,langObject){
          for (var key in langObject) {
            if (langObject.hasOwnProperty(key)) {
               var shortLang = langObject[key];
               if(shortLang===value){
                   return key;
               }
            }
          }
        },
        /**
         * Method to change the selected language to backend language string
         * @param {String} lang - selected language
         */
        getBackendLanguage : function(lang){
             var languageData = this.getLanguageMasterData();
             var configManager = applicationManager.getConfigurationManager();
             var langObject = configManager.locale;
             for(var key in languageData) {
                if (languageData.hasOwnProperty(key)) {
                    if(key===lang){
                      return this.getValueFromKey(languageData[key],langObject);
                    }
               }
            }
        },
          /**
          * Preshow of Profile Management
          */
        preShowProfileManagement:function(){

            this.view.customheader.forceCloseHamburger();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Profile");
            this.setLanguages();
			this.view.settings.flxSecureAccessCode.setVisibility(false);
            this.view.settings.flxChangeLanguageWrapper.setVisibility(false);
          	this.view.settings.flxOption4.setVisibility(false);
         	this.view.settings.btnNameChangeRequest.isVisible = false;
             this.view.settings.btnAddPhoto.isVisible = false;
          applicationManager.getNavigationManager().applyUpdates(this);             
            this.view.forceLayout();
        },

        /**
         * Method to hide all the flex of main body
         * @param {Object} editAddressViewModel - None
         */
        showEditAddressForm: function(editAddressViewModel) {
            var self = this;
            this.showEditAddress();
            this.view.settings.lbxEditType.masterData = editAddressViewModel.addressTypes;
            this.view.settings.lbxEditType.selectedKey = editAddressViewModel.addressTypeSelected;
            this.view.settings.tbxEditAddressLine1.text = editAddressViewModel.addressLine1;
            this.view.settings.tbxEditAddressLine2.text = editAddressViewModel.addressLine2 || "";
            data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", editAddressViewModel.countrySelected, editAddressViewModel.stateNew);
            self.view.settings.lbxEditState.masterData = data.states;
            self.view.settings.lbxEditState.selectedKey = editAddressViewModel.stateSelected;
            data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", editAddressViewModel.stateSelected, editAddressViewModel.cityNew);
            self.view.settings.tbxEditCity.masterData = data.cities;
            self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.citySelected;
            this.view.settings.lbxEditCountry.masterData = editAddressViewModel.countryNew;
            this.view.settings.lbxEditCountry.selectedKey = editAddressViewModel.countrySelected;
            this.view.settings.lbxEditState.selectedKey = editAddressViewModel.stateSelected;
            this.view.settings.tbxEditCity.selectedKey = editAddressViewModel.citySelected;
            this.view.settings.lbxEditCountry.onSelection = function() {
                var data = [];
                var countryId = self.view.settings.lbxEditCountry.selectedKeyValue[0];
                if (countryId == "1") {
                    self.checkUpdateAddressForm();
                    self.view.settings.lbxEditState.masterData = editAddressViewModel.stateNew;
                    self.view.settings.lbxEditState.selectedKey = editAddressViewModel.stateNew[0][0];
                    self.view.settings.tbxEditCity.masterData = editAddressViewModel.cityNew;
                    self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.cityNew[0][0];
                    self.view.settings.tbxEditCity.setEnabled(false);
                    self.view.settings.lbxEditState.setEnabled(false);
                } else {
                    self.view.settings.tbxEditCity.setEnabled(false);
                    self.view.settings.lbxEditState.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", countryId, editAddressViewModel.stateNew);
                    self.view.settings.lbxEditState.masterData = data.states;
                    self.view.settings.lbxEditState.selectedKey = data.states[0][0];
                    var stateId = self.view.settings.lbxEditState.selectedKeyValue[0];
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, editAddressViewModel.cityNew);
                    self.view.settings.tbxEditCity.masterData = data.cities;
                    self.view.settings.tbxEditCity.selectedKey = data.cities[0][0];
                    self.checkUpdateAddressForm();
                }
            };
            this.view.settings.lbxEditState.onSelection = function() {
                var data = [];
                var stateId = self.view.settings.lbxEditState.selectedKeyValue[0];
                if (stateId == "lbl1") {
                    self.checkUpdateAddressForm();
                    self.view.settings.tbxEditCity.masterData = editAddressViewModel.cityNew;
                    self.view.settings.tbxEditCity.selectedKey = editAddressViewModel.cityNew[0][0];
                    self.view.settings.tbxEditCity.setEnabled(false);
                } else {
                    self.view.settings.tbxEditCity.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, editAddressViewModel.cityNew);
                    self.view.settings.tbxEditCity.masterData = data.cities;
                    self.view.settings.tbxEditCity.selectedKey = data.cities[0][0];
                    self.checkUpdateAddressForm();
                }
            };
            this.view.settings.tbxEditCity.onSelection = function() {
                self.checkUpdateAddressForm();
            };
            this.view.settings.tbxEditZipcode.text = editAddressViewModel.zipcode;
          	this.view.settings.lblEditSetAsPreferredCheckBox.skin = editAddressViewModel.isPreferredAddress ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblEditSetAsPreferredCheckBox.text = editAddressViewModel.isPreferredAddress ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.settings.flxEditSetAsPreferred.setVisibility(!editAddressViewModel.isPreferredAddress);
            this.checkUpdateAddressForm();
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAddressSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAddressSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAddressSave.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    var data = this.getUpdateAddressData();
                    data.addressId = editAddressViewModel.addressId;
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAddress(data);
                }.bind(this);
            }
            this.view.settings.forceLayout();
        },


        /**
         * Method to show server error while editing username
         */
        showUpdateUsernameServerError: function() {
            this.view.settings.lblError0.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.settings.flxErrorEditUsername.setVisibility(true);
            this.view.settings.tbxUsername.text = "";
            this.showEditUsername();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while requestind OTP
         */
        showRequestOtpError: function() {
            //warning flex not there
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
      
        
        /**
         * Method to show error while verifying OTP
         */
        showVerifyOtpServerError: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
            this.view.settings.lblErrorSecuritySettingsVerification.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while verifying Question and answer
         */
        showVerifyQuestionAnswerError: function() {
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
            this.view.settings.lblErrorSecurityQuestions.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidAnswers");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show server error while displaying Question and answer
         */
        showVerifyQuestionAnswerServerError: function() {
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
            this.view.settings.lblErrorSecurityQuestions.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching Question and answer
         */
        showGetAnswerSecurityQuestionError: function() {
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error updating the username when service is hit
         */
        showUpdateUsernameError: function(error) {
           if(error.errorMessage && error.errorMessage!=""){
                this.view.settings.lblError0.text = error.errorMessage;
            }else{
                this.view.settings.lblError0.text = error.serverErrorRes.dbpErrMsg;
            }
            this.view.settings.flxErrorEditUsername.setVisibility(true);
            this.view.settings.tbxUsername.text = "";
            this.showEditUsername();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
            this.disableButton(this.view.settings.btnEditUsernameProceed);
        },
        /**
         * Method to show error while displaying Question and answer
         */
        showSecurityQuestionExistsError: function() {
            this.view.settings.lblErrorSecuritySettings.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showEditSecuritySettings();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating Question and answer at the service side
         */
        showUpdateSecurityQuestionError: function() {
            this.view.settings.lblErrorSecuritySettings.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showEditSecuritySettings();
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching Question and answer from backend
         */
        showFetchSecurityQuestionsError: function() {
            this.view.settings.lblErrorSecuritySettings.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
            this.showViews(["flxEditSecuritySettingsWrapper"]);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while fetching the secure access
         */
        showCheckSecurityAccessError: function() {
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while verifying OTP from backend
         */
        showVerifyOtpError: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
            this.view.settings.lblErrorSecuritySettingsVerification.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidCode");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating the secure access options
         */
        showSecureAccessOptionError: function() {
            //error flx not there
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating the password and server is down
         */
        showPasswordExistsServerError: function() {
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
            this.showViews(["flxEditPasswordWrapper"]);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
      
      
      
       showPasswordServerError: function(error) {
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            this.view.settings.lblError1.text = error.dbpErrMsg;
            this.disableButton(this.view.settings.btnEditPasswordProceed);
            this.showViews(["flxEditPasswordWrapper"]);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show error while updating the password and ad password is not same for the existing password entered by user
         */
        showExistingPasswordError: function() {
            this.disableButton(this.view.settings.btnEditPasswordProceed);
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordExists");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
        * Method used to show the invalid credentials error message when trying to change password.
        */
        showWrongPasswordError: function(){
            this.disableButton(this.view.settings.btnEditPasswordProceed);
            this.view.settings.flxErrorEditPassword.setVisibility(true);
            this.view.settings.tbxExistingPassword.text = "";
            this.view.settings.tbxNewPassword.text = "";
            this.view.settings.tbxConfirmPassword.text = "";
            this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.idm.existingPasswordMismatch");
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },

        /**
         * Method to convert question and answer to JSON 
         */
        onSaveAnswerSecurityQuestions: function() {
            var data = [{
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }];
            var quesData = "";
            quesData = this.view.settings.lblAnswerSecurityQuestion1.text;
            data[0].customerAnswer = this.view.settings.tbxAnswers1.text;
            data[0].questionId = this.getQuestionIDForAnswer(quesData);
            quesData = this.view.settings.lblAnswerSecurityQuestion2.text;
            data[1].customerAnswer = this.view.settings.tbxAnswers2.text;
            data[1].questionId = this.getQuestionIDForAnswer(quesData);
            return data;
        },
        /**
         * Method to return the ID of question sent from backend
         * @param {String} quesData- it is the question who's ID is to be searched
         */
        getQuestionIDForAnswer: function(quesData) {
            var qData;
            for (var i = 0; i < this.responseBackend.length; i++) {
                if (quesData === this.responseBackend[i].Question) {
                    qData = this.responseBackend[i].SecurityQuestion_id;
                }
            }
            return qData;
        },
        /**
         * Method to show security questions for verification
         * @param {Object} viewModel- Set of questions to be shown
         */
        showSecurityVerification: function(data) {
            this.responseBackend = data;
            var self = this;
            this.view.settings.lblAnswerSecurityQuestion1.text = data[0].Question;
            this.view.settings.lblAnswerSecurityQuestion2.text = data[1].Question;
            this.view.settings.btnBackAnswerSecurityQuestions.onClick = function() {
                self.showUsernameVerificationByChoice();
            };
            this.showAnswerSecurityQuestions();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to handle the configurations at account settings
         */
        configurationAccountSettings: function() {
            if (applicationManager.getConfigurationManager().enableAccountSettings === "true") {
                this.view.settings.flxAccountSettings.setVisibility(true);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(true);
            } else {
                this.view.settings.flxAccountSettings.setVisibility(false);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(false);
            }

            if (applicationManager.getConfigurationManager().enableDefaultAccounts === "true") {
                this.view.settings.flxSetDefaultAccount.setVisibility(true);
            } else {
                this.view.settings.flxSetDefaultAccount.setVisibility(false);
            }

            if (applicationManager.getConfigurationManager().enablePreferredAccounts === "true") {
                this.view.settings.flxAccountPreferences.setVisibility(true);
            } else {
                this.view.settings.flxAccountPreferences.setVisibility(false);
            }

            if (applicationManager.getConfigurationManager().enablePreferredAccounts === "false" && applicationManager.getConfigurationManager().enableDefaultAccounts === "false") {
                this.view.settings.flxAccountSettings.setVisibility(false);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(false);
            } else {
                this.view.settings.flxAccountSettings.setVisibility(true);
                this.view.settings.flxAccountSettingsSubMenu.setVisibility(true);
            }

        },
        /**
         *AdjustScreen- function to adjust the footer
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0)
                    this.view.flxFooter.top = mainheight + diff + "dp";
                else
                    this.view.flxFooter.top = mainheight + "dp";
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         *setAlertsFlowActions- function to add actions for alerts flow
         */
        setAlertsFlowActions: function() {
            //functions for menu flow
            var scopeObj = this;
            this.view.settings.flxTransactionAndPaymentsAlerts.onClick = function() {
                //alert = "Transactional";
                //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlerts("Transactional");
                scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text = scopeObj.view.settings.flxTransactionAndPaymentsAlerts.lblTransactionAndPaymentsAlerts.text;
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("TransactionAndPaymentsAlerts");
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(scopeObj.view.settings.flxTransactionAndPaymentsAlerts.lblReferenceTPA.text);
            };
            this.view.settings.flxSecurityAlerts.onClick = function() {  
                scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text = scopeObj.view.settings.flxSecurityAlerts.lblSecurityAlerts.text;
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("SecurityAlerts");
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(scopeObj.view.settings.flxSecurityAlerts.lblreferenceSA.text);
            };
            this.view.settings.flxPromotionalAlerts.onClick = function() { 
                scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text = scopeObj.view.settings.flxPromotionalAlerts.lblPromotionalAlerts.text;
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("PromotionalAlerts");
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(scopeObj.view.settings.flxPromotionalAlerts.lblReferencePA.text);
            };
            this.view.settings.flxAccountAlerts.onClick = function() {
                scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text = scopeObj.view.settings.flxAccountAlerts.lblAccountAlerts.text;
                scopeObj.expand(scopeObj.view.settings.flxAlertsSubMenu);
                scopeObj.setSelectedSkin("AccountAlerts");
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(scopeObj.view.settings.flxAccountAlerts.lblReferenceAA.text);
            };
/*
            //security alerts button actions
            this.view.settings.securityAlerts.flxChannel1.onClick = function() {
                scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.securityAlerts);
            };
            this.view.settings.securityAlerts.flxChannel2.onClick = function() {
                scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.securityAlerts);
            };
            this.view.settings.securityAlerts.flxChannel3.onClick = function() {
                scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.securityAlerts);
            };


            this.view.settings.promotionalAlerts.flxChannel1.onClick = function() {
                scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.promotionalAlerts);
            };
            this.view.settings.promotionalAlerts.flxChannel2.onClick = function() {
                scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.promotionalAlerts);
            };
            this.view.settings.promotionalAlerts.flxChannel3.onClick = function() {
                scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.promotionalAlerts);
            };
            this.view.settings.flxAccountAlerts.onClick = function() {
                alert = "Account";
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlerts("Account");
            };
        */
               },



        /**
         *toggleChannel1Checkboxes- Method to toggle the checkbox on click of channels
         * @param {String} alertWidget - ID of the Channel
         *
        toggleChannel1Checkboxes: function(alertWidget, alert) {
            if (alertWidget.lblCheckBoxChannel1.text === "C")
                alertWidget.lblCheckBoxChannel1.text = "D";
            else
                alertWidget.lblCheckBoxChannel1.text = "C";
            var newData = alertWidget.segAlerts.data;
            var alertData = alert.alertTypes[0].alerts;
            for (var i = 0; i < alertData.length; i++) {
                if ((alertData[i].canSmsBeSelected === "true" || alertData[i].canSmsBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                    newData[i].lblCheckBoxChannel1.text = alertWidget.lblCheckBoxChannel1.text;
                }
            }
            if(kony.application.getCurrentBreakpoint() === 1024){
                for(var j=0;j<newData.length;j++){
                newData[j].template = "flxAlertsTablet";
             }
            }
            alertWidget.segAlerts.setData(newData);
        },
        /**
         *toggleChannel2Checkboxes- Method to toggle the checkbox on click of channels
         * @param {String} alertWidget - ID of the Channel
         *
        toggleChannel2Checkboxes: function(alertWidget, alert) {
            if (alertWidget.lblCheckBoxChannel2.text === "C")
                alertWidget.lblCheckBoxChannel2.text = "D";
            else
                alertWidget.lblCheckBoxChannel2.text = "C";
            var newData = alertWidget.segAlerts.data;
            var alertData = alert.alertTypes[0].alerts;
            for (var i = 0; i < alertData.length; i++) {
                if ((alertData[i].canEmailBeSelected === "true" || alertData[i].canEmailBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                    newData[i].lblCheckBoxChannel2.text = alertWidget.lblCheckBoxChannel2.text;
                }
            }
            if(kony.application.getCurrentBreakpoint() === 1024){
                for(var j=0;j<newData.length;j++){
                newData[j].template = "flxAlertsTablet";
             }
            }
            alertWidget.segAlerts.setData(newData);
        },
        /**
         *toggleChannel3Checkboxes- Method to toggle the checkbox on click of channels
         * @param {String} alertWidget - ID of the Channel
         *
        toggleChannel3Checkboxes: function(alertWidget, alert) {
            if (alertWidget.lblCheckBoxChannel3.text === "C")
                alertWidget.lblCheckBoxChannel3.text = "D";
            else
                alertWidget.lblCheckBoxChannel3.text = "C";
            var newData = alertWidget.segAlerts.data;
            var alertData = alert.alertTypes[0].alerts;
            for (var i = 0; i < alertData.length; i++) {
                if ((alertData[i].canPushBeSelected === "true" || alertData[i].canPushBeSelected === true) && (alertData[i].canBeSelected === "true" || alertData[i].canBeSelected === true) && (alertWidget.id === "securityAlerts" || newData[i].lblAlertsStatusCheckBox.text === "C")) {
                    newData[i].lblCheckBoxChannel3.text = alertWidget.lblCheckBoxChannel3.text;
                }
            }
            if(kony.application.getCurrentBreakpoint() === 1024){
                for(var j=0;j<newData.length;j++){
                newData[j].template = "flxAlertsTablet";
             }
            }
            alertWidget.segAlerts.setData(newData);
        },
*/
        /**
         * Method to navigate to terms and Conditions
         */
        NavToTermsAndConditions: function() {
            var currForm = this.view;
            currForm.flxTermsAndConditions.height = currForm.flxHeader.frame.height + currForm.flxContainer.frame.height + currForm.flxFooter.frame.height;
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.lblTermsAndConditions.setFocus(true);
        },
        /**
         * Method to close Terms and Conditions
         */
        CloseTermsAndConditions: function() {
            this.view.flxTermsAndConditions.setVisibility(false);
        },
        /**
         * Method to enable/disable the terms and condition button
         */
        SaveTermsAndConditions: function() {
            if (this.view.lblTCContentsCheckbox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                this.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                this.enableButton(this.view.settings.btnEditAccountsSave);
            } else {
                this.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.disableButton(this.view.settings.btnEditAccountsSave);
            }
            this.view.flxTermsAndConditions.setVisibility(false);
        },

        /**
         * Method to change the the progress bar state
         * @param {Object} isLoading- Consists of the loading indicator status.
         */
        changeProgressBarState: function(isLoading) {
            if (isLoading) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
        },

        updatePhoneNumber: function(editPhoneViewModel, formData) {
            var servicesToSend = [];
           /* editPhoneViewModel.services.forEach(function(account) {
                if (!account.selected && formData.services[account.id]) {
                    servicesToSend.push({
                        id: account.id,
                        selected: true
                    });
                } else if (account.selected && !formData.services[account.id]) {
                    servicesToSend.push({
                        id: account.id,
                        selected: false
                    });
                }

            });
            formData.services = servicesToSend;*/
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editPhoneNumber(editPhoneViewModel.id, formData);
        },

        /**
         * Method to validate the phone umber after editing
         */
        validateEditPhoneNumberForm: function() {
            var phoneData = this.getEditPhoneFormData();
            if (!applicationManager.getValidationUtilManager().isValidPhoneNumber(phoneData.phoneNumber) || phoneData.phoneNumber.length != 10) {
                this.view.settings.flxError.setVisibility(true);
                this.view.settings.lblError.text = kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber");
                return false;
            } else {
                this.view.settings.flxError.setVisibility(false);
                return true;
            }
        },
        /**
         * Method to show error while updating email
         * @param {String} errorMessage- Message to be shown
         */
        showEmailError: function(errorMessage) {
            this.view.settings.flxErrorAddNewEmail.setVisibility(true);
            this.view.settings.CopylblError0e8bfa78d5ffc48.text = errorMessage;
        },
        /**
         * Method to error while updating the edit email scenario
         * @param {String} errorMessage- Message to be shown
         */
        showEditEmailError: function(errorMessage) {
            this.view.settings.flxErrorEditEmail.setVisibility(false);
            this.view.settings.CopylblError0e702a95b68d041.text = errorMessage;
        },

        /**
         * Method to show data after updating the New Address scenario
         */
        getUpdateAddressData: function() {
            return {
                addrLine1: this.view.settings.tbxEditAddressLine1.text.trim(),
                addrLine2: this.view.settings.tbxEditAddressLine2.text.trim(),
                Country_id: this.view.settings.lbxEditCountry.selectedKey,
                Region_id: this.view.settings.lbxEditState.selectedKey,
                City_id: this.view.settings.tbxEditCity.selectedKey,
                ZipCode: this.view.settings.tbxEditZipcode.text.trim(),
                isPrimary: this.view.settings.lblEditSetAsPreferredCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                Addr_type: this.view.settings.lbxEditType.selectedKey
            };
        },
        /**
         * Method to show data related to New Email scenario
         */
        getNewEmailData: function() {
            return {
                value: this.view.settings.tbxEmailId.text,
                isPrimary: this.view.settings.lblMarkAsPrimaryEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
            };
        },
        /**
         * Method to reset the fields while adding email
         */
        resetAddEmailForm: function() {
            this.view.settings.tbxEmailId.text = "";
          	this.view.settings.lblMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblMarkAsPrimaryEmailCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.settings.flxErrorAddNewEmail.setVisibility(false);
            this.disableButton(this.view.settings.btnAddEmailIdAdd);
        },
        /**
         * Method to enable/disable the button of terms and condition
         */
        closeTermsAndConditions: function() {
            this.view.flxTermsAndConditions.isVisible = false;
        },
        /**
         * Method to toggle the buttons of terms and condition
         */
        toggleTC: function() {
            CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
            this.enableButton(this.view.btnSave);
        },
        /**
         * Method on Click of button Save of TnC Popup
         */
        onSaveTnC: function() {
            var isEnabledTnCPop = CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox);
            var isEnabledTnC = CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox);
            if (isEnabledTnC !== isEnabledTnCPop) {
                CommonUtilities.toggleFontCheckbox(this.view.settings.lblTCContentsCheckbox);
            }
            if (isEnabledTnCPop === true) {
                this.enableButton(this.view.settings.btnEditAccountsSave);
            } else {
                this.disableButton(this.view.settings.btnEditAccountsSave);
            }
            this.view.flxTermsAndConditions.isVisible = false;
        },
        /**
         * Method to manupulate the UI of profile management on post show
         */

        postShowProfileManagement: function() {
            var scopeObj = this;
            this.view.flxTCContentsCheckbox.onClick = this.toggleTC;
            this.view.flxClose.onClick = this.closeTermsAndConditions;
            this.view.btnCancel.onClick = this.closeTermsAndConditions;
            this.view.settings.lbxSelectLanguage.onSelection = this.disableorEnableSaveButton;
            this.view.btnSave.onClick = this.onSaveTnC;
            if (kony.os.deviceInfo().screenHeight >= "900") {
                var mainheight = 0;
                var screenheight = kony.os.deviceInfo().screenHeight;
                mainheight = this.view.customheader.frame.height;
                mainheight = mainheight + this.view.flxContainer.frame.height;
                var diff = screenheight - mainheight;

                if (diff > 0)
                    this.view.flxFooter.top = diff + "dp";
            }
            this.view.settings.btnChangeLanguageSave.onClick = function() {
                var langSelected = scopeObj.view.settings.lbxSelectLanguage.selectedKeyValue[1];
                scopeObj.view.CustomChangeLanguagePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.changeLanguageMessage") + " " + langSelected + "?";
                scopeObj.view.flxChangeLanguage.isVisible = true;
                scopeObj.view.CustomChangeLanguagePopup.flxCross.onClick = function(){
                    scopeObj.view.flxChangeLanguage.isVisible = false;
                    scopeObj.view.forceLayout();
                }
                scopeObj.view.flxChangeLanguage.height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height + scopeObj.view.flxFooter.frame.height + "dp";
                scopeObj.view.forceLayout();
              };
            this.view.CustomChangeLanguagePopup.btnNo.onClick = function() {
                scopeObj.view.flxChangeLanguage.isVisible = false;
                scopeObj.view.forceLayout();
            };
            this.view.CustomChangeLanguagePopup.btnYes.onClick = function() {
                var localeCode = scopeObj.view.settings.lbxSelectLanguage.selectedKey;
                var langSelected = scopeObj.view.settings.lbxSelectLanguage.selectedKeyValue[1];
                kony.i18n.setCurrentLocaleAsync(localeCode, function() {
                    applicationManager.getStorageManager().setStoredItem("langObj", {language:scopeObj.getBackendLanguage(langSelected)});
                    applicationManager.getConfigurationManager().setLocaleAndDateFormat({"data":{}});
                    applicationManager.getNavigationManager().navigateTo("frmLoginLanguage");
                    scopeObj.showViews(["flxChangeLanguageWrapper"]);
                    scopeObj.setSelectedSkin("ChangeLanguage");
                    scopeObj.view.customheader.customhamburger.forceInitializeHamburger();
                }, function() {});
                scopeObj.view.flxChangeLanguage.isVisible = false;
                scopeObj.showViews(["flxChangeLanguageWrapper"]);
                scopeObj.setSelectedSkin("ChangeLanguage");
                scopeObj.view.forceLayout();
            };
            this.view.customheader.imgKony.setFocus(true);
            this.view.settings.imgAddRadioBtnUS.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            this.view.settings.imgAddRadioBtnInternational.src = ViewConstants.IMAGES.ICON_RADIOBTN;
          
             // UI code
            this.view.flxDeletePopUp.height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height+ "dp";
            // Checking if it is External Account edit as there is no service call hence because of async
            // nature of js init preshow and postshow gets called at later stage tha updateUI api.
            var extAccountData= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getEditExternalAccount();
            if(extAccountData && extAccountData.flow==='editExternalAccounts'){
                this.goToEditExternalAccounts(extAccountData.data);
            }
            this.AdjustScreen();
        },
        /**
         * Method to show a particular view among all
         * @param {String} views - ID of the view to be shown
         */
        showViews: function(views) {         
          if( this.currentVisiblePage ) {
            this.view.settings[ this.currentVisiblePage ].isVisible = false;
          }
          if ( views && views.constructor === Array ) {
            this.view.settings[ views[0] ].isVisible = true;
            this.currentVisiblePage = views[0];
          }

          this.view.settings.forceLayout();
          this.AdjustScreen();
        },

        showNameChangeRequest: function() {
            this.showViews(["flxNameChangeRequestWrapper"]);
        },

        /**
        * Method used to set the guidelines for name change request screen.
        */
        setNameChangeRequestGuidelines: function() {
            this.view.settings.lblRules1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelinesNameRequest");
            this.view.settings.lblRule1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest1");
            this.view.settings.lblRule2.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest2");
            this.view.settings.lblRule3.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.GuidelineNameRequest3");
            this.view.settings.flxRule4.setVisibility(false);
        },


        /**
         * Method to Check Dots in Username Entered by the User
         * @param {String} enteredUserName - username entered by the User
         */
        checkDot: function(enteredUserName) {
            var count = 0;
            if (enteredUserName.charAt(0) === "." || enteredUserName.charAt(enteredUserName.length - 1) === ".") return false;
            var index = enteredUserName.indexOf(".");
            for (var i = index + 1; i < enteredUserName.length - 1; i++) {
                if (enteredUserName[i] === ".") {
                    return false;
                }
            }
            return true;
        },
        /**
         * Method to check whether the username is valid or not
         * @param {String} enteredUserName - username entered by the User
         */
       isUserNameValid: function(enteredUserName) {
            var validationUtility = applicationManager.getValidationUtilManager();
          var isValidUsername =  validationUtility.isUsernameValidForPolicy(enteredUserName);
            if (isValidUsername) {
                    return true;
            }
            return false;
        },
        /**
         * Method to Check whether the password is valid or not
         */
       isPasswordValid: function(enteredPassword) {
           var validationUtility = applicationManager.getValidationUtilManager();
          var isValidPassword =  validationUtility.isPasswordValidForPolicy(enteredPassword);
            var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            if (isValidPassword && !this.hasConsecutiveDigits(enteredPassword) && enteredPassword !== userName && this.view.settings.tbxExistingPassword.text.length > 0) {
                return true;
            }
            return false;
       },
        /**
         * Method to Check whether the entered text has consecutive digits or not
         * @param {Int} input - field entered by the User
         */
        hasConsecutiveDigits: function(input) {
            var i;
            var count = 0;
            for (i = 0; i < input.length; i++) {
                // alert(abc[i]);
                if (input[i] >= 0 && input[i] <= 9)
                    count++;
                else
                    count = 0;
                if (count === 9)
                    return true;
            }
            return false;
        },

        /**
         * Method to Check whether the username is valid or not and if valid then enable/disable the button
         */
        ValidateUsername: function() {
            if (this.isUserNameValid(this.view.settings.tbxUsername.text)) {
                this.enableButton(this.view.settings.btnEditUsernameProceed);
               this.view.settings.flxErrorEditUsername.setVisibility(false);
            } else {
                this.disableButton(this.view.settings.btnEditUsernameProceed);
            }
        },


        showEditUsername: function() {
            this.showViews(["flxEditUsernameWrapper"]);
        },
        

        /**
         * Method to verify OTP by hitting backend
         */
        verifyOtp: function() {
            if (this.view.settings.tbxEnterOTP.text !== "" && this.view.settings.tbxEnterOTP.text != null) {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.verifyOtp(this.view.settings.tbxEnterOTP.text);
            }
        },
        showAddNewEmail: function() {
            this.showViews(["flxAddNewEmailWrapper"]);
        },


        showEditAddress: function() {
            this.showViews(["flxEditAddressWrapper"]);
        },
        showAccounts: function() {
            this.showViews(["flxAccountsWrapper"]);
        },
        showEditAccounts: function() {
            this.showViews(["flxEditAccountsWrapper"]);
        },

        showAcknowledgement: function() {
            this.showViews(["flxAcknowledgementWrapper"]);
        },

        /**
         * Method to Update the next step according to the the flag UPDATE
         */
        updateRequirements: function() {
            this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
            this.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
            var self = this;
            FormControllerUtility.showProgressBar(self.view);
            if (update === "password")
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updatePassword(self.view.settings.tbxExistingPassword.text,self.view.settings.tbxNewPassword.text);
            else if (update === "username")
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateUsername(self.view.settings.tbxUsername.text);
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp(self.onSaveSecurityQuestions());

        },
        /**
         * Method to Update the acknowledgment according to the the flag UPDATE
         */
        acknowledge: function(viewModel) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (update === "password") {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    scopeObj.showUsernameAndPassword();
                    scopeObj.setSelectedSkin("UsernameAndPassword");
                }
                scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordUpdatedSuccessfully");
            } else if (update === "username") {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    scopeObj.showUsernameAndPassword();
                    scopeObj.setSelectedSkin("UsernameAndPassword");
                }
                kony.mvc.MDAApplication.getSharedInstance().appContext.username = this.view.settings.tbxUsername.text;
                scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess");
            } else {
                this.view.settings.btnAcknowledgementDone.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                    scopeObj.setSelectedSkin("SecurityQuestions");
                }
                scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.YourSecurityQuestionswereupdatedSuccessfully");
            }
            scopeObj.showAcknowledgement();
            update = "";
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },
        showOTP: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = false;
        },

        /**
         * Method to show secure access options
         */
        showSecureAccessOptions: function(viewModel) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (viewModel.isPhoneEnabled === "true"){
                scopeObj.view.settings.lblPhoneUnchecked.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
              	scopeObj.view.settings.lblPhoneUnchecked.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
            else{
                scopeObj.view.settings.lblPhoneUnchecked.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
              	scopeObj.view.settings.lblPhoneUnchecked.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
            if (viewModel.isEmailEnabled === "true"){
                scopeObj.view.settings.lblemailunchecked.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
              	scopeObj.view.settings.lblemailunchecked.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
            else{
                scopeObj.view.settings.lblemailunchecked.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
              	scopeObj.view.settings.lblemailunchecked.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
            scopeObj.view.settings.flxPhoneUnchecked.onClick = function() {};
            scopeObj.view.settings.flxemailunchecked.onClick = function() {};
            scopeObj.view.settings.flxPhoneUnchecked.setEnabled = false;
            scopeObj.view.settings.flxemailunchecked.setEnabled = false;
            scopeObj.view.settings.lblPhoneUnchecked.setEnabled = false;
            scopeObj.view.settings.lblemailunchecked.setEnabled = false;
            scopeObj.setSelectedSkin("SecureAccessCode");
            scopeObj.collapseAll();
            scopeObj.expandWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            scopeObj.showSecureAccessCode();
            scopeObj.view.forceLayout();
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },
        /**
         * Method to handle the Enter OTP flow
         */
        enterOtp: function() {
            this.setFlowActions();
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.setEnabled(false);
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = ViewConstants.SKINS.BUTTON_BLOCKED;
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.hoverSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
            this.resendOtpTimer = setTimeout(
                function() {
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.setEnabled(true);
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = ViewConstants.SKINS.NORMAL;
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.hoverSkin = ViewConstants.SKINS.HOVER;
                }, 10000);
            scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.Enrollnow.EnterOTPMsg");
            scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
            scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
            scopeObj.view.settings.tbxEnterOTP.text = "";
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.toolTip = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
            scopeObj.view.settings.btnSecuritySettingVerificationSend.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
            scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
            scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
            scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                scopeObj.hideOTP();
                scopeObj.verifyOtp();
            };
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = CommonUtilities.disableButtonActionForCSRMode();
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
                    if (update === "username" || update === "password") {
                        scopeObj.showUsernameAndPassword();
                        scopeObj.setSelectedSkin("UsernameAndPassword");
                        scopeObj.view.forceLayout();
                    } else {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                        scopeObj.setSelectedSkin("SecurityQuestions");
                        scopeObj.view.forceLayout();
                    }
                };
            }
            scopeObj.showSecuritySettingVerification();
            scopeObj.disableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
            if (scopeObj.view.settings.btnSecuritySettingVerificationSend.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
                scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
                    FormControllerUtility.showProgressBar(scopeObj.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp();
                };
            } 
            FormControllerUtility.hideProgressBar(scopeObj.view);
            scopeObj.view.forceLayout();
        },
        /**
         * Method to check the validation of username
         */
        checkUsername: function() {
            var self = this;
            if (this.view.settings.tbxUsername.text.length >= 7 && this.isUserNameValid(this.view.settings.tbxUsername.text) && (this.view.settings.tbxUsername.text !== kony.mvc.MDAApplication.getSharedInstance().appContext.username))
                this.enableButton(self.view.settings.btnEditUsernameProceed);
            else
                this.disableButton(self.view.settings.btnEditUsernameProceed);
        },
        /**
         * Method to check the validation of OTP
         */
        checkOTP: function() {
            if (CommonUtilities.isCSRMode())
                CommonUtilities.disableButtonActionForCSRMode();
            else {
                if (this.view.settings.tbxEnterOTP.text.length >= 6)
                    this.enableButton(this.view.settings.btnSecuritySettingVerificationSend);
            }
        },
        
        showUsernameVerification: function() {
            this.showViews(["flxUsernameVerificationWrapper2"]);
        },
        showAnswerSecurityQuestions: function() {
            this.showViews(["flxAnswerSecurityQuestionsWrapper"]);
        },
        showSecureAccessCode: function() {
            this.showViews(["flxSecureAccessCodeWrapper"]);
            flag = 1;
        },

        /**
         * Method to enable/disable button based on the email entered
         */
        checkAddEmailForm: function() {
            if (applicationManager.getValidationUtilManager().isValidEmail(this.view.settings.tbxEmailId.text)) {
                this.enableButton(this.view.settings.btnAddEmailIdAdd);
            } else {
                this.disableButton(this.view.settings.btnAddEmailIdAdd);

            }
        },

        /**
         * Method to set the validation function while entering email
         */
        setAddEmailValidationActions: function() {
            var scopeObj = this;
            this.disableButton(this.view.settings.btnAddEmailIdAdd);
            this.view.settings.tbxEmailId.onKeyUp = function() {
                scopeObj.checkAddEmailForm();
            };
        },
        /**
         * Method to set the validation function while editing email
         */
        setUpdateEmailValidationActions: function() {
            var scopeObj = this;
            this.disableButton(this.view.settings.btnEditEmailIdSave);
            this.view.settings.tbxEditEmailId.onKeyUp = function() {
                scopeObj.checkUpdateEmailForm();
            };
        },

        /**
         * Method to validate the edited address
         */
        checkUpdateAddressForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                var addAddressFormData = this.getUpdateAddressData();
                if (addAddressFormData.addressLine1 === '') {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.zipcode === '') {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.city === '') {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.Country_id === "1") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.Region_id === "lbl1") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else if (addAddressFormData.City_id === "lbl2") {
                    this.disableButton(this.view.settings.btnEditAddressSave);
                } else {
                    this.enableButton(this.view.settings.btnEditAddressSave);
                }
            }
        },


        /**
         * Method to assign validation action on the address fields
         */
        setNewAddressValidationActions: function() {
            this.disableButton(this.view.settings.btnAddNewAddressAdd);
            this.view.settings.tbxAddressLine1.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.tbxAddressLine2.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.tbxZipcode.onKeyUp = this.checkNewAddressForm.bind(this);
            this.view.settings.tbxCity.onKeyUp = this.checkNewAddressForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        setUpdateAddressValidationActions: function() {
            this.disableButton(this.view.settings.btnEditAddressSave);
            this.view.settings.tbxEditAddressLine1.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.tbxEditAddressLine2.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.tbxEditZipcode.onKeyUp = this.checkUpdateAddressForm.bind(this);
            this.view.settings.tbxEditCity.onKeyUp = this.checkUpdateAddressForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        getBase64: function(file, successCallback) {
            var reader = new FileReader();
            reader.onloadend = function() {
                successCallback(reader.result);
            };
            reader.readAsDataURL(file);
        },
        /**
         * Method to assign validation action on the new phone entered fields
         */
        setAddNewPhoneValidationActions: function() {
            this.view.settings.tbxAddPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
        },
        /**
         * Method to assign validation action while editing phone number  fields
         */
        setEditNewPhoneValidationActions: function() {
            this.view.settings.tbxPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
        },
        /**
         * Method to assign validation action on the edit address fields
         */
        selectedFileCallback: function(events, files) {
            var scopeObj = this;
            this.getBase64(files[0].file, function(base64String) {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.userImageUpdate(base64String);
            });
        },
        /**
         * Method to assign the general flow action throughout the profile  module
         */
        setFlowActions: function() {
            var scopeObj = this;
            var self = this;
            this.view.settings.lbxQuestion1.onSelection = this.setQuestionForListBox1.bind(this);
            this.view.settings.lbxQuestion2.onSelection = this.setQuestionForListBox2.bind(this);
            this.view.settings.lbxQuestion3.onSelection = this.setQuestionForListBox3.bind(this);
            this.view.settings.lbxQuestion4.onSelection = this.setQuestionForListBox4.bind(this);
            this.view.settings.lbxQuestion5.onSelection = this.setQuestionForListBox5.bind(this);
            this.view.settings.tbxAnswer1.onBeginEditing = this.onEditingAnswer1.bind(this);
            this.view.settings.tbxAnswer2.onBeginEditing = this.onEditingAnswer2.bind(this);
            this.view.settings.tbxAnswer3.onBeginEditing = this.onEditingAnswer3.bind(this);
            this.view.settings.tbxAnswer4.onBeginEditing = this.onEditingAnswer4.bind(this);
            this.view.settings.tbxAnswer5.onBeginEditing = this.onEditingAnswer5.bind(this);

            this.view.settings.tbxAnswer1.onEndEditing = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer2.onEndEditing = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer3.onEndEditing = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer4.onEndEditing = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer5.onEndEditing = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer1.onKeyUp = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer2.onKeyUp = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer3.onKeyUp = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer4.onKeyUp = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.tbxAnswer5.onKeyUp = function(){
                self.enableSecurityQuestions(self.view.settings.tbxAnswer1.text, self.view.settings.tbxAnswer2.text, self.view.settings.tbxAnswer3.text, self.view.settings.tbxAnswer4.text, self.view.settings.tbxAnswer5.text);
            };
            this.view.settings.btnEditSecuritySettingsCancel.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                    self.setSelectedSkin("SecurityQuestions");
                };
            this.view.settings.btnEditSecuritySettingsProceed.onClick = this.onProceedSQ.bind(this);
            this.view.settings.tbxEnterOTP.onKeyUp = this.checkOTP.bind(this);
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (!CommonUtilities.isCSRMode()) {
                this.setAddEmailValidationActions();
                this.setUpdateEmailValidationActions();
                this.setNewAddressValidationActions();
                this.setUpdateAddressValidationActions();
                this.setAddNewPhoneValidationActions();
                this.setEditNewPhoneValidationActions();
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnAddPhoto.setEnabled(false);
                this.view.settings.btnAddPhoto.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnAddPhoto.onClick = function() {
                    var config = {
                        selectMultipleFiles: true,
                        filter: ["image/png", "image/jpeg"]
                    };
                    kony.io.FileSystem.browse(config, this.selectedFileCallback.bind(this));
                }.bind(this);
            }
            //Menu flow
            this.view.settings.flxProfile.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserProfile();
            };
            this.view.settings.flxPhone.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
            };
            this.view.settings.flxEmail.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
            };
            this.view.settings.flxAddress.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserAddresses();
            };
            this.view.settings.flxUsernameAndPassword.onClick = function() {
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
                scopeObj.showUsernameAndPassword();
                scopeObj.setSelectedSkin("UsernameAndPassword");
            };
            this.view.settings.flxChangeLanguage.onClick = function() {
                scopeObj.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
                scopeObj.showViews(["flxChangeLanguageWrapper"]);
                scopeObj.setSelectedSkin("ChangeLanguage");
                scopeObj.view.forceLayout();
            };
            this.view.settings.flxSecurityQuestions.onClick = function() {
                scopeObj.showSecurityQuestionsScreen();
            };
            this.view.settings.flxSecureAccessCode.onClick = this.showSecureAccessSettings.bind(this);
            this.view.settings.flxAccountPreferences.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getPreferredAccounts();
            };
            this.view.settings.flxSetDefaultAccount.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.showDefaultScreen();
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getDefaultUserProfile();
            };

            this.view.settings.btnNameChangeRequest.onClick = function() {
                scopeObj.setNameChangeRequestGuidelines();
                scopeObj.showNameChangeRequest();
            };
            
            this.view.settings.btnAddNewNumber.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddPhoneNumberView();
            };
            this.view.settings.btnAddPhoneNumberCancel.onClick = function() {
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnAddPhoneNumberSave.onClick = function() {
                //Add code for Saving the Phone Number
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnEditPhoneNumberCancel.onClick = function() {
                scopeObj.showPhoneNumbers();
            };
            this.view.settings.btnEditPhoneNumberSave.onClick = function() {
                //Add code for saving the edited Phone Number
                scopeObj.showPhoneNumbers();
            };
            this.view.flxDeleteClose.onClick = function() {
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.btnDeleteNo.onClick = function() {
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            };

            this.view.settings.flxCheckBox3.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblCheckBox3);
            };
            this.view.settings.flxCheckBox4.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblCheckBox4);
            };

            this.view.settings.flxAddCheckBox3.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblAddCheckBox3);
            };
            this.view.settings.flxAddCheckBox4.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblAddCheckBox4);
            };
            this.view.settings.flxRadioBtnInternational.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnInternational, scopeObj.view.settings.imgRadioBtnUS);
            };
            this.view.settings.flxRadioBtnUS.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnUS, scopeObj.view.settings.imgRadioBtnInternational);
            };
            this.view.settings.flxAddRadioBtnInternational.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnInternational, scopeObj.view.settings.imgAddRadioBtnUS);
            };
            this.view.settings.flxAddRadioBtnUS.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnUS, scopeObj.view.settings.imgAddRadioBtnInternational);
            };

            //Email flow
            this.view.settings.btnAddNewEmail.onClick = function() {
                scopeObj.resetAddEmailForm();
                scopeObj.showAddNewEmail();
            };
            this.view.settings.btnAddEmailIdCancel.onClick = function() {
                scopeObj.showEmail();
            };
            this.view.settings.btnAddEmailIdAdd.onClick = function() {
                //add code to ADD new email
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveEmail(scopeObj.getNewEmailData());
                // scopeObj.showEmail();
            };

            this.view.settings.btnEditAddNewEmail.onClick = function() {
                scopeObj.showAddNewEmail();
            };
            this.view.settings.btnEditEmailIdCancel.onClick = function() {
                scopeObj.showEmail();
            };

            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox);
            };
            this.view.settings.flxMarkAsPrimaryEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblMarkAsPrimaryEmailCheckBox);
            };

            //Address flow
            this.view.settings.btnAddNewAddress.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAddNewAddressView();
            };

            this.view.settings.btnAddNewAddressCancel.onClick = function() {
                scopeObj.showAddresses();
            };
            this.view.settings.btnAddNewAddressAdd.onClick = function() {
                //write code to ADD new Address
                scopeObj.showAddresses();
            };
            this.view.settings.btnEditAddressCancel.onClick = function() {
                scopeObj.showAddresses();
            };
            this.view.settings.btnEditAddressSave.onClick = function() {
                //Add code to save new address
                scopeObj.showAddresses();
            };
            this.view.settings.flxSetAsPreferredCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblSetAsPreferredCheckBox);
            };
            this.view.settings.flxEditSetAsPreferredCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblEditSetAsPreferredCheckBox);
            };


            //Username & Password
            this.view.settings.btnUsernameEdit.onClick = function() {
               FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getUsernameRulesAndPolicies();
                var userPreferencesManager = applicationManager.getUserPreferencesManager();
                scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                scopeObj.view.settings.tbxUsername.text = userPreferencesManager.getCurrentUserName();
                scopeObj.view.settings.placeholder = "";
                if (CommonUtilities.isCSRMode()) {
                    scopeObj.view.settings.btnEditUsernameProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    scopeObj.view.settings.btnEditUsernameProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                    scopeObj.view.settings.tbxUsername.onKeyUp = function() {
                        scopeObj.ValidateUsername();
                    }.bind(this);
                    scopeObj.view.settings.tbxUsername.onEndEditing = function() {
                        if (scopeObj.isUserNameValid(scopeObj.view.settings.tbxUsername.text)) {
                            scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                        } else {
                            scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
                            scopeObj.view.settings.flxErrorEditUsername.setVisibility(true);
                            scopeObj.view.settings.lblError0.text = kony.i18n.getLocalizedString("i18n.enrollNow.validUsername");
                            scopeObj.view.forceLayout();
                        }
                    }.bind(this);
                        scopeObj.view.settings.btnEditUsernameProceed.onClick = function() {
                            if (scopeObj.isUserNameValid(scopeObj.view.settings.tbxUsername.text)) {
                                scopeObj.enableButton(scopeObj.view.settings.btnEditUsernameProceed);
                                scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
                                update = "username";
                                scopeObj.updateRequirements();
                            } else {
                                scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
                                scopeObj.view.settings.flxErrorEditUsername.setVisibility(true);
                                scopeObj.view.settings.lblError0.text = kony.i18n.getLocalizedString("i18n.enrollNow.validUsername");
                                scopeObj.view.forceLayout();
                            }
                        }.bind(this);
                }
                scopeObj.showEditUsername();
            };
            this.view.settings.btnPasswordEdit.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getPasswordRulesAndPolicies();
               	if (CommonUtilities.isCSRMode()) {
                     scopeObj.view.settings.btnEditPasswordProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                     scopeObj.view.settings.btnEditPasswordProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
                }else{
                   scopeObj.view.settings.tbxNewPassword.onEndEditing = function() {
                   if(scopeObj.isPasswordValid(scopeObj.view.settings.tbxNewPassword.text)){
                        scopeObj.enableButton(scopeObj.view.settings.btnEditPasswordProceed);
                       scopeObj.view.settings.flxErrorEditPassword.setVisibility(false);
                       scopeObj.view.forceLayout();
                     }else{
                          scopeObj.view.settings.flxErrorEditPassword.setVisibility(true);
                          scopeObj.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.enrollNow.validPassword");
                          scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
                          scopeObj.view.forceLayout();
                     }
                }.bind(this);
                scopeObj.view.settings.tbxConfirmPassword.onKeyUp = function() {
                    scopeObj.ValidatePassword();
                }.bind(this);
                    scopeObj.view.settings.btnEditPasswordProceed.onClick = function() {
                    scopeObj.view.settings.flxErrorEditPassword.setVisibility(false);
                    update = "password";
                    FormControllerUtility.showProgressBar(scopeObj.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkExistingPassword(scopeObj.view.settings.tbxExistingPassword.text);
            }.bind(this);
                }
               scopeObj.view.settings.tbxExistingPassword.text = "";
                scopeObj.view.settings.tbxNewPassword.text = "";
                scopeObj.view.settings.tbxConfirmPassword.text = "";
                scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
                scopeObj.showEditPassword();
            };
            this.view.settings.btnEditUsernameCancel.onClick = function() {
                scopeObj.showUsernameAndPassword();
            };
            this.view.settings.btnEditPasswordCancel.onClick = function() {
                scopeObj.showUsernameAndPassword();
            };
          
            this.view.settings.btnUsernameVerification2Proceed.onClick = function() {
                if (scopeObj.view.settings.btnUsernameVerification2Proceed.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
                    scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess");
                    scopeObj.showAcknowledgement();
                } else {
                    scopeObj.view.settings.btnUsernameVerification2Cancel.text = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
                    scopeObj.view.settings.btnUsernameVerification2Proceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
                    scopeObj.disableButton(scopeObj.view.settings.btnUsernameVerificationSend);
                    scopeObj.view.settings.tbxEnterOTP.text = "";
                    scopeObj.view.settings.lblUsernameVerification2SendingYouTheOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EnterOTPsentOnYourMobilePhone");
                    scopeObj.view.settings.lblUsernameVerificationtext2.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
                    scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
                }
            };
            this.view.settings.btnUsernameVerificationCancel.onClick = function() {
                scopeObj.view.settings.tbxExistingPassword.text = "";
                scopeObj.view.settings.tbxNewPassword.text = "";
                scopeObj.view.settings.tbxConfirmPassword.text = "";
                scopeObj.view.settings.tbxUsername = "";
                scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showProfileSettings();
            };
            this.view.settings.btnUsernameVerificationProceed.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                scopeObj.showOTPAction();
                scopeObj.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
                scopeObj.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
                if (scopeObj.view.settings.imgUsernameVerificationcheckedRadio.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
                    scopeObj.view.settings.tbxEnterOTP.onKeyUp = function() {
                        scopeObj.checkOTP();
                    };
                    scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
                    scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
                    scopeObj.view.settings.flxOTPtextbox.setVisibility(false);
                    scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                    scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.common.send");
                    scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                        scopeObj.assignOTPSendAction();
                        //FormControllerUtility.hideProgressBar(scopeObj.view);
                    };
                    scopeObj.enableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
                    //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
                    scopeObj.showViews(["flxSecuritySettingVerificationWrapper"]);
                    FormControllerUtility.hideProgressBar(scopeObj.view);
                } else
                    scopeObj.getAndShowAnswerSecurityQuestions();
            };
            this.view.settings.btnAcknowledgementDone.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showProfileSettings();
            };
            this.view.settings.btnBackAnswerSecurityQuestions.onClick = function() {
                scopeObj.view.settings.tbxAnswers1.text = " ";
                scopeObj.view.settings.tbxAnswers2.text = " ";
            };
            this.view.settings.btnVerifyAnswerSecurityQuestions.onClick = function() {
                FormControllerUtility.showProgressBar(scopeObj.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.verifyQuestionsAnswer(scopeObj.onSaveAnswerSecurityQuestions());
                //FormControllerUtility.hideProgressBar(scopeObj.view);
            };

            //Secure Access Code  
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnSecureAccessCodeModify.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnSecureAccessCodeModify.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnSecureAccessCodeModify.onClick = function() {
                    var isPhoneEnabled, isEmailenabled;
                    var data = [];
                    if (scopeObj.view.settings.btnSecureAccessCodeModify.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")) {
                        if (scopeObj.view.settings.lblPhoneUnchecked.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED)
                            data.isPhoneEnabled = "true";
                        else
                            data.isPhoneEnabled = "false";
                        if (scopeObj.view.settings.lblemailunchecked.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED)
                            data.isEmailEnabled = "true";
                        else
                            data.isEmailEnabled = "false";

                        if(data.isPhoneEnabled == 'false' &&  data.isEmailEnabled == 'false'){
                          scopeObj.showEditSecureAccessSettingsError(kony.i18n.getLocalizedString("i18n.profile.editSecureAccessSettingsError"));
                        }else{
                          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
                          scopeObj.view.settings.btnSecureAccessCodeModify.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                          FormControllerUtility.showProgressBar(scopeObj.view);
                          kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateSecureAccessOptions(data);
                        }
                        
                    } else {
                        scopeObj.view.settings.flxPhoneUnchecked.onClick = function() {
                            scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblPhoneUnchecked);
                        };
                        scopeObj.view.settings.flxemailunchecked.onClick = function() {
                            scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblemailunchecked);
                        };
                        scopeObj.view.settings.btnSecureAccessCodeCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                        scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(true);
                        scopeObj.view.settings.btnSecureAccessCodeModify.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                        scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                    }
                };
            }
            this.view.settings.flxPhoneUnchecked.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblPhoneUnchecked);
            };
            this.view.settings.flxemailunchecked.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblemailunchecked);
            };
            this.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
                scopeObj.assignOTPSendAction();
            };
            this.view.settings.flxUsernameVerificationRadioOption1.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadio, scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2);
            };
            this.view.settings.flxUsernameVerificationRadioOption2.onClick = function() {
                scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2, scopeObj.view.settings.imgUsernameVerificationcheckedRadio);
            };

            this.view.settings.btnEditAccountsCancel.onClick = function() {
                scopeObj.showAccounts();
            };
            this.view.settings.btnEditAccountsSave.onClick = function() {
                scopeObj.showAccounts();
            };
            this.view.settings.flxFavoriteEmailCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBox(scopeObj.view.settings.lblFavoriteEmailCheckBox);
            };
            this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
                scopeObj.toggleFontCheckBoxEditAccounts(scopeObj.view.settings.lblEnableEStatementsCheckBox);
            };
            this.view.settings.flxTCContentsCheckbox.onClick = function() {
                if (scopeObj.view.settings.lblTCContentsCheckbox.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                    scopeObj.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    scopeObj.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
                } else {
                    scopeObj.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                    scopeObj.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                    scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
                }
            };

            //Set Default Accounts
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnDefaultTransactionAccountEdit.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnDefaultTransactionAccountEdit.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnDefaultTransactionAccountEdit.onClick = this.onSaveDefaultAccounts;
            }
            this.view.settings.btnDefaultTransctionAccountsCancel.text= kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            this.view.settings.btnDefaultTransctionAccountsCancel.onClick = function() {
                self.showDefaultScreen();
            };
            //Info Actions
            this.view.settings.flxImgInfoIcon.onClick = function() {
                var scrollpos = scopeObj.view.contentOffsetMeasured;
                if (scopeObj.view.settings.AllForms.isVisible === false) {
                    scopeObj.view.settings.AllForms.isVisible = true;
                } else
                    scopeObj.view.settings.AllForms.isVisible = false;
                scopeObj.view.forceLayout();
                scopeObj.view.setContentOffset(scrollpos);
                scopeObj.AdjustScreen();
            };
            this.view.settings.AllForms1.flxCross.onClick = function() {
                var scrollpos = scopeObj.view.contentOffsetMeasured;
                scopeObj.view.settings.AllForms1.isVisible = false;
                scopeObj.view.forceLayout();
                scopeObj.view.setContentOffset(scrollpos);
            };

        },

        /**
         * Method to assign action show OTP
         */
        showOTPAction: function() {
            var scopeObj = this;
            this.view.settings.imgViewOTP.onClick = function() {
                if (showOTP) {
                    scopeObj.showOTP();
                    showOTP = 0;
                } else {
                    scopeObj.hideOTP();
                    showOTP = 1;
                }
            };
        },


        /**
         * Method to show default screen
         */
        showDefaultScreen: function() {
            this.view.settings.lbxTransfers.setVisibility(false);
            this.view.settings.lbxBillPay.setVisibility(false);
            this.view.settings.lbxPayAPreson.setVisibility(false);
            this.view.settings.lbxCheckDeposit.setVisibility(false);
            this.view.settings.flxTransfersValue.setVisibility(true);
            this.view.settings.flxBillPayValue.setVisibility(true);
            this.view.settings.flxPayAPersonValue.setVisibility(true);
            this.view.settings.flxCheckDepositValue.setVisibility(true);
            this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
            this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            this.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(false);
            this.view.settings.btnDefaultTransactionAccountEdit.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
        },
        showPassword: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = false;
        },
        hidePassword: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = true;
        },
      
        showEditSecureAccessSettingsError: function(errorMessage){
          this.view.flxDowntimeWarning.setVisibility(true);
          this.view.rtxDowntimeWarning.text = errorMessage;
          this.view.flxDowntimeWarning.forceLayout();
        },
      
        hideEditSecureAccessSettingsError : function(){
          this.view.flxDowntimeWarning.setVisibility(false);
          this.view.flxDowntimeWarning.forceLayout();
        }, 
        /**
         * Method to toggle arrows when menu is expanded
         * @param {String} widget- widget ID
         * @param {String} imgArrow- ID of the arrow to be changed
         */
        toggle: function(widget, imgArrow) {
            var scope = this;
            if (widget.frame.height > 0) {
                this.collapseAll();
            } else {
                this.collapseAll();
               // imgArrow.src = ViewConstants.IMAGES.ARRAOW_UP;
                imgArrow.text = "P";
                imgArrow.toolTip = "Collapse";

                this.expand(widget);
            }
        },
        /**
         * Method to assign images when checkbox is clicked
         * @param {String} imgCheckBox- ID of the checkbox
         */
        toggleCheckBox: function(imgCheckBox) {
            if (imgCheckBox.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                imgCheckBox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
            } else {
                imgCheckBox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
            }
        },
        toggleFontCheckBox: function(imgCheckBox) {
            if (imgCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                imgCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                imgCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            } else {
                imgCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                imgCheckBox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
        },
      
       setDataForEmailListBox:function(response){
            var data = [];
            var list = [];
            var i;
            for (i = 0; i < response.length; i++) {
                list = [];
                if (response[i].isPrimary === "true") {
                    list.push("primaryemail");
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            for (i = 0; i < response.length; i++) {
                list = [];
                if (data[0][1] !== response[i].Value) {
                    list.push(response[i].id);
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            this.view.settings.lbxEmailForReceiving.masterData = data;
             this.view.settings.lbxEmailForReceiving.selectedKey = "primaryemail";
        },

      
        /**
         * Method to assign images when checkbox is clicked in accounts
         * @param {String} imgCheckBox- ID of the checkbox
         */

        toggleCheckBoxEditAccounts: function(imgCheckBox) {
             CommonUtilities.toggleFontCheckbox(imgCheckBox);
            if (CommonUtilities.isFontIconChecked(imgCheckBox) === true) {
                this.view.settings.flxTCCheckBox.setVisibility(true);
                this.view.settings.flxEmailForReceiving.setVisibility(true);
                this.view.settings.flxTCContentsCheckbox.setVisibility(true);
                this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";
                var emailObj = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
              var emailObj = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
                this.setDataForEmailListBox(emailObj);
                this.view.settings.lbxEmailForReceiving.setVisibility(true);
                if (CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                    this.enableButton(this.view.settings.btnEditAccountsSave);
                } else {
                    this.disableButton(this.view.settings.btnEditAccountsSave);
                }
            } else {
                this.view.settings.flxTCCheckBox.setVisibility(false);
                this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                this.view.settings.lbxEmailForReceiving.setVisibility(false);
                 this.view.settings.flxEmailForReceiving.setVisibility(false);
                this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
                this.enableButton(this.view.settings.btnEditAccountsSave);
                if (CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                    CommonUtilities.toggleFontCheckbox(this.view.settings.lblTCContentsCheckbox);
                }
            }
        },

        /**
         * Method to expand menu without animation
         * @param {String} widget- ID of the widget
         */
        expandWithoutAnimation: function(widget) {
            widget.height = widget.widgets().length * 40;
            this.view.settings.forceLayout();
        },

        /**
         * Method to activate a menu item
         * @param {String} parentIndex- parent to be exapnded
         * @param {String} childrenIndex- child to be expanded in parent
         */
        activateMenu: function(parentIndex, childrenIndex) {
            var menuObject = widgetsMap[parentIndex];
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
            this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.skin = "sknMenuSelected";
            this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.hoverSkin = "sknMenuSelected";
        },
        /**
         * Method to assign initial action on onclick of widgets
         */
        initProfileSettingsMenu: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function(){
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.view.settings.lblChangeLanguage.toolTip= kony.i18n.getLocalizedString("i18n.Profile.Language");
            this.view.customheader.forceCloseHamburger();
            this.view.settings.rtxRulesPassword.text = "";
            if (applicationManager.getConfigurationManager().editPassword === "false") {
                this.view.settings.btnPasswordEdit.setVisibility(false);
            } else
                this.view.settings.btnPasswordEdit.setVisibility(true);
            if (applicationManager.getConfigurationManager().editUsername === "false") {
                this.view.settings.btnUsernameEdit.setVisibility(false);
            } else
                this.view.settings.btnUsernameEdit.setVisibility(true);

            // Resetting Top Menu Skin
            this.view.customheader.topmenu.flxMenu.skin = "slFbox";
            this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
            this.view.settings.transactionalAndPaymentsAlerts.btnSave.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
            this.view.settings.transactionalAndPaymentsAlerts.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");      
            this.view.settings.transactionalAndPaymentsAlerts.btnSave.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
            this.view.settings.transactionalAndPaymentsAlerts.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.lblAlertsDisableContent.text = kony.i18n.getLocalizedString("i18n.Profilemanagement.AlertsDisablePopup");
            this.view.rtxAlertsDisableWarning.text = kony.i18n.getLocalizedString("i18n.Profilemanagement.AlertsDisableWarning");        
            this.view.btnAlertsDisableNo.onClick = function(){
                scopeObj.view.flxAlertsDisablePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            this.view.flxAlertsDisableClose.onClick = function(){
                scopeObj.view.flxAlertsDisablePopUp.setVisibility(false);
                scopeObj.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
            };
            var isLanguage = applicationManager.getConfigurationManager().getLanguageSelectionFlag();
            scopeObj.setLeftMenuUI(isLanguage);

        },
         setLeftMenuUI: function(flag){
            function applyConfigurations(menuObject) {
                var children = menuObject.subMenu.children
                var invisibleCount = children.reduce(function(count, child) {
                    var configuration = applicationManager.getConfigurationManager().undefined;
                    if (configuration === "false" || (child.widget === "flxChangeLanguage" && !flag)) {
                        count += 1;
                        scopeObj.view.settings[child.widget].setVisibility(false);
                    }
                    return count;
                }, 0)
                var visibleCount = children.length - invisibleCount;
                scopeObj.view.settings[menuObject.subMenu.parent].height = 40 * visibleCount + "px";
                if (invisibleCount === children.length) {
                    scopeObj.view.settings[menuObject.menu].setVisibility(false);
                }
            }
            var scopeObj = this;
            var component = this.view.settings;
            for (var i = 0; i < widgetsMap.length; i++) {
                var menuObject = widgetsMap[i];
                applyConfigurations(menuObject);
                scopeObj.view.settings.forceLayout();
                scopeObj.view.settings[menuObject.menu].onClick = this.getMenuHandler(this.view.settings[menuObject.subMenu.parent], this.view.settings[menuObject.image]);
            }
            this.setFlowActions();
            this.collapseAll();
            this.showViews();
            this.configurationContactSettings();
            this.expand(this.view.settings.flxProfileSettingsSubMenu);
            this.view.settings.lblProfileSettingsCollapse.text = "P";
            this.view.settings.lblProfileSettingsCollapse.toolTip = "Collapse";
            //this.resetSkins();
        },
        /**
         * Method to manage Configuration of Address fields and phone number fields
         */
        configurationContactSettings: function() {
            this.view.settings.btnAddNewAddress.setVisibility(applicationManager.getConfigurationManager().additionalAddressAllowed === "true");
            this.view.settings.btnAddNewNumber.setVisibility(applicationManager.getConfigurationManager().additionalPhoneAllowed === "true")
        },
        
        /**
         * Method to highlight the menu item and collapse others on the left side
         * @param {String} submenu- Item of the menu to be highlighted
         * @param {String} collapseImage- Arrow to be expanded
         */
        getMenuHandler: function(subMenu, collapseImage) {
            return function() {
                this.toggle(subMenu, collapseImage);
            }.bind(this);
        },

        /**
         * Method to collapse the menu item with animation
         * @param {String} widget- ID of the widget to be collapsed
         */
        collapse: function(widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height": 0
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function() {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();

                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },

        

        selectedQuestions: {
            ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
            key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
        },
        selectedQuestionsTemp: {
            securityQuestions: [],
            flagToManipulate: []
        },
        responseBackend: [{
            question: "",
            SecurityID: ""
        }],

        /**
         * Method to assign initial action on onclick of widgets
         * @param {Object} response- JSON of the questions fetched from MF
         * @param {Object} data- JSON of the questions
         */
        staticSetQuestions: function(response, data) {
            if (!data.errmsg) {
                FormControllerUtility.hideProgressBar(this.view);
                this.responseBackend = data;
                this.successCallback(response);
                this.showSetSecurityQuestions();
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.showFetchSecurityQuestionsError();
            }

            this.AdjustScreen();
        },
        /**
         * Method to show all the security questions
         */
        showSetSecurityQuestions: function() {
            //this.view.flxUsernameAndPasswordAck.setVisibility(false);
            this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
            this.view.settings.flxSecurityQuestionSet.setVisibility(true);
            this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
            this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
            this.view.settings.flxSecurityQASet1.setVisibility(true);
            this.view.settings.flxSecurityQASet2.setVisibility(true);
            this.view.settings.flxSecurityQASet3.setVisibility(true);
            this.view.settings.flxSecurityQASet4.setVisibility(true);
            this.view.settings.flxSecurityQASet5.setVisibility(true);
            this.view.settings.lblEditSecuritySettingsHeader.setFocus(true);
            this.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.view.forceLayout();
        },
        /**
         * Method to set data to all the list boxes initially
         * @param {Object} response- JSON of questions which should get set
         */
        successCallback: function(response) {
            var data = [];
            this.selectedQuestionsTemp = response;
            data = this.getQuestions(response);
            this.view.settings.lbxQuestion1.masterData = data;
            this.view.settings.lbxQuestion2.masterData = data;
            this.view.settings.lbxQuestion3.masterData = data;
            this.view.settings.lbxQuestion4.masterData = data;
            this.view.settings.lbxQuestion5.masterData = data;
            this.view.settings.tbxAnswer1.text = "";
            this.view.settings.tbxAnswer2.text = "";
            this.view.settings.tbxAnswer3.text = "";
            this.view.settings.tbxAnswer4.text = "";
            this.view.settings.tbxAnswer5.text = "";
            this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);


        },
        /**
         * Method to manipulate data in listbox
         * @param {Object} data- JSON of all the questions
         * @returns {Object} selectedData- JSON of seleted question and answer
         */
        flagManipulation: function(data, selectedData) {
            var tempData1 = [],
                tempData2 = [];
            if (selectedData[0] !== "lb0") {
                tempData1[0] = selectedData;
                tempData2 = tempData1.concat(data);
            } else
                tempData2 = data;
            return tempData2;

        },
        /**
         * Method  to manipulate the flag of the question selected
         * @param {Object} data - JSON of all the questions with answers
         * @param {Object} selectedQues - JSON of the selected question with its answer
         * @param {Object} key - ID of the question selected
         */
        getQuestionsAfterSelected: function(data, selectedQues, key) {
            var response = [];
            var temp = 10;
            if (data[1] !== "Select a Question") {
                for (var i = 0; i < this.selectedQuestionsTemp.securityQuestions.length; i++) {
                    if (this.selectedQuestionsTemp.securityQuestions[i] === data[1]) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "false") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "true";
                            temp = i;
                        }
                    }
                    if (this.selectedQuestionsTemp.securityQuestions[i] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "false";
                            this.selectedQuestions.key[key] = "lb" + (i + 1);
                        }
                    }
                }
            } else {
                this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
                if (key === 0) {
                    this.view.settings.tbxAnswer1.text = "";
                } else if (key === 1) {
                    this.view.settings.tbxAnswer2.text = "";
                } else if (key === 2) {
                    this.view.settings.tbxAnswer3.text = "";
                } else if (key === 3) {
                    this.view.settings.tbxAnswer4.text = "";
                } else if (key === 4) {
                    this.view.settings.tbxAnswer5.text = "";
                }
                for (var ij = 0; ij < this.selectedQuestionsTemp.securityQuestions.length; ij++) {
                    if (this.selectedQuestionsTemp.securityQuestions[ij] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[ij] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[ij] = "false";
                            this.selectedQuestions.key[key] = "lb" + (ij + 1);
                        }
                    }
                }
            }
            if (temp !== 10) {
                this.selectedQuestions.ques[key] = this.selectedQuestionsTemp.securityQuestions[temp];
                this.selectedQuestions.key[key] = "lb" + (temp + 1);
            } else {
                this.selectedQuestions.ques[key] = "Select a Question";
                this.selectedQuestions.key[key] = "lb0";
            }
            var questions = [];
            questions = this.getQuestions(this.selectedQuestionsTemp);
            return questions;
        },
        /**
         * Method that changes questions into key-value pairs basing on the flagManipulation
         * @param {Object} response- JSON of all the questions 
         */
        getQuestions: function(response) {
            var temp = [];
            temp[0] = ["lb0", "Select a Question"];
            for (var i = 0, j = 1; i < response.securityQuestions.length; i++) {
                var arr = [];
                if (response.flagToManipulate[i] === "false") {
                    arr[0] = "lb" + (i + 1);
                    arr[1] = response.securityQuestions[i];
                    temp[j] = arr;
                    j++;
                }
            }
            return temp;
        },
        /**
         * Method to enable Proceed button
         * @param {String} question1- First question selected
         * @param {String} question1- Second question selected
         * @param {String} question1- Third question selected
         * @param {String} question1- Fourth question selected
         * @param {String} question1- Fifth question selected
         */
        enableSecurityQuestions: function(question1, question2, question3, question4, question5) {
     	if (CommonUtilities.isCSRMode()) {
               this.view.settings.btnEditSecuritySettingsProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditSecuritySettingsProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
               if (question1 !== null && question1 !== "" && question2 !== null && question2 !== "" && question3 !== null && question3 !== "" && question4 !== null && question4 !== "" && question5 !== null && question5 !== "") {
                this.btnSecurityQuestions(true);
            } else {
                this.btnSecurityQuestions(false);
            }
            }
           
        },
        /**
         * Method to toggle the proceed button visibility
         * @param {Boolean} status - Status of the button to be enabled or disabled
         */
        btnSecurityQuestions: function(status) {
            if (status === true) {
                this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
            } else {
                this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
            }
        },

        /**
         * Method to set Question for lbxQuestion1
         */
        setQuestionForListBox1: function() {
            var value = [];
            value = this.view.settings.lbxQuestion1.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[0];
            selectedQues.key = this.selectedQuestions.key[0];
            var position = 0;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion2.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
        },

        /**
         * Method to set Question for lbxQuestion4
         */
        setQuestionForListBox4: function() {
            var value = [];
            value = this.view.settings.lbxQuestion4.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[3];
            selectedQues.key = this.selectedQuestions.key[3];
            var position = 3;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;

            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion2.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
        },
        /**
         * Method to set Question for lbxQuestion3
         */
        setQuestionForListBox3: function() {
            var value = [];
            value = this.view.settings.lbxQuestion3.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[2];
            selectedQues.key = this.selectedQuestions.key[2]
            var position = 2;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion2.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
        },
        /**
         * Method to set Question for lbxQuestion2
         */
        setQuestionForListBox2: function() {
            var value = [];
            value = this.view.settings.lbxQuestion2.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[1];
            selectedQues.key = this.selectedQuestions.key[1]
            var position = 1;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion5.masterData = mainData4;
        },
        /**
         * Method to set Question for lbxQuestion5
         */
        setQuestionForListBox5: function() {
            var value = [];
            value = this.view.settings.lbxQuestion5.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[4];
            selectedQues.key = this.selectedQuestions.key[4];
            var position = 4;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.settings.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.settings.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.settings.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.settings.lbxQuestion2.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.settings.lbxQuestion2.masterData = mainData4;
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer1
         */
        onEditingAnswer1: function() {
            var data = [];
            data = this.view.settings.lbxQuestion1.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer1.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer1.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer2
         */
        onEditingAnswer2: function() {
            var data = [];
            data = this.view.settings.lbxQuestion2.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer2.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer2.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer3
         */
        onEditingAnswer3: function() {
            var data = [];
            data = this.view.settings.lbxQuestion3.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer3.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer3.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer4
         */
        onEditingAnswer4: function() {
            var data = [];
            data = this.view.settings.lbxQuestion4.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer4.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer4.maxTextLength = 50;
            }
        },
        /**
         * Method to assign onBeginEditing function of tbxAnswer5
         */
        onEditingAnswer5: function() {
            var data = [];
            data = this.view.settings.lbxQuestion5.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.settings.tbxAnswer5.maxTextLength = 0;
            } else {
                this.view.settings.tbxAnswer5.maxTextLength = 50;
            }
        },
        /**
         * Method for saving security questions
         */
        onSaveSecurityQuestions: function() {
            var data = [{
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }];
            var quesData = "";
            quesData = this.view.settings.lbxQuestion1.selectedKeyValue;
            data[0].customerAnswer = this.view.settings.tbxAnswer1.text;
            data[0].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion2.selectedKeyValue;
            data[1].customerAnswer = this.view.settings.tbxAnswer2.text;
            data[1].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion3.selectedKeyValue;
            data[2].customerAnswer = this.view.settings.tbxAnswer3.text;
            data[2].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion4.selectedKeyValue;
            data[3].customerAnswer = this.view.settings.tbxAnswer4.text;
            data[3].questionId = this.getQuestionID(quesData);
            quesData = this.view.settings.lbxQuestion5.selectedKeyValue;
            data[4].customerAnswer = this.view.settings.tbxAnswer5.text;
            data[4].questionId = this.getQuestionID(quesData);
            return data;
        },
        /**
         * Method to get questionID from question
         */
        getQuestionID: function(quesData) {
            var qData;
            for (var i = 0; i < this.responseBackend.length; i++) {
                if (quesData[1] === this.responseBackend[i].SecurityQuestion) {
                    qData = this.responseBackend[i].SecurityQuestion_id;
                }
            }
            return qData;
        },

        /**
         * Method to edit phone number options
         */
        setEditPhoneNoOption2SegmentData: function() {
            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
              	"lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };
            var data = [{
                    "lblCheckBox": {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN},
                    "lblAccounts": "Joint Checking â€¦.4323  "
                },
                {
                    "lblCheckBox": {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": "Joint Checking â€¦.4323 "
                },
                {
                    "lblCheckBox": {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": "Joint Checking â€¦.4323 "
                }
            ];
            this.view.settings.segEditPhoneNumbersOption2.widgetDataMap = dataMap;
            this.view.settings.segEditPhoneNumbersOption2.setData(data);
            this.view.forceLayout();
        },

        /**
         * Method onSaveExternalAccount
         * @param {boolean} errorScenario - contains the errorScenario.
         */
        onSaveExternalAccount: function(errorScenario) {
            var self = this;
            if (errorScenario === true) {
                this.view.settings.flxErrorEditAccounts.setVisibility(true);
                this.view.settings.lblErrorEditAccounts.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.weAreUnableToProcess");
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.view.settings.flxErrorEditAccounts.setVisibility(false);
                self.showViews();
                self.view.settings.btnEditAccountsCancel.onClick = function() {
                    self.showAccounts();
                };
                self.view.settings.btnEditAccountsSave.onClick = function() {
                    self.showAccounts();
                };
            }
        },

        /**
         * Method to go to edit accounts
         * @param {Object} data - contains account
         */
        goToEditExternalAccounts: function(data) {
            var self = this;
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings.flxAccountSettingsSubMenu);
            this.setSelectedSkin("AccountPreferences");
            this.view.settings.flxProfileWrapper.setVisibility(false);
            this.view.settings.flxErrorEditAccounts.setVisibility(false);
            this.view.settings.tbxAccountNickNameValue.text = data.nickName;

            this.view.settings.lblFullNameValue.text = data.accountHolder;
            this.view.settings.lblAccountTypeValue.text = data.accountType;
            this.view.settings.lblAccountNumberValue.text = data.accountID;
            this.view.settings.lblFavoriteEmailCheckBox.text = (data.favouriteStatus === "1") ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED :  ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.settings.lblFavoriteEmailCheckBox.skin = (data.favouriteStatus === "1") ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN :  ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblFavoriteEmailCheckBox.onClick = function() {
                self.toggleFontCheckBox(self.view.settings.lblFavoriteEmailCheckBox);
            };


            //this.view.settings.flxEmailForReceiving.setVisibility(false);
            this.view.settings.flxTCCheckBox.setVisibility(false);
            this.view.settings.flxTCContentsCheckbox.setVisibility(false);
            this.view.settings.lblTCContentsCheckbox.setVisibility(false);
            this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(false);
            this.view.settings.flxEnableEStatementsCheckBox.setVisibility(false);
            this.view.settings.btnTermsAndConditions.setVisibility(false);


            this.enableButton(this.view.settings.btnEditAccountsSave);
            this.view.settings.flxTCCheckBox.setVisibility(true);
            this.enableButton(this.view.btnSave);

            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAccountsSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAccountsSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.settings.btnEditAccountsSave.focus = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAccountsSave.onClick = function(data) {
                    FormControllerUtility.showProgressBar(this.view);
                    var newdata = {
                        Account_id: data.externalAccountId,
                        NickName: self.view.settings.tbxAccountNickNameValue.text,
                        FavouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? "true" : "false",

                    };
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveExternalAccountsData(newdata);

                }.bind(this, data);
                this.view.settings.btnEditAccountsCancel.onClick = function() {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showAccountsDashboard();
                    self.showViews();
                    self.view.settings.btnEditAccountsCancel.onClick = function() {
                        self.showAccounts();
                    }
                    self.view.settings.btnEditAccountsSave.onClick = function() {
                        //Write Code to SAVE the changes of accounts
                        self.showAccounts();
                    };
                };
            }
          	this.showViews(["flxEditAccountsWrapper"]);
            if (applicationManager.getConfigurationManager().editNickNameAccountSettings === "true") {
                this.view.settings.flxAccountNickName.setVisibility(true);
            } else {
                this.view.settings.flxAccountNickName.setVisibility(false);
            }
            this.AdjustScreen();
            this.changeProgressBarState(false);
        },

        /**
         * Method used to show the email view.
         */
        showEmail: function() {
            this.showViews(["flxEmailWrapper"]);
        },
        /**
         * Method to update the list of emails
         * @param {Object} emailListViewModel- list of emails
         */
        updateEmailList: function(emailListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showEmail();
            if (emailListViewModel.length >= 3) {
                this.view.settings.btnAddNewEmail.setVisibility(false);
                this.view.settings.btnEditAddNewEmail.setVisibility(false);
            } else {
                this.view.settings.btnAddNewEmail.setVisibility(true);
                this.view.settings.btnEditAddNewEmail.setVisibility(true);
            }
            this.setEmailSegmentData(emailListViewModel);
            this.setSelectedSkin("Email");
            
        },

        /**
         * Method to set all the Data comming from backend to the email module
         * @param {Object} emailList - List of all the emails of the user
         */
        setEmailSegmentData: function(emailList) {
            var scopeObj = this;

            function getDeleteEmailListener(emailObj) {
                return function() {
                    var currForm = scopeObj.view;
                    currForm.flxDeletePopUp.height = currForm.flxHeader.frame.height + currForm.flxContainer.frame.height + currForm.flxFooter.frame.height+ "dp";
                    currForm.flxDeletePopUp.setVisibility(true);
                    currForm.flxDeletePopUp.setFocus(true);
                    currForm.lblDeleteHeader.setFocus(true);
                    currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
                    currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteEmail");
                    currForm.forceLayout();
                    currForm.btnDeleteYes.onClick = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deleteEmail(emailObj);
                        scopeObj.view.flxDelete.setFocus(true);
                        scopeObj.view.flxDeletePopUp.setVisibility(false);
                        scopeObj.view.forceLayout();
                    };
                }
            }

            var dataMap = {
                "btnDelete": "btnDelete",
                "btnEdit": "btnEdit",
                "flxDeleteAction": "flxDeleteAction",
                "flxEdit": "flxEdit",
                "flxEmailId": "flxEmailId",
                "flxEmail": "flxEmail",
                "flxPrimary": "flxPrimary",
                "flxProfileManagementEmail": "flxProfileManagementEmail",
                "flxRow": "flxRow",
                "flxInfo": "flxInfo",
                "lblSeperator": "lblSeperator",
                "lblSeperatorActions": "lbeditEmaillSeperatorActions",
                "imgInfo": "imgInfo",
                "lblEmailId": "lblEmailId",
                "lblPrimary": "lblPrimary",
                "lblEmail": "lblEmail",
                "template":"template"
            };
            var data = emailList.map(function(emailObj) {
                var dataObject = {
                    "lblEmail": emailObj.Value,
                    "lblEmailId": " ",
                     "flxPrimary" : {
                        "isVisible" : true,
                    },
                    "lblPrimary": (emailObj.isPrimary == "true") ? "Primary" : "  ",
                    "btnDelete": {
                        "text": emailObj.isPrimary == "true" ? " " : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                        "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteEmailListener(emailObj),
                        "skin": CommonUtilities.isCSRMode() ? CommonUtilities.disableSegmentButtonSkinForCSRMode(13) : "sknBtnSSP0273e313Px"
                    },
                    "flxInfo": {
                        "onClick": scopeObj.toggleContextualMenuEmail.bind(scopeObj)
                    },
                    "btnEdit": {
                        "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        "onClick": (emailObj.isPrimary == "true") ? scopeObj.editPrimaryEmail.bind(scopeObj, emailObj) : scopeObj.editEmail.bind(scopeObj, emailObj)
                    },
                    "imgInfo": (emailObj.isPrimary == "true") ? ViewConstants.IMAGES.INFO_GREY : " ",
                    "lblSeperator": "lblSeperator",
                    "lblSeperatorActions": "lblSeperatorActions",
                    "Extension": emailObj.Extension,
                    "template":"flxProfileManagementEmail"
                }
                if (CommonUtilities.isCSRMode()) {
                    dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                }
                return dataObject;
            })
            this.view.settings.segEmailIds.widgetDataMap = dataMap;
            if(kony.application.getCurrentBreakpoint() === 1024){
                for(var i=0;i<data.length;i++){
                data[i].template = "flxProfileManagementEmailTablet";
             }
           };
            this.view.settings.segEmailIds.setData(data);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },

        toggleContextualMenuEmail: function() {
            var index = this.view.settings.segEmailIds.selectedIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 51) + 108;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";
            leftInfo = this.view.settings.segEmailIds.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segEmailIds.clonedTemplates[index].flxPrimary.frame.x  - 130;
            this.view.settings.AllForms1.left = leftInfo +"dp";            
            this.view.settings.AllForms1.right = "";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoEmail");
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },


        toggleContextualMenuAddress: function() {
            var index = this.view.settings.segAddresses.selectedIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 144) + 108;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";
            leftInfo = this.view.settings.segAddresses.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segAddresses.clonedTemplates[index].flxCommunicationAddress.frame.x  - 130;
            this.view.settings.AllForms1.left = leftInfo +"dp";            
            this.view.settings.AllForms1.right = "";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoAddress");
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },

        /**
         * Method to edit the email which is already set
         * @param {Object} emailObj- JSON object of the email with all fields comminf from backend
         */
        editEmail: function(emailObj) {
            var scopeObj = this;
            this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(!(emailObj.isPrimary == "true"));
            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(!(emailObj.isPrimary == "true"));
            scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
          	scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            scopeObj.showEditEmail();
            this.view.forceLayout();
            scopeObj.view.settings.tbxEditEmailId.text = emailObj.Value;
            this.checkUpdateEmailForm();
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                scopeObj.view.settings.btnEditEmailIdSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editEmail({
                        id: emailObj.id,
                        extension: emailObj.Extension,
                        email: scopeObj.view.settings.tbxEditEmailId.text,
                        isPrimary: scopeObj.view.settings.lblEditMarkAsPrimaryEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? true : false
                    });
                };
            }
            scopeObj.checkAddEmailForm();
        },

        /**
         * Method to reset fields in the UI of Email module and show the module
         */
        showEditEmail: function() {
            this.resetUpdateEmailForm();
            this.showViews(["flxEditEmailWrapper"]);
        },

        /**
         * Method to reset fields in the UI of Email module
         */
        resetUpdateEmailForm: function() {
            this.view.settings.tbxEditEmailId.text = "";
            this.view.settings.lblEditMarkAsPrimaryEmailCheckBox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
          	this.view.settings.lblEditMarkAsPrimaryEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.disableButton(this.view.settings.btnEditEmailIdSave);
            this.view.settings.flxErrorEditEmail.setVisibility(false);
        },

        /**
         * Method to Disable a button
         * @param {String} button - ID of the button to be disabled
         */
        disableButton: function(button) {
            button.setEnabled(false);
            button.skin = "sknBtnBlockedSSPFFFFFF15Px";
            button.hoverSkin = "sknBtnBlockedSSPFFFFFF15Px";
            button.focusSkin = "sknBtnBlockedSSPFFFFFF15Px";
        },

        /**
         * Method to edit the primary Email of the User
         * @param {JSON} emailObj- JSON object with email and its ID
         */
        editPrimaryEmail: function(emailObj) {
            this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(false);
            this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(false);
            this.view.forceLayout();
            var scopeObj = this;
            scopeObj.showEditEmail();
            scopeObj.view.settings.tbxEditEmailId.text = emailObj.Value;
            this.checkUpdateEmailForm();
            if (CommonUtilities.isCSRMode()) {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = CommonUtilities.disableButtonSkinForCSRMode();
                scopeObj.view.settings.btnEditEmailIdSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                scopeObj.view.settings.btnEditEmailIdSave.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editEmail({
                        id: emailObj.id,
                        extension: emailObj.extension,
                        email: scopeObj.view.settings.tbxEditEmailId.text,
                        isPrimary: true
                    });
                };
            }
            scopeObj.checkUpdateEmailForm();
        },

        /**
         * Method to enable/disable button based on the email entered after editing
         */
        checkUpdateEmailForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                if (applicationManager.getValidationUtilManager().isValidEmail(this.view.settings.tbxEditEmailId.text)) {
                    this.enableButton(this.view.settings.btnEditEmailIdSave);
                } else {
                    this.disableButton(this.view.settings.btnEditEmailIdSave);
                }
            }
        },

        /**
         * Method to Enable a button
         * @param {String} button - ID of the button to be enabled
         */
        enableButton: function(button) {
            if(!CommonUtilities.isCSRMode()){
               button.setEnabled(true);
               button.skin = "sknbtnSSPffffff15px0273e3bg";
               button.hoverSkin = "sknBtnFocusSSPFFFFFF15Px0273e3";
               button.focusSkin = "sknBtnHoverSSPFFFFFF15Px0273e3";
            }
        },

        /**
         * Method to update the module while adding a new phone number
         * @param {Object} addPhoneViewModel- responce from backend after fetching phone number
         */
        updateAddPhoneViewModel: function(addPhoneViewModel) {
            this.checkAddNewPhoneForm();
            if (addPhoneViewModel.serverError) {
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(true);
                this.view.settings.CopylblError0f2f036aaf7534c.text = addPhoneViewModel.serverError;
            } else {
                this.showAddPhonenumber();
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
                //this.view.settings.imgAddRadioBtnUS.src = addPhoneViewModel.countryType === 'domestic' ?  ViewConstants.IMAGES.ICON_RADIOBTN: ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE ;
                this.showPhoneRadiobtn(this.view.settings.imgAddRadioBtnUS, this.view.settings.imgAddRadioBtnInternational);
                this.view.settings.lbxAddPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
                this.view.settings.lbxAddPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
                this.view.settings.tbxAddPhoneNumber.text = addPhoneViewModel.phoneNumber;
                this.view.settings.tbxAddPhoneNumberCountryCode.text = addPhoneViewModel.phoneCountryCode;
                this.view.settings.tbxAddExtension.text = addPhoneViewModel.ext;
              	this.view.settings.lblAddCheckBox3.skin = addPhoneViewModel.isPrimary ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.settings.lblAddCheckBox3.text = addPhoneViewModel.isPrimary ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
              	this.view.settings.lblAddCheckBox4.skin = addPhoneViewModel.recievePromotions ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.settings.lblAddCheckBox4.text = addPhoneViewModel.recievePromotions ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
               // this.setAddPhoneServicesData(addPhoneViewModel.services);
                this.view.settings.btnAddPhoneNumberCancel.onClick = function() {
                    this.showPhoneNumbers();
                }.bind(this);
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.btnAddPhoneNumberSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.btnAddPhoneNumberSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.btnAddPhoneNumberSave.onClick = function() {
                        if (this.validateAddPhoneNumberForm()) {
                            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.savePhoneNumber(this.getNewPhoneFormData());
                            this.view.settings.flxHeader.setFocus(true);
                        }
                    }.bind(this);
                }
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
            }
        },

        /**
         * Method to enable/disable button based on the new phone number entered
         */
        checkAddNewPhoneForm: function() {
            var formdata = this.getNewPhoneFormData();
            if (formdata.phoneNumber === "") {
                this.disableButton(this.view.settings.btnAddPhoneNumberSave);
            } else {
                this.enableButton(this.view.settings.btnAddPhoneNumberSave);
            }
        },

        /**
         * Method to show the UI of Add Phone Number
         */
        showAddPhonenumber: function() {
            this.showViews(["flxAddPhoneNumbersWrapper"]);
            this.view.settings.flxAddPhoneNumberCountryCode.setVisibility(true);
            if(applicationManager.getConfigurationManager().getCountryCodeFlag() == true){
              this.view.settings.tbxAddPhoneNumberCountryCode.setVisibility(true);
            }else{
              this.view.settings.tbxAddPhoneNumberCountryCode.setVisibility(false);
            }
            this.view.settings.flxAddOption2.setVisibility(false);
            this.view.settings.flxAddOption4.setVisibility(false);
        },

        showPhoneNumbers: function() {
            this.showViews(["flxPhoneNumbersWrapper"]);
        },

        /**
         * Method to update add phone number UI based on the error or success senario
         */
        validateAddPhoneNumberForm: function() {
            var phoneData = this.getNewPhoneFormData();
            if ((!applicationManager.getValidationUtilManager().isValidPhoneNumber(phoneData.phoneNumber)) || phoneData.phoneNumber.length != 10) {
                FormControllerUtility.showErrorForTextboxFields(this.view.settings.tbxAddPhoneNumber,this.view.settings.flxErrorAddPhoneNumber);
                this.view.settings.CopylblError0f2f036aaf7534c.text = kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber");
                return false;
            } else {
                FormControllerUtility.hideErrorForTextboxFields(this.view.settings.tbxAddPhoneNumber,this.view.settings.flxErrorAddPhoneNumber);
                return true;
            }
        },

        /**
         * Method to change the image of radio button
         * @param {String} imgRadio1- path of the first radio button
         * @param {String} imgRadio2- path of the second radio button
         */
        showPhoneRadiobtn: function(imgRadio1, imgRadio2) {
            if (imgRadio1.src === ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE) {
                imgRadio1.src = ViewConstants.IMAGES.ICON_RADIOBTN;
                imgRadio2.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            } else {
                imgRadio1.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
                imgRadio2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            }
            this.view.forceLayout();
        },

        /**
         * Method to set the mapping to save phone data
         * @param {JSON} services - Array of accounts
         */
        /*setAddPhoneServicesData: function(services) {
            var scopeObj = this;
            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };

            function getCheckBoxListener(serviceObj, index) {
                return function() {
                    var data = scopeObj.view.settings.segAddPhoneNumbersOption1.data[index];
                  	data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    scopeObj.view.settings.segAddPhoneNumbersOption1.setDataAt(data, index);
                }
            }
            var data = services.map(function(serviceObj, index) {
                return {
                    "id": serviceObj.id,
                    "lblCheckBox": {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": serviceObj.name,
                    "flxCheckbox": {
                        "onClick": getCheckBoxListener(serviceObj, index)
                    }
                }
            })
            this.view.settings.segAddPhoneNumbersOption1.widgetDataMap = dataMap;
            this.view.settings.segAddPhoneNumbersOption1.setData(data);
            this.view.forceLayout();
        },*/

        /**
         * Method to show data related to phone numbers
         * @returns {Object} - contains the type, phoneNumber, extension, isPrimary, services, receivePromotions attributes.
         */
        getNewPhoneFormData: function() {
            return {
                type: this.view.settings.lbxAddPhoneNumberType.selectedKey,
                phoneNumber: this.view.settings.tbxAddPhoneNumber.text.trim(),
                phoneCountryCode: this.view.settings.tbxAddPhoneNumberCountryCode.text.trim(),
                extension: this.view.settings.tbxAddExtension.text.trim(),
                isPrimary: this.view.settings.lblAddCheckBox3.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
               /* services: this.view.settings.segAddPhoneNumbersOption1.data == undefined ? {} : this.view.settings.segAddPhoneNumbersOption1.data
                    .filter(function(data) {
                        return data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
                    })
                    .map(function(data) {
                        return data.id
                    }),*/
                receivePromotions: false,
            }
        },

        /**
         * Method to update the list of phone number
         * @param {Object} phoneListViewModel- list of phone numbers
         */
        updatePhoneList: function(phoneListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showPhoneNumbers();
            if (phoneListViewModel.length >= 3) {
                this.view.settings.btnAddNewNumber.setVisibility(false);
            } else {
                this.view.settings.btnAddNewNumber.setVisibility(true);
            }
            this.setPhoneSegmentData(phoneListViewModel);
            this.setSelectedSkin("Phone");
            
        },
      
        getPhoneNumber:function(phoneObj){
            if(applicationManager.getConfigurationManager().getCountryCodeFlag() == true){
                return phoneObj.phoneCountryCode +  "-" + phoneObj.phoneNumber;
            }
            else{
                return phoneObj.phoneNumber;
            }
        },

        /**
         * Method to set data  of all the phone numbers
         * @param {Object} phoneListViewModel - List of all the phone numbers got from backend
         */
        setPhoneSegmentData: function(phoneListViewModel) {
            var scopeObj = this;
            var dataMap = {
                "btnViewDetail": "btnViewDetail",
                "flxCheckBox1": "flxCheckBox1",
                "flxCheckBox2": "flxCheckBox2",
                "flxCheckBox3": "flxCheckBox3",
                "flxCheckBox4": "flxCheckBox4",
                "flxCollapsible": "flxCollapsible",
                "flxDeleteAction": "flxDeleteAction",
                "flxEdit": "flxEdit",
                "flxHome": "flxHome",
                "flxOption1": "flxOption1",
                "flxOption2": "flxOption2",
                "flxOption3": "flxOption3",
                "flxOption4": "flxOption4",
                "flxOptions": "flxOptions",
                "flxPhoneNumber": "flxPhoneNumber",
                "flxPrimary": "flxPrimary",
                "flxRow": "flxRow",
                "flxSelectedPhoneNumbers": "flxSelectedPhoneNumbers",
                "lblCheckBox1": "lblCheckBox1",
                "lblCheckBox2": "lblCheckBox2",
                "lblCheckBox3": "lblCheckBox3",
                "lblCheckBox4": "lblCheckBox4",
                "imgCollapsible": "imgCollapsible",
                "imgInfo": "imgInfo",
                "flxInfo": "flxInfo",
                "lblHome": "lblHome",
                "lblOption1": "lblOption1",
                "lblOption2": "lblOption2",
                "lblOption3": "lblOption3",
                "lblOption4": "lblOption4",
                "lblOptionSeperator": "lblOptionSeperator",
                "lblPhoneNumber": "lblPhoneNumber",
                "lblPleaseChoose": "lblPleaseChoose",
                "lblPrimary": "lblPrimary",
                "lblSelectedSeperator": "lblSelectedSeperator",
                "lblSeperator": "lblSeperator",
                "lblSeperatorActions": "lblSeperatorActions",
                "template": "template",
                "lblCountryCode": "lblCountryCode"
            };

            function getShowDetailListener(phoneModel) {
                return function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getPhoneDetails(phoneModel);
                }
            }
            var isCountryCodeEnabled = applicationManager.getConfigurationManager().getCountryCodeFlag();
            var data = phoneListViewModel.map(function(phoneModel) {
                return {
                    "imgCollapsible": ViewConstants.IMAGES.ARRAOW_DOWN,
                    "lblHome": CommonUtilities.changedataCase(phoneModel.Extension),
                    "lblPhoneNumber": (phoneModel.phoneNumber) ? phoneModel.phoneNumber : " ",
                    "lblCountryCode": isCountryCodeEnabled == true ? phoneModel.phoneCountryCode : "",
                    "lblPrimary": phoneModel.isPrimary === "true" ? "Primary" : " ",
                    "imgInfo": phoneModel.isPrimary === "true" ? ViewConstants.IMAGES.INFO_GREY : "",
                    "flxInfo": {
                        onClick: scopeObj.toggleContextualMenuPhoneNumbers.bind(scopeObj)
                    },
                    "btnViewDetail": {
                        text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                        toolTip: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                        onClick: getShowDetailListener(phoneModel)
                    },
                    "lblPleaseChoose": "Please choose what service you like to use this number for:",
                    "lblOption1": "Joint Savings ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦. 1234",
                    "lblOption2": "Joint Checking ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦. 1234",
                    "lblOption3": "Make my primary phone number",
                    "lblOption4": "Allow to recieve text messages",
                    "lblCheckBox1": {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN},
                    "lblCheckBox2": {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN},
                    "lblCheckBox3": {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN},
                    "lblCheckBox4": {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN},
                    "lblSelectedSeperator": "lblSelectedSeperator",
                    "lblSeperator": "lblSeperator",
                    "lblSeperatorActions": "lblSeperatorActions",
                    "lblOptionSeperator": "lblOptionSeperator",
                    "template": "flxPhoneNumbers",
                    "phoneCountryCode": phoneModel.phoneCountryCode,
                    "Extension": phoneModel.Extension,
                    "phoneNumber": phoneModel.Value
                };
            })
            this.view.settings.segPhoneNumbers.widgetDataMap = dataMap;
            if (kony.application.getCurrentBreakpoint() === 1024) {
                for (var i = 0; i < data.length; i++) {
                    data[i].btnViewDetail.text = "Details";
                }
            };
            this.view.settings.segPhoneNumbers.setData(data);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },

        toggleContextualMenuPhoneNumbers: function() {
            var index = this.view.settings.segPhoneNumbers.selectedIndex[1];
            var scrollpos = this.view.contentOffsetMeasured;
            var hgt = ((index) * 51) + 130;
            if (this.view.settings.AllForms1.isVisible === false)
                this.view.settings.AllForms1.isVisible = true;
            else
                this.view.settings.AllForms1.isVisible = false;
            this.view.settings.AllForms1.top = hgt + "dp";            
            leftInfo = this.view.settings.segPhoneNumbers.clonedTemplates[index].flxInfo.frame.x + this.view.settings.segPhoneNumbers.clonedTemplates[index].flxPrimary.frame.x  - 130;
            this.view.settings.AllForms1.left = leftInfo +"dp";            
            this.view.settings.AllForms1.right = "";
            this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoPhone");
            this.view.forceLayout();
            this.view.setContentOffset(scrollpos);
        },

        /**
         * Method to assign details of the phone number
         * @param {Object} viewModel- JSON of the details of the phone number got from backend
         */
      
        /**
         * @function
         *
         * @param viewModel 
         */
        constructPhoneNumber: function(phoneObj){
           var phoneNumber = "";
            if (applicationManager.getConfigurationManager().getCountryCodeFlag() == true) {
                if (phoneObj.phoneCountryCode) {
                    phoneNumber = phoneNumber + phoneObj.phoneCountryCode + "-" + phoneObj.phoneNumber;
                } else {
                    phoneNumber = phoneNumber + phoneObj.phoneNumber;
                }
            } else {
                phoneNumber = phoneObj.phoneNumber;
            }
            if(phoneNumber === "undefined"){
                return "";
            }else{
                return phoneNumber;
            }
        },
        showPhoneDetails: function(viewModel) {
            this.view.settings.flxError.setVisibility(false);
            var phoneModel = viewModel.phone;
            var scopeObj = this;
            this.showDetailPhoneNumber();
            this.view.settings.btnEdit.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxEditPhoneNumberButtons.setVisibility(false);
            this.view.settings.btnDelete.text = phoneModel.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.billPay.Edit") : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
            this.view.settings.btnDelete.toolTip = phoneModel.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.billPay.Edit") : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
            this.view.settings.lblTypeValue.text = CommonUtilities.changedataCase(phoneModel.Extension);
            this.view.settings.lblPhoneNumberValue.text = scopeObj.constructPhoneNumber(phoneModel);
          	this.view.settings.lblCheckBox3.skin = phoneModel.isPrimary === "true" ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
			this.view.settings.lblCheckBox3.text = phoneModel.isPrimary === "true" ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
          	this.view.settings.lblCheckBox4.skin = phoneModel.receivePromotions === "1" ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblCheckBox4.text = phoneModel.receivePromotions === "1" ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.settings.flxOptions.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxOption3.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxWarning.setVisibility(phoneModel.isPrimary !== "true");
            this.view.settings.flxCheckBox3.onClick = null;
            this.view.settings.lblExtensionUnEditable.text = phoneModel.Extension;
            this.view.settings.flxCheckBox4.onClick = null;
            this.view.settings.btnEditPhoneNumberSave.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones;
           // this.setViewPhoneServicesData(viewModel.services, viewModel.phone);
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnDelete.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
            } else {
                this.view.settings.btnDelete.onClick = phoneModel.isPrimary === "true" ? scopeObj.editPhone.bind(scopeObj, phoneModel) : scopeObj.deletePhone.bind(scopeObj, phoneModel);
            }
            this.view.settings.btnEdit.onClick = scopeObj.editPhone.bind(scopeObj, phoneModel);
        },

        /**
         * Method to show the details at the phone number flex
         */
        showDetailPhoneNumber: function() {
            
            this.view.settings.flxEditPhoneNumberCountryCode.setVisibility(false);
            this.view.settings.lblEditPhoneNumberHeading.text = kony.i18n.getLocalizedString("i18n.profilemanagement.phoneNumberDetail");
            this.view.settings.flxOption2.setVisibility(false);
            this.showViews(["flxEditPhoneNumbersWrapper"]);
            this.view.settings.btnDelete.isVisible = true;
            this.view.settings.btnEdit.isVisible = true;
            this.view.settings.flxEditPhoneNumberButtons.setVisibility(false);
            this.view.settings.lblExtensionUnEditable.isVisible = false;
            this.view.settings.lblCountryValue.isVisible = false;
            this.view.settings.lblTypeValue.isVisible = true;
            this.view.settings.lblCountry.setVisibility(false);
            this.view.settings.lblCountryValue.setVisibility(false);
            this.view.settings.lblPhoneNumberValue.isVisible = true;
            this.view.settings.flxRadioBtnUS.isVisible = false;
            this.view.settings.flxRadioBtnInternational.isVisible = false;
            this.view.settings.tbxPhoneNumber.isVisible = false;
            this.view.settings.lbxPhoneNumberType.isVisible = false;
            this.view.settings.lblRadioInternational.isVisible = false;
            this.view.settings.lblRadioUS.isVisible = false;
            this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = false;
            this.view.settings.flxWarning.isVisible = false;
            this.view.settings.btnEditPhoneNumberCancel.isVisible = false;
           // this.view.settings.lblEditPhoneNumberHeading.setFocus(true);
            this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
        },

        /**
         * Method to assign initial action on onclick of widgets
         * @param {Object} services - Array of accounts
         * @param {Object} phoneObj - JSON object of phone number
         */
      /*  setViewPhoneServicesData: function(services, phoneObj) {
            var scopeObj = this;

            function getServiceToggleListener(account, index) {
                return function() {
                    var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
                  	data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
                }
            }

            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };

            var data = services.map(function(account, index) {
                return {
                    "id": account.id,
                    "lblCheckBox": account.selected ? {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN} : {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": account.name,
                }
            })
            this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
            this.view.settings.segEditPhoneNumberOption1.setData(data);
            this.view.forceLayout();
        },*/

        /**
         * Method to delete a particular phone number
         * @param {Object} phoneObj- JSON object of the phone to be deleted
         */
        deletePhone: function(phoneObj) {
            var scopeObj = this;
            var currForm = scopeObj.view;
            currForm.flxDeletePopUp.setVisibility(true);
            currForm.flxDeletePopUp.setFocus(true);
            currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
            currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deletePhoneNum");
            currForm.forceLayout();
            currForm.btnDeleteYes.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deletePhone(phoneObj);
                scopeObj.view.flxDelete.setFocus(true);
                scopeObj.view.flxDeletePopUp.setVisibility(false);
                scopeObj.view.forceLayout();
            }
        },
        /**
         * Method to edit a phone number
         * @param {Object} phoneObj - JSON object of the phone number to be edited
         */
        editPhone: function(phoneObj) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.editPhoneView(phoneObj);
        },

        /**
         * Method to update the list of address
         * @param {Object} addressListViewModel- list of addresses
         */
        updateAddressList: function(addressListViewModel) {
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
            this.showAddresses();
            if (addressListViewModel.length >= 3) {
                this.view.settings.btnAddNewAddress.setVisibility(false);
            } else {
                this.view.settings.btnAddNewAddress.setVisibility(true);
            }
            this.setAddressSegmentData(addressListViewModel);
            this.setSelectedSkin("Address");
            
        },

        showAddresses: function() {
            this.showViews(["flxAddressesWrapper"]);
        },

        /**
         * Method to set all the data of the address module
         * @param {Object} userAddresses- List of all the addresses related to user
         */
        setAddressSegmentData: function(userAddresses) {
            var scopeObj = this;

            function getDeleteAddressEmailListener(address) {
                return function() {
                    var currForm = scopeObj.view;
                    currForm.flxDeletePopUp.height = currForm.flxHeader.frame.height + currForm.flxContainer.frame.height + currForm.flxFooter.frame.height;
                    currForm.flxDeletePopUp.setVisibility(true);
                    currForm.flxDeletePopUp.setFocus(true);
                    currForm.lblDeleteHeader.setFocus(true);
                    currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
                    currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteAddress");
                    currForm.forceLayout();
                    currForm.btnDeleteYes.onClick = function() {
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.deleteAddress(address);
                        scopeObj.view.flxDelete.setFocus(true);
                        scopeObj.view.flxDeletePopUp.setVisibility(false);
                        scopeObj.view.forceLayout();
                    };
                }
            }

            function getEditAddressListener(address) {
                return function() {
                    scopeObj.editAddress(address, userAddresses);
                };
            }
            var dataMap = {
                "btnEdit": "btnEdit",
                "btnDelete": "btnDelete",
                "flxAddress": "flxAddress",
                "flxAddressWrapper": "flxAddressWrapper",
                "flxCommunicationAddress": "flxCommunicationAddress",
                "flxRow": "flxRow",
                "flxInfo": "flxInfo",
                "lblSeperator": "lblSeperator",
                "imgCommunicationAddressInfo": "imgCommunicationAddressInfo",
                "lblAddessLine2": "lblAddessLine2",
                "lblAddressLine1": "lblAddressLine1",
                "lblAddressLine3": "lblAddressLine3",
                "lblAddressType": "lblAddressType",
                "lblSeperatorActions": "lblSeperatorActions",
                "lblCommunicationAddress": "lblCommunicationAddress"
            };
            var addressTypes = {
                "ADR_TYPE_WORK": 'Work',
                "ADR_TYPE_HOME": 'Home'
            }
            var data = userAddresses.map(function(address) {
                var dataObject = {
                    "lblAddressType": addressTypes[address.AddressType],
                    "lblAddressLine1": address.AddressLine1,
                    "lblAddessLine2": address.AddressLine2 ? address.AddressLine2 : (address.CityName + ', ' + address.CountryCode + ', ' + address.ZipCode),
                    "lblAddressLine3": address.AddressLine2 ? (address.CityName + ', ' + address.CountryCode + ', ' + address.ZipCode) : "",
                    "lblCommunicationAddress": address.isPrimary === "true" ? kony.i18n.getLocalizedString("i18n.ProfileManagement.CommunicationAddress") : "",
                    "lblSeperatorActions": address.isPrimary === "true" ? "" : "lblSeperatorActions",
                    "imgCommunicationAddressInfo": address.isPrimary === "true" ? ViewConstants.IMAGES.INFO_GREY : "",
                    "btnEdit": {
                        text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        onClick: getEditAddressListener(address)
                    },
                    "btnDelete": {
                        text: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                        toolTip: address.isPrimary === "true" ? "" : kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                        onClick: CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : getDeleteAddressEmailListener(address)
                    },
                    "flxInfo": {
                        onClick: scopeObj.toggleContextualMenuAddress.bind(scopeObj)
                    },
                    "lblSeperator": "lblSeperator",
                    "template": "flxRow"
                }
                if (CommonUtilities.isCSRMode()) {
                    dataObject.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                }
                return dataObject;
            })
            this.view.settings.segAddresses.widgetDataMap = dataMap;
            this.view.settings.segAddresses.setData(data);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },

        /**
         * Method to assign skin to the menu item selected
         * @param {String} name - menu item selected
         */
        setSelectedSkin: function(name) {
          
          if( this.currentSelectedSubMenu ) {
            flxName = "flx" + this.currentSelectedSubMenu;
            var temp = this.view.settings[flxName].widgets();
            temp[2].setVisibility(false);
            temp[1].skin = "sknLblSSP72727213px";
            this.view.settings[flxName].skin = "sknFlxffffffBottomCursornoBottom";
            this.view.settings[flxName].hoverSkin = "sknhoverf7f7f7";
          }

          flxName = "flx" + name;
          lblName = "lbl" + name;
          imgName = "flxIndicator" + name;
          this.view.settings[flxName][imgName].setVisibility(true);
          this.view.settings[flxName].hoverSkin = "sknFlxffffffBottomCursornoBottom";
          this.view.settings[flxName].skin = "sknFlxffffffBottomCursornoBottom";
          this.currentSelectedSubMenu = name;

        },

        /**
         * Method to validate the address entered
         */
        checkNewAddressForm: function() {
            var addAddressFormData = this.getNewAddressFormData();
            if (addAddressFormData.addressLine1 === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.zipcode === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.city === '') {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.countrySelected === "1") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.stateSelected === "lbl1") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else if (addAddressFormData.citySelected === "lbl2") {
                this.disableButton(this.view.settings.btnAddNewAddressAdd);
            } else {
                this.enableButton(this.view.settings.btnAddNewAddressAdd);
            }
        },

        /**
         * Method to hide all the flex of main body
         * @param {Object} addAddressViewModel - None
         */
        showAddNewAddressForm: function(addAddressViewModel) {
            var self = this;
            this.showAddNewAddress();
            if (addAddressViewModel.serverError) {
                this.view.settings.flxErrorAddNewEmail.setVisibility(true);
                this.view.settings.CopylblError0e8bfa78d5ffc48.text = addAddressViewModel.serverError;
            } else {
                this.view.settings.flxErrorAddNewEmail.setVisibility(false);
            }
            this.view.settings.lbxType.masterData = addAddressViewModel.addressTypes;
            this.view.settings.lbxType.selectedKey = addAddressViewModel.addressTypeSelected;
            this.view.settings.tbxAddressLine1.text = addAddressViewModel.addressLine1;
            this.view.settings.tbxAddressLine2.text = addAddressViewModel.addressLine2;
            this.view.settings.lbxCountry.masterData = addAddressViewModel.countryNew;
            this.view.settings.lbxCountry.selectedKey = addAddressViewModel.countrySelected;
            this.view.settings.lbxState.masterData = addAddressViewModel.stateNew;
            this.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
            this.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
            this.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
            var countryId = self.view.settings.lbxCountry.selectedKeyValue[0];
            if (countryId == "1") {
                self.view.settings.tbxCity.setEnabled(false);
                self.view.settings.lbxState.setEnabled(false);
            }
            this.view.settings.lbxCountry.onSelection = function() {
                var data = [];
                var countryId = self.view.settings.lbxCountry.selectedKeyValue[0];
                if (countryId == "1") {
                    self.checkNewAddressForm();
                    self.view.settings.lbxState.masterData = addAddressViewModel.stateNew;
                    self.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.lbxCountry.selectedKey = countryId;
                    self.view.settings.tbxCity.setEnabled(false);
                    self.view.settings.lbxState.setEnabled(false);
                } else {
                    self.view.settings.tbxCity.setEnabled(true);
                    self.view.settings.lbxState.setEnabled(true);
                    self.view.settings.lbxCountry.selectedKey = countryId;
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("country", countryId, addAddressViewModel.stateNew);
                    self.view.settings.lbxState.masterData = data.states;
                    self.view.settings.lbxState.selectedKey = addAddressViewModel.stateSelected;
                    var stateId = self.view.settings.lbxState.selectedKeyValue[0];
                    if (stateId == "lbl1") {
                        self.checkNewAddressForm();
                        self.view.settings.lbxCountry.masterData = addAddressViewModel.countryNew;
                        self.view.settings.lbxCountry.selectedKey = countryId;
                        self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                        self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        self.view.settings.tbxCity.setEnabled(false);
                    } else {
                        self.view.settings.lbxState.setEnabled(true);
                        self.view.settings.tbxCity.setEnabled(true);
                        data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, addAddressViewModel.cityNew);
                        if (data.length == 0) {
                            self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                            self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        } else {
                            self.view.settings.tbxCity.masterData = data.cities;
                            self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                        }
                        self.checkNewAddressForm();
                    }
                }
            };
            this.view.settings.lbxState.onSelection = function() {
                var data = [];
                var stateId = self.view.settings.lbxState.selectedKeyValue[0];
                if (stateId == "lbl1") {
                    self.checkNewAddressForm();
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.tbxCity.setEnabled(false);
                } else {
                    self.view.settings.lbxState.setEnabled(true);
                    self.view.settings.tbxCity.setEnabled(true);
                    data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getSpecifiedCitiesAndStates("state", stateId, addAddressViewModel.cityNew);
                    self.view.settings.tbxCity.masterData = addAddressViewModel.cityNew;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                    self.view.settings.tbxCity.masterData = data.cities;
                    self.view.settings.tbxCity.selectedKey = addAddressViewModel.citySelected;
                }
                self.checkNewAddressForm();
            };
            this.view.settings.tbxCity.onSelection = function() {
                var cityId = self.view.settings.tbxCity.selectedKeyValue[0];
                self.checkNewAddressForm();
            };

            this.view.settings.tbxZipcode.text = addAddressViewModel.zipcode;
          	this.view.settings.lblSetAsPreferredCheckBox.skin = addAddressViewModel.isPreferredAddress ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            this.view.settings.lblSetAsPreferredCheckBox.text = addAddressViewModel.isPreferredAddress ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.checkNewAddressForm();
            this.view.settings.btnAddNewAddressAdd.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveAddress(this.getNewAddressFormData());
            }.bind(this);
        },

        showAddNewAddress: function() {
            this.showViews(["flxAddNewAddressWrapper"]);
        },

        /**
         * Method to show data related to New Address scenario
         * @param {Object} - which sets the data
         */
        getNewAddressFormData: function() {
            return {
                addrLine1: this.view.settings.tbxAddressLine1.text.trim(),
                addrLine2: this.view.settings.tbxAddressLine2.text.trim(),
                countrySelected: this.view.settings.lbxCountry.selectedKey,
                zipcode: this.view.settings.tbxZipcode.text.trim(),
                isPreferredAddress: this.view.settings.lblSetAsPreferredCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                Addr_type: this.view.settings.lbxType.selectedKey,
                stateSelected: this.view.settings.lbxState.selectedKey,
                citySelected: this.view.settings.tbxCity.selectedKey
            };
        },

        /**
         * Method to edit the address which is already set
         * @param {Object} address- All the fields of the Address
         */
        editAddress: function(address) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getEditAddressView(address);
        },

        /**
         * Method to Check whether the password is valid and matches with the re entered password
         */
     ValidatePassword: function() {
            if ((this.isPasswordValid(this.view.settings.tbxNewPassword.text)) && (this.isPasswordValid(this.view.settings.tbxConfirmPassword.text))) {
                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    this.enableButton(this.view.settings.btnEditPasswordProceed);
                    this.view.settings.flxErrorEditPassword.setVisibility(false);
                } else {
                    this.disableButton(this.view.settings.btnEditPasswordProceed);
                    this.view.settings.flxErrorEditPassword.setVisibility(true);
                    this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.idm.newPasswordMismatch");
                }
            } else {
                    this.disableButton(this.view.settings.btnEditPasswordProceed);
                    this.view.settings.flxErrorEditPassword.setVisibility(true);
                    this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.enrollNow.validPassword");
            }
            this.view.forceLayout();
        },

        /**
         * Method to Check whether the password is valid and matches with the re entered password
         */
        isPasswordValidAndMatchedWithReEnteredValue: function() {
            if (this.view.settings.tbxNewPassword.text && this.view.settings.tbxConfirmPassword.text) {
                if (this.view.settings.tbxNewPassword.text === this.view.settings.tbxConfirmPassword.text) {
                    return true;
                }
            }
            return false;
        },

        showEditPassword: function() {
            this.view.settings.flxErrorEditPassword.setVisibility(false);
            this.showViews(["flxEditPasswordWrapper"]);
        },

        getPasswordRules: function(data) {
            FormControllerUtility.hideProgressBar(this.view);
            if (data) {
                this.view.settings.rtxRulesPassword.text = data.passwordpolicies.content;//
            } else {
                CommonUtilities.showServerDownScreen();
            }
        },

        showUsernameAndPassword: function() {
             var userPreferencesManager = applicationManager.getUserPreferencesManager();
             if(kony.mvc.MDAApplication.getSharedInstance().appContext.userName){
                 this.view.settings.lblUsernameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userName;     
             }else{
                this.view.settings.lblUsernameValue.text = userPreferencesManager.getCurrentUserName();
             }
            this.showViews(["flxUsernameAndPasswordWrapper"]);
          this.view.flxHeader.setFocus(true);
        },

        /**
         * Method to fetch 2 security question and show them
         */
        getAndShowAnswerSecurityQuestions: function() {
            FormControllerUtility.showProgressBar(this.view);
            this.view.settings.tbxAnswers1.text = "";
            this.view.settings.tbxAnswers2.text = "";
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAnswerSecurityQuestions();
        },

        /**
         * Method to assign action to send button to send OTP
         */
        assignOTPSendAction: function() {
            this.hideOTP();
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.requestOtp();
        },

        /**
         * Method to assign OnClick function of button Proceed in SecurityQuestions
         */
        onProceedSQ: function() {
            FormControllerUtility.showProgressBar(this.view);
            finaldata = this.onSaveSecurityQuestions();
            FormControllerUtility.hideProgressBar(this.view);
            //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveSecurityQuestions(data,this.saveSQCallback,this.saveSQfailureCallback);
        },

        hideOTP: function() {
            this.view.settings.tbxEnterOTP.secureTextEntry = true;
        },

        showSecuritySettingVerification: function() {
            this.showViews(["flxSecuritySettingVerificationWrapper"]);
        },

        /**
         * Method to show security questions which should get edit
         */
        showEditSecuritySettings: function() {
            var self = this;
            if (flag === 0) {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchSecurityQuestions(this.selectedQuestionsTemp, this.staticSetQuestions.bind(this));
            } else {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
                this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
                this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(false);
                this.view.settings.flxSecurityQASet1.setVisibility(false);
                this.view.settings.flxSecurityQASet2.setVisibility(false);
                this.view.settings.flxSecurityQASet3.setVisibility(false);
                this.view.settings.flxSecurityQASet4.setVisibility(false);
                this.view.settings.flxSecurityQASet5.setVisibility(false);
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                this.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
                this.enableButton(self.view.settings.btnEditSecuritySettingsProceed);
            }
        },

        /**
         * Method to show all the security questions screen after fetching from backend
         * @param {Object} viewModel- None
         */
        showSecurityQuestions: function(viewModel) {
            FormControllerUtility.showProgressBar(this.view);
            var self = this;
            if (viewModel !== "Questions Exist") {
                flag = 0;
                this.showEditSecuritySettings();
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                this.AdjustScreen();
            } else {
                this.showViews(["flxEditSecuritySettingsWrapper"]);
                this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
                this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
                this.view.settings.flxSecurityQuestionSet.setVisibility(false);
                this.view.settings.flxEditSecuritySettingsButtons.top = "300dp";
                this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
                this.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
                this.view.settings.btnEditSecuritySettingsProceed.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
                this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
            }
            this.view.settings.btnEditSecuritySettingsProceed.onclick = function() {
                self.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
                flag = 0;
                if (self.view.settings.btnEditSecuritySettingsProceed.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions")) {
                    self.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
                    self.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
                    self.view.settings.flxSecurityQuestionSet.setVisibility(false);
                    self.view.settings.flxEditSecuritySettingsButtons.top = "0dp";
                    self.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
                    self.view.settings.flxSecurityQASet1.setVisibility(true);
                    self.view.settings.flxSecurityQASet2.setVisibility(true);
                    self.view.settings.flxSecurityQASet3.setVisibility(true);
                    self.view.settings.flxSecurityQASet4.setVisibility(true);
                    self.view.settings.flxSecurityQASet5.setVisibility(true);
                    self.view.settings.btnEditSecuritySettingsCancel.setVisibility(true);
                    self.view.settings.btnEditSecuritySettingsProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
                    self.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
                    self.showEditSecuritySettings();
                    self.AdjustScreen();
                } else {
                    update = "security questions";
                  self.updateRequirements();
            }
            }
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);

        },

        /**
         * Method to show secure access code settings
         */
        showSecureAccessSettings: function() {
            var scopeObj = this;
            scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
            scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
            scopeObj.view.settings.lblViewEmailSettings.onTouchEnd = function() {
                scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
                scopeObj.view.forceLayout();
            }
            scopeObj.view.settings.lblViewPhoneSettings.onTouchEnd = function() {
                scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
                scopeObj.view.forceLayout();
            }
            scopeObj.view.settings.btnSecureAccessCodeCancel.onClick = function() {
                scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
                scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
                scopeObj.hideEditSecureAccessSettingsError();
                scopeObj.view.settings.lblViewEmailSettings.onClick = function() {
                    scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                    scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                    scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                    scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                    scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                    scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserEmail();
                    scopeObj.view.forceLayout();
                }
                scopeObj.view.settings.lblViewPhoneSettings.onClick = function() {
                    scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
                    scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
                    scopeObj.view.settings.imgSecuritySettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_DOWN;
                    scopeObj.view.settings.imgSecuritySettingsCollapse.toolTip = "Expand";
                    scopeObj.view.settings.imgProfileSettingsCollapse.src = ViewConstants.IMAGES.ARRAOW_UP;
                    scopeObj.view.settings.imgProfileSettingsCollapse.toolTip = "Collapse";
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.showUserPhones();
                    scopeObj.view.forceLayout();
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
                scopeObj.view.forceLayout();
            }
            scopeObj.collapseAll();
            scopeObj.expandWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecureAccess();
            scopeObj.view.forceLayout();
            FormControllerUtility.hideProgressBar(scopeObj.view);
        },

        /**
         * Method to close all menu options without animation
         */
        collapseAll: function() {
            for (var i = 0; i < widgetsMap.length; i++) {
                var menuObject = widgetsMap[i];
                this.collapseWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
               // this.view.settings[menuObject.image].src = ViewConstants.IMAGES.ARRAOW_DOWN;
                this.view.settings[menuObject.image].text = "O";
                this.view.settings[menuObject.image].toolTip = "Expand";
            }
        },

        /**
         * Method to assign animation to menu while collapsing
         * @param {String} widget- ID of the widget
         */
        collapseWithoutAnimation: function(widget) {
            widget.height = 0;
            this.view.settings.forceLayout();
        },

        /**
         * Method to expand a menu item with animation
         * @param {String} widget- ID of the widget to be expanded
         */
        expand: function(widget) {
            var scope = this;

            function getVisibleChildrenLength(widget) {
                var children = widget.widgets();
                var visible = children.reduce(function(count, child) {
                    if (child.isVisible) {
                        count += 1
                    }
                    return count;
                }, 0)
                return visible;
            }
            var animationDefinition = {
                100: {
                    "height": getVisibleChildrenLength(widget) * 40
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function() {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },

        /**
         *onClickModifyAlerts- function that triggers on click of button Modify of  alerts
         * @param {Object} alertsViewModel - alerts payload
         * @param {Object} channelsPayLoad - Channels payload
         **/
        onClickModifyAlerts: function( alertCategoryId, channelsPayLoad) {
            FormControllerUtility.showProgressBar(this.view);
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.isVisible = false;
            this.showModifyScreen(alertCategoryId);
            if(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C"){
                this.setChannelsDataonModify(channelsPayLoad);
                this.changeCheckBoxOnModify(this.view.settings.transactionalAndPaymentsAlerts.segAlerts);
            }
            this.setSeperatorHeight();
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setFocus(true);
            this.callEnableOrDisableAlertsSave();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
      
          /**
         *onClickCancelAlerts- function that triggers on click of button Modify of  alerts
         *@param {Object} alertsViewModel - alerts payload
         */
        onClickCancelAlerts: function(alertsViewModel) {
            if(!this.isServiceCallNeeded){
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(false);     
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true); 
            this.setAlertsToUI(alertsViewModel);
            this.setSeperatorHeight();
            this.AdjustScreen();
            }
            else{
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.fetchAlertsDataById(alertsViewModel.AlertCategoryId);
            }
        },

        /**
         *onClickSaveAlerts- function that triggers on click of button save of  alerts
        * @param {Object} alertsViewModel - alerts payload
        **/
        onClickSaveAlerts: function(alertsViewModel) {
            var scopeObj = this;
            this.saveAlerts(scopeObj.view.settings.transactionalAndPaymentsAlerts, alertsViewModel);
            this.setSeperatorHeight();
            this.AdjustScreen();
        },
        
        /**
         * Method to set UI and initial stages
         * @param {Object} alertsViewModel - alerts payload
         **/
        showAlerts: function(alertsViewModel) {
        this.view.customheader.customhamburger.activateMenu("Settings", "Alert Settings");
        this.collapseAll();
        this.expandWithoutAnimation(this.view.settings.flxAlertsSubMenu);
        var Alertflexes = [{
                key: "flxAccountAlerts"
            }, {
                key: "flxTransactionAndPaymentsAlerts"
            }, {
                key: "flxSecurityAlerts"
            }, { 
                key: "flxPromotionalAlerts"
            }];
            var AlertReferenceLabel = [{
                key: "lblReferenceAA"
            }, {
                key: "lblReferenceTPA"
            }, {
                key: "lblreferenceSA"
            }, {
                key: "lblReferencePA"
            }];
            var AlertsText = [{
                key: "lblAccountAlerts"
            }, {
                key: "lblTransactionAndPaymentsAlerts"
            }, {
                key: "lblSecurityAlerts"
            }, {
                key: "lblPromotionalAlerts"
            }];
            for (i = 0; i < alertsViewModel.length; i++) {
                this.view.settings[Alertflexes[i].key].setVisibility(true);
                this.view.settings[Alertflexes[i].key][AlertsText[i].key].text = alertsViewModel[i].alertcategorytext_DisplayName;
                this.view.settings[Alertflexes[i].key][AlertsText[i].key].toolTip = alertsViewModel[i].alertcategorytext_DisplayName;
                this.view.settings[Alertflexes[i].key][AlertReferenceLabel[i].key].text = alertsViewModel[i].alertcategory_id;
            }
            for (i; i < Alertflexes.length; i++) {
                this.view.settings[Alertflexes[i].key].setVisibility(false);
            }
            this.setAlertsFlowActions();
        },
         /**
         * Method to set UI after async call for fetching channels and alerts Data
         * @param {Object} alertsViewModel - alerts payload
         **/
        showAlertsDataByID: function(alertsViewModel){
            this.view.customheader.customhamburger.activateMenu("Settings", "Alert Settings");
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true); 
            this.setAlertsToUI(alertsViewModel);
            FormControllerUtility.hideProgressBar(this.view);
         },

         setAlertMessageByCategoryId: function (alertId) {
            if (alertId === "ALERT_CAT_SECURITY") {
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text =  kony.i18n.getLocalizedString("i18n.alerts.recieveSecurityAlerts");
           }
           else if (alertId == "ALERT_CAT_TRANSACTIONAL") {
               this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text =  kony.i18n.getLocalizedString("i18n.alerts.recieveTransactionAndPaymentAlerts"); 
            }
            else {
               this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text = kony.i18n.getLocalizedString("i18n.alerts.receiveAlerts");
            }
         },

         showModifyScreen: function (alertId) {
             var isEnabledScreen = this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin === ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN ? false : true;
             this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.setVisibility(true);
             this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.setVisibility(false);
             this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(false);
             this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(true);  
             if (isEnabledScreen) {
                 this.setAlertMessageByCategoryId(alertId);
                 this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "C";
                 this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled");
                }
                else{   
                    this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text = kony.i18n.getLocalizedString("i18n.alerts.alertsTimelyUpdates");
                    this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "D";
                    this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.Alerts.ENABLEALERTS");
            }
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.flxClickBlocker.setVisibility(false);
        },

         showDisabledScreen: function (isEnabledScreen, alertId) {
            this.view.settings.transactionalAndPaymentsAlerts.flxClickBlocker.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.setVisibility(true);
            this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true);
            if(isEnabledScreen === false){
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(false);
                this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text = kony.i18n.getLocalizedString("i18n.alerts.alertsDisabledModifyAlerts");
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin = ViewConstants.SKINS.DEACTIVATE_STATUS_SKIN;
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.Alerts.DisabledAlerts");
            }
            else{
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningImageNew.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.imgAlertsWarningImageNew.setVisibility(false);
                this.setAlertMessageByCategoryId(alertId);
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled");
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertStatusIndicator.skin = ViewConstants.SKINS.ACTIVE_STATUS_SKIN;
            }
         },
         /**
         * Method to show error UI for invalid input
         *@param {boolean} isShow - to determine whether to show  error or not
         */
        showInvalidAlertsUI:function(isShow, alertWidget, alerts, index){
            if(isShow){
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setFocus(true);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.setEnabled(false);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = ViewConstants.SKINS.BUTTON_BLOCKED;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.hoverSkin = ViewConstants.SKINS.BUTTON_BLOCKED;
            }
            else{
                if(index!==undefined){
                    this.resetSegmentRedSkins(alertWidget, alerts, index);
                }
                this.view.settings.transactionalAndPaymentsAlerts.flxInvalidAmount.setVisibility(false);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.setEnabled(true);
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = ViewConstants.SKINS.NORMAL;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = ViewConstants.SKINS.FOCUS;
                this.view.settings.transactionalAndPaymentsAlerts.btnSave.hoverSkin = ViewConstants.SKINS.HOVER;
            }
            this.view.settings.transactionalAndPaymentsAlerts.forceLayout();
        },
        /**
         * Method to reset error skin for alerts segment
         */
        resetSegmentRedSkins: function (alertWidget, alerts, index) {
            var data = alertWidget.segAlerts.data[index];
			if(data != undefined && data.tbxMinLimit!= undefined){
				 if (data.tbxMinLimit.skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX || data.tbxMaxLimit.skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX || data.tbxAmount.   skin !== ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX) {
					data.tbxMinLimit.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
					data.tbxMaxLimit.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
					data.tbxAmount.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
					alertWidget.segAlerts.setDataAt(data, index);
                }
			}
        },
        /**
         * Method to set UI of alerts
         *@param {Object} alertsViewModel - alerts payload
         */
        setAlertsToUI: function(alertsViewModel) {
            var scopeObj = this;
            scopeObj.showViews(["flxTransactionalAndPaymentsAlertsWrapper"]);            
            if(alertsViewModel.alertsData.length===0){
                scopeObj.showNoAlertsFoundUI();
            }
            else{
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsBody.setVisibility(true);
                this.view.settings.transactionalAndPaymentsAlerts.flxNoAlertsFound.setVisibility(false);
                var isEnabledScreen = (alertsViewModel.isAlertTypeSelected === true || alertsViewModel.isAlertTypeSelected === "true") ? true : false;
                var isModifyScreen = this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.isVisible === true ? false : true;
                this.showDisabledScreen(isEnabledScreen, alertsViewModel.alertCategoryId);
                this.setChannelsDataToUI(alertsViewModel.channels, isModifyScreen);
                this.setAlertsSegmentData( this.view.settings.transactionalAndPaymentsAlerts,alertsViewModel.alertsData);
                scopeObj.showInvalidAlertsUI(false,this.view.settings.transactionalAndPaymentsAlerts ,alertsViewModel);
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);                
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setFocus(true);
                this.isServiceCallNeeded= false;
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel1.onClick = scopeObj.onToggleChannel1.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel2.onClick = scopeObj.onToggleChannel2.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel3.onClick = scopeObj.onToggleChannel3.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.flxChannel4.onClick = scopeObj.onToggleChannel4.bind(this);
                this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.onClick = scopeObj.onClickModifyAlerts.bind(this, alertsViewModel.alertCategoryId, alertsViewModel.channels);
                this.view.settings.transactionalAndPaymentsAlerts.btnCancel.onClick = scopeObj.onClickCancelAlerts.bind(this, alertsViewModel);
                this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.onClick = scopeObj.onToggleAlert.bind(this, alertsViewModel);
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick = scopeObj.onClickSaveAlerts.bind(this, alertsViewModel);
                }
            }
            this.setSeperatorHeight();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        showNoAlertsFoundUI:function(){
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsBody.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.flxNoAlertsFound.setVisibility(true);            
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(false);
        },
        /**
         * Method to set channels UI of channels
         *@param {Object} channelsViewModel - channels payload
         *@param {Boolean} isModifyScreen - flag variable that determines whether the flow is in modify state or non-editable state 
         */
        setChannelsDataToUI: function(channelsViewModel, isModifyScreen) {
            for (var i = 1, j=0; i <=channelsViewModel.length; i++, j++) {
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].isVisible = true;
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin = channelsViewModel[j].isChannelSupported === "true" && isModifyScreen ? "flxHoverSkinPointer" : "skna0a0a0Bgf7f7f720pxolbfonticons";
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].text = channelsViewModel[j].isSubscribed === "true" ? "C" : "D";
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].toolTip = kony.i18n.getLocalizedString("i18n.NUO.Select")+" "+channelsViewModel[j].channeltext_Description;
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin =  channelsViewModel[j].isChannelSupported === "true" && isModifyScreen ? "skn0273e320pxolbfonticons" : "skna0a0a0Bgf7f7f720pxolbfonticons";
                this.view.settings.transactionalAndPaymentsAlerts["lblChannel" + i].text = channelsViewModel[j].channeltext_Description;
                this.view.settings.transactionalAndPaymentsAlerts["lblChannel" + i].isVisible = true;
            }
        },

         /**
         * Method to set channels UI of channels
         *@param {Object} channelsPayLoad - channels payload
         *@param {Boolean} isModifyScreen - flag variable that determines whether the flow is in modify state or non-editable state 
         */
        setChannelsDataonModify: function(channelsPayLoad) {
            var widget = this.view.settings.transactionalAndPaymentsAlerts;
            for (var i = 1, j=0; widget["flxChannel" + i] !== undefined ; i++, j++) {
                if (widget["flxChannel" + i].isVisible === true || widget["flxChannel" + i].isVisible === "true") {
                    this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin =   channelsPayLoad[j].isChannelSupported === "true" ? "flxHoverSkinPointer" : "skna0a0a0Bgf7f7f720pxolbfonticons";
                    this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin = channelsPayLoad[j].isChannelSupported === "true" ?  "skn0273e320pxolbfonticons" : "skna0a0a0Bgf7f7f720pxolbfonticons";
                }
            }
        },
        
        /**
         * on Click action of enable/disable button
         * @param {Object} alertsViewModel- alerts Data
         */
        onToggleAlert: function(alertsViewModel){
            var scopeObj = this;
            if(this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C"){
                this.view.flxAlertsDisablePopUp.height =  this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height + ViewConstants.POSITIONAL_VALUES.DP;
                this.view.flxAlertsDisablePopUp.setVisibility(true);
                if(CommonUtilities.isCSRMode()){
                    this.view.btnAlertsDisableYes.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.btnAlertsDisableYes.skin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.btnAlertsDisableYes.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                    this.view.btnAlertsDisableYes.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                }else{
                    this.view.btnAlertsDisableYes.onClick = scopeObj.onDisableAlerts.bind(this,alertsViewModel);
                }
            }
            else{
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "C";
                this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.alerts.alertsEnabled");
                this.setChannelsDataonModify(alertsViewModel.channels);
                this.setAlertMessageByCategoryId(alertsViewModel.alertId);
                this.changeCheckBoxOnEnableAlerts(this.view.settings.transactionalAndPaymentsAlerts,alertsViewModel.isInitialLoad);
            }
        },
        /**
         * on Click action of Yes button of popup confirmation to disable alerts 
         * @param {Object} alertsViewModel- alerts Data
         */
        onDisableAlerts : function(alertsViewModel){
            this.view.flxAlertsDisablePopUp.setVisibility(false);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text = "D";
            var data = this.view.settings.transactionalAndPaymentsAlerts.segAlerts.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.text = "D";
            }
            this.view.settings.transactionalAndPaymentsAlerts.segAlerts.setData(data);
            this.setAlertsToUI(alertsViewModel);
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text = kony.i18n.getLocalizedString("i18n.Alerts.ENABLEALERTS");            
            this.view.settings.transactionalAndPaymentsAlerts.lblAlertsWarningNew.text = kony.i18n.getLocalizedString("i18n.alerts.alertsTimelyUpdates");
            this.updateAlerts(this.view.settings.transactionalAndPaymentsAlerts, alertsViewModel);
            this.view.settings.transactionalAndPaymentsAlerts.flxAlertsHeader.setFocus(true);
        },

        updateAlerts : function(alertWidget, alertsViewModel) {
            alertWidget.flxAlertsHeader.setFocus(true); 
            alertWidget.flxAlertsStatusCheckbox.setFocus(true);
            var alertsData = {
                isSubscribed:  alertWidget.lblAlertsStatusCheckBox.text === "C" ? true :false,
                AlertCategoryId: alertsViewModel.AlertCategoryId,
            }
            var alertResponseData = {
                alertWidget: alertWidget,
                alertsViewModel: alertsViewModel
            };
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlerts(alertsData, alertResponseData);
        },

         /**
         * on Click action of channel flex (flxChannel1)
         */
        onToggleChannel1 :  function(){
            if(this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.skin === "skn0273e320pxolbfonticons"){
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.text =  this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            }
            else{
            }
        },
        /**
        * on Click action of channel flex (flxChannel2)
        */
        onToggleChannel2 :  function(){
            if(this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.skin === "skn0273e320pxolbfonticons"){
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.text =  this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            }
            else{
            }
        },
        /**
        * on Click action of channel flex (flxChannel3)
        */
        onToggleChannel3 :  function(){
            if(this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.skin === "skn0273e320pxolbfonticons"){
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.text =  this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            }
            else{
            }
        },
        /**
        * on Click action of channel flex (flxChannel4)
        */
        onToggleChannel4:  function(){
            if(this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.skin === "skn0273e320pxolbfonticons"){
                this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.text =  this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel4.text === "C" ? "D" : "C";
                this.callEnableOrDisableAlertsSave();
            }
            else{
            }
        },
        
        /**
         * Method to show downtime error in alerts
         */
        showAlertsError: function(errorMessage) {
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.text = errorMessage;
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
        * UI function to manage alerts UI
        */
        setSeperatorHeight: function() {
            this.view.settings.flxSeperator3.height = this.view.settings.flxRight.frame.height + "px";
            this.view.forceLayout();
        },
        /**
         *alertInputType- function that returns input type for specific alert value
         * @param {Object} rowAlert - alert object
         */
        alertInputType:function(rowAlert){
            if(rowAlert.alertCondition){
                if(rowAlert.alertCondition.NoOfFields==="2"){
                    return ViewConstants.ALERTS_INPUT_TYPES.Range;
                } else if(rowAlert.alertCondition.NoOfFields==="1"){
                    if(rowAlert.alertAttribute && rowAlert.alertAttribute.values){
                        return ViewConstants.ALERTS_INPUT_TYPES.List;
                    }
                    else{
                        return ViewConstants.ALERTS_INPUT_TYPES.EqualTo;
                    }
                }
            }
            return ViewConstants.ALERTS_INPUT_TYPES.NoInputNeeded;
        },
        /**
         *setAlertsSegmentData- function that sets data to segment segAlerts
         * @param {Object} alerts - alerts array
         * @param {Object} alertWidget - widget 
         */
        setAlertsSegmentData: function(alertWidget, alerts) {         
            var scopeObj = this;
            var rowData;
            var dataMap = {
                "flxAccountAlertsCategoryDetails": "flxAccountAlertsCategoryDetails",
                "flxEnabledIcon":"flxEnabledIcon",
                "lblEnabledIcon":"lblEnabledIcon",
                "lblAlertDescription":"lblAlertDescription",
                "flxAccBalanceInBetween":"flxAccBalanceInBetween",
                "tbxMinLimit":"tbxMinLimit",
                "tbxMaxLimit":"tbxMaxLimit",
                "flxHorSeparator":"flxHorSeparator",
                "lblHorSeparator":"lblHorSeparator",
                "flxAccBalanceCreditedDebited":"flxAccBalanceCreditedDebited",
                "tbxAmount": "tbxAmount",
                "flxNotifyBalance":"flxNotifyBalance",
                "lstNotify":"lstNotify",
                "lblSeperator": "lblSeperator",
                "alerttype_id":"alerttype_id",
                "template":"template"
            };
            var list = [];
            alerts.forEach(function(rowAlert, index){
                var type = scopeObj.alertInputType(rowAlert);
                data = {};
                    data = {
                        "lblEnabledIcon":{
                            "text":rowAlert.isSubscribed==="true"?"C":"D",
                            "skin":"skna0a0a0Bgf7f7f720pxolbfonticons",
                            "onClick":function(){
                                if(scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.text === "C"){
                                    rowData = alertWidget.segAlerts.data[index];
                                    if (rowData.lblEnabledIcon.text === "C") {
                                        rowData.lblEnabledIcon.text = "D";
                                    }
                                    else{
                                        rowData.lblEnabledIcon.text = "C";
                                    }
                                    alertWidget.segAlerts.setDataAt(rowData, index);
                                    scopeObj.showOrHideAlertsInputField(alertWidget, rowData, type, index);
                                    scopeObj.showInvalidAlertsUI(false,alertWidget, alerts, index);
                                    scopeObj.enableOrDisableSaveOption(scopeObj.checkIfAtleastOneChannelSelected( alertWidget ) &&
                                    scopeObj.checkIfAtleastOneAlertSelected( alertWidget ) );
                                }
                            }
                        },
                        "flxEnabledIcon":{
                            "skin":"skna0a0a0Bgf7f7f720pxolbfonticons"
                        },
                        "lblAlertDescription":rowAlert.alerttypetext_DisplayName,
                        "flxAccBalanceInBetween": {
                            isVisible:type===ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.isSubscribed==="true"?true:false
                        },
                        "tbxMinLimit":{
                            "text":(type===ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value1)?rowAlert.preference.Value1:"",
                            "skin":ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                            "onTextChange": scopeObj.showInvalidAlertsUI.bind(scopeObj, false,alertWidget, alerts, index)
                        },
                        "tbxMaxLimit":{
                            "text":(type===ViewConstants.ALERTS_INPUT_TYPES.Range && rowAlert.preference && rowAlert.preference.Value2)?rowAlert.preference.Value2:"",
                            "skin":ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                            "onTextChange": scopeObj.showInvalidAlertsUI.bind(scopeObj, false,alertWidget, alerts, index)
                        },              
                        "flxHorSeparator":{
                            isVisible:true
                        },
                        "lblHorSeparator":" ",
                        "flxAccBalanceCreditedDebited":{
                            isVisible:type===ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowAlert.isSubscribed==="true"?true:false,
                        },
                        "tbxAmount":{
                            "text": (type===ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowAlert.preference && rowAlert.preference.Value1)?rowAlert.preference.Value1:"",
                            "skin":ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX,
                            "onTextChange": scopeObj.showInvalidAlertsUI.bind(scopeObj, false,alertWidget, alerts, index)
                        },             
                        "flxNotifyBalance": {
                            isVisible:type===ViewConstants.ALERTS_INPUT_TYPES.List && rowAlert.isSubscribed==="true"?true:false,
                        },
                        //Needs Refactor
                        "lstNotify":{
                            selectedKey: (type===ViewConstants.ALERTS_INPUT_TYPES.List && rowAlert.preference)?rowAlert.preference.Value1:type===ViewConstants.ALERTS_INPUT_TYPES.List?scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values)[0][0]:"",
                            masterData:type===ViewConstants.ALERTS_INPUT_TYPES.List?scopeObj.returnMasterDataFromAlerts(rowAlert.alertAttribute.values):"",
                        },
                        "lblSeperator": "",
                        "alerttype_id":rowAlert.alerttype_id,
                        "template":"flxAccountAlertsCategoryDetails"
                    };
                    if(kony.application.getCurrentBreakpoint() === 1024){
                      for(var j=0;j<data.length;j++){
                        data[j].template = "flxAccountAlertsCategoryDetails";
                      }
                    }
                    list.push(data);
                               
            });
            alertWidget.segAlerts.widgetDataMap = dataMap;
            alertWidget.segAlerts.setData(list);
        },
        showOrHideAlertsInputField:function(alertWidget, rowData, type, index){
            var data = rowData;
			rowData.flxAccBalanceInBetween.isVisible = (type===ViewConstants.ALERTS_INPUT_TYPES.Range && rowData.lblEnabledIcon.text === "C")?true:false;
            rowData.flxAccBalanceCreditedDebited.isVisible = (type===ViewConstants.ALERTS_INPUT_TYPES.EqualTo && rowData.lblEnabledIcon.text === "C")?true:false;
            rowData.flxNotifyBalance.isVisible = (type===ViewConstants.ALERTS_INPUT_TYPES.List && rowData.lblEnabledIcon.text === "C")?true:false;
			alertWidget.segAlerts.setDataAt(data, index);
        },
        returnMasterDataFromAlerts:function(values){
            var data=[];
            values.forEach(function(value){
                var list = [];
                list.push(value.alertattributelistvalues_id);
                list.push(value.alertattributelistvalues_name);
                data.push(list);
            });
            return data;
        },
        /**
        *changeCheckBoxOnModify- function that will change skin of all the checkboxes to selected skin
        * @param {Object} alertWidget - alerts segment widget Id
        */
        changeCheckBoxOnModify: function(alertWidget){
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.skin = "skn0273e320pxolbfonticons";
                data[index].flxEnabledIcon.skin = "flxHoverSkinPointer";
            }
            alertWidget.setData(data);
        },
        /**
        *changeCheckBoxOnEnableAlerts - function that will change state of all the alerts based on enabling and disabling of parent level alert
        * @param {Object} alertWidget - alerts segment widget Id
        * @param {Boolean} isEnabled - Flag to identify if parent level alert is enabled or disabled
        * @param {Boolean} isInitialLoad - Flag to identify if alert will be selected for first time
        */
        changeCheckBoxOnEnableAlerts: function(alertWidget, isInitialLoad){
            if (isInitialLoad === "true" || isInitialLoad === true ) {
                this.enableAllCheckboxes(alertWidget.segAlerts);
            }
            else{
                this.changeCheckBoxOnModify(alertWidget.segAlerts);
            }     
        },
        /**
        *enableAllCheckboxes- function that enable all alerts
        * @param {Object} alertWidget - alerts segment widget Id
        */
        enableAllCheckboxes:function(alertWidget){
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.onClick();
            }
        },
        checkIfAtleastOneChannelSelected : function( alertWidget ) {
          for ( var i = 1 ; alertWidget["flxChannel" + i] !== undefined ; i++) 
          {
            if( alertWidget["lblCheckBoxChannel" + i].text === "C" ){
              return true;
            }            
          }
          return false;
        },
      
        checkIfAtleastOneAlertSelected : function( alertWidget ) {
            var data = alertWidget.segAlerts.data;
            for (var index = 0; index < data.length; index++) {
               if( data[index].lblEnabledIcon.text === "C"){
                 return true;
               }
            }
            return false;          
        },
      
        enableOrDisableSaveOption : function( enable ) {
          if( enable && !CommonUtilities.isCSRMode()) {
            FormControllerUtility.enableButton(this.view.settings.transactionalAndPaymentsAlerts.btnSave);         
          }
          else {
            FormControllerUtility.disableButton(this.view.settings.transactionalAndPaymentsAlerts.btnSave);         
          }
        }, 
        callEnableOrDisableAlertsSave : function() {
          var alertWidget = this.view.settings.transactionalAndPaymentsAlerts;
          this.enableOrDisableSaveOption(this.checkIfAtleastOneChannelSelected( alertWidget ) &&
                                         this.checkIfAtleastOneAlertSelected( alertWidget ) );

        }, 
        /**
        *fetchAlertsSegmentData- function to fetch all alerts and their status to form payload for set service
        * @param {Object} alertWidget - alerts segment widget Id
        */
        fetchAlertsSegmentData:function(alertWidget, alertsData){
            var scopeObj = this;
            var data = alertWidget.data;
            typePreference = [];
            data.forEach(function (rowAlert, i) {
                var type = scopeObj.alertInputType(alertsData[i]);
                var list = {};
                list.typeId = rowAlert.alerttype_id;
                list.isSubscribed = rowAlert.lblEnabledIcon.text === "C" ? "true" : "false";
                if (type === ViewConstants.ALERTS_INPUT_TYPES.Range) {
                    list.value1 = rowAlert.tbxMinLimit.text;
                    list.value2 = rowAlert.tbxMaxLimit.text;
                }
                else if (type === ViewConstants.ALERTS_INPUT_TYPES.EqualTo) {
                    list.value1 = rowAlert.tbxAmount.text;
                }
                else if (type === ViewConstants.ALERTS_INPUT_TYPES.List) {
                    list.value1 = rowAlert.lstNotify.selectedKey;
                }
                typePreference.push(list);
            });
            return typePreference;                       
        },
        /**
         * validateAlertsSegmentData - function to validate inputs for alerts
         */
        validateAlertsSegmentData:function(alertWidget, alertsData){      
            var scopeObj = this;
            var data = alertWidget.data;
            var isValid=true;
            data.forEach(function(rowAlert, i){
                var type = scopeObj.alertInputType(alertsData[i]);
                if(type===ViewConstants.ALERTS_INPUT_TYPES.Range){
                    if(rowAlert.lblEnabledIcon.text==="C" && (rowAlert.tbxMinLimit.text.trim().length === 0 || rowAlert.tbxMaxLimit.text.trim().length === 0))
                    {
                        rowAlert.tbxMinLimit.skin = ViewConstants.SKINS.BORDER;
                        rowAlert.tbxMaxLimit.skin = ViewConstants.SKINS.BORDER;
                        alertWidget.setDataAt(rowAlert, i);                        
                        isValid= false;                     
                    }                
                }
                else if(type===ViewConstants.ALERTS_INPUT_TYPES.EqualTo){
                    if(rowAlert.lblEnabledIcon.text==="C" && rowAlert.tbxAmount.text.trim().length === 0 )
                    {
                        rowAlert.tbxAmount.skin = ViewConstants.SKINS.BORDER;
                        alertWidget.setDataAt(rowAlert, i);                        
                        isValid= false;
                    }   
                }
            });            
            return isValid;

        },
          /**
         *getAlertsFromForm- function that fetches data of alerts changed
         * @param {Object} widget - widget 
         */
        getAlertsFromForm: function(widget, channelData){
            var channelPreferences = [];
            for (var i = 1, j=0; widget["flxChannel" + i] !== undefined ; i++, j++) {
                if(widget["flxChannel" + i] !== undefined && (widget["flxChannel" + i].isVisible === true || this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].isVisible === "true")) {
                    var temp={};
                    temp.isSubscribed = widget["lblCheckBoxChannel" + i].text === "C" ? true : false;
                    temp.channelId = channelData[j].channel_id;
                    channelData[j].isSubscribed = widget["lblCheckBoxChannel" + i].text === "C" ? "true" : "false";
                    channelPreferences.push(temp);
                }
            }
            return channelPreferences;
        },
        /**
         *saveAlerts- function that saves/updates alerts changed
         * @param {Object} alerts - alerts array
         */
        saveAlerts: function(alertWidget, alertsViewModel) {
            alertWidget.flxAlertsHeader.setFocus(true);            
            alertWidget.flxAlertsStatusCheckbox.setFocus(true);
            var alertsChannelData = this.getAlertsFromForm(alertWidget, alertsViewModel.channels);
            var isvalidAlertTypePreference= this.validateAlertsSegmentData(alertWidget.segAlerts, alertsViewModel.alertsData);
            if(isvalidAlertTypePreference){
                var alertTypePreference = this.fetchAlertsSegmentData(alertWidget.segAlerts, alertsViewModel.alertsData);
                var alertsData = {
                    channelPreference: alertsChannelData,
                    typePreference: alertTypePreference,
                    isSubscribed:  alertWidget.lblAlertsStatusCheckBox.text === "C" ? true :false,
                    AlertCategoryId: alertsViewModel.AlertCategoryId,
                }
                var alertResponseData ={
                    alertWidget: alertWidget,
                    alertsViewModel: alertsViewModel
                };
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.updateAlerts(alertsData, alertResponseData);
            }
            else{
                this.showInvalidAlertsUI(true,alertWidget, alertsViewModel);
            }
            //this.view.forceLayout();
        },
        saveAlertSuccess: function(alertWidget, alertsViewModel){            
            var isEnabledScreen = alertWidget.lblAlertsStatusCheckBox.text === "C" ? true : false;
            this.showDisabledScreen(isEnabledScreen, alertsViewModel.alertId);
            this.disableAllCheckboxes(alertWidget.segAlerts, alertsViewModel.channels);
            this.isServiceCallNeeded = true;
            this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true);
        },
        disableAllCheckboxes: function (alertWidget, channelsViewModel) {
            var data = alertWidget.data;
            for (var index = 0; index < data.length; index++) {
                data[index].lblEnabledIcon.skin = "skna0a0a0Bgf7f7f720pxolbfonticons";
                data[index].flxEnabledIcon.skin = "skna0a0a0Bgf7f7f720pxolbfonticons";                
            }
            alertWidget.setData(data);
            for (var i = 1, j=0; i <=channelsViewModel.length; i++, j++) {
                this.view.settings.transactionalAndPaymentsAlerts["flxChannel" + i].skin = "skna0a0a0Bgf7f7f720pxolbfonticons";
                this.view.settings.transactionalAndPaymentsAlerts["lblCheckBoxChannel" + i].skin =  "skna0a0a0Bgf7f7f720pxolbfonticons";
            }
        },
         
        /**
         *hideAllWidgetsAlerts- function to hide widgets
         * @param {String} alertWidget - ID of the widget
         */
        hideAllWidgetsAlerts: function(alertWidget) {
            alertWidget.btnModifyAlerts.setVisibility(false);
            alertWidget.flxWhiteSpace.setVisibility(false);
            alertWidget.flxClickBlocker.setVisibility(false);

            alertWidget.flxAlertsStatusCheckbox.setVisibility(false);
            alertWidget.flxAlertsRow1.setVisibility(false);
            alertWidget.flxAlertsRow2.setVisibility(false);
            alertWidget.flxAlertsSegment.setVisibility(false);

            alertWidget.flxButtons.setVisibility(false);
            alertWidget.btnEnableAlerts.setVisibility(false);
            alertWidget.btnCancel.setVisibility(false);
            alertWidget.btnSave.setVisibility(false);
        },


        /**
         * Method to update edit phone number UI based on the responce form backend
         * @param {Object} addPhoneViewModel - responce from backend after fetching phone number
         */
        updateEditPhoneViewModel: function(addPhoneViewModel) {
          var scopeObj = this;
            if (addPhoneViewModel.serverError) {
                this.view.settings.flxError.setVisibility(true);
                this.view.settings.lblError.text = addPhoneViewModel.serverError;
            } else {
                this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
                this.showEditPhonenumber();
                this.view.settings.flxError.setVisibility(false);
                this.view.settings.lbxPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
                this.view.settings.lbxPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
                this.view.settings.tbxPhoneNumber.text = addPhoneViewModel.phoneNumber;
                this.view.settings.tbxEditPhoneNumberCountryCode.text = addPhoneViewModel.phoneCountryCode;
                this.view.settings.tbxAddExtensionEditPhoneNumber.text = addPhoneViewModel.ext || "";
                this.view.settings.flxOptions.setVisibility(!addPhoneViewModel.isPrimary);                
                this.view.settings.flxOption3.setVisibility(!addPhoneViewModel.isPrimary);
                this.view.settings.flxWarning.setVisibility(!addPhoneViewModel.isPrimary);
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
              	this.view.settings.lblCheckBox3.skin = addPhoneViewModel.isPrimary ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
				this.view.settings.lblCheckBox3.text = addPhoneViewModel.isPrimary ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.settings.flxCheckBox3.onClick = function() {
                    this.toggleFontCheckBox(this.view.settings.lblCheckBox3);
                }.bind(this);
                this.view.settings.flxCheckBox4.onClick = function() {
                    this.toggleFontCheckBox(this.view.settings.lblCheckBox4);
                }.bind(this);
                this.view.settings.btnEditPhoneNumberCancel.onClick = addPhoneViewModel.onBack;
              	this.view.settings.lblCheckBox4.skin = addPhoneViewModel.recievePromotions ? ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
				this.view.settings.lblCheckBox4.text = addPhoneViewModel.recievePromotions ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                if (CommonUtilities.isCSRMode()) {
                    this.view.settings.btnEditPhoneNumberSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    this.view.settings.btnEditPhoneNumberSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                } else {
                    this.view.settings.btnEditPhoneNumberSave.onClick = function() {
                        if (this.validateEditPhoneNumberForm()) {
                            this.updatePhoneNumber(addPhoneViewModel, this.getEditPhoneFormData());
                            this.view.settings.flxHeader.setFocus(true);
                        }
                    }.bind(this);
                }
                this.view.settings.flxEditPhoneNumberButtons.setVisibility(true);
                this.checkUpdateEditPhoneForm();
              //  this.setEditPhoneServicesData(addPhoneViewModel.services);
            }
        },

        /**
         * Method to show data related to edit phone numbers scenario
         * @param {Object} - which sets the data
         */
        getPhoneNumberFromForm: function(data){
          var self = this;
          data.phoneNumber = self.view.settings.tbxPhoneNumber.text;
          if(applicationManager.getConfigurationManager().getCountryCodeFlag() == true){
            data.phoneCountryCode = self.view.settings.tbxEditPhoneNumberCountryCode.text;
          }else{
            data.phoneCountryCode = "";
          }
          return data;
        },
        getEditPhoneFormData: function() {
          var self = this;
            var data = {
                Extension: this.view.settings.lbxPhoneNumberType.selectedKey,
                isPrimary: this.view.settings.lblCheckBox3.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                receivePromotions: this.view.settings.lblCheckBox4.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED,
                extension: this.view.settings.tbxAddExtensionEditPhoneNumber.text.trim()
            }
            self.getPhoneNumberFromForm(data);
          /*  var services = {};
            this.view.settings.segEditPhoneNumberOption1.data.forEach(function(data) {
                services[data.id] = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED
            })
            data.services = services;*/
            return data;
        },

        /**
         * Method to show the details while editing the phone number
         */
        showEditPhonenumber: function() {
            this.view.settings.lblEditPhoneNumberHeading.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EditPhoneNumber");
          //  this.setEditPhoneNoOption2SegmentData();
            this.showViews(["flxEditPhoneNumbersWrapper"]);

            this.view.settings.btnDelete.isVisible = false;
            this.view.settings.btnEdit.isVisible = false;
            this.view.settings.lblExtensionUnEditable.isVisible = false;
            this.view.settings.lblCountryValue.isVisible = false;
            this.view.settings.lblTypeValue.isVisible = false;
            this.view.settings.lblPhoneNumberValue.isVisible = false;

            this.view.settings.flxRadioBtnUS.isVisible = false;
            this.view.settings.flxRadioBtnInternational.isVisible = false;
            this.view.settings.tbxPhoneNumber.isVisible = true;
            this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = false;
            this.view.settings.lbxPhoneNumberType.isVisible = true;
            this.view.settings.lblRadioInternational.isVisible = false;
            this.view.settings.lblRadioUS.isVisible = false;
            this.view.settings.flxOption2.setVisibility(false);
            this.view.settings.flxWarning.isVisible = true;
            this.view.settings.btnEditPhoneNumberCancel.isVisible = true;
            this.view.settings.flxEditPhoneNumberCountryCode.setVisibility(true);
            if(applicationManager.getConfigurationManager().getCountryCodeFlag() == true){
              this.view.settings.tbxEditPhoneNumberCountryCode.setVisibility(true);
            }else{
              this.view.settings.tbxEditPhoneNumberCountryCode.setVisibility(false);
            }
            this.view.settings.btnEditPhoneNumberSave.toolTip = kony.i18n.getLocalizedString('i18n.ProfileManagement.SAVE');
            this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.SAVE');
        },

        /**
         * Method to enable/disable button based on the edited phone number entered
         */
        checkUpdateEditPhoneForm: function() {
            if (!CommonUtilities.isCSRMode()) {
                var formdata = this.getEditPhoneFormData();
                if (formdata.phoneNumber === "") {
                    this.disableButton(this.view.settings.btnEditPhoneNumberSave);
                } else {
                    this.enableButton(this.view.settings.btnEditPhoneNumberSave);
                }
            }
        },

        /**
         * Method to set phone number according to the services
         * @param {Object} services - Array of accounts
         */
       /* setEditPhoneServicesData: function(services) {
            var scopeObj = this;

            function getServiceToggleListener(account, index) {
                return function() {
                    var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
                    data.lblCheckBox.skin = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
					data.lblCheckBox.text = data.lblCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
                }
            }

            var dataMap = {
                "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
                "flxCheckbox": "flxCheckbox",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts"
            };

            var data = services.map(function(account, index) {
                return {
                    "id": account.id,
                    "lblCheckBox": account.selected ? {"text":ViewConstants.FONT_ICONS.CHECBOX_SELECTED,"skin":ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN} : {"text":ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,"skin":ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN},
                    "lblAccounts": account.name,
                    "flxCheckbox": {
                        "onClick": getServiceToggleListener(account, index)
                    }
                }
            })
            this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
            this.view.settings.segEditPhoneNumberOption1.setData(data);
            this.view.forceLayout();
        },*/

        /**
         * Render The Profile Settings View
         * @param {Object} userProfielViewModel 
         * Values : name, dob, maskedSSN
         */

        updateUserProfileSetttingsView: function(data) {
            var userProfileViewModel = {
                name: data.userfirstname + " " + data.userlastname,
                dob: CommonUtilities.getFrontendDateString(data.dateOfBirth, "YYYY-MM-DD"),
                maskedSSN: '***-**-' + CommonUtilities.getLastFourDigit(data.ssn),
                userImage: data.userImageURL
            };

            this.showProfile();
            this.collapseAll();
            this.setSelectedSkin("Profile");
            this.expandWithoutAnimation(this.view.settings.flxProfileSettingsSubMenu);
            this.view.settings.lblNameValue.text = userProfileViewModel.name;
            this.view.settings.lblDOBValue.text = userProfileViewModel.dob;
            this.view.settings.lblSocialSecurityValue.text = userProfileViewModel.maskedSSN;
            this.view.customheader.customhamburger.activateMenu("Settings", "Profile Settings");
          if(userProfileViewModel.userImage){
             this.view.settings.imgProfile.src = userProfileViewModel.userImage;
          }else{
             this.view.settings.imgProfile.src = "profile_image_grey.png";
          }
           
        },

        /**
         * Method to show the profile management wrapper
         */
        showProfile: function() {
            this.showViews(["flxProfileWrapper"]);
        },
        /*********************Ankit Refactored***************************/

        /**
         * Method to set email got from backend
         * @param {Object} response - response of email JSON got from backend
         */
         setEmailsToLbx: function(response) {
            var data = [];
            var index = this.view.settings.segAccounts.selectedIndex[1];
            var accData = this.view.settings.segAccounts.data[index];
            var list = [];
            if (accData[1][0].email !== null && accData[1][0].email !== " " && accData[1][0].email !== undefined) {
                list.push("assignedEmail");
                list.push(accData[1][0].email);
                data.push(list);
            }
            for (var i = 0; i < response.length; i++) {
                list = [];
                if (accData[1][0].email !== response[i].Value) {
                    list.push(response[i].id);
                    list.push(response[i].Value);
                    data.push(list);
                }
            }
            this.view.settings.lbxEmailForReceiving.masterData = data;
            if (accData[1][0].email === null || accData[1][0].email === " " || accData[1][0].email === undefined) {
                this.view.settings.lbxEmailForReceiving.selectedKey = "email";
            } else {
                this.view.settings.lbxEmailForReceiving.selectedKey = "assignedEmail";
            }
        },
        /**
         * Method to enable/disable the button of terms and condition
         */
        showTermsAndConditions: function() {
            this.enableButton(this.view.btnSave);
            var currForm = this.view;
            currForm.flxTermsAndConditions.height = currForm.flxHeader.frame.height + currForm.flxContainer.frame.height + currForm.flxFooter.frame.height;
            this.view.flxTermsAndConditions.isVisible = true;
            this.view.lblTermsAndConditions.setFocus(true);
            if (CommonUtilities.isFontIconChecked(this.view.settings.lblEnableEStatementsCheckBox) === true && CommonUtilities.isFontIconChecked(this.view.settings.lblTCContentsCheckbox) === true) {
                if (CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox) === false) {
                    CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
                }
            } else {
                if (CommonUtilities.isFontIconChecked(this.view.lblTCContentsCheckbox) === true) {
                    CommonUtilities.toggleFontCheckbox(this.view.lblTCContentsCheckbox);
                }
            }
        },
        /**
         * Method to save details of account on click of save button
         */
        onSaveAccountDetails: function() {
            var self = this;
            var index = self.view.settings.segAccounts.selectedIndex[1];
            var data = self.view.settings.segAccounts.data[index];
            if (self.view.settings.tbxAccountNickNameValue.text === "" || self.view.settings.tbxAccountNickNameValue.text === null) {
                data.nickName = data.lblAccountHolderValue;
            } else {
                data.nickName = self.view.settings.tbxAccountNickNameValue.text;
            }
            if (data.external === true) {
                data = {
                    Account_id: data.accountIdPk,
                    NickName: data.nickName,
                    FavouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? "true" : "false",
                    external: true,
                };
            } else {
                data = {
                    accountID: self.view.settings.lblAccountNumberValue.text,
                    nickName: data.nickName,
                    favouriteStatus: self.view.settings.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? 1 : 0,
                    eStatementEnable: self.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? 1 : 0,
                    email: self.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ? self.view.settings.lbxEmailForReceiving.selectedKeyValue[1] : " ",
                };
            }
            FormControllerUtility.showProgressBar(self.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.savePreferredAccountsData(data);
        },
        /**
         * Method to prefill the data while editing prefered account
         * @param {boolean} errorScenario true/false
         */
        onPreferenceAccountEdit: function(errorScenario) {
          this.view.settings.lblIAccept.text=kony.i18n.getLocalizedString("i18n.ProfileManagement.IAccept");
            if (errorScenario === true) {
                this.view.settings.flxErrorEditAccounts.setVisibility(true);
                this.view.settings.lblErrorEditAccounts.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.weAreUnableToProcess");
            } else {
                this.view.settings.flxErrorEditAccounts.setVisibility(false);
            }
            var self = this;
            var section = this.view.settings.segAccounts.selectedIndex[0]
            var index = this.view.settings.segAccounts.selectedIndex[1];
            var data = this.view.settings.segAccounts.data[section][1][index];
            if (data.nickName === data.lblAccountHolderValue) {
                this.view.settings.tbxAccountNickNameValue.text = "";
            } else {
                this.view.settings.tbxAccountNickNameValue.text = data.nickName;
            }
            this.view.settings.lblFullNameValue.text = data.lblAccountHolderValue;
            this.view.settings.lblAccountTypeValue.text = data.lblAccountTypeValue;
            this.view.settings.lblAccountNumberValue.text = data.lblAccountNumberValue;
            this.view.settings.lblFavoriteEmailCheckBox.text = data.lblMarkAsFavouriteAccountCheckBoxIcon.text;
          	this.view.settings.lblFavoriteEmailCheckBox.skin = data.lblMarkAsFavouriteAccountCheckBoxIcon.skin;
            this.view.settings.lblFavoriteEmailCheckBox.onClick = function() {
                self.toggleFontCheckBox(self.view.settings.lblFavoriteEmailCheckBox);
            };
            if (data.external === false) {
                this.view.settings.lblTCContentsCheckbox.setVisibility(true);
                this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(true);
                this.view.settings.flxEnableEStatementsCheckBox.setVisibility(true);
                this.view.settings.btnTermsAndConditions.setVisibility(true);
                this.view.settings.lblEnableEStatementsCheckBox.text = data.lblEstatementCheckBoxIcon.text;
              	this.view.settings.lblEnableEStatementsCheckBox.skin = data.lblEstatementCheckBoxIcon.skin;
                this.view.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
              	this.view.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                this.view.settings.btnTermsAndConditions.onClick = this.showTermsAndConditions;
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getUserEmail();
                if (this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                    this.view.settings.flxTCCheckBox.setVisibility(true);
                    this.view.settings.flxTCContentsCheckbox.setVisibility(true);
                    this.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
                    this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                    this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";
                } else {
                    this.view.settings.flxTCCheckBox.setVisibility(false);
                    this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                    this.view.settings.lblTCContentsCheckbox.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                    this.view.settings.lblTCContentsCheckbox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                    this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
                }
            } else {
                this.view.settings.flxEmailForReceiving.setVisibility(false);
                this.view.settings.flxTCCheckBox.setVisibility(false);
                this.view.settings.flxTCContentsCheckbox.setVisibility(false);
                this.view.settings.lblTCContentsCheckbox.setVisibility(false);
                this.view.settings.flxPleaseNoteTheFollowingPoints.setVisibility(false);
                this.view.settings.flxEnableEStatementsCheckBox.setVisibility(false);
                this.view.settings.btnTermsAndConditions.setVisibility(false);
            }
            if (this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED && this.view.settings.lblTCContentsCheckbox.text !== ViewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                this.disableButton(this.view.settings.btnEditAccountsSave);
                this.disableButton(this.view.btnSave);
            } else {
                this.enableButton(this.view.settings.btnEditAccountsSave);
                this.view.settings.flxTCCheckBox.setVisibility(true);
                this.enableButton(this.view.btnSave);
            }
            this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
                self.toggleCheckBoxEditAccounts(self.view.settings.lblEnableEStatementsCheckBox);
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.settings.btnEditAccountsSave.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.settings.btnEditAccountsSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.settings.btnEditAccountsSave.focus = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.settings.btnEditAccountsSave.onClick = this.onSaveAccountDetails;
            }
            this.showViews(["flxEditAccountsWrapper"]);
            if (applicationManager.getConfigurationManager().editNickNameAccountSettings === "true") {
                this.view.settings.flxAccountNickName.setVisibility(true);
            } else {
                this.view.settings.flxAccountNickName.setVisibility(false);
            }
           if(data.email && this.view.settings.lblEnableEStatementsCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED){
                var response = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
                this.setEmailsToLbx(response);
                this.view.settings.flxEmailForReceiving.setVisibility(true);
                this.view.settings.lbxEmailForReceiving.setVisibility(true);
            
          }else{
                 this.view.settings.flxEmailForReceiving.setVisibility(false);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.AdjustScreen();
        },
        /**
         * Method to move a particular account upwards
         */
        onBtnMoveUp: function() {
            var section = this.view.settings.segAccounts.selectedIndex[0]
            var index = this.view.settings.segAccounts.selectedIndex[1];
            var segData = this.view.settings.segAccounts.data[section][1];
            var updatedAccounts = [{
                accountNumber: segData[index].lblAccountNumberValue,
                accountPreference: segData[index - 1].accountPreference.toString(),
            }, {
                accountNumber: segData[index - 1].lblAccountNumberValue,
                accountPreference: segData[index].accountPreference.toString(),
            }]
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.setAccountsPreference(updatedAccounts);
        },
        /**
         * Method to move a particular account downwards
         */
        onBtnMoveDown: function() {

            var section = this.view.settings.segAccounts.selectedIndex[0]
            var index = this.view.settings.segAccounts.selectedIndex[1];
            var segData = this.view.settings.segAccounts.data[section][1];

            var updatedAccounts = [{
                accountNumber: segData[index].lblAccountNumberValue,
                accountPreference: segData[index + 1].accountPreference.toString(),
            }, {
                accountNumber: segData[index+1].lblAccountNumberValue,
                accountPreference: segData[index].accountPreference.toString(),
            }]
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.setAccountsPreference(updatedAccounts);
        },
        accountTypeConfig: (function(){
            var accountTypeConfig ={};
            var ApplicationManager = require('ApplicationManager');
            applicationManager = ApplicationManager.getApplicationManager();
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.SAVING)]={
                sideImage: ViewConstants.IMAGES.SIDEBAR_TURQUOISE,
                skin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CHECKING)]= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_PURPLE,
                skin: ViewConstants.SKINS.CHECKINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_PURPLE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.CREDITCARD)]= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_YELLOW,
                skin: ViewConstants.SKINS.CREDIT_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.DEPOSIT)]= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BLUE,
                skin: ViewConstants.SKINS.DEPOSIT_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.MORTGAGE)]= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BROWN,
                skin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig[applicationManager.getTypeManager().getAccountTypeBackendValue(OLBConstants.ACCOUNT_TYPE.LOAN)]= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_BROWN,
                skin: ViewConstants.SKINS.MORTGAGE_CARD_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_YELLOW
            };
            accountTypeConfig['Default']= {
                sideImage: ViewConstants.IMAGES.SIDEBAR_TURQUOISE,
                skin: ViewConstants.SKINS.SAVINGS_SIDE_BAR,
                image: ViewConstants.IMAGES.ACCOUNT_CHANGE_TURQUOISE
            }
            return accountTypeConfig;
        })(),
        /**
         * Method to get configurations for accounts
         * @param {string} accountType account type
         * @returns
         */
        getConfigFor: function(accountType) {
            if (this.accountTypeConfig[accountType]) {
                return this.accountTypeConfig[accountType];
            } else {
                return this.accountTypeConfig.Default;
            }
        },
        /**
         * Method to generate accounts viewmodel for account preferences
         * @param {JSON} accountsData Data related to account
         * @returns JSON accounts data
         */
        generateViewModelAccounts: function(accountsData) {
            accountsData = JSON.parse(JSON.stringify(accountsData));
            var getAccountHolder=function(accHolder){
                try {
                    var nameObj = JSON.parse(accHolder);
                    return nameObj.fullname;
                }
                catch (exception) {
                    return accHolder;
                }
            }
            if (accountsData.isExternalAccount) {
                return {
                    "accountIdPK": accountsData.accountID,
                    "flxIdentifier": this.getConfigFor(accountsData.accountType).skin,
                    "AccountName": accountsData.AccountName,
                    "AccountHolder": accountsData.accountHolder,
                    "AccountNumber": accountsData.accountID,
                    "AccountType": accountsData.accountType,
                    "imgFavoriteCheckBox": (String(accountsData.favouriteStatus)!== "0") ? ViewConstants.IMAGES.CHECKED_IMAGE:ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgMenu": this.getConfigFor(accountsData.accountType).image,
                    "imgEStatementCheckBox": ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                    "fullName": "",
                    "nickName": accountsData.nickName,
                    "accountPreference": Number.MAX_SAFE_INTEGER,
                    "email": "",
                    "external": true
                }
            } else {
                return {
                    "flxIdentifier": this.getConfigFor(accountsData.accountType).skin,
                    "AccountName": accountsData.accountName,
                    "AccountHolder": getAccountHolder(accountsData.accountHolder),
                    "AccountNumber": accountsData.accountID,
                    "AccountType": accountsData.accountType,
                    "imgFavoriteCheckBox": accountsData.favouriteStatus == "1" ? ViewConstants.IMAGES.CHECKED_IMAGE:ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgEStatementCheckBox": (accountsData.eStatementEnable == "true" || accountsData.eStatementEnable == "1") ? ViewConstants.IMAGES.CHECKED_IMAGE:ViewConstants.IMAGES.UNCHECKED_IMAGE,
                    "imgMenu": this.getConfigFor(accountsData.accountType).image,
                    "fullName": accountsData.firstName + " " + accountsData.lastName,
                    "nickName": accountsData.nickName,
                    "accountPreference": Number(accountsData.accountPreference),
                    "email": accountsData.email,
                    "external": false,
                    "displayName":accountsData.displayName
                }
            }
        },
        /**
         * Method to show accounts preffered list
         * @param {Collection} viewModel Array od accounts jsons
         */
        showPreferredAccountsList: function(viewModel) {
            var scopeObj = this;
            this.view.customheader.customhamburger.activateMenu("Settings", "Account Settings");
            this.view.settings.tbxAccountNickNameValue.onKeyUp = function() {
                var regex = new RegExp("^[a-zA-Z0-9 _.-]*$");
                if (regex.test(scopeObj.view.settings.tbxAccountNickNameValue.text)) {
                    scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
                } else {
                    scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
                }
            };
            this.showAccounts();
            this.collapseAll();
            this.expandWithoutAnimation(this.view.settings.flxAccountSettingsSubMenu);
            if (viewModel.errorCase === true) {
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.settings.segAccounts.setVisibility(false);
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.view.flxDowntimeWarning.setVisibility(false);
                this.setAccountsSegmentData(viewModel.map(this.generateViewModelAccounts));
                this.setSelectedSkin("AccountPreferences");
                this.view.settings.segAccounts.setVisibility(true);
            }
        },
        /**
         * Method to map accounts data to segment
         * Array od accounts jsons
         * @param {*} viewModel
         */
        setAccountsSegmentData: function(viewModel) {
            var self = this;
            var dataMap = {
                "btnEdit": "btnEdit",
                "flxAccountHolder": "flxAccountHolder",
                "flxContent": "flxContent",
                "flxAccountHolderKey": "flxAccountHolderKey",
                "flxAccountHolderValue": "flxAccountHolderValue",
                "flxAccountListItemWrapper": "flxAccountListItemWrapper",
                "flxAccountName": "flxAccountName",
                "flxAccountNumber": "flxAccountNumber",
                "flxAccountNumberKey": "flxAccountNumberKey",
                "flxAccountNumberValue": "flxAccountNumberValue",
                "flxAccountRow": "flxAccountRow",
                "flxAccountType": "flxAccountType",
                "flxAccountTypeKey": "flxAccountTypeKey",
                "flxAccountTypeValue": "flxAccountTypeValue",
                "flxEStatement": "flxEStatement",
                "flxEStatementCheckBox": "flxEStatementCheckBox",
                "flxFavoriteCheckBox": "flxFavoriteCheckBox",
                "flxLeft": "flxLeft",
                "flxMarkAsFavorite": "flxMarkAsFavorite",
                "flxMenu": "flxMenu",
                "flxRight": "flxRight",
                "imgEStatementCheckBox": "imgEStatementCheckBox",
                "imgFavoriteCheckBox": "imgFavoriteCheckBox",
                "flxIdentifier": "flxIdentifier",
                "imgMenu": "imgMenu",
                "lblAccountHolderColon": "lblAccountHolderColon",
                "lblAccountHolderKey": "lblAccountHolderKey",
                "lblAccountHolderValue": "lblAccountHolderValue",
                "lblAccountName": "lblAccountName",
                "lblAccountNumberColon": "lblAccountNumberColon",
                "lblAccountNumberKey": "lblAccountNumberKey",
                "lblAccountNumberValue": "lblAccountNumberValue",
                "lblAccountTypeColon": "lblAccountTypeColon",
                "lblAccountTypeKey": "lblAccountTypeKey",
                "lblAccountTypeValue": "lblAccountTypeValue",
                "lblEStatement": "lblEStatement",
                "lblMarkAsFavorite": "lblMarkAsFavorite",
                "lblSepeartorlast": "lblSepeartorlast",
                "lblSeperator": "lblSeperator",
                "lblSeperator2": "lblSeperator2",
                "lblseperator3": "lblseperator3",
                "lblsepeartorfirst": "lblsepeartorfirst",
                "flxMoveUp": "flxMoveUp",
                "flxMoveDown": "flxMoveDown",
                "imgMoveUp": "imgMoveUp",
                "lblMove": "lblMove",
                "imgMoveDown": "imgMoveDown",
                "lblMarkAsFavouriteAccountCheckBoxIcon": "lblMarkAsFavouriteAccountCheckBoxIcon",
                "lblEstatementCheckBoxIcon": "lblEstatementCheckBoxIcon",
                "lblMoveUpIcon": "lblMoveUpIcon",
                "lblMoveDownIcon": "lblMoveDownIcon",
                "btn": "btn",
                "template":"template",
                "lblTransactionHeader":"lblTransactionHeader"
            };           
            var mapViewModel = function(data, index, viewModel) {
                var dataObject = {
                    "btn": {
                        text: ""
                    },
                    "accountPreference": data.accountPreference,
                    "accountIdPk": (data.external) ? data.accountIdPK : "",
                    "external": data.external,
                    "fullName": data.fullName,
                    "nickName": data.nickName,
                    "flxIdentifier": {
                        "skin": data.flxIdentifier
                    },
                    "lblTransactionHeader":data.AccountType,
                    "lblMarkAsFavouriteAccountCheckBoxIcon": {
                        text: data.imgFavoriteCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                      	skin: data.imgFavoriteCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ?ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                    },
                    "lblEstatementCheckBoxIcon": (data.external) ? {
                        isVisible: false
                    } : {
                        text: data.imgEStatementCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ? ViewConstants.FONT_ICONS.CHECBOX_SELECTED : ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                      	skin: data.imgEStatementCheckBox === ViewConstants.IMAGES.CHECKED_IMAGE ?ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN
                    },
                    "lblMoveUpIcon": index !== 0 ? {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        skin: ViewConstants.SKINS.FONT_ICON_BLUE_21PX
                    } : {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                    },
                    "lblMoveDownIcon": {
                        text: ViewConstants.FONT_ICONS.DISABLE_DOWN,
                        skin: ViewConstants.SKINS.FONT_ICON_BLUE_21PX
                    },
                    "lblAccountName": data.AccountName,
                    "lblAccountHolderKey": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountHolder'),
                    "lblAccountHolderColon": ": ",
                    "lblAccountHolderValue": data.AccountHolder,
                    "lblAccountNumberKey": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountNumber'),
                    "lblAccountNumberValue": data.AccountNumber,
                    "lblAccountTypeKey": kony.i18n.getLocalizedString('i18n.ProfileManagement.AccountType'),
                    "lblAccountNumberColon": ": ",
                    "lblAccountTypeValue": data.AccountType,
                    "imgEStatementCheckBox": data.imgEStatementCheckBox,
                    "lblAccountTypeColon": ": ",
                    "imgFavoriteCheckBox": data.imgFavoriteCheckBox,
                    "lblMarkAsFavorite": "Mark as Favorite Account",
                    "lblEStatement": (data.external) ? {
                        isVisible: false
                    } : "E-Statement Enabled",
                    "btnEdit": {
                        text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        onClick: self.onPreferenceAccountEdit
                    },
                    "flxMenu": {
                        "skin": "CopyslFbox",
                        "isVisible": applicationManager.getConfigurationManager().reOrderAccountPreferences === "true" ? true : false
                    },
                    "imgMenu": data.imgMenu,
                    "imgMoveUp": (data.external) ? {
                        isVisible: false
                    } : (index !== 0 ? ViewConstants.IMAGES.ACTIVE_UP : ViewConstants.IMAGES.DISABLE_UP),
                    "lblMove": (data.external) ? {
                        isVisible: false
                    } : "Move",
                    "imgMoveDown": (data.external) ? {
                        isVisible: false
                    } : ViewConstants.IMAGES.DISABLE_DOWN,
                    "lblSepeartorlast": "lblSepeartorlast",
                    "lblsepeartorfirst": "lblsepeartorfirst",
                    "flxMoveUp": (data.external) ? {
                        isVisible: false
                    } : {
                        onClick: function() {
                            if (index !== 0) {
                                self.onBtnMoveUp();
                            }
                        }
                    },
                    "flxMoveDown": (data.external) ? {
                        isVisible: false
                    } : {
                        onClick: function() {
                            self.onBtnMoveDown();
                        }
                    },
                    "lblSeperator": "lblSeperator",
                    "lblSeperator2": "lblSeperator2",
                    "lblseperator3": "lblseperator3",
                    "template": "flxAccountRow",
                    "email": data.email
                };
                if (CommonUtilities.isCSRMode()) {
                    dataObject.flxMoveUp.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    dataObject.flxMoveDown.onClick = CommonUtilities.disableButtonActionForCSRMode();
                }
                
                return dataObject;
            };

            var data = self.getDataWithSections(viewModel,mapViewModel);
            var len=data.length;
            var i = 0;
            if (len !== 0) {
            for(i=0;i<data.length;i++){
                var rowCount = data[i][1].length;
                
                    data[i][1][rowCount-1].imgMoveDown = ViewConstants.IMAGES.DISABLE_DOWN;
                      data[i][1][rowCount-1].lblMoveDownIcon = {
                          text: ViewConstants.FONT_ICONS.DISABLE_DOWN,
                          skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                      };
                    data[i][1][rowCount-1].flxMoveDown = function() {};

                    data[i][1][0].imgMoveUp = ViewConstants.IMAGES.DISABLE_UP;
                    data[i][1][0].lblMoveUpIcon = {
                        text: ViewConstants.FONT_ICONS.LABEL_IDENTIFIER,
                        skin: ViewConstants.SKINS.LABEL_MOVE_INACTIVE
                    };
                    data[i][1][0].flxMoveUp = function() {};

            }
            }
          
            this.view.settings.segAccounts.widgetDataMap = dataMap;
            this.view.settings.segAccounts.sectionHeaderSkin="segAccountListItemHeaderPM";
            if (kony.application.getCurrentBreakpoint() === 1024  || orientationHandler.isTablet) {
              for (i = 0; i < data.length; i++) {
                for(var j=0;j<data[i][1].length;j++)
                  data[i][1][j].template = "flxAccountRowTablet";
              }
            }; 
            this.view.settings.segAccounts.setData(data);
            this.view.settings.forceLayout();
            this.AdjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to create section header for accounts
         * @param {Collection} accounts List of accounts
         */
        getDataWithSections: function(viewModel,mapViewModel){
            var finalData={};
            var prioritizeAccountTypes = applicationManager.getTypeManager().getAccountTypesByPriority();  
                    
          
            viewModel.forEach(function(account,index,viewModel){
                if(account.external != true){
                  var accountType = applicationManager.getTypeManager().getAccountType(account.AccountType);
                    if(finalData.hasOwnProperty(accountType)){
                        finalData[accountType][1].push(mapViewModel(account,index,viewModel));
                    }else{
                        finalData[accountType]=[{
                            lblTransactionHeader:applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) != undefined ?
                            applicationManager.getTypeManager().getAccountTypeDisplayValue(accountType) :
                            accountType + " " + kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                            template:"flxAccountListItemHeaderPM"
                        },[mapViewModel(account,index,viewModel)]]
                    }
                }
            })
            var data=[];
            for(var key in prioritizeAccountTypes){
                var accountType=prioritizeAccountTypes[key];
                if(finalData.hasOwnProperty(accountType)){
                    data.push(finalData[accountType]);
                }
            }   
                return data;
        },
        /**
         * Method that gets called on click of save default accounts
         */
        onSaveDefaultAccounts: function() {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(scopeObj.view);
            if (scopeObj.view.settings.btnDefaultTransactionAccountEdit.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")) {
                var defaultAccounts = {
                    default_account_transfers: scopeObj.view.settings.lbxTransfers.selectedKey,
                    default_account_billPay: scopeObj.view.settings.lbxBillPay.selectedKey,
                    default_from_account_p2p: scopeObj.view.settings.lbxPayAPreson.selectedKey,
                    default_account_deposit: scopeObj.view.settings.lbxCheckDeposit.selectedKey
                };
                if (defaultAccounts.default_account_transfers === "undefined" || defaultAccounts.default_account_billPay === "undefined" || defaultAccounts.default_from_account_p2p === "undefined" || defaultAccounts.default_account_deposit === "undefined") {
                    self.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.profile.defaultAccountToBlank");
                    self.view.settings.txtDefaultTransactionAccountWarning.setVisibility(true);
                    self.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                    self.view.settings.forceLayout();
                    FormControllerUtility.hideProgressBar(scopeObj.view);
                } else {
                    scopeObj.view.settings.txtDefaultTransactionAccountWarning.setVisibility(true);
                    scopeObj.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.saveDefaultAccounts(defaultAccounts);
                    scopeObj.showDefaultScreen();
                    scopeObj.view.settings.btnDefaultTransactionAccountEdit.text = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                    scopeObj.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.billPay.Edit");
                }
            } else {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.getAccountsList();
                scopeObj.view.settings.lbxTransfers.setVisibility(true);
                scopeObj.view.settings.flxTransfersValue.setVisibility(false);
                scopeObj.view.settings.lbxBillPay.setVisibility(true);
                scopeObj.view.settings.flxBillPayValue.setVisibility(false);
                scopeObj.view.settings.lbxPayAPreson.setVisibility(true);
                scopeObj.view.settings.flxPayAPersonValue.setVisibility(false);
                scopeObj.view.settings.lbxCheckDeposit.setVisibility(true);
                scopeObj.view.settings.flxCheckDepositValue.setVisibility(false);
                scopeObj.view.settings.btnDefaultTransctionAccountsCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                scopeObj.view.settings.btnDefaultTransactionAccountEdit.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                scopeObj.view.settings.btnDefaultTransactionAccountEdit.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
                scopeObj.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(true);
            }
        },
        /**
         * Method to show UI according to the default account
         * @param {JSON} viewModel list of default accounts for different transactions
         */
        showDefaultUserAccount: function(viewModel) {
            this.showViews(["flxDefaultTransactionAccountWrapper"]);
            this.view.customheader.customhamburger.activateMenu("Settings", "Account Settings");
            if (viewModel.errorCase === true) {
                this.view.settings.flxDefaultTransactionButtons.setVisibility(false);
                this.view.settings.flxDefaultTransctionAccounts.setVisibility(false);
                this.view.settings.flxDefaultTransactionAccountWrapper.setVisibility(true);
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                this.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
                this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
            } else {
                this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
                this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
                this.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.YouhaveSelectedTheFollowingAccounts");
                this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
                this.showDefaultTransctionAccount(viewModel);
                this.setSelectedSkin("SetDefaultAccount");
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to show UI according to the default account
         * @param {JSON} viewModel list of default accounts for different transactions
         */
        showDefaultTransctionAccount: function(viewModel) {
            this.showViews(["flxDefaultTransactionAccountWrapper"]);
            var count = 0;
            for (var keys in viewModel) {
                if (viewModel[keys] !== 'None') count++;
            }
            if (count !== 0) {
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
                this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            } else {
                this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
                this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
            }
            this.view.settings.lblTransfersValue.text = viewModel.defaultTransferAccount;
            this.view.settings.lblBillPayValue.text = viewModel.defaultBillPayAccount;
            this.view.settings.lblPayAPersonValue.text = viewModel.defaultP2PAccount;
            this.view.settings.lblCheckDepositValue.text = viewModel.defaultCheckDepositAccount;
        },
        /**
         * Method to get account in Format AccountName-XXXX1234
         * @param {JSON} fromAccount Account information
         * @returns Array with account id and display name in format AccountName-XXXX1234
         */
        generateFromAccounts: function(fromAccount) {
            var getAccountDisplayName = function(fromAccount) {
                return fromAccount.accountName + '-XXXX' + fromAccount.accountID.slice(-4);
            };
            return [fromAccount.accountID, getAccountDisplayName(fromAccount)];
        },
        /**
         * Method to show the list of accounts
         * @param {Object} viewModel Model containing list of accounts related to transactions
         */
        showAccountsList: function(viewModel) {
            this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
            this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
            this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
            this.view.settings.txtDefaultTransactionAccountWarning.setVisibility(false);
            this.view.settings.lbxTransfers.setVisibility(true);
            this.view.settings.lbxBillPay.setVisibility(true);
            this.view.settings.lbxPayAPreson.setVisibility(true);
            this.view.settings.lbxCheckDeposit.setVisibility(true);
            this.view.settings.lbxTransfers.masterData = viewModel.TransfersAccounts.map(this.generateFromAccounts);
            this.view.settings.lbxTransfers.selectedKey = viewModel.defaultTransfersAccounts;
            this.view.settings.lbxBillPay.masterData = viewModel.BillPayAccounts.map(this.generateFromAccounts);
            this.view.settings.lbxBillPay.selectedKey = viewModel.defaultBillPayAccounts;
            this.view.settings.lbxPayAPreson.masterData = viewModel.P2PAccounts.map(this.generateFromAccounts);
            this.view.settings.lbxPayAPreson.selectedKey = viewModel.defaultP2PAccounts;
            this.view.settings.lbxCheckDeposit.masterData = viewModel.CheckDepositAccounts.map(this.generateFromAccounts);
            this.view.settings.lbxCheckDeposit.selectedKey = viewModel.defaultCheckDepositAccounts;
            this.view.settings.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
      
      
      getUsernameRules : function(data){
          FormControllerUtility.hideProgressBar(this.view);
            if (data) {
                this.view.settings.rtxRulesUsername.text = data.usernamepolicies.content;//
            } else {
                CommonUtilities.showServerDownScreen();
            }
        },
        responsiveViews:{},
        initializeResponsiveViews:function(){
            this.responsiveViews["flxProfileWrapper"] = this.isViewVisible("flxProfileWrapper");
            this.responsiveViews["flxNameChangeRequestWrapper"] = this.isViewVisible("flxNameChangeRequestWrapper");
            this.responsiveViews["flxPhoneNumbersWrapper"] = this.isViewVisible("flxPhoneNumbersWrapper");
            this.responsiveViews["flxEditPhoneNumbersWrapper"] = this.isViewVisible("flxEditPhoneNumbersWrapper");
            this.responsiveViews["flxChangeLanguageWrapper"] = this.isViewVisible("flxChangeLanguageWrapper");
            this.responsiveViews["flxAddPhoneNumbersWrapper"] = this.isViewVisible("flxAddPhoneNumbersWrapper");
            this.responsiveViews["flxEmailWrapper"] = this.isViewVisible("flxEmailWrapper");
            this.responsiveViews["flxEditEmailWrapper"] = this.isViewVisible("flxEditEmailWrapper");
            this.responsiveViews["flxAddNewEmailWrapper"] = this.isViewVisible("flxAddNewEmailWrapper");
            this.responsiveViews["flxAddressesWrapper"] = this.isViewVisible("flxAddressesWrapper");
            this.responsiveViews["flxAddNewAddressWrapper"] = this.isViewVisible("flxAddNewAddressWrapper");
            this.responsiveViews["flxEditAddressWrapper"] = this.isViewVisible("flxEditAddressWrapper");
            this.responsiveViews["flxAccountsWrapper"] = this.isViewVisible("flxAccountsWrapper");
            this.responsiveViews["flxEditAccountsWrapper"] = this.isViewVisible("flxEditAccountsWrapper");
            this.responsiveViews["flxDefaultTransactionAccountWrapper"] = this.isViewVisible("flxDefaultTransactionAccountWrapper");
            this.responsiveViews["flxUsernameAndPasswordWrapper"] = this.isViewVisible("flxUsernameAndPasswordWrapper");
            this.responsiveViews["flxEditUsernameWrapper"] = this.isViewVisible("flxEditUsernameWrapper");
            this.responsiveViews["flxAcknowledgementWrapper"] = this.isViewVisible("flxAcknowledgementWrapper");
            this.responsiveViews["flxEditPasswordWrapper"] = this.isViewVisible("flxEditPasswordWrapper");
            this.responsiveViews["flxSecuritySettingVerificationWrapper"] = this.isViewVisible("flxSecuritySettingVerificationWrapper");
            this.responsiveViews["flxUsernameVerificationWrapper"] = this.isViewVisible("flxUsernameVerificationWrapper");
            this.responsiveViews["flxUsernameVerificationWrapper2"] = this.isViewVisible("flxUsernameVerificationWrapper2");
            this.responsiveViews["flxEditSecuritySettingsWrapper"] = this.isViewVisible("flxEditSecuritySettingsWrapper");
            this.responsiveViews["flxAnswerSecurityQuestionsWrapper"] = this.isViewVisible("flxAnswerSecurityQuestionsWrapper");
            this.responsiveViews["flxSecureAccessCodeWrapper"] = this.isViewVisible("flxSecureAccessCodeWrapper");
            this.responsiveViews["flxTransactionalAndPaymentsAlertsWrapper"] = this.isViewVisible("flxTransactionalAndPaymentsAlertsWrapper");
        },
        isViewVisible: function(container){
            return this.view.settings[container].isVisible;
        },
        onBreakpointChange : function(width){
            var scope= this;
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChange(width);
            this.setupFormOnTouchEnd(width);
            var views = Object.keys(this.responsiveViews);
            views.forEach(function (e) {
                scope.view.settings[e].isVisible = scope.responsiveViews[e];
            });
        },
        setupFormOnTouchEnd: function(width){
          if(width==640){
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }else{
            if(width==1024){
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }else{
                this.view.onTouchEnd = function(){
                  hidePopups();   
                } 
              }
              var userAgent = kony.os.deviceInfo().userAgent;
              if (userAgent.indexOf("iPad") != -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }
          }
        },
        nullifyPopupOnTouchStart: function(){
        },
        /**
         * Method to show security questions screen
         * @param {} 
         */
        showSecurityQuestionsScreen: function(){
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            this.collapseAll();
            this.expand(this.view.settings.flxSecuritySettingsSubMenu);
            this.view.customheader.customhamburger.activateMenu("Settings", "Security Settings");
            scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
            scopeObj.setSelectedSkin("SecurityQuestions");
            scopeObj.view.settings.btnEditSecuritySettingsCancel.onClick = function() {
                scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule").presentationController.checkSecurityQuestions();
                scopeObj.setSelectedSkin("SecurityQuestions");
                scopeObj.selectedQuestions = {
                    ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
                    key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
                };
                scopeObj.selectedQuestionsTemp = {
                    securityQuestions: [],
                    flagToManipulate: []
                };
            };
            scopeObj.view.settings.tbxEnterOTP.onKeyUp = function() {
                scopeObj.checkOTP();
            };
            scopeObj.showOTPAction();
        },
    };
});