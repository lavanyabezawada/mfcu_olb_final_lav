define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxCallUsAt **/
    AS_FlexContainer_cf40254229984a1aada2e2d45d4c9b39: function AS_FlexContainer_cf40254229984a1aada2e2d45d4c9b39(eventobject) {
        var self = this;
        //this.addInternalBankAccount();
        this.presenter.showSameBankAccounts();
    },
    /** onClick defined for flxEmailUsAt **/
    AS_FlexContainer_bb2c13318bfe408e842a4b62ed54df45: function AS_FlexContainer_bb2c13318bfe408e842a4b62ed54df45(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** init defined for frmNewUserOnboarding **/
    AS_Form_b8bafac5aa9e430f935bf8d6235f8061: function AS_Form_b8bafac5aa9e430f935bf8d6235f8061(eventobject) {
        var self = this;
        this.initialize();
    },
    /** postShow defined for frmNewUserOnboarding **/
    AS_Form_e840a3c0ca25436995a4612fe60df7ad: function AS_Form_e840a3c0ca25436995a4612fe60df7ad(eventobject) {
        var self = this;
        this.postShowfrmNewUserOnboarding();
    },
    /** onDeviceBack defined for frmNewUserOnboarding **/
    AS_Form_jd58a410ac934d809db2bdc097d55173: function AS_Form_jd58a410ac934d809db2bdc097d55173(eventobject) {
        var self = this;
        kony.print("on device back");
    }
});