/**
 * Module representing a NUO form.
 * @module frmNewUserOnBoardingController
 */
define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    return /** @alias module:frmNewUserOnBoardingController */ {

        /** updates the present Form based on required function.
        * @param {nuoView[]} nuoView 
        */
        //eslint-disable-next-line 
        updateFormUI: function (nuoView) {
            this.view.flxDownTimeWarning.setVisibility(false);
            if (nuoView.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (nuoView.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (nuoView.serverError) {
                this.showServerError(nuoView.serverError);
            }
            if (nuoView.productSelection) {
                this.showProductSelection(nuoView.productSelection);
            }
            if (nuoView.createNewCustomer) {
                this.showEnterOTP();
            }
            if (nuoView.customerExists) {
                this.showPhoneAlreadyOptedScreen(true);
            }
            if (nuoView.continueApplication) {
                this.showPhoneAlreadyOptedScreen(false);
            }
            if (nuoView.OTPValidated) {
                this.showEligibilityScreen();
            }
            if (nuoView.NUOLanding) {
                this.showNUOEnterPhoneScreen();
            }
            if (nuoView.showUserInformationStep) {
                this.showUserInformationStep();
            }
            if (nuoView.showUserInformationViewModel) {
                this.showUserInformationForm(nuoView.showUserInformationViewModel);
            }
            if (nuoView.showEmploymentInformationViewModel) {
                this.showEmploymentInformationForm(nuoView.showEmploymentInformationViewModel);
            }
            if (nuoView.showFinancialInformationViewModel) {
                this.showFinancialInformationForm(nuoView.showFinancialInformationViewModel);
            }
            if (nuoView.continueOrResetApplication) {
                this.showOpenNewAccount(nuoView.continueOrResetApplication);
            }
            if (nuoView.showUploadDocuments) {
                this.showUploadDocumentsScreen1(nuoView.showUploadDocuments.employementStatus);
            }
            if (nuoView.documentsSaved) {
                this.getIdentityVerification();
            }
            if (nuoView.identityVerificationQuestions) {
                this.showIdentityVerification(nuoView.identityVerificationQuestions);
            }
            if (nuoView.identityVerificationSuccess) {
                this.performCreditCheck();
            }
            if (nuoView.identityVerificationError) {
                this.showIdentityVerificationError();
            }
            if (nuoView.userCreditCheckSuccess) {
                this.showDigitalSignatureFlow();
            }
            if (nuoView.userCreditCheckError) {
                this.showCreditCheckErrorFlow();
            }
            if (nuoView.uploadSignatureSuccess) {
                this.showAcknowledgementScreen();
            }
            if (nuoView.enrollError) {
                this.showEnrollError(nuoView.enrollError);
            }
            if(nuoView.verifyOtpError)
            {
                this.showVerifyOtpError(nuoView.verifyOtpError);
            }
            if (nuoView.loginError) {
                this.showLoginError(nuoView.loginError);
            }
            if (nuoView.uploadSignature) {
                this.showUploadSignatureScreen(nuoView.uploadSignature);
            }
            if (nuoView.customerLoginError) {
                this.showCustomerLoginError(nuoView.customerLoginError);
            }
            if (nuoView.usernamepasswordrules) {
                this.showEnrollForm(nuoView.usernamepasswordrules);
            }
            if (nuoView.sendMetrics) {
                this.sendCustomMetrics(nuoView.sendMetrics);
            }
        },


        /**
         * used to send the custom metrics
         * @param {object} viewModel viewModel
         */
        sendCustomMetrics: function (viewModel) {
            if (applicationManager.getConfigurationManager().isCustomMetricsEnabled === "true") {
                kony.mvc.MDAApplication.getSharedInstance().appContext.dateOfBirth = viewModel.data.dateOfBirth;
                kony.setUserID(kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName);
                CommonUtilities.sendCustomMetrics(this);
            }
        },
        /**
         * Handle server error, shows serverFlex
         * @param {object} serverError error
         */
        showServerError: function (serverError) {
            this.view.flxDownTimeWarning.setVisibility(true);
            this.view.rtxDownTimeWarning.setVisibility(true);
            this.view.rtxDownTimeWarning.setFocus(true);
        },

        /**
   * Method to load and returns the NUO Module Object
  * @returns {object} Method to load and returns the NUO Module Object
  */
        loadNuoModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NUOModule");
        },

        /**
         * Entry Point Method for Product Selection 
         * @param {object} productSelectionViewModel productSelectionViewModel 
         */
        showProductSelection: function (productSelectionViewModel) {
            this.hideAll();
            this.showProductSelectionUI();
            this.showTopMenu();
            if (productSelectionViewModel.isRevisiting) {
                this.view.flxWelcomeMessage.setVisibility(false);
            }
            else {
                this.view.flxWelcomeMessage.setVisibility(true);
                this.view.lblWelcome.text = kony.i18n.getLocalizedString("i18n.accounts.welcome") + " " + productSelectionViewModel.username + "!";
            }
            this.setProductSelectionData(productSelectionViewModel.products, productSelectionViewModel.selectedProducts);
            this.validateProductSelection();
            this.view.btnContinueProceed.onClick = this.saveProducts.bind(this, productSelectionViewModel.products);
            this.AdjustScreen();
        },

        /**
         * used to save the products
         * @param {object}  productList product List
         */
        saveProducts: function (productList) {
            var selectedProductObjects = [];
            this.view.segProductsNUO.data.forEach(function (segementDataItem, index) {
                if (segementDataItem.lblCheckBox === "C") {
                    selectedProductObjects.push(productList[index])
                }
            })
            this.loadNuoModule().presentationController.saveUserProducts(selectedProductObjects);
        },

        /**
         * Binds Products to Product Segment
         * @param {object} products model objects array 
         * @param {object} selectedProducts selectedProducts
         * @returns {void} - None
         */
        setProductSelectionData: function (products, selectedProducts) {
            var selectedProductIds = selectedProducts.map(function (selectedProduct) {
                return selectedProduct.productId
            })
            var self = this;
            var widgetDataMap = {
                "imgflxAccounts": "imgflxAccounts",
                "lblAccountsName": "lblAccountsName",
                "lblFeature": "lblFeature",
                "flxRule1": "flxRule1",
                "lblRule1": "lblRule1",
                "flxRule2": "flxRule2",
                "imgRule1": "imgRule1",
                "imgRule2": "imgRule2",
                "imgRule3": "imgRule3",
                "lblRule2": "lblRule2",
                "flxRule3": "flxRule3",
                "lblRule3": "lblRule3",
                "lblMonthlyServiceFee": "lblMonthlyServiceFee",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts",
                "btnKnowMore": "btnKnowMore",
                "flxCheckBox": "flxCheckBox",
                "template":"template"

            }
            this.view.segProductsNUO.widgetDataMap = widgetDataMap;
            var data = products.map(function (product, index) {
                return {
                    "imgflxAccounts": ViewConstants.IMAGES.NUO_SAVINGS,
                    "lblAccountsName": product.productName,
                    "lblFeature": kony.i18n.getLocalizedString("i18n.NUO.Features"),
                    "lblRule1": product.productDescription,
                    "lblRule2": product.features,
                    "lblRule3": product.info,
                    "imgRule1": ViewConstants.IMAGES.PAGEOFFDOT,
                    "imgRule2": ViewConstants.IMAGES.PAGEOFFDOT,
                    "imgRule3": ViewConstants.IMAGES.PAGEOFFDOT,
                    "lblMonthlyServiceFee": product.rates,
                    "btnKnowMore": {
                        "text": kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
                        "onClick": self.showProductDetail.bind(this, product, index)
                    },
                    "lblAccounts": kony.i18n.getLocalizedString("i18n.NUO.SlectProduct"),
                    "lblCheckBox": selectedProductIds.indexOf(product.productId) > -1 ? "C" : "D",
                    "flxCheckBox": {
                        "onClick": self.toggleProductSelectionCheckbox.bind(this, index)
                    },
                    "template":"flxSelectProducts"
                }
            });
            if(kony.application.getCurrentBreakpoint() === 640){
               for(var i=0;i<data.length;i++){
		        data[i].template = "flxSelectProductsMobile";
               }
            };
            this.view.segProductsNUO.setData(data);
            this.AdjustScreen();
        },
        /**
  *AdjustScreen- function to adjust the footer
  */
        AdjustScreen: function () {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * Product Selection UI 
         */
        showProductSelectionUI: function () {
            this.hideAll();
            this.view.flxMainContainer.setVisibility(true);
            this.view.flxAccounts.setVisibility(false);
            this.view.flxAccountsView.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = "Accounts";
            this.view.flxColorLine.width = "14.28%";
            this.AdjustScreen();
        },

        /**
         * Toggle Select Product Checkbox of Select Products Segment
         * @param {string} index index of row
         */
        toggleProductSelectionCheckbox: function (index) {
            var data = this.view.segProductsNUO.data[index];
            data.lblCheckBox = data.lblCheckBox === "C" ? "D" : "C";
            if(kony.application.getCurrentBreakpoint() === 640){
               for(var i=0;i<data.length;i++){
		        data[i].template = "flxSelectProductsMobile";
               }
            };
            this.view.segProductsNUO.setDataAt(data, index);
            this.validateProductSelection();
        },
        /**
         *  Select Product Checkbox of Select Products Segment
         * @param {string} index index of row
         */
        selectProductSelectionCheckbox: function (index) {
            var data = this.view.segProductsNUO.data[index];
            data.lblCheckBox = "C";
            if(kony.application.getCurrentBreakpoint() === 640){
               for(var i=0;i<data.length;i++){
		        data[i].template = "flxSelectProductsMobile";
               }
            };
            this.view.segProductsNUO.setDataAt(data, index);
            this.validateProductSelection();
        },

        /**
         * used to validate the product
         */
        validateProductSelection: function () {
            var count = this.view.segProductsNUO.data.filter(function (item) {
                return item.lblCheckBox === "C";
            }).length;
            if (count === 0) {
                FormControllerUtility.disableButton(this.view.btnContinueProceed);
            }
            else {
                FormControllerUtility.enableButton(this.view.btnContinueProceed);
            }
        },


        /**
         * Show Product Detail
         * @param {object} product NewUserProduct Model Object 
         * @param {string} index of which row is shown in segment 
         */
        showProductDetail: function (product, index) {
            this.showProductDetailUI();
            var data = [product.productDescription,
            product.features,
            product.info,
            product.rates,
            product.features
            ];

            var widgetDataMap = {
                "lblRule1": "lblRule1",
                "imgRule1": "imgRule1",
                "template":"template"
            }

            var segData = data.map(function (text) {
                return {
                    "lblRule1": text,
                    "imgRule1": ViewConstants.IMAGES.PAGEOFFDOT,
                    "template": "flxSelectProducts"
                }
            });
            if(kony.application.getCurrentBreakpoint() === 640){
               for(var i=0;i<segData.length;i++){
		        segData[i].template = "flxSelectProductsMobile";
               }
            };
            this.view.AccountsDescription.segFeatures.setData(segData);
            this.view.AccountsDescription.imgflxAccountsDescription.src = ViewConstants.IMAGES.NUO_SAVINGS;
            this.view.AccountsDescription.lblAccountName.text = product.productName;
            this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function () {
                this.showProductSelectionUI();
            }.bind(this);
            this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function () {
                this.selectProductSelectionCheckbox(index)
                this.showProductSelectionUI();
            }.bind(this);
            this.AdjustScreen();
        },

        /**
         * Show Product Detail UI
         * @param {object} product Product Model Object
         */
        showProductDetailUI: function (product) {
            // this.view.flxMainContainer.setVisibility(false);
            this.hideAll();
            this.view.flxAccounts.setVisibility(true);
        },
        /**
         * shows Eligibility Screen for the User and handles the action
         */
        showEligibilityScreen: function () {
            var scopeObj = this;
            this.showEligibilityScreenUI();
            this.radioBtnAction(this.getEgligibilityRadioButtonFlexes(), this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage1);
            this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.text = "";
            this.view.ChooseEligibilityCriteria.btnSendOTPCancel.onClick = function () {
                scopeObj.navigateToLoginScreen();
            };
            this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.onKeyUp = this.validateEgligibilityForm.bind(this);
            this.view.ChooseEligibilityCriteria.btnProceed.onClick = function () {
                scopeObj.loadNuoModule().presentationController.showEnrollForm();
            }
            this.bindEgligibilityRadioButtonActions();
            this.AdjustScreen();
        },

        /**
         * Sets Eglibility Radio Button Actions
         */
        bindEgligibilityRadioButtonActions: function () {
            var scopeObj = this;
            this.getEgligibilityRadioButtonFlexes().forEach(function (radioFlex) {
                radioFlex.onClick = function () {
                    scopeObj.radioBtnAction(scopeObj.getEgligibilityRadioButtonFlexes(), radioFlex);
                    scopeObj.validateEgligibilityForm();
                }
            })
        },

        /**
         * Validates Egligibility Form 
         */
        validateEgligibilityForm: function () {
            var valid = false;
            this.getEgligibilityRadioButtonFlexes().forEach(function (radioFlex) {
                if (radioFlex.widgets()[0].text === "R") {
                    valid = true;
                }
            })
            var companyName = this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.text;
            if (!valid) {
                FormControllerUtility.disableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
            else if (this.view.ChooseEligibilityCriteria.lblRadioBtn2.text === "R" && companyName === "") {
                FormControllerUtility.disableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
            else {
                FormControllerUtility.enableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
        },

        /**
         * Returns array of Radio Flexes of egligibility in order.
         * @returns {void} - None
         */
        getEgligibilityRadioButtonFlexes: function () {
            return [
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage1,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage2,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage3,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage4
            ];
        },

        /**
         * Shows the egligibility UI
         */
        showEligibilityScreenUI: function () {
            this.hideAll();
            this.view.flxChooseEligibilityCriteria.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaHeading.text;
            this.AdjustScreen();
        },

        /**
         * Entry point method of enroll form 
         * @param {object} usernameAndPasswordRules usernameAndPasswordRules
         */
        showEnrollForm: function (usernameAndPasswordRules) {
            var scopeObj = this;
            this.bindEnrollFieldActions();
            this.showUserNameRules(usernameAndPasswordRules);
            this.showPasswordRules(usernameAndPasswordRules);
            this.view.CreateUsernamePassword.btnCreateUsernamePasswordCancel.onClick = function () {
                scopeObj.navigateToLoginScreen();
            };
            this.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed.onClick = this.enrollUser.bind(this);
            this.view.CreateUsernamePassword.tbxEnterUsername.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxRulesUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterUsername.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxRulesUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterPassword.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterPassword.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.showEnrollFormUI();
            this.resetEnrollForm();
        },

        /**
         * Enroll the user - Checks validity and sends data to presentation controller
         */
        enrollUser: function () {
            if (this.isEnrollDataValid()) {
                this.loadNuoModule().presentationController.enrollUser(this.getEnrollData());
            }
        },

        /**
         * Checks if enroll data is valid or not
         * @param {object} products model objects array 
         * @returns {boolean} - True if valid and false if not. 
         */
        isEnrollDataValid: function() {
            var enrollData = this.getEnrollData();
            var scopeObj = this;
            function showError(text, widegetId) {
                scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = text;
                widegetId.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
                scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(true);
                scopeObj.view.forceLayout();
            }
            scopeObj.view.CreateUsernamePassword.flxEnterEmailId.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            scopeObj.view.CreateUsernamePassword.flxEnterUsername.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            scopeObj.view.CreateUsernamePassword.flxEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            scopeObj.view.CreateUsernamePassword.flxEnterReEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
			this.AdjustScreen();
            if (!this.isValidEmail(enrollData.email)) {
                showError("Not a valid email",scopeObj.view.CreateUsernamePassword.flxEnterEmailId);
                return false;
            } else if (!this.isUsernameValid(enrollData.username)) {
                showError("Not a valid username",scopeObj.view.CreateUsernamePassword.flxEnterUsername);
                return false;
            } else if (enrollData.username === enrollData.email) {
                showError("Username cannot be same as Email",scopeObj.view.CreateUsernamePassword.flxEnterUsername);
                return false;
            } else if (enrollData.username === enrollData.password) {
                showError("Password cannot be same as username",scopeObj.view.CreateUsernamePassword.flxEnterPassword);
                return false;
            } else if (!this.isPasswordValid(enrollData.password)) {
                showError("Not a valid password",scopeObj.view.CreateUsernamePassword.flxEnterPassword);
                return false;
            } else if (enrollData.password !== this.view.CreateUsernamePassword.tbxReEnterPassowrd.text) {
                showError("Password doesn't match. Please enter it again",scopeObj.view.CreateUsernamePassword.flxEnterPassword);
                showError("Password doesn't match. Please enter it again",scopeObj.view.CreateUsernamePassword.flxEnterReEnterPassword);
                return false;
            }
            this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
            return true;
        },

        /**
         * used to show the userName rules
         * @param {object} data data
         */
        showUserNameRules: function (data) {
            var rules = [];
            for (var i = 0; i < data.length; i++) {
                var policyName = data[i].policyName;
                if (CommonUtilities.substituteforIncludeMethod(policyName, "Username Policy for End Customers")) {
                    rules.push(data[i].policyDescription);
                }
            }
            if (rules.length >= 0) {
                this.view.CreateUsernamePassword.rtxRulesUsername.text = rules;
            }
        },

        /**
         * used to show the password rules
         * @param {object} data data
         */
        showPasswordRules: function (data) {
            this.view.CreateUsernamePassword.rtxRulesPassword.text = CommonUtilities.getPasswordPolicies(data);
        },
        /**
         * Returns enroll data from fields
         * @returns {object} - Object contains data from fields
         */
        getEnrollData: function () {
            return {
                username: this.view.CreateUsernamePassword.tbxEnterUsername.text,
                password: this.view.CreateUsernamePassword.tbxEnterPassword.text,
                phone: this.view.SendOTP.tbxOTP.text,
                email: this.view.CreateUsernamePassword.tbxNewEmailId.text
            }
        },

        /**
         * Binds Enroll field key up acion to validate function
         */
        bindEnrollFieldActions: function () {
            var scopeObj = this;
            this.view.CreateUsernamePassword.tbxEnterUsername.onKeyUp = function(){
            	scopeObj.view.CreateUsernamePassword.flxEnterUsername.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            	scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
				scopeObj.AdjustScreen();
            	scopeObj.validateEnrollFields();
            };
            this.view.CreateUsernamePassword.tbxNewEmailId.onKeyUp = function(){
               scopeObj.view.CreateUsernamePassword.flxEnterEmailId.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
	           scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
		       scopeObj.AdjustScreen();           
               scopeObj.validateEnrollFields();
            };
            this.view.CreateUsernamePassword.tbxEnterPassword.onKeyUp = function(){
          	    scopeObj.view.CreateUsernamePassword.flxEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            	scopeObj.view.CreateUsernamePassword.flxEnterReEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
            	scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
				scopeObj.AdjustScreen();
                scopeObj.validateEnrollFields();
            };
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onKeyUp = function(){
	            scopeObj.view.CreateUsernamePassword.flxEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
    	        scopeObj.view.CreateUsernamePassword.flxEnterReEnterPassword.skin = ViewConstants.SKINS.COMMON_FLEX_NOERROR;
        	    scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
				scopeObj.AdjustScreen();
                scopeObj.validateEnrollFields();
            };
        },

        /**
         * Show enroll form UI
         */
        showEnrollFormUI: function () {
            this.hideAll();
            this.view.flxCreateUsernamePassowrd.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.CreateUsernamePassword.flxCreateUsernamePasswordHeading.text;
            this.view.CreateUsernamePassword.imgValidEmail.setVisibility(false);
            this.view.CreateUsernamePassword.imgUsername.setVisibility(false);
            this.view.CreateUsernamePassword.imgPassword.setVisibility(false);
            this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(false);
            this.view.forceLayout();
            this.AdjustScreen();
        },

        /**
         * Resets enroll form UI 
         * @returns {void} - None
         * @throws {void} - None
         */
        resetEnrollForm: function () {
            this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = "";
            this.view.CreateUsernamePassword.tbxNewEmailId.text = "";
            this.view.CreateUsernamePassword.tbxEnterUsername.text = "";
            this.view.CreateUsernamePassword.tbxEnterPassword.text = "";
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.text = "";
            FormControllerUtility.disableButton(this.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
        },

        /**
         * shows phone number required Screen
         */
        showNUOEnterPhoneScreen: function () {
            var scopeObj = this;
            this.view.SendOTP.tbxOTP.text = "";
            this.showNUOEnterPhoneScreenUI();
            this.hideTopMenu();
            this.view.SendOTP.tbxOTP.onKeyUp = this.checkPhoneNumber.bind(this);
            this.view.SendOTP.btnSendOTPCancel.onClick = this.navigateToLoginScreen.bind(this);
            this.view.SendOTP.btnSendOTPProceed.onClick = function () {
                scopeObj.loadNuoModule().presentationController.checkPhoneNumber(scopeObj.view.SendOTP.tbxOTP.text);
            };
        },

        /**
         * Show NUO Phone Screen UI 
         */
        showNUOEnterPhoneScreenUI: function () {
            this.hideAll();
            FormControllerUtility.disableButton(this.view.EnterSecureAccessCode.btnSendOTPProceed);
            this.showViews(["flxSendOTP"]);
        },

        /**
         * Navigate to login screen
         */
        navigateToLoginScreen: function () {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.showLoginScreen();
        },

        /**
         * used to check valid OTP
         * @param {string} otpString otpString
         * @returns {otpString} otpString 
         */
        isValidOTP: function (otpString) {
            return /^\d{6}$/.test(otpString);
        },

        /**
         * Callback for onKeyUp of any OTP
         * @param {object} widget widget
         * @returns {void} - None
         * @throws {void} - None
         */
        onKeyUpOtp: function (widget) {
            var scopeObj = this;
            if (this.isValidOTP(this.getOTP())) {
                FormControllerUtility.enableButton(scopeObj.view.EnterSecureAccessCode.btnSendOTPProceed);
            }
            else {
                this.changeFocus(widget);
                FormControllerUtility.disableButton(scopeObj.view.EnterSecureAccessCode.btnSendOTPProceed);
            }
        },

        /**
         * Change focus of OTP Field to next/previous OTP Field
         * @param {object} currentField currentField
         */
         changeFocus: function(currentField) {
            var fields = this.getOTPTextFields();
            if (currentField.text.length === 0) {
                var previousField = fields[fields.indexOf(currentField) - 1];
                if (previousField) {
                    previousField.setFocus();
                }
            } else {
                var nextField = fields[fields.indexOf(currentField) + 1];
				if(currentField.text.length === 2 && nextField)
				{
					var currentText = currentField.text;
					nextField.setFocus();
					if(nextField.text.length===1)
					{													
						currentField.text = currentText[1];
						nextField.setFocus();						
					}else{
						currentField.text = currentText[0];
						nextField.text = currentText[1];
						this.onKeyUpOtp(currentField);
						if(currentField.id === tbxEnterSecureAccessCodeDigit5)
							FormControllerUtility.enableButton(this.view.EnterSecureAccessCode.btnSendOTPProceed);
					}	
				}
					else if (nextField) {
						nextField.setFocus();
					} 
			}
        },

        /**
         * Show Enter OTP Screen
         */
        showEnterOTP: function() {
            var scopeObj = this;
            scopeObj.showEnterOTPUI();
            scopeObj.resetOTP();
            this.view.EnterSecureAccessCode.btnResend.onClick = function() {
                scopeObj.loadNuoModule().presentationController.requestOTP();
            }
            this.view.EnterSecureAccessCode.btnSendOTPProceed.onClick = function() {
                scopeObj.loadNuoModule().presentationController.validateOTP(scopeObj.getOTP());
            };
            this.view.EnterSecureAccessCode.btnSendOTPCancel.onClick = scopeObj.navigateToLoginScreen.bind(scopeObj);
            this.getOTPTextFields().forEach(function(otpField) {
                otpField.onKeyUp = scopeObj.onKeyUpOtp.bind(this);
            })

        },

        /**
         * Returns the arary of text fields of enter OTP
         * @returns {array} - Array of text fields of OTP Screen
         */
        getOTPTextFields: function () {
            return [this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit1,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit2,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit3,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit4,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit5,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit6]
        },

        /**
         * Return OTP value from all text fields
         * @returns {otp} - OTP
         */
        getOTP: function() {
            var fields = this.getOTPTextFields();
            return fields.reduce(function(prevValue, currentValue, index) {
                return prevValue + fields[index].text[0];
            }, "");
        },
        /**
         * Resets OTP
         */
        resetOTP: function() {
            this.getOTPTextFields().forEach(function(otpField) {
                otpField.text = "";
            })
            FormControllerUtility.disableButton(this.view.EnterSecureAccessCode.btnSendOTPProceed);
        },
        /**
         * shows Enter OTP UI
         */
        showEnterOTPUI: function() {
            this.hideAll();
            this.view.flxEnterSecureAccessCode.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.EnterSecureAccessCode.flxEnterSecureAccessCodeHeading.text;
            this.AdjustScreen();
        },

        /**
         * Check Phone Number on send otp screen
         * @param {string} phoneNumber phone Number
         * @returns {void} - None
         */
        isPhoneNumberValid: function (phoneNumber) {
            if (phoneNumber === "" || phoneNumber === null || phoneNumber === undefined) {
                return false;
            }
            else {
                return /^\d{10}$/.test(phoneNumber)
            }
        },

        /**
         * used to check the phone number
         */
        checkPhoneNumber: function () {
            var enteredPhoneNumber = this.view.SendOTP.tbxOTP.text;
            if (this.isPhoneNumberValid(enteredPhoneNumber)) {
                FormControllerUtility.enableButton(this.view.SendOTP.btnSendOTPProceed);
            }
            else {
                FormControllerUtility.disableButton(this.view.SendOTP.btnSendOTPProceed);
            }
        },

        /**
         * shows requests OTP from back end
         */
        requestOTP: function () {
            this.loadNuoModule().presentationController.requestOTP();
        },

        /**
         * shows Enter OTP Screen
         * @param {boolean} isEnrolled isEnrolled
         */
        showPhoneAlreadyOptedScreen: function (isEnrolled) {
            var scopeObj = this;
            this.hideAll();
            this.view.flxCreateAccountUponAccountFound.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.CreateAccountUponAccountFound.flxCreateAccountHeading.text;
            this.AdjustScreen();
            this.view.CreateAccountUponAccountFound.btnCreateAccountEditPhoneNumber.onClick = function () {
                scopeObj.showNUOEnterPhoneScreenUI();
            };
            if (isEnrolled) {
                this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = this.navigateToLoginScreen.bind(this);
            }
            else {
                this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = this.showNUOLogin.bind(this);
            }
        },


        /**
         * shows NUO login
         */
        showNUOLogin: function () {
            var self = this;
            this.showNUOLoginUI();
            this.resetNUOLoginForm();
            this.view.EnterUsernamePassword.tbxNewUsername.onKeyUp = this.checkNUOLoginForm.bind(this);
            this.view.EnterUsernamePassword.tbxEnterpassword.onKeyUp = this.checkNUOLoginForm.bind(this);
            this.view.EnterUsernamePassword.btnOpenNewAccountCancel.onClick = this.navigateToLoginScreen.bind(this);
            this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
                self.loadNuoModule().presentationController.loginAndContinueApplication(self.getNUOLoginFormData());
            }
        },

        /**
         * used to get the NUO Login Form Data
         * @returns {object} NUO Form object
         */
        getNUOLoginFormData: function () {
            return {
                username: this.view.EnterUsernamePassword.tbxNewUsername.text.trim(),
                password: this.view.EnterUsernamePassword.tbxEnterpassword.text
            }
        },

        /**
         * used to reset the nuo login form
         */
        resetNUOLoginForm: function () {
            this.view.EnterUsernamePassword.tbxNewUsername.text = "";
            this.view.EnterUsernamePassword.tbxEnterpassword.text = "";
            this.view.EnterUsernamePassword.lblError.setVisibility(false);
        },

        /**
         * checks the NUO login form
         */
        checkNUOLoginForm: function () {
            var loginFormData = this.getNUOLoginFormData();
            if (loginFormData.username.length > 0 && loginFormData.password.length > 0) {
                FormControllerUtility.enableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            }
            else {
                FormControllerUtility.disableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            }
        },

        /**
         * used to show the NUO Login UI
         */
        showNUOLoginUI: function () {
            this.hideAll();
            this.view.flxEnterUsernamePassword.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.EnterUsernamePassword.flxOpenNewAccountHeading.text;
            this.AdjustScreen();
        },
        /**
         * shows Enter username/password Screen
         */
        showEnterUsernamePassword: function () {
            var scopeObj = this;
            this.view.EnterUsernamePassword.tbxNewUsername.text = "";
            this.view.EnterUsernamePassword.tbxEnterpassword.text = "";
            FormControllerUtility.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            this.showViews(["flxEnterUsernamePassword"]);
            this.view.EnterUsernamePassword.tbxNewUsername.onKeyUp = function () {
                if (scopeObj.view.EnterUsernamePassword.tbxNewUsername.text.length > 0 && scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text.length > 0) {
                    FormControllerUtility.enableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
                else {
                    FormControllerUtility.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
            }
            this.view.EnterUsernamePassword.tbxEnterpassword.onKeyUp = function () {
                if (scopeObj.view.EnterUsernamePassword.tbxNewUsername.text.length > 0 && scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text.length > 0) {
                    FormControllerUtility.enableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
                else {
                    FormControllerUtility.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
            }
            this.view.EnterUsernamePassword.btnOpenNewAccountCancel.onClick = function () {
                scopeObj.showViews(["flxCreateAccountUponAccountFound"]);
            }
            this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
                var username = scopeObj.view.EnterUsernamePassword.tbxNewUsername.text;
                var password = scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text;
                var usernamePasswordJSON = {
                    username: username,
                    password: password
                };
                self.loadNuoModule().presentationController.doLogin(usernamePasswordJSON);
            }
        },
        /**
   * Method to check whether the username is valid or not
   * @param {String} enteredUserName - username entered by the User
   * @returns {boolean} valid username
   */
        isUsernameValid: function (enteredUserName) {
            var userName = /^[a-zA-Z0-9.]+$/;
            if ((enteredUserName.match(userName)) && (enteredUserName.length > 7) && (enteredUserName.length < 25)) {
                if (this.checkDot(enteredUserName)) {
                    return true;
                }
            }
            return false;
        },
        /**
    * Method to Check Dots in Username Entered by the User
    * @param {String} enteredUserName - username entered by the User
    * @returns {boolean} checkDot status
    */
        checkDot: function (enteredUserName) {
            if (enteredUserName.charAt(0) === "." || enteredUserName.charAt(enteredUserName.length - 1) === ".") { return false };
            var index = enteredUserName.indexOf(".");
            for (var i = index + 1; i < enteredUserName.length - 1; i++) {
                if (enteredUserName[i] === ".") {
                    return false;
                }
            }
            return true;
        },
        /**
    * Method to Check Dots in Username Entered by the User
    * @param {String} enteredPassword - username entered by the User
    * @returns {boolean} - valid password
    */
        isPasswordValid: function (enteredPassword) {
            var password = /^(?=.*\d)(?=.*[a-zA-Z])(?=.+[\!\#\$\%\(\*\)\+\,\-\;\=\?\@\[\\\]\^\_\'\{\}]).{8,20}$/;
            if (enteredPassword.match(password) && !this.hasConsecutiveDigits(enteredPassword)) {
                return true;
            }
            return false;
        },
        /**
        * Method to Check whether the entered text has consecutive digits or not
        * @param {Int} input - field entered by the User
        * @returns {void} - None
        * @throws {void} -None
        */
        hasConsecutiveDigits: function (input) {
            var i;
            var count = 0;
            for (i = 0; i < input.length; i++) {
                // alert(abc[i]);
                if (input[i] >= 0 && input[i] <= 9) {
                    count++;
                }
                else { count = 0; }
                if (count === 9) { return true; }
            }
            return false;
        },
        /**
      * Method to validate email by the User
      * @param {String} email - email entered by the User
      * @returns {string} - valid email
      */
        isValidEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        /**
      * Method to validate fields entered by the User
      */
        validateEnrollFields: function () {
            function isUsernameAndEmailNotSame(enrollData) {
                return (enrollData.username.length > 0 && enrollData.email.length > 0 && (enrollData.username !== enrollData.email));
            }
            var scopeObj = this;
            var enrollData = this.getEnrollData();
            if (enrollData.email.length < 1 || enrollData.username.length < 1 || enrollData.password.length < 1 || scopeObj.view.CreateUsernamePassword.tbxReEnterPassowrd.text.length < 1) {
                FormControllerUtility.disableButton(scopeObj.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
            }
            else {
                FormControllerUtility.enableButton(scopeObj.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
            }
            if (this.isValidEmail(enrollData.email) && isUsernameAndEmailNotSame(enrollData)) {
                this.view.CreateUsernamePassword.imgValidEmail.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgValidEmail.setVisibility(false);
            }
            if (this.isUsernameValid(enrollData.username) && isUsernameAndEmailNotSame(enrollData)) {
                this.view.CreateUsernamePassword.imgUsername.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgUsername.setVisibility(false);
            }
            if (this.isPasswordValid(enrollData.password) && enrollData.username !== enrollData.password) {
                this.view.CreateUsernamePassword.imgPassword.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgPassword.setVisibility(false);
            }
            if (this.isPasswordValid(enrollData.password) && enrollData.password === this.view.CreateUsernamePassword.tbxReEnterPassowrd.text) {
                this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(false);
            }
            this.view.CreateUsernamePassword.forceLayout();

        },
        /**
         * Show First page of the User information Step[Step 3]
         */
        showUserInformationStep: function () {
            var self = this;
            self.hideAll();
            self.view.UploadDocPhoneOrComputer.flxColorLine.width = "28.57%";
            self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.UploadDocPhoneOrComputer.lblUserInformation.text;
            self.view.UploadDocPhoneOrComputer.flxProofs.setVisibility(false);
            self.view.UploadDocPhoneOrComputer.lblUserInformation.text = kony.i18n.getLocalizedString("i18n.NUO.UserInformation");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage1.text = kony.i18n.getLocalizedString("i18n.NUO.UserInformationMessage1");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage2.text = kony.i18n.getLocalizedString("i18n.NUO.UserInformationMessage2");
            self.view.UploadDocPhoneOrComputer.btnBack.onClick = function () {
                self.loadNuoModule().presentationController.navigateToProductSelection({ isRevisiting: true });
            };
            self.view.UploadDocPhoneOrComputer.btnUserInformationSaveClose.onClick = self.ShowSaveClosePopup.bind(self);
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
            self.view.UploadDocPhoneOrComputer.btnUserInformationProceed.onClick = function () {
                if (self.view.UploadDocPhoneOrComputer.lblRadioBtn2.text === "R") {
                    self.showLinkSentToPhoneForm();
                } else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUserInformationForm();
                }
            };
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.AdjustScreen();
        },
        /**
         * Get the Radio Button Widges array for Upload Doc From Computer or Phone flex
         * @returns {void} - None
         */
        getUploadDocRadioWidgetsArray: function () {
            return [this.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1, this.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2];
        },

        /**
         * Get the Radio Button Widges array for Gender in UserInformation flex
         * @returns {void} - None
         */
        getGenderRadioWidgetsArray: function () {
            return [this.view.UserInformationForm.flxRadioBtnMale, this.view.UserInformationForm.flxRadioBtnFemale];
        },

        /**
         * Show the form when user selects to upload data from mobile [Step 3]
         */
        showLinkSentToPhoneForm: function () {
            var self = this;
            this.hideAll();
            this.view.flxIdentityVerification.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.IdentityVerification.lblIdentityVerification.text;
            this.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
            this.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.UploadFromPhone");
            this.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.LinkSentToPhone");
            this.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.ClickTheLinkToContinue");
            this.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.MFA.RESEND");
            this.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.MFA.RESEND");
            this.view.IdentityVerification.btnAction.onClick = function () {
                self.showLoadingIndicator();
            };
            this.view.IdentityVerification.imgflxIdentityVerification.src = ViewConstants.IMAGES.NUO_SIGNATURE_MOBILE;
            this.view.IdentityVerification.btnDONE.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
        },
        /**
         * used to show the laoding indicator
         */
        showLoadingIndicator: function () {
            var self = this;
            FormControllerUtility.showProgressBar(self.view);
            function hideProgressBar() {
                FormControllerUtility.hideProgressBar(self.view);
            }
            kony.timer.schedule("timer5", hideProgressBar, 3, false);
        },
        /**
         * used to show the personal information UI
         * @returns {object} userInformation
         */
        getUserPersonalInformationFromUI: function () {
            var userInfo = {
                "userfirstname": this.view.UserInformationForm.tbxFirstName.text,
                "userlastname": this.view.UserInformationForm.tbxLastName.text,
                "dateOfBirth": CommonUtilities.sendDateToBackend(this.view.UserInformationForm.calDOB.date),
                "gender": this.view.UserInformationForm.lblRadioBtnMale.text === 'R' ? 'Male' : 'Female',
                "addressLine1": this.view.UserInformationForm.tbxAddressLine1.text,
                "addressLine2": this.view.UserInformationForm.tbxAddressLine2.text,
                "country": this.view.UserInformationForm.tbxCountry.text,
                "state": this.view.UserInformationForm.tbxState.text,
                "city": this.view.UserInformationForm.tbxCity.text,
                "zipcode": this.view.UserInformationForm.tbxZipcode.text,
                "maritalStatus": this.view.UserInformationForm.lbxMaritalStatus.selectedKeyValue[0],
                "noOfDependents": this.view.UserInformationForm.lbxDependents.selectedKeyValue[0],
                "ssn": this.view.UserInformationForm.tbxEnterSSN.text,
                "informationType": "PersonalInfo"
            };
            if (userInfo.maritalStatus === "Married") {
                userInfo.spouseFirstName = this.view.UserInformationForm.tbxSpouseName.text;
                userInfo.spouseLastName = this.view.UserInformationForm.tbxSpouseLastName.text;
            }
            return userInfo;
        },
        /**
         * used to show or hide the spouse details
         */
        showOrHideSpouseDetails: function () {
            if (this.view.UserInformationForm.lbxMaritalStatus.selectedKey === "Married") {
                this.view.UserInformationForm.flxUserInfoRow2.setVisibility(true);
            } else {
                this.view.UserInformationForm.flxUserInfoRow2.setVisibility(false);
            }
            this.AdjustScreen();
        },
        /**
         * Show Second page of the User information Step[Step 3 - Screen 2]
         * @param {object} viewModel it is an optional parameter, if provided it contains the data to be binded in the form
         */
        showUserInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            self.view.UserInformationForm.flxColorLine.width = "28.57%";
            self.view.flxUserInformationForm.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.UserInformationForm.lblUserInformationForm.text;
            self.view.UserInformationForm.btnBack.onClick = function () {
                self.showUserInformationStep();
            };
             self.view.UserInformationForm.calDOB.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            var todayDate = kony.os.date(applicationManager.getFormatUtilManager().getDateFormat());
            var endDate  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(todayDate, applicationManager.getFormatUtilManager().getDateFormat());
            self.view.UserInformationForm.calDOB.dateComponents = [endDate.getDate(), endDate.getMonth()+1, endDate.getFullYear()];
            self.view.UserInformationForm.tbxFirstName.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxLastName.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxAddressLine1.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxZipcode.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxCountry.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxState.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxCity.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxEnterSSN.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.calDOB.skin = ViewConstants.SKINS.SKNCALTRANSACTIONS;
            self.view.UserInformationForm.calDOB.onSelection = function () {
                self.view.UserInformationForm.calDOB.skin = ViewConstants.SKINS.SKNCALTRANSACTIONS;
                self.validateUserForm();
            };
            self.view.UserInformationForm.lbxMaritalStatus.onSelection = self.showOrHideSpouseDetails.bind(self);
            self.view.UserInformationForm.flxRadioBtnMale.onClick = self.radioBtnAction.bind(self, self.getGenderRadioWidgetsArray());
            self.view.UserInformationForm.flxRadioBtnFemale.onClick = self.radioBtnAction.bind(self, self.getGenderRadioWidgetsArray());
            self.view.UserInformationForm.tbxEnterSSN.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            self.view.UserInformationForm.tbxEnterSSN.onBeginEditing = function () {
                self.view.UserInformationForm.tbxEnterSSN.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            };
            //Terms and Conditions in user personal information and popup
            self.view.UserInformationForm.flxTCContentsCheckbox.onClick = function () {
                FormControllerUtility.toggleCheckbox(self.view.UserInformationForm.imgTCContentsCheckbox);
                FormControllerUtility.toggleCheckbox(self.view.imgTCContentsCheckbox);
                self.validateUserForm();
            };
            self.view.UserInformationForm.btnTermsAndConditions.onClick = function () {
                if (self.view.UserInformationForm.imgTCContentsCheckbox.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                    self.view.imgTCContentsCheckbox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
                } else {
                    self.view.imgTCContentsCheckbox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
                }
                self.view.flxTermsAndConditions.setVisibility(true);
                var height = self.SetPopUpHeight();
                self.view.flxTermsAndConditions.height = height + "dp";
                self.view.lblTermsAndConditions.setFocus(true);
            };
            self.view.flxTCContentsCheckbox.onClick = function () {
                FormControllerUtility.toggleCheckbox(self.view.imgTCContentsCheckbox);
            };
            self.view.btnSave.onClick = function () {
                if (self.view.imgTCContentsCheckbox.src === ViewConstants.IMAGES.UNCHECKED_IMAGE) {
                    self.view.UserInformationForm.imgTCContentsCheckbox.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
                } else {
                    self.view.UserInformationForm.imgTCContentsCheckbox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
                }
                self.validateUserForm();
                self.hideTermsAndConditionsFlex();
            };
            self.view.btnCancel.onClick = self.hideTermsAndConditionsFlex.bind(self);
            self.view.flxClose.onClick = self.hideTermsAndConditionsFlex.bind(self);
            self.view.UserInformationForm.btnUserInformationFormProceed.onClick = function () {
                self.validateDOBAndSSNAndProceed({ "showEmploymentInformationViewModel": "showEmploymentInformationViewModel" });
            };
            self.view.UserInformationForm.btnUserInformationFormSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.view.flxSaveClose.setVisibility(false);
                self.validateDOBAndSSNAndProceed({ "action": "saveAndClose" });
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                self.view.UserInformationForm.tbxFirstName.text = data.userfirstname;
                self.view.UserInformationForm.tbxLastName.text = data.userlastname;
                self.view.UserInformationForm.calDOB.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                self.view.UserInformationForm.calDOB.date = CommonUtilities.getFrontendDateString(data.dateOfBirth);
                self.view.UserInformationForm.calDOB.dateComponents = self.view.UserInformationForm.calDOB.dateComponents;
                self.view.UserInformationForm.lblRadioBtnMale.text = data.gender === 'Male' ? 'R' : "";
                self.view.UserInformationForm.lblFemale.text = data.gender === 'Female' ? 'R' : "";
                self.view.UserInformationForm.tbxAddressLine1.text = data.addressLine1;
                self.view.UserInformationForm.tbxAddressLine2.text = data.addressLine2;
                self.view.UserInformationForm.tbxCountry.text = data.country;
                self.view.UserInformationForm.tbxState.text = data.state;
                self.view.UserInformationForm.tbxCity.text = data.city;
                self.view.UserInformationForm.tbxZipcode.text = data.zipcode;
                self.view.UserInformationForm.lbxMaritalStatus.selectedKey = data.maritalStatus;
                if (data.maritalStatus === "Married") {
                    self.view.UserInformationForm.tbxSpouseName.text = data.spouseFirstName;
                    self.view.UserInformationForm.tbxSpouseLastName.text = data.spouseLastName;
                } else {
                    self.view.UserInformationForm.tbxSpouseName.text = "";
                    self.view.UserInformationForm.tbxSpouseLastName.text = "";
                }
                self.view.UserInformationForm.lbxDependents.selectedKey = data.noOfDependents;
                self.view.UserInformationForm.tbxEnterSSN.text = data.ssn;
                self.view.UserInformationForm.imgTCContentsCheckbox.src = ViewConstants.IMAGES.CHECKED_IMAGE;
            }
            self.validateUserForm();
            self.showOrHideSpouseDetails();
            self.AdjustScreen();
        },

        /**
         * Hide Terms and conditions Flex
         */
        hideTermsAndConditionsFlex: function () {
            this.view.flxTermsAndConditions.setVisibility(false);
        },

        /**
         * Show Employment Information Step[Step 4 -- Screen 1]
         * @param {object} viewModel it is an optional parameter, if provided it contains the data to be binded in the form
         */
        showEmploymentInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            this.view.EmploymentInformation.flxColorLine.width = "42.85%";
            this.view.flxEmploymentInformation.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.EmploymentInformation.lblEmploymentInfo.text;
            this.view.EmploymentInformation.lbxStatus.onSelection = function () {
                self.validateEmploymentForm();
                self.showOrHideCompanyNameAndJobTitle();
            };
            this.view.EmploymentInformation.tbxCompanyName.onKeyUp = self.validateEmploymentForm.bind(self);
            this.view.EmploymentInformation.tbxJobTitle.onKeyUp = self.validateEmploymentForm.bind(self);
            this.view.EmploymentInformation.btnBack.onClick = function () {
                self.loadNuoModule().presentationController.getUserInformation("showUserInformationViewModel");
            };
            this.view.EmploymentInformation.btnEmploymentInfoProceed.onClick = function () {
                var employementInfo = self.getEmploymentInformationfromUI();
                self.loadNuoModule().presentationController.createUserInformation(employementInfo);
            };
            this.view.EmploymentInformation.btnEmploymentInfoSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                var employementInfo = self.getEmploymentInformationfromUI();
                self.loadNuoModule().presentationController.createUserInformation(employementInfo, { "action": "saveAndClose" });
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                this.view.EmploymentInformation.lbxStatus.selectedKey = data.employmentInfo;
                this.view.EmploymentInformation.tbxCompanyName.text = data.company;
                this.view.EmploymentInformation.tbxJobTitle.text = data.jobProfile;
                this.view.EmploymentInformation.lbxYearsOfExperience.selectedKey = data.experience;
                this.showOrHideCompanyNameAndJobTitle();
            }
            self.validateEmploymentForm();
            self.AdjustScreen();
        },
        /**
         * used to show Or Hide CompanyNameAndJobTitle
         */
        showOrHideCompanyNameAndJobTitle: function () {
            if (this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" ||
                this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Retired") {
                this.view.EmploymentInformation.flxRow2.setVisibility(true);
            } else {
                this.view.EmploymentInformation.flxRow2.setVisibility(false);
            }
        },
        /**
         * used to get the EmploymentInformationfrom UI
         * @returns {object} employmentInfo employmentInformation
         */
        getEmploymentInformationfromUI: function () {
            var employmentInfo = {
                "employmentInfo": this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0],
                "experience": this.view.EmploymentInformation.lbxYearsOfExperience.selectedKeyValue[0],
                "informationType": "EmploymentInfo"
            };
            if (this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" ||
                this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Retired") {
                employmentInfo.company = this.view.EmploymentInformation.tbxCompanyName.text;
                employmentInfo.jobProfile = this.view.EmploymentInformation.tbxJobTitle.text;
            } else {
                employmentInfo.company = "";
                employmentInfo.jobProfile = "";
            }
            return employmentInfo;
        },

        /**
         * used to get the Financial Information FromUI
         * @returns {object} financialInfo
         */
        getFinancialInformationFromUI: function () {
            var financialInfo = {
                "annualIncome": this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKeyValue[0],
                "assets": this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKeyValue[0],
                "montlyExpenditure": this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKeyValue[0],
                "informationType": "FinancialInfo"
            };
            return financialInfo;
        },

        /**
         * Show Financial Information Step[Step 5 -- Screen 1]
         * @member frmNewUserOnBoardingController
         * @param {object} viewModel it is an optional parameter, if provided it contains the data to be binded in the form
         */
        showFinancialInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            this.view.FinancialInformation.flxColorLine.width = "57.14%";
            this.view.flxFinancialInformation.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.FinancialInformation.lblFinancialInfo.text;
            this.view.FinancialInformation.btnBack.onClick = function () {
                self.loadNuoModule().presentationController.getUserInformation("showEmploymentInformationViewModel");
            };
            this.view.FinancialInformation.btnFinancialInfoSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                var financialInfo = self.getFinancialInformationFromUI();
                self.loadNuoModule().presentationController.createUserInformation(financialInfo, { "action": "saveAndClose" });
            };
            this.view.FinancialInformation.btnFinancialInfoProceed.onClick = function () {
                var financialInfo = self.getFinancialInformationFromUI();
                self.loadNuoModule().presentationController.createUserInformation(financialInfo);
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKey = data.annualIncome;
                this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKey = data.assets;
                this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKey = data.montlyExpenditure;
            }
            self.AdjustScreen();
        },
        /**
         *used to validate the user form 
         */
        validateUserForm: function () {
            var self = this;
            var isValid = true;
            if (!self.view.UserInformationForm.tbxFirstName.text ||
                !self.view.UserInformationForm.tbxLastName.text ||
                !self.view.UserInformationForm.tbxAddressLine1.text ||
                !self.view.UserInformationForm.tbxCountry.text ||
                !self.view.UserInformationForm.tbxState.text ||
                !self.view.UserInformationForm.tbxCity.text ||
                !self.view.UserInformationForm.tbxZipcode.text ||
                !self.view.UserInformationForm.tbxEnterSSN.text ||
                self.view.UserInformationForm.imgTCContentsCheckbox.src !== ViewConstants.IMAGES.CHECKED_IMAGE) {
                isValid = false;
            }
            if (!isValid) {
                FormControllerUtility.disableButton(self.view.UserInformationForm.btnUserInformationFormProceed);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.setEnabled(false);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.skin = ViewConstants.SKINS.SKNBTNFFFFFFBORDER3343A81PXRADIUS2PXDISABLED;
            } else {
                FormControllerUtility.enableButton(self.view.UserInformationForm.btnUserInformationFormProceed);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.setEnabled(true);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.skin = ViewConstants.SKINS.LOCATE_BTNSHARECANCEL;
            }
            //self.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
            //self.AdjustScreen();
        },

        /**
         * validates the DOB and SSN
         * @param {string} context context
         */
        validateDOBAndSSNAndProceed: function (context) {
            var isValid = true;
            var errMsg = "";
            this.view.UserInformationForm.calDOB.skin = ViewConstants.SKINS.SKNCALTRANSACTIONS;
            if (!this.isValidSSN(this.view.UserInformationForm.tbxEnterSSN.text)) {
                isValid = false;
                errMsg = kony.i18n.getLocalizedString("i18n.NUO.invalidSSN");
            }
            if (!this.isValidDateOfBirth(this.view.UserInformationForm.calDOB.date)) {
                isValid = false;
                errMsg = kony.i18n.getLocalizedString("i18n.NUO.invalidDOB");
            }
            if (isValid) {
                this.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
                var userInfo = this.getUserPersonalInformationFromUI();
                this.loadNuoModule().presentationController.createUserInformation(userInfo, context);
            } else {
                if (errMsg === kony.i18n.getLocalizedString("i18n.NUO.invalidSSN")) {
                    this.view.UserInformationForm.tbxEnterSSN.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
                } else {
                    this.view.UserInformationForm.calDOB.skin = ViewConstants.SKINS.SKNFF0000CAL;
                }
                this.view.UserInformationForm.lblWarningUserInfo.text = errMsg;
                this.view.UserInformationForm.lblWarningUserInfo.setVisibility(true);
                this.view.UserInformationForm.lblWarningUserInfo.setFocus(true);
                this.AdjustScreen();
            }
        },

        /**
         * used to validate the SSN 
         * @param {number} ssn ssn number 
         * @returns {boolean} valid SSN
         */
        isValidSSN: function (ssn) {
            var isValid = false;
            var ssnRE = /^\d{9}$/;
            if (ssn && ssnRE.test(ssn)) {
                isValid = true;
            }
            return isValid;
        },
        /**
         * used to check valid date of birth 
         * @param {date} dateOfBirth dateOfBirth
         * @returns {boolean} valid or not
         */
        isValidDateOfBirth: function (dateOfBirth) {
            if(dateOfBirth){
                var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateOfBirth,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
                var today = new Date();
                var timeDiff = Math.abs(today.getTime() - dateObj.getTime());
                var diffYears = Math.ceil(timeDiff / (1000 * 3600 * 24 * 365)); 
                if(diffYears >= 18){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        },
        /**
         * used to validate the employee form
         */
        validateEmploymentForm: function () {
            if (this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" &&
                (!this.view.EmploymentInformation.tbxCompanyName.text ||
                    !this.view.EmploymentInformation.tbxJobTitle.text)) {
                FormControllerUtility.disableButton(this.view.EmploymentInformation.btnEmploymentInfoProceed);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.setEnabled(false);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.skin = ViewConstants.SKINS.SKNBTNFFFFFFBORDER3343A81PXRADIUS2PXDISABLED;
            } else {
                FormControllerUtility.enableButton(this.view.EmploymentInformation.btnEmploymentInfoProceed);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.setEnabled(true);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.skin = ViewConstants.SKINS.LOCATE_BTNSHARECANCEL;
            }
        },
        /**
         * used to initlize the dates and view
         */
        initialize: function () {
            this.view.UserInformationForm.calDOB.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            var mydate = new Date();
            var currDate = [mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear()];
            this.view.UserInformationForm.calDOB.validEndDate = currDate;
            this.view.UserInformationForm.calDOB.date = CommonUtilities.getFrontendDateString(kony.os.date(applicationManager.getFormatUtilManager().getDateFormat()));
            this.view.UserInformationForm.calDOB.dateComponents = this.view.UserInformationForm.calDOB.dateComponents;
        },

        /**
         * populate listboxes
         */
        resetPersonalInfoData: function () {
            this.view.UserInformationForm.lbxMaritalStatus.masterData = [['Single', 'Single'], ['Married', 'Married'], ['Widowed', 'Widowed'], ['Divorced', 'Divorced']];
            this.view.UserInformationForm.lbxDependents.masterData = [['0', '00'], ['1', '01'], ['2', '02'], ['3', '03'], ['4', '04'], ['5', '05'], ['6', '06'], ['7', '07'], ['8', '08']];
            this.view.EmploymentInformation.lbxStatus.masterData = [['Employed', 'Employed'], ['Unemployed', 'Unemployed'], ['Retired', 'Retired'], ['Student', 'Student']];
            this.view.EmploymentInformation.lbxYearsOfExperience.masterData = [[0, '0 year to 1 year'], [1, '1 year to 2 years'], [2, '2 years to 3 years'], [3, '3 years to 4 years'], [4, '4 years to 5 years'], [5, '5 years to 6 years'], [6, '6 years to 7 years'], [7, '7 years to 8 years'], [8, '8 years to 9 years'], [9, '9 years to 10 years'], [10, '10+ years']];
            this.view.FinancialInformation.lbxAnnualGrossIncome.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.FinancialInformation.lbxValuOfAssetsYouOwn.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.FinancialInformation.lbxMonthlyExpenditure.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
            this.view.UserInformationForm.tbxFirstName.text = "";
            this.view.UserInformationForm.tbxLastName.text = "";
            this.view.UserInformationForm.tbxAddressLine1.text = "";
            this.view.UserInformationForm.tbxAddressLine2.text = "";
            this.view.UserInformationForm.tbxZipcode.text = "";
            this.view.UserInformationForm.tbxSpouseName.text = "";
            this.view.UserInformationForm.tbxSpouseLastName.text = "";
            this.view.UserInformationForm.tbxEnterSSN.text = "";
            this.view.UserInformationForm.lblRadioBtnMale.text = 'R';
            this.view.UserInformationForm.lblRadioBtnMale.skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
            this.view.UserInformationForm.lblFemale.text = "";
            this.view.UserInformationForm.lblFemale.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
            this.view.UserInformationForm.tbxCountry.text = "";
            this.view.UserInformationForm.tbxState.text = "";
            this.view.UserInformationForm.tbxCity.text = "";
            //this.view.UserInformationForm.lbxMaritalStatus.selectedKey = "Single";
            //this.view.UserInformationForm.lbxDependents.selectedKey = data.noOfDependents;
            //this.view.EmploymentInformation.lbxStatus.selectedKey = "Employed";
            //this.view.EmploymentInformation.lbxYearsOfExperience.selectedKey = 0;
            this.view.EmploymentInformation.tbxCompanyName.text = "";
            this.view.EmploymentInformation.tbxJobTitle.text = "";
            //this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKey = 0;
            //this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKey = 0;
            //this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKey = 0;
        },

        /**
         * Change focus of OTP Field to next/previous OTP Field
         * @param {obejct} radioWidgetFlexArray Radio Widget FLex Array 
         * @param {obejct}  toBeSelected User Clicks on to be selected
         */
        radioBtnAction: function (radioWidgetFlexArray, toBeSelected) {
            radioWidgetFlexArray.forEach(function (flex) {
                flex.widgets()[0].text = " ";
                flex.widgets()[0].skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
            })
            toBeSelected.widgets()[0].text = "R";
            toBeSelected.widgets()[0].skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
        },

        /**
         * used to select the radio button
         * @param {object} RadioBtn1 radio button
         * @param {object} RadioBtn2 radio button
         * @param {object} RadioBtn3 radio button
          * @param {object} RadioBtn4 radio button
         */
        RadioBtnAction: function (RadioBtn1, RadioBtn2, RadioBtn3, RadioBtn4) {
            if (RadioBtn1.text === "R") {
                RadioBtn1.text = " ";
                RadioBtn1.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
                if (RadioBtn2 != " " || RadioBtn2 !== null) {
                    RadioBtn2.text = "R";
                    RadioBtn2.skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
                }
                if (RadioBtn3 != " " || RadioBtn3 !== null) {
                    RadioBtn3.text = "R";
                    RadioBtn3.skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
                }
                if (RadioBtn4 != " " || RadioBtn2 !== null) {
                    RadioBtn4.text = "R";
                    RadioBtn4.skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
                }
            } else {
                if (RadioBtn2 != " " || RadioBtn2 !== null) {
                    RadioBtn2.text = " ";
                    RadioBtn2.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
                }
                if (RadioBtn3 != " " || RadioBtn3 !== null) {
                    RadioBtn3.text = " ";
                    RadioBtn3.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
                }
                if (RadioBtn4 != " " || RadioBtn4 !== null) {
                    RadioBtn4.text = " ";
                    RadioBtn4.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
                }
                RadioBtn1.text = "R";
                RadioBtn1.skin = ViewConstants.SKINS.RADIO_BTN_SELECTED;
            }
        },

        /**
         * shows the open new account form
         * @param {object} continueOrResetApplicationViewModel continueOrResetApplicationViewModel
         */
        showOpenNewAccount: function (continueOrResetApplicationViewModel) {
            var scopeObj = this;
            this.showOpenNewAccountUI();
            this.showTopMenu();
            this.view.OpenNewAccount.lblUploadSignatureError.text = kony.i18n.getLocalizedString("i18n.NUO.youhavealreadystartedwarning");
            this.view.OpenNewAccount.lblOpenNewAccountMessageBox2.text = kony.i18n.getLocalizedString("i18n.NUO.youwillstartapplication");
            var radioFlexes = [this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1, this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2];
            this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1.onClick = function () {
                scopeObj.validateOpenNewAccount();
                scopeObj.radioBtnAction(radioFlexes, scopeObj.view.OpenNewAccount.flxOpenNewAccountRadiobtn1);
            }
            this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2.onClick = function () {
                scopeObj.validateOpenNewAccount();
                scopeObj.radioBtnAction(radioFlexes, scopeObj.view.OpenNewAccount.flxOpenNewAccountRadioBtn2);
            };
            this.view.OpenNewAccount.lblUploadSignatureColorSeperator.width = continueOrResetApplicationViewModel.progress + "%";
            this.view.OpenNewAccount.btnOpenNewAccountCancel.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
            this.view.OpenNewAccount.btnOpenNewAccountProceed.onClick = this.openNewAccountProceed.bind(this, continueOrResetApplicationViewModel);
        },

        /**
         * new account proceed handler
         * @param {obejct} continueOrResetApplicationViewModel continueOrResetApplicationViewModel
         */
        openNewAccountProceed: function (continueOrResetApplicationViewModel) {
            if (this.view.OpenNewAccount.lblRadioBtn1.text === 'R') {
                this.loadNuoModule().presentationController.findStageAndContinueApplication(continueOrResetApplicationViewModel.userDetails, continueOrResetApplicationViewModel.userState);
            }
            else {
                this.loadNuoModule().presentationController.resetApplication(continueOrResetApplicationViewModel.userDetails);
            }
        },

        /**
         * validates the open new account form
         */
        validateOpenNewAccount: function () {
            var valid = false;
            var radioFlexes = [this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1, this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2];
            radioFlexes.forEach(function (radioFlex) {
                if (radioFlex.widgets()[0].text === "R") {
                    valid = true;
                }
            })
            if (valid) {
                FormControllerUtility.enableButton(this.view.OpenNewAccount.btnOpenNewAccountProceed);
            }
            else {
                FormControllerUtility.disableButton(this.view.OpenNewAccount.btnOpenNewAccountProceed);
            }
        },
        /**
         * Method to show upload document screen with channels option
         * @param {boolean} employmentStatus  employment Status
         */
        showUploadDocumentsScreen1: function (employmentStatus) {
            var self = this;
            self.hideAll();
            self.view.UploadDocPhoneOrComputer.flxColorLine.width = "71.42%";
            self.view.UploadDocPhoneOrComputer.flxProofs.setVisibility(true);
            self.view.UploadDocPhoneOrComputer.flxEmployementProof.isVisible = (employmentStatus === "Employed") ? true : false;
            self.view.UploadDocPhoneOrComputer.flxIncomeProof.isVisible = (employmentStatus === "Employed") ? true : false;
            self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
            self.view.UploadDocPhoneOrComputer.lblUserInformation.text = kony.i18n.getLocalizedString("i18n.NUO.UploadDocuments");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage1.text = (employmentStatus === "Employed") ? kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage1") : kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage1ForAddress");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage2.text = kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage2");
            self.view.UploadDocPhoneOrComputer.btnBack.onClick = function () {
                self.loadNuoModule().presentationController.getUserInformation("showFinancialInformationViewModel");
            };
            self.view.UploadDocPhoneOrComputer.btnUserInformationProceed.onClick = function () {
                if (self.view.UploadDocPhoneOrComputer.lblRadioBtn2.text === "R") {
                    self.showLinkSentToPhoneForm();
                } else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUploadDocumentsScreen2(employmentStatus);
                }
            };
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());

            self.view.UploadDocPhoneOrComputer.btnUserInformationSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
            self.AdjustScreen();
        },

        /**
         * Method to show upload document screen 2
         * @param  {boolean} employmentStatus employmentStatus
         */
        showUploadDocumentsScreen2: function (employmentStatus) {
            var self = this;
            self.hideAll();
            self.view.UploadDocuments.flxHeaderUploadDocuments.setFocus(true);
            self.view.flxUploadDocuments.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.UploadDocuments.lblUploadDocuments.text;
            self.view.UploadDocuments.flxColorLine.width = "71.42%";
            self.view.UploadDocuments.flxUploadDocumentsEmploymentProof.isVisible = (employmentStatus === "Employed") ? true : false;
            self.view.UploadDocuments.lblUploadDocumentsSeperator.isVisible = (employmentStatus === "Employed") ? true : false;
            self.view.UploadDocuments.flxUploadDocumentsIncomeProof.isVisible = (employmentStatus === "Employed") ? true : false;
            if (employmentStatus !== "Employed") {
                self.resetUIAfterRemovingAttach("employmentProof");
                self.resetUIAfterRemovingAttach("incomeProof");
            }

            var btnVisibilityEmployed = (self.view.UploadDocuments.flxDocAttachmentIP.isVisible === true && self.view.UploadDocuments.flxDocAttachmentEP.isVisible === true && self.view.UploadDocuments.flxDocAttachmentAP.isVisible === true)
            var btnVisibilityUnemployed = (employmentStatus !== "Employed" && self.view.UploadDocuments.flxDocAttachmentAP.isVisible === true);
            //To enable Proceed button based on some condition - (to solve a issue if user is traversing back and proceed flows)
            if (btnVisibilityEmployed || btnVisibilityUnemployed) {
                self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(true);
                self.view.UploadDocuments.btnUploadDocumentsProceed.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PX;
            }
            if (employmentStatus !== "Employed" && self.filesToBeUploaded) {
                //Clearing the "extra" earlier selected files if any - (to solve a issue if user is traversing back and proceed flows)
                if (self.filesToBeUploaded.employmentProof) {
                    self.filesToBeUploaded.employmentProof = null;
                }
                if (self.filesToBeUploaded.incomeProof === null) {
                    self.filesToBeUploaded.incomeProof = null;
                }
            }
            self.view.UploadDocuments.btnAttachAP.onClick = function () {
                self.browseFiles("addressProof");
            };
            self.view.UploadDocuments.btnAttachEP.onClick = function () {
                self.browseFiles("employmentProof");
            };
            self.view.UploadDocuments.btnAttachIP.onClick = function () {
                self.browseFiles("incomeProof");
            };
            self.view.UploadDocuments.flxRemoveAttachmentAP.onClick = function () {
                self.actionForRemoveAttachment("addressProof");
            };
            self.view.UploadDocuments.flxRemoveAttachmentEP.onClick = function () {
                self.actionForRemoveAttachment("employmentProof");
            };
            self.view.UploadDocuments.flxRemoveAttachmentIP.onClick = function () {
                self.actionForRemoveAttachment("incomeProof");
            };
            self.view.UploadDocuments.btnBack.onClick = function () {
                self.hideAll();
                self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
                self.AdjustScreen();
            };
            self.view.UploadDocuments.btnUploadDocumentsProceed.onClick = function () {
                self.saveUploadedDocuments();
            };
            self.view.UploadDocuments.btnUploadDocumentsSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.AdjustScreen();
        },
        /**
         * used to reset the UI after reseting the UI
         * @param {string} context context
         */
        resetUIAfterRemovingAttach: function (context) {
            var self = this;
            self.getConfigForUploadUI(context).btnAttach.skin = ViewConstants.SKINS.BREADCRUM;
            self.getConfigForUploadUI(context).btnAttach.setEnabled(true);
            self.getConfigForUploadUI(context).errorMsg.setVisibility(false);
            self.getConfigForUploadUI(context).attachmentFlex.setVisibility(false);
        },
        /**
         * used to remove the attachment
         * @param {string} context context
         */
        actionForRemoveAttachment: function (context) {
            var self = this;
            self.resetUIAfterRemovingAttach(context);
            //disable Proceed & saveNclose button
            self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(false);
            self.view.UploadDocuments.btnUploadDocumentsProceed.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PXDISABLED;
            self.AdjustScreen();
        },
        /**
         * Opens the browser for uploading the files on click of the Attachment icon
         * @param {object} context defines attachment type  
         */
        browseFiles: function (context) {
            var self = this;
            var config = {
                selectMultipleFiles: false,
                filter: ["application/pdf"]
            };
            self.getConfigForUploadUI(context).errorMsg.setVisibility(false);
            kony.io.FileSystem.browse(config, this.browseFilesCallback.bind(self, context));
        },
        /**
         * Returns UI - widget paths based on context
         * @param {object} context defines attachment type  
         * @return {object} object
         */
        getConfigForUploadUI: function (context) {
            var self = this;
            if (context === "addressProof") {
                return {
                    errorMsg: self.view.UploadDocuments.lblWarningAddressProof,
                    btnAttach: self.view.UploadDocuments.btnAttachAP,
                    fileName: self.view.UploadDocuments.lblDocNameAP,
                    attachmentFlex: self.view.UploadDocuments.flxDocAttachmentAP
                };
            }
            else if (context === "employmentProof") {
                return {
                    errorMsg: self.view.UploadDocuments.lblWarningEmployementProof,
                    btnAttach: self.view.UploadDocuments.btnAttachEP,
                    fileName: self.view.UploadDocuments.lblDocNameEP,
                    attachmentFlex: self.view.UploadDocuments.flxDocAttachmentEP
                };
            }
            else if (context === "incomeProof") {
                return {
                    errorMsg: self.view.UploadDocuments.lblWarningIncomeProof,
                    btnAttach: self.view.UploadDocuments.btnAttachIP,
                    fileName: self.view.UploadDocuments.lblDocNameIP,
                    attachmentFlex: self.view.UploadDocuments.flxDocAttachmentIP
                };
            }
        },
        /**
         * Callback for browse files
         * @param {object} context defines attachment type
        * @param {object} event event
         * @param {object}file file
         */
        browseFilesCallback: function (context, event, file) {
            var self = this;
            self.getBase64(file[0].file, context);
            self.bindFileToUI(file[0], context);
        },
        /**
        * Binds the attachment data to the segment 
        * @param {object} file is a  JSON which consists of the data of the files attached ,
        * @param {object} context defines attachment type
        */
        bindFileToUI: function (file, context) {
            var self = this;
            if (self.isFileTypeNotSupported(file.file)) {
                self.getConfigForUploadUI(context).errorMsg.text = kony.i18n.getLocalizedString("i18n.NUO.FileTypeNotSupported");
                self.getConfigForUploadUI(context).errorMsg.setVisibility(true);
            } else if (self.isFileSizeExceeds(file)) {
                self.getConfigForUploadUI(context).errorMsg.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                self.getConfigForUploadUI(context).errorMsg.setVisibility(true);
            } else {
                if (file) {
                    self.getConfigForUploadUI(context).btnAttach.skin = ViewConstants.SKINS.BREADCRUMCSR;
                    self.getConfigForUploadUI(context).btnAttach.setEnabled(false);
                    self.getConfigForUploadUI(context).fileName.text = file.name;
                    self.getConfigForUploadUI(context).attachmentFlex.setVisibility(true);
                    if (self.view.UploadDocuments.flxUploadDocumentsIncomeProof.isVisible === false || (self.view.UploadDocuments.flxDocAttachmentIP.isVisible === true && self.view.UploadDocuments.flxDocAttachmentEP.isVisible === true && self.view.UploadDocuments.flxDocAttachmentAP.isVisible === true)) {
                        //enable Proceed & saveNclose button
                        self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(true);
                        self.view.UploadDocuments.btnUploadDocumentsProceed.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PX;
                    }
                }
            }
            self.AdjustScreen();
        },
        /**
         * validates the file type
         * @param {object} file file
         * @returns {boolean} status
         */
        isFileTypeNotSupported: function (file) {
            var type = file.type;
            if (type !== "application/pdf") {
                return true;
            }
            else {
                return false;
            }
        },
        /**
         * Checks if the size of the file exceeds more than 1MB
         * @param {JSON} file JSON which consists of the data of the files
         * @return {boolean}  boolean true if the size of the file exceeds false if the file size is less than 1MB
         */
        isFileSizeExceeds: function (file) {
            return file.size > 1048576;
        },

        /**
         * getBase64
         * @param {object} file file
         * @param {object} context context
         */
        getBase64: function (file, context) {
            var self = this;
            var reader = new FileReader();
            self.filesToBeUploaded = this.filesToBeUploaded || {};
            reader.onloadend = function () {
                self.filesToBeUploaded[context] = reader.result;
            }
            reader.readAsDataURL(file);
        },
        /**
         * Method to save uploaded documents
         */
        saveUploadedDocuments: function () {
            var self = this;
            self.loadNuoModule().presentationController.uploadDocuments(self.filesToBeUploaded);
        },
        /**
         * Method to show next flow after saving documents
         */
        performIdentityVerification: function () {
            var self = this;
            var response = [];
            self.loadNuoModule().presentationController.doIdentityVerification(response);
        },
        /**
         * Get identity verification questions
         */
        getIdentityVerification: function () {
            var self = this;
            self.loadNuoModule().presentationController.getIdentityVerificationQuestion();
        },
        /**
         * Show identity verification flow
         * @member frmNewUserOnboardingController
         * @param {object} questions questions 
         */
        showIdentityVerification: function (questions) {
            var self = this;
            self.hideAll();
            self.view.flxIdentityVerificationQuestions.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.IdentityVerificationQuestions.lblIdentityVerQuestions.text;
            self.view.IdentityVerificationQuestions.setFocus(true);
            self.view.IdentityVerificationQuestions.flxColorLine.width = "71.42%";
            self.bindIdentityQuestionSegment(questions);
            self.view.IdentityVerificationQuestions.btnBack.onClick = function () {
                self.hideAll();
                self.view.flxUploadDocuments.setVisibility(true);
                self.view.customheader.lblHeaderMobile.text = self.view.UploadDocuments.lblUploadDocuments.text;
                self.AdjustScreen();
            };
            self.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.IdentityVerificationQuestions.btndentityVerificationProceed.onClick = function () {
                self.showSubmitApplicationPopup();
            };
            self.validateIdentityQuestions();
            self.AdjustScreen();
        },
        /**
         * used to validate the questions
         */
        validateIdentityQuestions: function () {
            function isRowAnwsered(rowData) {
                var isSelected = false;
                [1, 2, 3, 4].forEach(function (index) {
                    var isRadioBtnSelected = rowData["lblRadioBtn" + index].text === "M";
                    if (isRadioBtnSelected) {
                        isSelected = true;
                    }
                })
                return isSelected;
            }

            var answeredRows = 0;
            var questionsData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.data;
            questionsData.forEach(function (questionRow) {
                if (isRowAnwsered(questionRow)) {
                    answeredRows++;
                }
            })

            if (answeredRows === questionsData.length) {
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.skin = ViewConstants.SKINS.LOCATE_BTNSHARECANCEL;
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.setEnabled(true);
                FormControllerUtility.enableButton(this.view.IdentityVerificationQuestions.btndentityVerificationProceed)

            }
            else {
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.skin = ViewConstants.SKINS.SKNBTNFFFFFFBORDER3343A81PXRADIUS2PXDISABLED;
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.setEnabled(false);
                FormControllerUtility.disableButton(this.view.IdentityVerificationQuestions.btndentityVerificationProceed)
            }
        },
        /**
         * used to bind the identity questions
         * @param {object} data questions
         */
        bindIdentityQuestionSegment: function (data) {
            var widgetDataMap = {
                "lblQuestion": "lblQuestion",
                "lblRadioBtn1": "lblRadioBtn1",
                "lblRadioBtn2": "lblRadioBtn2",
                "lblRadioBtn3": "lblRadioBtn3",
                "lblRadioBtn4": "lblRadioBtn4",
                "lblOption1": "lblOption1",
                "lblOption2": "lblOption2",
                "lblOption3": "lblOption3",
                "lblOption4": "lblOption4",
                "flxNUORadioBtn1": "flxNUORadioBtn1",
                "flxNUORadioBtn2": "flxNUORadioBtn2",
                "flxNUORadioBtn3": "flxNUORadioBtn3",
                "flxNUORadioBtn4": "flxNUORadioBtn4",
                "lblQuestionSeperator": "lblQuestionSeperator"
            };

            var segData = [
                {
                    "lblQuestion": kony.i18n.getLocalizedString("i18n.NUO.Question1"),
                    "lblRadioBtn1": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn2": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn3": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn4": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "flxNUORadioBtn1": {
                        onClick: this.toggleRadioBtn.bind(this, 1)
                    },
                    "flxNUORadioBtn2": {
                        onClick: this.toggleRadioBtn.bind(this, 2)
                    },
                    "flxNUORadioBtn3": {
                        onClick: this.toggleRadioBtn.bind(this, 3)
                    },
                    "flxNUORadioBtn4": {
                        onClick: this.toggleRadioBtn.bind(this, 4)
                    },
                    "lblOption1": "January 2016",
                    "lblOption2": "March 2013",
                    "lblOption3": "December 2015",
                    "lblOption4": "None of the Above",
                    "lblQuestionSeperator": "lblQuestionSeperator"
                },
                {
                    "lblQuestion": kony.i18n.getLocalizedString("i18n.NUO.Question2"),
                    "lblRadioBtn1": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn2": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn3": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "lblRadioBtn4": {
                        text: "L",
                        skin: ViewConstants.SKINS.RADIOBTN_UNSELECTED
                    },
                    "flxNUORadioBtn1": {
                        onClick: this.toggleRadioBtn.bind(this, 1)
                    },
                    "flxNUORadioBtn2": {
                        onClick: this.toggleRadioBtn.bind(this, 2)
                    },
                    "flxNUORadioBtn3": {
                        onClick: this.toggleRadioBtn.bind(this, 3)
                    },
                    "flxNUORadioBtn4": {
                        onClick: this.toggleRadioBtn.bind(this, 4)
                    },
                    "lblOption1": "Bank Of America",
                    "lblOption2": "Wells Fargo",
                    "lblOption3": "Chase",
                    "lblOption4": "None of the Above",
                    "lblQuestionSeperator": "lblQuestionSeperator"
                }
            ];
            this.view.IdentityVerificationQuestions.segIdentityVerQuestions.widgetDataMap = widgetDataMap;
            this.view.IdentityVerificationQuestions.segIdentityVerQuestions.setData(segData);
        },
        /**
         * used to toggles the radio button
         * @param {string} index index
         */
        toggleRadioBtn: function (index) {
            var labelWidgetIndex = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.selectedIndex[1];
            var segData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.data;
            var rowData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.selectedItems[0];
            rowData.lblRadioBtn1 = {
                "text": "L",
                "skin": ViewConstants.SKINS.RADIOBTN_UNSELECTED
            };
            rowData.lblRadioBtn2 = {
                "text": "L",
                "skin": ViewConstants.SKINS.RADIOBTN_UNSELECTED
            };
            rowData.lblRadioBtn3 = {
                "text": "L",
                "skin": ViewConstants.SKINS.RADIOBTN_UNSELECTED
            };
            rowData.lblRadioBtn4 = {
                "text": "L",
                "skin": ViewConstants.SKINS.RADIOBTN_UNSELECTED
            };
            rowData['lblRadioBtn' + index] = {
                "text": "M",
                "skin": ViewConstants.SKINS.RADIOBTN_SELECTED
            };
            this.view.IdentityVerificationQuestions.segIdentityVerQuestions.setDataAt(rowData, labelWidgetIndex);
            this.validateIdentityQuestions();
        },
        /**
         * used to show the identity verification error
         */
        showIdentityVerificationError: function () {
            var self = this;
            self.hideAll();
            self.view.flxIdentityVerification.setVisibility(true);
            self.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(false);
            self.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.IdentityVerification");
            self.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.IdentityVerificationError");
            self.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.TheIdentityVerificationhasFailed");
            self.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.NUO.retry");
            self.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.NUO.retry");
            self.view.IdentityVerification.btnAction.onClick = function () {
                self.showIdentityVerification();
            }
            self.view.IdentityVerification.imgflxIdentityVerification.src = ViewConstants.IMAGES.NUO_CREDIT_CHECK_FAILURE;
            self.AdjustScreen();
        },
        /**
         * used to show the application popup
         */
        showSubmitApplicationPopup: function () {
            var self = this;
            self.view.flxSubmitApplication.setVisibility(true);
            self.view.SubmitApplication.btnYes.onClick = function () {
                self.performIdentityVerification();
            };
            var height = self.SetPopUpHeight();
            self.view.flxSubmitApplication.height = height + "dp";
            self.view.SubmitApplication.lblPopupMessage.setFocus(true);
            self.view.forceLayout();
        },
        /**
         * performCreditCheck
         */
        performCreditCheck: function () {
            var self = this;
            self.loadNuoModule().presentationController.userCreditCheck();
        },
        /**
         * used to show the open new account screen
         */
        showOpenNewAccountUI: function () {
            this.hideAll();
            this.view.flxOpenNewAccount.setVisibility(true);
            this.view.customheader.lblHeaderMobile.text = this.view.OpenNewAccount.lblOpenNewAccountHeading.text;
            this.AdjustScreen();
        },
        /**
         * used to show the digital signature flow
         */
        showDigitalSignatureFlow: function () {
            var self = this;
            self.hideAll();
            self.loadNuoModule().presentationController.getUserInformation("sendMetrics");
            self.view.flxDigitalSignatureSelectionMode.setVisibility(true);
            
            self.view.DigitalSignatureSelectionMode.flxColorLine.width = "85.71%";
            self.view.DigitalSignatureSelectionMode.btnDigitalSignatureProceed.onClick = function () {
                if (self.view.DigitalSignatureSelectionMode.lblRadioBtn2.text === "R") {
                    self.showUploadSignatureFromPhone();
                }
                else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUploadSignatureFromSystem();
                }
            };
            self.AdjustScreen();
        },
        /**
         * used to upload a signature from phone
         */
        showUploadSignatureFromPhone: function () {
            var self = this;
            self.hideAll();
            self.view.flxIdentityVerification.setVisibility(true);
            self.view.IdentityVerification.flxColorLine.width = "85.71%";
            self.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
            self.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.DigitalSignature");
            self.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.LinkSentToPhone");
            self.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.ClickTheLinkToContinue");
            self.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.MFA.RESEND");
            self.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.MFA.RESEND");
            self.view.IdentityVerification.btnAction.onClick = function () {
                self.showLoadingIndicator();
            };
            self.view.IdentityVerification.imgflxIdentityVerification.src = ViewConstants.IMAGES.NUO_SIGNATURE_MOBILE;
            self.view.IdentityVerification.btnDONE.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
        },
        /**
         * used to uplad a signature
         */
        showUploadSignatureFromSystem: function () {
            var self = this;
            self.hideAll();
            var data = {
                url: "http://pmqa.konylabs.net/KonyWebBanking/digital_signature_form.pdf",
                filename: kony.i18n.getLocalizedString('i18n.NUO.SignatureForm')
            }
            self.downloadFile(data);
            self.view.flxDigitalSignature.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.DigitalSignatureUploadForm.lblDigitalSignature.text;
            if (self.view.DigitalSignatureUploadForm.flxDocAttachment.isVisible === false) {
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin = ViewConstants.SKINS.BREADCRUM;
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(true);
            }
            else {
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin = ViewConstants.SKINS.BREADCRUMCSR;
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(false);
            }
            self.view.DigitalSignatureUploadForm.flxColorLine.width = "85.71%";
            self.AdjustScreen();
            self.view.DigitalSignatureUploadForm.btnDigitalSignatureBack.onClick = function () {
                self.hideAll();
                self.view.flxDigitalSignatureSelectionMode.setVisibility(true);
                self.view.customheader.lblHeaderMobile.text = self.view.DigitalSignatureSelectionMode.lblDigitalSignature.text;
                self.AdjustScreen();
            };
            this.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.onClick = function () {
                self.loadNuoModule().presentationController.uploadSignature(self.filesToBeUploaded.signature);
            };
            this.view.DigitalSignatureUploadForm.btnUploadForm.onClick = function () {
                self.browseFilesForSignature();
            };
            this.view.DigitalSignatureUploadForm.flxRemoveAttachment.onClick = function () {
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin = ViewConstants.SKINS.BREADCRUM;
                self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(false);
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(true);
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.setEnabled(false);
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PXDISABLED;
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.hoverSkin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PXDISABLED;
            };
        },
        /**
         * handels the credit check error flow
         */
        showCreditCheckErrorFlow: function () {
            var self = this;
            self.hideAll();
            self.view.flxCreditCheckError.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.CreditCheckNAO.lblCreditCheck.text;
            self.view.CreditCheckNAO.btnCreditCheckClose.onClick = function () {
                self.loadNuoModule().presentationController.doLogout();
            }
            self.AdjustScreen();
        },
        /**
         * Download Upload Signature File
         * @param {object} data data
         */
        downloadFile: function (data) {
            CommonUtilities.downloadFile({
                'url': data.url,
                'filename': data.filename
            })
        },
        /**
         * used to show the acknowledgement screen
         */
        showAcknowledgementScreen: function () {
            var self = this;
            self.hideAll();
            self.view.flxReEnterPassword.setVisibility(true);
            self.view.ReEnterPassword.lblSuccessmsg.text = kony.i18n.getLocalizedString("i18n.NUO.ReEnterPasswordMsg");
            self.view.customheader.lblHeaderMobile.text = self.view.ReEnterPassword.lblUserInformation.text;
            self.view.ReEnterPassword.txtEnterPassword.text = "";
            self.view.ReEnterPassword.txtEnterPassword.onKeyUp = function () {
                if (self.view.ReEnterPassword.txtEnterPassword.text.length < 1) {
                    FormControllerUtility.disableButton(self.view.ReEnterPassword.btnProceed)
                }
                else {
                    FormControllerUtility.enableButton(self.view.ReEnterPassword.btnProceed)
                }
            }
            self.view.ReEnterPassword.btnProceed.onClick = function () {
                self.loadNuoModule().presentationController.doCustomerLogin(self.view.ReEnterPassword.txtEnterPassword.text);
            }
            self.AdjustScreen();

        },
        /**
         * used to browse the signature files
         */
        browseFilesForSignature: function () {
            var self = this;
            var config = {
                selectMultipleFiles: false,
                filter: ["application/pdf"]
            };
            self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(false);
            kony.io.FileSystem.browse(config, self.signatureBrowseCallback);
        },
        /**
         * used to handels the signature browse call back schenario
         * @param {object} event event
         * @param {object} file file
         */
        signatureBrowseCallback: function (event, file) {
            var self = this;
            self.getBase64(file[0].file, "signature");
            self.bindSignatureFileToUI(file[0]);
        },
        /**
         * used to bind the signatures to form
         * @param {object} file file
         */
        bindSignatureFileToUI: function (file) {
            var self = this;
            if (self.isFileTypeNotSupported(file.file)) {
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.text = kony.i18n.getLocalizedString("i18n.NUO.FileTypeNotSupported");
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(true);
            } else if (self.isFileSizeExceeds(file)) {
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(true);
            } else {
                if (file) {
                    self.view.DigitalSignatureUploadForm.btnUploadForm.skin = ViewConstants.SKINS.BREADCRUMCSR;
                    self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(false);
                    self.view.DigitalSignatureUploadForm.lblDocName.text = file.name;
                    self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(true);
                    //enable Done button
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.setEnabled(true);
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PX;
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.hoverSkin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PX;
                }
            }
            self.AdjustScreen();
        },

        /**
         * used to hide the topmenu
         */
        hideTopMenu: function () {
            this.view.customheader.headermenu.setVisibility(false);
        },

        /**
         * used to show the top menu
         */
        showTopMenu: function () {
            this.view.customheader.headermenu.setVisibility(true);
            this.view.customheader.headermenu.btnLogout.text = "";
            this.view.customheader.headermenu.flxWarning.setVisibility(false);
            this.view.customheader.headermenu.imgLogout.setVisibility(true);
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                this.loadNuoModule().presentationController.doLogout();
            }.bind(this);
            this.view.forceLayout();
        },

        /**
         * shows the enroll error
         * @param {object} response error
         */
        showVerifyOtpError : function(response)
        {
			this.view.flxDownTimeWarning.setVisibility(true);
			this.view.flxDownTimeWarning.rtxDowntimeWarning.text= response;
            this.AdjustScreen();
        },

        /**
         * shows the enroll error
         * @param {object} error error
         */
        showEnrollError: function (error) {
            if (error === 'userNameError') {
                this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(true);
                this.view.CreateUsernamePassword.imgUsername.setVisibility(false);
                this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = "This username is not available.";
                this.view.forceLayout();
            }
        },
        /**
         * used to shows the login error
         * @param {string} errorMessage error Message
         */
        showLoginError: function (errorMessage) {
            this.view.EnterUsernamePassword.lblError.setVisibility(true);
            this.view.EnterUsernamePassword.lblError.text = errorMessage;
            this.view.forceLayout();
        },

        /**
         * used to shows the upload signature screen
         */
        showUploadSignatureScreen: function () {
            var self = this;
            self.hideAll();
            self.view.flxUploadSignature.setVisibility(true);
            self.view.customheader.lblHeaderMobile.text = self.view.UploadSignature.lblUploadSignatureHeading.text;
            self.view.UploadSignature.flxUploadSignatureMailAddress2.text = "Theodore Lowe";
            self.view.UploadSignature.flxUploadSignatureMailAddress3.text = "Ap #867-859 Sit Rd.";
            self.view.UploadSignature.flxUploadSignatureMailAddress4.text = "Azusa New York - 39531";
            self.AdjustScreen();
            self.view.UploadSignature.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            self.view.UploadSignature.btnProceed.onClick = function () {
                self.loadNuoModule().presentationController.navigateToCreditCheckSuccess();
            }
            self.showTopMenu();
        },

        /**
         * used to show the custom login error message
         * @param {string} errorMessage error message
         */
        showCustomerLoginError: function (errorMessage) {
            this.view.ReEnterPassword.lblWarningReenterPassword.text = errorMessage;
            this.view.ReEnterPassword.lblWarningReenterPassword.setVisibility(true);
            this.view.forceLayout();
        },
        /**
         * handels the form post show
         */
        postShowfrmNewUserOnboarding: function () {
            var self = this;
            this.view.customheader.lblHeaderMobile.text = "Create Account";
            FormControllerUtility.disableButton(this.view.SendOTP.btnSendOTPProceed);
            this.view.BannerNUO.imgBanner.src = ViewConstants.IMAGES.DESKTOP_BANNER_IMAGE;
            FormControllerUtility.disableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            FormControllerUtility.disableButton(this.view.DigitalSignatureUploadForm.btnDigitalSignatureDone);
            self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(false);
            self.view.UploadDocuments.btnUploadDocumentsProceed.skin = ViewConstants.SKINS.SKNBTN3343A8BORDER3343A82PXRADIUS2PXDISABLED;
            self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(false);
            self.view.UploadDocuments.flxDocAttachmentAP.setVisibility(false);
            self.view.UploadDocuments.flxDocAttachmentEP.setVisibility(false);
            self.view.UploadDocuments.flxDocAttachmentIP.setVisibility(false);
            self.view.UploadDocuments.btnAttachAP.skin = ViewConstants.SKINS.BREADCRUM;
            self.view.UploadDocuments.btnAttachAP.setEnabled(true);
            self.view.UploadDocuments.btnAttachEP.skin = ViewConstants.SKINS.BREADCRUM;
            self.view.UploadDocuments.btnAttachEP.setEnabled(true);
            self.view.UploadDocuments.btnAttachIP.skin = ViewConstants.SKINS.BREADCRUM;
            self.view.UploadDocuments.btnAttachIP.setEnabled(true);
            self.view.customheader.imgKony.setFocus(true);
            this.ShowLoginView();
            this.resetPersonalInfoData();
            this.setFlowActions();
            this.view.customheader.headermenu.isVisible = true;
            this.view.onBreakpointChange = function(){
                self.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            if(kony.application.getCurrentBreakpoint() === 640){
                self.view.customheader.flxTopmenu.isVisible = true;
                self.view.customheader.lblHeaderMobile.text = "Create Account";
            }
          applicationManager.getNavigationManager().applyUpdates(this);
            this.AdjustScreen();
        },
        /**
         * handels the UI action items
         */
        setFlowActions: function () {
            var scopeObj = this;
            this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = function () {
                scopeObj.showEnterUsernamePassword();
            };
            this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
                scopeObj.showViews(["flxChooseEligibilityCriteria"]);
            };
            this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function () {
                scopeObj.view.flxMainContainer.setVisibility(false);
                scopeObj.view.flxAccounts.setVisibility(true);
            };
            this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function () {
                scopeObj.view.flxMainContainer.setVisibility(false);
                scopeObj.view.flxAccounts.setVisibility(true);
            };
            this.view.btnContinueProceed.onClick = function () {
                scopeObj.showViews(["flxUploadDocFromPhoneOrComputer"]);
            };
            this.view.SubmitApplication.flxCross.onClick = function () {
                scopeObj.view.flxSubmitApplication.setVisibility(false);
            };
            this.view.SubmitApplication.btnNo.onClick = function () {
                scopeObj.view.flxSubmitApplication.setVisibility(false);
            };
            this.view.DigitalSignatureSelectionMode.flxDigitalSignatureRadiobtn1.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn1, scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn2, " ", " ");
            };
            this.view.DigitalSignatureSelectionMode.flxUserInformationRadioBtn2.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn2, scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn1, " ", " ");
            };
            this.view.ViewReport.btnClose.onClick = function () {
                scopeObj.showViews(["flxDigitalSignature"]);
            };
            this.view.AcknowledgementNAO.btnAckGoToAccounts.onClick = function () {
                applicationManager.getNavigationManager().navigateTo("frmAccountsLanding");
            };
            this.view.DigitalSignatureSelectionMode.btnDigitalSignatureSaveClose.onClick = this.ShowSaveClosePopup;
            this.view.ReviewApplication.btnReviewApplicationSaveClose.onClick = this.ShowSaveClosePopup;
            this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.OpenNewAccount.lblRadioBtn1, scopeObj.view.OpenNewAccount.lblRadioBtn2, " ", " ");
            };
            this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.OpenNewAccount.lblRadioBtn2, scopeObj.view.OpenNewAccount.lblRadioBtn1, " ", " ");
            };

        },
        /**
         * used to show the close popUO
         */
        ShowSaveClosePopup: function () {
            var scopeObj = this;
            this.view.flxSaveClose.setVisibility(true);
            var height = this.SetPopUpHeight();
            this.view.flxSaveClose.height = height + "dp";
            this.view.SaveAndClose.lblPopupMessage.setFocus(true);
            this.view.SaveAndClose.btnNo.onClick = function () {
                scopeObj.view.flxSaveClose.setVisibility(false);
            };
            this.view.SaveAndClose.flxCross.onClick = function () {
                scopeObj.view.flxSaveClose.setVisibility(false);
            };
        },
        /**
         * used to show 
         * @param {object} views views
         */
        showViews: function (views) {
            this.hideAll();
            for (var i = 0; i < views.length; i++) {
                this.view[views[i]].isVisible = true;
                this.view.customheader.lblHeaderMobile.text = this.view[views[i]].text;
            }
            this.view.forceLayout();
        },
        /**
         * used to set the popup height
         * @returns {string} main height
         */
        SetPopUpHeight: function () {
            this.view.forceLayout();
            var mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height;
            return mainheight;
        },
        /**
         * used to hide the flow
         */
        hideAll: function () {
            this.view.flxEnterSecureAccessCode.setVisibility(false);
            this.view.flxChooseEligibilityCriteria.setVisibility(false);
            this.view.flxUploadSignature.setVisibility(false);
            this.view.flxCreateUsernamePassowrd.setVisibility(false);
            this.view.flxEnterUsernamePassword.setVisibility(false);
            this.view.flxCreateAccountUponAccountFound.setVisibility(false);
            this.view.flxOpenNewAccount.setVisibility(false);
            this.view.flxSendOTP.setVisibility(false);
            this.view.flxIdentityVerification.setVisibility(false);
            this.view.flxUserInformationForm.setVisibility(false);
            this.view.flxIdentityVerificationQuestions.setVisibility(false);
            this.view.flxAccountsView.setVisibility(false);
            this.view.flxCreditCheckError.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxDigitalSignatureSelectionMode.setVisibility(false);
            this.view.flxReviewApplication.setVisibility(false);
            this.view.flxEmploymentInformation.setVisibility(false);
            this.view.flxFinancialInformation.setVisibility(false);
            this.view.flxDigitalSignature.setVisibility(false);
            this.view.flxReEnterPassword.setVisibility(false);
            this.view.flxUploadDocuments.setVisibility(false);
            this.view.flxSubmitApplication.setVisibility(false);
            this.view.flxUploadDocFromPhoneOrComputer.setVisibility(false);
            this.view.flxAccounts.setVisibility(false);
            this.view.flxAccountsView.setVisibility(false);
        },
        responsiveViews:{},
        initializeResponsiveViews:function(){
            this.responsiveViews["flxDownTimeWarning"] = this.isViewVisible("flxDownTimeWarning");
            this.responsiveViews["flxContainer"] = this.isViewVisible("flxContainer");
            this.responsiveViews["flxMainContainer"] = this.isViewVisible("flxMainContainer");
            this.responsiveViews["flxMainLeftContainers"] = this.isViewVisible("flxMainLeftContainers");
            this.responsiveViews["flxEnterSecureAccessCode"] = this.isViewVisible("flxEnterSecureAccessCode");
            this.responsiveViews["flxChooseEligibilityCriteria"] = this.isViewVisible("flxChooseEligibilityCriteria");
            this.responsiveViews["flxUploadSignature"] = this.isViewVisible("flxUploadSignature");
            this.responsiveViews["flxCreateUsernamePassowrd"] = this.isViewVisible("flxCreateUsernamePassowrd");
            this.responsiveViews["flxEnterUsernamePassword"] = this.isViewVisible("flxEnterUsernamePassword");
            this.responsiveViews["flxCreateAccountUponAccountFound"] = this.isViewVisible("flxCreateAccountUponAccountFound");
            this.responsiveViews["flxSendOTP"] = this.isViewVisible("flxSendOTP");
            this.responsiveViews["flxUploadDocFromPhoneOrComputer"] = this.isViewVisible("flxUploadDocFromPhoneOrComputer");
            this.responsiveViews["flxIdentityVerification"] = this.isViewVisible("flxIdentityVerification");
            this.responsiveViews["flxUserInformationForm"] = this.isViewVisible("flxUserInformationForm");
            this.responsiveViews["flxUploadDocuments"] = this.isViewVisible("flxUploadDocuments");
            this.responsiveViews["flxDigitalSignature"] = this.isViewVisible("flxDigitalSignature");
            this.responsiveViews["flxFinancialInformation"] = this.isViewVisible("flxFinancialInformation");
            this.responsiveViews["flxEmploymentInformation"] = this.isViewVisible("flxEmploymentInformation");
            this.responsiveViews["flxReviewApplication"] = this.isViewVisible("flxReviewApplication");
            this.responsiveViews["flxDigitalSignatureSelectionMode"] = this.isViewVisible("flxDigitalSignatureSelectionMode");
            this.responsiveViews["flxAcknowledgement"] = this.isViewVisible("flxAcknowledgement");
            this.responsiveViews["flxReEnterPassword"] = this.isViewVisible("flxReEnterPassword");
            this.responsiveViews["flxCreditCheckError"] = this.isViewVisible("flxCreditCheckError");
            this.responsiveViews["flxOpenNewAccount"] = this.isViewVisible("flxOpenNewAccount");
            this.responsiveViews["flxAccountsView"] = this.isViewVisible("flxAccountsView");
            this.responsiveViews["flxMainRight"] = this.isViewVisible("flxMainRight");
            this.responsiveViews["flxAccounts"] = this.isViewVisible("flxAccounts");
            this.responsiveViews["flxFooter"] = this.isViewVisible("flxFooter");
        },
        isViewVisible: function(container){
            if(this.view[container].isVisible){
                return true;
            }else{
                return false;
            }
        },
        onBreakpointChange: function(width) {
            var self = this;
            var views;
            if (width === 640) {
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    self.view[e].isVisible = self.responsiveViews[e];
                });
                self.view.flxMainRight.isVisible = false;
                self.view.customheader.flxTopmenu.isVisible = true;
                self.view.customheader.lblHeaderMobile.text = "Create Account";
                self.TextInputModeChange("N");
            } else if(width === 1024){
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    self.view[e].isVisible = self.responsiveViews[e];
                });
                self.view.flxMainRight.isVisible = false;
                self.view.customheader.topmenu.flxMenusMain.isVisible = false;
                self.view.customheader.topmenu.isVisible = false;
                self.view.customheader.lblHeaderMobile.isVisible = false;
                self.TextInputModeChange("N");
            }
            else {
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    self.view[e].isVisible = self.responsiveViews[e];
                });
                self.view.flxMainRight.isVisible = true;
                self.view.customheader.topmenu.flxMenusMain.isVisible = false;
                self.view.customheader.topmenu.isVisible = false;
                self.view.customheader.lblHeaderMobile.isVisible = false;
                self.TextInputModeChange("A");
            }
            self.hideAll();
            self.view.flxSendOTP.isVisible = true;
            self.view.forceLayout();
            self.AdjustScreen();
        },
        ShowLoginView: function(){
            this.view.flxContainer.top = "90dp";
            this.view.customheader.FlexContainer0e2898aa93bca45.height = "70dp";
            this.view.customheader.flxBottomContainer.isVisible = true;
            this.view.customheader.flxBottomContainer.height = "70dp";
            this.view.customheader.flxBottomContainer.skin = "sknFlxffffffShadowe3e3e312px";
            this.view.customheader.flxBottomContainer.clipBounds = false;
            this.view.customheader.flxBottomBlue.isVisible=false;
            this.view.customheader.flxTopmenu.setVisibility(false);
            this.view.customheader.headermenu.flxMessages.isVisible = false;
            this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = false;
            this.view.customheader.headermenu.flxResetUserImg.isVisible = false;
            this.view.customheader.headermenu.flxUserId.isVisible = false;
            this.view.customheader.flxSeperatorHor2.setVisibility(false);
            this.view.customheader.headermenu.flxWarning.isVisible = true;
            this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.customheader.headermenu.imgLogout.setVisibility(false);
            this.view.customheader.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
        },
       TextInputModeChange:function(val) {
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit1.textInputMode = val;
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit2.textInputMode = val;
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit3.textInputMode = val;
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit4.textInputMode = val;
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit5.textInputMode = val;
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit6.textInputMode = val;
       }
    };

});
