define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_cf2b18e33e634229831f7d60dedf6c46: function AS_FlexContainer_cf2b18e33e634229831f7d60dedf6c46(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnAction **/
    AS_Button_b33ede6b507443869ccfb1afc00a99f8: function AS_Button_b33ede6b507443869ccfb1afc00a99f8(eventobject, context) {
        var self = this;
        this.MakeTransferAction();
    }
});