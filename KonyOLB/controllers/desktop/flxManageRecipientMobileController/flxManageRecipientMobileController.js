define({ 
	
  showSelectedRow: function(){
    var previousIndex
    var index = kony.application.getCurrentForm().tableView.segP2P.selectedIndex;
    var rowIndex = index[1];
    var data = kony.application.getCurrentForm().tableView.segP2P.data;
    //data[rowIndex].template = "flxManageRecipientSelected";
      for(i=0;i<data.length;i++)
         {
           if(i==rowIndex)
             {
               data[i].imgDropdown = "chevron_up.png";
               if(kony.application.getCurrentBreakpoint()==640){
                  data[i].template = "flxManageRecipientSelectedMobile";
               }else{
                  data[i].template = "flxManageRecipientSelected";
               }
             }
           else
             {
               data[i].imgDropdown = "arrow_down.png";
               if(kony.application.getCurrentBreakpoint()==640){
                  data[i].template = "flxManageRecipientMobile";
               }else{
                  data[i].template = "flxManageRecipient";
               }
             }
         }  
  //  kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[rowIndex], rowIndex);
     kony.application.getCurrentForm().tableView.segP2P.setData(data);
      this.AdjustScreen(150);
  },
  //UI Code
  AdjustScreen: function(data) {
    var currForm = kony.application.getCurrentForm();
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = currForm.customheader.frame.height + currForm.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - currForm.flxFooter.frame.height;
        if (diff > 0) 
            currForm.flxFooter.top = mainheight + diff + data + "dp";
        else
            currForm.flxFooter.top = mainheight + data + "dp";        
     } else {
        currForm.flxFooter.top = mainheight + data + "dp";
     }
    currForm.forceLayout();
  },  


 });

