define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmUserManagement **/
    AS_Form_i7d826f1f6ee43d781bcb3838edffd6c: function AS_Form_i7d826f1f6ee43d781bcb3838edffd6c(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmUserManagement **/
    AS_Form_cea29091ea9c456490f5eb5fe8a1536e: function AS_Form_cea29091ea9c456490f5eb5fe8a1536e(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmUserManagement **/
    AS_Form_ad32ac2c9fc945d1952af4fa86d0b342: function AS_Form_ad32ac2c9fc945d1952af4fa86d0b342(eventobject) {
        var self = this;
        this.postShow();
    }
});