define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {

    return /** @alias module:frmUserManagementController */ {

        /** 
        * Method to display the footer at the end of the screen by calculating the size of screen dynamically 
        * @param {integer} data value
        **/
       adjustScreen: function () {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.frame.height + this.view.flxMain.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }                 
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }
        },

        /**
         * hide all ui flexes in user management form
         */
        resetUI: function () {
            this.view.flxLeftContainer.setVisibility(false);
            this.view.flxLeftContainerSearchRoles.setVisibility(false);
            this.view.flxLeftContainerAccessAccounts.setVisibility(false);
            this.view.flxLeftContainerTransferPermissions.setVisibility(false);
            this.view.flxLeftContainerAdditionalServices.setVisibility(false);
            this.view.flxErrorMessage.setVisibility(false);
          	this.view.InfoIconPopup.setVisibility(false);
			this.view.btnCheckAvailability.setEnabled(false);
			this.view.btnCheckAvailability.skin = ViewConstants.SKINS.USER_BUTTON_DISABLED;
            this.view.flxDetailsContainer.lblValidUserName.text = kony.i18n.getLocalizedString('i18n.enrollNow.validUsername');
            this.setServerError({ show: false });
            this.adjustScreen();
        },

        /**
         * Method will invoke on form init
         */
        initActions: function () {
          	var scopeObj = this;
            this.validationUtilManager = applicationManager.getValidationUtilManager();
            this.view.btnProceedCreate.onClick = this.onUserDetailsProceedBtnClick.bind(this);
            this.view.btnCheckAvailability.onClick = this.onCheckUserNameBtnClick.bind(this);
            this.view.btnCancelCreate.onClick = function() {
                scopeObj.navigateToBBUsersDashboard();
                scopeObj.adjustScreen();
            }
            this.view.InfoIconPopup.flxCross.onClick = function () {
              scopeObj.view.InfoIconPopup.setVisibility(false);
            }         
			this.initRolesActions();
            this.staticBindingForAccountAccess();
            this.staticBindingForTransactionLimits();
            this.view.imgCloseDowntimeWarning.onTouchEnd = this.setServerError.bind(this,{ show: false });
            this.view.tbxName.onKeyUp = this.updateUserDetailsProceedState.bind(this);
			this.view.tbLastName.onKeyUp = this.updateUserDetailsProceedState.bind(this);
            this.view.tbxPhoneNum.onKeyUp = this.onEnteringPhoneNumber.bind(this);
            this.view.tbxEmail.onKeyUp = this.onUserEmailChanged.bind(this);
            this.view.tbxSSN.onKeyUp = this.onEnteringSSN.bind(this);
            this.view.CustomDate.onTextChange(this.isDobValid.bind(this));
            this.view.tbxUsername.onBeginEditing = this.onEnteringUserName.bind(this);
            this.view.tbxUsername.onKeyUp = this.onUserNameonKeyUp.bind(this);
			
        },

        /**
         * Method will invoke on form pre show
         */
        preShow: function () {
            this.view.customheader.forceCloseHamburger();
			this.view.customheader.customhamburger.activateMenu("User Management","Create A User");
            applicationManager.getNavigationManager().applyUpdates(this);            
        },
		
		/**
         *  Method will invoke for roles screen
  		 */
		initRolesActions: function(){
         	var scopeObj = this;
            this.view.btnProceedRoles.onClick = this.onUserRolesProceedOrBackBtnClick.bind(this);
          	this.view.btnBackAccessRoles.onClick = function(event) {
                scopeObj.onUserRolesProceedOrBackBtnClick(event,true);
				scopeObj.loadBusinessBankingModule().presentationController.showCreateUser({edit : true});
                scopeObj.adjustScreen();
            };
            this.view.btnCancelRoles.onClick = function() {
                scopeObj.navigateToBBUsersDashboard();
                scopeObj.adjustScreen();
            };
            
            this.view.SegSeachInfo.widgetDataMap = {
                 "lblRoleName":"lblRoleName",
				 "richTextDescription":"richTextDescription",
				 "flxSelectRole":"flxSelectRole",
				 "imgSelectRole":"imgSelectRole"
            };
          	
        },

        /**
         * Method to handle static bindings for Account Access page
         */
        staticBindingForAccountAccess : function () {
            var scopeObj = this;
            scopeObj.view.btnProceedAccess.onClick = scopeObj.onUserAccountAccessProceedOrBackBtnClick.bind(this);
            scopeObj.view.btnCancelAccess.onClick = function() {
                scopeObj.navigateToBBUsersDashboard();
                //scopeObj.adjustScreen();
            }
            scopeObj.view.btnBackAccess.onClick = function(e) {
                scopeObj.onUserAccountAccessProceedOrBackBtnClick(e,true);
				scopeObj.loadBusinessBankingModule().presentationController.fetchRoles();
                //scopeObj.adjustScreen();
            }            
            scopeObj.view.flxInfoIcon.onClick = function() {
                scopeObj.view.InfoIconPopup.flxInformation.height = "105dp";
                if (scopeObj.view.InfoIconPopup.isVisible === true) {
					scopeObj.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
					scopeObj.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.createUser.InfoIconTextForAccountAccess");
					scopeObj.view.InfoIconPopup.setVisibility(true);
                    scopeObj.view.InfoIconPopup.top = scopeObj.view.flxInfoIcon.frame.y + 275 + "dp";
                    scopeObj.view.InfoIconPopup.left = scopeObj.view.flxInfoIcon.frame.x + 47 + "dp";
				}
            }
            this.view.segAccountPermissions.widgetDataMap = {
                "lblGroupName" : "lblGroupName",
                "lblCheckGroupView" : "lblCheckGroupView",
                "lblViewTitle" : "lblViewTitle",
                "lblCheckGroupTransaction": "lblCheckGroupTransaction",
                "lblTransactionTitle": "lblTransactionTitle",
                "lblAccountName": "lblAccountName",
                "lblViewAccess": "lblViewAccess",
                "lblTransactionAccess": "lblTransactionAccess",
                "lblAccountAccess": "lblAccountAccess",
                "lblNameValue": "lblNameValue",
                "lblCheckView": "lblCheckView",
                "flxImgCheckAccount": "flxImgCheckAccount",
                "lblCheckTransaction" :"lblCheckTransaction",
                "lblCheckAccount" :"lblCheckAccount"
            };
        },

        /**
         * Method to handle static bindings for Transaction Limits page
         */
        staticBindingForTransactionLimits : function () {
            var scopeObj = this;
            scopeObj.view.btnProceedTransferPermissions.onClick = scopeObj.onUserTransactionAccessNextBtnClick.bind(this);
            scopeObj.view.btnCancelTransferPermissions.onClick = function() {
                scopeObj.navigateToBBUsersDashboard();
                //scopeObj.adjustScreen();
            }
            scopeObj.view.btnBackTransferPermission.onClick = function() {
                scopeObj.onBackTransferPermission();
				scopeObj.loadBusinessBankingModule().presentationController.showAllAccounts();
            }
            scopeObj.view.flxInfoTransactionAccess.onClick = function () {
				scopeObj.view.InfoIconPopup.flxInformation.height = "80dp";
                if (scopeObj.view.InfoIconPopup.isVisible === true) {
                    scopeObj.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
                    scopeObj.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.createUser.InfoIconTextForTransactionAccess");
                    scopeObj.view.InfoIconPopup.setVisibility(true);
                    scopeObj.view.InfoIconPopup.top = scopeObj.view.flxInfoTransactionAccess.frame.y + 275 + "dp";
                    scopeObj.view.InfoIconPopup.left = scopeObj.view.flxInfoTransactionAccess.frame.x + 45 + "dp";
                }
			}
            scopeObj.view.segBBTransferPermissions.widgetDataMap = {
                "lblTransferType" : "lblTransferType",
                "lblMaxTransactionLimit" : "lblMaxTransactionLimit",
                "flxMaxTransactionLimit" : "flxMaxTransactionLimit",
				"lblCurrSymbol": "lblCurrSymbol",
				"lblDailyLimitCurrSymbol": "lblDailyLimitCurrSymbol",
                "tbxMaxTransactionValue" : "tbxMaxTransactionValue",
                "lblMaxDailyLimit" : "lblMaxDailyLimit",
                "flxMaxDailyLimit" : "flxMaxDailyLimit",
                "tbxMaxDailyValue" : "tbxMaxDailyValue"
            };
        },

        /**
         * Method will invoke on form post show
         */
        postShow: function () {
            this.adjustScreen();
        },

        /**
         * Method to update form using given context
         * @param {object} context depending on the context the appropriate function is executed to update view  
         */
        updateFormUI: function (context) {

            if (context.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            }
            else if (context.progressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }

            if (context.serverError === true) {
                this.setServerError({ show: true , errorMessage: context.errorMessage});
            } else if (context.serverError === false) {
                this.setServerError({ show: false });
            }
          
            if (context.invalidUserError) {
                this.setOFACError();
            }
            if (context.invalidUser) {
                this.showInvalidUserError();
            }
            if (context.userNameAvailability) {
                this.showUserNameAvailabilityUI(context.userNameAvailability);
            }
			if (context.id){
				this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.konybb.Common.EditUser");
            }
            if (context.createNewUser) {
				this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.konybb.Common.CreateUser");
                this.showCreatUserUI(null, context.onCancel);
            }

            if (context.updateUser) {
                this.showCreatUserUI(context.updateUser, context.onCancel);
            }
            if (context.userRoles) {
                this.showRolesUI(context.userRoles, context.selectedRoleId);
            }
            if(context.accounts){
                this.showAccountAccessUI(context.accounts, context.selectedAccounts);
            }
            if(context.transactionLimits){
                this.showTransactionAccessUI(context.transactionLimits, context.selectedLimits);
            }
            if(context.userNamePolicies){
                this.setuserNamePolicies(context.userNamePolicies.usernamerules);
            }
            if(context.createUserFail){
                this.showUserCreationFailError(context.errMsg);
            }
        },
		
      
      	/**
         *  Method to set the username policies
         * @param {object} userNamePolicies - which consists of list of username policies
        */
        setuserNamePolicies: function(userNamePolicies) {
            this.view.flxRulesUsername.rtxRulesUsername.text = userNamePolicies.content;
        },
      
		/**
         * Method to check whether user name is available or not.
		 * @param {var} userNameAvailability is a boolean value
         */
        showUserNameAvailabilityUI: function (userNameAvailability) {
            if (userNameAvailability.isAvailable) {
                this.view.tbxUsername.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;//Default skin
                this.view.flxRulesUsername.setVisibility(false);
            	this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);
                this.view.btnCheckAvailability.setVisibility(false);
                this.view.flxAvailabilityStatus.setVisibility(true);
                this.view.flxUsernameAvailability.forceLayout();
				this.setErrorMessaage({ show: false });                                               
            } else {
                this.showInvalidUserError();
                this.view.tbxUsername.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
                this.view.btnCheckAvailability.setVisibility(true);
                this.view.flxAvailabilityStatus.setVisibility(false);               
            }
			this.adjustScreen();
            this.isUserNameAvailable = userNameAvailability.isAvailable;
            this.updateUserDetailsProceedState();
        },

        /**
         * Method to display error message on invalid user
         */
        showInvalidUserError: function () {
            this.setErrorMessaage({ show: true, errMsgi18nKey: "i18n.enrollNow.wrongUserName" }); //update i18n key
        },

        /**
         * Method to display the error message from the backend
		 * @param {var} errorMessage is a string having the error from backend
         */
        showUserCreationFailError : function (errorMessage) {
            this.setServerError({ show: true, errorMessage: errorMessage });
        },

        /**
         * Method to display error message 
         */
        setErrorMessaage: function (context) {
            if (context.show) {
                this.view.lblError.text = kony.i18n.getLocalizedString(context.errMsgi18nKey || "i18n.StopPayments.errormessages.InvalidDetails")
                this.view.flxErrorMessage.setVisibility(true);
                this.view.flxErrorMessage.setFocus();

            } else {
                this.view.flxErrorMessage.setVisibility(false);
            }
            this.adjustScreen();
        },

        /**
         * Method to display server error.
         * @param {object} context - server error context object
         */
        setServerError: function (context) {
            if (context.show) {
                this.view.flxMainWrapper.setVisibility(true);
                this.view.lblDowntimeWarning.text = context.errorMessage ||  kony.i18n.getLocalizedString(context.errMsgi18nKey || "i18n.common.OoopsServerError")
                this.view.flxDowntimeWarning.setFocus();

            } else {
                this.view.flxMainWrapper.setVisibility(false);
            }
            this.adjustScreen();
        },
      
        /**
         * Method to display OFAC/CIP  error.
         */
        setOFACError: function (context) {
            this.setErrorMessaage({ show: true, errMsgi18nKey: "i18n.userManagement.OFAC/CIPCheck" });
        },

        /**
         * Method to load and returns the Business User Module Object
         * @returns {object} Method to load and returns the Business User Module Object
         */
        loadBusinessBankingModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
        },


        /**
         * Method to display create user ui.
         * @param {object} userObj - user details object
         */
        showCreatUserUI: function (userObj) {
            var scopeObj = this;
            scopeObj.resetUI();
            this.view.flxLeftContainer.setVisibility(true);
            this.view.CustomDate.setDateFormat(applicationManager.getFormatUtilManager().getDateFormat());
            this.populateUserDetails(userObj || {});
            this.updateUserDetailsProceedState();
            this.adjustScreen();            
        },

        /**
         * Method to pre populate user details in create form.
         * @param {object} userObj - user details object
         */
        populateUserDetails: function(userObj) {
            this.view.tbxName.text = userObj.FirstName || "";
            this.view.tbxMiddleName.text = userObj.MiddleName || "";
            this.view.tbLastName.text = userObj.LastName || "";
            this.view.tbxEmail.text = userObj.Email || "";
            this.view.tbxSSN.text = userObj.Ssn || "";
            this.view.tbxPhoneNum.text = userObj.Phone || "";
            this.view.tbxDriversLicense.text = userObj.DrivingLicenseNumber || "";
            this.view.tbxUsername.text = userObj.UserName || "";
			if(userObj.id){
				this.view.tbxUsername.setEnabled(false);
				this.view.tbxSSN.setEnabled(false);
				this.isUserNameAvailable = true;
                this.view.tbxSSN.skin = ViewConstants.SKINS.DISABLED;
                this.view.tbxUsername.skin = ViewConstants.SKINS.DISABLED;
			}else{
				this.view.flxRulesUsername.rtxRulesUsername.text = "";
				this.loadBusinessBankingModule().presentationController.getUserNamePolicies();
				this.view.tbxUsername.setEnabled(true);
				this.view.tbxSSN.setEnabled(true);
                this.view.tbxSSN.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
                this.view.tbxUsername.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
                if(this.validationUtilManager.isUsernameValidForPolicy(this.view.tbxUsername.text) && (this.view.tbxUsername.text.length !== 0)){
                	this.isUserNameAvailable = true;  
                }
              else{
				this.isUserNameAvailable = false;
              }
			}
			if(userObj.UserName){
              this.view.flxAvailabilityStatus.setVisibility(true);
			  this.view.btnCheckAvailability.setVisibility(false);
            }
            else{
              this.view.flxAvailabilityStatus.setVisibility(false);
			  this.view.btnCheckAvailability.setVisibility(true);
            }
            this.view.CustomDate.setDate(userObj.DateOfBirth ? CommonUtilities.getFrontendDateString(userObj.DateOfBirth) : "");
        },

        /**
         * validate all user details 
		 * @return {boolean} true:if all the user details are correct ,false: if user details are not valid
         */
        updateUserDetailsProceedState: function() {
            FormControllerUtility.disableButton(this.view.btnProceedCreate);
			this.view.flxRulesUsername.setVisibility(false);
            this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);
            if(CommonUtilities.isEmptyString(this.view.tbxName.text)) {
                return false;
            }
            if(CommonUtilities.isEmptyString(this.view.tbLastName.text)) {
                return false;
            }
 			if(CommonUtilities.isEmptyString(this.view.tbxEmail.text)) {
                return false;
            }
            if(CommonUtilities.isEmptyString(this.view.tbxPhoneNum.text)) {
                return false;
            }
            if(CommonUtilities.isEmptyString(this.view.tbxSSN.text)) {
                return false;
            }
            if (CommonUtilities.isEmptyString(this.view.tbxUsername.text) || (!this.isUserNameAvailable) && (this.view.tbxUsername.text.length !== 0)) {
                this.view.btnCheckAvailability.skin = ViewConstants.SKINS.USER_BUTTON_DISABLED;
                this.view.btnCheckAvailability.setEnabled(false);
                this.adjustScreen();
                return false;
            }
          
            if(CommonUtilities.isEmptyString(this.view.CustomDate.getDate()) || !CommonUtilities.isValidDOB(this.view.CustomDate.getDate())){
              this.view.CustomDate.flxWrapper.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.konybb.createUser.error.InvalidDOB"
                }); 
                return false;
            }   

			if(!this.validationUtilManager.isValidSSNNumber(this.view.tbxSSN.text)){
              this.view.tbxSSN.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.login.incorrectSSN"
                });
			return false;
          	} 
		  
 			if(!this.validationUtilManager.isValidEmail(this.view.tbxEmail.text)){
                this.view.tbxEmail.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
                this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.konybb.createUser.error.InvalidEmail"
                });
				return false;
            }
          
			if(!this.validationUtilManager.isValidPhoneNumber(this.view.tbxPhoneNum.text)){
              this.view.tbxPhoneNum.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.profile.notAValidPhoneNumber"
                });
			  return false;				
         	} 
            FormControllerUtility.enableButton(this.view.btnProceedCreate);
        },
		
		/**
         * Method will validate the ssn number on text changed
        */
        onEnteringSSN: function(){
		  this.view.flxRulesUsername.setVisibility(false);
          this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);  
		  if(this.view.tbxSSN.text.length > 0){
      	  if(!this.validationUtilManager.isValidSSNNumber(this.view.tbxSSN.text)){
			  FormControllerUtility.disableButton(this.view.btnProceedCreate);
              this.view.tbxSSN.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.login.incorrectSSN"
                });             
          } 
          else{
              this.setErrorMessaage({
                    show: false
            });
            this.view.tbxSSN.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;//Default skin
            this.updateUserDetailsProceedState();  
            }
		  }
          else{
            this.updateUserDetailsProceedState();  
          }
        },
		
        /**
         * Method will execute on email text changed
         */
        onUserEmailChanged: function(){
			this.view.flxRulesUsername.setVisibility(false);
            this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);  
            if(this.view.tbxEmail.text.length > 0){
            FormControllerUtility.disableButton(this.view.btnProceedCreate);
            this.view.tbxEmail.skin =  ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;

            if(!this.validationUtilManager.isValidEmail(this.view.tbxEmail.text)){
                this.view.tbxEmail.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
                this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.konybb.createUser.error.InvalidEmail"
                });
            }
            else{
              this.setErrorMessaage({
                    show: false
            });
            this.view.tbxEmail.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;//Default skin
            this.updateUserDetailsProceedState();  
            }
			}
          else{
            this.updateUserDetailsProceedState();  
          }
        },
		
		/**
         * Method will validate the entered phone number
        */
        onEnteringPhoneNumber: function(){
		  this.view.flxRulesUsername.setVisibility(false);
          this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);  
		  if(this.view.tbxPhoneNum.text.length > 0){
      	  if(!this.validationUtilManager.isValidPhoneNumber(this.view.tbxPhoneNum.text)){
			  FormControllerUtility.disableButton(this.view.btnProceedCreate);
              this.view.tbxPhoneNum.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.profile.notAValidPhoneNumber"
                });             
          } 
          else{
              this.setErrorMessaage({
                    show: false
            });
            this.view.tbxPhoneNum.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;//Default skin
            this.updateUserDetailsProceedState();  
            }
		  }
          else{
            this.updateUserDetailsProceedState();  
          }
        },

      	 /**
         * Method will validate the entered DOB
        */
        isDobValid: function(){
		  this.view.flxRulesUsername.setVisibility(false);
          this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);  	
          if(CommonUtilities.isEmptyString(this.view.CustomDate.getDate()) || !CommonUtilities.isValidDOB(this.view.CustomDate.getDate())){
             FormControllerUtility.disableButton(this.view.btnProceedCreate);
              this.view.CustomDate.flxWrapper.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;//Error skin
			  this.setErrorMessaage({
                    show: true,
                    errMsgi18nKey: "i18n.konybb.createUser.error.InvalidDOB"
                });             
          } 
          else{
              this.setErrorMessaage({
                    show: false
            });
            this.view.CustomDate.flxWrapper.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;//Default skin
            this.updateUserDetailsProceedState();  
          }
          
        },
		
        /**
         * Method to handle user name start entered
         */
        onEnteringUserName: function() {
            var validationUtility = applicationManager.getValidationUtilManager();
            if (this.view.flxRulesUsername.rtxRulesUsername.text.length !== 0) {
                this.view.flxRulesUsername.setVisibility(true);
            }
            if (!validationUtility.isUsernameValidForPolicy(this.view.tbxUsername.text) && this.view.tbxUsername.text.length !== 0) {
                this.view.flxDetailsContainer.lblValidUserName.setVisibility(true);
                this.view.btnCheckAvailability.setEnabled(false);
                this.view.btnCheckAvailability.skin = ViewConstants.SKINS.USER_BUTTON_DISABLED;
                FormControllerUtility.disableButton(this.view.btnProceedCreate);
            } else {
                this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);
            }
            this.view.flxUsernameAvailability.forceLayout();
            this.adjustScreen();
        },
     
        /**
         * Method to handle user name keyup
         */
		onUserNameonKeyUp: function() {
            FormControllerUtility.disableButton(this.view.btnProceedCreate);
			if(this.view.tbxUsername.text.length > 0){
            this.view.flxDetailsContainer.lblValidUserName.setVisibility(true);
            if(this.view.flxRulesUsername.rtxRulesUsername.text.length !== 0){  
            this.view.flxRulesUsername.setVisibility(true);
            }
            this.view.btnCheckAvailability.setVisibility(true);
            this.view.flxAvailabilityStatus.setVisibility(false);
            this.isUserNameAvailable = false;
            if(!this.validationUtilManager.isUsernameValidForPolicy(this.view.tbxUsername.text)) {
				this.view.btnCheckAvailability.skin = ViewConstants.SKINS.USER_BUTTON_DISABLED;
                this.view.btnCheckAvailability.setEnabled(false);
            } else {
                this.view.flxDetailsContainer.lblValidUserName.setVisibility(false);
                this.view.btnCheckAvailability.setEnabled(true);
				this.view.btnCheckAvailability.skin = ViewConstants.SKINS.USER_BUTTON_ENABLED;  
            }
            this.adjustScreen();
			}
        },
        /**
         * Method to handle on check user name availability button clicked.
         */
        onCheckUserNameBtnClick: function () {
            if(this.validationUtilManager.isUsernameValidForPolicy(this.view.tbxUsername.text)){
            this.loadBusinessBankingModule().presentationController.validateUserName({
                "UserName": this.view.tbxUsername.text.toString()
            });
            this.view.flxUsernameAvailability.forceLayout();
            }
        },

        /**
         * Method to handle proceed button in user details form.
         */
        onUserDetailsProceedBtnClick: function () {
            var userDetails = {
                firstName: this.view.tbxName.text,
                middleName: this.view.tbxMiddleName.text,
                lastName: this.view.tbLastName.text,
                email: this.view.tbxEmail.text,
                ssn: this.view.tbxSSN.text,
                phoneNumber: this.view.tbxPhoneNum.text,
                driverLicenseNumber: this.view.tbxDriversLicense.text,
                userName: this.view.tbxUsername.text,
                dob: this.view.CustomDate.getDate()
            };
            this.loadBusinessBankingModule().presentationController.updateUserDetails(userDetails);
        },
		
		/**
		* Method to show user roles
		* @param {object} roles - which consists of list of roles
        * @param {object} selectedRoleId - consists of roles already selected
		*/
        showRolesUI: function(roles, selectedRoleId) {
            var scopeObj = this;
            scopeObj.resetUI();
            this.view.flxLeftContainerSearchRoles.setVisibility(true);
            var rolesData = scopeObj.populateRolesSegment(roles, selectedRoleId);
            scopeObj.view.SegSeachInfo.setData(rolesData);
			scopeObj.selectRole(roles, selectedRoleId);
            scopeObj.view.SegSeachInfo.onRowClick = scopeObj.rolesAccessValidation.bind(scopeObj);
            scopeObj.rolesAccessValidation();
            scopeObj.adjustScreen();
        },
		
		/**
         * Method to select the previously selected role
         *  @param {object[]} roles - which consists of list of roles
         *  @param {object} selectedRoleId - consists of roles already selected
         *  @return {object} selectedRoleId - selected roles
         */
		selectRole : function(roles, selectedRoleId) {
			var scopeObj = this;
			var selectedRoleIndex = roles.map(function(role){
				return role.id;
			}).indexOf(selectedRoleId);
			if(selectedRoleIndex>=0 ){
				scopeObj.view.SegSeachInfo.selectedIndex =[0,selectedRoleIndex];
			}
		},
		
		/**
         * Method to populate the Segment with roles
         * @param {object[]} roles - which consists of list of roles
         * @returns {object} mapping for the Roles segment
         */
        populateRolesSegment : function(roles){
			var scopeObj = this;
            return roles.map(function(role){
                return {
                    "lblRoleName":  role.Name, 
                    "roleData" : role,
                    "richTextDescription" : role.Description,//will depend on the form of backend data
                    "flxSelectRole": "flxSelectRole",
                    "imgSelectRole": {
                        src: ViewConstants.IMAGES.RADIO_BTN_INACTIVE,
                        onClick: scopeObj.rolesAccessValidation.bind(scopeObj)
                    }
                }
            });
        },
       
        /**createNewUser
         * Method to handle navigate to Business Banking account Dashboard on cancellation of Create User 
         */
        rolesAccessValidation: function() {
            var scopeObj = this;
            var rolesSeg = scopeObj.view.SegSeachInfo;
            if(rolesSeg.selectedIndex) {
                FormControllerUtility.enableButton(scopeObj.view.btnProceedRoles);
                return;
            }
            FormControllerUtility.disableButton(scopeObj.view.btnProceedRoles);
        },

        /**Confirm role
         * Method to handle navigate account access after confirming the roles 
         * @param {object} event - consists of event 
         * @param {object} isBack - consists of parameter specifying if it is a back scenario
         */
        onUserRolesProceedOrBackBtnClick: function (event,isBack) {
            var scopeObj = this;
            var rolesSeg = scopeObj.view.SegSeachInfo;
            var selectedRole = {};
			if(!isBack){
				selectedRole = rolesSeg.selectedItems[0].roleData;
			} else {
				if(rolesSeg.selectedItems){
					selectedRole = rolesSeg.selectedItems[0].roleData;
				}
			}
            this.loadBusinessBankingModule().presentationController.confirmRole(selectedRole);
			if(!isBack){
				this.loadBusinessBankingModule().presentationController.showAllAccounts();
			}
            //this.showAccountAccessUI();
        },


        /**
         * Method to handle UI of the Account Access screen
         *  @param {object} accounts - which consists of list of selected accounts of the user 
         *  @param {object} selectedAccounts - consists of accounts already selected 
         */
        showAccountAccessUI: function (accounts, selectedAccounts) {     
            var scopeObj = this;
            scopeObj.resetUI();
            scopeObj.view.flxLeftContainerAccessAccounts.setVisibility(true);
            var accountsData = scopeObj.populateAccountSegment(accounts, selectedAccounts);
            scopeObj.view.segAccountPermissions.setData(accountsData);
            scopeObj.accountAccessValidation();
            scopeObj.adjustScreen();
        },

        /**
         * Method to populate the Segment with account-list
         *  @param {object} accounts - which consists of list of accounts
         *  @param {object} selectedAccounts - consists of accounts already selected
         * @returns {object} formatted accounts in the segment
         */
        populateAccountSegment: function(accounts, selectedAccounts) { 
            var scopeObj = this;
			var selectedAccounts = selectedAccounts ? selectedAccounts.map(function(account){
				return account.Account_id || account.id;
			}): [];
            return accounts.map(function(account, index) {
                return {
                    "lblNameValue": CommonUtilities.getAccountDisplayNameNew(account),
                    "accountName": account.accountName,
                    "accountId": account.accountID, 
                    "flxImgCheckAccount": "flxImgCheckAccount",
                    "lblCheckAccount": {
                        text : (selectedAccounts.indexOf(account.accountID) >= 0) ?  OLBConstants.FONT_ICONS.CHECBOX_SELECTED : OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED,
            			skin : (selectedAccounts.indexOf(account.accountID) >= 0) ?  OLBConstants.SKINS.CHECKBOX_SELECTED_SKIN : OLBConstants.SKINS.CHECKBOX_UNSELECTED_SKIN,
                        onClick: scopeObj.onAccountCheckboxClick.bind(scopeObj, index)
                    }
                }
            });
        },

        /**
         * Method to check or uncheck the checkbox
         * @param {var} index : specifies the row-index 
         */
        onAccountCheckboxClick : function (index) {
            var scopeObj = this;
            var accountData = scopeObj.view.segAccountPermissions.data;
            FormControllerUtility.toggleFontCheckbox(accountData[index].lblCheckAccount);
            //scopeObj.view.segAccountPermissions.setData(accountData)
            scopeObj.view.segAccountPermissions.setDataAt(accountData[index], index);
            scopeObj.accountAccessValidation();
        },

        /**
         * Method to handle navigate to Business Banking Users Dashboard on cancellation of Create User 
         */
        accountAccessValidation : function() {
            var scopeObj = this;
            var accountSegData = scopeObj.view.segAccountPermissions.data;
            for(var i in accountSegData) { 
                if(FormControllerUtility.isFontIconChecked(accountSegData[i].lblCheckAccount)){
                    FormControllerUtility.enableButton(scopeObj.view.btnProceedAccess);
                    return;
                }
            }
            FormControllerUtility.disableButton(scopeObj.view.btnProceedAccess);
        },

        /**
         * Method to handle navigate to Business Banking Users Dashboard on cancellation of Create User 
         */
        navigateToBBUsersDashboard : function () {
            this.loadBusinessBankingModule().presentationController.navigateToUsers();
        },

        /**
         * Method to handle navigate to Business Banking Presentation Controller on click of Proceed on Account Selection screen
         *  @param {object} event - consists of event 
         *  @param {object} isBack - consists of parameter specifying if it is a back scenario
         */
        onUserAccountAccessProceedOrBackBtnClick: function (event,isBack) {
            var scopeObj = this;
            var accounts = [];
            var accountSegData = scopeObj.view.segAccountPermissions.data;
            for(var i in accountSegData) {  
                if(FormControllerUtility.isFontIconChecked(accountSegData[i].lblCheckAccount)){
                    var account = {
						"id": accountSegData[i].accountId,
                        "Account_Name": accountSegData[i].accountName
                    };
					accounts.push(account);
                }
            }
            this.loadBusinessBankingModule().presentationController.confirmAccounts(accounts);
            if(!isBack){
                this.loadBusinessBankingModule().presentationController.fetchTransactionLimits();
            }
        },

        /**
         * Method to handle the display of Transaction Limit screen UI
         *  @param {object} limits - which consists of list of accounts
         *  @param {object} selectedLimits - consists of limits already entered
         */
        showTransactionAccessUI: function (limits, selectedLimits) {
            var scopeObj = this;
            scopeObj.resetUI();
            scopeObj.view.flxLeftContainerTransferPermissions.setVisibility(true);
            var transactionLimitsData = scopeObj.populateTransferLimitSegment(limits, selectedLimits);
            scopeObj.view.segBBTransferPermissions.setData(transactionLimitsData);
            scopeObj.validateTransactionLimitsUI();
            scopeObj.adjustScreen();
        },

        /**
         * Method to populate the Segment for transaction limits
         * @param {object} limits - which consists of list of account-limits
         * @param {object} selectedLimits - consists of limits already entered
         * @returns {object} formatted accounts in the segment
         */
        populateTransferLimitSegment: function(limits, selectedLimits) {
            var scopeObj = this;
            var selectedLimitsMap = {};
            if (selectedLimits) {
                selectedLimits.forEach(function(obj) {
                    selectedLimitsMap[obj.Id || obj.Service_id] = obj;
                })
            }
            return limits.map(function(limit, index) {
                return {
                    "lblTransferType": limit.Name,
                    "maxTransactionLimit": limit.MaxTransferLimit,
                    "minTransferLimit": limit.MinTransferLimit,
                    "lblMaxTransactionLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxTransactionLimit"),
                    "lblMaxDailyLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxDailyLimit"),
					"lblDailyLimitCurrSymbol": applicationManager.getConfigurationManager().configurations.getItem("CURRENCYCODE"),
					"lblCurrSymbol": applicationManager.getConfigurationManager().configurations.getItem("CURRENCYCODE"),
                    "maxDailyLimit": limit.MaxDailyLimit,
                    "serviceId": limit.Service_id,
                    "tbxMaxTransactionValue": {
                        text: selectedLimitsMap[limit.Service_id] ? selectedLimitsMap[limit.Service_id].MaxTransactionLimit : "",
                        onKeyUp: scopeObj.validateTransactionLimitsUI.bind(scopeObj)
                    },
					"flxMaxTransactionLimit": {
						skin: ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20
					},
					"flxMaxDailyLimit": {
						skin: ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20
					},
                    "tbxMaxDailyValue": {
                        text: selectedLimitsMap[limit.Service_id] ? selectedLimitsMap[limit.Service_id].MaxDailyLimit : "",
                        onKeyUp: scopeObj.validateTransactionLimitsUI.bind(scopeObj)
                    }
                }
            });
        },

        /**
         * Method to handle validation of Transaction Limits
         */
        validateTransactionLimitsUI: function() {
            var scopeObj = this;
            scopeObj.view.flxErrorMessageTransferPermissions.setVisibility(false);
			this.adjustScreen();
            var limitsSegData = scopeObj.view.segBBTransferPermissions.data;
			FormControllerUtility.disableButton(this.view.btnProceedTransferPermissions);
            for (var i in limitsSegData) {
                if (limitsSegData[i].tbxMaxTransactionValue.text === "" || limitsSegData[i].tbxMaxDailyValue.text === "") {
                    FormControllerUtility.disableButton(this.view.btnProceedTransferPermissions);
                    return;
                }
            }
            FormControllerUtility.enableButton(this.view.btnProceedTransferPermissions);
        },

        /**
         * Method to handle back of Transaction Limits
         */
        onBackTransferPermission : function () {
            var scopeObj = this;
            scopeObj.view.flxErrorMessageTransferPermissions.setVisibility(false);
            var limits = [];
            var limitsSegData = scopeObj.view.segBBTransferPermissions.data;
            for (var i in limitsSegData) {
                var limit = {
                    "Service_id": limitsSegData[i].serviceId,
                    "MaxTransactionLimit": limitsSegData[i].tbxMaxTransactionValue.text,
                    "MaxDailyLimit": limitsSegData[i].tbxMaxDailyValue.text
                };
                limits.push(limit);
            }
            scopeObj.loadBusinessBankingModule().presentationController.addTransactionLimitsOnBack(limits);
        },

        /**
         * Method to handle validation of Transaction Limits
         * @returns {object} limits
         */
        validateTransactionLimitsWithServiceLimits: function() {
            var scopeObj = this;
            scopeObj.view.flxErrorMessageTransferPermissions.setVisibility(false);
            var limits = [];
			var flag = 0;
            var limitsSegData = scopeObj.view.segBBTransferPermissions.data;
            for (var i in limitsSegData) {
                var tbxMaxTranValue =Number.parseInt(CommonUtilities.deFormatAmount(limitsSegData[i].tbxMaxTransactionValue.text));
                var tbxMaxDailyValue =Number.parseInt(CommonUtilities.deFormatAmount(limitsSegData[i].tbxMaxDailyValue.text));
                var maxTranValue = Number.parseInt(CommonUtilities.deFormatAmount(limitsSegData[i].maxTransactionLimit));
                var maxDailyLimit = Number.parseInt(CommonUtilities.deFormatAmount(limitsSegData[i].maxDailyLimit));
                var minTransferLimit = Number.parseInt(CommonUtilities.deFormatAmount(limitsSegData[i].minTransferLimit));

                if ((tbxMaxTranValue <= maxTranValue && tbxMaxTranValue >= minTransferLimit) && (tbxMaxDailyValue <= maxDailyLimit && tbxMaxDailyValue >= minTransferLimit)) {
                    var limit = {
                        "Service_id": limitsSegData[i].serviceId,
                        "Name": limitsSegData[i].lblTransferType,
                        "MaxTransactionLimit": CommonUtilities.deFormatAmount(limitsSegData[i].tbxMaxTransactionValue.text),
                        "MaxDailyLimit": CommonUtilities.deFormatAmount(limitsSegData[i].tbxMaxDailyValue.text)
                    };
					limitsSegData[i].flxMaxTransactionLimit.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
					limitsSegData[i].flxMaxDailyLimit.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20
                    limits.push(limit);
                } else {                    
                    FormControllerUtility.disableButton(this.view.btnProceedTransferPermissions);
                    if (tbxMaxTranValue > maxTranValue || tbxMaxTranValue < minTransferLimit) {
						limitsSegData[i].flxMaxTransactionLimit.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
					} else {
						limitsSegData[i].flxMaxTransactionLimit.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
					}
                    if (tbxMaxDailyValue > maxDailyLimit || tbxMaxDailyValue < minTransferLimit) {
						limitsSegData[i].flxMaxDailyLimit.skin =ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
					} else {
						limitsSegData[i].flxMaxDailyLimit.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
					}					
					flag = 1;
                }
            }
			if(flag === 1){
				this.view.lblErrorMessageTransferPermissions.text = kony.i18n.getLocalizedString("i18n.konybb.createUser.ErrorMessageTransactionLimits");
                scopeObj.view.flxErrorMessageTransferPermissions.setVisibility(true);
				scopeObj.view.flxErrorMessageTransferPermissions.setFocus();
				limits = [];
			}
			scopeObj.view.segBBTransferPermissions.setData(limitsSegData);
            scopeObj.adjustScreen();			
            return limits;
        },

        /**
         * Method to handle validation of Transaction Limits
         */
        onUserTransactionAccessNextBtnClick: function () {
           var scopeObj = this;
           var limits = scopeObj.validateTransactionLimitsWithServiceLimits();
           if(limits.length){
			FormControllerUtility.showProgressBar(this.view);
            scopeObj.loadBusinessBankingModule().presentationController.confirmTransactionLimits(limits);
           }
        }

    }

});