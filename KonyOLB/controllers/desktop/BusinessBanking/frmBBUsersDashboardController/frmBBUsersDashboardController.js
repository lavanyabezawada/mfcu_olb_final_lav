define(['CommonUtilities','FormControllerUtility','ViewConstants','OLBConstants'], function (CommonUtilities, FormControllerUtility, ViewConstants,OLBConstants) {
    return {
        /**
         * Update Ui method starts here.
         * @param {String} - View Model.
         */
        updateFormUI: function (viewModel) {
            if (viewModel.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewModel.serverError) {
                if (viewModel.serverError === true){
                viewModel.show = true;
                this.showServerError(viewModel);
             } else {
                viewModel.show = false;
                this.showServerError(viewModel);
                }
            }  
            if (viewModel.serverDown) {
                CommonUtilities.showServerDownScreen(); 
            }
            if (viewModel.allUsers) {
                this.updateHamburgerMenu();
                this.setUsersDataToDashboard(viewModel.allUsers);
            }
            if (viewModel.activationLinkSuccess) {
               //code for success activation
            }
            if (viewModel.statusSuccess) {
                this.updateUserStatusSuccess(viewModel.statusSuccess);
            }
            if (viewModel.manageUser) {
                this.showManageUser(viewModel.manageUser);
            }
            if (viewModel.createUserSuccess) {
                this.showAcknowledgement(viewModel.userObject,viewModel.referenceNumber);
            }
            if(viewModel.isPrintCancelled){
                this.showAcknowledgementScreenOnPrintCancel();
            }
        },
              
          /** 
         * Show No Search results - UI logic 
         * @param {String} errorMsg to be displayed
         **/
         showNoRecords: function(errorMsg){
             this.resetUI();
             this.view.TabPane.TabBodyNew.flxNoTransactions.setVisibility(true);
             this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
             this.view.flxContent.setVisibility(true);
             this.view.TabPane.TabBodyNew.rtxNoPaymentMessage.text = errorMsg;
        },
          /**
         * Method to load data after successful update of user's status
         * @param {object} userStatus - response
         */  
        updateUserStatusSuccess: function(userStatus){
            if(this.view.flxContentDetails.isVisible){
                //manage user screen
                this.view.lblStatusKeyValue.text = this.statusConfig(userStatus,false);
                this.view.lblUserStatusIndicator.skin = this.setStatusImageConfig(userStatus,true);
                this.showActivateButton(userStatus,this.view.lblUserNameValue.text);        
            }
            else {
                //BB users dashboard
                var scopeObj = this;
                var segData = this.view.TabPane.TabBodyNew.segTemplates.data;
                var index = this.view.TabPane.TabBodyNew.segTemplates.selectedIndex[1];
                var tempData = segData[index];
                tempData.lblUserStatus = {
                    "text":  scopeObj.statusConfig(userStatus,false),
                };
                tempData.btnChangeStatus = {
                    onClick: scopeObj.onClickOfChangeStatus.bind(this, userStatus, tempData.lblUsernameValue.text),
                    "text": scopeObj.setStatusImageConfig(userStatus, false)
                };
                tempData.lblUserStatusIndicator = {
                    isVisible: true,
                    skin: scopeObj.setStatusImageConfig(userStatus, true)
                };
                this.view.TabPane.TabBodyNew.segTemplates.setDataAt(tempData, index);
            } 
            this.adjustScreen();           
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to load and returns the Business User Module Object
         * @returns {object} Method to load and returns the Business User Module Object
         */
        loadBusinessBankingModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
        },
         /**
         * hide all ui flexes in user management form
         */
        resetUI: function () {
            this.view.flxContent.setVisibility(false);
            this.view.flxContentDetails.setVisibility(false);
            this.view.flxPopupConfirmation.setVisibility(false);
            this.view.TabPane.TabBodyNew.flxNoTransactions.setVisibility(false);
            this.view.TabPane.TabBodyNew.segTemplates.setVisibility(false);
            this.view.TabPane.TabSearchBarNew.flxNoSearchResult.setVisibility(false);
            this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(false);
            this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);
            this.showServerError({ show: false });
            },
          /**
         * Method to navigate to create User page
         */
        navigateToCreateUser: function() {
            this.loadBusinessBankingModule().presentationController.showCreateUser();
        },

        /**
         * Method will invoke on form init
         */
        onInit: function () {
            var self = this;
            this.view.onDeviceBack = function(){};
            this.view.TabPane.TabsHeaderNew.btnTab1.text = kony.i18n.getLocalizedString("i18n.userManagement.allUsers");
            this.view.dbRightContainerNew.btnAction1.onClick = this.navigateToCreateUser;
            this.view.TabPane.TabSearchBarNew.tbxSearch.onKeyUp = this.checkSearchForm.bind(this);
            this.view.imgCloseDowntimeWarning.onTouchEnd = this.showServerError.bind(this,{ show: false });
            this.usersSortMap = [ {
                name: 'FirstName',
                imageFlx:this.view.TabPane.TabBodyNew.imgName,
                clickContainer: this.view.TabPane.TabBodyNew.flxName
                 },
                {
                name: 'role_name',
                imageFlx: this.view.TabPane.TabBodyNew.imgRole,
                clickContainer: this.view.TabPane.TabBodyNew.flxRole
            },
            {
                name: 'UserName',
                imageFlx: this.view.TabPane.TabBodyNew.imgUsername,
                clickContainer: this.view.TabPane.TabBodyNew.flxUsername         
            },
            {
                name: 'Status_id',
                imageFlx: this.view.TabPane.TabBodyNew.imgStatus,
                clickContainer: this.view.TabPane.TabBodyNew.flxStatus
            }];
            this.view.TabPane.TabBodyNew.segTemplates.widgetDataMap = {
                "imgBottonSeparator":"imgBottonSeparator",
                "flxBottomSeperator":"flxBottomSeperator",
                "flxActions":"flxActions",  
                "flxManageUsers":"flxManageUsers",
                "lblUserStatusIndicator":"lblUserStatusIndicator",
                "btnManageUsers":"btnManageUsers",
                "flxDetails":"flxDetails",
                "lblLastSignedInTitle":"lblLastSignedInTitle",
                "lblEmailTitle":"lblEmailTitle",
                "lblLastSignedIn":"lblLastSignedIn",
                "lblEmailValue":"lblEmailValue",
                "lblPhoneNumberTitle":"lblPhoneNumberTitle",
                "lblPhoneNumber":"lblPhoneNumber",
                "flxUserDetails":"flxUserDetails",
                "flxUserCommonRowDetails":"flxUserCommonRowDetails",
                "flxUserCommonRowHeader":"flxUserCommonRowHeader",
                "flxDropDown":"flxDropDown",
                "lblDropdown":"lblDropdown",
                "flxUserHeader":"flxUserHeader",
                "lblName":"lblName",
                "lblRole":"lblRole",
                "lblUsernameValue":"lblUsernameValue",
                "flxUserStatusIcon":"flxUserStatusIcon",
                "lblUserStatus":"lblUserStatus",
                "flxUserStatusChange":"flxUserStatusChange",
                "btnChangeStatus":"btnChangeStatus",
                "flxSeparatorForHeader":"flxSeparatorForHeader",
                "imgSeparatorHeader":"imgSeparatorHeader",
            };
            //Manager user code
            this.view.btnEdit.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.showCreateUser({"edit": true});
              }
              else{}
            };
            this.view.btnEditUserRoles.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.fetchRoles();
              }
              else{}
            };
            this.view.btnEditUserPermissions.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.showAllAccounts();
              }
              else{}
            };
            this.view.btnViewAllUsers.onClick = function(){
                //code for going back to all users
                self.loadBusinessBankingModule().presentationController.navigateToUsers();
            };
            this.view.flxInfoIcon.onClick = function() {
                if (self.view.InfoIconPopup.isVisible === true) {
                    self.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
                    self.view.InfoIconPopup.setVisibility(true);
                    self.view.InfoIconPopup.left = self.view.flxInfoIcon.frame.x + self.view.flxContentContainer.frame.x - 40 + "dp";
                    if(self.view.flxAcknowledgement.isVisible === true){
                    self.view.InfoIconPopup.top = self.view.flxInfoIcon.frame.y + self.view.flxAcknowledgement.frame.height + 755 + "dp";    
                    }
                    else
                    self.view.InfoIconPopup.top = self.view.flxInfoIcon.frame.y + 755 + "dp";
                    
                }
                self.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.accountAcessDetailstext");
            };
            this.view.flxInfoTransactionAccess.onClick = function() {
                if (self.view.InfoIconPopup.isVisible === true) {
                    self.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
                    self.view.InfoIconPopup.setVisibility(true);
                    if(self.view.flxAcknowledgement.isVisible === true){  
                        if (self.view.flxAccountsAndPermissons.isVisible === true) {
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAccountsAndPermissons.frame.height + self.view.flxAcknowledgement.frame.height  + 816 + "dp";
                        } else 
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAcknowledgement.frame.height + 816 + "dp";
                    }
                    else{
                        if (self.view.flxAccountsAndPermissons.isVisible === true) {
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAccountsAndPermissons.frame.height + 816 + "dp";
                        } else 
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + 816 + "dp";
                    }    
                    self.view.InfoIconPopup.left = self.view.flxInfoTransactionAccess.frame.x + self.view.flxContentContainer.frame.x - 40 + "dp";
                }
                self.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.transactionAccessDetailstext");
            };
            this.view.InfoIconPopup.flxCross.onClick = function () {
                self.view.InfoIconPopup.isVisible = false;
            };

        },
        /**
       *  Pre show Method 
       */
        onPreShow: function () {
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.customheader.forceCloseHamburger();
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        /** 
         * onPostshow event Function 
         **/
	    onPostShow: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function(){
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
        },
        /**
         * Method to load status image
         * @param {String} status -  contains Status
         * @param {Boolean} needImage -  contains Boolean if need image or not
         * @returns {String} - may contain Skin for label or Status
         */
        setStatusImageConfig: function(status,needImage){
            switch(status.toLowerCase().trim()){
                case  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.ACTIVE_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.deactivate");
                case kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.SUSPENDED_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.activate");
                case kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.SUSPENDED_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.activate");
                default: 
                    return needImage === true ? ViewConstants.SKINS.NEW_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
             }
        },
        /**
            *  Method on click of btnYes of deactivate/activate popup.
            * @param {Object} params -  contains Status and Username
       */
        updateUserStatus : function(params){
            FormControllerUtility.showProgressBar(this.view);
            this.view.flxPopupConfirmation.setVisibility(false);
            this.loadBusinessBankingModule().presentationController.setUserStatus(params); 
        },
        /**
            *  Method to provide status to updateUserStatus method basing on present status of the user
            * @param {String} status -  status to which the user has to change
            * @param {Boolean} needConversion -  Bpolean that specifies conversion needed or not
            * @returns {String} - Status
       */
      statusConfig : function(status, needConversion){
        switch(status.toLowerCase().trim()){
            case  "Active".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.userManagement.suspended").toUpperCase() : kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE");
            case "Suspend".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.suspended");
            case "Suspended".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.suspended");
            case "New".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.userManagement.new").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.new");
            default:
                return status;
        }
    },
       /**
            *  Method on click of activate/deactivate button.
            * @param {String} status - Status
            * @param {String} userName - username
       */
        onClickOfChangeStatus: function(status,userName){
            FormControllerUtility.showProgressBar(this.view);
            var scopeObj = this;
           this.view.PopupHeaderUM.formActionsNew.btnCancel.onClick = function(){
                scopeObj.view.flxPopupConfirmation.setVisibility(false);
                FormControllerUtility.hideProgressBar(scopeObj.view);
            };
            this.view.PopupHeaderUM.imgClose.onTouchEnd = function(){
                scopeObj.view.flxPopupConfirmation.setVisibility(false);
                FormControllerUtility.hideProgressBar(scopeObj.view);
            };
            var statusName = scopeObj.statusConfig(status.toLowerCase().trim(), true);
            var params = {
                "Status": statusName,
                "UserName": userName
            };
            FormControllerUtility.enableButton(scopeObj.view.PopupHeaderUM.formActionsNew.btnNext);
            this.view.PopupHeaderUM.formActionsNew.btnNext.onClick = this.updateUserStatus.bind(this,params);
            if(status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim()){
                this.view.flxPopupConfirmation.setVisibility(true);
                this.view.flxPopupConfirmation.setFocus(true);
                this.view.flxPopupConfirmation.height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + "dp";
                this.view.PopupHeaderUM.lblHeader.text = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
                this.view.PopupHeaderUM.lblPopupMsg.text = kony.i18n.getLocalizedString("i18n.konybb.manageUser.SuspendUserConfirm");
            }
            else if(status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim()){
                this.view.flxPopupConfirmation.setVisibility(true);
                this.view.flxPopupConfirmation.setFocus(true);
                this.view.flxPopupConfirmation.height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + "dp";
                this.view.PopupHeaderUM.lblHeader.text = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
                this.view.PopupHeaderUM.lblPopupMsg.text = kony.i18n.getLocalizedString("i18n.konybb.manageUser.ActivateUserConfirm");
            }
            else{
                this.view.flxPopupConfirmation.setVisibility(false);
                this.resendActivationLink(userName);
            }
        },
        /**
         * on click of dropdown in segment row
         *  @param {object} userObj - user object
         */
        onUserDropdownClick: function(userObj) {
            var scopeObj = this;
          	var break_point = kony.application.getCurrentBreakpoint();
            var segData = this.view.TabPane.TabBodyNew.segTemplates.data;
            var index = this.view.TabPane.TabBodyNew.segTemplates.selectedIndex;
            var sectionIndex = index[0];
            var rowIndex = index[1];
            var collapseAll = function(segments) {
                segments.forEach(function(segment, i) {
                    if (segment.template === "flxBBUserBasicDetailsSelected" || segment.template === "flxBBUserBasicDetailsMobileSelected") {
                        if(break_point === 640){
                            segment.template = "flxBBUserBasicDetailsMobile"; 
                        }
                        else
                        	segment.template = "flxBBUserBasicDetails";
                        segment.lblDropdown ={
                            text: "O",
                            onClick: scopeObj.onUserDropdownClick.bind(this, userObj)
                        };
                        scopeObj.view.TabPane.TabBodyNew.segTemplates.setDataAt(segment, i, sectionIndex);
                    }
                });
            };
            var prevData = segData[rowIndex].template;
            collapseAll(segData);
            var tempData = scopeObj.view.TabPane.TabBodyNew.segTemplates.data[rowIndex];
            if ((tempData.template === "flxBBUserBasicDetails" && prevData === "flxBBUserBasicDetailsSelected") || (tempData.template === "flxBBUserBasicDetailsMobile" && prevData === "flxBBUserBasicDetailsMobileSelected")) {
                if(break_point === 640){
                    tempData.template = "flxBBUserBasicDetailsMobile";
                }
                else 
                    tempData.template = "flxBBUserBasicDetails";
                tempData.lblDropdown = {
                    text: "O",
                    onClick: scopeObj.onUserDropdownClick.bind(this, userObj)
                };
            } else {
                tempData.lblDropdown = {
                    text: "P",
                    onClick: scopeObj.onUserDropdownClick.bind(this, userObj)
                };
                if(break_point === 640){
                    tempData.template = "flxBBUserBasicDetailsMobileSelected";
                }
                else 
                    tempData.template = "flxBBUserBasicDetailsSelected";
            } 
            this.view.TabPane.TabBodyNew.segTemplates.setDataAt(tempData, rowIndex);
            this.adjustScreen();
        },
        /**
           *  Method to show error flex.
           * @param {String} response - Response containing Error message and boolean variable to validate whether error flex should be displayed.
           */
        showServerError: function(response) {
            this.view.flxDowntimeWarning.setVisibility(response.show);
            this.view.lblDowntimeWarning.text = response.errorMessage || kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
            this.view.flxMainWrapper.setVisibility(response.show);
            this.adjustScreen();
            this.view.flxDowntimeWarning.setFocus(response.show);
        },
        /**
        * AdjustScreen - Method that sets the height of footer properly.
        */
        adjustScreen: function () {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.frame.height + this.view.flxMain.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }                 
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }
            this.view.forceLayout();
        },
        /**
         * Method that updates Hamburger Menu.
         */
        updateHamburgerMenu: function () {
            this.view.customheader.customhamburger.activateMenu("User Management","All Users");
        },

        /**
         * Method to show User Details
         * @param {String} username - contains the username
         */
        showUserDetails : function(username){
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.fetchUserDetails(username);
        },

        /**
         * Method to Resend Activation link
         * @param {String} username - contains the username
         */
        resendActivationLink : function(username){
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.resendActivationLink(username);
        },

        /**
         * Method to show manage user flex.
         * @param {Object} userDetails - contains the user details
         */
        showManageUser: function (userDetails) {
            this.populateUserDetails(userDetails);
            this.view.flxAcknowledgement.isVisible = false;
            if (applicationManager.getUserPreferencesManager().getCurrentUserName() !== userDetails.Username){
                this.showActivateButton(userDetails.Status, userDetails.Username);
                this.view.btnEdit.isVisible = true;
                this.view.btnEditUserRoles.isVisible = true;
                this.view.btnEditUserPermissions.isVisible = true;
            }                
            else{
                this.view.btnAddAnother.isVisible = false;     
                this.view.btnEdit.isVisible = false;
                this.view.btnEditUserRoles.isVisible = false;
                this.view.btnEditUserPermissions.isVisible = false;       
            }                
            this.view.btnViewAllUsers.text = kony.i18n.getLocalizedString("i18n.userManagement.BackToAllUsers");
            this.view.btnViewAllUsers.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.BackToAllUsers");
            this.view.lblStatusKey.isVisible = true;
            this.view.flxStatusValue.isVisible = true;
            this.view.lblStatusKeyValue.text = this.statusConfig(userDetails.Status,false);
            this.view.lblUserStatusIndicator.skin = this.setStatusImageConfig(userDetails.Status,true);
            this.adjustScreen();
        },

        /**
         * Method to manipulate text and click functionality of button btnAddAnother
         * @param {String} status - contains the status
         * @param {String} username - contains the username
         */
        showActivateButton : function(status,username){
            this.view.btnAddAnother.isVisible = true;
            var scopeObj = this;
            if (status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim()) {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
                this.view.btnAddAnother.onClick = this.updateUserStatus.bind(this,{ "UserName" :username, "Status": scopeObj.statusConfig(status.toLowerCase().trim(), true)});
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
            }
            else if (status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim()) {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
                this.view.btnAddAnother.onClick = this.updateUserStatus.bind(this,{ "UserName" :username, "Status": scopeObj.statusConfig(status.toLowerCase().trim(), true)});
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
            }
            else {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
                this.view.btnAddAnother.onClick = this.resendActivationLink.bind(this,username);
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
            }
        },
        /**
         * Method to show manage user flex.
         * @param {Object} userDetails - contains the user details
         * @param {String} referenceNumber - contains the reference Number
         */
        showAcknowledgement: function(userDetails,referenceNumber){
            this.populateUserDetails(userDetails);
            if (CommonUtilities.isPrintEnabled()) {
                this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(true);
                this.view.flxAcknowledgementNew1.flxImgPrint.onClick = this.printAcknowledgement.bind(this);
              } else {
                this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
              }
            this.view.flxAcknowledgement.isVisible = true;
            var ackMsg = "";
            if(referenceNumber !== undefined && referenceNumber !== null && referenceNumber !== ""){
                ackMsg = kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserAddedSuccess")+"\n"+kony.i18n.getLocalizedString("i18n.konybb.common.ReferenceNumber") +" "+ referenceNumber;
            }
            else{
                ackMsg = kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserUpdatedSuccess")+"\n"+kony.i18n.getLocalizedString("i18n.konybb.common.ReferenceNumber") +" "+ userDetails.id;
            }
            this.view.flxAcknowledgementNew1.rTextSuccess.text = ackMsg;
            this.view.btnEdit.isVisible = false;
            this.view.btnEditUserRoles.isVisible = false;
            this.view.btnEditUserPermissions.isVisible = false;
            this.view.btnAddAnother.isVisible = true;
            this.view.btnAddAnother.text =  kony.i18n.getLocalizedString("i18n.userManagement.AddAnotherUser");
            var self = this;
            this.view.btnAddAnother.onClick = function(){
                self.loadBusinessBankingModule().presentationController.showCreateUser();
            };
            this.view.btnViewAllUsers.text = kony.i18n.getLocalizedString("i18n.userManagement.ViewAllUsers");
            this.view.btnViewAllUsers.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.ViewAllUsers");
            this.view.lblStatusKey.isVisible = false;
            this.view.flxStatusValue.isVisible = false;
            this.adjustScreen();
        },
        /**
         * Method to populate User details
         * @param {Object} userDetails - contains the user details
         */
        populateUserDetails : function(userDetails){
            this.view.flxContent.isVisible = false;
            this.view.flxContentDetails.isVisible = true;
            this.view.lblFullNameValue.text = CommonUtilities.getFullName( userDetails.FirstName, userDetails.MiddleName, userDetails.LastName);
            this.view.lblEmailIdValue.text = userDetails.Email;
            this.view.lblDateOfBirthValue.text = CommonUtilities.getFrontendDateString(userDetails.DateOfBirth);
            this.view.lblSSNValue.text = userDetails.Ssn;
            this.view.lblDriverLicenseValue.text = userDetails.DrivingLicenseNumber || "--";
            this.view.lblPhoneNumberValue.text = userDetails.Phone;
            this.view.lblUserNameValue.text = userDetails.Username || userDetails.UserName;
            this.view.lblSelectedRole.text = userDetails.Group_Name;
            this.view.RichTextSelectedRole.text = userDetails.Group_Description;
            var self = this;
            var hasAccounts = false;
            var hasServices = false;
            var segData;

            //Resetting User Persmissions flexes
            this.view.flxNoAccountAccessUsers.isVisible = false;    
            this.view.flxAccountsAndPermissons.isVisible = false;
            this.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_DOWN;
            this.view.flxTransferPermissions.isVisible = false;
            this.view.flxNoPermissionsUsers.isVisible = false;
            this.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_DOWN;

            if (userDetails.accounts[0].hasOwnProperty("AccountName") || userDetails.accounts[0].hasOwnProperty("Account_Name")) {
                segData = userDetails.accounts.map(function (dataItem) {
                    var data = {
                        "lblGeneralAccountName": (dataItem.AccountName || dataItem.Account_Name) + "-X"+ CommonUtilities.getLastFourDigit(dataItem.Account_id || dataItem.id) //need to update object pro
                    }
                    return data;
                });
                this.view.segAccountPermissions.setData(segData);
                hasAccounts = true;
            }
            else{
                this.view.segAccountPermissions.setData([{}]);
                hasAccounts = false;
            }                
                
            this.view.flxDropDownAccount.onClick = function () {
                if (self.view.flxAccountsAndPermissons.isVisible) {
                    self.view.flxNoAccountAccessUsers.isVisible = false;    
                    self.view.flxAccountsAndPermissons.isVisible = false;
                    self.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_DOWN;
                }

                else {
                    if(hasAccounts)
                        self.view.flxNoAccountAccessUsers.isVisible = false;
                    else
                        self.view.flxNoAccountAccessUsers.isVisible = true;
                    self.view.flxAccountsAndPermissons.isVisible = true;
                    self.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_UP;
                }
                self.adjustScreen();
            }
            if (userDetails.services === null){
                hasServices = false;
                this.view.segTransferLimits.setData([{}]);
            }
            else if(userDetails.services.length === 0){
                hasServices = false;
                this.view.segTransferLimits.setData([{}]);
            }
            else{
                hasServices = true;
                var segData = userDetails.services.map(function (dataItem) {
                    var data = {
                        "lblTransferType": dataItem.Name,
                        "lblMaxTransactionLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxTransactionLimit"),
                        "lblMaxDailyLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxDailyLimit"),
                        "lblMaxTransactionLimitValue": CommonUtilities.formatCurrencyWithCommas(dataItem.MaxTransactionLimit),
                        "lblMaxDailyLimitValue": CommonUtilities.formatCurrencyWithCommas(dataItem.MaxDailyLimit)
                    }
                    return data;
                });
                this.view.segTransferLimits.setData(segData);
            }
            this.view.flxDropDownTransaction.onClick = function () {
                if (self.view.flxTransferPermissions.isVisible) {
                    self.view.flxTransferPermissions.isVisible = false;
                    self.view.flxNoPermissionsUsers.isVisible = false;
                    self.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_DOWN;
                }
                else {
                    if (hasServices)
                        self.view.flxNoPermissionsUsers.isVisible = false;
                    else
                        self.view.flxNoPermissionsUsers.isVisible = true;
                    self.view.flxTransferPermissions.isVisible = true;
                    self.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_UP;
                };
                self.adjustScreen();
            }
            this.view.customheader.imgKony.setFocus(true);
        },
        
        /**
         * Method to switch to users dashboard UI
         */
        showUsersUI: function(){
            this.resetUI();
            this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
            this.view.TabPane.TabBodyNew.segTemplates.setVisibility(true);
          	if(kony.application.getCurrentBreakpoint() === 640){
                this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);   
            }
            else
            	this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(true);
            this.view.flxContent.setVisibility(true);
            this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.common.UserManagement");
        },
        /**
         * Method to populate the Segment with user-list
         *  @param {object} context - which consists of list of users, context
         *  Searching|Sorting
         */
        setUsersDataToDashboard: function (context) {
            var scopeObj = this;
            if (context.users.length === 0) {
                var errorMsg =  kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.noRecords"); 
                if(context.searchString !== ""){
                   errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.emptySearchMsg");
                }
                this.showNoRecords(errorMsg);
            }
            else if(context.users.length > 0){
              this.showUsersUI();       
              var segData = context.users.map(function (dataItem) {
              var data = { 
                "flxUserCommonRowDetails":"flxUserCommonRowDetails",
                "btnManageUsers":{
                    onClick: scopeObj.showUserDetails.bind(this, dataItem.UserName),
                    text: kony.i18n.getLocalizedString("i18n.konybb.manageUser.manageUsers"),
                 },
                "flxMain":"flxMain",
                "flxDropDown": "flxDropDown",
                "lblDropdown":{
                    text: "O",
                    onClick : scopeObj.onUserDropdownClick.bind(this,dataItem)
                },
                "flxDetailsHighlighter":"flxDetailsHighlighter",
                "flxDetails":"flxDetails",
                "imgSample":"imgSample",
                "flxSeparatorForHeader":"flxSeparatorForHeader",
                "imgSeparatorHeader":"imgSeparatorHeader",
                "lblPhoneNumberTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.PhoneNo"),
                },
                "lblLastSignedInTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.LastSignedIn"),
                },
                "lblEmailTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.EmailID"),
                },
                "flxActions":"flxActions",  
                "flxManageUsers":"flxManageUsers",    
                "lblEmailValue":  {
                    "text":dataItem.Email ? dataItem.Email : "", 
                },
                "lblLastSignedIn" :  {
                    "text": dataItem.Lastlogintime ? CommonUtilities.getFrontendDateString(dataItem.Lastlogintime) : "",
                },
                "lblPhoneNumber":  {
                    "text":dataItem.Phone ? dataItem.Phone : "",
                },
                "lblName": {
                    "text":dataItem.FirstName,
                },
                //"template":"flxBBUserBasicDetails",
                "lblRole": {
                    "text":dataItem.role_name,
                },
                "lblUsernameValue": {
                    "text":dataItem.UserName, 
                },
                "flxUserStatusIcon":"flxUserStatusIcon",
                "lblUserStatus": {
                    "text":scopeObj.statusConfig(dataItem.status,false),
                },
                "btnChangeStatus": {
                    onClick: scopeObj.onClickOfChangeStatus.bind(this,dataItem.status,dataItem.UserName),
                    "text": scopeObj.setStatusImageConfig(dataItem.status,false),
                    "isVisible": dataItem.UserName === applicationManager.getUserPreferencesManager().getCurrentUserName() ? false : true
                },
                "lblUserStatusIndicator":{
                    isVisible : true,
                    skin :scopeObj.setStatusImageConfig(dataItem.status,true)
                },
                };
                return data;
            });
            if(kony.application.getCurrentBreakpoint() === 640){
                this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetailsMobile";
		    }
		    else
               	this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetails";  
            this.view.TabPane.TabBodyNew.segTemplates.setData(segData);
            FormControllerUtility.setSortingHandlers(this.usersSortMap,  this.sortUsersHandler, this);
            FormControllerUtility.updateSortFlex(this.usersSortMap, context.config);
            this.configureSearch(context.searchString, this.searchUsersHandler);
            }
            this.adjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
         },
        /**
         * Method to configure  sorting handler  for the list of users
         *  @param {JSON} obj - sorting parameters
         */
        searchUsersHandler: function(obj) {
            this.loadBusinessBankingModule().presentationController.navigateToUsers(obj);
        },
  
        /**
         * Method to configure  sorting handler  for the list of users
         * @param {EventObject} event - event
         *  @param {JSON} obj - sorting parameters
         */
        sortUsersHandler: function(event,obj){
            this.loadBusinessBankingModule().presentationController.navigateToUsers(obj);
        },
        /**
         * Method to configure search logic  for the list of users
         *  @param {String} searchText - search String
         *  @param {Method} onSearch - callback
         */
    configureSearch: function (searchText, onSearch) {
        this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
        this.view.TabPane.TabSearchBarNew.tbxSearch.text = searchText || "";
        this.checkSearchForm();
        this.view.TabPane.TabSearchBarNew.tbxSearch.onDone = function () {
            onSearch({
                searchString: this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim()
            });
            this.checkSearchForm();
        }.bind(this);
        this.view.TabPane.TabSearchBarNew.flxSearchimg.onClick =  function () {
            onSearch({
                searchString: this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim()
            });
            this.checkSearchForm();
        }.bind(this);
        this.view.TabPane.TabSearchBarNew.flxClose.onClick = function () {
            onSearch({
                searchString: ""
            });
            this.view.TabPane.TabSearchBarNew.tbxSearch.text = "";
            this.checkSearchForm();
        }.bind(this)
    },
     /**
         * Method to check whether the user typed anything in the search bar and making the UI compatible according to the actions of the user
         */
    checkSearchForm: function () {
        if (this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim() === "") {
            this.view.TabPane.TabSearchBarNew.flxClose.setVisibility(false);
        }
        else {
            this.view.TabPane.TabSearchBarNew.flxClose.setVisibility(true);                    
        }
        this.view.TabPane.TabSearchBarNew.forceLayout();
    },
    orientationHandler:null,
    onBreakpointChange: function(width){
        kony.print('on breakpoint change');
        if (this.orientationHandler === null) {
         this.orientationHandler = new OrientationHandler();
        }
        this.orientationHandler.onOrientationChange(this.onBreakpointChange);
        
            //var responsiveFonts = new ResponsiveFonts();
            if(width ===640){
                    //this.view.TabPaneNew.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxempty";
                    this.view.TabPaneNew.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetailsMobile";
                    //this.view.TabPaneNew.TabBodyNew.setExpandableRowHeight(250);
                    this.view.customheader.lblHeaderMobile.isVisible = true;
                    //responsiveFonts.setMobileFonts();
          }else{
                  //this.view.TabPaneNew.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxBBUserDashboardCommonHeader";
                  this.view.TabPaneNew.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetails";
                  this.view.customheader.lblHeaderMobile.isVisible = false;
                  this.view.customheader.lblHeaderMobile.text = "";
                   //responsiveFonts.setDesktopFonts();
                }
          this.adjustScreen();
       },
       
       printAcknowledgement : function(){
         var tableList  = [
                { 
                    tableHeader : kony.i18n.getLocalizedString("i18n.userManagement.userInfo"),
                    tableRows : this.getUserData()
                },
                {
                    tableHeader  : kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    tableRows : this.getUserPermissions()
                }
            ];
            if( this.view.segTransferLimits.data.length > 1){
            tableList.push(   {
                tableHeader : kony.i18n.getLocalizedString("i18n.userManagement.transPermission"),
                tableRows : this.getTransactionsAccess()
            });
        }
            var viewModel = {
                moduleHeader :  this.view.lblContentHeader.text,
                tableList : tableList
           };
           this.loadBusinessBankingModule().presentationController.showPrintPage({printKeyValueGroupModel:viewModel});
    },

       getUserData : function(){
        var self = this;   
        var userData  = [];
        userData.push({
            key : kony.i18n.getLocalizedString("i18n.common.status"),
            value : self.view.flxAcknowledgementNew1.rTextSuccess.text, 
        });
        userData.push({
            key : self.view.lblFullNameKey.text,
            value : self.view.lblFullNameValue.text
        })
        userData.push({
            key : self.view.lblDriverLicenseKey.text,
            value : self.view.lblDriverLicenseValue.text
       })
       userData.push({
           key  : self.view.lblEmailIdKey.text,
           value : self.view.lblEmailIdValue.text
       })
       userData.push({
           key : self.view.lblPhoneNumberKey.text,
           value : self.view.lblPhoneNumberValue.text,
       })
       userData.push({
           key : self.view.lblDateOfBirthKey.text,
           value : self.view.lblDateOfBirthValue.text
       })
       userData.push({
           key  : self.view.lblUserNameKey.text,
           value : self.view.lblUserNameValue.text
       })
       userData.push({
           key : self.view.lblSSNKey.text,
           value : self.view.lblSSNValue.text,
       })
       userData.push({
           key : kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserRoles"),
           value : self.view.lblSelectedRole.text
       })
       userData.push({
           key : " ",
           value : self.view.RichTextSelectedRole.text
       })
       return userData
    },

    getUserPermissions : function(){
          var accounts = [];
          var data =  this.view.segAccountPermissions.data;
          if(data){
              data.forEach(function(item){
                  accounts.push({
                     key : kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    value :  item.lblGeneralAccountName
                });
              });
          }
        return accounts;
    },

    getTransactionsAccess : function(){
        var transactionsAccess = [];
        var data = this.view.segTransferLimits.data;
        if(data){
            data.forEach(function(item){
                transactionsAccess.push(
                    {
                        key  : item.lblTransferType,
                        value : item.lblMaxDailyLimit+" - "+item.lblMaxDailyLimitValue+"\n"+ item.lblMaxTransactionLimit+" - "+item.lblMaxTransactionLimitValue
                    });  
            });
        }
        return transactionsAccess;
   },

    showAcknowledgementScreenOnPrintCancel : function(){
         this.view.flxAcknowledgement.setVisibility(true);
         if (CommonUtilities.isPrintEnabled()) {
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(true);
          } else {
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
          }
    },

  }
});