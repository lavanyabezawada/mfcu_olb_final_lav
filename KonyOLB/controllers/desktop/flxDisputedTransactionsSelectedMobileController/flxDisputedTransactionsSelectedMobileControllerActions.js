define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_cc9b6e32facb408ab57c94c1ba1ed591: function AS_FlexContainer_cc9b6e32facb408ab57c94c1ba1ed591(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnCancelRequests **/
    AS_Button_fe463990964b4c2b96d84f2985023013: function AS_Button_fe463990964b4c2b96d84f2985023013(eventobject, context) {
        var self = this;
        this.showEditRule();
    }
});