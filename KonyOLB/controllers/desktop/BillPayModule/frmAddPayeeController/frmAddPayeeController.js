/**
 * Representing a Add Payee form.
 * @module frmAddPayeeController
 */

define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants','CSRAssistUI'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants,CSRAssistUI) {
  var orientationHandler = new OrientationHandler();
  return /** @alias module:frmAddPayeeController */ {

    /** updates the present Form based on required function.
    * @param {list} uiDataMap used to load a view
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.ProgressBar) {
        if (uiDataMap.ProgressBar.show) {
          FormControllerUtility.showProgressBar(this.view);
        }
        else {
          FormControllerUtility.hideProgressBar(this.view);
        }
      }
      if (uiDataMap.firstLoad) { this.initiateAddPayee(uiDataMap.firstLoad) };
      if (uiDataMap.billersList) { this.showPayeeList(uiDataMap.billersList) };
      if (uiDataMap.payeeUpdateDetails) { this.presentPayeeDetails(uiDataMap.payeeUpdateDetails) };
      if (uiDataMap.payeeConfirmDetails) { this.presentConfirmPayeeDetails(uiDataMap.payeeConfirmDetails) };
      if (uiDataMap.payeeSuccessDetails) { this.presentAddPayeeAcknowledgement(uiDataMap.payeeSuccessDetails) };
      if (uiDataMap.billerDetails) { this.selectPayeeName(uiDataMap.billerDetails) };
      if (uiDataMap.registeredPayeeList) { this.updateRegisteredPayees(uiDataMap.registeredPayeeList) };
      if (uiDataMap.isInvalidPayee) { this.enterCorrectBillerName() };
      if (uiDataMap.errorInAddingPayee) { this.showAddPayeeErrorMessage(uiDataMap.errorInAddingPayee) };
      //one time payment
      if (uiDataMap.initOneTimePayment) { this.initOneTimePayment(uiDataMap.initOneTimePayment) };
      if (uiDataMap.validationError) { this.showValidationError(uiDataMap.validationError) };
      if (uiDataMap.showBillpayTransaction) { this.showBillpayTransaction() };
      this.AdjustScreen(100);
    },
    /**
     * addPayee pre show functionality
     */
    preshowFrmAddPayee: function () {
      var scopeObj = this;
      this.view.onBreakpointChange = function(){
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      
      this.view.customheader.forceCloseHamburger();
      this.hideCancelPopup();
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") }, { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee");
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
      if(document.getElementById("frmAddPayee_oneTimePay_txtZipCode")) {
        document.getElementById("frmAddPayee_oneTimePay_txtZipCode").autocomplete = "new-password";
      }     
      if(document.getElementById("frmAddPayee_oneTimePay_txtAccountNumber")) {
        document.getElementById("frmAddPayee_oneTimePay_txtAccountNumber").autocomplete = "new-password";
      }     
    },
    /**
     * used to hide the cancel PopUp
     */
    hideCancelPopup: function () {
      this.view.flxCancelPopup.isVisible = false;
      this.view.forceLayout();
    },

    /**
     * used to navigate the manage payees page.
     */
    viewallpayeesbtnaction: function () {
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      BillPayModule.presentationController.showBillPayData();
    },

    /**
     * used to show the payee information 
     */
    showFlxEnterPayeeInfo: function () {
      this.view.btnSearchPayee.skin = ViewConstants.SKINS.TAB_INACTIVE;
      this.view.btnSearchPayee.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
      this.view.btnEnterPayeeInfo.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
      this.view.btnEnterPayeeInfo.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
      this.view.flxSearchPayee.isVisible = false;
      this.view.flxRestNext2.isVisible = false;
      this.view.flxEnterPayeeInfo.isVisible = true;
      this.view.flxResetNext.isVisible = true;
      this.selectedMode = 'Manual';
      this.AdjustScreen(100);
      this.view.forceLayout();
      this.AdjustScreen(100);
    },

    /**
     * used to show the search payee screen
     */
    showFlxSearchPayee: function () {
      this.view.btnSearchPayee.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
      this.view.btnSearchPayee.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
      this.view.btnEnterPayeeInfo.skin =  ViewConstants.SKINS.TAB_INACTIVE;
      this.view.btnEnterPayeeInfo.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
      this.view.flxEnterPayeeInfo.isVisible = false;
      this.view.flxResetNext.isVisible = false;
      this.view.flxSearchPayee.isVisible = true;
      this.view.flxRestNext2.isVisible = true;
      this.resetSearchFormAddPayeeData();
      this.resetManualInformationPayeeData();
      this.AdjustScreen(100);
      this.selectedMode = 'Search';
      this.view.forceLayout();
      this.AdjustScreen(100);
    },

    /**
     * used to reset the search form in Add Payee Flow
     */
    resetSearchFormAddPayeeData: function () {
      this.view.tbxCustomerName.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxOne.text = "";
      this.view.tbxTwo.text = "";
      FormControllerUtility.disableButton(this.view.btnNext2);
    },

    /**
     * used to reset the manual payee information
     */
    resetManualInformationPayeeData: function () {
      this.view.tbxEnterName.text = "";
      this.view.tbxEnterAddress.text = "";
      this.view.tbxEnterAddressLine2.text = "";
      this.view.tbxCity.text = "";
      this.view.tbxEnterZipCode.text = "";
      this.view.tbxAdditionalNote.text = "";
      this.view.tbxEnterAccountNmber.text = "";
      this.view.tbxConfirmAccNumber.text = "";
      this.view.tbxAdditionalNote.text = "";
      //this.view.imgRememberMe.src = ViewConstants.IMAGES.CHECKED_IMAGE;
      this.view.lblRememberMeIcon.text=ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
      this.view.lblRememberMeIcon.skin=ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
      this.rememberThis();
    },

    /**
     * used to show the payee verification page
     */
    showFlxVerifyPayeeInformation: function () {
      this.view.flxMainWrapper.isVisible = false;
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") },
      { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Confirm") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Confirm");
      this.view.flxAddBillerDetails.isVisible = false;
      this.view.flxVerifyPayeeInformation.isVisible = true;
      this.view.flxAddPayeeAck.isVisible = false;
      this.view.forceLayout();
    },
    /**
     * used to show the biller details information
     */
    showflxAddBillerDetailsInformation: function () {
      this.view.flxMainWrapper.isVisible = false;
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") },
      { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Confirm") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Confirm");
      this.view.flxAddBillerDetails.top = '0dp';
      this.view.flxAddBillerDetails.isVisible = true;
      this.view.flxVerifyPayeeInformation.isVisible = false;
      this.view.flxAddPayeeAck.isVisible = false;
      this.view.forceLayout();
    },

    selectedMode: 'Search',
    /**
     * used to initilize the payee information
     */
    initializePayeeInformation: function () {
      this.view.customheader.forceCloseHamburger();
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = false;
      this.view.tbxCustomerName.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxOne.text = "";
      this.view.tbxTwo.text = "";
      this.view.tbxThree.text = "";
      this.normalizeBoxes(this.view.tbxOne, this.view.tbxTwo);
      FormControllerUtility.disableButton(this.view.btnNext2);

      this.view.flxNoExactMatch.isVisible = false;
      this.view.lblErrorInfo.isVisible = false;
      this.view.tbxEnterName.text = "";
      this.view.tbxEnterAddress.text = "";
      this.view.tbxEnterAddressLine2.text = "";
      this.view.tbxCity.text = "";
      this.view.tbxEnterZipCode.text = "";
      //this.view.imgRememberMe.src = ViewConstants.IMAGES.CHECKED_IMAGE;
      this.view.lblRememberMeIcon.text=ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
      this.view.lblRememberMeIcon.skin=ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
      this.view.tbxAdditionalNote.text = "";
      this.rememberThis();
      this.view.tbxEnterAccountNmber.text = "";
      this.view.tbxConfirmAccNumber.text = "";
      this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
      FormControllerUtility.disableButton(this.view.btnNext);
      this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
    },
    /**
     * used to reset the serach information
     */
    resetSearchedInformation: function () {
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = false;
      this.view.tbxCustomerName.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxOne.text = "";
      this.view.tbxTwo.text = "";
      this.view.tbxThree.text = "";
      this.normalizeBoxes(this.view.tbxOne, this.view.tbxTwo);
      FormControllerUtility.disableButton(this.view.btnNext2);
    },

    /**
     * used ti reset the manual information
     */
    resetManualInformation: function () {
      this.view.flxNoExactMatch.isVisible = false;
      this.view.lblErrorInfo.isVisible = false;
      this.view.tbxEnterName.text = "";
      this.view.tbxEnterAddress.text = "";
      this.view.tbxEnterAddressLine2.text = "";
      this.view.tbxCity.text = "";
      this.view.tbxEnterZipCode.text = "";
      this.view.tbxAdditionalNote.text = "";
      if (this.accountNumberAvailable) {
        this.view.tbxEnterAccountNmber.text = "";
        this.view.tbxConfirmAccNumber.text = "";
        this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
      }
      FormControllerUtility.disableButton(this.view.btnNext);
    },

    /**
     * reset the payee information
     */
    resetPayeeInformation: function () {
      if (this.selectedMode === 'Search') {
        this.resetSearchedInformation();
      }
      if (this.selectedMode === 'Manual') {
        this.resetManualInformation();
      }
      this.view.forceLayout();
    },
    /**
     * used to show the payee acknowledge ment screen
     */
    showFlxAddPayeeAck: function () {
      if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
      this.view.flxPrint.setVisibility(false); }
      else{this.view.flxPrint.setVisibility(true);}
      this.view.flxVerifyPayeeInformation.isVisible = false;
      this.view.flxMainWrapper.isVisible = false;
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") },
      { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee") + '-' + kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
      this.view.imgDownloadIcon.isVisible = true;
      if (CommonUtilities.isPrintEnabled()) {
        this.view.imgPrintIcon.isVisible = true;
      }else{
        this.view.imgPrintIcon.isVisible = false;
      }
      this.view.flxAddPayeeAck.isVisible = true;
      this.view.flxAddBillerDetails.isVisible = false;
      this.view.flxPrint.onClick = this.onClickPrint.bind(this);
      this.view.forceLayout();
    },
    /**
     * used to show the alert breadcrumb
     */
   alterBreadcrumb: function() {
     		var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            this.view.breadcrumb.imgBreadcrumb2.isVisible = false;
            this.view.breadcrumb.lblBreadcrumb3.isVisible = false;
			this.registerWidgetActions();
			applicationManager.getNavigationManager().applyUpdates(this);
            this.view.forceLayout();
        }, 
 

    /**
     * executes the post show of the form
     */
    postShowAddPayee: function () {
      var scopeObj = this;
      this.AdjustScreen(100);
      if (CommonUtilities.isCSRMode()) {
        CSRAssistUI.setCSRAssistConfigurations(scopeObj, "frmAddPayee");
      }
      if(kony.application.getCurrentBreakpoint()===640 || orientationHandler.isMobile){
          this.view.tbxCustomerName.placeholder= kony.i18n.getLocalizedString("i18n.AddPayee.SEARCHFORPAYEEBYNAME");
        this.view.tbxOne.placeholder=kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder");
      }
      else{
         this.view.tbxCustomerName.placeholder=kony.i18n.getLocalizedString("i18n.BillPay.SearchforPayeebyCompanyNameExATTorComcast");
       this.view.tbxOne.placeholder=kony.i18n.getLocalizedString("i18n.AddPayee.EnterAccountNumberasit");
      }
      this.AdjustScreen();
    },

    /**
     * used to exxcute the Ui functionality
     * @param {object}  data
     */
    AdjustScreen: function (data) {
      if (!data) {
          data = 0;
      }
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) { this.view.flxFooter.top = mainheight + diff + data + "dp"; }
        else { this.view.flxFooter.top = mainheight + data + "dp"; }
      } else {
        this.view.flxFooter.top = mainheight + data + "dp";
      }
      this.view.forceLayout();
      this.initializeResponsiveViews();
    },
    /**
     * remember functionality
     */
    rememberThis: function () {
      if (this.view.lblRememberMeIcon.text===ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
        //this.view.imgRememberMe.src = ViewConstants.IMAGES.CHECKED_IMAGE;
        this.view.lblRememberMeIcon.text=ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
        this.view.lblRememberMeIcon.skin=ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
        this.view.tbxAdditionalNote.isVisible = true;
        this.view.tbxEnterAccountNmber.skin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxEnterAccountNmber.focusSkin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxEnterAccountNmber.hoverSkin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxEnterAccountNmber.setEnabled(false);
        this.view.tbxConfirmAccNumber.skin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxConfirmAccNumber.focusSkin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxConfirmAccNumber.hoverSkin = 'sknSSP42424215BgD3D3D3Op10BorderE3E3E3';
        this.view.tbxConfirmAccNumber.setEnabled(false);
        this.accountNumberAvailable = false;
        this.checkIfAllManualFieldsAreFilled();
      }
      else {
        //this.view.imgRememberMe.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
        this.view.lblRememberMeIcon.text=ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
        this.view.lblRememberMeIcon.skin=ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
        this.view.tbxAdditionalNote.isVisible = false;
        this.view.tbxAdditionalNote.text = "";
        this.view.tbxEnterAccountNmber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
        this.view.tbxEnterAccountNmber.focusSkin = 'sknSSP42424215PxBorder4A90E2';
        this.view.tbxEnterAccountNmber.hoverSkin = 'sknSSP42424215PxBorder4A90E2';
        this.view.tbxEnterAccountNmber.setEnabled(true);
        this.view.tbxConfirmAccNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
        this.view.tbxConfirmAccNumber.focusSkin = 'sknSSP42424215PxBorder4A90E2';
        this.view.tbxConfirmAccNumber.hoverSkin = 'sknSSP42424215PxBorder4A90E2';
        this.view.tbxConfirmAccNumber.setEnabled(true);
        this.accountNumberAvailable = true;
        this.checkIfAllManualFieldsAreFilled();
      }
      this.view.forceLayout();
    },

    /**
     * used to navigate the history tab
     */
    navigateToBillPayHistoryTab: function () {
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      BillPayModule.presentationController.showBillPayData(null, "History", true);
    },

    /**
     * used tj navigate the pay a bill screen
    */
    makeabillpaymentbtnaction: function () {
      
    },

    /**
     * used to show the main wrapper 
     */
    showMainWrapper: function () {
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") }, { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee");
      this.view.flxMainWrapper.isVisible = true;
      this.view.flxVerifyPayeeInformation.isVisible = false;
      this.view.flxAddBillerDetails.isVisible = false;
      this.view.flxAddPayeeAck.isVisible = false;
      this.view.flxAdditionError.isVisible = false;
      this.view.forceLayout();
      this.AdjustScreen(100);
    },
    /**
     * used to cancel the popup
     */
    showCancelPopup: function () {
      var height_to_set = this.view.flxFooter.frame.height + this.view.flxFooter.frame.y;
      this.view.flxCancelPopup.height = height_to_set + "dp";
      this.view.flxCancelPopup.isVisible = true;
      this.view.flxCancelPopup.setFocus(true);
      this.view.CustomPopupCancel.lblHeading.setFocus(true);
      this.view.forceLayout();
    },
    payeeList: null,
    accountNumberAvailable: true,

    /**
     * used to show the BillPay Transaction
     */
    showBillpayTransaction: function () {
      this.view.oneTimePay.flxSearchPayee.setVisibility(false);
      this.view.oneTimePay.flxDetails.setVisibility(true);
    },
    /**
     * one time payement initlize screen
     */
    initOneTimePayment: function () {
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") }, { text: kony.i18n.getLocalizedString("i18n.billpay.oneTimePayment") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billpay.oneTimePayment");
      this.view.lblAddPayee.text = kony.i18n.getLocalizedString("i18n.billpay.oneTimePayment");
      this.view.flxAddPayee.setVisibility(false);
      this.view.customheader.forceCloseHamburger();
      this.view.flxonetimepayment.setVisibility(true);
      this.resetTextBoxes();
      this.hideCancelPopup();
      this.view.oneTimePay.flxmobilenumber.setVisibility(true);
      this.view.oneTimePay.flxDetails.setVisibility(false);
      this.view.oneTimePay.lblWarning.isVisible = false;
      this.view.oneTimePay.flxPayeeList.setVisibility(false);
      this.view.flxVerifyPayeeInformation.isVisible = false;
      this.view.flxAddBillerDetails.isVisible = false;
      this.view.flxAddPayeeAck.isVisible = false;
      this.view.flxMainWrapper.isVisible = true;
      FormControllerUtility.disableButton(this.view.oneTimePay.btnNext);
      this.normalizeBoxes(this.view.oneTimePay.tbxName, this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);
      this.view.oneTimePay.btnNext.onClick = this.updateNewPayee;
      this.view.btnDetailsBack.onClick = this.navigateToAcknowledgementForm;
      this.view.oneTimePay.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.reset");
      this.view.oneTimePay.btnCancel.onClick = this.resetTextBoxes;
      this.view.btnModify.onClick = this.showUpdateBillerPage;
      this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
      this.view.flxAdditionError.isVisible = false;
      this.view.oneTimePay.flxSearchPayee.setVisibility(true);
      this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.billPay.BillPay"), "Make One Time Payment");
      if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
      else this.view.customheader.lblHeaderMobile.text = "";
    },
    /**
     * used to show the biller page
     */
    showUpdateBillerPage: function () {
      var scopeObj = this;
      scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      this.presenter.showUpdateBillerPage();
    },

    /**
     * used to reset the text box values
     */
    resetTextBoxes: function () {
      [this.view.oneTimePay.tbxName, this.view.oneTimePay.txtZipCode, this.view.oneTimePay.txtAccountNumber,
      this.view.oneTimePay.txtAccountNumberAgain, this.view.oneTimePay.txtmobilenumber].map(function (element) {
        element.onKeyUp = this.checkIfAllSearchFieldsAreFilled;
        element.text = "";
      }, this);
      this.view.oneTimePay.tbxName.onKeyUp = this.callOnKeyUp;
    },
    /**
     * used to navigate the billPay Acknowledgement form.
     */
    navigateToAcknowledgementForm: function () {
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      BillPayModule.presentationController.navigateToAcknowledgementForm({});
    },
    /**
     * used to update the new payee information
     */
    updateNewPayee: function () {
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      var inputObject = {
        billerName: this.view.oneTimePay.tbxName.text,
        zipCode: this.view.oneTimePay.txtZipCode.text,
        accountNumber: this.view.oneTimePay.txtAccountNumber.text,
        accountNumberAgain: this.view.oneTimePay.txtAccountNumberAgain.text,
        mobileNumber: this.view.oneTimePay.txtmobilenumber.text
      };
      this.view.oneTimePay.lblWarning.isVisible = false;
      this.normalizeBoxes(this.view.oneTimePay.tbxName, this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);
      BillPayModule.presentationController.updateOneTimePayeeInfo(inputObject);
    },

    /**
     * used to show the payee
     * @param {list} payeeList list of payees
     */
    showPayeeList: function (payeeList) {
      if (this.view.flxonetimepayment.isVisible) {
        if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
        this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
        else
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.oneTimePay.segPayeesName.widgetDataMap = {
          "flxAccountTypes": "flxNewPayees",
          "lblUsers": "lblNewPayees"
        };
        if (payeeList.length > 0) {
          this.view.oneTimePay.segPayeesName.setData(this.createNewPayeeModel(payeeList));
          this.view.oneTimePay.flxPayeeList.isVisible = true;
          this.view.forceLayout();
        }
        else {
          this.view.oneTimePay.flxPayeeList.isVisible = false;
          this.view.forceLayout();
        }
      } else {
        this.view.segPayeesName.widgetDataMap = {
          "flxNewPayees": "flxNewPayees",
          "lblNewPayees": "lblNewPayees"
        };
        if (payeeList.length > 0) {
          this.view.segPayeesName.setData(this.createNewPayeeModel(payeeList));
          this.view.flxPayeeList.isVisible = true;
          this.view.forceLayout();
        }
        else {
          this.view.flxPayeeList.isVisible = false;
          this.view.forceLayout();
        }
      }
    },

    /**
     * used to create the new payee
     *  @param {list} billerList list of billers 
     * @returns {object} list of billers
     */
    createNewPayeeModel: function (billerList) {
      return billerList.map(function (biller) {
        return {
          "lblNewPayees": biller.billerName,
          "flxNewPayees": {
            "onClick": biller.onBillerSelection

          }
        };
      }).reduce(function (p, e) { return p.concat(e); }, []);
    },

    /**
     * used to select the payee name
     * @param {object} viewModel object
     */
    selectPayeeName: function (viewModel) {

      if (this.view.flxonetimepayment.isVisible) {
        if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
        this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
        else
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.oneTimePay.tbxName.text = viewModel.billerName;
        this.view.oneTimePay.flxPayeeList.isVisible = false;
      } else {
        this.view.tbxCustomerName.text = viewModel.billerName;
        this.view.flxPayeeList.isVisible = false;
      }
      this.setDynamicFields(viewModel.billerCategoryName);
      this.checkIfAllSearchFieldsAreFilled();
      this.view.forceLayout();
    },

    /**
     * used to set the payee data dynamicllay
     * @param {object} billerCategory billerCategory
    */
    setDynamicFields: function (billerCategory) {
      if (billerCategory == 'Credit Card' || billerCategory == 'Utilities') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
          kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
          { 'fieldHolds': 'AccountNumber' },
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber")
        );
      }
      else if (billerCategory == 'Phone') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder"),
          { 'fieldHolds': 'RelationshipNumber' },
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
          kony.i18n.getLocalizedString("i18n.common.MobilePhone"),
          kony.i18n.getLocalizedString("i18n.common.MobilePhonePlaceholder"),
          { 'fieldHolds': 'MobileNumber' });
      }
      else if (billerCategory == 'Insurance') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
          kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
          { 'fieldHolds': 'AccountNumber' },
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumber"),
          kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumberPlaceholder"),
          { 'fieldHolds': 'PolicyNumber' });
      }
    },

    /**
     * used to change the fields according to category.
     * @param {string} displaylabel1 label information
     * @param {string} placeholder1  place holder information
     * @param {string} info1 description
     * @param {string} displaylabel2 label information
     * @param {string} placeholder2  place holder information
     * @param {string} displaylabel3 label information
     * @param {string} placeholder3  place holder information
     * @param {string} info3 description
     */
    doDynamicChangesAccordingToCategory: function (displaylabel1, placeholder1, info1, displaylabel2, placeholder2, displaylabel3, placeholder3, info3) {

      if (this.view.flxonetimepayment.isVisible) {
        this.view.oneTimePay.lblAccountNumber.text = displaylabel1;
        this.view.oneTimePay.txtAccountNumber.placeholder = placeholder1;
        this.view.oneTimePay.txtAccountNumber.info = info1;
        if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
        this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
        else
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.oneTimePay.lblAccountNumberAgain.text = displaylabel2;
        this.view.oneTimePay.txtAccountNumberAgain.placeholder = placeholder2;

        if (displaylabel3 !== undefined && placeholder3 !== undefined && info3 !== undefined) {
          this.view.oneTimePay.lblmobilenumeber.text = displaylabel3;
          this.view.oneTimePay.txtmobilenumber.placeholder = placeholder3;
          this.view.oneTimePay.txtmobilenumber.info = info3;
          this.view.oneTimePay.lblmobilenumeber.isVisible = true;
          this.view.oneTimePay.txtmobilenumber.isVisible = true;
          this.view.oneTimePay.flxmobilenumber.setVisibility(true);
        } else {
          this.view.oneTimePay.lblmobilenumeber.isVisible = false;
          this.view.oneTimePay.txtmobilenumber.isVisible = false;
          this.view.oneTimePay.flxmobilenumber.setVisibility(false);
        }
      } else {

        this.view.lblOne.text = displaylabel1;
        this.view.tbxOne.placeholder = placeholder1;
        this.view.tbxOne.info = info1;

        this.view.lblTwo.text = displaylabel2;
        this.view.tbxTwo.placeholder = placeholder2;

        if (displaylabel3 !== undefined && placeholder3 !== undefined && info3 !== undefined) {
          this.view.lblThree.text = displaylabel3;
          this.view.tbxThree.placeholder = placeholder3;
          this.view.tbxThree.info = info3;
          this.view.lblThree.isVisible = true;
          this.view.tbxThree.isVisible = true;
        }
        else {
          this.view.lblThree.isVisible = false;
          this.view.tbxThree.isVisible = false;
        }
      }
    },

    /**
     * used to edit the manullay added payee details
     * @returns {object} manuallyAddedPayee
     */
    editManuallyAddedPayeeDetails: function () {
      var manuallyAddedPayee = {};
      manuallyAddedPayee.isManualUpdate = true;
      manuallyAddedPayee.billerName = this.view.tbxEnterName.text;
      if (this.accountNumberAvailable) {
        manuallyAddedPayee.noAccountNumber = false;
        manuallyAddedPayee.accountNumber = this.view.tbxConfirmAccNumber.text;
      }
      else {
        manuallyAddedPayee.noAccountNumber = true;
        manuallyAddedPayee.note = this.view.tbxAdditionalNote.text;
      }

      manuallyAddedPayee.addressLine1 = this.view.tbxEnterAddress.text;
      manuallyAddedPayee.addressLine2 = this.view.tbxEnterAddressLine2.text;
      manuallyAddedPayee.cityName = this.view.tbxCity.text;
      manuallyAddedPayee.state = this.view.lbxState.selectedKeyValue[1];
      manuallyAddedPayee.zipCode = this.view.tbxEnterZipCode.text;
      return manuallyAddedPayee;
    },

    /**
     * used to search the biller details
     */
    goToSerchedBillerDetails: function () {
      var manuallyAddedPayee = {};
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      this.view.flxMisMatch.isVisible = false;
      if (this.handleErrorInBillerSearch()) {
        manuallyAddedPayee.isManualUpdate = false;
        this.view.flxPayeeList.isVisible = false;
        if (this.view.tbxOne.info.fieldHolds === 'AccountNumber') {
          manuallyAddedPayee.accountNumber = this.view.tbxOne.text;
          if (this.view.tbxThree.isVisible) {
            if (this.view.tbxThree.info.fieldHolds === 'PolicyNumber') {
              manuallyAddedPayee.policyNumber = this.view.tbxThree.text;
            }
          }
        }
        else if (this.view.tbxOne.info.fieldHolds === 'RelationshipNumber') {
          manuallyAddedPayee.relationShipNumber = this.view.tbxOne.text;
          manuallyAddedPayee.mobileNumber = this.view.tbxThree.text;
        }
        manuallyAddedPayee.billerName = this.view.tbxCustomerName.text;
        manuallyAddedPayee.zipCode = this.view.tbxZipcode.text;
        this.presenter.showUpdateBillerPage(manuallyAddedPayee);
        this.resetManualInformation();
      }
    },
    /**
     * used to show the biller details screen
     */
    goToAddBillerDetails: function () {
      if (this.handleErrorInManualAddition()) {
        this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
        var contextData = this.editManuallyAddedPayeeDetails();
        this.presenter.showUpdateBillerPage(contextData);
        this.resetSearchedInformation();
      }
    },
    /**
     * used to show the payee details
     * @param {object} payeeDetails payee details
     */
    presentPayeeDetails: function (payeeDetails) {

      this.prePopulatePayeeDetails(payeeDetails);
      this.showflxAddBillerDetailsInformation();
      this.view.forceLayout();
    },
    /**
     * used to show the payee details
     * @param {object} payeeDetails payee details
     */
     prePopulatePayeeDetails: function (payeeDetails) {

      if (payeeDetails.accountNumber === "") {
        this.view.lblAddAccNumValue.text = kony.i18n.getLocalizedString("i18n.common.NotAvailable");
      } else {
        this.view.lblAddAccNumValue.text = payeeDetails.accountNumber;
      }
      this.view.lblAddBillerValue.text = payeeDetails.billerName;
      this.view.rtxAddAddressValue.text = payeeDetails.billerAddress;

      this.view.tbxAddNickValue.text = payeeDetails.payeeNickName;
      this.view.tbxAddNameOnBillValue.text = payeeDetails.nameOnBill;
    },
    /**
     * used to show the confirmation payee details screen
     */
    goToConfirmPayeeDetails: function () {
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      var data = this.storeNickNameAndNameOnBill();
      this.view.flxAdditionError.isVisible = false;
      this.presenter.showAddPayeeConfirmPage(data);
    },
    /**
     * used to store the nickName and name on bill
     * @returns {object} returns the nickname and payee Name
     */
    storeNickNameAndNameOnBill: function () {
      var model = {
        payeeNickName: null,
        nameOnBill: null
      };
      model.payeeNickName = this.view.tbxAddNickValue.text;
      model.nameOnBill = this.view.tbxAddNameOnBillValue.text;
      return model;
    },
    /**
     * used to show the ConfirmPayeeDetails
     * @param {object} completePayeeDetails payee details
     */
    presentConfirmPayeeDetails: function (completePayeeDetails) {
      this.prePopulateConfirmPayeeDetails(completePayeeDetails);
      this.showFlxVerifyPayeeInformation();
    },
    /**
     * used to show the payee details  screen on confirmation screen
     * @param {object} completePayeeDetails payee details
     */
    prePopulateConfirmPayeeDetails: function (completePayeeDetails) {
      this.view.lblBillerValue.text = completePayeeDetails.billerName;
      this.view.rtxBillerAddress.text = completePayeeDetails.billerAddress;
      if (completePayeeDetails.accountNumber === "") {
        this.view.lblBillerAccNumValue.text = kony.i18n.getLocalizedString("i18n.common.NotAvailable");
      }
      else {
        this.view.lblBillerAccNumValue.text = completePayeeDetails.accountNumber;
      }
      this.view.lblNicknameValue.text = completePayeeDetails.payeeNickName;
      this.view.lblNameOnBillValue.text = completePayeeDetails.nameOnBill;
    },

    /**
     * used to show the adpayee acknowledgement screen 
     */
    goToAddPayeeAcknowledgement: function () {
      FormControllerUtility.showProgressBar(this.view)
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      this.presenter.showAddPayeeSucess();
    },
    /**
     * used to show the payee acknowledgement screen
     * @param {object} successDetails response object
     */
    presentAddPayeeAcknowledgement: function (successDetails) {
      if(!successDetails.isDataBindedAlready) {
        this.prePopulateAcknowledgementDetails(successDetails);
      }
      this.showFlxAddPayeeAck();
    },
    /**
     * used to show the acknowledgement screen with prepopulated
     * @param {object} model response details
     */
    prePopulateAcknowledgementDetails: function (model) {
      this.view.lblAcknowledgementMessage.text = "‘" + model.billerName + "’ " + kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully");
      this.view.lblBillerVal.text = model.billerName;
      this.view.rtxDetailsAddressVal.text = model.billerAddress;
      if (model.accountNumber === "") {
        this.view.lblAccNumberVal.text = kony.i18n.getLocalizedString("i18n.common.NotAvailable");
      }
      else {
        this.view.lblAccNumberVal.text = model.accountNumber;
      }

      this.view.lblDetailsNickVal.text = model.payeeNickName;
      this.view.lblDetailsNOBVal.text = model.nameOnBill;
    },
    /**
     * used to show the add payee screen
     */
    initiateAddPayee: function () {
      this.hideCancelPopup();
      this.view.customheader.forceCloseHamburger();
      this.view.flxPrint.setVisibility(false);
      this.view.flxAddPayee.setVisibility(true);
      if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile){
        this.view.customheader.lblHeaderMobile.text = "Add Payee";
        this.view.btnSearchPayee.text = kony.i18n.getLocalizedString("i18n.BillPay.SearchforPayee");
        this.view.btnEnterPayeeInfo.text = kony.i18n.getLocalizedString("i18n.billpay.EnterInformation");
      }
      else{
      	this.view.customheader.lblHeaderMobile.text = "";
        this.view.btnSearchPayee.text = kony.i18n.getLocalizedString("i18n.AddPayee.SEARCHFORPAYEEBYNAME");
        this.view.btnEnterPayeeInfo.text = kony.i18n.getLocalizedString("i18n.AddPayee.ENTERPAYEEINFORMATION");
      }
      this.view.flxonetimepayment.setVisibility(false);
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString("i18n.billPay.BillPay") }, { text: kony.i18n.getLocalizedString("i18n.billPay.addPayee") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.addPayee");
      this.view.lblAddPayee.text = kony.i18n.getLocalizedString("i18n.billPay.addPayee");
      this.view.flxMainWrapper.isVisible = true;
      this.view.flxVerifyPayeeInformation.isVisible = false;
      this.view.flxAddBillerDetails.isVisible = false;
      this.view.flxAddPayeeAck.isVisible = false;
      this.initializePayeeInformation();
      this.registerWidgetActions();
      this.showFlxSearchPayee();
      this.accountNumberAvailable = true;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.flxAdditionError.isVisible = false;
      this.view.btnDetailsBack.onClick = this.showMainWrapper;
      this.view.btnModify.onClick = this.showMainWrapper;
      this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.billPay.BillPay"), "Add Payee");
    },
    /**
     * used to validate the manually added payee details
     * @returns {string} validation message
     */
    validateManuallyAddedPayeeDetails: function () {
      var accountNumber = this.view.tbxEnterAccountNmber.text;
      var duplicateAccountNumber = this.view.tbxConfirmAccNumber.text;
      if (this.accountNumberAvailable) {
        if (accountNumber === duplicateAccountNumber) {
          return 'VALIDATION_SUCCESS';
        }
        else {
          return 'ACCOUNT_NUMBER_MISMATCH';
        }
      }
      else {
        return 'VALIDATION_SUCCESS';
      }
    },
    /**
     * used to handle the error schenario from manually added payee
     * @returns {boolean} returns the status 
    */
    handleErrorInManualAddition: function () {
      var response = this.validateManuallyAddedPayeeDetails();
      if (response === 'VALIDATION_SUCCESS') {
        this.view.lblErrorInfo.isVisible = false;
        this.normalizeBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
        return true;
      }
      else if (response === 'ACCOUNT_NUMBER_MISMATCH') {
        this.view.lblErrorInfo.text = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
        this.view.lblErrorInfo.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext);
        this.higlightBoxes(this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber);
        this.view.forceLayout();
        return false;
      }
    },
    /**
     * used to validate the payee details
     * @returns {string} status of payee validation
     */
    validateSearchedPayeeDetails: function () {
      var identityNumber = this.view.tbxOne.text;
      var duplicateIdentityNumber = this.view.tbxTwo.text;
      if (this.view.tbxOne.info === undefined) {
        return 'NO_BILLER_SELECTED_FROM_LIST';
      }
      else {
        if (identityNumber === duplicateIdentityNumber) {
          return 'VALIDATION_SUCCESS';
        }
        else {
          return 'IDENTITY_NUMBER_MISMATCH';
        }
      }
    },
    /**
     * used to handle the error schenarion in biller search 
     * @returns {boolean} status
     */
    handleErrorInBillerSearch: function () {
      var response = this.validateSearchedPayeeDetails();
      if (response === 'VALIDATION_SUCCESS') {
        this.normalizeBoxes(this.view.tbxOne, this.view.tbxTwo);
        this.view.lblNotMatching.isVisible = false;
        return true;
      }
      else if (response === 'IDENTITY_NUMBER_MISMATCH') {
        if (this.view.tbxOne.info.fieldHolds === 'AccountNumber') {
          this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
        }
        else if (this.view.tbxOne.info.fieldHolds === 'RelationshipNumber') {
          this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberMismatch");
        }
        this.view.flxMisMatch.isVisible = false;
        this.view.lblNotMatching.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext2);
        this.higlightBoxes(this.view.tbxOne, this.view.tbxTwo);
        this.view.forceLayout();
        return false;
      }
      else if (response === 'NO_BILLER_SELECTED_FROM_LIST') {
        this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
        this.view.flxMisMatch.isVisible = false;
        this.view.lblNotMatching.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext2);
        this.view.forceLayout();
        return false;
      }
    },
    /**
     * used to register the widget actions
     */
    registerWidgetActions: function () {
      var scopeObj = this;
      FormControllerUtility.disableButton(this.view.btnNext);
      FormControllerUtility.disableButton(this.view.btnNext2);
      [this.view.tbxEnterName, this.view.tbxEnterAddress, this.view.tbxEnterAddressLine2, this.view.tbxCity,
      this.view.tbxEnterZipCode, this.view.tbxEnterAccountNmber, this.view.tbxConfirmAccNumber, this.view.tbxAdditionalNote].map(function (element) {
        element.onKeyUp = this.checkIfAllManualFieldsAreFilled;
      }, this);
      [this.view.tbxZipcode, this.view.tbxOne, this.view.tbxTwo, this.view.tbxThree].map(function (element) {
        element.onKeyUp = this.checkIfAllSearchFieldsAreFilled;
      }, this);
      this.view.tbxCustomerName.onKeyUp = CommonUtilities.debounce(scopeObj.callOnKeyUp.bind(scopeObj),OLBConstants.FUNCTION_WAIT,false); 
    },
    /**
     * used to validates the on key up information
     */
    callOnKeyUp: function () {
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      BillPayModule.presentationController.fetchBillerList(this.view.flxonetimepayment.isVisible ? this.view.oneTimePay.tbxName.text : this.view.tbxCustomerName.text);
      this.checkIfAllSearchFieldsAreFilled();
    },
    /**
     * higlightBoxes
     */
    higlightBoxes: function () {
      for (var i = 0; i < arguments.length; i++) {
        arguments[i].skin = 'sknTbxSSPffffff15PxBorderFF0000opa50';
      }
    },
    /**
     * normalizeBoxes
     */
    normalizeBoxes: function () {
      for (var i = 0; i < arguments.length; i++) {
        arguments[i].skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
      }
    },
    /**
     * used to validates the all payee search fields 
     */
    checkIfAllSearchFieldsAreFilled: function() {
      var validationUtilityManager = applicationManager.getValidationUtilManager();
      if (this.view.flxonetimepayment.isVisible) {
        if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
        this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
        else
        this.view.customheader.lblHeaderMobile.text = "";
        if (this.view.oneTimePay.tbxName.text.trim() && this.view.oneTimePay.txtZipCode.text.trim() && this.view.oneTimePay.txtAccountNumber.text.trim() && this.view.oneTimePay.txtAccountNumberAgain.text.trim() &&
          ((this.view.oneTimePay.txtmobilenumber.isVisible && this.view.oneTimePay.txtmobilenumber.text.trim() && validationUtilityManager.phoneNumberRegex.test(this.view.oneTimePay.txtmobilenumber.text.trim())) || !this.view.oneTimePay.txtmobilenumber.isVisible)) {
          FormControllerUtility.enableButton(this.view.oneTimePay.btnNext);
        } else {
          FormControllerUtility.disableButton(this.view.oneTimePay.btnNext);
        }
      } else { // need to change the conditions
        if (this.view.tbxThree.isVisible === true) {
          if (this.view.tbxCustomerName.text.trim() && this.view.tbxZipcode.text.trim() && this.view.tbxOne.text.trim() && this.view.tbxTwo.text.trim() && (this.view.tbxThree.text.trim() && validationUtilityManager.phoneNumberRegex.test(this.view.tbxThree.text.trim()))) {
            FormControllerUtility.enableButton(this.view.btnNext2);
          }
          else {
            FormControllerUtility.disableButton(this.view.btnNext2);
          }
        }
        else {
          if (this.view.tbxCustomerName.text.trim() && this.view.tbxZipcode.text.trim() && this.view.tbxOne.text.trim() && this.view.tbxTwo.text.trim()) {
            FormControllerUtility.enableButton(this.view.btnNext2);
          }
          else {
            FormControllerUtility.disableButton(this.view.btnNext2);
          }
        }
      }
    },
    /**
     * validates the all manually added payee manidatory information 
     */
    checkIfAllManualFieldsAreFilled: function () {
      if (this.view.tbxAdditionalNote.isVisible === true) {
        if (this.view.tbxEnterName.text.trim() && this.view.tbxEnterAddress.text.trim() && this.view.tbxCity.text.trim() && this.view.tbxEnterZipCode.text.trim()) {
          FormControllerUtility.enableButton(this.view.btnNext);
        }
        else {
          FormControllerUtility.disableButton(this.view.btnNext);
        }
      }
      else {
        if (this.view.tbxEnterName.text.trim() && this.view.tbxEnterAddress.text.trim() && this.view.tbxCity.text.trim() && this.view.tbxEnterZipCode.text.trim() &&
          this.view.tbxEnterAccountNmber.text.trim() && this.view.tbxConfirmAccNumber.text.trim()) {
          FormControllerUtility.enableButton(this.view.btnNext);
        }
        else {
          FormControllerUtility.disableButton(this.view.btnNext);
        }
      }
    },

    /**
     * used to set the payee widget Map
     * @param {list} registeredPayees list of payees
     */
    updateRegisteredPayees: function (registeredPayees) {
      this.view.segRegisteredPayees.widgetDataMap = {
        "flxRegistered": "flxRegistered",
        "lblCustomer": "lblCustomer",
        "lblAmount": "lblAmount",
        "lblDate": "lblDate",
        "btnViewDetails": "btnViewDetails",
        "btnPayBills": "btnPayBills",
        "lblHorizontalLine": "lblHorizontalLine"
      };
      if(registeredPayees.length === 0){
        this.view.flxRegisteredPayees.setVisibility(false);
        this.view.flxBillPayActivities.top = "0dp";
      }
      else{
        this.view.flxRegisteredPayees.setVisibility(true);
        this.view.flxBillPayActivities.top = "552dp";
      	this.view.segRegisteredPayees.setData(this.createRegisteredPayeesSegmentModel(registeredPayees));
      }
      this.view.forceLayout();
    },

    /**
     * used to show the  list of all payees
     * @param {list} payees list of payees
     * @returns {object} payees object
     */
    createRegisteredPayeesSegmentModel: function (payees) {
      return payees.map(function (payee) {
        return {
          "lblCustomer": payee.payeeName,
          "lblHorizontalLine": "A",
          "lblDate": CommonUtilities.getFrontendDateString(payee.lastPaidDate),
          "lblAmount": CommonUtilities.formatCurrencyWithCommas(payee.lastPaidAmount),
          "btnViewDetails": {
            text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
            onClick: payee.onViewDetailsClick
          },
          "btnPayBills": {
            text: kony.i18n.getLocalizedString("i18n.Pay.PayBills"),
            onClick: payee.onPayBillsClick
          }
        };
      }).reduce(function (p, e) { return p.concat(e); }, []);
    },

    /**
     * validate  the biller Name
     */
    enterCorrectBillerName: function () {
      this.view.rtxMisMatch.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");;
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = true;
      FormControllerUtility.disableButton(this.view.btnNext2);
      this.view.forceLayout();
    },
    /**
     * used to show the validation error message
     * @param {object} response validation error message
     */
    showValidationError: function (response) {
      var errorMessage = response;
      if (response === "INVALID_BILLER") {
        errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
      } else if (response === "MISMATCH_RELATIONSHIPNUMBER") {
        errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberMismatch");
      } else if (response === "MISMATCH_ACCOUNTNUMBER") {
        errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
      }
      if (this.view.flxonetimepayment.isVisible) {
        if(kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
        this.view.customheader.lblHeaderMobile.text = "Make One Time Payment";
        else
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.oneTimePay.lblWarning.text = errorMessage;
        this.view.oneTimePay.lblWarning.isVisible = true;
        FormControllerUtility.disableButton(this.view.oneTimePay.btnNext);
        if (response === "INVALID_BILLER") {
          this.higlightBoxes(this.view.oneTimePay.tbxName);
        } else {
          this.higlightBoxes(this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);
        }
      } else {
        this.view.rtxMisMatch.text = errorMessage;
        this.view.lblNotMatching.isVisible = false;
        this.view.flxMisMatch.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext2);
      }
      this.view.forceLayout();
    },

    /**
     * used to show the add payee error message
     * @param {string} msg error message
     */
    showAddPayeeErrorMessage: function (msg) {
      this.view.rtxAdditionError.text = msg;
      this.view.flxAdditionError.isVisible = true;
      this.showflxAddBillerDetailsInformation();
      this.view.flxAddBillerDetails.top = '10dp';
      this.view.forceLayout();
    },


    responsiveViews:{},
    initializeResponsiveViews:function(){
        this.responsiveViews["flxMainWrapper"] = this.isViewVisible("flxMainWrapper");
        this.responsiveViews["flxAddPayee"] = this.isViewVisible("flxAddPayee");
        this.responsiveViews["flxonetimepayment"] = this.isViewVisible("flxonetimepayment");
        this.responsiveViews["flxRegisteredPayees"] = this.isViewVisible("flxRegisteredPayees");
        this.responsiveViews["flxBillPayActivities"] = this.isViewVisible("flxBillPayActivities");
        this.responsiveViews["flxVerifyPayeeInformation"] = this.isViewVisible("flxVerifyPayeeInformation");
        this.responsiveViews["flxAddBillerDetails"] = this.isViewVisible("flxAddBillerDetails");
        this.responsiveViews["flxAddPayeeAck"] = this.isViewVisible("flxAddPayeeAck");        
    },
    isViewVisible: function(container){
        if(this.view[container].isVisible){
            return true;
        }else{
            return false;
        }
    },
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmAddPayeeController}
    * @param {integer} width - current browser width
    * @return {} 
    * @throws {}
    */
    onBreakpointChange: function(width){
      kony.print('on breakpoint change');
      orientationHandler.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChange(width);
      this.setupFormOnTouchEnd(width);

      this.AdjustScreen();
      var responsiveFonts = new ResponsiveFonts();
      var views;
      if(width===640 || orientationHandler.isMobile){
        this.view.customheader.lblHeaderMobile.text = "Add Payee";
        this.view.btnSearchPayee.text = kony.i18n.getLocalizedString("i18n.BillPay.SearchforPayee");
        this.view.btnEnterPayeeInfo.text = kony.i18n.getLocalizedString("i18n.billpay.EnterInformation");
        this.view.lblAddPayee.setVisibility(false);
        responsiveFonts.setMobileFonts();
        views = Object.keys(this.responsiveViews);
        var scope = this;
        views.forEach(function(e) {
          scope.view[e].isVisible = scope.responsiveViews[e];
        });
      }else{
        this.view.customheader.lblHeaderMobile.text = "";
        this.view.btnSearchPayee.text = kony.i18n.getLocalizedString("i18n.AddPayee.SEARCHFORPAYEEBYNAME");
        this.view.btnEnterPayeeInfo.text = kony.i18n.getLocalizedString("i18n.AddPayee.ENTERPAYEEINFORMATION");
        this.view.lblAddPayee.setVisibility(true);
        responsiveFonts.setDesktopFonts();
        views = Object.keys(this.responsiveViews);
        var scope = this;
        views.forEach(function(e) {
          scope.view[e].isVisible = scope.responsiveViews[e];
        });
      }
      this.AdjustScreen();

    },

      setupFormOnTouchEnd: function(width){
        if(width==640 || orientationHandler.isMobile){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024 || orientationHandler.isTablet){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();   
              } 
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
      },

    onClickPrint : function() {
      var printData = [];
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.common.status"),
        value: this.view.lblAcknowledgementMessage.text
      });
      printData.push({
        key: this.view.lblBillerKey.text,
        value: this.view.lblBillerVal.text
      });
      printData.push({
        key: this.view.lblDetailsAddressKey.text,
        value: this.view.rtxDetailsAddressVal.text
      });
      printData.push({
        key: this.view.lblAccNumberKey.text,
        value: this.view.lblAccNumberVal.text
      });
      printData.push({
        key: this.view.lblDetailsNickKey.text,
        value: this.view.lblDetailsNickVal.text
      });
      printData.push({
        key: this.view.lblDetailsNOBkey.text,
        value: this.view.lblDetailsNOBVal.text
      });
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;

      var printCallback = function(){
        applicationManager.getNavigationManager().navigateTo("frmAddPayee");
        applicationManager.getNavigationManager().updateForm({payeeSuccessDetails : {isDataBindedAlready: true}}, "frmAddPayee");
      }
      var viewModel = {
        moduleHeader: this.view.lblAddPayee.text,
        tableList: [{
            tableHeader: this.view.lblHeaderPayeeDetails.text,
            tableRows: printData
        }],
        printCallback: printCallback
      }
      this.presenter.showPrintPage({
        printKeyValueGroupModel: viewModel
      });
    }
  }
});