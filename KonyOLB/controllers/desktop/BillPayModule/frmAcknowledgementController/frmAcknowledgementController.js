/**
 * Description of Module representing a Confirm form.
 * @module frmAckowledgementController
 */

define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function (commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
  var orientationHandler = new OrientationHandler();
  return  /** @alias module:frmAckowledgementController */{
    /** updates the present Form based on required function.
       * @param {uiDataMap[]} uiDataMap 
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
      } else { 
          FormControllerUtility.hideProgressBar(this.view);
      }
      if (uiDataMap.sideMenu) {
        this.updateHamburgerMenu(uiDataMap.sideMenu);
      }
      if (uiDataMap.topBar) {
        this.updateTopBar(uiDataMap.topBar);
      }

      if (uiDataMap.bulkPay) {
        this.updateBulkPayUIAcknowledge(uiDataMap.bulkPay);
      }
      if (uiDataMap.transferAcknowledge) {
        this.updateTransferAcknowledgeUI(uiDataMap.transferAcknowledge);
      }

      if (uiDataMap.ackPayABill) {
        this.updateBillPayAcknowledgeUI(uiDataMap.ackPayABill);
      }

      this.AdjustScreen();
    },

    /**
     * used o update the BulkPay Acknoweldgement screen
     * @param {object} bulkPay list of responses
     * 
     */
    updateBulkPayUIAcknowledge: function (bulkPay) {
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      var self = this;
      var totalSum = 0;
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
      this.view.flxContainer.isVisible = false;
      this.view.flxMainBulkPay.isVisible = true;
      var bulkPayAckWidgetDataMap = {
        "lblRefernceNumber": "lblRefernceNumber",
        "lblPayee": "lblPayee",
       // "lblPayeeAddress": "lblPayeeAddress",
        "lblPaymentAccount": "lblPaymentAccount",
        "lblEndingBalanceAccount": "lblEndingBalanceAccount",
        "lblSendOn": "lblSendOn",
        "lblDeliverBy": "lblDeliverBy",
        "lblAmount": "lblAmount",
        "imgAcknowledgement": "imgAcknowledgement"
      };
      bulkPay = bulkPay.map(function (dataItem) {
        var ackImage;
        var deliverBy = dataItem.deliverBy;
        if (deliverBy.length > 10) {
          deliverBy = self.getDateFromDateString(dataItem.deliverBy, "YYYY-MM-DDTHH:MM:SS");
        }
        else {
          deliverBy = dataItem.deliverBy;
        }
        if (dataItem.transactionId !== "" || dataItem.transactionId !== null || dataItem.transactionId !== undefined) {
          ackImage = ViewConstants.IMAGES.BULKPAY_SUCCESS;
          totalSum = totalSum + parseFloat(dataItem.amount);
        } else {
          ackImage = ViewConstants.IMAGES.BULKPAY_UNSUCCESS;
        }
        return {
          "lblRefernceNumber": dataItem.transactionId,
          "lblPayee": dataItem.payeeName,
          "lblPayeeAddress": "Address: " + dataItem.payeeAddressLine1,
          "lblPaymentAccount": dataItem.fromAccountName,
          "lblEndingBalanceAccount": "Ending Balance: " + commonUtilities.formatCurrencyWithCommas(dataItem.fromAccountBalance),
          "lblSendOn": self.getDateFromDateString(dataItem.transactionDate, "YYYY-MM-DDTHH:MM:SS"),
          "lblDeliverBy": deliverBy,
          "lblAmount": commonUtilities.formatCurrencyWithCommas(dataItem.amount),
          "imgAcknowledgement": ackImage
        };
      });
      this.view.lblAmountValue.text = commonUtilities.formatCurrencyWithCommas(totalSum);
      this.view.segBill.widgetDataMap = bulkPayAckWidgetDataMap;
      this.view.segBill.setData(bulkPay);
      this.view.btnViewPaymentActivity.onClick = this.onClickPaymentActivity;
      this.view.btnMakeAnotherPayment.onClick = this.onClickMakePayment;
      this.view.flxSuccessMessage.setVisibility(false);
      this.view.customheader.customhamburger.activateMenu("Bill Pay");
      this.view.forceLayout();
    },

    /**
     * used to perform UI Activities like fith the UI in Screen.
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },

    /**
     * used to navigate the billPay History screen
     */
    onClickPaymentActivity: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showBillPayData(null, 'History', false);
    },

    /**
     * used to navigate the allPayees screen
     */
    onClickMakePayment: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showBillPayData(null, {
        show: 'AllPayees',
        sender: 'acknowledgement',
        resetSorting: true
      });
    },

    /**
     * used perform the initialize activities.
     * 
    */
    initActions: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      if(kony.application.getCurrentBreakpoint() == 640) {
        this.view.Balance.lblAvailableBalance.skin = "sknSSP42424217Pxop70";
        this.view.Balance.lblSavingsAccount.skin = "sknSSPMediium42424217Pxop70";
      } else {
        this.view.Balance.lblAvailableBalance.skin = "sknLblSSP42424215px";
        this.view.Balance.lblSavingsAccount.skin = "sknLblSSP42424215pxBold";
      }
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
      this.AdjustScreen();
      this.view.btnSavePayee.onClick = this.onClickSavePayee;
      if (commonUtilities.isPrintEnabled()) {
        if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
          this.view.flxPrintBulkPay.setVisibility(false);
        }else{  this.view.flxPrintBulkPay.setVisibility(true);}
        this.view.imgPrintBulk.setVisibility(true);
        this.view.lblPrintfontIcon.setVisibility(true);
        this.view.imgPrintBulk.onTouchStart = this.onClickPrintBulk;
        this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
      } else {
        this.view.imgPrintBulk.setVisibility(false);
        this.view.lblPrintfontIcon.setVisibility(false);
        this.view.flxPrintBulkPay.setVisibility(false);
      }
    },

    /**
     * used to perform the post show activities
     * 
     */
    postShowFrmAcknowledgement: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.confirmDialogAccounts.confirmButtons.setVisibility(false);
      this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text=kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+applicationManager.getConfigurationManager().getCurrencyCode()+")";
      this.customiseHeaderComponentforAcknowledgement();
      this.view.forceLayout();
      this.view.flxDownload.setVisibility(false); // AOLB-997
      applicationManager.getNavigationManager().applyUpdates(this);
      this.AdjustScreen();
    },

    /**
     * used to perform Ui Activities
     */
    customiseHeaderComponentforAcknowledgement: function () {
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
    },
    /**used to convert the CalenderFormat Date
     * @param {String} dateString string formated date 
     * @param {String} inputFormat input format of the date
     * @return {date} Date object
     */
    getDateFromDateString: function (dateString, inputFormat) {
      var fu = applicationManager.getFormatUtilManager();
      var dateObj = fu.getDateObjectfromString(dateString, inputFormat);
      var outputDate= fu.getFormatedDateString(dateObj, fu.getApplicationDateFormat());
      return outputDate;
    },
    /**Returns Date Object from Date Components
   * @param  {array} dateComponents Date Components returned from Calendar Widget
   * @return {date} date
   */
    getDateObj: function (dateComponents) {
      var date = new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1]) - 1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0, 0, 0, 0)
      return date;
    },
    /** Compares Date with todays and tell is its future or not
   * @param  {object} dateComponents object
   * @returns {boolean} True for future else false
   */
    isFutureDate: function (dateComponents) {
      var dateObj = this.getDateObj(dateComponents)
      var endTimeToday = new Date();
      var minutes = ViewConstants.MAGIC_NUMBERS.MAX_MINUTES;
      endTimeToday.setHours(ViewConstants.MAGIC_NUMBERS.MAX_HOUR, minutes, minutes, minutes);
      if (dateObj.getTime() > endTimeToday.getTime()) {
        return true;
      }
      return false;
    },
    /**Formats the Currency
       * @param  {Array} amount Array of transactions model 
       * @param  {function} currencySymbolNotRequired Needs to be called when cancel button is called
       * @return {function} function to put comma
       */
    formatCurrency: function (amount, currencySymbolNotRequired,currencyCode) {
      return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired,currencyCode);
    },
    /**Entry Point for Transfer Acknowledgement
    * @param {object} viewModel Transfer Data
    */
    updateTransferAcknowledgeUI: function (viewModel) {
      var current_breakpoint = kony.application.getCurrentBreakpoint();
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");
      this.customizeUIForTransferAcknowledege();
      if (this.isFutureDate(viewModel.transferData.sendOnDateComponents) || viewModel.transferData.frequencyKey !== "Once") {
        this.showTransferAcknowledgeForScheduledTransaction(viewModel);
      }
      else {
        this.showTransferAcknowledgeForRecentTransaction(viewModel);
      }
      if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.frame.height+ this.view.flxAcknowledgementMain.frame.y + 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(true);
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = commonUtilities.getAccountDisplayName(viewModel.transferData.accountFrom);
      this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.accountTo.beneficiaryName ? viewModel.transferData.accountTo.nickName : (commonUtilities.getAccountDisplayName(viewModel.transferData.accountTo));
      this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = this.formatCurrency(viewModel.transferData.amount, true);
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.send_on');
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.sendOnDate;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = viewModel.transferData.frequencyType;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = viewModel.transferData.notes;
      this.view.btnMakeTransfer.onClick = function () {
        this.presenter.showTransferScreen();
      }.bind(this);

      this.view.btnAddAnotherAccount.onClick = function () {
        this.presenter.showTransferScreen({ initialView: 'recent' });
      }.bind(this);
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.viewTransferActivity');
      this.view.customheader.customhamburger.activateMenu("Transfers")
    },
    /**UI Logic for Transfers
       */
    customizeUIForTransferAcknowledege: function () {
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(false);
      this.view.flxContainer.setVisibility(true);
      this.view.btnSavePayee.setVisibility(false);
      this.view.lblBillPayAcknowledgement.text = "Transfer Money- Acknowledgement";
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfereuro");
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfereuro");
      this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity");
      this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl");
      this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
    },
    /**Entry Point for Transfer Acknowledgement for Scheduled Transaction
       * @param {object} viewModel Transfer Data
       */
    showTransferAcknowledgeForScheduledTransaction: function (viewModel) {
      function getDateFromDateComponents(dateComponents) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return [dateComponents[0], monthNames[dateComponents[1] - 1], dateComponents[2]].join(" ");
      }
      this.view.acknowledgmentModify.setVisibility(true);
      this.view.acknowledgment.setVisibility(false);
      this.view.Balance.setVisibility(false);
      this.view.acknowledgmentModify.btnModifyTransfer.text = kony.i18n.getLocalizedString("i18n.common.modifyTransaction");
      this.view.acknowledgmentModify.btnModifyTransfer.onClick = this.modifyTransfer.bind(this, viewModel.transferData);
      this.view.acknowledgmentModify.btnModifyTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
      this.view.acknowledgmentModify.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
      this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourTransactionHasBeenScheduledfor");
      this.view.acknowledgmentModify.lblTransactionDate.text = getDateFromDateComponents(viewModel.transferData.sendOnDateComponents);
      this.view.acknowledgmentModify.lblTransactionDate.setVisibility(true);  
      if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "ON_SPECIFIC_DATE") {
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);        
        this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourRecurringTransferScheduled");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.endby");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.endOnDate;
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);

      }
      else if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "NO_OF_RECURRENCES") {        
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);  
        this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourRecurringTransferScheduled");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
        this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.numberOfRecurrences;
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
      else {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      }
    },

    /**Modify A Scheduled Transfer
       * @param {object} transferData Transfer Data
       */
    modifyTransfer: function (transferData) {
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");
      var record =  {transactionType: this.presenter.getTransferType(transferData.accountTo),
      toAccountNumber: transferData.accountTo.accountID,
      fromAccountNumber: transferData.accountFrom.accountID,
      ExternalAccountNumber: transferData.accountTo.accountNumber,
      amount: transferData.amount,
      frequencyType: transferData.frequencyKey,
      numberOfRecurrences: transferData.numberOfRecurrences,
      transactionsNotes: transferData.notes,
      scheduledDate: transferData.sendOnDate,
      frequencyEndDate: transferData.endOnDate,
      frequencyStartDate: transferData.sendOnDate,
      isScheduled: "1",
      transactionDate: transferData.sendOnDate,
      transactionId: transferData.referenceId,
      isModifiedTransferFlow: true,
      serviceName: transferData.serviceName
      }
      this.presenter.showTransferScreen({
       editTransactionObject: record
      });
    },
    /**Entry Point for Transfer Acknowledgement for Recent Transaction 
       * @param {object} viewModel Transfer Data
       */
    showTransferAcknowledgeForRecentTransaction: function (viewModel) {
      this.view.acknowledgment.setVisibility(true);
      this.view.Balance.setVisibility(true);
      this.view.acknowledgmentModify.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      this.view.Balance.lblSavingsAccount.text = viewModel.transferData.accountFrom.accountName;
      this.view.Balance.lblBalanceValue.text = this.formatCurrency(viewModel.transferData.accountFrom.availableBalance,false,viewModel.transferData.accountFrom.currencyCode);
    },
    /**
     * used to set single Bill Pay scrren
     * @param {object} data
     */
    updateBillPayAcknowledgeUI: function (data) {
      var self = this;
       var current_breakpoint = kony.application.getCurrentBreakpoint();
      if (data.savedData.gettingFromOneTimePayment) {
        this.view.btnSavePayee.text = kony.i18n.getLocalizedString("i18n.billpay.savePayee");
        this.view.btnSavePayee.toolTip = kony.i18n.getLocalizedString("i18n.billpay.savePayee");
        this.view.btnSavePayee.setVisibility(true);
      } else {
        this.view.btnSavePayee.setVisibility(false);
      }
      this.customizeUIForBillPayAcknowledege();
      this.registerActionsBillPay(data.savedData);
      this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.text = data.referenceId;
      this.view.Balance.lblSavingsAccount.text = data.accountData.accountName;
      this.view.Balance.lblBalanceValue.text = commonUtilities.formatCurrencyWithCommas(data.accountData.availableBalance, false,data.accountData.currencyCode);
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.savedData.payFrom;
      this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = data.savedData.payeeName;
      this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.savedData.languageAmount;
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.savedData.sendOn;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = data.savedData.categories;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.savedData.notes;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = kony.i18n.getLocalizedString("i18n.accounts.Note");
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentDate");
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.category");
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(true);
      this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.DeliverBy");
      this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text = data.savedData.deliveryDate;
      if (data.savedData.gettingFromOneTimePayment) {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      }
      else {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.savedData.frequencyType;
      if (data.savedData.frequencyType !== "Once" && data.savedData.hasHowLong === "ON_SPECIFIC_DATE") {
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.recurringAcknowledgementMessage")+data.savedData.payeeName;
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.ConfirmEur.SendOn");
        this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.savedData.frequencyStartDate;
        this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.endby");
        this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text = data.savedData.frequencyEndDate;
      }
      else if (data.savedData.frequencyType !== "Once" && data.savedData.hasHowLong === "NO_OF_RECURRENCES") { 
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.recurringAcknowledgementMessage")+data.savedData.payeeName;
        this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
        this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text = data.savedData.numberOfRecurrences;
      }      
       if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.frame.height + this.view.flxAcknowledgementMain.frame.y + 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.customheader.customhamburger.activateMenu("Bill Pay");
    },

    /**
     * used to set single BillPay Acknowledge Page
     */
    customizeUIForBillPayAcknowledege: function () {
      var current_breakpoint = kony.application.getCurrentBreakpoint();
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.flxContainer.setVisibility(true);
      this.view.acknowledgmentModify.setVisibility(false);
      this.view.acknowledgment.setVisibility(true);
      this.view.Balance.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay")
      }]);
      if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.frame.height +this.view.flxAcknowledgementMain.frame.y+ 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.billPay.MakeAnotherPayment");
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.billPay.MakeAnotherPayment");
      this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString("i18n.billPay.ViewPaymentActivity");
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.billPay.ViewPaymentActivity");
      this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
      this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
      this.view.forceLayout();
    },

    /**
     * used to set actions to billPay Acknowledge Form
     * @param {object} data
     */
    registerActionsBillPay: function (data) {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      if (data.gettingFromOneTimePayment === true) {
        this.view.btnMakeTransfer.onClick = function () {
          self.presenter.showOneTimePaymentPage('acknowledgement', data);
        };
      } else {
        this.view.btnMakeTransfer.onClick = function () {
          data.categories = '';
          data.amount = '';
          data.sendOn = '';
          data.notes = '';
          data.deliveryDate = '';
          data.frequencyType = '';
          data.numberOfRecurrences = '';
          data.frequencyStartDate = '';
          data.frequencyEndDate = '';
          data.billCategory = '';
          self.presenter.showBillPayData('acknowledgement', 'PayABill', true, data);
        };
      }
      this.view.btnAddAnotherAccount.onClick = function () {
        FormControllerUtility.showProgressBar(this.view);
        self.presenter.showBillPayData('acknowledgement', 'History', true);
      };
    },
    /**
     * used to save the payee
     */
    onClickSavePayee: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showUpdateBillerPageWithTransaction({
        transactionId: this.view.acknowledgment.lblRefrenceNumberValue.text
      });
    },
    
    acknowledgementUI: function(){
         this.view.flxMain.top=50 +"dp";
        if (!this.view.Balance.isVisible) {
			   this.view.flxAcknowledgementMain.height=450+"dp";
			   this.view.flxTransactionDetails.top=490+"dp";
			   this.view.flxTransactionDetails.height=650+"px";
			} 
			else{
				this.view.flxAcknowledgementMain.height=530+"dp";
				this.view.flxAcknowledgementMain.frame.height = 530;
			   this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.frame.height + 40 + "dp";
			   this.view.flxTransactionDetails.height=563+"px";
			   this.view.flxTransactionDetails.frame.height=563;
			}
        this.view.btnMakeTransfer.top = 590 + this.view.flxTransactionDetails.frame.height + "dp";
        this.view.btnAddAnotherAccount.top = 590 + this.view.flxTransactionDetails.frame.height + 60 + "dp";
        this.view.btnSavePayee.top = 590 + this.view.flxTransactionDetails.frame.height + 120 + "dp";
        this.view.btnAddAnotherAccount.isVisible = true;
        this.view.flxTransactionDetails.left="10dp";
        this.view.flxTransactionDetails.width = this.view.flxAcknowledgementMain.width;
       this.AdjustScreen();
    },
    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmAcknowledgementController}
    * @param {integer} width - current browser width
    * @return {} 
    * @throws {}
    */
    onBreakpointChange: function (width) {
      kony.print('on breakpoint change');
      orientationHandler.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChange(width);
      this.setupFormOnTouchEnd(width);
      
      this.AdjustScreen();
      if (width == 640) {
        this.view.acknowledgmentModify.lblTransactionMessage.skin="sknSSPLight42424218Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin="sknSSPLight42424218Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin="sknBtnSSP0273e315px";
        this.view.btnTransferActivity.isVisible = false;
        this.view.confirmDialogAccounts.flxMain.skin = "sknFlexRoundedBorderFFFFFF3Pxshadowd9d9d9noshadow";
        this.view.customheader.lblHeaderMobile.text = "Acknowledgement";
        this.view.flxMain.top=50 +"dp";
        this.view.flxTransactionDetails.top = 470 + "dp";
        this.view.btnMakeTransfer.top = 590 + this.view.flxTransactionDetails.frame.height + "dp";
        this.view.btnAddAnotherAccount.top = 590 + this.view.flxTransactionDetails.frame.height + 60 + "dp";
        this.view.btnSavePayee.top = 590 + this.view.flxTransactionDetails.frame.height + 120 + "dp";
        this.view.btnAddAnotherAccount.isVisible = true;
        this.view.flxTransactionDetails.left="10dp";
        this.view.flxTransactionDetails.width = this.view.flxAcknowledgementMain.width;
      } else {
        this.view.acknowledgmentModify.lblTransactionMessage.skin="sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin="sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin="sknBtnSSP0273e317px";
        this.view.flxMain.top = 120 + "dp";
        this.view.btnTransferActivity.isVisible = false;
        //this.view.btnMakeTransfer.top = 525 + "dp";
        this.view.btnAddAnotherAccount.skin = "sknBtnSSPBg0273e3Border0273e3";
        this.view.customheader.lblHeaderMobile.text = "";
      }
      if (width == 1024) {
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.AdjustScreen();
    },

      setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();   
              } 
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
      },
    onClickPrintBulk: function () {
      this.presenter.showPrintPage({ transactionList: { "details": this.view.segBill.data, "module": this.view.lblBulkBillPayAcknowledgement.text}});
    },
    onClickPrint: function () {
      var printData = [];
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.common.status"),
        value: this.view.acknowledgment.lblTransactionMessage.text
      });
      printData.push({
        key: this.view.acknowledgment.lblRefrenceNumber.text,
        value: this.view.acknowledgment.lblRefrenceNumberValue.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.transfers.accountName"), 
        value: this.view.Balance.lblSavingsAccount.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.accounts.availableBalance"),
        value: this.view.Balance.lblBalanceValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueBankName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueBankName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueCountryName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text
      });
      if(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text
        });
      }
      if(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
        });
      }
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text
      });
      if (this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text !== "") {
        printData.push({
          key: this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text,
          value: this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text
        });
      }
      var printCallback = function(){
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.navigateToAcknowledgementForm();
      }
      var viewModel = {
        moduleHeader: this.view.lblBillPayAcknowledgement.text,
        tableList: [{
            tableHeader: this.view.confirmDialogAccounts.confirmHeaders.lblHeading.text,
            tableRows: printData
        }],
        printCallback: printCallback
      }
      this.presenter.showPrintPage({
        printKeyValueGroupModel: viewModel
      });
    }
  }
});
