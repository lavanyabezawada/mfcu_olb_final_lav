define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274: function AS_Image_c57b68e2aaef4b22a4af5e88e7bfd274(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_g0db6ac9e74540de9db3c0e123f05090: function AS_Image_g0db6ac9e74540de9db3c0e123f05090(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c: function AS_TextField_f4a97ba83dad4449a5b06a9cab771b2c(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_g69571ded423405b9dc951ca48f115e1: function AS_TextField_g69571ded423405b9dc951ca48f115e1(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_e078a62143e14ffa9b70000f80946fa6: function AS_TextField_e078a62143e14ffa9b70000f80946fa6(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_c2223a86ea724db7a29054904f7e4c0e: function AS_TextField_c2223a86ea724db7a29054904f7e4c0e(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** preShow defined for frmMultiFactorAuthentication **/
    AS_Form_j032b3e7aca54e5a8342a195f4bccf4a: function AS_Form_j032b3e7aca54e5a8342a195f4bccf4a(eventobject) {
        var self = this;
        this.frmMultiFactorAuthenticationPreShow();
    },
    /** onTouchEnd defined for frmMultiFactorAuthentication **/
    AS_Form_dccf5a07e82b4b41bc2010a08fb4230f: function AS_Form_dccf5a07e82b4b41bc2010a08fb4230f(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});