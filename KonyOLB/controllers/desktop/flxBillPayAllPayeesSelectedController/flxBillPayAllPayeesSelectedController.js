define({ 

  segmentAllPayeesRowClick: function() {
     var currForm = kony.application.getCurrentForm();
     var index = currForm.tableView.segmentBillpay.selectedIndex[1];
     var data = currForm.tableView.segmentBillpay.data;
     data[index].imgDropdown = "arrow_down.png";
     data[index].imgErrorSelected = "error_yellow.png";
    data[index].lblError ="Verify payment amount and available account balance";
    if(kony.application.getCurrentBreakpoint() == 640) {
      data[index].template = "flxBillPayAllPayeesMobile";
    } else {
       data[index].template = "flxBillPayAllPayees";
    }
    
     currForm.tableView.segmentBillpay.setDataAt(data[index],index);
     this.AdjustScreen(-321);
     /*currForm.flxCustomListBoxContainerPayFrom.isVisible=false;
     currForm.flxCustomListBoxContainerCategory.isVisible=false;*/  
 },
  viewEBill: function() 
  {
     var currForm = kony.application.getCurrentForm();
     var height_to_set=140+currForm.flxContainer.frame.height;
     currForm.flxViewEbill.height =height_to_set+"dp";
     currForm.flxViewEbill.isVisible = true;
     this.AdjustScreen(30);
     currForm.forceLayout();
  },
 //UI Code
 AdjustScreen: function(data) {
  var currForm = kony.application.getCurrentForm();
  if (data !== undefined) {} else {
      data = 0;
  }
  var footerTop = currForm.flxFooter.frame.y;
  footerTop+=data;
  currForm.flxFooter.top = footerTop + "dp";
  currForm.forceLayout();

  return;

  var mainheight = 0;
  var flag = 0;
  var screenheight = kony.os.deviceInfo().screenHeight;
  mainheight = currForm.customheader.frame.height + currForm.flxContainer.frame.height;
  var diff = screenheight - mainheight;
  if (currForm.flxBillPay.frame.height <= currForm.flxrightcontainerbillpay.frame.height) {
      flag = 1;
  }
  if (flag === 0) {
      if (mainheight < screenheight) {
          diff = diff - currForm.flxFooter.frame.height;
          if (diff > 0) currForm.flxFooter.top = mainheight + diff + data+ "dp";
          else currForm.flxFooter.top = mainheight + data + "dp";
      } else {
          currForm.flxFooter.top = mainheight + data + "dp";
      }
  } else {
      if (mainheight < screenheight) {
          diff = diff - currForm.flxFooter.frame.height;
          if (diff > 0) currForm.flxFooter.top = mainheight + diff + "dp";
          else currForm.flxFooter.top = mainheight + "dp";
      } else {
          currForm.flxFooter.top = mainheight + "dp";
      }
  }
  currForm.forceLayout();
}
 });