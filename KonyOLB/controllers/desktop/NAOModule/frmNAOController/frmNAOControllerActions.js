define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** preShow defined for frmNAO **/
    AS_Form_a263266632e64a25a821b81e36653b5c: function AS_Form_a263266632e64a25a821b81e36653b5c(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for frmNAO **/
    AS_Form_a5feeee0d299419dbe3c8d44e3f504ec: function AS_Form_a5feeee0d299419dbe3c8d44e3f504ec(eventobject) {
        var self = this;
        this.postshowfrmNAO();
    },
    /** onDeviceBack defined for frmNAO **/
    AS_Form_hf198fbf60fd44588ec3db2db3905a56: function AS_Form_hf198fbf60fd44588ec3db2db3905a56(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** onTouchEnd defined for frmNAO **/
    AS_Form_eaac9043a304488196c6d746ddb1816f: function AS_Form_eaac9043a304488196c6d746ddb1816f(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});