/**
 * Description of Module representing a New Account Opening form.
 * @module frmNAOController
 */
define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility','ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility,ViewConstants) {
  var orientationHandler = new OrientationHandler();
  return /** @alias module:frmNAOController */{
    /** updates the present Form based on required function.
       * @param {uiDataMap[]} uiDataMap list of functions
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.showProgressBar) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (uiDataMap.hideProgressBar) {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (uiDataMap.campaignRes) {
        this.campaignSuccess(uiDataMap.campaignRes);
      }
      if (uiDataMap.campaignError) {
        this.view.BannerNUO.setVisibility(false);
      }
      if (uiDataMap.serverError) {
        this.showServerError(uiDataMap.serverError);
      }
      if (uiDataMap.productSelection) {
        this.showProductSelection(uiDataMap.productSelection);
      }
      if (uiDataMap.performCreditCheck) {
        this.performCreditCheck(uiDataMap.performCreditCheck);
      }
      if (uiDataMap.showAcknowledgement) {
        this.showAcknowledgement(uiDataMap.showAcknowledgement);
      }
      if (uiDataMap.resetForm) {
        this.hideAll();
      }
    },

     /**
     * Method which is called if the response is succesful.
     * @params {object} -  contains campaign data
     */
    campaignSuccess : function(data){
       if(data.length === 0)
        this.view.BannerNUO.setVisibility(false);
      else{
        this.view.BannerNUO.setVisibility(true);
       this.view.BannerNUO.imgBanner.src = data[0].imageURL;
       this.view.BannerNUO.imgBanner.onTouchStart = function(){
         kony.application.openURL(data[0].destinationURL);
      }; 
      }
      this.view.forceLayout();
	},
    /**
     * Method to load and return NAO Module.
     * @returns {object} NAO Module object.
     */
    loadNAOModule : function () {
      return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
    },
    
    /**
     * Form preShow action handler
     */
    preshow : function () {
      var scopeObj = this;
      applicationManager.getNavigationManager().applyUpdates(this);

      this.view.onBreakpointChange = function(){
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      this.setFlowActions();      
      applicationManager.getLoggerManager().setCustomMetrics(this, false, "New Account Opening");
      this.loadNAOModule().presentationController.getCampaignsNAO();
    },
    /**
     * Entry Point Method for Product Selection
     * @member frmNewUserOnBoardingController
     * @param {object} productSelectionView productSelectionView
     * @returns {void} - None
     * @throws {void} - None
     */
    showProductSelection: function (productSelectionView) {
      var self = this;
      this.showProductSelectionUI();
      this.setProductSelectionData(productSelectionView.products);
      this.validateProductSelection();
      this.configureTermsAndConditions();
      this.view.btnContinueProceed.onClick = self.showReviewApplication.bind(self,productSelectionView.products);
    },
    showReviewApplication:function(products){  
      if (kony.application.getCurrentBreakpoint() === 640) {
        this.view.customheader.lblHeaderMobile.text = "Review Application";   
      }
      else {
        this.view.customheader.lblHeaderMobile.text = "";
      }       
      this.hideAll();
      this.view.flxReviewApplication.setVisibility(true);      
      this.view.ReviewApplicationNAO.flxColorLine.setFocus(true);
      this.setUserData();
      this.setSelectedAccountSegmentData(products);
      this.view.ReviewApplicationNAO.btnEdit.onClick =  this.showSelectedProductsList;
      this.view.ReviewApplicationNAO.btnReviewApplicationSubmit.onClick = this.performCreditCheck.bind(this, this.saveProducts.bind(this, products));
      this.AdjustScreen();
    },
     /**
     * Set User's data for Review your application screen
     */
    setUserData : function(){
      var userObject = applicationManager.getUserPreferencesManager().getUserObj();
      this.view.ReviewApplicationNAO.lblProductsSelected.text = kony.i18n.getLocalizedString("i18n.NAO.ProductsSelected");  
      this.view.ReviewApplicationNAO.lblUserInfo.text = kony.i18n.getLocalizedString("i18n.NUO.UserInformation");
      this.view.ReviewApplicationNAO.lblFirstName.text = kony.i18n.getLocalizedString("i18n.NUO.FirstName");
      this.view.ReviewApplicationNAO.lblFirstNameValue.text = userObject.userfirstname ;
      this.view.ReviewApplicationNAO.lblLastName.text = kony.i18n.getLocalizedString("i18n.login.Lastname");
      this.view.ReviewApplicationNAO.lblLastNameValue.text = userObject.userlastname ;
      this.view.ReviewApplicationNAO.lblDOB.text = kony.i18n.getLocalizedString("i18n.Enroll.DOB");
      this.view.ReviewApplicationNAO.lblDOBValue.text = CommonUtilities.getFrontendDateString(userObject.dateOfBirth, "YYYY-MM-DD") ;
      if(userObject.gender){
        this.view.ReviewApplicationNAO.lblGender.isVisible = true;
        this.view.ReviewApplicationNAO.lblGenderValue.isVisible = true;
        this.view.ReviewApplicationNAO.lblGender.text = kony.i18n.getLocalizedString("i18n.NAO.Gender");
        this.view.ReviewApplicationNAO.lblGenderValue.text = userObject.gender ;
      }
      else{
        this.view.ReviewApplicationNAO.lblGender.isVisible = false;
        this.view.ReviewApplicationNAO.lblGenderValue.isVisible = false;
      }
      var addressLine1 = (userObject.addressLine1 && userObject.addressLine1 !== "" && userObject.addressLine1 !== null) ? userObject.addressLine1 : "";
      var addressLine2 = (userObject.addressLine2 && userObject.addressLine2 !== "" && userObject.addressLine2 !== null) ? userObject.addressLine2 : "";
      var city = (userObject.city && userObject.city !== "" && userObject.city !== null) ? userObject.city : "";
      var state = (userObject.state && userObject.state !== "" && userObject.state !== null) ? userObject.state : "";
      var country = (userObject.country && userObject.country !== "" && userObject.country !== null) ? userObject.country : "";
      var zipCode = (userObject.zipCode && userObject.zipCode !== "" && userObject.zipCode !== null) ? userObject.zipCode : "";
      this.view.ReviewApplicationNAO.lblAddress.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Address");
      this.view.ReviewApplicationNAO.rtxAddress.text = this.getAddress([addressLine1,addressLine2,city,state,country,zipCode]);
      this.view.ReviewApplicationNAO.lblSSN.text = kony.i18n.getLocalizedString("i18n.common.SSN");
      this.view.ReviewApplicationNAO.lblSSNKey.text = kony.i18n.getLocalizedString("i18n.Enroll.SSN");
      this.view.ReviewApplicationNAO.lblSSNValue.text = userObject.ssn ;   
    },

    getAddress: function (fields) {
      return fields.reduce(function (fullAddress, field) {
          if (field) {
            return fullAddress + " " + field;
          }
          else {
              return fullAddress;
          }
      }, "").trim();
    },

     /**
     * Set selected products data for Review your application screen
     */
    setSelectedAccountSegmentData: function(products){
      var selectedProductObjects = this.returnSelectedProductList(products);
      var widgetDataMap ={
        "imgSelectedAccount":"imgSelectedAccount",
        "lblSelectedAccountName":"lblSelectedAccountName",
        "lblSelectedAccountDescription":"lblSelectedAccountDescription"
      };
      var data = selectedProductObjects.map(function(product){
        return {
          "imgSelectedAccount": ViewConstants.IMAGES.NUO_SAVINGS,
          "lblSelectedAccountName": product.productName,
          "lblSelectedAccountDescription": product.productDescription
        };
      });
      this.view.ReviewApplicationNAO.segProductsSelected.widgetDataMap = widgetDataMap;
      if(kony.application.getCurrentBreakpoint() === 640){
        for(var i=0;i<data.length;i++){
          data[i].template = "flxNAOProductsSelectedMobile";
        }
      }
      this.view.ReviewApplicationNAO.segProductsSelected.setData(data);
    },
    /**
     * set the terms and conditions
     */
    configureTermsAndConditions: function () {
      var self = this;
      FormControllerUtility.setCheckboxState(false, this.view.imgTCContentsCheckbox);
      self.view.flxTCContentsCheckbox.onClick = function () {
        FormControllerUtility.toggleCheckbox(self.view.imgTCContentsCheckbox);
        self.validateProductSelection();
      };
      self.view.CopyflxTCCheckBox0h2854eb77a214b.onClick = function () {
        FormControllerUtility.toggleCheckbox(self.view.CopyimgTCContentsCheckbox0h9889054d45745);
      };
      self.view.btnTermsAndConditions.onClick = this.showTermsAndConditions.bind(this);
      self.view.btnCancel.onClick = self.hideTermsAndConditions.bind(this);
      self.view.flxClose.onClick = self.hideTermsAndConditions.bind(this);
      self.view.btnSave.onClick = function () {
        FormControllerUtility.setCheckboxState(FormControllerUtility.isCheckboxChecked(self.view.CopyimgTCContentsCheckbox0h9889054d45745), self.view.imgTCContentsCheckbox);
        self.view.flxTermsAndConditions.setVisibility(false);
        self.validateProductSelection();
      }
    },

    /**
     * used to set the terms and conditons
     */
    showTermsAndConditions: function () {
      FormControllerUtility.setCheckboxState(FormControllerUtility.isCheckboxChecked(this.view.imgTCContentsCheckbox), this.view.CopyimgTCContentsCheckbox0h9889054d45745);
      this.view.flxTermsAndConditions.setVisibility(true);
      this.view.lblTermsAndConditions.setFocus(true);
      this.view.flxTermsAndConditions.height = this.getTotalFormHeight();
      this.view.forceLayout();
    },

    /**
     * used to hide the terms and conditions
     */
    hideTermsAndConditions: function () {
      this.view.flxTermsAndConditions.setVisibility(false);
      this.view.forceLayout();
    },

    /** 
     * get the total form height
     * @returns {string} height
    */
    getTotalFormHeight: function () {
      return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height + "dp";
    },

    /**
    * save the selected products
    * @param {object} productList list of products
    */
    saveProducts: function (productList) {
      var selectedProductObjects = this.returnSelectedProductList(productList);
      this.loadNAOModule().presentationController.saveUserProducts(selectedProductObjects);
    },
    /**
     * return selected product list
     */
    returnSelectedProductList:function(productList){
      var selectedProductObjects = [];
      this.view.segProductsNAO.data.forEach(function (segementDataItem, index) {
          if (segementDataItem.lblCheckBox.text === "C") {
          selectedProductObjects.push(productList[index])
        }
      });
      return selectedProductObjects;
    },
    /**
     * used to show the product selection UI 
     */
    showProductSelectionUI: function () {
      this.hideAll();
      this.view.flxAccounts.setVisibility(false);
      this.view.flxMainContainer.setVisibility(true);
      this.view.flxAccountsView.setVisibility(true);
      this.view.flxMainRight.top="72dp";
      if (kony.application.getCurrentBreakpoint() === 640) {
        this.view.customheader.lblHeaderMobile.text = "Select Products";
      }
      else {
        this.view.customheader.lblHeaderMobile.text = "";

      }
      this.AdjustScreen();
    },

    /**
     * Binds Products to Product Segment
     * @member frmNewUserOnBoardingController
     * @param {object} products model objects array
     * @returns {void} - None
     * @throws {void} - None
     */
    setProductSelectionData: function (products) {
      var self = this;
      var widgetDataMap = {
        imgflxAccounts: "imgflxAccounts",
        lblAccountsName: "lblAccountsName",
        lblFeature: "lblFeature",
        flxRule1: "flxRule1",
        lblRule1: "lblRule1",
        flxRule2: "flxRule2",
        imgRule1: "imgRule1",
        imgRule2: "imgRule2",
        imgRule3: "imgRule3",
        lblRule2: "lblRule2",
        flxRule3: "flxRule3",
        lblRule3: "lblRule3",
        lblMonthlyServiceFee: "lblMonthlyServiceFee",
        lblCheckBox: "lblCheckBox",
        lblAccounts: "lblAccounts",
        btnKnowMore: "btnKnowMore",
        flxCheckBox: "flxCheckBox",
      };
      var data = products.map(function (product, index) {
        return {
          imgflxAccounts: ViewConstants.IMAGES.NUO_SAVINGS,
          lblAccountsName: product.productName,
          lblFeature: kony.i18n.getLocalizedString("i18n.NUO.Features"),
          "lblRule1": product.productDescription,
          "lblRule2": product.features,
          "lblRule3": product.info,
          imgRule1: ViewConstants.IMAGES.PAGEOFFDOT,
          imgRule2: ViewConstants.IMAGES.PAGEOFFDOT,
          imgRule3: ViewConstants.IMAGES.PAGEOFFDOT,
          lblMonthlyServiceFee: product.rates,
          flxRule1: {
            isVisible: !!product.productDescription
          },
          "template":"flxSelectProducts",
          btnKnowMore: {
            text: kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
            onClick: self.showProductDetail.bind(this, product, index)
          },
          lblAccounts: kony.i18n.getLocalizedString("i18n.NUO.SlectProduct"),
          lblCheckBox: {"text": "D", "skin": "sknlblOLBFontsE3E3E320pxOlbFontIcons"},
          flxCheckBox: {
            onClick: self.toggleProductSelectionCheckbox.bind(this, index)
          },
        }
      });
      
      this.view.segProductsNAO.widgetDataMap = widgetDataMap;
      if(kony.application.getCurrentBreakpoint() === 640){
               for(var i=0;i<data.length;i++){
		        data[i].template = "flxSelectProductsMobile";
               }
       }
      this.view.segProductsNAO.setData(data);
      this.AdjustScreen();
    },

    /**
     * Show Product Detail
     * @param {object} product NewUserProduct Model Object
     * @param {string} index of which row is shown in segment
     */
    showProductDetail: function (product, index) {
      var scope = this;
      this.showProductDetailUI();
      var data = [
        product.productDescription,
        product.features,
        product.info,
        product.rates
      ];
	  //var break_point = kony.application.getCurrentBreakpoint();
      var widgetDataMap = {
        lblRule1: "lblRule1",
        imgRule1: "imgRule1",
      };

      var segData = data.filter(function(text) {
        return !!text;
      }).map(function (text) {
        return {
          lblRule1: text,
          imgRule1: ViewConstants.IMAGES.PAGEOFFDOT,
        };
      });
      this.view.AccountsDescription.lblAccountName.text = product.productName;
      this.view.AccountsDescription.segFeatures.setData(segData);
      this.view.AccountsDescription.imgflxAccountsDescription.src = ViewConstants.IMAGES.NUO_SAVINGS;
      this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function () {
        this.showProductSelectionUI();
      }.bind(this);
      this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function () {
        this.toggleProductSelectionCheckbox(index);
        this.showProductSelectionUI();
      }.bind(this);
      this.view.AccountsDescription.imgflxAccountsDescription.onDownloadComplete = function () {
        // Pushing Adjust Screen to end of the stack. This screen needs to be adjusted after image gets rendered
        setTimeout(function () {
          scope.AdjustScreen();
        }, 0);
      }
      this.AdjustScreen();
    },

    /**
     *use to validate the product 
     */
    validateProductSelection: function () {
      var count = this.view.segProductsNAO.data.filter(function (item) {
        return item.lblCheckBox.text === "C";
      }).length;
      if (count === 0 || !FormControllerUtility.isCheckboxChecked(this.view.imgTCContentsCheckbox)) {
        FormControllerUtility.disableButton(this.view.btnContinueProceed);
      }
      else {
        FormControllerUtility.enableButton(this.view.btnContinueProceed);
      }
    },

    /**
     * Show Product Detail UI
     * @param {object} product Product Model Object
     */
    showProductDetailUI: function (product) {
      //this.view.flxMainContainer.setVisibility(false);
      this.view.flxAccountsView.setVisibility(false);
      this.view.flxAccounts.setVisibility(true);
      this.view.flxMainRight.top="60dp";
      this.AdjustScreen();
    },

    /**
     * Toggle Select Product Checkbox of Select Products Segment
     * @param {object}  index of row
     */
    toggleProductSelectionCheckbox: function (index) {
      var data = this.view.segProductsNAO.data[index];
      data.lblCheckBox.text = data.lblCheckBox.text === "D" ? "C" : "D";
      if (data.lblCheckBox.text === "D") {
          data.lblCheckBox.skin = "sknlblOLBFontsE3E3E320pxOlbFontIcons";
      } else {
          data.lblCheckBox.skin = "sknlblOLBFonts0273E420pxOlbFontIcons";
      }
      this.view.segProductsNAO.setDataAt(data, index);
      this.validateProductSelection();
    },

    /**
     * used to select the perform the credit check
     * @param {object} onYesPressed onYesPressed
     */
    performCreditCheck: function (onYesPressed) {
      this.view.flxSubmitApplication.setVisibility(true);
      this.view.SubmitApplication.lblPopupMessage.setFocus(true);
      this.view.SubmitApplication.setFocus(true);
      this.view.SubmitApplication.flxCross.onClick = function () {
        this.view.flxSubmitApplication.setVisibility(false);
      }.bind(this);
      this.view.SubmitApplication.btnNo.onClick = function () {
        this.view.flxSubmitApplication.setVisibility(false);
      }.bind(this);
      this.view.SubmitApplication.btnYes.onClick = function () {
        this.view.flxSubmitApplication.setVisibility(false);
        onYesPressed();
      }.bind(this);
      this.view.flxSubmitApplication.height = this.getTotalFormHeight();
      this.view.forceLayout();
    },

    /**
     * shows the acknowledgement scrren
     * @param {object} acknowledgementView acknowledgementViewModel
     */
    showAcknowledgement: function (acknowledgementView) {
      this.showAcknowledgementUI();
      this.setSelectedProductsData(acknowledgementView.selectedProducts)
      this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function () {
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
      }
      if(kony.application.getCurrentBreakpoint()==640){
        this.view.customheader.lblHeaderMobile.text = "Acknowledgement";
        this.view.customheader.forceLayout();
      }
      else{
        this.view.customheader.lblHeaderMobile.text = "";
      }
    },

    /**
     * used to set the selcted products
     * @param {object} selectedProducts selected Products
     */
    setSelectedProductsData: function (selectedProducts) {
      var widgetDataMap = {
        "flxSelectedProduct2": "flxSelectedProduct2",
        "imgSelectedProduct1": "imgSelectedProduct1",
        "lblSelectedProductName1": "lblSelectedProductName1",
        "lblSelectedDescriptonName1": "lblSelectedDescriptonName1",
        "imgSelectedProduct2": "imgSelectedProduct2",
        "lblSelectedProductName2": "lblSelectedProductName2",
        "lblSelectedDescriptionName2": "lblSelectedDescriptionName2",
      };
      this.view.AcknowledgementNAO.segProducts.widgetDataMap = widgetDataMap;
      var data = [];
      for (var i = 0; i < selectedProducts.length; i = i + 2) {
        var productLeft = selectedProducts[i];
        var productRight = selectedProducts[i + 1];
        var item = {
          "imgSelectedProduct1": ViewConstants.IMAGES.NUO_SAVINGS,
          "lblSelectedProductName1": productLeft.productName,
          "lblSelectedDescriptonName1": kony.i18n.getLocalizedString("i18n.nao.accountActive"),
          "template":"flxNAOProductsAck"
        }
        if (productRight) {
          item.imgSelectedProduct2 = ViewConstants.IMAGES.NUO_SAVINGS;
          item.lblSelectedProductName2 = productRight.productName;
          item.flxSelectedProduct2 = {
            isVisible: true
          }
          item.lblSelectedDescriptionName2 = kony.i18n.getLocalizedString("i18n.nao.accountActive")

        }
        data.push(item);
      }
      
      if(kony.application.getCurrentBreakpoint() === 640){
               for(var idata=0;idata<data.length;idata++){
		        data[idata].template = "flxNAOProductsAckMobile";
               }
            };
      this.view.AcknowledgementNAO.segProducts.setData(data);
      this.AdjustScreen();

    },

    /**
     * shows the AcknowledgementUI
     */
    showAcknowledgementUI: function () {
      this.hideAll();
      this.view.flxNAOAcknowledgement.setVisibility(true);
      this.view.flxMainRight.top="76dp";
      this.view.AcknowledgementNAO.btnAckGoToAccounts.setVisibility(false);
      this.view.AcknowledgementNAO.btnAckFundAccounts.text = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
      this.view.AcknowledgementNAO.btnAckFundAccounts.toolTip = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
      this.AdjustScreen();
    },

    /**
     * hides the all UI
     */
    hideAll: function () {
      this.view.flxFundAccounts.setVisibility(false);
      this.view.flxCreditCheck.setVisibility(false);
      this.view.flxNAOAcknowledgement.setVisibility(false);
      this.view.flxReviewApplication.setVisibility(false);
      this.view.flxAccountsView.setVisibility(false);
    },
    /**
     * post show of the form
     */
    postshowfrmNAO: function () {
      if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxPleaseNoteTheFollowingPoints.left = "2%";
                this.view.flxPleaseNoteTheFollowingPoints.width = "96%";
                this.view.lblThisServicesIsChargeble.width = "80%";
            }
     else if (kony.application.getCurrentBreakpoint() === 1024){
              this.view.flxPleaseNoteTheFollowingPoints.centerX = "50%";
              this.view.flxPleaseNoteTheFollowingPoints.width = "92%";
              this.view.lblThisServicesIsChargeble.width = "87.4%";
           }
      else{
                this.view.flxPleaseNoteTheFollowingPoints.centerX = "50%";
                this.view.flxPleaseNoteTheFollowingPoints.width = "92.5%";
                this.view.lblThisServicesIsChargeble.width = "88%";
          }
      this.view.customheader.imgKony.setFocus(true);
      this.view.customheader.customhamburger.activateMenu("Accounts", "Open New Account");
      this.view.flxHeaderSelectProducts.zIndex = "22"
      var scopeObj = this;  
      this.view.AcknowledgementNAO.segProducts.setData([]);
      var config = applicationManager.getConfigurationManager();
      this.view.customheader.headermenu.flxMessages.setVisibility(true);
      
      if (CommonUtilities.isCSRMode()) {
        this.view.flxMainRight.flxRightContainer.flxRightBarContactDetails.flxEmailUsAt.onClick = CommonUtilities.disableButtonActionForCSRMode();
        //this.view.customheader.flxEmailUsAt.onClick.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
    } else {
        //write a new message
		        this.view.flxMainRight.flxRightContainer.flxRightBarContactDetails.flxEmailUsAt.onClick = this.loadNAOModule().presentationController.newMessage.bind(this.loadNAOModule().presentationController);
    }
      
      this.AdjustScreen();
    },
    /**
     * used to set the actions to the  NAO On clicks
     */
    setFlowActions: function () {
      var self = this;
      this.view.onBreakpointChange = function(){
        self.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      this.view.SubmitApplication.flxCross.onClick = function () {
        self.view.flxSubmitApplication.setVisibility(false);
      };
      this.view.SubmitApplication.btnNo.onClick = function () {
        self.view.flxSubmitApplication.setVisibility(false);
      };
      this.view.ReviewApplicationNAO.btnBack.onClick = this.showSelectedProductsList;
      this.view.SubmitApplication.btnYes.onClick = function () {
        self.view.flxSubmitApplication.setVisibility(false);
        self.hideAll();
        self.view.flxNAOAcknowledgement.setVisibility(true);
        self.view.flxMainRight.top="76dp";
      };
      this.view.FundAccountsNAO.btnFundAccountGoToAccounts.onClick = this.NavigateToAccountsLandingForm;
      this.view.AcknowledgementNAO.btnAckGoToAccounts.onClick = this.NavigateToAccountsLandingForm;
      this.view.FundAccountsNAO.btnFundAccountProceed.onClick = function () {
        applicationManager.getNavigationManager().navigateTo("frmTransfers");
      };
      this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function () {
        self.hideAll();
        self.view.flxFundAccounts.setVisibility(true);
      };
      this.view.FundAccountsNAO.flxFundAccountRadiobtn1.onClick = function () {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn2,
          self.view.FundAccountsNAO.lblRadioBtn3
        );
      };
      this.view.FundAccountsNAO.flxFundAccountRadioBtn2.onClick = function () {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn2,
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn3
        );
      };
      this.view.FundAccountsNAO.flxFundAccountRadioBtn3.onClick = function () {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn3,
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn1
        );
      };
      this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = this.showSelectedProductsList;
      this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = this.showSelectedProductsList;
    },
    /**
     * set the Ui Activitities
     * @param {object} RadioBtn1 RadioBtn1
     * @param {object}  RadioBtn2 RadioBtn2
     * @param {object} RadioBtn3 RadioBtn3
     */
    RadioBtnAction: function (RadioBtn1, RadioBtn2, RadioBtn3) {
      if (RadioBtn1.text === "R") {
        RadioBtn1.text = " ";
        RadioBtn1.skin =  ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
        if (RadioBtn2 != " " || RadioBtn2 !== null) {
          RadioBtn2.text = "R";
          RadioBtn2.skin = ViewConstants.SKINS.RADIO_BUTTON_SELECTED;
        }
        if (RadioBtn3 != " " || RadioBtn3 !== null) {
          RadioBtn3.text = "R";
          RadioBtn3.skin = ViewConstants.SKINS.RADIO_BUTTON_SELECTED;
        }
      } else {
        if (RadioBtn2 != " " || RadioBtn2 !== null) {
          RadioBtn2.text = " ";
          RadioBtn2.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
        }
        if (RadioBtn3 != " " || RadioBtn3 !== null) {
          RadioBtn3.text = " ";
          RadioBtn3.skin = ViewConstants.SKINS.RADIO_BUTTON_UNSELECTED;
        }
        RadioBtn1.text = "R";
        RadioBtn1.skin = ViewConstants.SKINS.RADIO_BUTTON_SELECTED;
      }
    },

    /**
     * used to navigate the account landing form
     */
    NavigateToAccountsLandingForm: function () {
      applicationManager.getNavigationManager().navigateTo("frmAccountsLanding");
    },

    /**
     * shows the selcted product list
     */
    showSelectedProductsList: function () {
      this.hideAll();
      this.view.flxAccounts.setVisibility(false);
      this.view.flxAccountsView.setVisibility(true);
      this.view.lblSelectProduct.setFocus(true);
      this.AdjustScreen();
    },
    
    
   
    /**
     *AdjustScreen- function to adjust the footer
     */
    AdjustScreen: function () {
      this.view.forceLayout();
       var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
       mainheight =
         this.view.customheader.frame.height +
         this.view.flxContainer.frame.height;
       var diff = screenheight - mainheight;
       if (mainheight < screenheight) {
         diff = diff - this.view.flxFooter.frame.height;
         if (diff > 0) {
           this.view.flxFooter.top = mainheight + diff + "dp";
         }
         else { this.view.flxFooter.top = mainheight + "dp"; }
       } else {
         this.view.flxFooter.top = mainheight + "dp";
       }
       this.view.forceLayout();
     },
    

    
 /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmNAOController}
         * @param {integer} width - current browser width
         * @return {} 
         * @throws {}
         */
        onBreakpointChange: function(width) {
          var data;
          var selectedData;
            this.view.flxFooter.isVisible=true;
          this.view.customheader.forceCloseHamburger();
          orientationHandler.onOrientationChange(this.onBreakpointChange);
          this.loadNAOModule().presentationController.getCampaignsNAO();
          this.view.customheader.onBreakpointChange(width);
          this.setupFormOnTouchEnd(width);
            kony.print('on breakpoint change');
            var scope = this;
            var responsiveFonts = new ResponsiveFonts();
//             var mobileTemplates = {
//                 "flxSelectProducts": "flxSelectProductsMobile",
//             };
//             var desktopTemplates = {
//                 "flxSelectProductsMobile": "flxSelectProducts",
//             };
            if (width === 640) {
                data = this.view.segProductsNAO.data;
                if (data === undefined) return;
                data.map(function(e) {
                    e.template = "flxSelectProductsMobile";
                });
                this.view.segProductsNAO.setData(data);
                selectedData = this.view.ReviewApplicationNAO.segProductsSelected.data;
                if (selectedData === undefined) return;
                selectedData.map(function(e) {
                    e.template = "flxNAOProductsSelectedMobile";
                });
                this.view.segProductsNAO.setData(selectedData);

            } else {
                data = this.view.segProductsNAO.data;
                if (data === undefined) return;
                data.map(function(e) {
                  e.template = "flxSelectProducts";
                });
                this.view.segProductsNAO.setData(data);
                selectedData = this.view.ReviewApplicationNAO.segProductsSelected.data;
                if (selectedData === undefined) return;
                selectedData.map(function(e) {
                    e.template = "flxNAOProductsSelected";
                });
                this.view.segProductsNAO.setData(selectedData);
            }
          	this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width){
          if(width==640){
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }else{
            if(width==1024){
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }else{
                this.view.onTouchEnd = function(){
                  hidePopups();   
                } 
              }
              var userAgent = kony.os.deviceInfo().userAgent;
              if (userAgent.indexOf("iPad") != -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                this.view.onTouchEnd = function(){}
                this.nullifyPopupOnTouchStart();
              }
          }
        },
        nullifyPopupOnTouchStart: function(){
        }    
    
  };
});
