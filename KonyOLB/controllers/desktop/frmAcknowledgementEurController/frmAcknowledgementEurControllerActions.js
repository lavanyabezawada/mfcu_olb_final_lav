define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_de438d1b048e4ee2b6e9b5e124997baa: function AS_FlexContainer_de438d1b048e4ee2b6e9b5e124997baa(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_df5673836c98456391c89b3dfcd7e40d: function AS_FlexContainer_df5673836c98456391c89b3dfcd7e40d(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d63b08cee0fe4419a32c9f99d59be9ec: function AS_FlexContainer_d63b08cee0fe4419a32c9f99d59be9ec(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_bbe7f442fa3e4ee78fc88bb338a181de: function AS_FlexContainer_bbe7f442fa3e4ee78fc88bb338a181de(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_a77ddd80054c4f9d823850848e4433a3: function AS_Button_a77ddd80054c4f9d823850848e4433a3(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_id3a9aedc80240929eac93621fadd034: function AS_Button_id3a9aedc80240929eac93621fadd034(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_f9e75644b77f4f1e85bb588c6cf28647: function AS_Button_f9e75644b77f4f1e85bb588c6cf28647(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_cd500a3ec2fe4079ac5c0b0d34cb6525: function AS_Button_cd500a3ec2fe4079ac5c0b0d34cb6525(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_j992f543e7d0434ca5a9043650c5f06c: function AS_Button_j992f543e7d0434ca5a9043650c5f06c(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_eb2b676acc51438db2456505c6dce70a: function AS_Button_eb2b676acc51438db2456505c6dce70a(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_eb622be3e70e4ea6ba9ef8c206eba7b4: function AS_Button_eb622be3e70e4ea6ba9ef8c206eba7b4(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_c304459a74d6458ea53c943441680d97: function AS_Button_c304459a74d6458ea53c943441680d97(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_d52247fd05924bf1a59643839c4f092d: function AS_Button_d52247fd05924bf1a59643839c4f092d(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_jb0eb7e7905d4e9993107ebeb999ca4d: function AS_Button_jb0eb7e7905d4e9993107ebeb999ca4d(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_b499b731c5b547f193490cf851381593: function AS_Button_b499b731c5b547f193490cf851381593(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_i5a51a26b7b04cedb7e6fba7c3bd2b83: function AS_Button_i5a51a26b7b04cedb7e6fba7c3bd2b83(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmAcknowledgementEur **/
    AS_Form_g93759e4fdd9461192cd2189559d280c: function AS_Form_g93759e4fdd9461192cd2189559d280c(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmAcknowledgementEur **/
    AS_Form_hd7fde64b4674e44a2992a736c086459: function AS_Form_hd7fde64b4674e44a2992a736c086459(eventobject) {
        var self = this;
        this.postShowFrmAcknowledgement();
    },
    /** onDeviceBack defined for frmAcknowledgementEur **/
    AS_Form_f6abdae0c588417cb686dec20bc562c0: function AS_Form_f6abdae0c588417cb686dec20bc562c0(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAcknowledgementEur **/
    AS_Form_gb6a44f9a91c4bf79746466cdc2dbe07: function AS_Form_gb6a44f9a91c4bf79746466cdc2dbe07(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onClick defined for flxInfo **/
    AS_FlexContainer_c5d8a3f31d7d40b094e8fb476a9cd727: function AS_FlexContainer_c5d8a3f31d7d40b094e8fb476a9cd727(eventobject, context) {
        var self = this;
    }
});