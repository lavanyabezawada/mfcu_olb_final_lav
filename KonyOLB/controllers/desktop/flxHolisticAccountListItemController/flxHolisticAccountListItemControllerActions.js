define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxFavourite **/
    AS_FlexContainer_e51eaa51beba43b5bd1a29e9ac5a1b8e: function AS_FlexContainer_e51eaa51beba43b5bd1a29e9ac5a1b8e(eventobject, context) {
        var self = this;
        this.imgPressed();
    },
    /** onClick defined for flxAccountName **/
    AS_FlexContainer_a21e32be49bb4eb1a2cf278137f13159: function AS_FlexContainer_a21e32be49bb4eb1a2cf278137f13159(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxAvailableBalance **/
    AS_FlexContainer_c329b87c42b84ae9a7dfecaa00c15551: function AS_FlexContainer_c329b87c42b84ae9a7dfecaa00c15551(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxContent **/
    AS_FlexContainer_aa6693329cbe4be58e2c71ba95f95156: function AS_FlexContainer_aa6693329cbe4be58e2c71ba95f95156(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_a3d299802f454408b40d3b11f129fe43: function AS_FlexContainer_a3d299802f454408b40d3b11f129fe43(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});