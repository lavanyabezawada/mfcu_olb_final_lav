define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** postShow defined for frmNewOpeningAccount **/
    AS_Form_de3e71354897435784619e46eff3cf13: function AS_Form_de3e71354897435784619e46eff3cf13(eventobject) {
        var self = this;
        this.postShowFrmNAO();
    }
});