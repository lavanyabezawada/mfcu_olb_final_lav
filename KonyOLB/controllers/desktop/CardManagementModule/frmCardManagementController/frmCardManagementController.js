define(['CommonUtilities', 'OLBConstants', 'ViewConstants','FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants,FormControllerUtility) {
    var orientationHandler = new OrientationHandler();
    return {

        /**
         * Globals for storing travel-notification,card-image, status-skin mappings.
         */
        notificationObject: {},
        cardImages: {},
        countries: {},
        states: {},
        cities: {},
        selectedCountry: {},
        statusSkinsLandingScreen: {},
        statusSkinsDetailsScreen: {},
        

        /**
         * Form lifecycle method.
         */
        formPreShowFunction: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function(){
              scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.view.customheader.forceCloseHamburger();
            this.initializeCards();
            this.updateHamburgerMenu();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Cards");
            this.initRightContainer();
           // this.hideAllCardManagementViews();
            this.setTravelNotificationActions();
            var dateformat = applicationManager.getFormatUtilManager().getDateFormat();
            this.view.calFrom.dateFormat = dateformat;
            this.view.calTo.dateFormat = dateformat;
            CommonUtilities.disableOldDaySelection(this.view.calFrom);
            CommonUtilities.disableOldDaySelection(this.view.calTo);
            this.view.calFrom.hidePreviousNextMonthDates = true;
            this.view.calTo.hidePreviousNextMonthDates = true;
            FormControllerUtility.disableButton(this.view.btnCardsContinue);
            this.view.flxDownload.setVisibility(false); // TODO: implementation in next sprint,currently hidding the view
            applicationManager.getNavigationManager().applyUpdates(this);            
        },

        setTravelNotificationActions: function() {
            this.showContactUsNavigation();
        },
        /**
         * initializeCards - Method to initialize the globals.
         */
        initializeCards: function() {
            this.initializeCardImages();
            this.initializeStatusSkins();
        },

        /**
         * initializeStatusSkins - Method to initialize the status skins in globals.
         */
        initializeStatusSkins: function() {
            this.statusSkinsLandingScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Inactive'] = ViewConstants.SKINS.CARDS_INACTIVE_STATUS_LANDING;
            
            this.statusSkinsDetailsScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_DETAILS;
            this.statusSkinsDetailsScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_DETAILS;
        },

        /**
         * initializeCardImages - Method to initialize the card images in globals.
         */
        initializeCardImages: function() {
            this.cardImages['My Platinum Credit Card'] = ViewConstants.IMAGES.PREMIUM_CLUB_CREDITS;
            this.cardImages['Gold Debit Card'] = ViewConstants.IMAGES.GOLDEN_CARDS;
            this.cardImages['Premium Club Credit Card'] = ViewConstants.IMAGES.PLATINUM_CARDS;
            this.cardImages['Shopping Card'] = ViewConstants.IMAGES.SHOPPING_CARDS;
            this.cardImages['Petro Card'] = ViewConstants.IMAGES.PETRO_CARDS;
            this.cardImages['Eazee Food Card'] = ViewConstants.IMAGES.EAZEE_FOOD_CARDS;
        },

        /**
         * Method to initialize the actions and tooltips for right side flex.
         */
        initRightContainer: function() {
            this.view.lblActivateNewCard.text = kony.i18n.getLocalizedString("i18n.footer.contactUs");
            this.view.lblActivateNewCard.toolTip = kony.i18n.getLocalizedString("i18n.footer.contactUs");
            this.view.lblApplyForNewCard.text = kony.i18n.getLocalizedString("i18n.CardManagement.ApplyForNewCard");
            this.view.lblApplyForNewCard.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.ApplyForNewCard");
            var infoContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('InformationContentModule');
            this.view.flxActivateNewCard.onTouchEnd = infoContentModule.presentationController.showContactUsPage.bind(infoContentModule.presentationController);
        },

        /**
         * Form lifecycle method.
         */
        PostShowfrmCardManagement: function() {
            this.view.customheader.imgKony.setFocus(true);
            var context1 = {
                "widget": this.view.calFrom,
                "anchor": "bottom"
            };
            this.view.calFrom.setContext(context1);
            context1 = {
                "widget": this.view.calTo,
                "anchor": "bottom"
            };
            this.view.calTo.setContext(context1);
            context1 = {
                "widget": this.view.CardLockVerificationStep.calFrom1,
                "anchor": "bottom"
            };
            this.view.CardLockVerificationStep.calFrom1.setContext(context1);
            context1 = {
                "widget": this.view.CardLockVerificationStep.calTo1,
                "anchor": "bottom"
            };
            this.view.CardLockVerificationStep.calTo1.setContext(context1);
            this.AdjustScreen();
        },
        
        /**
         * showIncorrectSecureAnswersFlex: This function enables the incorrect security answers flex.
         */
        showIncorrectSecurityAnswersFlex: function() {
            CommonUtilities.hideProgressBar(this.view);
            this.view.CardLockVerificationStep.flxWarningMessage.setVisibility(true);
            this.view.CardLockVerificationStep.lblWarning.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecurityQuestionsDoesNotMatch");
            this.view.forceLayout();
        },

        /**
         * showSecurityQuestionsScreen - Shows the screen where user can enter secure access code and verify.
         * @param {Array} - Security Questions array.
         * @param {params} contains the params object.
         * @param {String} action - contains the action to be performed.
         */
        showSecurityQuestionsScreen: function(securityQuestions, params, action) {
            var self = this;
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.LockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardSecurityQuestions"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin") || action === "Offline_Change_Pin") {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinSecurityQuestions"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardSecurityQuestions"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardSecurityQuestions"));
            } else if (action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardSecurityQuestions"));
            }else if(action === kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard")){
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelVerification"));
            }
            this.view.CardLockVerificationStep.flxWarningMessage.setVisibility(false);
            FormControllerUtility.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyBySecurityQuestions.setVisibility(true);
            this.view.CardLockVerificationStep.lblAnswerSecurityQuestion1.text = securityQuestions[0].Question;
            this.view.CardLockVerificationStep.lblAnswerSecurityQuestion2.text = securityQuestions[1].Question;
            this.view.CardLockVerificationStep.tbxAnswers1.text = "";
            this.view.CardLockVerificationStep.tbxAnswers2.text = "";
            this.view.forceLayout();
            this.view.CardLockVerificationStep.tbxAnswers1.onKeyUp = function() {
                if (self.view.CardLockVerificationStep.tbxAnswers1.text === "" || self.view.CardLockVerificationStep.tbxAnswers2.text === "" || CommonUtilities.isCSRMode()) {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
            };
            this.view.CardLockVerificationStep.tbxAnswers2.onKeyUp = function() {
                if (self.view.CardLockVerificationStep.tbxAnswers1.text === "" || self.view.CardLockVerificationStep.tbxAnswers2.text === "" || CommonUtilities.isCSRMode()) {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
            };
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")
                },
                'btnModify': {
                    'isVisible': false,
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = this.showMFAScreen.bind(this, params, action);
            if (CommonUtilities.isCSRMode()) {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
            } else {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                    self.view.CardLockVerificationStep.flxWarningMessage.setVisibility(false);
                    var questionAnswers = [];
                    questionAnswers.push({
                        'questionId': securityQuestions[0].SecurityQuestion_id,
                        'customerAnswer': self.view.CardLockVerificationStep.tbxAnswers1.text
                    }, {
                        'questionId': securityQuestions[1].SecurityQuestion_id,
                        'customerAnswer': self.view.CardLockVerificationStep.tbxAnswers2.text
                    });
                    params.questionAnswers = questionAnswers;
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.verifySecurityQuestionAnswers(params, action);
                };
            }
            CommonUtilities.hideProgressBar(self.view);
        },
        
        
        getSelectedAddressId: function() {
            var i = 0;
            var addresses = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchUserAddresses();
            if (this.view.CardLockVerificationStep.lblAddressCheckBox1.text === "M") i = 0;
            if (this.view.CardLockVerificationStep.lblAddressCheckBox2.text === "M") i = 1;
            if (this.view.CardLockVerificationStep.lblAddressCheckBox3.text === "M") i = 2;
            return addresses[i].addressId;
            //return kony.mvc.MDAApplication.getSharedInstance().appContext.address[i].Address_id;
        },
        
        
          
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.cards) {
                this.showCards();
                //this.setCardsData(this.constructCardsViewModel(viewPropertiesMap.cards));
            }
            if (viewPropertiesMap.secureAccessCode) {
                this.showSecureAccessCodeScreen(viewPropertiesMap.params, viewPropertiesMap.action);
            }
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.sideMenu) {
                this.updateHamburgerMenu(viewPropertiesMap.sideMenu);
            }
            if (viewPropertiesMap.incorrectSecureAccessCode) {
                this.showIncorrectSecureAccessCodeFlex();
            }
            if (viewPropertiesMap.travelNotificationsList) {
                this.showTravelNotifications(viewPropertiesMap.travelNotificationsList.TravelRequests);
            }
            if (viewPropertiesMap.actionAcknowledgement) {
                this.showCardOperationAcknowledgement(viewPropertiesMap.card, viewPropertiesMap.actionAcknowledgement);
            }
            if (viewPropertiesMap.AddNewTravelPlan) {
                this.showAddNewTravelPlan(viewPropertiesMap);
            }
            if (viewPropertiesMap.notificationAcknowledgement) {
                this.showTravelNotificationAcknowledgement(viewPropertiesMap.notificationAcknowledgement);
            }
            if (viewPropertiesMap.eligibleCards) {
                this.showSelectCardScreen(viewPropertiesMap.eligibleCards);
            }
            if (viewPropertiesMap.travelStatus) {
                this.showCards();
                this.showCardsStatus(viewPropertiesMap.travelStatus);
            }
            if (viewPropertiesMap.securityQuestions) {
                this.showSecurityQuestionsScreen(viewPropertiesMap.securityQuestions, viewPropertiesMap.card, viewPropertiesMap.action);
            }
            if (viewPropertiesMap.incorrectSecurityAnswers) {
                this.showIncorrectSecurityAnswersFlex();
            }
            if (viewPropertiesMap.notificationDeleted) {
                this.deleteNotificationSuccess();
            }
            if(viewPropertiesMap.isPrintCancelled){
                this.showAcknowledgementOnPrintCancel();
            }
        },

        /**
         * AdjustScreen - Method that sets the height of footer properly.
         */
        AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + 70+ "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + 70 + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + 70 + "dp";
      }
      this.view.forceLayout();
    },
      
        /**
         * Method that hides all other flexes except the Cards segment.
         * @param {Array} - Array of JSON objects of cards.
         */
        showCards: function(cards) {
            var self = this;
            this.hideAllCardManagementViews();
            this.showContactUsNavigation();
            this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
            this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
            this.view.flxMangeTravelPlans.onClick = self.fetchTravelNotifications;
            this.view.forceLayout();
            //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchCardsStatus(cards);
        },

        /**
         *  Method that hides all flexes in frmCardManagement.
         */
        hideAllCardManagementViews: function() {
            this.view.flxAcknowledgment.setVisibility(false);
            this.view.flxActivateCard.setVisibility(false);
            this.view.flxCardVerification.setVisibility(false);
            this.view.flxMyCardsView.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(false);
            this.view.breadcrumb.setVisibility(false);
            this.view.flxTravelPlan.setVisibility(false);
            this.view.flxConfirm.setVisibility(false);
            this.view.myCards.flxNoCardsError.setVisibility(false);
            this.view.myCards.segMyCards.setVisibility(false);
            this.view.flxEligibleCardsButtons.setVisibility(false);
        },

        /**
         *  Method to navigate to contactus.
         */
        showContactUsNavigation: function() {
            this.view.flxApplyForNewCard.setVisibility(false);
            this.view.lblActivateNewCard.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };
        },

        /**
         * Method to call function of presenter to fetch travel notifications
         */
        fetchTravelNotifications: function() {
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchTravelNotifications();
        },
      
        /**
         * This function shows the masked Secure Access Code on click of the eye icon
         * @param {Object} - The textbox widget of the Secure Access Code.  
         */
        toggleSecureAccessCodeMasking: function(widgetId) {
            widgetId.secureTextEntry = !(widgetId.secureTextEntry);
        },
      
        /**
         *Shows the screen where user can enter secure access code and verify.
         * @param {Object} - card object
         * @param {String} action - contains the action to be performed.
         */
        showSecureAccessCodeScreen: function(params, action) {
            var self = this;
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.LockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardSecureAccessCode"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin") || action === "Offline_Change_Pin") {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinSecureAccessCode"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardSecureAccessCode"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardSecureAccessCode"));
            } else if (action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardSecureAccessCode"));
            }else if(action === kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard")){
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelVerification"));
            }
            this.hideServerError();
            FormControllerUtility.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            this.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(true);
            this.view.CardLockVerificationStep.tbxCVV.text = "";
            this.view.CardLockVerificationStep.tbxCVV.secureTextEntry = false;
            this.view.CardLockVerificationStep.imgViewCVV.setVisibility(false);
            this.view.forceLayout();
            this.view.CardLockVerificationStep.imgViewCVV.onTouchStart = this.toggleSecureAccessCodeMasking.bind(this, this.view.CardLockVerificationStep.tbxCVV);
            this.view.CardLockVerificationStep.tbxCVV.onKeyUp = function() {
                if (!self.isValidSecureAccessCode(self.view.CardLockVerificationStep.tbxCVV.text) || CommonUtilities.isCSRMode()) {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
            };
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")
                },
                'btnModify': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.login.ResendOtp"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.login.ResendOtp")
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            var resendOtpTimer;
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
                clearTimeout(resendOtpTimer);
                self.view.forceLayout();
                self.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(false);
                self.showMFAScreen(params, action);
            };
            FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
            resendOtpTimer = setTimeout(
                function() {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
                }, 5000);
            this.view.CardLockVerificationStep.confirmButtons.btnModify.onClick = function() {
                FormControllerUtility.showProgressBar(self.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.sendSecureAccessCode(params, action);
                self.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(true);
                self.view.CardLockVerificationStep.lblWarningSecureAccessCode.text = kony.i18n.getLocalizedString("i18n.MFA.ResentOTPMessage");
                FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
                resendOtpTimer = setTimeout(
                    function() {
                        FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
                    }, 5000);
                self.view.forceLayout();
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                    clearTimeout(resendOtpTimer);
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
                    var enteredAccessCode = self.view.CardLockVerificationStep.tbxCVV.text;
                    params.enteredAccessCode = enteredAccessCode;
                    FormControllerUtility.showProgressBar(self.view);
                    self.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(false);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.verifySecureAccessCode(params, action);
                    self.view.forceLayout();
                };
            }
            self.view.forceLayout();
        },
      
        /**
         * This function enables the incorrect OTP flex.
         */
        showIncorrectSecureAccessCodeFlex: function() {
            CommonUtilities.hideProgressBar(this.view);
            FormControllerUtility.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnModify);
            this.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(true);
            this.view.CardLockVerificationStep.lblWarningSecureAccessCode.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecureAccessCodeDoesNotMatch");
            this.view.CardLockVerificationStep.tbxCVV.text = "";
            this.view.forceLayout();
        },
      
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            this.view.flxDowntimeWarning.setVisibility(true);
            if(errorMsg.errorMessage && errorMsg.errorMessage!=""){
                this.view.rtxDowntimeWarning.text = errorMsg.errorMessage;
            }else{
                this.view.rtxDowntimeWarning.text = errorMsg.serverErrorRes.dbpErrMsg;
            }
          this.view.CardLockVerificationStep.confirmButtons.btnModify.setVisibility(false);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.left = '53.41%';
            this.view.CardLockVerificationStep.confirmButtons.height = "100dp";
            this.view.rtxDowntimeWarning.setFocus(true);
            this.AdjustScreen();
        },

        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
      
        /**
         * Method that updates Hamburger Menu.
         * @param {Object} sideMenuModel - contains the side menu view model.
         */
        updateHamburgerMenu: function() {
            this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "Card Management");
        },
      
        /**
         * Method to navigate to travel notifications screen
         * @param {Object} - travelNotifications object
         */
        showTravelNotifications: function(travelNotifications) {
            var self = this;
            this.hideAllCardManagementViews();
            this.view.flxMyCardsView.setVisibility(true);
            this.view.flxTravelPlan.setVisibility(false);
            this.view.flxMyCards.setVisibility(true);
            this.view.flxRightBar.setVisibility(true);
            this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
            this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
            this.view.flxMangeTravelPlans.onClick = self.navigateToAddTravelNotification.bind(this);
            this.setBreadCrumbAndHeaderDataTravelPlan()
            this.setTravelNotificationsData(travelNotifications);
        },
        /**
         * Method to navigate to creat travel notifications screen
         */
        navigateToAddTravelNotification: function() {
            var self = this;
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.AddNewTravelPlan();
          this.view.lblManageTravelPlans.tooltip=kony.i18n.getLocalizedString("i18n.CardManagement.AddNewTravelPlan");
        },
      
        /**
         * Method to set breadcrumb and header data for Travel plan
         */
        setBreadCrumbAndHeaderDataTravelPlan: function() {
            var self = this;
            this.view.breadcrumb.setVisibility(false);
            this.view.breadcrumb.setBreadcrumbData([{
                'text': kony.i18n.getLocalizedString("i18n.CardManagement.MyCards"),
                'callback': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchCardsList.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController)
            }, {
                'text': kony.i18n.getLocalizedString("i18n.CardManagement.ManageTravelPlans"),
                'callback': null
            }]);
            this.view.myCards.lblMyCardsHeader.text = kony.i18n.getLocalizedString("i18n.CardManagement.ManageTravelPlans");
        },

        /**
         * Method to set travel notifications data.
         */
        setTravelNotificationsData: function(travelNotifications) {
            var self = this;
            if (travelNotifications == undefined || travelNotifications.lengh == 0) {
                self.showNoTravelNotificationScreen();
            } else {
                self.view.myCards.segMyCards.setVisibility(true);
                var widgetDataMap = {
                    "flxActions": "flxActions",
                    "lblSeparator1": "lblSeparator1",
                    "lblCardHeader": "lblCardHeader",
                    "lblCardId": "lblCardId",
                    "lblCardStatus": "lblCardStatus",
                    "lblIdentifier": "lblIdentifier",
                    "flxCollapse": "flxCollapse",
                    "imgCollapse": "imgCollapse",
                    "imgChevron": "imgChevron",
                    "lblKey1": "lblKey1",
                    "rtxValue1": "rtxValue1",
                    "lblKey2": "lblKey2",
                    "rtxValue2": "rtxValue2",
                    "flxDestination1": "flxDestination1",
                    "flxDestination2": "flxDestination2",
                    "flxDestination3": "flxDestination3",
                    "flxDestination4": "flxDestination4",
                    "flxDestination5": "flxDestination5",
                    "lblDestination1": "lblDestination1",
                    "rtxDestination1": "rtxDestination1",
                    "lblDestination2": "lblDestination2",
                    "rtxDestination2": "rtxDestination2",
                    "lblDestination3": "lblDestination3",
                    "rtxDestination3": "rtxDestination3",
                    "lblDestination4": "lblDestination4",
                    "rtxDestination4": "rtxDestination4",
                    "lblDestination5": "lblDestination5",
                    "rtxDestination5": "rtxDestination5",
                    "lblKey4": "lblKey4",
                    "rtxValue4": "rtxValue4",
                    "lblKey5": "lblKey5",
                    "rtxValue5": "rtxValue5",
                    "lblKey6": "lblKey6",
                    "rtxValueA": "rtxValueA",
                    "btnAction1": "btnAction1",
                    "btnAction2": "btnAction2"
                };
                var segData = travelNotifications.map(function(dataItem) {
                    var destinations = self.returnDestinationsArray(dataItem.destinations);
                    return {
                        "flxActions": {
                            "isVisible": true
                        },
                        "lblSeparator1": " ",
                        "lblIdentifier": " ",
                        "lblCardHeader": kony.i18n.getLocalizedString("i18n.CardManagement.requestId"),
                        "lblCardId": dataItem.notificationId,
                        "lblCardStatus": {
                            "text": dataItem.status,
                            "skin": self.statusSkinsLandingScreen[dataItem.status],
                        },
                        "flxCollapse": {
                            "onClick": self.changeNotificationRowTemplate
                        },
                        "imgCollapse": {
                            "src":ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig":{
                                "a11yLabel":"View Transaction Details"
                            }
                        },
                        "lblKey1": kony.i18n.getLocalizedString("i18n.CardManagement.travelStartDate"),
                        "rtxValue1": self.returnFrontendDate(dataItem.startDate),
                        "lblKey2": kony.i18n.getLocalizedString("i18n.CardManagement.travelEndDate"),
                        "rtxValue2": self.returnFrontendDate(dataItem.endDate),
                        "flxDestination1": {
                            "isVisible": destinations[0] ? true : false
                        },
                        "lblDestination1": kony.i18n.getLocalizedString("i18n.CardManagement.destination1"),
                        "rtxDestination1": destinations[0],
                        "flxDestination2": {
                            "isVisible": destinations[1] ? true : false
                        },
                        "lblDestination2": kony.i18n.getLocalizedString("i18n.CardManagement.destination2"),
                        "rtxDestination2": destinations[1],
                        "flxDestination3": {
                            "isVisible": destinations[2] ? true : false
                        },
                        "lblDestination3": kony.i18n.getLocalizedString("i18n.CardManagement.destination3"),
                        "rtxDestination3": destinations[2],
                        "flxDestination4": {
                            "isVisible": destinations[3] ? true : false
                        },
                        "lblDestination4": kony.i18n.getLocalizedString("i18n.CardManagement.destination4"),
                        "rtxDestination4": destinations[3],
                        "flxDestination5": {
                            "isVisible": destinations[4] ? true : false
                        },
                        "lblDestination5": kony.i18n.getLocalizedString("i18n.CardManagement.destination5"),
                        "rtxDestination5": destinations[4],
                        "lblKey4": kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumber"),
                        "rtxValue4": dataItem.contactNumber,
                        "lblKey5": kony.i18n.getLocalizedString("i18n.CardManagement.additionalInfo"),
                        "rtxValue5": dataItem.additionalNotes.length >= 1 ? dataItem.additionalNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                        "lblKey6": kony.i18n.getLocalizedString("i18n.CardManagement.selectedCards"),
                        "rtxValueA": self.returnCardDisplayName(dataItem.cardNumber),
                        "btnAction1": {
                            "skin": dataItem.status === 'Expired' ? "sknBtnSSP3343A813PxBg0CSR" : "sknBtnSSP3343a813px",
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "onClick": dataItem.status === 'Expired' ? null : self.editTravelNotification.bind(self, dataItem)
                        },
                        "btnAction2": {
                            "skin": dataItem.status === 'Expired' ? "sknBtnSSP3343A813PxBg0CSR" : "sknBtnSSP3343a813px",
                            "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "onClick": dataItem.status === 'Expired' ? null : self.deleteNotification.bind(self, dataItem.notificationId)
                        },
                        "flxMyCards" :{
                          "clipBounds":false,
                          "skin":"sknFlxffffffShadowdddcdc",
                          "onClick": self.viewCardDetailsMobile
                        },
                        "imgChevron":"arrow_left_grey.png",
                        "template": "flxTravelNotificationsCollapsed"
                    };
                });
                this.view.myCards.segMyCards.widgetDataMap = widgetDataMap;
                this.view.myCards.segMyCards.setData(segData);
            }
            this.AdjustScreen();
            CommonUtilities.hideProgressBar(this.view);
        },
      
        /**
         * Method to show no travel notification screen 
         */
        showNoTravelNotificationScreen: function() {
            this.view.myCards.flxNoCardsError.setVisibility(true);
            this.view.myCards.lblNoCardsError.text = kony.i18n.getLocalizedString('i18n.CardsManagement.NoTravelNotificationError');
            this.AdjustScreen();
        },
      
        /**
         * Method to be called after success of delete Notification to refresh and load notification list
         */
        deleteNotificationSuccess: function() {
            this.fetchTravelNotifications();
        },
      
        /**
         * Method to return cards names with line break for manage travel plan 
         * @param {String} cards Cards names seperated by comma
         * @returns {String} cards name seperated by line break
         */
        returnCardDisplayName: function(cards) {
            return cards.replace(/,/g, "<br/>");
        },
      
        /**
         * Method to return frontend date for Travel Plan
         * @param {Date} date in yyyy-mm-dd format
         * @returns {String} dateString which has date in frontend date format
         */
        returnFrontendDate: function(date) {
            var dateString = CommonUtilities.getFrontendDateString(date);
            return dateString;
        },
      
        /**
         * changeRowTemplateMethod to toggle row template(btween selected and unselected templates for Travel Notifications) for selected row of segment
         */
        changeNotificationRowTemplate: function() {
            var index = this.view.myCards.segMyCards.selectedIndex;
            var rowIndex = index[1];
            var data = this.view.myCards.segMyCards.data;
            for (var i = 0; i < data.length; i++) {
                if (i === rowIndex) {

                    if (data[i].template === "flxTravelNotificationsCollapsed") {
                        data[i].imgCollapse = {
                            "src":ViewConstants.IMAGES.ARRAOW_UP,
                            "accessibilityconfig":{
                                "a11yLabel":"View Details"
                            }
                        };
                        data[i].template = "flxTravelNotificationsExpanded";
                    } else {
                        data[i].imgCollapse = {
                            "src":ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig":{
                                "a11yLabel":"View Details"
                            }
                        };
                        data[i].template = "flxTravelNotificationsCollapsed";
                    }
                } else {
                    data[i].imgCollapse = {
                        "src":ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig":{
                            "a11yLabel":"View Details"
                        }
                    };
                    data[i].template = "flxTravelNotificationsCollapsed";
                }
            }
            this.view.myCards.segMyCards.setData(data);
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
        /**
         * Method to return array of destinations from a string seperated by '-'
         * @param {String} destinations Destination seperated by '-'
         * @returns {Array} array that stores destinations
         */
        returnDestinationsArray: function(destinations) {
            var array = [];
            destinations.split('-').forEach(function(destination) {
                array.push(destination);
            })
            return array;
        },
      
        /**
         * Method to show edit travel notification screen
         * @param {Object} - notification object
         */
        editTravelNotification: function(data) {
            var self = this;
            this.notificationObject.requestId = data.notificationId;
            this.notificationObject.isEditFlow = true;
            self.navigateToAddTravelNotification();
            self.getBackendCards(data.cardNumber);
            this.view.lblRequestID.setVisibility(true);
            this.view.lblRequestNo.setVisibility(true);
            this.view.segDestinations.setVisibility(true);
            this.view.lblRequestID.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId');
            this.view.lblRequestNo.text = data.notificationId;
            CommonUtilities.blockFutureDate(this.view.calFrom, 60, self.returnFrontendDate(data.startDate));
            this.view.calTo.dateComponents = this.getDateComponent(self.returnFrontendDate(data.endDate));
            this.view.txtPhoneNumber.text = data.contactNumber;
            this.view.txtareaUserComments.text = data.additionalNotes;
            this.setDestinations(data.destinations);
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
         /**
         * Method to get already selected cards for edit travel notification 
         * @param {Object} - cards object
         */
        getBackendCards: function(data) {
            var cardNumber, cards = [];
            if (data) {
                if (data.indexOf(",")) {
                    data = data.split(",");
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                } else {
                    data.map(function(dataItem) {
                        cardNumber = dataItem.substring(dataItem.length - OLBConstants.MASKED_CARD_NUMBER_LENGTH, dataItem.length);
                        cards.push({
                            "number": cardNumber
                        });
                    });
                }
                this.notificationObject.selectedcards = cards;
            }
        },
      
      
        /**
         * Method to set destinations for edit travel notification 
         * @param {Object} - locationsObject
         */
        setDestinations: function(data) {
            var self = this;
            var destinations = [];
            if (data) {
                var dataMap = {
                    "lblDestination": "lblDestination",
                    "lblPlace": "lblPlace",
                    "lblAnotherDestination": "lblAnotherDestination",
                    "lblSeparator2": "lblSeparator2",
                    "imgClose": "imgClose"
                };
                if (data.indexOf("-") > 0) {
                    data = data.split("-");
                    data.forEach(function(dataItem) {
                        var segData = {
                            "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                            "lblPlace": dataItem,
                            "imgClose": {
                                "onClick": self.removeAddressFromList.bind(self)
                            },
                            "lblSeparator2": "a"
                        };
                        destinations.push(segData);
                    })
                } else {
                    var segData = {
                        "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                        "lblPlace": data,
                        "imgClose": {
                            "onClick": self.removeAddressFromList.bind(self)
                        },
                        "lblSeparator2": "a"
                    };
                    destinations.push(segData);
                }
                this.view.segDestinations.widgetDataMap = dataMap;
                destinations.forEach(function(item, value) {
                    item.lblDestination = kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (value + 1);
                })
                this.view.segDestinations.setData(destinations);
            }
            this.validateTravelPlanData();
        },

        /**
         * Method to remove address from the list of address for create/edit travel notification 
         */
        removeAddressFromList: function() {
            var self = this;
            var index = this.view.segDestinations.selectedIndex;
            if (index) {
                this.view.segDestinations.removeAt(index[1]);
            }
            var data = this.view.segDestinations.data;
            data.forEach(function(item, value) {
                item.lblDestination = kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (value + 1);
            });
            this.view.segDestinations.setData(data);
            self.validateTravelPlanData();
        },
       
        /**
         * Method to validate the data entered by user for creating/editing travel notification 
         */
        validateTravelPlanData: function() {
            var self = this;
            var validationUtilityManager = applicationManager.getValidationUtilManager();
            if(this.view.txtPhoneNumber.text !== ""){
              if(validationUtilityManager.isValidPhoneNumber(this.view.txtPhoneNumber.text) && this.view.txtPhoneNumber.text.length == 10 && this.view.segDestinations.data.length > 0){
                FormControllerUtility.enableButton(this.view.btnContinue);
              }
              else{
                FormControllerUtility.disableButton(this.view.btnContinue);
              }
            }else{
                FormControllerUtility.disableButton(this.view.btnContinue);
            }
        },
      
        /**
         * Method to get date component
         * @param {string} - Date string
         * @returns {Object} - dateComponent Object
         */
        getDateComponent: function(dateString) {
            var dateObj  = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase());
            return [dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear()];
        },
      
        /**
         * Shows the acknowledgement screen based on the action.
         * @param {Object} - card object.
         * @param {String} - contains the actino to be performed.
         */
        showCardOperationAcknowledgement: function(card, action) {
            var self = this;
            CommonUtilities.hideProgressBar(self.view);
            this.hideAllCardManagementViews();
            this.view.ConfirmDialog.confirmButtons.setVisibility(false);
            this.view.flxAcknowledgment.setVisibility(true);
            this.view.ConfirmDialog.flxDestination.setVisibility(false);
            this.view.ConfirmDialog.flxSelectCards.setVisibility(false);
            this.view.btnRequestReplacement.setVisibility(false);
            switch (action) {
                case kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardAcknowledgement"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockCard");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage1");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(true);
                        this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage2");
                        break;
                    }
                case "Offline_Change_Pin":
                case kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinAcknowledgement"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = card.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulChangePinAckMessage") : kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulChangePinRequestAckMessage");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
                        break;
                    }
                case kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardAcknowledgement"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulUnlockCardAckMessage");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
                        break;
                    }
                case kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardAcknowledgement"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenLower");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
                        this.view.btnRequestReplacement.text = kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement");
                        this.view.btnRequestReplacement.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement");
                        this.view.btnRequestReplacement.setVisibility(true);
                        this.view.btnRequestReplacement.onClick = function() {
                            self.replaceCard(card);
                        };
                        break;
                    }
                case kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardAcknowledgement"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
                        break;
                    }
                    case kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"):
                    {
                        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelAck"));
                        this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard");
                        this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelMsg");
                        this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
                        break;
                    }
            }
            this.view.Acknowledgement.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
            this.view.ConfirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardDetails");
            this.view.ConfirmDialog.keyValueCardHolder.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder") + ":";
            this.view.ConfirmDialog.keyValueCardHolder.lblValue.text = card.cardHolder;
            this.view.ConfirmDialog.keyValueCardName.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":";
            this.view.ConfirmDialog.keyValueCardName.lblValue.text = card.cardNumber;
            this.view.ConfirmDialog.keyValueValidThrough.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough") + ":";
            this.view.ConfirmDialog.keyValueValidThrough.lblValue.text = card.validThrough;
            this.view.ConfirmDialog.keyValueServiceProvider.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider") + ":";
            this.view.ConfirmDialog.keyValueServiceProvider.lblValue.text = card.serviceProvider;
            if (card.cardType === 'Credit') {
                this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(true);
                this.view.ConfirmDialog.keyValueCreditLimit.lblKey.text = kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit") + ":";
                this.view.ConfirmDialog.keyValueCreditLimit.lblValue.text = card.creditLimit;
                this.view.ConfirmDialog.keyValueAvailableCredit.lblKey.text = kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit") + ":";
                this.view.ConfirmDialog.keyValueAvailableCredit.lblValue.text = card.availableCredit;
            } else if (card.cardType === 'Debit') {
                this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(false);
                this.view.ConfirmDialog.keyValueCreditLimit.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.WithdrawalLimit") + ":";
                this.view.ConfirmDialog.keyValueCreditLimit.lblValue.text = card.dailyWithdrawalLimit;
            }
            this.view.btnBackToCards.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards");
            this.view.btnBackToCards.text = kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards");
            this.view.flxPrint.onClick = this.printAcknowlegement;
            this.view.btnBackToCards.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            this.AdjustScreen();
        },
      
        /**
         *  Entry point for replace card flow.
         * @param {Object} - card object.
         */
        cancelCard: function(card){
            this.showCancelCardView();
            this.setCancelCardButtonsActions(card);
            this.setCardDetails(card);
            this.view.forceLayout();
            this.AdjustScreen();
        },
       
        showCancelCardView:function(){
           this.hideAllCardManagementViews();
           this.hideAllCardManagementRightViews();
           this.view.flxCardVerification.setVisibility(true);
           this.view.CardLockVerificationStep.setVisibility(true);
           this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
           this.view.CardLockVerificationStep.flxWarning2.setVisibility(true);
           this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(true);
           this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
           this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(false);
           this.view.CardLockVerificationStep.flxAddress.setVisibility(false);
           FormControllerUtility.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
           CommonUtilities.setCheckboxState(false,  this.view.CardLockVerificationStep.CardActivation.imgChecbox);
           this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard");
           this.view.CardLockVerificationStep.WarningMessage.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn1");
           this.view.CardLockVerificationStep.WarningMessage.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn2");
           this.view.CardLockVerificationStep.WarningMessage.rtxWarningText3.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn3");
           this.view.CardLockVerificationStep.WarningMessage.rtxWarningText4.text = kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn4");
           this.view.CardLockVerificationStep.lblReason2.setVisibility(true);
           this.view.CardLockVerificationStep.lbxReason2.setVisibility(true);
           this.view.CardLockVerificationStep.WarningMessage.flxIAgree.setVisibility(true);
           CommonUtilities.setCheckboxState(false, this.view.CardLockVerificationStep.WarningMessage.imgChecbox);
           this.view.CardLockVerificationStep.lblReason2.text =  kony.i18n.getLocalizedString("i18n.CardManagement.PleaseEnterTheReasonMessage");
           var reasonMasterData = [];
           reasonMasterData.push(["Reason1",kony.i18n.getLocalizedString("i18n.cardManagement.privacy")]);
           reasonMasterData.push(["Reason2",kony.i18n.getLocalizedString("i18n.cardsManagement.annualCharges")]);
           reasonMasterData.push(["Reason3",kony.i18n.getLocalizedString("i18n.cardsManagement.others")]);
           this.view.CardLockVerificationStep.lbxReason2.masterData = reasonMasterData;    
           this.view.CardLockVerificationStep.lbxReason2.selectedKey = "Reason1";  
           this.view.forceLayout();
           this.AdjustScreen();     
       },

       setCancelCardButtonsActions : function(card){
           var self = this;
           var buttonsJSON = {
               'btnConfirm': {
                   'isVisible': true,
                   'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
                   'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
               },
               'btnModify': {
                   'isVisible': false,
               },
               'btnCancel': {
                   'isVisible': true,
                   'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                   'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
               },
           };
           this.alignConfirmButtons(buttonsJSON);
           this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
               kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards();
           };
         if (CommonUtilities.isCSRMode()) {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
               this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
               var params = {
                   'card': card,
                   'Action' : 'Cancel Card',
                   'Reason' : self.view.CardLockVerificationStep.lbxReason2.selectedKeyValue[1],
                   'notes' : ""
               };
               self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
           }.bind(this);
            }
          
           this.view.CardLockVerificationStep.WarningMessage.flxCheckbox.onTouchEnd = function() {
               CommonUtilities.toggleCheckBox(self.view.CardLockVerificationStep.WarningMessage.imgChecbox);
               if (CommonUtilities.isChecked(self.view.CardLockVerificationStep.WarningMessage.imgChecbox)) {
                   FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                   CommonUtilities.setCheckboxState(true, self.view.CardLockVerificationStep.WarningMessage.imgChecbox);
               } else {
                   FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                   CommonUtilities.setCheckboxState(false, self.view.CardLockVerificationStep.WarningMessage.imgChecbox);
               }
           };
           this.setCheckBoxToggleProperty();
           this.AdjustScreen();
           this.view.forceLayout();
       },

       setCheckBoxToggleProperty : function(){
           var self = this;
           this.view.CardLockVerificationStep.WarningMessage.btnTermsAndConditions.onClick = function() {
               self.view.flxTermsAndConditionsPopUp.height = (self.view.flxMain.frame.height + 355)+"dp";
               self.view.flxTC.top = "100dp";
               self.view.flxTermsAndConditionsPopUp.setVisibility(true);
               self.view.lblTermsAndConditions.setFocus(true);
               if (CommonUtilities.isChecked(self.view.CardLockVerificationStep.WarningMessage.imgChecbox)) {
                   CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
               } else {
                   CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
               }
           };
           this.view.btnCancel.onClick = function() {
               self.view.flxTermsAndConditionsPopUp.setVisibility(false);
           };
           this.view.flxClose.onClick = function() {
               self.view.flxTermsAndConditionsPopUp.setVisibility(false);
           };
           this.view.flxTCContentsCheckbox.onClick = function() {
               CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
           };
           this.view.btnSave.onClick = function() {
               if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                   FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                   CommonUtilities.setCheckboxState(true, self.view.CardLockVerificationStep.WarningMessage.imgChecbox);
               } else {
                   FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                   CommonUtilities.setCheckboxState(false, self.view.CardLockVerificationStep.WarningMessage.imgChecbox);
               }
               self.view.flxTermsAndConditionsPopUp.setVisibility(false);
           };
       },

        /**
         *  Entry point for replace card flow.
         * @param {Object} - card object.
         */
        replaceCard: function(card) {
            this.showCardReplacementView();
            this.setCardDetails(card);
            this.showCardReplacementGuildlines(card);
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.cardReplacement"));
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * Method to show replaced card details
         */
        showCardReplacementView: function() {
            this.hideAllCardManagementViews();
            this.hideAllCardManagementRightViews();
            this.view.flxCardVerification.setVisibility(true);
            this.view.CardLockVerificationStep.setVisibility(true);
            this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
            this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(true);
            this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.CardLockVerificationStep.WarningMessage.flxIAgree.setVisibility(false);
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * Method to show guidelines card replacement.
         * @param {Object} - card object 
         */
        showCardReplacementGuildlines: function(card) {
            var self = this;
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.cardReplacement"));
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline1");
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline2");
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText3.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline3");
            this.view.CardLockVerificationStep.WarningMessage.flxWarningText4.setVisibility(false);
            this.view.CardLockVerificationStep.lblReason2.setVisibility(false);
            this.view.CardLockVerificationStep.lbxReason2.setVisibility(false);
            this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
            this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(true);
            this.view.CardLockVerificationStep.flxAddress.setVisibility(true);
            this.view.CardLockVerificationStep.tbxNoteOptional.text = "";
            this.view.CardLockVerificationStep.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.CardLockVerificationStep.lblReason2.setVisibility(true);
            this.view.CardLockVerificationStep.lbxReason2.setVisibility(true);
            this.view.CardLockVerificationStep.lblReason2.text =  kony.i18n.getLocalizedString("i18n.CardsManagement.replaceReason");
            var reasonMasterData = [];
            reasonMasterData.push(["Reason1",kony.i18n.getLocalizedString("i18n.CardsManagement.replaceReason1")]);
            reasonMasterData.push(["Reason2",kony.i18n.getLocalizedString("i18n.CardsManagement.damageCard")]);
            reasonMasterData.push(["Reason3",kony.i18n.getLocalizedString("i18n.cardsManagement.others")]);
            this.view.CardLockVerificationStep.lbxReason2.masterData = reasonMasterData;    
            this.view.CardLockVerificationStep.lbxReason2.selectedKey = "Reason1";
            this.view.CardLockVerificationStep.lblAddress.text = kony.i18n.getLocalizedString("i18n.CardManagement.addressforcards");
            var addressObject = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchUserAddresses();
            var addressArray = [];
            for(var addressIndex = 0;addressIndex < 3;addressIndex = addressIndex + 1){//To reset all the radio buttons to off state.
                this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].text = "L";
                this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].skin = "sknC0C0C020pxolbfonticons";
            }
            if (addressObject) {
                addressObject.forEach(function(dataItem) {
                    addressArray.push(dataItem.AddressLine1 + ", " + dataItem.AddressLine2 + ", " + dataItem.CityName + ", " + dataItem.CountryName + ", " + dataItem.ZipCode);
                });
            }
            if(addressObject){
                for(var addressIndex = 0;addressIndex < addressObject.length && addressIndex<3;addressIndex = addressIndex + 1){
                    if(addressObject[addressIndex].isPrimary == "true"){
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].text = "M";
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].skin = "sknLblFontTypeIcon3343e820pxMOD";
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].toolTip =  kony.i18n.getLocalizedString("i18n.CardManagement.PrimaryAddress");
                    }
                    else{
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].text = "L";
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].skin = "sknC0C0C020pxolbfonticons";
                        this.view.CardLockVerificationStep["lblAddressCheckBox"+ (addressIndex+1)].toolTip =  kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryAddress");
                    }
                }
            }
            if (addressArray[0]) {
                this.view.CardLockVerificationStep.rtxAddress1.setVisibility(true);
                this.view.CardLockVerificationStep.flxAddressCheckbox1.setVisibility(true);
                this.view.CardLockVerificationStep.rtxAddress1.text = addressArray[0];
            } else {
                this.view.CardLockVerificationStep.flxAddress.setVisibility(false);
                this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(false);
                this.view.CardLockVerificationStep.rtxAddress1.setVisibility(false);
                this.view.CardLockVerificationStep.flxAddressCheckbox1.setVisibility(false);
            }
            if (addressArray[1]) {
                this.view.CardLockVerificationStep.rtxAddress2.setVisibility(true);
                this.view.CardLockVerificationStep.flxAddressCheckbox2.setVisibility(true);
                this.view.CardLockVerificationStep.rtxAddress2.text = addressArray[1];
            } else {
                this.view.CardLockVerificationStep.flxAddressCheckbox2.setVisibility(false);
                this.view.CardLockVerificationStep.rtxAddress2.setVisibility(false);
            }
            if (addressArray[2]) {
                this.view.CardLockVerificationStep.rtxAddress3.setVisibility(true);
                this.view.CardLockVerificationStep.flxAddressCheckbox3.setVisibility(true);
                this.view.CardLockVerificationStep.rtxAddress3.text = addressArray[2];
            } else {
                this.view.CardLockVerificationStep.flxAddressCheckbox3.setVisibility(false);
                this.view.CardLockVerificationStep.rtxAddress3.setVisibility(false);
            }
            var checkBoxArray = [];
            checkBoxArray.push({
                'flex1': this.view.CardLockVerificationStep.flxAddressCheckbox1,
                'flex2': this.view.CardLockVerificationStep.flxAddressCheckbox2,
                'flex3': this.view.CardLockVerificationStep.flxAddressCheckbox3
            });
            this.view.CardLockVerificationStep.flxAddressCheckbox1.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            this.view.CardLockVerificationStep.flxAddressCheckbox2.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            this.view.CardLockVerificationStep.flxAddressCheckbox3.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
                    'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
                },
                'btnModify': {
                    'isVisible': false,
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards();
            };
           
          	 if (CommonUtilities.isCSRMode()) {
               this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
               FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
               this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                var params = {
                    'card': card,
                    'userName': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.getUserName(),
                    'CardAccountNumber': card.maskedCardNumber,
                    'CardAccountName': card.productName,
                    'AccountType': 'CARD',
                    'RequestCode': 'REPLACEMENT',
                    'Channel': OLBConstants.Channel,
                    'Address_id': self.getSelectedAddressId(),
                    'AdditionalNotes': self.view.CardLockVerificationStep.tbxNoteOptional.text,
                    'reason' : self.view.CardLockVerificationStep.lbxReason2.selectedKeyValue[1]
                };
                self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
            }.bind(this);
            }
           
            this.AdjustScreen();
        },
      
        /**
         * Method to reset skin to calendar
         */
        setSkinToCalendar: function() {
            this.view.calFrom.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
            this.view.calTo.skin = ViewConstants.SKINS.COMMON_CALENDAR_NOERROR;
            this.view.lblWarningTravelPlan.setVisibility(false);
            this.AdjustScreen();
        },


        /**
         * Method to show creat travel notifications screen
         * @param {Object} - country , region(states) , city object
         */
        showAddNewTravelPlan: function(locationData) {
            var self = this;
            this.setSkinToCalendar();
            if (locationData) {
                if (locationData.country) {
                    this.setCountryObject(locationData.country);
                }
                if (locationData.states) {
                    this.setStatesObject(locationData.states);
                }
                if (locationData.city) {
                    this.setCitiesObject(locationData.city)
                }
            }
            if (self.view.lblManageTravelPlans.text === kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan')) {
                self.view.flxMyCards.setVisibility(false);
                self.view.lblMyCardsHeader.text = kony.i18n.getLocalizedString('i18n.CardManagement.CreateTravelPlan');
                self.view.lblRequestID.setVisibility(false);
                self.view.lblRequestNo.setVisibility(false);
                self.view.flxTravelPlan.setVisibility(true);
                this.view.lblWarningTravelPlan.setVisibility(false);
                this.view.calFrom.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                this.view.calTo.dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                this.view.calFrom.onSelection = self.setSkinToCalendar;
                this.view.calTo.onSelection = self.setSkinToCalendar;
                this.view.flxOtherDestinations.setVisibility(false);
                FormControllerUtility.disableButton(this.view.btnContinue);
                this.setDefaultDataForNotifications();
                this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular40Pr';
                this.view.txtPhoneNumber.onKeyUp = self.validateTravelPlanData.bind(this);
                this.view.btnCancel1.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                this.view.btnCancel1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                this.view.btnCancel1.onClick = function() {
                    if (self.notificationObject.isEditFlow) {
                        self.notificationObject.isEditFlow = false;
                    }
                    self.fetchTravelNotifications();
                }
                this.view.btnContinue.text = kony.i18n.getLocalizedString("i18n.common.proceed");
                this.view.btnContinue.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
                this.view.btnContinue.onClick = function() {
                    if (self.validateDateRange()) {
                        FormControllerUtility.showProgressBar(self.view);
                        self.view.lblWarningTravelPlan.setVisibility(false);
                        self.getDetailsOfNotification(self.notificationObject.isEditFlow);
                        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.getEligibleCards();
                    } else {
                        self.view.lblWarningTravelPlan.setVisibility(true);
                        self.view.lblWarningTravelPlan.text = kony.i18n.getLocalizedString('i18n.CardManagement.invalidDateError');
                        self.view.calFrom.skin = ViewConstants.SKINS.SKNFF0000CAL;
                        self.view.calTo.skin = ViewConstants.SKINS.SKNFF0000CAL;
                    }
                }
                this.view.flxDestinationList.setVisibility(false);
                this.setFlowActions();
                this.view.forceLayout();
                this.AdjustScreen();
                CommonUtilities.hideProgressBar(this.view)
            }
        },
        
        /**
         * Method to set countryObject for create/edit travel notification 
         * @param {Object} - countryObject
         */
        setCountryObject: function(countryList) {
            var self = this
            countryList.forEach(
                function(element) {
                    self.countries[element.id] = {
                        "name": element.Name
                    }
                })
        },
      
        /**
         * Method to set statesObject for create/edit travel notification 
         * @param {Object} - stateObject(regionObject)
         */
        setStatesObject: function(stateList) {
            var self = this
            stateList.forEach(
                function(element) {
                    self.states[element.id] = {
                        "name": element.Name,
                        "country_id": element.Country_id
                    }
                })
        },
      
        /**
         * Method to set cityObject for create/edit travel notification 
         * @param {Object} - cityObject
         */
        setCitiesObject: function(cityList) {
            var self = this
            cityList.forEach(
                function(element) {
                    self.cities[element.id] = {
                        "name": element.Name,
                        "state_id": element.Region_id,
                        "country_id": element.Country_id
                    }
                })
        },

        /**
         * Method to set default data for create/edit travel notification 
         */
        setDefaultDataForNotifications: function() {
            var self = this;
            if (this.notificationObject.isEditFlow === true) {
                this.view.flxOtherDestinations.setVisibility(true);
                this.view.lblRequestID.setVisibility(true);
                this.view.lblRequestNo.setVisibility(true);
                this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
              	this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
                this.view.flxMangeTravelPlans.onClick = function() {
                    self.notificationObject.isEditFlow = false;
                    self.navigateToAddTravelNotification();
                }
                this.view.lblMyCardsHeader.text = kony.i18n.getLocalizedString('i18n.CardManagement.editTravelPlan');
                self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.editTravelPlan'));
                FormControllerUtility.enableButton(this.view.btnContinue);
            } else {
                this.view.txtDestination.text = "";
                this.view.txtPhoneNumber.text = "";
                this.view.txtareaUserComments.text = "";
                this.view.segDestinations.setData([]);
                this.view.segDestinationList.setData([]);
                this.notificationObject.selectedcards = [];
                this.blockFutureDateSelection(self.view.calFrom);
                CommonUtilities.disableOldDaySelection(this.view.calTo);
                this.view.calFrom.dateComponents = self.getDateComponents(kony.os.date(applicationManager.getFormatUtilManager().getDateFormat()));
                this.view.calTo.dateComponents = self.getDateComponents(kony.os.date(applicationManager.getFormatUtilManager().getDateFormat()));
                this.view.calFrom.dateEditable = false;
                this.view.calTo.dateEditable = false;
                this.view.txtPhoneNumber.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterPhoneNumber");
                this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
              	this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');
                this.view.flxMangeTravelPlans.onClick = self.fetchTravelNotifications.bind(this);
                self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.CreateTravelPlan'));
            }
        },
        getDateComponents: function(dateString) {
            var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase())
            return [dateObj.getDate(), dateObj.getMonth()+1, dateObj.getFullYear()];
        },
      
        /**
         * Method to block future date selection for create/edit travel notification screen
         * @param {Object} - widget reference
         */
        blockFutureDateSelection: function(widgetId) {
            CommonUtilities.blockFutureDate(this.view.calFrom, 60, kony.os.date());
        },
      
        setBreadCrumbAndHeaderDataForCardOperation: function(header) {
            this.view.breadcrumb.setVisibility(false);
            this.view.breadcrumb.setBreadcrumbData([{
                'text': kony.i18n.getLocalizedString("i18n.CardManagement.ManageCard"),
                'callback': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController)
            }, {
                'text': header,
                'callback': null
            }]);
            this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = header;
        },
      
        /**
         * Method to get details of the notification for creating/updating travel notification 
         */
        getDetailsOfNotification: function(isEditFlow) {
            var self = this;
            var selectedcards = self.notificationObject.selectedcards;
            var requestId = this.notificationObject.requestId;
            self.notificationObject = {
                'fromDate': this.view.calFrom.date,
                'toDate': this.view.calTo.date,
                'phone': this.view.txtPhoneNumber.text,
                'notes': this.view.txtareaUserComments.text,
                'locations': self.getSelectedLocations()
            };
            if (selectedcards) {
                self.notificationObject.selectedcards = selectedcards;
            }
            if (isEditFlow) {
                self.notificationObject.isEditFlow = isEditFlow;
                self.notificationObject.requestId = requestId
            }
            return self.notificationObject;
        },
      
        /**
         * Method to get selected locations for create/edit travel notification 
         * @returns {Object} selectedLocations object
         */
        getSelectedLocations: function() {
            var locationArray = this.view.segDestinations.data;
            var selectedLocations = [];
            for (var key in locationArray) {
                selectedLocations.push(locationArray[key].lblPlace);
            }
            return selectedLocations;
        },
      
        /**
         * Method to set country,regions(states),city in drop down for create/edit travel notification
         */
        setFlowActions: function() {
            var scope = this;
            this.view.txtDestination.onKeyUp = function() {
                scope.showTypeAhead();
            };
            this.view.segDestinationList.onRowClick = function() {
                var rowNo = scope.view.segDestinationList.selectedRowIndex[1];
                var dest = scope.view.segDestinationList.data[rowNo].lblListBoxValues;
                scope.view.txtDestination.text = dest;
                scope.view.txtDestination.onKeyUp = function() {
                    scope.showTypeAhead();
                    if (dest === scope.view.txtDestination.text) {
                        scope.enableAddButton();
                    } else {
                        scope.disableAddButton();
                    }
                }
                scope.enableAddButton();
                scope.view.flxDestinationList.setVisibility(false);
                scope.view.txtDestination.setFocus();
            };
        },
        /**
         * Method to show country,regions(states),city in drop down for create/edit travel notification
         */
        showTypeAhead: function() {
            var countryData = [];
            var newData = {};
            var stateId;
            var countryId;
            var key;
            var dataMap = {
                "flxCustomListBox": "flxCustomListBox",
                "lblListBoxValues": "lblListBoxValues"
            };
            var tbxText = this.toTitleCase(this.view.txtDestination.text);
            if (tbxText.length > 2) {
                for (key in this.cities) {
                    if (this.cities[key].name.indexOf('' + tbxText) === 0) {
                        stateId = this.cities[key].state_id;
                        countryId = this.cities[key].country_id;
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": this.countries[countryId].name + ", " + this.states[stateId].name + ", " + this.cities[key].name,
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                for (key in this.states) {
                    if (this.states[key].name.indexOf('' + tbxText) === 0) {
                        countryId = this.states[key].country_id;
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": this.countries[countryId].name + ", " + this.states[key].name,
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                for (key in this.countries) {
                    if (this.countries[key].name.indexOf('' + tbxText) === 0) {
                        newData = {
                            "countryId": countryId,
                            "lblListBoxValues": "" + this.countries[key].name,
                            "template": "flxCustomListBox"
                        };
                        countryData.push(newData);
                    }
                }
                this.view.segDestinationList.widgetDataMap = dataMap;
                this.view.segDestinationList.setData(countryData);
                this.view.flxDestinationList.setVisibility(true);
                if (this.view.lblWarningTravelPlan.isVisible === true)
                    this.view.flxDestinationList.top = 275 + "dp";
                else if (this.view.lblRequestID.isVisible === true && this.view.lblRequestNo.isVisible === true)
                    this.view.flxDestinationList.top = 320 + "dp";
                else
                    this.view.flxDestinationList.top = 245 + "dp";
                this.view.forceLayout();
            } else {
                this.view.flxDestinationList.setVisibility(false);
            }
        },
      
        /**
         * Method to enable Add button on create/edit travel notification screen
         */
        enableAddButton: function() {
            var self = this;
            if (this.view.segDestinations.data.length < applicationManager.getConfigurationManager().numberOfLocations) {
                this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular';
                this.view.flxAddFeatureRequestandimg.onClick = self.addAddressTOList;
            }
        },
        /**
         * Method to disable Add button on create/edit travel notification screen
         */
        disableAddButton: function() {
            this.view.lblAnotherDestination.skin = 'skn3343a8labelSSPRegular40Pr';
            this.view.flxAddFeatureRequestandimg.onClick = null;
        },
      
         /**
         * changeRowTemplate - Method to toggle row template(btween selected and unselected templates for My Cards) for selected row of segment 
         */
        changeRowTemplate: function() {
            var index = this.view.myCards.segMyCards.selectedIndex;
            var rowIndex = index[1];
            var data = this.view.myCards.segMyCards.data;
            for (var i = 0; i < data.length; i++) {
                if (i == rowIndex) {
                    if (data[i].template == "flxMyCardsCollapsed") {
                        data[i].imgCollapse = {
                            "src":ViewConstants.IMAGES.ARRAOW_UP,
                            "accessibilityconfig":{
                                "a11yLabel":"View Details"
                            }
                        };
                        data[i].template = "flxMyCardsExpanded";
                    } else {
                        data[i].imgCollapse = {
                            "src":ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig":{
                                "a11yLabel":"View Details"
                            }
                        };
                        data[i].template = (kony.application.getCurrentBreakpoint()==640|| orientationHandler.isMobile)?"flxMyCardsCollapsedMobile":"flxMyCardsCollapsed";
                    }
                } 
//               else {
//                     data[i].imgCollapse = {
//                         "src":ViewConstants.IMAGES.ARRAOW_DOWN,
//                         "accessibilityconfig":{
//                             "a11yLabel":"View Details"
//                         }
//                     };
//                     data[i].template = kony.application.getCurrentBreakpoint()==640?"flxMyCardsCollapsedMobile":"flxMyCardsCollapsed";
//                 }
            }
            this.view.myCards.segMyCards.setDataAt(data[rowIndex], rowIndex, index[0]);
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
        /**
         * Method that hides all right side flexes in frmCardManagement.
         */
        hideAllCardManagementRightViews: function() {
            this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(false);
            this.view.CardLockVerificationStep.flxVerifyBySecurityQuestions.setVisibility(false);
            this.view.CardLockVerificationStep.flxDeactivateCard.setVisibility(false);
            this.view.CardLockVerificationStep.flxTravelNotification.setVisibility(false);
            this.view.CardLockVerificationStep.flxChangeCardPin.setVisibility(false);
            this.view.CardLockVerificationStep.flxConfirmPIN.setVisibility(false);
            this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(false);
        },
      
        /**
         * Method to show pop up for delete notification and register onClicks for custom pop up
         * @param {String} notificationId id of notification that is to be deleted
         */
        deleteNotification: function(notificationId) {
            var self = this;
            this.view.flxAlert.setVisibility(true);
            this.view.CustomAlertPopup.lblHeading.setFocus(true);
            var height = this.view.customheader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height;
            this.view.flxAlert.height = height + "dp";
            this.view.CustomAlertPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.deleteTravelMsg") + " " + notificationId + " ?";
            this.view.CustomAlertPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
            this.view.CustomAlertPopup.lblHeading.setFocus(true);
            this.view.CustomAlertPopup.btnYes.onClick = function() {
                FormControllerUtility.showProgressBar(self.view);
                self.view.flxAlert.setVisibility(false);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.deleteNotification(notificationId);
            };
            this.view.CustomAlertPopup.flxCross.onClick = function() {
                self.view.flxAlert.isVisible = false;
            };
            this.view.CustomAlertPopup.btnNo.onClick = function() {
                self.view.flxAlert.isVisible = false;
            };
            this.view.forceLayout();
        },
      
        /**
         * Method to validate fromDate and toDate for create/edit travel notification screen
         */
        validateDateRange: function() {
            var startDateObject = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(this.view.calFrom.date,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            var endDateObject = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(this.view.calTo.date,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            
            if (startDateObject < endDateObject) {
                return true;
            }
            return false;
        },
        
        /**
         * Method to show cards selection screen
         * @param {Object} - cards object
         */
        showSelectCardScreen: function(cards) {
            var self = this;
            var internationEligibleCards = [];
            if (this.isInternationalPlan()) {
                cards.forEach(function(item) {
                    if (item.isInternational == "true")
                        internationEligibleCards.push(item);
                });
            }
            if (internationEligibleCards.length >= 0 && this.isInternationalPlan()) {
                cards = internationEligibleCards;
            }
            if (cards.length > 0) {
                this.view.myCards.flxNoCardsError.setVisibility(false);
                this.view.flxMyCards.setVisibility(true);
                this.view.flxMyCards.top="67dp";
                this.view.flxMyCards.skin="sknFlxffffffShadowdddcdc";
                this.view.myCards.segMyCards.setVisibility(true);
                this.view.myCards.lblMyCardsHeader.left="20dp";
                this.view.flxTravelPlan.setVisibility(false);
                var widgetDataMap = {
                    "imgCard": "imgCard",
                    "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                    "flxCheckBox": "flxCheckBox",
                    "lblCheckBox": "lblCheckBox",
                    "flxCardDetails": "flxCardDetails",
                    "flxDetailsRow1": "flxDetailsRow1",
                    "flxDetailsRow2": "flxDetailsRow2",
                    "flxDetailsRow3": "flxDetailsRow3",
                    "lblKey1": "lblKey1",
                    "lblKey2": "lblKey2",
                    "lblKey3": "lblKey3",
                    "rtxValue1": "rtxValue1",
                    "rtxValue2": "rtxValue2",
                    "rtxValue3": "rtxValue3",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator3": "lblSeperator3",
                    "lblCardsSeperator": "lblCardsSeperator",
                    "imgChevron":"imgChevron"
                };

                var cardSegData = [];
                var card = {};
                cards.forEach(function(dataItem) {
                    card = {
                        "lblCardsSeperator": {
                            "text": ".",
                            "height": "105px"
                        },
                        "lblSeperator3": {
                            "text": "a",
                            "height": "1"
                        },
                        "imgCard": {
                            "src": self.getImageForCard(dataItem.cardProductName),
                        },
                        "flxCheckBox": {
                            "onClick": self.toggleSelectedCardCheckbox.bind(self),
                        },
                        "lblCheckBox": {
                            "text": self.isCardSelected(dataItem)
                        },
                        "lblKey1": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.cardName") + ":",
                        },
                        "lblKey2": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":",
                        },
                        "lblKey3": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.internationalEligible") + ":",
                        },
                        "rtxValue1": {
                            "text": dataItem.cardProductName
                        },
                        "rtxValue2": {
                            "text": dataItem.maskedCardNumber
                        },
                        "rtxValue3": {
                            "text": dataItem.isInternational === "false"? kony.i18n.getLocalizedString('i18n.common.no') : kony.i18n.getLocalizedString('i18n.common.yes')
                        },
                        "template": "flxSelectFromEligibleCards"
                    };
                    cardSegData.push(card);
                });
                this.view.myCards.segMyCards.widgetDataMap = widgetDataMap;
                this.view.myCards.segMyCards.setData(cardSegData);
                this.view.flxEligibleCardsButtons.setVisibility(true);
                this.view.myCards.flxApplyForCards.setVisibility(false);
                this.view.btnCardsCancel.onClick = this.showNotificationToModify.bind(this);
                self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans'));
                this.view.btnCardsContinue.onClick = function() {
                    self.getSelectedCards();
                    self.showConfirmationScreen(self.notificationObject);
                }
            } else {
                this.view.myCards.flxNoCardsError.setVisibility(true);
                this.view.myCards.lblNoCardsError.text = kony.i18n.getLocalizedString('i18n.CardManagement.noInternationalCardError');
                this.view.flxMyCards.setVisibility(true);
                this.view.flxMyCards.top="0dp";
                this.view.flxMyCards.skin="slfbox";
                this.view.myCards.segMyCards.setVisibility(false);
                this.view.flxTravelPlan.setVisibility(false);
                this.view.myCards.flxApplyForCards.setVisibility(true);
                this.view.flxEligibleCardsButtons.setVisibility(false);
                this.view.myCards.btnApplyForCard.setVisibility(true);
                this.view.myCards.btnApplyForCard.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                this.view.myCards.btnApplyForCard.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
                this.view.myCards.btnApplyForCard.onClick = this.showNotificationToModify.bind(this);
            }
            this.view.forceLayout();
            this.AdjustScreen();
            CommonUtilities.hideProgressBar(this.view);
        },
      
        /**
         * Method to get selected cards for creating/editing travel notification 
         */
        getSelectedCards: function() {
            var cards = this.view.myCards.segMyCards.data;
            var selectedcards = []
            cards.forEach(function(dataItem) {
                if (dataItem.lblCheckBox.text === "C")
                    selectedcards.push({
                        "name": dataItem.rtxValue1.text,
                        "number": dataItem.rtxValue2.text
                    });
            });
            this.notificationObject.selectedcards = selectedcards;
        },

        /**
         * Method to check if travel notification is international travel notification
         */
        isInternationalPlan: function() {
            var userCountry;
            var Country = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchUserAddresses();
            Country.forEach(function(dataItem) {
                if (dataItem.isPrimary === "true") userCountry = dataItem.CountryName;
            });
            var selectedCountries = this.notificationObject.locations.map(function(item) {
                var arr = item.split(",");
                return arr[0];
            });
            for (var key in selectedCountries) {
                if (selectedCountries[key] === userCountry) return false;
            }
            return true;
        },
      
        /**
         * Method to show confirmation screen for create/edit travel notification 
         */
        showConfirmationScreen: function(data) {
            var self = this;
            var cards = "";
            this.view.flxConfirmBody.top = "2dp";
            this.view.flxActivateCard.setVisibility(false);
            this.view.flxAcknowledgment.setVisibility(false);
            this.view.flxCardVerification.setVisibility(false);
            this.view.flxMyCardsView.setVisibility(false);
            this.view.flxConfirm.setVisibility(true);
            this.view.flxDestination2.setVisibility(false);
            this.view.flxDestination3.setVisibility(false);
            this.view.flxDestination4.setVisibility(false);
            this.view.flxDestination5.setVisibility(false);
            this.view.flxErrorMessage.setVisibility(false);
            this.view.flxConfirmHeading.setVisibility(true);
            this.view.lblSeparator5.setVisibility(false);
            this.view.flxDownload.setVisibility(false);
            self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.manageTravelPlanConfirmation'));
            this.view.lblKey1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate');
            this.view.rtxValue1.text = data.fromDate;
            this.view.lblKey2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate');
            this.view.rtxValue2.text = data.toDate;
            this.view.lblDestination1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1');
            this.view.rtxDestination1.text = data.locations[0];
            if (data.locations[1]) {
                this.view.flxDestination2.setVisibility(true);
                this.view.lblDestination2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2');
                this.view.rtxDestination2.text = data.locations[1];
            }
            if (data.locations[2]) {
                this.view.flxDestination3.setVisibility(true);
                this.view.lblDestination3.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3');
                this.view.rtxDestination3.text = data.locations[2];
            }
            if (data.locations[3]) {
                this.view.flxDestination4.setVisibility(true);
                this.view.lblDestination4.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4');
                this.view.rtxDestination4.text = data.locations[3];
            }
            if (data.locations[4]) {
                this.view.flxDestination5.setVisibility(true);
                this.view.lblDestination5.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5');
                this.view.rtxDestination5.text = data.locations[4];
            }
            this.view.lblKey5.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.PhoneNumber');
            this.view.rtxValue5.text = data.phone;
            this.view.lblKey6.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation');
            this.view.rtxValue6.text = data.notes;
            this.view.lblKey4.text = kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards');
            data.selectedcards.map(function(dataItem) {
                cards = cards + dataItem.name + "-" + dataItem.number + "<br/>";
            });
            this.view.rtxValueA.text = cards;
            this.view.btnCancelPlan.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.btnCancelPlan.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            this.view.btnCancelPlan.onClick = function() {
                self.showPopUp();
            }
            this.view.btnModify.text = kony.i18n.getLocalizedString('i18n.common.modifiy');
            this.view.btnModify.tooltip = kony.i18n.getLocalizedString('i18n.common.modifiy');
            this.view.btnModify.onClick = this.showNotificationToModify.bind(this);
            this.view.btnConfirm.text = kony.i18n.getLocalizedString('i18n.common.confirm');
            this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirm');
            if (this.notificationObject.isEditFlow) {
                this.view.btnConfirm.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.updateTravelNotifications(self.notificationObject);
                }
            } else {
                this.view.btnConfirm.onClick = function() {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.createTravelNotification(self.notificationObject);
                }
            }
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
      
      initMFAFlow:function(params,action){
             CommonUtilities.showProgressBar(this.view);
             kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.verifySecureAccessCodeSuccess(params,action);

        },
      
        /**
         * Method to show confirmation pop up for create/edit travel notification 
         */
        showPopUp: function() {
            var self = this;
            this.view.flxAlert.height = this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + "dp";
            this.view.flxAlert.setVisibility(true);
            this.view.CustomAlertPopup.lblHeading.setFocus(true);
            this.view.CustomAlertPopup.lblPopupMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.cancelCreateMsg');
            this.view.CustomAlertPopup.btnYes.onClick = function() {
                self.view.flxAlert.setVisibility(false);
                self.notificationObject = {};
                self.fetchTravelNotifications();
            }
            this.view.CustomAlertPopup.btnNo.onClick = function() {
                self.view.flxAlert.setVisibility(false);
            }
            this.view.CustomAlertPopup.flxCross.onClick = function() {
                self.view.flxAlert.setVisibility(false);
            }
        },
      
        /**
         * Method to show create/edit travel notification for modification
         */
        showNotificationToModify: function() {
            this.view.txtDestination.text = "";
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.lblRequestID.setVisibility(false);
            this.view.lblRequestNo.setVisibility(false);
            this.view.flxConfirm.setVisibility(false);
            this.view.flxMyCards.setVisibility(false);
            this.view.myCards.btnApplyForCard.setVisibility(false);
            this.view.flxTravelPlan.setVisibility(true);
            this.view.flxOtherDestinations.setVisibility(true);
            this.view.flxMyCardsView.setVisibility(true);
            FormControllerUtility.enableButton(this.view.btnCardsContinue);
            this.view.forceLayout();
            this.AdjustScreen();
        },

        /**
         * Method to validate whether a card is selected or not
         * @param {Object} - cards object
         */
        isCardSelected: function(dataItem) {
            var self = this;
            var cardNumber = dataItem.maskedCardNumber;
            var selectedcards = this.notificationObject.selectedcards;
            var txt = "D";
            if (selectedcards) {
                selectedcards.forEach(function(data) {
                    if (data.number === cardNumber) {
                        txt = "C";
                        FormControllerUtility.enableButton(self.view.btnCardsContinue);
                    }
                });
            } else {
                txt = "D";
            }
            return txt;
        },
      
        /**
         * Method to get the image for a given card product.
         * @param {String} - card product name.
         * @returns {String} - Name of image file.
         */
        getImageForCard: function(cardProductName) {
            if (cardProductName)
                return this.cardImages[cardProductName];
            return ViewConstants.IMAGES.GOLDEN_CARDS;
        },
      
        /**
         * Method to handle selection of cards on card selection screen
         */
        toggleSelectedCardCheckbox: function() {
            var self = this;
            var selectedIndex = this.view.myCards.segMyCards.selectedIndex[1];
            var data = this.view.myCards.segMyCards.data;
            for (var index = 0; index < data.length; index++) {
                if (index === selectedIndex) {
                    if (data[index].lblCheckBox.text === "C") {
                        data[index].lblCheckBox.text = "D";
                    } else {
                        data[index].lblCheckBox.text = "C";
                    }
                }
                this.view.myCards.segMyCards.setData(data);
            }
            self.checkContinue(data);
        },
      
        /**
         * Method to enable continue button,if any of the card is selected
         * @param {Object} - cards data
         */
        checkContinue: function(data) {
            var enable = false;
            data.forEach(function(dataItem) {
                if (dataItem.lblCheckBox.text === "C")
                    enable = true;
            })
            if (enable) {
                FormControllerUtility.enableButton(this.view.btnCardsContinue)
            } else {
                FormControllerUtility.disableButton(this.view.btnCardsContinue)
            }
        },
      
         /**
         * Method to add selected address to the list of address for create/edit travel notification 
         */
        addAddressTOList: function() {
            if (this.view.txtDestination.text !== "") {
                this.view.flxOtherDestinations.setVisibility(true);
                var self = this;
                var dataMap = {
                    "lblDestination": "lblDestination",
                    "lblPlace": "lblPlace",
                    "lblAnotherDestination": "lblAnotherDestination",
                    "lblSeparator2": "lblSeparator2",
                    "imgClose": "imgClose"
                };
                var data = [{
                    "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
                    "lblPlace": this.view.txtDestination.text,
                    "imgClose": {
                        "onClick": self.removeAddressFromList
                    },
                    "lblSeparator2": "a"
                }];
                data[0].lblDestination = kony.i18n.getLocalizedString("i18n.CardManagement.destination") + " " + (this.view.segDestinations.data.length + 1);
                this.view.segDestinations.widgetDataMap = dataMap;
                this.view.segDestinations.addAll(data);
                this.view.txtDestination.text = "";
                self.disableAddButton();
                this.validateTravelPlanData();
            }
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
        /**
         * showCardsStatus - Method that hides all other flexes except the Cards segment.
         * @param {Array} - Array of card ids.
         */
        showCardsStatus: function(cards) {
            var self = this;
            this.setCardsData(cards);
            this.view.flxMyCardsView.setVisibility(true);
            this.view.flxMyCards.setVisibility(true);
            this.view.myCards.setVisibility(true);
            this.view.myCards.lblMyCardsHeader.text = kony.i18n.getLocalizedString("i18n.CardManagement.MyCards");
            this.view.flxMyCards.skin="slFbox";
            this.view.forceLayout();
            this.AdjustScreen();
            CommonUtilities.hideProgressBar(this.view);
        },
      
        /**
         * setCardsData - Method that binds the cards data to the segment.
         * @param {Array} - Array of JSON objects of cards.
         */
        setCardsData: function(cards) {
            if (cards.data && cards.data.length <= 0) {
                this.showCardsNotAvailableScreen();
            } else {
                var self = this;
                var dataMap = {
                    "btnAction1": "btnAction1",
                    "btnAction2": "btnAction2",
                    "btnAction3": "btnAction3",
                    "btnAction4": "btnAction4",
                    "btnAction5": "btnAction5",
                    "btnAction6": "btnAction6",
                    "btnAction7": "btnAction7",
                    "btnAction8": "btnAction8",
                    "flxActions": "flxActions",
                    "flxBlankSpace1": "flxBlankSpace1",
                    "flxBlankSpace2": "flxBlankSpace2",
                    "flxCardDetails": "flxCardDetails",
                    "flxCardHeader": "flxCardHeader",
                    "flxCardImageAndCollapse": "flxCardImageAndCollapse",
                    "lblCardsSeperator": "lblCardsSeperator",
                    "flxCollapse": "flxCollapse",
                    "flxDetailsRow1": "flxDetailsRow1",
                    "flxDetailsRow10": "flxDetailsRow10",
                    "flxDetailsRow2": "flxDetailsRow2",
                    "flxDetailsRow3": "flxDetailsRow3",
                    "flxDetailsRow4": "flxDetailsRow4",
                    "flxDetailsRow5": "flxDetailsRow5",
                    "flxDetailsRow6": "flxDetailsRow6",
                    "flxDetailsRow7": "flxDetailsRow7",
                    "flxDetailsRow8": "flxDetailsRow8",
                    "flxDetailsRow9": "flxDetailsRow9",
                    "flxMyCards": "flxMyCards",
                    "flxMyCardsExpanded": "flxMyCardsExpanded",
                    "flxRowIndicatorColor": "flxRowIndicatorColor",
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator1": "lblSeparator1",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator": "lblSeperator",
                    "imgCard": "imgCard",
                    "imgCollapse": "imgCollapse",
                    "lblChevron":"lblChevron",
                    "lblCardHeader": "lblCardHeader",
                    "lblCardStatus": "lblCardStatus",
                    "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
                    "lblKey1": "lblKey1",
                    "lblKey10": "lblKey10",
                    "lblKey2": "lblKey2",
                    "lblKey3": "lblKey3",
                    "lblKey4": "lblKey4",
                    "lblKey5": "lblKey5",
                    "lblKey6": "lblKey6",
                    "lblKey7": "lblKey7",
                    "lblKey8": "lblKey8",
                    "lblKey9": "lblKey9",
                    "rtxValue1": "rtxValue1",
                    "rtxValue10": "rtxValue10",
                    "rtxValue2": "rtxValue2",
                    "rtxValue3": "rtxValue3",
                    "rtxValue4": "rtxValue4",
                    "rtxValue5": "rtxValue5",
                    "rtxValue6": "rtxValue6",
                    "rtxValue7": "rtxValue7",
                    "rtxValue8": "rtxValue8",
                    "rtxValue9": "rtxValue9",
                    "segMyCardsExpanded": "segMyCardsExpanded"
                };
                var cardsSegmentData = [];
                var card = {};
                cards.data = this.constructCardsViewModel(cards.data);
                var travelStatusData = cards.status;
                for (var i = 0; i < cards.data.length; i++) {
                    var dataItem = cards.data[i];
                    card = {
                        "lblCardsSeperator": {
                            "text": ".",
                            "height": "105px"
                        },
                        "flxCollapse": {
                            "onClick": self.changeRowTemplate
                        },
                        "flxRowIndicatorColor": {
                            "height": "190Px",
                            "skin": "sknFlxF4BA22"
                        },
                        "lblSeperator": ".",
                        "lblSeparator1": ".",
                        "lblSeparator2": ".",
                        "imgCard": {
                            "src": self.getImageForCard(dataItem.productName),
                        },
                        "imgCollapse": {
                            "src":ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig":{
                                "a11yLabel":"View Details"
                            }
                        },
                        "lblCardHeader": dataItem.productName,
                        "lblCardStatus": {
                            "text":self.geti18nDrivenString(dataItem.cardStatus),
                            "skin": self.statusSkinsLandingScreen[dataItem.cardStatus]
                        },
                        "template": (kony.application.getCurrentBreakpoint()==640|| orientationHandler.isMobile)?"flxMyCardsCollapsedMobile":"flxMyCardsCollapsed",
                        "flxDetailsRow1": {
                            "isVisible": true
                        },
                        "flxDetailsRow2": {
                            "isVisible": true
                        },
                        "flxDetailsRow3": {
                            "isVisible": true
                        },
                        "flxDetailsRow4": {
                            "isVisible": true
                        },
                        "flxDetailsRow5": {
                            "isVisible": true
                        },
                        "flxDetailsRow6": {
                            "isVisible": true
                        },
                        "flxDetailsRow7": {
                            "isVisible": true
                        },
                        "flxDetailsRow8": {
                            "isVisible": dataItem.secondaryCardHolder ? true : false
                        },
                        "flxDetailsRow9": {
                            "isVisible": false
                        },
                        "lblKey1": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":",
                        "lblKey9": kony.i18n.getLocalizedString("i18n.CardManagement.productName") + ":",
                        "lblKey2": kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough") + ":",
                        "lblKey3": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.DailyWithdrawalLimit") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit") + ":",
                        "lblKey4": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.transfers.accountName") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit") + ":",
                        "lblKey5": dataItem.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.common.accountNumber") + ":" : kony.i18n.getLocalizedString("i18n.CardManagement.BillingAddress") + ":",
                        "lblKey6": kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider") + ":",
                        "lblKey7": kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder") + ":",
                        "lblKey8": kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryHolder") + ":",
                        "rtxValue1": dataItem.maskedCardNumber,
                        "rtxValue9": dataItem.productName,
                        "rtxValue2": dataItem.validThrough,
                        "rtxValue3": dataItem.cardType === 'Debit' ? dataItem.dailyWithdrawalLimit : dataItem.creditLimit,
                        "rtxValue4": dataItem.cardType === 'Debit' ? dataItem.accountName : dataItem.availableCredit,
                        "rtxValue5": dataItem.cardType === 'Debit' ? dataItem.maskedAccountNumber : dataItem.billingAddress,
                        "rtxValue6": dataItem.serviceProvider,
                        "rtxValue7": dataItem.cardHolder,
                        "rtxValue8": dataItem.secondaryCardHolder,
                        "lblTravelNotificationEnabled": cards.status[i].status.toLowerCase() === "yes" ? kony.i18n.getLocalizedString('i18n.CardManagement.TravelNotificationsEnabled') : "",
                        "flxMyCards" :{
                          "clipBounds":false,
                          "skin":"sknFlxffffffShadowdddcdc",
                          "onClick": self.viewCardDetailsMobile
                        },
                        "lblChevron":{
                          "skin":"sknLblrightArrowFontIcon0273E3"
                        }
                    };
                    var actionButtonIndex;
                    for (var index = 0; index < dataItem.actions.length; index++) {
                        actionButtonIndex = Number(index) + 1;
                        card['btnAction' + actionButtonIndex] = self.getActionButton(dataItem, dataItem.actions[index]);
                    }
                    cardsSegmentData.push(card);
                }
                this.view.myCards.segMyCards.widgetDataMap = dataMap;
                this.view.myCards.segMyCards.setData(cardsSegmentData);
				this.view.myCards.segMyCards.setVisibility(true);
                this.view.flxApplyForNewCard.setVisibility(false);
            }            
        },

        /**
         * showCardsNotAvailableScreen - Method to show no cards screen.
         */
        showCardsNotAvailableScreen: function() {
            this.view.flxMyCardsView.setVisibility(true);
            this.view.myCards.segMyCards.setVisibility(false);
            this.view.myCards.flxNoCardsError.setVisibility(true);
            this.view.myCards.flxApplyForCards.setVisibility(true);
            this.view.myCards.lblNoCardsError.text = kony.i18n.getLocalizedString('i18n.CardsManagement.NocardsError');
            this.view.myCards.btnApplyForCard.onClick = function() {
                var naoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
                naoModule.presentationController.showNewAccountOpening();
            };
          
        },

        /**
         * getActionButton - Method to get a JSON for action button based on the action.
         * @param {Object, String} - card object.
         * @param {String} actino - contains the action to be performed.
         * @returns {Object} - JSON with text and onclick function for the button.
         */
        getActionButton: function(card, action) {
            return {
                'text': action,
                'toolTip': action,
                'onClick': this.getAction(card, action),
                'isVisible': true,
                'accessibilityconfig':{
                    'a11yLabel': action
                }
            };
        },

        /**
         * getAction - Method that actually returns the action to the action.
         * @param {Object, String} - card object .
         * @param {String} action - contains the action to be performed.
         * @returns {function} - Action for the given name.
         */
        getAction: function(card, action) {
            switch (action) {
                case kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"):
                    {
                        return this.lockCard.bind(this, card);
                    }
                case kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"):
                    {
                        return this.unlockCard.bind(this, card);
                    }
                case kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"):
                    {
                        return this.replaceCard.bind(this, card);
                    }
                case kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"):
                    {
                        return this.reportLost.bind(this, card);
                    }
                case kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"):
                    {
                        return this.cancelCard.bind(this, card);
                    }
                case kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"):
                    {
                        return this.changePin.bind(this, card);
                    }
            }
        },
      
        /**
         * Entry point for lock card flow.
         * @param {Object} - card object.
         */
        lockCard: function(card) {
            this.showLockCardView();
            this.setCardDetails(card);
            this.showLockCardGuidelines(card);
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"))
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * Sets the UI for lock card flow.
         */
        showLockCardView: function() {
            this.hideAllCardManagementViews();
            this.hideAllCardManagementRightViews();
            this.view.flxCardVerification.setVisibility(true);
            this.view.CardLockVerificationStep.setVisibility(true);
            this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
            this.view.CardLockVerificationStep.flxDeactivateCard.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * Binds the card details.
         * @param {Object} - Card object.
         */
        setCardDetails: function(card) {
            var self = this;
            this.view.CardLockVerificationStep.cardDetails.lblCardName.text = card.productName;
            this.view.CardLockVerificationStep.cardDetails.lblCardStatus.text = self.geti18nDrivenString(card.cardStatus);
            this.view.CardLockVerificationStep.cardDetails.lblCardStatus.skin = self.statusSkinsDetailsScreen[card.cardStatus];
            this.view.CardLockVerificationStep.cardDetails.imgCard.src = this.getImageForCard(card.productName);
            this.view.CardLockVerificationStep.cardDetails.lblKey1.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber") + ":";
            this.view.CardLockVerificationStep.cardDetails.rtxValue1.text = card.cardNumber;
            this.view.CardLockVerificationStep.cardDetails.lblKey2.text = kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough") + ":";
            this.view.CardLockVerificationStep.cardDetails.rtxValue2.text = card.validThrough;
            this.view.CardLockVerificationStep.cardDetails.lblKey3.text = card.cardType === 'Debit' ? kony.i18n.getLocalizedString("i18n.CardManagement.WithdrawalLimit") + ":" : kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit") + ":";
            this.view.CardLockVerificationStep.cardDetails.rtxValue3.text = card.cardType === 'Debit' ? card.dailyWithdrawalLimit : card.creditLimit;
            this.view.CardLockVerificationStep.cardDetails.flxDetailsRow4.setVisibility(card.cardType === 'Credit');
            this.view.CardLockVerificationStep.cardDetails.lblKey4.text = kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit") + ":";
            this.view.CardLockVerificationStep.cardDetails.rtxValue4.text = card.availableCredit;
            this.view.CardLockVerificationStep.cardDetails.lblCardHeader.text = card.productName;
            this.view.CardLockVerificationStep.cardDetails.rtxValueMobile.text = card.maskedCardNumber;
            this.view.CardLockVerificationStep.cardDetails.lblCardStatusMobile.text =  self.geti18nDrivenString(card.cardStatus);
            this.view.CardLockVerificationStep.cardDetails.lblCardStatusMobile.skin =  self.statusSkinsDetailsScreen[card.cardStatus];
        },
        /**
         * geti18nDrivenString - Returns i18n driven string for card status
         * @param {String} - card status.
         * @returns {String}  - i18n driven card status.
        */
        geti18nDrivenString:function(cardStatus){
            if(cardStatus===OLBConstants.CARD_STATUS.Active){
                 return kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE"); 
            }
            if(cardStatus===OLBConstants.CARD_STATUS.Inactive){
                return kony.i18n.getLocalizedString("i18n.CardManagement.inactive");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.Cancelled){
                return kony.i18n.getLocalizedString("i18n.CardManagement.cancelled");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.ReportedLost){
                return kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.ReplaceRequestSent){
                return kony.i18n.getLocalizedString("i18n.CardManagement.replaceRequestSent");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.CancelRequestSent){
                return kony.i18n.getLocalizedString("i18n.CardManagement.cancelRequestSent");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.Locked){
                return kony.i18n.getLocalizedString("i18n.CardManagement.locked");
            }
            if(cardStatus===OLBConstants.CARD_STATUS.Replaced){
                return kony.i18n.getLocalizedString("i18n.CardManagement.replaced");
            }
        },
        /**
         * Method to enable terms and conditions on lock card.
         */
        EnableTermsAndConditionsForLockCards: function() {
            var self = this;
            this.view.CardLockVerificationStep.CardActivation.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.height = (self.view.flxMain.frame.height + 320)+"dp";
                self.view.flxTermsAndConditionsPopUp.isVisible = true;
                self.view.lblTermsAndConditions.setFocus(true);
                if (CommonUtilities.isFontIconChecked(self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
            };
            this.view.btnCancel.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
            };
            this.view.btnSave.onClick = function() {
                if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(true, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(false, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                }
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
        },
        /**
         * showLockCardGuidelines - Shows the guidelines for Locking a card and sets flow actions.
         * @param {Object} - card object.
         */
        showLockCardGuidelines: function(card) {
            var self = this;
            self.EnableTermsAndConditionsForLockCards();
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
            this.view.CardLockVerificationStep.CardActivation.lblWarning.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber);
            this.view.CardLockVerificationStep.CardActivation.lblIns1.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline1");
            this.view.CardLockVerificationStep.CardActivation.lblIns2.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline2");
            this.view.CardLockVerificationStep.CardActivation.lblIns3.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline3");
            this.view.CardLockVerificationStep.CardActivation.lblIns4.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline4");
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
                    'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
                },
                'btnModify': {
                    'isVisible': false
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            FormControllerUtility.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            CommonUtilities.setLblCheckboxState(false, this.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
            this.view.CardLockVerificationStep.CardActivation.flxCheckbox.onTouchEnd = function() {
                CommonUtilities.toggleFontCheckbox(self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                if (CommonUtilities.isFontIconChecked(self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon)) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(true, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(false, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                }
            };
            var params = {
                'card': card
            };
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards();
            };
          if (CommonUtilities.isCSRMode()) {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
            	this.view.CardLockVerificationStep.CardActivation.flxCheckbox.onTouchEnd = function() {
                CommonUtilities.toggleFontCheckbox(self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                if (CommonUtilities.isFontIconChecked(self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon)) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(true, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                    CommonUtilities.setLblCheckboxState(false, self.view.CardLockVerificationStep.CardActivation.lblRememberMeIcon);
                }
            }.bind(this);
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = self.initMFAFlow.bind(this, params, kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));

            }
            
        },
      
        alignConfirmButtons: function(buttonsJSON){
            if(buttonsJSON.btnModify.isVisible){
              this.view.CardLockVerificationStep.confirmButtons.btnCancel.left = '30.33%';
			  if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile)
                this.view.CardLockVerificationStep.confirmButtons.height = "150dp";
              else
                this.view.CardLockVerificationStep.confirmButtons.height = "100dp";
            }else{
              this.view.CardLockVerificationStep.confirmButtons.btnCancel.left = '53.41%';
              this.view.CardLockVerificationStep.confirmButtons.height = "100dp";
            }
            this.view.CardLockVerificationStep.confirmButtons.btnConfirm.setVisibility(buttonsJSON.btnConfirm.isVisible);
            this.view.CardLockVerificationStep.confirmButtons.btnConfirm.text = buttonsJSON.btnConfirm.text;
            this.view.CardLockVerificationStep.confirmButtons.btnConfirm.toolTip = buttonsJSON.btnConfirm.toolTip;
            this.view.CardLockVerificationStep.confirmButtons.btnModify.setVisibility(buttonsJSON.btnModify.isVisible);
            this.view.CardLockVerificationStep.confirmButtons.btnModify.text = buttonsJSON.btnModify.text;
            this.view.CardLockVerificationStep.confirmButtons.btnModify.toolTip = buttonsJSON.btnModify.toolTip;
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.setVisibility(buttonsJSON.btnCancel.isVisible);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.text = buttonsJSON.btnCancel.text;
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.toolTip = buttonsJSON.btnCancel.toolTip;
            this.view.forceLayout();
        },
      
        /**
         * unlockCard - Entry point for unlock card flow.
         * @param {Object} - card object.
         */
        unlockCard: function(card) {
            this.showUnlockCardGuidelines(card);
            this.view.forceLayout();
        },

        /**
         * showUnlockCardViewAndShowMFAScreen - Sets the UI for unlock card flow.
         * @param {Object} params - contains the card details.
         * @param {String} action - contains the action to be performed.
         */
        showUnlockCardViewAndShowMFAScreen: function(params, action) {
            this.hideAllCardManagementViews();
            this.hideAllCardManagementRightViews();
            this.view.flxActivateCard.setVisibility(false);
            this.view.flxCardVerification.setVisibility(true);
            this.view.CardLockVerificationStep.setVisibility(true);
            this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
            this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.setCardDetails(params.card);
            this.showMFAScreen(params, kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
        * Method used to show the unlock card guidelines screen.
        * @param {Object} card - contains the card object.
        */
        showUnlockCardGuidelines: function(card) {
            var self = this;
            this.hideAllCardManagementViews();
            this.hideAllCardManagementRightViews();
            this.view.flxActivateCard.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            self.EnableTermsAndConditionsForUnLockCards();
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
            this.view.CardActivation.lblWarning.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber);
            this.view.CardActivation.lblHeading2.text = kony.i18n.getLocalizedString("i18n.CardManagement.BenefitsOfUnlockingTheCard");
            this.view.CardActivation.lblIns1.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit1");
            this.view.CardActivation.lblIns2.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit2");
            this.view.CardActivation.lblIns3.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit3");
            this.view.CardActivation.flxFour.setVisibility(false);
            FormControllerUtility.disableButton(this.view.CardActivation.btnProceed);
            CommonUtilities.setLblCheckboxState(false, this.view.CardActivation.lblRememberMeIcon);
            this.view.CardActivation.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            this.view.CardActivation.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
            if (CommonUtilities.isCSRMode()) {
              this.view.CardActivation.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.CardActivation.btnProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
               this.view.CardActivation.flxCheckbox.onTouchEnd = function() {
                CommonUtilities.toggleFontCheckbox(self.view.CardActivation.lblRememberMeIcon);
                if (CommonUtilities.isFontIconChecked(self.view.CardActivation.lblRememberMeIcon)) {
                    FormControllerUtility.enableButton(self.view.CardActivation.btnProceed);
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    FormControllerUtility.disableButton(self.view.CardActivation.btnProceed);
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
            }.bind(this);
            	  this.view.CardActivation.btnProceed.onClick = function() {
                var params = {
                    'card': card
                };
                self.initMFAFlow.call(self, params,kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
            }.bind(this);
            }
          
            this.view.forceLayout();
        },
        /**
         * EnableTermsAndConditionsForUnLockCards - Method to enable terms and conditions on unlock card.
         */
        EnableTermsAndConditionsForUnLockCards: function() {
            var self = this;
            this.view.CardActivation.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.height = (self.view.flxMain.frame.height + 270)+"dp";
                self.view.flxTermsAndConditionsPopUp.isVisible = true;
                self.view.lblTermsAndConditions.setFocus(true);
                if (CommonUtilities.isFontIconChecked(self.view.CardActivation.lblRememberMeIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
            };
            this.view.btnCancel.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
            };
            this.view.btnSave.onClick = function() {
                if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.CardActivation.lblRememberMeIcon);
                    FormControllerUtility.enableButton(self.view.CardActivation.btnProceed);
                } else {
                    FormControllerUtility.disableButton(self.view.CardActivation.btnProceed);
                    CommonUtilities.setLblCheckboxState(false, self.view.CardActivation.lblRememberMeIcon);
                }
                self.view.flxTermsAndConditionsPopUp.isVisible = false;
            };
        },
      
        /**
        * Method used as the entry point for report lost screen.
        * @param {Object} card - contains the card object.
        */
        reportLost: function(card) {
            this.showReportLostView();
            this.setCardDetails(card);
            this.showReportLostGuidelines(card);
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolen"));
            this.AdjustScreen();
            this.view.forceLayout();
        },
      
        /**
        * Method used to show report lost view.
        */
        showReportLostView: function() {
            this.hideAllCardManagementViews();
            this.hideAllCardManagementRightViews();
            this.view.flxCardVerification.setVisibility(true);
            this.view.CardLockVerificationStep.setVisibility(true);
            this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
            this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.CardLockVerificationStep.WarningMessage.flxIAgree.setVisibility(false);
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
        /**
        * Method used to set report card lost guidelines.
        */
        showReportLostGuidelines: function(card) {
            var self = this;
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolen"));
            this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolen");
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline1");
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline2");
            this.view.CardLockVerificationStep.WarningMessage.rtxWarningText3.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline3");
            this.view.CardLockVerificationStep.WarningMessage.flxWarningText4.setVisibility(false);
            this.view.CardLockVerificationStep.lblReason2.setVisibility(true);
            this.view.CardLockVerificationStep.lblReason2.text = kony.i18n.getLocalizedString("i18n.CardManagement.PleaseEnterTheReasonMessage");
            this.view.CardLockVerificationStep.lbxReason2.setVisibility(true);
            var reasonsMasterData = [];
            reasonsMasterData.push([OLBConstants.CARD_REPORTLOST_REASON.LOST, OLBConstants.CARD_REPORTLOST_REASON.LOST], [OLBConstants.CARD_REPORTLOST_REASON.STOLEN, OLBConstants.CARD_REPORTLOST_REASON.STOLEN]);
            this.view.CardLockVerificationStep.lbxReason2.masterData = reasonsMasterData;
            this.view.CardLockVerificationStep.lbxReason2.selectedKey = OLBConstants.CARD_REPORTLOST_REASON.LOST;
            this.view.CardLockVerificationStep.tbxNoteOptional.text = "";
            this.view.CardLockVerificationStep.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
            this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(false);
            this.view.CardLockVerificationStep.flxAddress.setVisibility(false);
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
                    'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
                },
                'btnModify': {
                    'isVisible': false
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
           if (CommonUtilities.isCSRMode()) {
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
               this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                var params = {
                    'card': card,
                    'Reason': self.view.CardLockVerificationStep.lbxReason2.selectedKey,
                    'notes': self.view.CardLockVerificationStep.tbxNoteOptional.text
                };
                self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
            }.bind(this);
            }
         
        },
      
        /**
        * Method used as entry point method for change pin flow.
        * @param {Object} card - contains the card object.
        */
        changePin: function(card) {
            this.showChangePinView();
            this.setCardDetails(card);
            if (card.cardType === 'Credit') {
                this.startOfflineChangePinFlow(card);
            } else if (card.cardType === 'Debit') {
                this.startOnlineChangePinFlow(card);
            }
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"))
            this.view.forceLayout();
            this.AdjustScreen();
        },

        /**
        * Method used to show change pin view.
        */
        showChangePinView: function() {
            this.hideAllCardManagementViews();
            this.view.flxCardVerification.setVisibility(true);
            this.view.CardLockVerificationStep.setVisibility(true);
            this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.CardLockVerificationStep.WarningMessage.flxIAgree.setVisibility(false); 
            this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.flxIAgree.setVisibility(false);
            this.view.CardLockVerificationStep.warning.flxIAgree.setVisibility(false);
            this.AdjustScreen();
        },

        /**
        * Method used to start online change pin flow.
        * @param {Object} card - contains the card object.
        */
        startOnlineChangePinFlow: function(card) {
            var self = this;
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin"));
            this.hideAllCardManagementRightViews();
            this.view.CardLockVerificationStep.flxConfirmPIN.setVisibility(true);
            this.view.CardLockVerificationStep.warning.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline1");
            this.view.CardLockVerificationStep.warning.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline2");
            this.view.CardLockVerificationStep.warning.flxWarningText3.setVisibility(false);
            this.view.CardLockVerificationStep.warning.flxWarningText4.setVisibility(false);
            this.view.CardLockVerificationStep.lblReason.setVisibility(false);
            this.view.CardLockVerificationStep.lbxReason.setVisibility(false);
            this.view.CardLockVerificationStep.tbxCurrentPIN.text = "";
            this.view.CardLockVerificationStep.tbxNewPIN.text = "";
            this.view.CardLockVerificationStep.imgNewPIN.setVisibility(false);
            this.view.CardLockVerificationStep.tbxConfirmPIN.text = "";
            this.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
            this.view.CardLockVerificationStep.tbxNote.text = "";
            this.view.CardLockVerificationStep.tbxNote.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.CardLockVerificationStep.tbxConfirmPIN.secureTextEntry = false;
            FormControllerUtility.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.common.proceed"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.common.proceed")
                },
                'btnModify': {
                    'isVisible': false,
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            if (CommonUtilities.isCSRMode()) {
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
            	this.view.CardLockVerificationStep.tbxCurrentPIN.onKeyUp = function() {
                var enteredPin = self.view.CardLockVerificationStep.tbxCurrentPIN.text;
                if (self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxNewPIN.text) && self.view.CardLockVerificationStep.tbxNewPIN.text === self.view.CardLockVerificationStep.tbxConfirmPIN.text) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
            }.bind(this);
            this.view.CardLockVerificationStep.tbxNewPIN.onKeyUp = function() {
                var enteredPin = self.view.CardLockVerificationStep.tbxNewPIN.text;
                if (self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxCurrentPIN.text) && enteredPin === self.view.CardLockVerificationStep.tbxConfirmPIN.text) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
                if (self.isValidPin(enteredPin) && enteredPin === self.view.CardLockVerificationStep.tbxConfirmPIN.text) {
                    self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(true);
                    self.view.CardLockVerificationStep.imgConfirmPIN.src = 'success_green.png';
                    self.view.forceLayout();
                } else {
                    self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
                }
                if (self.isValidPin(enteredPin)) {
                    self.view.CardLockVerificationStep.imgNewPIN.setVisibility(true);
                    self.view.CardLockVerificationStep.imgNewPIN.src = 'success_green.png';
                    self.view.forceLayout();
                } else {
                    self.view.CardLockVerificationStep.imgNewPIN.setVisibility(false);
                }
            }.bind(this);
            this.view.CardLockVerificationStep.tbxConfirmPIN.onKeyUp = function() {
                var enteredPin = self.view.CardLockVerificationStep.tbxConfirmPIN.text;
                if (self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxCurrentPIN.text) && enteredPin === self.view.CardLockVerificationStep.tbxNewPIN.text) {
                    FormControllerUtility.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                } else {
                    FormControllerUtility.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
                }
                if (self.isValidPin(enteredPin) && enteredPin === self.view.CardLockVerificationStep.tbxNewPIN.text) {
                    self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(true);
                    self.view.CardLockVerificationStep.imgConfirmPIN.src = 'success_green.png';
                    self.view.forceLayout();
                } else {
                    self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
                }
            }.bind(this);
                this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'reason': self.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey,
                        'notes': self.view.CardLockVerificationStep.tbxNote.text,
                        'newPin': self.view.CardLockVerificationStep.tbxConfirmPIN.text
                    };
                    self.initMFAFlow(params, kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
                }.bind(this);
            }
            this.view.forceLayout();
            this.AdjustScreen();
        },
      
        /**
        * Method used to check if the entered pin is valid or not.
        * @param {String} pin - contains the entered pin.
        */
        isValidPin: function(pin) {
            var regex = new RegExp('^[0-9]{6,6}$');
            if (regex.test(pin)) {
                for (var i = 1; i < pin.length; i++) {
                    if (Number(pin[i]) - 1 !== Number(pin[i - 1])) {
                        return true;
                    }
                }
            }
            return false;
        },
      
        /**
        * Method used to start teh offline change pin flow.
        * @param {Object} card - contains the card object.
        */
        startOfflineChangePinFlow: function(card) {
            var self = this;
            var selectedOption = "E-mail ID";
            this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin"));
            this.hideAllCardManagementRightViews();
            this.view.CardLockVerificationStep.flxChangeCardPin.setVisibility(true);
            this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline1");
            this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline2");
            this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.flxWarningText3.setVisibility(false);
            this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.flxWarningText4.setVisibility(false);
            var reasonsMasterData = [];
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED, OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED]);
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN, OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN]);
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.OTHER, OLBConstants.CARD_CHANGE_PIN_REASON.OTHER]);
            this.view.CardLockVerificationStep.lbxReasonPinChange.masterData = reasonsMasterData;
            this.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey = OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED;
            this.view.CardLockVerificationStep.tbxNotePinChange.text = "";
            this.view.CardLockVerificationStep.tbxNotePinChange.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.CardLockVerificationStep.lblOption1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EmailId");
            this.view.CardLockVerificationStep.lblOption2.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumbers");
            this.view.CardLockVerificationStep.lblOption3.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.postalAddress");
            this.view.CardLockVerificationStep.lblCheckBox1.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
            this.view.CardLockVerificationStep.lblCheckBox1.text = "M";
            this.view.CardLockVerificationStep.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
            this.view.CardLockVerificationStep.lblCheckBox2.text = "L";
            this.view.CardLockVerificationStep.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
            this.view.CardLockVerificationStep.lblCheckBox3.text = "L";
            this.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + applicationManager.getUserPreferencesManager().getUserEmail();;
            this.view.CardLockVerificationStep.flxCheckBox1.onTouchEnd = function() {
                self.view.CardLockVerificationStep.lblCheckBox1.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.CardLockVerificationStep.lblCheckBox1.text = "M";
                self.view.CardLockVerificationStep.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox2.text = "L";
                self.view.CardLockVerificationStep.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox3.text = "L";
                selectedOption = "E-mail ID";
                self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + applicationManager.getUserPreferencesManager().getUserEmail();;
            };
            this.view.CardLockVerificationStep.flxCheckBox2.onTouchEnd = function() {
                self.view.CardLockVerificationStep.lblCheckBox1.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox1.text = "L";
                self.view.CardLockVerificationStep.lblCheckBox2.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.CardLockVerificationStep.lblCheckBox2.text = "M";
                self.view.CardLockVerificationStep.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox3.text = "L";
                selectedOption = "Phone No";
                self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredPhoneNo") + " " + applicationManager.getUserPreferencesManager().getUserPhone();
            };
            this.view.CardLockVerificationStep.flxCheckBox3.onTouchEnd = function() {
                self.view.CardLockVerificationStep.lblCheckBox1.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox1.text = "L";
                self.view.CardLockVerificationStep.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.CardLockVerificationStep.lblCheckBox2.text = "L";
                self.view.CardLockVerificationStep.lblCheckBox3.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.CardLockVerificationStep.lblCheckBox3.text = "M";
                selectedOption = 'Postal Address';
                self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredAddress") + " " + card.billingAddress;
            };
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.common.proceed"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.common.proceed")
                },
                'btnModify': {
                    'isVisible': false,
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            FormControllerUtility.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
           this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            if (CommonUtilities.isCSRMode()) {
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else{
                 this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                var params = {
                    'card': card,
                    'CardAccountNumber': card.maskedCardNumber,
                    'CardAccountName': card.productName,
                    'RequestReason': self.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey,
                    'AdditionalNotes': self.view.CardLockVerificationStep.tbxNotePinChange.text,
                    'AccountType': 'CARD',
                    'RequestCode': 'NEW_PIN',
                    'Channel': OLBConstants.Channel,
                    'userName': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.getUserName()
                };
                if (selectedOption === 'Postal Address') {
                    params['Address_id'] = self.getSelectedAddressId();
                } else if (selectedOption === "Phone No") {
                    var contactNumbers = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchUserPhoneNumbers();
                    params['communication_id'] = contactNumbers.filter(function(item) {
                        return item.isPrimary === "true"
                    })[0].id;
                } else {
                    var emailids = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchUserEmailIds();
                    params['communication_id'] = emailids.filter(function(item) {
                        return item.isPrimary === "true"
                    })[0].id;
                }
                self.initMFAFlow(params, "Offline_Change_Pin");
            } .bind(this);
            }
            this.view.forceLayout();
            this.AdjustScreen();
        },
        
        /**
        * Method used to change the text to title case.
        * @param {String} str - string to be changed to title case.
        */
        toTitleCase: function(str) {
            return str.replace(/\w\S*/g, function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        },
      
        /**
        * Method used to change the radio button selection in the replace card flow.
        * @param {Object} radioButtons - contains the radio buttons list.
        * @param {Object} selectedradioButton - contains the raido button selected widget Object.
        */
        onRadioButtonSelection: function(radioButtons, selectedradioButton) {
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox1") {
                this.view.CardLockVerificationStep.lblAddressCheckBox1.text = "M";
                this.view.CardLockVerificationStep.lblAddressCheckBox1.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.CardLockVerificationStep.lblAddressCheckBox2.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox2.skin = "sknC0C0C020pxolbfonticons";
                this.view.CardLockVerificationStep.lblAddressCheckBox3.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox3.skin = "sknC0C0C020pxolbfonticons";
            }
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox2") {
                this.view.CardLockVerificationStep.lblAddressCheckBox2.text = "M";
                this.view.CardLockVerificationStep.lblAddressCheckBox2.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.CardLockVerificationStep.lblAddressCheckBox1.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox1.skin = "sknC0C0C020pxolbfonticons";
                this.view.CardLockVerificationStep.lblAddressCheckBox3.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox3.skin = "sknC0C0C020pxolbfonticons";
            }
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox3") {
                this.view.CardLockVerificationStep.lblAddressCheckBox3.text = "M";
                this.view.CardLockVerificationStep.lblAddressCheckBox3.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.CardLockVerificationStep.lblAddressCheckBox1.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox1.skin = "sknC0C0C020pxolbfonticons";
                this.view.CardLockVerificationStep.lblAddressCheckBox2.text = "L";
                this.view.CardLockVerificationStep.lblAddressCheckBox2.skin = "sknC0C0C020pxolbfonticons";
            }
        },

        /**
         * Validates given secure access code.
         * @param {String} secureaccesscode - contains the secure access code entered in form.
         */
        isValidSecureAccessCode: function(secureaccesscode) {
            if (secureaccesscode.length !== OLBConstants.OTPLength) return false;
            var regex = new RegExp('^[0-9]+$');
            return regex.test(secureaccesscode);
        },
      
        /**
         * Method to show travel notification acknowledgement for create/update travel notification 
         * @param {Object} - notification object
         */
        showTravelNotificationAcknowledgement: function(response) {
            var self = this;
            var data = this.notificationObject;
            var cards = "";
            self.hideAllCardManagementViews();
            this.view.flxPrint.setVisibility(false);
            this.view.flxPrint.setVisibility(false);
            this.view.ConfirmDialog.keyValueCreditLimit.setVisibility(false);
            this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(false);
            this.view.flxAcknowledgment.setVisibility(true);
            self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.manageTravelPlanAcknowledgement'));
            this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotification');
            if (this.notificationObject.isEditFlow) {
                this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationUpdateMsg');
                this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId') + ":" + this.notificationObject.requestId;
            } else {
                this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationCreatationMsg');
                this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId') + ":" + response.data.request_id;
            }
            this.view.ConfirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelDetails');
            this.view.ConfirmDialog.flxDestination.setVisibility(true);
            this.view.ConfirmDialog.flxSelectCards.setVisibility(true);
            this.view.ConfirmDialog.flxDestination2.setVisibility(false);
            this.view.ConfirmDialog.flxDestination3.setVisibility(false);
            this.view.ConfirmDialog.flxDestination4.setVisibility(false);
            this.view.ConfirmDialog.flxDestination5.setVisibility(false);
            this.view.ConfirmDialog.keyValueCardHolder.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate');
            this.view.ConfirmDialog.keyValueCardHolder.lblValue.text = data.fromDate;
            this.view.ConfirmDialog.keyValueCardName.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate');
            this.view.ConfirmDialog.keyValueCardName.lblValue.text = data.toDate;
            this.view.ConfirmDialog.flxDestination1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1');
            this.view.ConfirmDialog.rtxDestination1.text = data.locations[0];
            if (data.locations[1]) {
                this.view.ConfirmDialog.flxDestination2.setVisibility(true);
                this.view.ConfirmDialog.lblDestination2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2');
                this.view.ConfirmDialog.rtxDestination2.text = data.locations[1];
            }
            if (data.locations[2]) {
                this.view.ConfirmDialog.flxDestination3.setVisibility(true);
                this.view.ConfirmDialog.lblDestination3.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3');
                this.view.ConfirmDialog.rtxDestination3.text = data.locations[2];
            }
            if (data.locations[3]) {
                this.view.ConfirmDialog.flxDestination4.setVisibility(true);
                this.view.ConfirmDialog.lblDestination4.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4');
                this.view.ConfirmDialog.rtxDestination4.text = data.locations[3];
            }
            if (data.locations[4]) {
                this.view.ConfirmDialog.flxDestination5.setVisibility(true);
                this.view.ConfirmDialog.lblDestination5.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5');
                this.view.ConfirmDialog.rtxDestination5.text = data.locations[4];
            }
            this.view.ConfirmDialog.keyValueValidThrough.lblKey.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.PhoneNumber');
            this.view.ConfirmDialog.keyValueValidThrough.lblValue.text = data.phone;
            this.view.ConfirmDialog.keyValueServiceProvider.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation');
            this.view.ConfirmDialog.keyValueServiceProvider.lblValue.text = data.notes;
            this.view.ConfirmDialog.lblKey6.text = kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards');
            data.selectedcards.map(function(dataItem) {
                cards = cards + dataItem.name + "-" + dataItem.number + "<br/>";
            });
            this.view.ConfirmDialog.rtxValueA.text = cards;
            this.view.btnBackToCards.setVisibility(true);
            this.view.btnRequestReplacement.setVisibility(true);
            this.view.btnRequestReplacement.text = kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards');
            this.view.btnRequestReplacement.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards');
            this.view.btnRequestReplacement.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            this.view.btnBackToCards.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            this.view.btnBackToCards.text = kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan');
            this.view.btnBackToCards.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan');
            this.view.btnBackToCards.onClick = function() {
                self.fetchTravelNotifications();
            }
            this.notificationObject = {};
            CommonUtilities.hideProgressBar(this.view);
            this.view.forceLayout();
            this.AdjustScreen();
        },
  
        /**
         * Shows the MFA Options available.
         * @param {Object} - card object .
         * @param {String} action - contains the action to be performed.
         */
        showMFAScreen: function(params, action) {
            var self = this;
            if (action === kony.i18n.getLocalizedString("i18n.CardManagement.LockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardVerification"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin") || action === "Offline_Change_Pin") {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinVerification"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardVerification"));
            } else if (action === kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenVerification"));
                this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardVerification");
            } else if (action === kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard")) {
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardVerification"));
            } else if(action === kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard")){
                this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelVerification"));
            }
            this.hideServerError();
            this.view.flxTermsAndConditions.setVisibility(false);
            var selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE;
            self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
            self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
            if (applicationManager.getConfigurationManager().isSecurityQuestionConfigured === "true") {
                this.view.CardLockVerificationStep.flxUsernameVerificationOption2.setVisibility(true);
            } else {
                this.view.CardLockVerificationStep.flxUsernameVerificationOption2.setVisibility(false);
            }
            this.hideAllCardManagementRightViews();
            this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(true);
            FormControllerUtility.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            this.view.CardLockVerificationStep.flxUsernameVerificationRadioOption1.onTouchEnd = function() {
                self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
                self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = ViewConstants.IMAGES.ICON_RADIOBTN;
                selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE;
            };
            this.view.CardLockVerificationStep.flxUsernameVerificationRadiobtnOption2.onTouchEnd = function() {
                self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = ViewConstants.IMAGES.ICON_RADIOBTN;
                self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = ViewConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
                selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURITY_QUESTIONS;
            };
            var buttonsJSON = {
                'btnConfirm': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
                    'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
                },
                'btnModify': {
                    'isVisible': false
                },
                'btnCancel': {
                    'isVisible': true,
                    'text': kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                    'toolTip': kony.i18n.getLocalizedString("i18n.transfers.Cancel")
                },
            };
            this.alignConfirmButtons(buttonsJSON);
            this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards();
            };
            this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function() {
                if (selectedMFAOption === OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE) {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.sendSecureAccessCode(params, action);
                } else if (selectedMFAOption === OLBConstants.MFA_OPTIONS.SECURITY_QUESTIONS) {
                    FormControllerUtility.showProgressBar(self.view);
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.fetchSecurityQuestions(params, action);
                }
            };
            this.view.forceLayout();
            this.AdjustScreen();
        },

        /**
         * Returns the contact for which the isPrimary flag is true. Expects only one of the contacts to be primary. If there are more than one, returns the last.
         * @param {Array} - Array of contacts.  
         */
        getPrimaryContact: function(contacts) {
            var primaryContact = "";
            contacts.forEach(function(item) {
                if (item.isPrimary === "true") {
                    primaryContact = item.Value;
                }
            });
            return primaryContact;
        },
        
      constructCardsViewModel : function(cards){
        var self = this;
        var cardsViewModel = [];
        cards.forEach(function(card){
          if(card.cardType === "Debit"){
            cardsViewModel.push(self.getDebitCardViewModel(card));
          }
          else if(card.cardType === "Credit"){
            cardsViewModel.push(self.getCreditCardViewModel(card));
          }
        });
        return cardsViewModel;
    },
    
    /**
     * getDebitCardViewModel - Generates a viewModel for the given debit card.
     * @param {Object} - Debit card object.
     * @returns {Object}  - constructed view model for debit card.
     */
    getDebitCardViewModel : function(debitCard){
      var mfaManager = applicationManager.getMFAManager();
       mfaManager.setServiceId("SERVICE_ID_40");
        var debitCardViewModel = [];
        var debitCardActions = [];
        debitCardViewModel.cardId = debitCard.cardId;
        debitCardViewModel.cardType = debitCard.cardType;
        debitCardViewModel.cardStatus = debitCard.cardStatus;
        debitCardViewModel.cardNumber = debitCard.cardNumber;
        debitCardViewModel.maskedCardNumber = debitCard.maskedCardNumber;
        debitCardViewModel.productName = debitCard.cardProductName;
        debitCardViewModel.validThrough = this.getValidThroughForCard(debitCard.expiryDate);
        debitCardViewModel.dailyWithdrawalLimit = CommonUtilities.formatCurrencyWithCommas(debitCard.withdrawlLimit,false,debitCard.currencyCode);
        debitCardViewModel.accountName = debitCard.accountName;
        debitCardViewModel.maskedAccountNumber = debitCard.maskedAccountNumber;
        debitCardViewModel.serviceProvider = debitCard.serviceProvider;
        debitCardViewModel.cardHolder = debitCard.cardHolderName;
        debitCardViewModel.secondaryCardHolder = debitCard.secondaryCardHolder;
      
        switch(debitCard.cardStatus){
            case "Active": {
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
              break;
            }
            case "Locked": {
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              break;
            }
            case "Reported Lost": {
              //TODO:  
              break;
            }
            case "Replaced":{
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              break;
            }
            case "Replace Request Sent":{
              debitCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              break;
            }
            default: {
              break;
            }
        }
        debitCardViewModel.actions = debitCardActions;
        return debitCardViewModel;
    },
    
    /**
     * getValidThroughForCard - Generates a validThrough/expiry date for a given timestamp.
     * @param {String} - Expiry Date Timestamp.
     * @returns {String}  - Expiry Date in mm/yy format.
     */
    getValidThroughForCard : function(expiryDate){
        if(expiryDate === null || expiryDate === undefined || expiryDate === "")
          return "";
        expiryDate = expiryDate.split('T')[0];
        expiryDate = expiryDate.split('-');
        return expiryDate[1]+'/'+expiryDate[0].slice(2);
    },

    /**
     * getCreditCardViewModel - Generates a viewModel for the given credit card.
     * @param {Object} - credit card object.
     * @returns {Object}  - constructed view model for credit card.
     */
    getCreditCardViewModel : function(creditCard){
      var mfaManager = applicationManager.getMFAManager();
        var creditCardViewModel = [];
        var creditCardActions = [];
        creditCardViewModel.cardId = creditCard.cardId;
        creditCardViewModel.cardType = creditCard.cardType;
        creditCardViewModel.cardStatus = creditCard.cardStatus;
        creditCardViewModel.cardNumber = creditCard.cardNumber;
        creditCardViewModel.maskedCardNumber = creditCard.maskedCardNumber;
        creditCardViewModel.productName = creditCard.cardProductName;
        creditCardViewModel.validThrough = this.getValidThroughForCard(creditCard.expiryDate);
        creditCardViewModel.creditLimit = CommonUtilities.formatCurrencyWithCommas(creditCard.creditLimit,false,creditCard.currencyCode);
        creditCardViewModel.availableCredit = CommonUtilities.formatCurrencyWithCommas(creditCard.availableCredit,false,creditCard.currencyCode);
        creditCardViewModel.serviceProvider = creditCard.serviceProvider;
        creditCardViewModel.cardHolder = creditCard.cardHolderName;
        creditCardViewModel.secondaryCardHolder = creditCard.secondaryCardHolderName;
        creditCardViewModel.billingAddress = creditCard.billingAddress;
        
        switch(creditCard.cardStatus){
            case "Active": {
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
              creditCardActions.push( kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
              break;
            }
            case "Locked": {
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              creditCardActions.push( kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
              break;
            }
            case "Reported Lost": {
              creditCardActions.push( kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
              break;
            }
            case "Replaced":{
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              creditCardActions.push( kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
              break;
            }
            case "Replace Request Sent":{
              creditCardActions.push(kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
              creditCardActions.push( kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
              break;
            }
            default: {
              break;
            }
        }
        creditCardViewModel.actions = creditCardActions;
        return creditCardViewModel;
    },
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmCardManagementController}
    * @param {integer} width - current browser width
    * @return {} 
    * @throws {}
    */
    onBreakpointChange: function(width){
      kony.print('on breakpoint change');
      orientationHandler.onOrientationChange(this.onBreakpointChange);
        this.view.customheader.onBreakpointChange(width);
        this.setupFormOnTouchEnd(width);

      var scope = this;
      var data;
      this.view.breadcrumb.setVisibility(false);
      var responsiveFonts = new ResponsiveFonts();
      this.view.flxTravelPlan.skin = "slFbox";
      this.AdjustScreen();

      if(width===640 || orientationHandler.isMobile){
        this.view.customheader.lblHeaderMobile.text = "My Cards";
        responsiveFonts.setMobileFonts();
        this.view.CardLockVerificationStep.cardDetails.skin="slFbox";
        this.view.CardLockVerificationStep.confirmButtons.btnCancel.skin = "sknBtnffffffBorder0273e31pxRadius2px";
        data = this.view.myCards.segMyCards.data;
        if(data!==undefined){
          data.forEach(function(e){
            e.template = "flxMyCardsCollapsedMobile";
            e.flxMyCards = {
              "clipBounds":false,
              "skin":"sknFlxffffffShadowdddcdc",
              "onClick": scope.viewCardDetailsMobile
            }
          });
          
          scope.view.myCards.segMyCards.setData(data);
        }
        scope.view.flxMyCardsView.isVisible = true;
      }else{
        this.view.customheader.lblHeaderMobile.text = "";
        responsiveFonts.setDesktopFonts();
        
        data = this.view.myCards.segMyCards.data;
        if(data!==undefined){
          data.forEach(function(e){
            e.template = "flxMyCardsCollapsed";
            e.flxCollapse = {
              "onClick":scope.changeRowTemplate
            }
          });
          scope.view.myCards.segMyCards.setData(data);
        }
        
      }
      this.AdjustScreen();
    },

    setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();   
              } 
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
    },
      nullifyPopupOnTouchStart: function(){
      },
    /**
    * viewCardDetailsMobile : Goes to view card details flex, mobile only function
    * @member of {frmCardManagementController}
    * @param {}
    * @return {} 
    * @throws {}
    */
    viewCardDetailsMobile: function(){
        if(kony.application.getCurrentBreakpoint()!=640 || !orientationHandler.isMobile){
            return;
        }

      if(this.view.flxCardDetailsMobile.isVisible == true){
        return;
      }
      var scope = this;
      var index = this.view.myCards.segMyCards.selectedIndex;
      var rowIndex = index[1];
      var data = [];
      data.push(this.view.myCards.segMyCards.data[rowIndex]);
      data[0].template = "flxMyCardsExpandedMobile"
      data[0].flxMyCards = {
        "onClick" : null
      };
      data[0].flxBlankSpace2 = {
        "isVisible":true
      };
      if(data[0].btnAction1 != undefined){
        var action1 = data[0].btnAction1.onClick;
        data[0].btnAction1 = {
        "text":data[0].btnAction1.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action1();
          scope.view.flxHeader.setFocus(true);
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction2 != undefined){
        var action2 = data[0].btnAction2.onClick;
        data[0].btnAction2 = {
        "text":data[0].btnAction2.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action2();
          scope.view.flxHeader.setFocus(true);
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction3 != undefined){
        var action3 = data[0].btnAction3.onClick;
        data[0].btnAction3 = {
        "text":data[0].btnAction3.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action3();
          scope.view.flxHeader.setFocus(true);
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction4 != undefined){
        var action4 = data[0].btnAction4.onClick;
        data[0].btnAction4 = {
        "text":data[0].btnAction4.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action4();
          scope.view.flxHeader.setFocus(true);
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction5 != undefined){
        var action5 = data[0].btnAction5.onClick;
        data[0].btnAction5 = {
        "text":data[0].btnAction5.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action5();
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction6 != undefined){
        var action6 = data[0].btnAction6.onClick;
        data[0].btnAction6 = {
        "text":data[0].btnAction6.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action6();
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction7 != undefined){
        var action7 = data[0].btnAction7.onClick;
        data[0].btnAction7 = {
        "text":data[0].btnAction7.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action7();
          scope.AdjustScreen();
        }
      };
      }
      if(data[0].btnAction8 != undefined){
        var action8 = data[0].btnAction8.onClick;
        data[0].btnAction8 = {
        "text":data[0].btnAction8.text,
        "onClick":function(){
          scope.view.flxCardDetailsMobile.isVisible = false;
          action8();
          scope.AdjustScreen();
        }
      };
      }
      data[0].flxBlankSpace2 = {
        "height":"5dp"
      }
      data[0].flxBlankSpace = {
        "height":"5dp"
      }
      
      var dataMap={
        "btnAction1": "btnAction1",
        "btnAction2": "btnAction2",
        "btnAction3": "btnAction3",
        "btnAction4": "btnAction4",
        "btnAction5": "btnAction5",
        "btnAction6": "btnAction6",
        "btnAction7": "btnAction7",
        "btnAction8": "btnAction8",
        "flxActions": "flxActions",
        "flxBlankSpace1": "flxBlankSpace1",
        "flxBlankSpace2": "flxBlankSpace2",
        "flxBlankSpace": "flxBlankSpace",
        "flxCardDetails": "flxCardDetails",
        "flxCardHeader": "flxCardHeader",
        "flxCardImageAndCollapse": "flxCardImageAndCollapse",
        "lblCardsSeperator": "lblCardsSeperator",
        "flxCollapse": "flxCollapse",
        "flxDetailsRow1": "flxDetailsRow1",
        "flxDetailsRow10": "flxDetailsRow10",
        "flxDetailsRow2": "flxDetailsRow2",
        "flxDetailsRow3": "flxDetailsRow3",
        "flxDetailsRow4": "flxDetailsRow4",
        "flxDetailsRow5": "flxDetailsRow5",
        "flxDetailsRow6": "flxDetailsRow6",
        "flxDetailsRow7": "flxDetailsRow7",
        "flxDetailsRow8": "flxDetailsRow8",
        "flxDetailsRow9": "flxDetailsRow9",
        "flxMyCards": "flxMyCards",
        "flxMyCardsExpanded": "flxMyCardsExpanded",
        "flxRowIndicatorColor": "flxRowIndicatorColor",
        "lblIdentifier": "lblIdentifier",
        "lblSeparator1": "lblSeparator1",
        "lblSeparator2": "lblSeparator2",
        "lblSeperator": "lblSeperator",
        "imgCard": "imgCard",
        "imgCollapse": "imgCollapse",
        "lblCardHeader": "lblCardHeader",
        "lblCardStatus": "lblCardStatus",
        "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
        "lblKey1": "lblKey1",
        "lblKey10": "lblKey10",
        "lblKey2": "lblKey2",
        "lblKey3": "lblKey3",
        "lblKey4": "lblKey4",
        "lblKey5": "lblKey5",
        "lblKey6": "lblKey6",
        "lblKey7": "lblKey7",
        "lblKey8": "lblKey8",
        "lblKey9": "lblKey9",
        "rtxValue1": "rtxValue1",
        "rtxValue10": "rtxValue10",
        "rtxValue2": "rtxValue2",
        "rtxValue3": "rtxValue3",
        "rtxValue4": "rtxValue4",
        "rtxValue5": "rtxValue5",
        "rtxValue6": "rtxValue6",
        "rtxValue7": "rtxValue7",
        "rtxValue8": "rtxValue8",
        "rtxValue9": "rtxValue9"
      };
      this.view.segCardDetails.widgetDataMap = dataMap;
      this.view.segCardDetails.setData(data);
      this.view.flxBackToCards.onClick = function(){
        scope.view.flxMyCardsView.isVisible = true;
        scope.view.flxCardDetailsMobile.isVisible = false;
      }
      this.view.flxHeader.setFocus(true);
      this.view.flxMyCardsView.isVisible = false;
      this.view.flxCardDetailsMobile.isVisible = true;

      this.AdjustScreen();
    },

    showAcknowledgementOnPrintCancel : function(){
            var self = this;
            this.hideAllCardManagementViews();
            this.view.ConfirmDialog.confirmButtons.setVisibility(false);
            this.view.flxAcknowledgment.setVisibility(true);
            this.view.ConfirmDialog.flxDestination.setVisibility(false);
            this.view.ConfirmDialog.flxSelectCards.setVisibility(false);
            this.view.btnRequestReplacement.setVisibility(false);
            this.view.btnBackToCards.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.navigateToManageCards.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController);
            this.AdjustScreen();
    },

    printAcknowlegement : function(){
        var acknowledgementData = [];
        acknowledgementData.push({
            key : kony.i18n.getLocalizedString('i18n.common.status'),
            value : this.view.Acknowledgement.lblCardTransactionMessage.text
        });
        acknowledgementData.push({
            key : this.view.ConfirmDialog.keyValueCardHolder.lblKey.text,
            value  : this.view.ConfirmDialog.keyValueCardHolder.lblValue.text,
        })
        acknowledgementData.push({
            key : this.view.ConfirmDialog.keyValueCardName.lblKey.text,
            value : this.view.ConfirmDialog.keyValueCardName.lblValue.text
        })
        acknowledgementData.push({
            key : this.view.ConfirmDialog.keyValueValidThrough.lblKey.text,
            value  : this.view.ConfirmDialog.keyValueValidThrough.lblValue.text,
        })
        acknowledgementData.push({
            key  : this.view.ConfirmDialog.keyValueServiceProvider.lblKey.text,
            value : this.view.ConfirmDialog.keyValueServiceProvider.lblValue.text
        })
        acknowledgementData.push({
            key : this.view.ConfirmDialog.keyValueCreditLimit.lblKey.text,
            value : this.view.ConfirmDialog.keyValueCreditLimit.lblValue.text
        })
        acknowledgementData.push({
            key : this.view.ConfirmDialog.keyValueAvailableCredit.lblKey.text,
            value : this.view.ConfirmDialog.keyValueAvailableCredit.lblValue.text
        })
        var tableList = [{
            tableHeader : kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement"),
            tableRows : acknowledgementData
        }]
        var viewModel = {
            moduleHeader :  this.view.lblCardAcknowledgement.text,
            tableList : tableList
       };
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule").presentationController.showPrintPage({printKeyValueGroupModel:viewModel});
    },
    setMobileHeader: function (text) {
        if (kony.application.getCurrentBreakpoint() === 640) {
            this.view.customheader.lblHeaderMobile.text = text;
        }
        else {
            this.view.customheader.lblHeaderMobile.text = "";
        }
    }
    };
});