define({

    segmentHistoryRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.tableView.segmentBillpay.selectedIndex[1];
        var data = currForm.tableView.segmentBillpay.data;
        for(i=0;i<data.length;i++)
         {
           if(i==index)
             {
               kony.print("index:" + index);
               data[i].imgDropdown = "chevron_up.png";
               if(kony.application.getCurrentBreakpoint() == 640) {
                 data[i].template = "flxBillPayHistoryDetailsMobile";
               }
               else {
                 data[i].template = "flxContainer";
               }
               
             }
           else
             {
               data[i].imgDropdown = "arrow_down.png";
               if(kony.application.getCurrentBreakpoint() == 640) {
                 data[i].template = "flxBillPayHistoryMobile";
               } else {
                 data[i].template = "flxMain";
               }
               
             }
         }  
        currForm.tableView.segmentBillpay.setData(data);
        this.AdjustScreen(95);
    },
    showPayABill: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.payABill.isVisible = true;
        currForm.breadcrumb.setBreadcrumbData([{
            text: "BILL PAY"
        }, {
            text: "PAY A BILL"
        }]);
        currForm.btnConfirm.setVisibility(false);
        currForm.tableView.isVisible = false;
        this.AdjustScreen(30);
    },
   AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.frame.height + currentForm.flxContainer.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0) 
                currentForm.flxFooter.top = mainheight + data + diff + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
         } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
         }
    
      },

});