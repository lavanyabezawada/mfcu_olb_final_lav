define({ 
  
  segmentAllPayeesRowClick: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.tableView.segmentBillpay.selectedIndex[1];
        var data = currForm.tableView.segmentBillpay.data;
        for(i=0;i<data.length;i++)
         {
           if(i==index)
             {
               data[i].imgDropdown = "chevron_up.png";
               data[i].imgError = "error_yellow.png";
               data[i].lblError = "Verify payment amount and available account balance";
                if(kony.application.getCurrentBreakpoint() == 640) {
                 data[i].template = "flxBillPayAllPayeesSelectedMobile";
               } else {
                 data[i].template = "flxBillPayAllPayeesSelected";
               }
             }
           else
             {
               data[i].imgDropdown = "arrow_down.png";
               data[i].imgError = "error_yellow.png";
              // data[i].lblError = "lblError";
               if(kony.application.getCurrentBreakpoint() == 640) {
                  data[i].template = "flxBillPayAllPayeesMobile";
                } else {
                  data[i].template = "flxBillPayAllPayees";
                }
             }
         }  
        currForm.tableView.segmentBillpay.setData(data);
    this.AdjustScreen(221);
        //kony.timer.schedule("mytimer12",this.timerFunc, 0, false);
 },
timerFunc: function() 
{
kony.application.getCurrentForm().tableView.segmentBillpay.selectedIndex = kony.application.getCurrentForm().tableView.segmentBillpay.selectedRowIndex;
   this.AdjustScreen(30);
},
   viewEBill: function() 
  {
     var currForm = kony.application.getCurrentForm();
     var height_to_set=140+currForm.flxContainer.frame.height;
     currForm.flxViewEbill.height =height_to_set+"dp";
     currForm.flxViewEbill.isVisible = true;
     this.AdjustScreen(30);
     currForm.forceLayout();
  },
  //UI Code
  AdjustScreen: function(data) {
    var currForm = kony.application.getCurrentForm();
    if (data !== undefined) {} else {
        data = 0;
    }
    var mainheight = 0;
    var flag = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = currForm.customheader.frame.height + currForm.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (currForm.flxBillPay.frame.height <= currForm.flxrightcontainerbillpay.frame.height) {
        flag = 1;
    }
    if (flag === 0) {
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.frame.height;
            if (diff > 0) currForm.flxFooter.top = mainheight + diff + data+ "dp";
            else currForm.flxFooter.top = mainheight + data + "dp";
        } else {
            currForm.flxFooter.top = mainheight + data + "dp";
        }
    } else {
        if (mainheight < screenheight) {
            diff = diff - currForm.flxFooter.frame.height;
            if (diff > 0) currForm.flxFooter.top = mainheight + diff + "dp";
            else currForm.flxFooter.top = mainheight + "dp";
        } else {
            currForm.flxFooter.top = mainheight + "dp";
        }
    }
    currForm.forceLayout();
},
  
 /* calculateTopForCustomsListBox:function(index)
  {
    var currForm = kony.application.getCurrentForm();
    var topCalculated=314,topCalculated_category;
    if(currForm.tableView.Search.isVisible===true)
     {
       topCalculated=topCalculated+101;
     }
    topCalculated = topCalculated+(270*index); 
    topCalculated_category=topCalculated+81;
    currForm.flxCustomListBoxContainerPayFrom.top=topCalculated+"dp";
    currForm.flxCustomListBoxContainerCategory.top=topCalculated_category+"dp";
  }
    */

 });