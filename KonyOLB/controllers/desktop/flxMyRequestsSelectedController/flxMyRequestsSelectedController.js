define({ 

	showSelectedRow: function(){
		var index = kony.application.getCurrentForm().tableView.segP2P.selectedIndex;
		var sectionIndex = index[0];
		var rowIndex = index[1];
		var data = kony.application.getCurrentForm().tableView.segP2P.data;
		data[sectionIndex][1][rowIndex].template = "flxMyRequests";
		kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);  
	},
	showSendReminder: function(){
		kony.application.getCurrentForm().tableView.flxTabs.btnSendRequest.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnMyRequests.skin="sknBtnAccountSummarySelected";
		kony.application.getCurrentForm().tableView.flxTabs.btnSent.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnRecieved.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxTabs.btnManageRecepient.skin="sknBtnAccountSummaryUnselected";
		kony.application.getCurrentForm().tableView.flxSendMoney.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxSendReminder.setVisibility(true);
		kony.application.getCurrentForm().tableView.flxRequestMoney.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxHorizontalLine2.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxTableHeaders.setVisibility(false);
      kony.application.getCurrentForm().tableView.flxSearch1.setVisibility(false);
		kony.application.getCurrentForm().tableView.flxHorizontalLine3.setVisibility(false);
		kony.application.getCurrentForm().tableView.segP2P.setVisibility(false);
	}


});
