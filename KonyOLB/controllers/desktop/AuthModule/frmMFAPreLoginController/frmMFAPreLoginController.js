define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
  this.customerPhone = "";
 this.customerEmail = "";
  var orientationHandler = new OrientationHandler();
  return {



    /**
    * preShow function of frmMFAPReLogin
	**/
    onPreShow : function(){
      var scopeObj = this;
//       this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
//       this.view.OTPModule.flxEnterOTP.setVisibility(true);
      this.view.onBreakpointChange = function() {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
       this.view.flxDropdown.setVisibility(false);
      //this.view.OTPModule.flximgrtx.top = "60dp";
      this.view.OTPModule.lblWrongOTP.setVisibility(false);
      this.view.lblWrongOTP.setVisibility(false);
      this.view.lblAlertDescription.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
      this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
     // this.view.OTPModule.btnProceed.onClick =  this.onProceedToOTPPage.bind(this);
      this.view.logOutMsg1.AlterneteActionsLoginNow.onTouchStart = this.loginLater.bind(this);
      this.view.btnVeiwMore.onClick = function() {
          var config = applicationManager.getConfigurationManager();
          kony.application.openURL(config.getConfigurationValue("LINK_TO_DBX"));
      };
      this.view.flxClose.onClick = this.emptyFormData.bind(this);
      this.view.flxCloseSecurityQues.onClick = this.emptyFormData.bind(this);
      this.enableOrDisableRegisterDeviceOption();
      this.bindFooterActions();
      this.assignToolTips();
      this.initializeResponsiveViews(); 
      applicationManager.getNavigationManager().applyUpdates(this); 
    },

    assignToolTips:function(){
      this.view.OTPModule.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed"); 
      this.view.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed"); 
      this.view.OTPModule.btnResendOTP.toolTip = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
    },
    emptyFormData:function(){
      this.view.flxPhoneAndEmail.setVisibility(false);
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxLogoutMsgMFA.setVisibility(false);
      this.view.OTPModule.tbxCVV.text = "";
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.showLoginScreen();
       var view = kony.application.getCurrentForm();
      FormControllerUtility.hideProgressBar(view);
    },
    enableOrDisableRegisterDeviceOption:function(){
      var scopeObj = this;
      if(kony.mvc.MDAApplication.getSharedInstance().appContext.rememberMeStatus){
        this.view.OTPModule.flxRememberMe.isVisible = true;
        this.view.OTPModule.flexcheckuncheck.onClick = function(){
          if(FormControllerUtility.isFontIconChecked(scopeObj.view.OTPModule.lblMRememberMe)){
            FormControllerUtility.toggleFontCheckbox(scopeObj.view.OTPModule.lblMRememberMe);
            kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = false;
          }else{
            FormControllerUtility.toggleFontCheckbox(scopeObj.view.OTPModule.lblMRememberMe);
            kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = true;
          }
        }
      }else{
        this.view.OTPModule.flxRememberMe.isVisible= false;
      }
    },

    bindFooterActions:function(){
      var scopeObj = this;
      this.view.btnFaqs.onClick = scopeObj.loadAuthModule().presentationController.navigateToFAQ.bind(scopeObj);
      this.view.btnContactUs.onClick = scopeObj.loadAuthModule().presentationController.navigateToContactUs.bind(scopeObj);
      this.view.btnPrivacy.onClick = scopeObj.loadAuthModule().presentationController.navigateToPrivacyPrivacy.bind(scopeObj);
      this.view.btnTermsAndConditions.onClick = scopeObj.loadAuthModule().presentationController.navigateToTermsAndConditions.bind(scopeObj);
      this.view.btnLocateUs.onClick = function () {
        scopeObj.view.flxLogoutMsg.isVisible = false;
        scopeObj.loadAuthModule().presentationController.navigateToLocateUs();
      };
    },

     loadAuthModule: function () {
      return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    },

    loginLater:function(){
      this.view.flxLogoutMsg.isVisible = false;
      applicationManager.getNavigationManager().navigateTo("frmLogin");
      applicationManager.getConfigurationManager().fetchApplicationProperties(function (res) {
        applicationManager.getNavigationManager().updateForm({
          isLanguageSelectionEnabled: res.isLanguageSelectionEnabled,
          defaultLanguage: res.language
        }, "frmLogin");
      }, function () { });
      var view = kony.application.getCurrentForm();
      FormControllerUtility.hideProgressBar(view);
    },
    /**
    * onProceedToOTPPage - Click Action of btnProceed
	**/
    onProceedToOTPPage : function(){
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
    },

    updateFormUI: function(viewModel) {
      if(viewModel.phoneEmailScreen){
        this.showPhoneEmailScreen(viewModel.phoneEmailScreen);
      } else if(viewModel.otpReceived){
        this.showScreentoEnterOTP(viewModel.otpReceived);
      } else if(viewModel.primaryPhoneEmailScreen){
        this.showPrimaryPhoneEmailScreen(viewModel.primaryPhoneEmailScreen);
      }   else if(viewModel.otpIncorrect){
        this.showIncorrectOTPError(viewModel.otpIncorrect);
      }  else if(viewModel.defaultPhoneEmailScreen){
        this.showDefaultPhoneEmailScreen(viewModel.defaultPhoneEmailScreen);
      }   else if(viewModel.securityQuestions){
        this.setSecurityQuestionsUI(viewModel.securityQuestions);
      }   else if(viewModel.securityAnswerIncorrect){
        this.showIncorrectSecurityAnswerError(viewModel.securityAnswerIncorrect);
      }  else if(viewModel.otpRequestFailed){
        this.showRequestOTPError(viewModel.otpRequestFailed);
      }


    },


    showPhoneEmailScreen : function(customerCommunicationInfo){
      var scopeObj = this;
      FormControllerUtility.showProgressBar(this.view);
      this.view.OTPModule.lblWrongOTP.setVisibility(false);
      this.bindUIForOTPMFAScreen(customerCommunicationInfo);
      this.view.OTPModule.btnProceed.onClick = function() {
        this.view.flxLoading.height  = "100%";
        FormControllerUtility.showProgressBar(scopeObj.view);
        var selectedData = {
          "phone" :scopeObj.view.OTPModule.lbxPhone.selectedKeyValue[0],
          "email" : scopeObj.view.OTPModule.lbxEmail.selectedKeyValue[0],
        };
        scopeObj.customerPhone  = selectedData.phone;
        scopeObj.customerEmail  = selectedData.email;
        this.requestOTP(selectedData); 
      }.bind(this);
      this.view.forceLayout();
    },

    bindUIForOTPMFAScreen : function(customerCommunicationInfo){
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxPhoneAndEmail.setVisibility(true);
      this.view.securityQuestions.flxAnswerSecurityQuestions.setVisibility(false);
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
      this.view.OTPModule.flxEnterOTP.setVisibility(true);
      this.view.flxLogoutMsgMFA.setVisibility(false);
      this.view.flxLogoutMsg.setVisibility(false);
      this.view.OTPModule.lblWrongOTP.setVisibility(false);
      FormControllerUtility.enableButton(this.view.OTPModule.btnProceed);
      this.view.OTPModule.lblHeaderOTP.text = kony.i18n.getLocalizedString("i18n.mfa.newDeviceDetected");
      this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTP");
      this.view.OTPModule.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");  
      this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
     if (customerCommunicationInfo.phone.length>0 && customerCommunicationInfo.email.length>0) {
                this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTP");
                this.view.OTPModule.lbxPhone.masterData = this.setDataForPhoneListBox(customerCommunicationInfo.phone);
                this.view.OTPModule.lbxEmail.masterData = this.setDataForEmailListBox(customerCommunicationInfo.email);
                this.view.OTPModule.lblRegisteredPhone.setVisibility(true);
                this.view.OTPModule.lbxPhone.setVisibility(true);
                this.view.OTPModule.lblRegisteredEmail.setVisibility(true);
                this.view.OTPModule.lbxEmail.setVisibility(true);
                this.view.forceLayout();
            }
           else{
            if (customerCommunicationInfo.phone.length>0 || customerCommunicationInfo.email.length>0){
                if (customerCommunicationInfo.phone.length>0) {
                    this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTPPhone");
                    this.view.OTPModule.lbxPhone.masterData = this.setDataForPhoneListBox(customerCommunicationInfo.phone);
                    this.view.OTPModule.lblRegisteredPhone.setVisibility(true);
                    this.view.OTPModule.lbxPhone.setVisibility(true);
                    this.view.OTPModule.lblRegisteredEmail.setVisibility(false);
                    this.view.OTPModule.lbxEmail.setVisibility(false);
                    this.view.forceLayout();
                } else if (customerCommunicationInfo.email.length>0) {
                    this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTPEmail");
                    this.view.OTPModule.lbxEmail.masterData = this.setDataForEmailListBox(customerCommunicationInfo.email);
                    this.view.OTPModule.lblRegisteredPhone.setVisibility(false);
                    this.view.OTPModule.lbxPhone.setVisibility(false);
                    this.view.OTPModule.lblRegisteredEmail.setVisibility(true);
                    this.view.OTPModule.lbxEmail.setVisibility(true);
                    this.view.forceLayout();
                }
            }
            }
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    setDataForPhoneListBox:function(phoneObj){
      var phoneNumbers = phoneObj.map(function (dataItem) {
        var phoneNumber = [];
        phoneNumber.push(dataItem.unmasked);
        phoneNumber.push(dataItem.masked);
        return phoneNumber;
      });
      return phoneNumbers;
    },

    setDataForEmailListBox:function(emailObj){
      var emailsIds = emailObj.map(function (dataItem) {
        var email = [];
        email.push(dataItem.unmasked);
        email.push(dataItem.masked);
        return email;
      });
      return emailsIds;
    },

    requestOTP: function(selectedData) {
      applicationManager.getPresentationUtility().MFA.requestOTP(selectedData);
    },


    showScreentoEnterOTP:function(response){
      var scopeObj = this;
      if (response.remainingResendAttempts <= 0) {
        this.view.OTPModule.btnResendOTP.setVisibility(false);
      } else {
        this.bindUIForResendButton(response);
        this.view.OTPModule.btnResendOTP.setVisibility(true);
      }
      if(response.isOTPExpired === "true"){
        this.view.OTPModule.lblWrongOTP.setVisibility(true);
        this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");  
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        FormControllerUtility.hideProgressBar(this.view);
      }else{
        this.view.OTPModule.lblWrongOTP.setVisibility(false);
      }  
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxPhoneAndEmail.setVisibility(true);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
      this.view.flxLogoutMsgMFA.setVisibility(false);
      this.view.flxLogoutMsg.setVisibility(false);
      this.view.OTPModule.tbxCVV.text = "";
      this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.MFA.EnterSACOnPhone");
      this.view.OTPModule.flxCVV.skin = ViewConstants.SKINS.FLEX_UNSELECTED_SKIN;
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
      this.view.OTPModule.btnResendOTP.text = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
      this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
      this.view.OTPModule.tbxCVV.onKeyUp = function(){
        this.validatetoEnableContinueButton();
      }.bind(this);
       this.view.OTPModule.tbxCVV.onDone = function(){
        FormControllerUtility.showProgressBar(scopeObj.view);
        var selectedData = {
          "securityKey" : response.securityKey,
          "otp" : this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(selectedData);
      }.bind(this);
      this.view.OTPModule.btnLogin.onClick = function(){
        FormControllerUtility.showProgressBar(scopeObj.view);
        var selectedData = {
          "securityKey" : response.securityKey,
          "otp" : this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(selectedData);
      }.bind(this);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    validatetoEnableContinueButton:function(){
      var otp = this.view.OTPModule.tbxCVV.text.trim();
      if(otp === ""){
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      }else{
        FormControllerUtility.enableButton(this.view.OTPModule.btnLogin);
      }
    },

    bindUIForResendButton:function(response){
      var scopeObj = this;
      this.view.OTPModule.tbxCVV.text = "";
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.btnResendOTP.onClick = function() {
        FormControllerUtility.showProgressBar(scopeObj.view);
        if(response.customerCommunication){
          var params = {
            "phone": response.customerCommunication.phone[0].unmasked,
            "email": response.customerCommunication.email[0].unmasked,
            "securityKey": response.securityKey,
          };
        }else{
          var params = {
            "phone":  scopeObj.customerPhone,
            "email":  scopeObj.customerEmail,
            "securityKey": response.securityKey,
          };
        }
        scopeObj.resendOTP(params);
      };
    },

    resendOTP:function(params){
      applicationManager.getPresentationUtility().MFA.resendOTP(params);
    },
    verifyOTP : function(params){
      applicationManager.getPresentationUtility().MFA.verifyOTP(params);
    },

    showPrimaryPhoneEmailScreen : function(customerCommunicationInfo){
      this.bindUIForPrimaryScreen(customerCommunicationInfo);
    },

    bindUIForPrimaryScreen : function(response){
      if (response.remainingResendAttempts <= 0) {
        this.view.OTPModule.btnResendOTP.setVisibility(false);
      } else {
        this.bindUIForResendButton(response);
        this.view.OTPModule.btnResendOTP.setVisibility(true);
      }
      if(response.isOTPExpired === "true"){
        this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");
        this.view.OTPModule.lblWrongOTP.setVisibility(true);
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        FormControllerUtility.hideProgressBar(this.view);
      }else{
        this.view.OTPModule.lblWrongOTP.setVisibility(false);
      } 
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxPhoneAndEmail.setVisibility(true);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
      this.view.flxLogoutMsgMFA.setVisibility(false);
      this.view.flxLogoutMsg.setVisibility(false);
      this.view.OTPModule.flxCVV.skin = ViewConstants.SKINS.FLEX_UNSELECTED_SKIN;
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.tbxCVV.text = "";
      this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
      this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
      if(response.customerCommunication.phone.length>0 && response.customerCommunication.email.length>0){
        var phone = response.customerCommunication.phone[0].masked;
        var email = response.customerCommunication.email[0].masked;
        this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.EnterSACMobile") + phone + " & " + email;
      }else{
        if(response.customerCommunication.phone.length>0){
          var phone = response.customerCommunication.phone[0].masked;
          this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.EnterSACMobile") + phone;
        }else{
          var email = response.customerCommunication.email[0].masked;
          this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.EnterSACMobile") + email;
        }
      }
      
     this.view.OTPModule.rtxEnterCVVCode.height = "90px";
      this.view.OTPModule.tbxCVV.onKeyUp = function() {
        this.validatetoEnableContinueButton();
      }.bind(this);
       this.view.OTPModule.tbxCVV.onDone = function() {
        FormControllerUtility.showProgressBar(this.view);
        var params = {
          "securityKey": response.securityKey,
          "otp": this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(params);
      }.bind(this);
      this.view.OTPModule.btnLogin.onClick = function() {
        FormControllerUtility.showProgressBar(this.view);
        var params = {
          "securityKey": response.securityKey,
          "otp": this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(params);
      }.bind(this);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    showIncorrectOTPError: function(response) {
      var scopeObj = this;
      if(response.errmsg){
        this.view.OTPModule.lblWrongOTP.text = response.errmsg;
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        FormControllerUtility.hideProgressBar(this.view);
      }else{
        if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts && response.MFAAttributes.remainingFailedAttempts > 0) {
          this.view.OTPModule.lblWrongOTP.setVisibility(true);
          this.view.OTPModule.flxCVV.skin = ViewConstants.SKINS.LOANS_FLEX_ERROR;
          this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.invalidAccessCode") + " " + response.MFAAttributes.remainingFailedAttempts + " " + kony.i18n.getLocalizedString("i18n.mfa.remainingAttempts");
          this.view.flxSecurityQuestions.setVisibility(false);
          this.view.flxPhoneAndEmail.setVisibility(true);
          this.view.OTPModule.flxEnterOTP.setVisibility(false);
          this.view.flxLogoutMsgMFA.setVisibility(false);
          this.view.flxLogoutMsg.setVisibility(false);
          this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
          this.view.OTPModule.tbxCVV.text = "";
          if(FormControllerUtility.isFontIconChecked(scopeObj.view.OTPModule.lblMRememberMe)){
            FormControllerUtility.toggleFontCheckbox(scopeObj.view.OTPModule.lblMRememberMe);
            kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = false;
          }
          FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
          FormControllerUtility.hideProgressBar(this.view);
          this.view.forceLayout();
        } else if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.lockUser === "true") {
          FormControllerUtility.showProgressBar(this.view);
          this.showLockOutMFA(response.MFAAttributes.lockoutTime);
        } else if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.logoutUser === "true") {
          FormControllerUtility.showProgressBar(this.view);
          this.showLogOutMFA();
        }
      }
      this.view.forceLayout();
    },


    showLockOutMFA:function(lockTime){
      var scopeObj = this;
      this.view.flxPhoneAndEmail.setVisibility(false);
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxLogoutMsgMFA.setVisibility(true);
      this.view.logOutMsg1.imgLogoutSuccess.src = "error_yellow.png";
      this.view.logOutMsg1.AlterneteActionsLoginNow.fontIconOption.skin = "sknlblfonticon24px0273e3";
      this.view.logOutMsg1.AlterneteActionsLoginNow.fontIconOption.text = "V";
      this.view.logOutMsg1.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.mfalogin.lockeduser") + " " + lockTime + kony.i18n.getLocalizedString("i18n.mfa.minutes");
      this.view.logOutMsg1.lblSuccessIcon.setVisibility(false);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    showLogOutMFA:function(){
      var scopeObj = this;
      this.view.flxPhoneAndEmail.setVisibility(false);
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxLogoutMsgMFA.setVisibility(true);
      this.view.logOutMsg1.imgLogoutSuccess.src = "error_yellow.png";
      this.view.logOutMsg1.AlterneteActionsLoginNow.fontIconOption.skin = "sknlblfonticon24px0273e3";
      this.view.logOutMsg1.AlterneteActionsLoginNow.fontIconOption.text = "V";
      this.view.logOutMsg1.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.mfalogin.LockoutMFA");
      this.view.logOutMsg1.lblSuccessIcon.setVisibility(false);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    showDefaultPhoneEmailScreen : function(customerCommunicationInfo){
      this.bindUIForDefaultScreen(customerCommunicationInfo);
    },

    bindUIForDefaultScreen:function(response){
      if (response.remainingResendAttempts <= 0) {
        this.view.OTPModule.btnResendOTP.setVisibility(false);
      } else {
        this.bindUIForResendButton(response);
        this.view.OTPModule.btnResendOTP.setVisibility(true);
      }
      if(response.isOTPExpired === "true"){
        this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");  
        this.view.OTPModule.lblWrongOTP.setVisibility(true);
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        FormControllerUtility.hideProgressBar(this.view);
      }else{
        this.view.OTPModule.lblWrongOTP.setVisibility(false);
      } 
      this.view.flxSecurityQuestions.setVisibility(false);
      this.view.flxPhoneAndEmail.setVisibility(true);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
      this.view.flxLogoutMsgMFA.setVisibility(false);
      this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
      this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
      this.view.OTPModule.btnResendOTP.text =  kony.i18n.getLocalizedString("i18n.login.ResendOtp");
      this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
      this.view.OTPModule.flxCVV.skin = ViewConstants.SKINS.FLEX_UNSELECTED_SKIN;
      this.view.OTPModule.tbxCVV.text = "";
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.tbxCVV.onKeyUp = function() {
        this.validatetoEnableContinueButton();
      }.bind(this);
      this.view.OTPModule.tbxCVV.onDone = function() {
                FormControllerUtility.showProgressBar(this.view);
                this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
                var params = {
                    "securityKey": response.securityKey,
                    "otp": this.view.OTPModule.tbxCVV.text.trim()
                };
                this.verifyOTP(params);
            }.bind(this);
      this.view.OTPModule.btnLogin.onClick = function() {
        FormControllerUtility.showProgressBar(this.view);
        this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
        var params = {
          "securityKey": response.securityKey,
          "otp": this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(params);
      }.bind(this);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();


    },

    setSecurityQuestionsUI:function(response){
      var scopeObj = this;
            FormControllerUtility.hideProgressBar(this.view);
            this.view.lblWrongOTP.setVisibility(false);
            if (kony.mvc.MDAApplication.getSharedInstance().appContext.rememberMeStatus) {
                this.view.flxActions.isVisible = true;
                this.view.flxRegisterDevice.isVisible = true;
                this.view.lblAlertDescription.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
                this.view.flxEnabledIcon.onClick = function() {
                    if (scopeObj.view.lblEnabledIcon.text == 'D') {
                        scopeObj.view.lblEnabledIcon.text = 'C';
                        kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = true;
                    } else {
                        scopeObj.view.lblEnabledIcon.text = 'D';
                        kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = false;
                    }
                }
            } else {
                this.view.flxRegisterDevice.isVisible = false;
            }
            var securityQuestions = response.MFAAttributes.securityQuestions;
            if(response.MFAAttributes.securityQuestions){
                 this.view.securityQuestions.tbxAnswers1.text = "";
                  this.view.securityQuestions.tbxAnswers2.text = "";
                  this.view.securityQuestions.lblHeading2.text = kony.i18n.getLocalizedString("i18n.mfa.newDeviceDetectedSecurityQues");
                  this.view.flxPhoneAndEmail.setVisibility(false);
                  this.view.flxSecurityQuestions.setVisibility(true);
                  this.view.flxLogoutMsgMFA.setVisibility(false);
                  this.view.flxLogoutMsg.setVisibility(false);
                  this.view.OTPModule.flxEnterOTP.setVisibility(false);
                  this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
                  this.bindUIForSecurityQuestionsMFAScreen(securityQuestions);
                  this.view.flxSecurityQuestions.securityQuestions.flxAnswerSecurityQuestions.setVisibility(true);
                  this.view.flxSecurityQuestions.setVisibility(true);
                  FormControllerUtility.disableButton(this.view.btnProceed);
                  this.view.flxDropdown.setVisibility(false);
                  this.view.forceLayout();
            }
            if (response.MFAAttributes.remainingFailedAttempts && response.MFAAttributes.remainingFailedAttempts > 0) {
                this.view.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.incorrectanswer") + response.MFAAttributes.remainingFailedAttempts + " " + kony.i18n.getLocalizedString("i18n.mfa.remainingAttempts");
                this.view.lblWrongOTP.setVisibility(true);
                if (scopeObj.view.lblEnabledIcon.text == 'C') {
                    scopeObj.view.lblEnabledIcon.text = 'D';
                    kony.mvc.MDAApplication.getSharedInstance().appContext.registerStatus = false;
                }
                FormControllerUtility.hideProgressBar(this.view);
                this.view.forceLayout();
            } else if (response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.lockUser === "true") {
                FormControllerUtility.showProgressBar(this.view);
                this.showLockOutMFA(response.MFAAttributes.lockoutTime);
            } else if (response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.logoutUser === "true") {
                FormControllerUtility.showProgressBar(this.view);
                this.showLogOutMFA();
            }
    },

    bindUIForSecurityQuestionsMFAScreen: function(securityQuestions) {
      var scopeObj = this;
      if(securityQuestions.length === 2){
        this.view.securityQuestions.flxAnswerSecurityQuestionsQASet2.setVisibility(true);
        this.view.securityQuestions.lblSecurityQuestion1Desc.text = securityQuestions[0].Question;
        this.view.securityQuestions.lblSecurityQuestion2Desc.text = securityQuestions[1].Question;
        this.view.securityQuestions.tbxAnswers1.onKeyUp = this.isValidAnswer.bind(this);
        this.view.securityQuestions.tbxAnswers2.onKeyUp = this.isValidAnswer.bind(this);
      }else{
        this.view.securityQuestions.lblSecurityQuestion1Desc.text = securityQuestions[0].Question;
        this.view.securityQuestions.flxAnswerSecurityQuestionsQASet2.setVisibility(false);
        this.view.securityQuestions.tbxAnswers1.onKeyUp = function(){
          var answer1 = scopeObj.view.securityQuestions.tbxAnswers1.text;
          if(answer1=== ""){
            FormControllerUtility.disableButton(scopeObj.view.btnProceed);
          }else{
            FormControllerUtility.enableButton(scopeObj.view.btnProceed);
          }
        }
      }
      scopeObj.view.btnProceed.onClick = function() {
        FormControllerUtility.showProgressBar(this.view);
        var data  = this.onSaveAnswerSecurityQuestions(securityQuestions);
        applicationManager.getPresentationUtility().MFA.verifySecurityQuestions(data);
      }.bind(this);
    },

    onSaveAnswerSecurityQuestions:function(securityQuestions){
      var data = [{
        customerAnswer: "",
        questionId: ""
      }, {
        customerAnswer: "",
        questionId: ""
      }];
      var quesData = "";
      quesData = this.view.securityQuestions.lblSecurityQuestion1Desc.text;
      data[0].customerAnswer = this.view.securityQuestions.tbxAnswers1.text;
      data[0].questionId = this.getQuestionIDForAnswer(quesData,securityQuestions);
      quesData =  this.view.securityQuestions.lblSecurityQuestion2Desc.text;
      data[1].customerAnswer = this.view.securityQuestions.tbxAnswers2.text;
      data[1].questionId = this.getQuestionIDForAnswer(quesData,securityQuestions);
      return data;
    },

    isValidAnswer:function(){
      var answer1 = this.view.securityQuestions.tbxAnswers1.text;
      var answer2 = this.view.securityQuestions.tbxAnswers2.text;
      if(answer1=== "" || answer2  === ""){
        FormControllerUtility.disableButton(this.view.btnProceed);
      }else{
        FormControllerUtility.enableButton(this.view.btnProceed);
      }
    },

    getQuestionIDForAnswer: function(quesData,responseBackend) {
      var qData;
      for (var i = 0; i < responseBackend.length; i++) {
        if (quesData === responseBackend[i].Question) {
          qData = responseBackend[i].SecurityQuestion_id;
        }
      }
      return qData;
    },

    showIncorrectSecurityAnswerError:function(error){
      this.view.lblWrongOTP.text = error.errmsg;
      this.view.securityQuestions.tbxAnswers1.text ="";
      this.view.securityQuestions.tbxAnswers2.text = "";
      FormControllerUtility.disableButton(this.view.btnProceed);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
      showRequestOTPError:function(error){
            this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.requestOTPMessageFailed");
           this.view.OTPModule.lblWrongOTP.setVisibility(true);     
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
    
      responsiveViews:{},
  initializeResponsiveViews:function(){
      this.responsiveViews["flxPhoneAndEmail"] = this.view.flxPhoneAndEmail.isVisible;
      this.responsiveViews["flxSecurityQuestions"] = this.view.flxSecurityQuestions;
      this.responsiveViews["flxLogoutMsgMFA"] = this.view.flxLogoutMsgMFA;
      this.responsiveViews["flxEnterSecureAccessCode"] = this.view.OTPModule.flxEnterSecureAccessCode;
      this.responsiveViews["flxEnterOTP"] = this.view.OTPModule.flxEnterOTP;
     this.responsiveViews["flxAnswerSecurityQuestions"] =  this.view.securityQuestions.flxAnswerSecurityQuestions;
  },
  onBreakpointChange: function(width) {
    kony.print('on breakpoint change');
    orientationHandler.onOrientationChange(this.onBreakpointChange);
    
    if (width > 1024 && !orientationHandler.isTablet) {
      this.view.OTPModule.rtxEnterCVVCode.contentAlignment = "1";
      this.view.OTPModule.lblResendMessage.contentAlignment = "1"; 
      this.view.OTPModule.lblHeaderOTP.contentAlignment = "1";
     }
  }
 
  };
});