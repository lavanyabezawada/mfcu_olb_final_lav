define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants){ 
    return {
        init : function() {

        },
        preShow : function() {
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow : function() {

        },
        updateFormUI : function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if(context.showResetPasswordScreen) {
                this.showResetPasswordScreen(context.showResetPasswordScreen);
            }
            if(context.showResetPasswordAcknowledgementScreen) {
                this.showResetPasswordAcknowledgementScreen();
            }
            if(context.showResetPasswordErrorScreen) {
                this.showResetPasswordErrorScreen(context.showResetPasswordErrorScreen.errorMessage);
            }
        },
        loadAuthModule : function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        },
        showResetPasswordScreen : function(viewModel) {
            var self = this;
            this.view.flxCloseUsernamePassowrd.setVisibility(false);
            this.view.lblBeyondBanking.text = kony.i18n.getLocalizedString("i18n.login.BeyondBanking");
            this.view.lblBeyondBankingDesc.text = kony.i18n.getLocalizedString("i18n.login.BeyondBankingDesc");            
            this.view.newUsernamepasswordsetting.tbxNewUserName.text = "";
            this.view.newUsernamepasswordsetting.tbxNewPassword.text = "";
            this.view.newUsernamepasswordsetting.tbxMatchPassword.text = "";
            this.view.newUsernamepasswordsetting.tbxNewUserName.placeholder = kony.i18n.getLocalizedString("i18n.common.EnterUsername");
            this.view.newUsernamepasswordsetting.imgValidPassword.setVisibility(false);
            this.view.newUsernamepasswordsetting.imgPasswordMatched.setVisibility(false);
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.setVisibility(false);
            this.view.newUsernamepasswordsetting.btnCreate.text = kony.i18n.getLocalizedString("i18n.enrollNow.proceed");
            this.view.newUsernamepasswordsetting.lblResetPasswordTitle.text = kony.i18n.getLocalizedString("i18n.login.PasswordReset");
            FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            this.setPasswordPolicies(viewModel.policyData);
            this.view.btnVeiwMore.hoverSkin ="sknbtn41a0edviewmoreHover";
            this.view.btnVeiwMore.onClick = function() {
                var config = applicationManager.getConfigurationManager();
                kony.application.openURL(config.getConfigurationValue("LINK_TO_DBX"));
            };
            this.view.newUsernamepasswordsetting.btnCreate.onClick = function() {
                var params = {};
                params.identifier = viewModel.identifier;
                params.UserName = self.view.newUsernamepasswordsetting.tbxNewUserName.text;
                params.Password = self.view.newUsernamepasswordsetting.tbxNewPassword.text;
                self.loadAuthModule().presentationController.resetPasswordFromEmail(params);
            };
            this.view.newUsernamepasswordsetting.tbxNewUserName.onBeginEditing = function() {
                self.hidePasswordRules();
            };
            this.view.newUsernamepasswordsetting.tbxNewUserName.onKeyUp = this.validateUserName.bind(this);
            this.view.newUsernamepasswordsetting.tbxNewPassword.onBeginEditing = this.passwordEditing.bind(this);
            this.view.newUsernamepasswordsetting.tbxNewPassword.onKeyUp = this.newPwdValidation.bind(this);
            this.view.newUsernamepasswordsetting.tbxMatchPassword.onKeyUp = this.matchPwdKeyUp.bind(this);
            this.view.newUsernamepasswordsetting.tbxMatchPassword.onBeginEditing = this.showPasswordRules.bind(this);
        },
        showPasswordRules : function() {
            this.view.newUsernamepasswordsetting.flxRulesPassword.setVisibility(true);
            this.view.forceLayout();
        },
        hidePasswordRules : function() {
            this.view.newUsernamepasswordsetting.flxRulesPassword.setVisibility(false);
            this.view.forceLayout();
        },
        setPasswordPolicies: function (data) {
            if (data) {
                var policyData = (data.content+"").split("<li>").join("<li>\u2022 ");
                this.view.newUsernamepasswordsetting.rtxRulesPassword.text = policyData;
            } else {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.updateFormUI('frmLogin', {});
                authModule.presentationController.navigateToServerDownScreen.call(authModule.presentationController);
            }

        },
        showResetPasswordAcknowledgementScreen : function() {
            var self = this;
            this.view.flxUsernameAndPassword.setVisibility(false);
            this.view.flxResetSuccessful.setVisibility(true);
            this.view.passwordresetsuccess.btnProceed.onClick = function() {
                self.goToLogin();
            }
            this.view.forceLayout();
        },
        showResetPasswordErrorScreen : function(errorMessage) {
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = errorMessage;
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.setVisibility(true);
            this.view.newUsernamepasswordsetting.tbxNewUserName.text = "";
            this.view.newUsernamepasswordsetting.tbxNewPassword.text = "";
            this.view.newUsernamepasswordsetting.tbxMatchPassword.text = "";
            this.view.newUsernamepasswordsetting.imgValidPassword.setVisibility(false);
            this.view.newUsernamepasswordsetting.imgPasswordMatched.setVisibility(false);
            FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            this.view.forceLayout();
        },
        goToLogin: function () {
            var navManager = applicationManager.getNavigationManager();
            navManager.navigateTo("frmLogin");
        },

        passwordEditing: function () {
            this.showPasswordRules();
            if (this.view.newUsernamepasswordsetting.lblErrorInfo.isVisible) {
                this.reEnterNewPassword();
            }
        },

         /**
         * reEnterNewPassword :This function shows the error message if the entered password is wrong
         */
        reEnterNewPassword: function () {
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
            this.view.newUsernamepasswordsetting.flxNewPassword.top = "8.8%";
            this.view.flxUsernameAndPassword.parent.forceLayout();
        },
        
        /**
        * newPwdValidation :The function is internally calls other function which validates the password
        */
        newPwdValidation: function () {
            if(this.view.newUsernamepasswordsetting.tbxNewPassword.text !== "") {
                this.validateNewPassword(this.view.newUsernamepasswordsetting.tbxNewPassword.text);
            } else {
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
            }
            this.view.forceLayout();
        },

        /**
         * validateNewPassword :This function validates whether the entered password is correct or not
         * @param {Object}  enteredPassword password entered by the user
        */
        validateNewPassword: function (enteredPassword) {
            if (this.isPasswordValid(enteredPassword)) {
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = true;
                this.view.newUsernamepasswordsetting.flxRulesPassword.isVisible=true;
                if(this.view.newUsernamepasswordsetting.tbxMatchPassword.text!==""){
                    if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                      this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = true;
                      if(this.isUserNameValid(this.view.newUsernamepasswordsetting.tbxNewUserName.text.trim())) {
                        FormControllerUtility.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                  }
                  else {
                      this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = kony.i18n.getLocalizedString("i18n.login.passwordmismatch");
                      this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                      this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                      FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                  }
                }
                
            }
            else {
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                this.showPasswordRules();
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
                 this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = kony.i18n.getLocalizedString("i18n.login.invalidPassword");
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.flxUsernameAndPassword.parent.forceLayout();
        },

        /**
         * isPasswordValid: This function checks whether the password is valid or not
         * @param {Object} enteredPassword password entered by the user
         @return {boolean} true if the password is valid,false if it is not
        */
        isPasswordValid: function (enteredPassword) {
            return applicationManager.getValidationUtilManager().isPasswordValidForPolicy(enteredPassword);
        },

        /**
         * This function checks whether the entered password and reenter Password are matched or not
         * @return {boolean} true if they match,false if they do not match  
         */
        isPasswordValidAndMatchedWithReEnteredValue: function () {
            if (this.view.newUsernamepasswordsetting.tbxNewPassword.text && this.view.newUsernamepasswordsetting.tbxMatchPassword.text) {
                if (this.view.newUsernamepasswordsetting.tbxNewPassword.text === this.view.newUsernamepasswordsetting.tbxMatchPassword.text) {
                    return true;
                }
            }
            return false;
        },

        matchPwdKeyUp: function () {
            if (this.view.newUsernamepasswordsetting.tbxMatchPassword.text !== "") {
                this.validateConfirmPassword(this.view.newUsernamepasswordsetting.tbxMatchPassword.text);
            } else {
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
            }
            this.view.forceLayout();
        },
        /**
         * validateConfirmPassword :This function validates whether the entered password and the matched password are same or not
         * @param {Object}  enteredPassword password entered by the user
        */
        validateConfirmPassword: function (enteredPassword) {
            if (this.isPasswordValid(enteredPassword)) {

                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = true;
                    if(this.isUserNameValid(this.view.newUsernamepasswordsetting.tbxNewUserName.text.trim())) {
                        FormControllerUtility.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                } else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = kony.i18n.getLocalizedString("i18n.login.passwordmismatch");
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                    FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                }
            } else {
                if (enteredPassword.length === 0) {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                }
                else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = kony.i18n.getLocalizedString("i18n.login.invalidPassword");
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                }
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.flxUsernameAndPassword.parent.forceLayout();
        },

        validateUserName: function () {
            var userName = this.view.newUsernamepasswordsetting.tbxNewUserName.text.trim();
            var password = this.view.newUsernamepasswordsetting.tbxNewPassword.text;
            var confirmPassword = this.view.newUsernamepasswordsetting.tbxMatchPassword.text;
            if(this.isUserNameValid(userName) && this.isPasswordValid(password) && this.isPasswordValidAndMatchedWithReEnteredValue() ) {
                FormControllerUtility.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
            } else {
                FormControllerUtility.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.forceLayout();
        },

        isUserNameValid : function(userName) {
            return userName ? true : false;
        }
    };
});