/**
 * Login form controller which will handle all login page UI changes
 * @module frmLoginController
 */

define(['FormControllerUtility', 'Deeplinking', 'ViewConstants', 'CommonUtilities', 'OLBConstants'], function (FormControllerUtility, Deeplinking, ViewConstants, CommonUtilities, OLBConstants) {
    var DEVICE_TYPES = {
        DESKTOP: {},
        IPHONE: { linkConfigKey: 'iphoneNativeAppLink', image: ViewConstants.IMAGES.APP_STORE },
        IPAD: { linkConfigKey: 'ipadNativeAppLink', image: ViewConstants.IMAGES.APP_STORE },
        ANDROID_PHONE: { linkConfigKey: 'androidPhoneNativeAppLink', image: ViewConstants.IMAGES.PLAY_STORE },
        ANDROID_TABLET: { linkConfigKey: 'androidTabletNativeAppLink', image: ViewConstants.IMAGES.PLAY_STORE },
    }
    var orientationHandler = new OrientationHandler();
  	this.customerPhone = "";
 	this.customerEmail = "";
    return /** @alias module:frmLoginController */{
        /**
         * Method to load and return Auth Module
         * @returns {object} Auth Module object. 
         */
        loadAuthModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        },

        /**
         * Function to be called on pre show of the frmLogin 
         */
        onPreShow: function () {
            var authModule = this.loadAuthModule();
            this.attachToModule(authModule);
            this.presentationValidationUtility = applicationManager.getPresentationValidationUtility();
            this.dataProcessUtilityManager = applicationManager.getDataProcessorUtility();
            this.validationUtilManager = applicationManager.getValidationUtilManager();
            this.view.main.btnForgotPassword.toolTip = "Cannot Sign In?";
            this.view.forceLayout();
            this.restoreOriginalMainLoginUIChanges();
            this.preshowFrmLogin();
            this.initializeResponsiveViews();
            this.setLanguages();
            this.view.BrowserCheckPopup.setOnBrowserDownloaded(this.setOnBrowserDownloadedClicked);
            this.view.BrowserCheckPopup.setOnContinue(this.setOnBrowserCheckContinue);
            if (applicationManager.getStorageManager().getStoredItem("langObj")) {
                var langObj = applicationManager.getStorageManager().getStoredItem("langObj").language;
                this.showDefaultLanguageOnLoginScreen(langObj);
            }
        },
        frmLoginInit:function(){
            this.view.tbxAutopopulateIssueFix.secureTextEntry = true;
            this.view.tbxAutopopulateIssueFix.setVisibility(true);
        },
      setOnBrowserDownloadedClicked: function(params){
            //application manager service call
            applicationManager.logBrowser(params);
        },

        setOnBrowserCheckContinue: function(params) {
            applicationManager.logBrowser(params);
        },
      /**
        * setLanguages :This function sets the languages in the dropdown list
        */
       setLanguages: function () {
        var langlist = this.getLanguageMasterData();
        var languages = [];
        for (var lang in langlist) {
         if (langlist.hasOwnProperty(lang)) {
            var temp = {
                "lblLang": lang,
                "lblSeparator": "a"
            };
            languages.push(temp);
            }
        }
        this.view.segLanguagesList.setData(languages);
    },

    /**
    * showDefaultLanguage :This function sets the default language in the textbox as the value in local store or coming from backend
    * @param {String} lang - selected language
    */
    showDefaultLanguageOnLoginScreen: function (lang) {
        this.view.lblLanguage.text = this.getFrontendLanguage(lang);
        this.setLocale(lang);
    },

    /**
    * setLocale :This function sets the locale selected
    * @param {String} lang - selected language
    */
    setLocale: function (lang) {
        var localeCode = applicationManager.getConfigurationManager().locale[lang];
        kony.i18n.setCurrentLocaleAsync(localeCode, function () {
            applicationManager.getStorageManager().setStoredItem("langObj", { language: lang });
            applicationManager.getConfigurationManager().setLocaleAndDateFormat({ "data": {} });
        }, function () { });
    },
      /**
     * Method to fetch languages JSON
     */
    getLanguageMasterData: function() {
        return {
           "US English" : "en_US", 
           "UK English" : "en_GB", 
           "Spanish" : "es_ES",
           "German" : "de_DE",
           "French" : "fr_FR"    
        }
    },
     /**
     * Method to fetch language from key
     * @param {String} value - selected language
     * @param {Object} langObject - language Object
     */
    getValueFromKey : function(value,langObject){
      for (var key in langObject) {
        if (langObject.hasOwnProperty(key)) {
           var shortLang = langObject[key];
           if(shortLang===value){
               return key;
           }
        }
      }
    },
     /**
     * Method to change the selected language to backend language string
     * @param {String} lang - selected language
     */
    getBackendLanguage : function(lang){
         var languageData = this.getLanguageMasterData();
         var configManager = applicationManager.getConfigurationManager();
         var langObject = configManager.locale;
         for(var key in languageData) {
            if (languageData.hasOwnProperty(key)) {
                if(key===lang){
                  return this.getValueFromKey(languageData[key],langObject);
                }
           }
        }
    },
      /**
     * Method to change the selected language to backend language string
     * @param {String} lang - selected language
     */
    getFrontendLanguage : function(lang){
        var languageData = this.getLanguageMasterData();
        var configManager = applicationManager.getConfigurationManager();
        var langObject = configManager.locale;
        for(var key in langObject) {
           if (langObject.hasOwnProperty(key)) {
               if(key===lang){
                 return this.getValueFromKey(langObject[key],languageData);
               }
          }
       }
   },
    /**
     * showDefaultLanguage :This function sets the default language in the textbox if nothing is set in local store
     */
    showDefaultLanguage: function () {
        this.view.lblLanguage.text = this.view.segLanguagesList.data[0].lblLang;
        this.setLocale(this.getBackendLanguage(this.view.segLanguagesList.data[0].lblLang));
    },
    hideTextBox:function(){
            var scopeObj = this;
            function timerFunc() {
                scopeObj.view.tbxAutopopulateIssueFix.setVisibility(false);
                kony.timer.cancel("mytimer");
              }
            kony.timer.schedule("mytimer",timerFunc, 0.5, false); 
              
        },
        /**
         * Function to be called on post show of the frmLogin
         */
        onPostShow: function () {
            var scopeObj = this;
            scopeObj.hideTextBox();
            this.view.letsverify.flxDateInput.setVisibility(true);
            this.view.letsverify.flxDOB.setVisibility(false);
            this.view.letsverify.DateInput.onPostShow(applicationManager.getFormatUtilManager().getDateFormat(),"frmLogin_letsverify_DateInput");
            this.view.letsverify.DateInput.textChangeCallback = function() {
                this.view.letsverify.lblWrongInfo.isVisible = false;
                var text = this.view.letsverify.DateInput.getText();
                if(text.length === 10) {
                    var date = this.view.letsverify.DateInput.getDateObject();
                    if(!(date instanceof Date) || isNaN(date.getDay())) {
                        this.view.letsverify.lblWrongInfo.text =  kony.i18n.getLocalizedString("i18n.common.invalidDOB");
                        this.view.letsverify.lblWrongInfo.isVisible = true;
                          this.view.letsverify.DateInput.setText("");
                    } else {
                        this.view.letsverify.lblWrongInfo.isVisible = false;
                    }
                }
                this.view.forceLayout();
                this.allFieldsCheck();
                
            }.bind(this);
            if (applicationManager.getStorageManager().getStoredItem("langObj")) {
                var langObj = applicationManager.getStorageManager().getStoredItem("langObj").language;
                this.showDefaultLanguageOnLoginScreen(langObj);
            }
            applicationManager.getNavigationManager().applyUpdates(this);
            var configManager = applicationManager.getConfigurationManager();
            if (configManager.isAppPropertiesLoaded == "false") {
                applicationManager.getPresentationUtility().showLoadingScreen();
            }
            this.view.main.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.login.RememberMe");
            this.view.main.tbxPassword.secureTextEntry = true;
            var width = kony.application.getCurrentBreakpoint();
            if (width === 1400) this.view.main.lblWelcomeMobile.left = "0dp";  
            else if((width === 1024 && orientationHandler.isTablet)||(width === 1024 && orientationHandler.isDesktop)) this.view.main.lblWelcomeMobile.left = "33%";
            else if((width === 640 && orientationHandler.isMobile)||(width === 640 && orientationHandler.isDesktop))  this.view.main.lblWelcomeMobile.left = "20%";
            else if((width === 768 && orientationHandler.isTablet)||(width === 768 && orientationHandler.isDesktop)) this.view.main.lblWelcomeMobile.left = "30%";
            //executing deeplinking function to validate csr mode login
            this.view.AlterneteActionsSignIn.fontIconOption.text = "L";
            this.view.AlterneteActionsResetPassword.fontIconOption.text = "r";
            this.view.AlterneteActionsEnterCVV.fontIconOption.text = "x";
            this.view.AlterneteActionsEnterPIN.fontIconOption.text = "y";
            //this.view.AlterneteActionsLoginNow.fontIconOption.text = "l";
            this.view.logOutMsg.AlterneteActionsLoginNow.fontIconOption.skin = "sknlblfonticon24px0273e3";
            this.view.logOutMsg.AlterneteActionsLoginNow.fontIconOption.text = "V";
            //this.view.orline.top="36%";
            this.view.AlterneteActionsSignIn.skin = "sknBGFFFFFBdrE3E3E3BdrRadius2Px";
            this.view.AlterneteActionsSignIn.hoverSkin = "sknBGFFFFFBdrE3E3E3BdrRadius2PxHover";
            this.view.AlterneteActionsResetPassword.skin = "sknBGFFFFFBdrE3E3E3BdrRadius2Px";
            this.view.AlterneteActionsResetPassword.hoverSkin = "sknBGFFFFFBdrE3E3E3BdrRadius2PxHover";

            this.view.AlterneteActionsEnterCVV.skin = "sknBGFFFFFBdrE3E3E3BdrRadius2Px";
            this.view.AlterneteActionsEnterCVV.hoverSkin = "sknBGFFFFFBdrE3E3E3BdrRadius2PxHover";
            this.view.AlterneteActionsEnterPIN.skin = "sknBGFFFFFBdrE3E3E3BdrRadius2Px";
            this.view.AlterneteActionsEnterPIN.hoverSkin = "sknBGFFFFFBdrE3E3E3BdrRadius2PxHover";
            /*this.view.flxDropdown.onClick = function () {
                if (scopeObj.view.flxLanguagePicker.isVisible === true) {
                    scopeObj.view.flxLanguagePicker.isVisible = false;
                    scopeObj.view.lblCheckBox.text = "O";
                } else {
                    scopeObj.view.flxLanguagePicker.isVisible = true;
                    scopeObj.view.lblCheckBox.text = "P";
                }   
            };*/
            this.view.flxDropdown.onClick = function() {
                if (scopeObj.view.lblCheckBox.text === "O" && (scopeObj.view.lblCheckBox.origin===false)||scopeObj.view.lblCheckBox.origin===undefined) {
                    var right = scopeObj.view.flxMain.frame.width - (scopeObj.view.flxDropdown.frame.width+scopeObj.view.flxDropdown.frame.x);
                    scopeObj.view.flxLanguagePicker.left = "";
                    //TODO: remove Desktop conditional statement.
                    if(orientationHandler.isDesktop)
                        right+=25;
                    scopeObj.view.flxLanguagePicker.right = right+"dp";
                    
                    scopeObj.view.flxLanguagePicker.isVisible = true;
                    scopeObj.view.lblCheckBox.text = "P";
                    scopeObj.view.lblCheckBox.origin = true;
                } else {           
                    scopeObj.view.flxLanguagePicker.isVisible = false;
                    scopeObj.view.lblCheckBox.text = "O";
                    scopeObj.view.lblCheckBox.origin = false;
                }
            };
            this.view.logOutMsg.AlterneteActionsLoginNow.skin = "sknBGFFFFFBdrE3E3E3BdrRadius2Px";
            this.view.logOutMsg.AlterneteActionsLoginNow.hoverSkin = "sknBGFFFFFBdrE3E3E3BdrRadius2PxHover";

            var handCursor = document.querySelectorAll(".sknBGFFFFFBdrE3E3E3BdrRadius2Px");
            for (var i = 0; i < handCursor.length; i++) {
                handCursor[i].style.cursor = "pointer";
            }
            var sessiontoken = Deeplinking.deeplinking();
            if (CommonUtilities.isCSRMode()) {
                this.view.setVisibility(false);
                this.loadAuthModule().presentationController.onLogin({
                    "username": "",
                    "password": "",
                    "rememberMe": false,
                    "sessiontoken": sessiontoken
                });
            }
            this.setFlowActions();
            this.setmainflexheight();
            this.onLoadChangePointer();
            this.view.main.lblFavoriteEmailCheckBox.txt = OLBConstants.FONT_ICONS.CHECBOX_SELECTED;
          this.view.main.lblFavoriteEmailCheckBox.skin = OLBConstants.SKINS.CHECKBOX_SELECTED_SKIN; 
            this.view.CustomFeedbackPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.CustomerFeedback.TakeSurvey");
            this.view.CustomFeedbackPopup.btnNo.toolTip = kony.i18n.getLocalizedString("i18n.CustomerFeedback.MaybeLater");
            FormControllerUtility.disableButton(this.view.main.btnLogin);
            FormControllerUtility.disableButton(this.view.letsverify.btnProceed);
            FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            FormControllerUtility.disableButton(this.view.ResetOrEnroll.btnNext);
            FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            this.view.forceLayout();//Reset complete view at once 


            //Move to Form Init Actions
            this.view.newpasswordsetting.tbxNewPassword.onBeginEditing = this.passwordEditing;
            this.view.newpasswordsetting.tbxNewPassword.onKeyUp = this.newPwdKeyUp;
            this.view.newpasswordsetting.tbxMatchPassword.onKeyUp = this.matchPwdKeyUp;
            this.view.main.imgViewPassword.onTouchEnd = this.showPassword;
            this.view.imgClose.onTouchEnd = this.restoreOriginalMainLoginUIChanges.bind(this);
            this.view.imgCloseResetPassword.onTouchEnd = this.emptyLoginData.bind(this);
            this.view.imgCloseResetPswd.onTouchEnd = this.emptyLoginData.bind(this);
            this.view.imgCloseResetUsingCVV.onTouchEnd = this.emptyLoginData.bind(this);
            this.view.flxCloseFontIconParent.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseSendOTP.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseResetUsingOTP.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseBlocked.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseEnroll.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseWelcomeBack.onClick = this.emptyLoginData.bind(this);
            this.view.flxCloseResetSuccessful.onClick = this.emptyLoginData.bind(this);
          this.view.flxCloseSelectUsername.onClick = this.emptyLoginData.bind(this);
          this.view.flxCloseMFA.onClick = this.emptyLoginData.bind(this);
            //Footer actions.
            this.view.btnFaqs.onClick = scopeObj.loadAuthModule().presentationController.navigateToFAQ.bind(scopeObj);
            this.view.btnContactUs.onClick = scopeObj.loadAuthModule().presentationController.navigateToContactUs.bind(scopeObj);
            this.view.btnPrivacy.onClick = scopeObj.loadAuthModule().presentationController.navigateToPrivacyPrivacy.bind(scopeObj);
            this.view.btnTermsAndConditions.onClick = scopeObj.loadAuthModule().presentationController.navigateToTermsAndConditions.bind(scopeObj);

            this.view.btnLocateUs.onClick = function () {
                scopeObj.view.flxLogoutMsg.isVisible = false;
                scopeObj.loadAuthModule().presentationController.navigateToLocateUs();
            };
            kony.timer.schedule("masking", function () {
                scopeObj.view.main.tbxPassword.secureTextEntry = true;
            }, 1, false);
            this.initializeResponsiveViews();
            this.setupNativeAppLink();
              //Workaround for password autocomplete issue.
            if(document.getElementById("frmLogin_main_tbxPassword")) {
                document.getElementById("frmLogin_main_tbxPassword").autocomplete = "new-password";
            }     
          this.view.logOutMsg.AlterneteActionsLoginNow.onClick = function(){
            scopeObj.loginLater(scopeObj.view.flxLogoutMsg);
          };
          this.view.logOutMsg.AlterneteActionsLoginNow.rtxCVV.onTouchEnd = function(){
             scopeObj.loginLater(scopeObj.view.flxLogoutMsg);
          };
            this.view.main.tbxPassword.onBeginEditing = function () {
                if (scopeObj.view.main.flxPassword.skin == "sknBorderFF0101") {
                    scopeObj.credentialsMissingUIChangesAnti();
                }
                scopeObj.setFocusSkin(scopeObj.view.main.flxPassword);
            };
        },
        setFocusSkin: function (flexWidget) {
            flexWidget.skin = "sknFlxBorder4A90E23px";
        },
        setNormalSkin: function (flexWidget) {
            flexWidget.skin = "sknBorderE3E3E3";
        },
     
      
        /**
           * showDates :This function sets the Date Of Birth 
        */
        showDates: function () {
            this.setNoOfDays();
            this.allFieldsCheck();
        },

        /**
           * setNoOfDays :This function sets the Date Of Birth (day,month and year)
          */
        setNoOfDays: function () {
            var selectedMonth = this.view.letsverify.lbxMonth.selectedKey;
            var dayArray = [];

            dayArray = [{ "daykey": "day0", "dayvalue": "Day" }, { "daykey": "day1", "dayvalue": 1 }, { "daykey": "day2", "dayvalue": 2 }, { "daykey": "day3", "dayvalue": 3 },
            { "daykey": "day4", "dayvalue": 4 }, { "daykey": "day5", "dayvalue": 5 }, { "daykey": "day6", "dayvalue": 6 },
            { "daykey": "day7", "dayvalue": 7 }, { "daykey": "day8", "dayvalue": 8 }, { "daykey": "day9", "dayvalue": 9 },
            { "daykey": "day10", "dayvalue": 10 }, { "daykey": "day11", "dayvalue": 11 }, { "daykey": "day12", "dayvalue": 12 },
            { "daykey": "day13", "dayvalue": 13 }, { "daykey": "day14", "dayvalue": 14 }, { "daykey": "day15", "dayvalue": 15 },
            { "daykey": "day16", "dayvalue": 16 }, { "daykey": "day17", "dayvalue": 17 }, { "daykey": "day18", "dayvalue": 18 },
            { "daykey": "day19", "dayvalue": 19 }, { "daykey": "day20", "dayvalue": 20 }, { "daykey": "day21", "dayvalue": 21 },
            { "daykey": "day22", "dayvalue": 22 }, { "daykey": "day23", "dayvalue": 23 }, { "daykey": "day24", "dayvalue": 24 },
            { "daykey": "day25", "dayvalue": 25 }, { "daykey": "day26", "dayvalue": 26 }, { "daykey": "day27", "dayvalue": 27 },
            { "daykey": "day28", "dayvalue": 28 }
            ];
            var dayV, dayK;
            var dayObj;
            if (selectedMonth === 'm1' || selectedMonth === 'm3' || selectedMonth === 'm5' || selectedMonth === 'm7' || selectedMonth === 'm8' || selectedMonth === 'm10' || selectedMonth === 'm12') {
                var MaxMonths = 31;
                for (var i = 29; i <= MaxMonths; i++) {
                    dayK = "day" + i;
                    dayV = i;
                    dayObj = { "daykey": dayK, "dayvalue": dayV };
                    dayArray.push(dayObj);
                }
            }
            else if (selectedMonth === 'm4' || selectedMonth === 'm6' || selectedMonth === 'm9' || selectedMonth === 'm11') {
                var MonthEnd = 30;
                for (var j = 29; j <= MonthEnd; j++) {
                    dayK = "day" + j;
                    dayV = j;
                    dayObj = { "daykey": dayK, "dayvalue": dayV };
                    dayArray.push(dayObj);
                }
            }
            else {
                if ((this.view.letsverify.lbxYear.selectedKeyValue !== undefined) && (this.view.letsverify.lbxYear.selectedKeyValue !== null)) {
                    var thisYear = this.view.letsverify.lbxYear.selectedKeyValue[1];
                    var leapCheck1 = 4;
                    var leapCheck2 = 100;
                    var leapCheck3 = 400;
                    var daysAdded = 29;
                    if (((thisYear % leapCheck1 === 0) && (thisYear % leapCheck2 !== 0)) || (thisYear % leapCheck3 === 0)) {
                        dayK = "day" + daysAdded;
                        dayV = daysAdded;
                        dayObj = { "daykey": dayK, "dayvalue": dayV };
                        dayArray.push(dayObj);
                    }
                }
            }
            this.view.letsverify.lbxDate.masterDataMap = [dayArray, "daykey", "dayvalue"];
        },
        /**
         * Method to set flow actions
         */
        setFlowActions: function () {
            var scopeObj = this;
            this.view.letsverify.flxWhatIsSSN.onClick = function () {
                if (scopeObj.view.AllForms.isVisible === false)
                    scopeObj.view.AllForms.setVisibility(true);
                else
                    scopeObj.view.AllForms.setVisibility(false);
            };
            this.view.AllForms.flxCross.onClick = function () {
                scopeObj.view.AllForms.setVisibility(false);
            };
        },

        /**
         * UI Method to adjust screen height w.r.t width
         */
        setmainflexheight: function () {
//             if (kony.application.getCurrentBreakpoint() == 1400) {
//                 var adjustScreenWidthToHeightRatio = 0.5652;
//                 var height = kony.os.deviceInfo().screenWidth * adjustScreenWidthToHeightRatio;
//                 this.view.flxMain.height = height + "dp";
//             }
        },

        /**
         * The function calls the onLogin method depending on the entered username whether it is masked or not once clicked on Login Button
         */
        onLoginClick: function () {
            var status = FormControllerUtility.isFontIconChecked(this.view.main.lblFavoriteEmailCheckBox);
            this.view.main.rtxErrorMsg.setVisibility(false);
            this.view.main.rtxErrorMsgUser.setVisibility(false);
            this.view.flxMain.flxLogoutMsg.setVisibility(false);
            // this.view.lblLanguage.skin = ViewConstants.SKINS.LOGIN_LANGUAGE;
            var enteredUserName = this.view.main.tbxUserName.text.trim();
            if (this.dataProcessUtilityManager.isUserNameMasked(enteredUserName)) {
                var unMaskedUserName = this.getUnMaskedUserName(enteredUserName);
                this.loadAuthModule().presentationController.onLogin({
                    "username": unMaskedUserName,
                    "password": this.view.main.tbxPassword.text,
                    "rememberMe": status
                });
            } else {
                this.loadAuthModule().presentationController.onLogin({
                    "username": enteredUserName,
                    "password": this.view.main.tbxPassword.text,
                    "rememberMe": status
                });
            }
        },

        /**
         * Method update view while login failure
         */
        onLoginFailure: function () {
            this.view.main.tbxPassword.text = "";
            this.view.main.tbxPassword.secureTextEntry = true;
            FormControllerUtility.disableButton(this.view.main.btnLogin);
        },

        /**
         * This function used to update form using given context
         * @param {object} context depending on the context the appropriate function is executed to update view  
         */
        updateFormUI: function (context) {
            if (context.action === "Logout") {
                this.showLogoutPage();
            }
            if (context.action === "SessionExpired") {
                this.showSessionExpiredPage();
            }
            if (context.action === "ServerDown") {
                this.showServerErrorPage(this.view.flxLogin);
            }
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
          
           if (context.action === "userNamePasswordSuccessfullyChanged") {
                this.showUserNameSuccessfulMessage(context);
            }
          
            if (context.errorMessage) {
                this.showErrorMessage(context.errorMessage);
            }
            if (context.loginFailure) {
                this.onLoginFailure();
            }
            if (context.isLanguageSelectionEnabled) {
                this.languageDropdownEnabled(context.isLanguageSelectionEnabled);
            }
            if (applicationManager.getStorageManager().getStoredItem("langObj")) {
                var langObj = applicationManager.getStorageManager().getStoredItem("langObj").language;
                this.showDefaultLanguageOnLoginScreen(langObj);
            } else if (context.defaultLanguage) {
                this.showDefaultLanguageOnLoginScreen(context.defaultLanguage);
            } else {
                this.showDefaultLanguage();
            }
            if (context.welcomeUser) {
                this.welcomeVerifiedUser(context.welcomeUser);
            }
            if (context.userEnroll) {
                this.showEnrollFlex(context.userEnroll);
            }
            if (context.fetchUserFail) {
                this.showFetchUserNameErrorUI(context.fetchUserFail);
            }
            if (context.cardsDataForResetPassword) {
                this.showResetPasswordUI(context.cardsDataForResetPassword);
            }
            if (context.cvvFailure) {
                this.showErrorForCVV();
            }
            if (context.resetPasswordFailed) {
                this.showErrorForResetPassword(context.resetPasswordFailed);
            }
            if (context.passwordPolicies) {
                this.showResetPasswordPage(context.passwordPolicies);
            }
            if (context.resetPasswordSuccss) {
                this.showResetPasswordConfirmation();
            }
            if (context.fetchOTPSuccess) {
                this.showEnterOTPPage();
            }
            if (context.OTPFailed) {
                this.showErrorForOTP();
            }
            if (context.fetchOTPBBSuccess) {
                this.showResetUsingOTPBB();
            }
            if (context.passwordPoliciesBB) {
                this.showResetPasswordPageBB(context.passwordPoliciesBB);
            }
           if (context.action === "LogOutMFA") {
                this.showLogoutPageMFA();
            }
           if (context.action === "LockOutMFA") {
                this.showLockoutPageMFA(context.lockOutTime);
            }
            if (context.browserConfiguration) {
                this.checkBrowser();
            }
          if(context.showScreenToEnterSecureCode){
            this.showScreenToEnterSecureCode(context.showScreenToEnterSecureCode);
          }
          if(context.isEnteredOTPIncorrect){
            this.showIncorrectOTPError(context.isEnteredOTPIncorrect);
          }
          if(context.isOTPReceived){
            this.showScreentoEnterOTP(context.isOTPReceived);
          }
          if(context.isOTPRequestFailed){
            this.showRequestOTPError(context.isOTPRequestFailed);
          }

          if (context.showSecureAccessCodeScreenAfterResend) {
            this.showSecureAccessCodeScreenAfterResend(context.showSecureAccessCodeScreenAfterResend);
        }

            this.initializeResponsiveViews();
        },

        /**
         * Method to hides all views in login form.
         */
        hideAllLoginViews: function () {
            this.view.flxLogin.setVisibility(false);
            this.view.flxVerification.setVisibility(false);
            this.view.flxWelcomeBack.setVisibility(false);
            this.view.flxResetPasswordOptions.setVisibility(false);
            this.view.flxSendOTP.setVisibility(false);
            this.view.flxResetUsingOTP.setVisibility(false);
            this.view.flxResetUsingCVV.setVisibility(false);
            this.view.flxResetPassword.setVisibility(false);
            this.view.flxLoginMFA.setVisibility(false);
            this.view.flxResetSuccessful.setVisibility(false);
            this.view.flxBlocked.setVisibility(false);
            this.view.flxEnrollOrServerError.setVisibility(false);
            this.view.flxEnroll.setVisibility(false);
            this.view.flxLogoutMsg.setVisibility(false);
        },
        /**
         * This function shows the logout Message on the login form
         */
        showLogoutPage: function () {
            // this.view.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.SuccessfullyLoggedOut");
            // this.view.imgLogoutSuccess.src = ViewConstants.IMAGES.LOGOUT_TICK_MARK;
            var scopeObj = this;
            this.view.logOutMsg.imgLogoutSuccess.src = ViewConstants.IMAGES.SUCCESS_GREEN;
            this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.SuccessfullyLoggedOut");
            this.view.logOutMsg.lblSuccessIcon.text = kony.i18n.getLocalizedString("i18n.login.LogOutSuccess");
//             this.view.logOutMsg.lblLoggedOut.top = "45%";
//             this.view.logOutMsg.lblLoggedOut.height = "27%";
            this.restoreOriginalMainLoginUIChanges();
            this.changesAfterLogout();
            kony.timer.schedule("survey", function () {
                scopeObj.changesAfterLogout();
            }, 1, false);
            FormControllerUtility.hideProgressBar(scopeObj.view);
            this.initializeResponsiveViews();
        },

      
       /**
         * This function shows the logout Message on the login form
         */
        showLogoutPageMFA: function () {
           var scopeObj = this;
            this.view.logOutMsg.imgLogoutSuccess.src = "error_yellow.png";
            this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.logoutMFA");
            this.view.logOutMsg.lblSuccessIcon.setVisibility(false);
//             this.view.logOutMsg.lblLoggedOut.top = "45%";
//             this.view.logOutMsg.lblLoggedOut.height = "27%";
            this.restoreOriginalMainLoginUIChanges();
            this.changesAfterLogout();
            kony.timer.schedule("survey", function() {
                scopeObj.changesAfterLogout();
            }, 1, false);
            FormControllerUtility.hideProgressBar(scopeObj.view);
            this.initializeResponsiveViews();
        },
      
      
        /**
         * This function shows the logout Message on the login form
         */
        showLockoutPageMFA: function (lockTime) {
          var scopeObj = this;
            this.view.logOutMsg.imgLogoutSuccess.src = "error_yellow.png";
            this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.LockoutMFA") + " " + lockTime + " " + kony.i18n.getLocalizedString("i18n.mfa.minutes");
            this.view.logOutMsg.lblSuccessIcon.setVisibility(false);
//             this.view.logOutMsg.lblLoggedOut.top = "45%";
//             this.view.logOutMsg.lblLoggedOut.height = "27%";
            this.restoreOriginalMainLoginUIChanges();
            this.changesAfterLogout();
            kony.timer.schedule("survey", function() {
                scopeObj.changesAfterLogout();
            }, 1, false);
            this.initializeResponsiveViews();
        },

      
       showUserNameSuccessfulMessage :function (context) {
               if (context.text === "username") {
                      this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.common.usernamechanged");  
                  } else if (context.text === "password") {
                      this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.common.passwordchanged");
                  }
              this.view.logOutMsg.imgLogoutSuccess.src = ViewConstants.IMAGES.SUCCESS_GREEN;
              this.restoreOriginalMainLoginUIChanges();
              this.initializeResponsiveViews();
            },
      
        /**
         * This function shows the message when the session is Expired
         */
        showSessionExpiredPage: function () {
            this.view.logOutMsg.imgLogoutSuccess.src = ViewConstants.IMAGES.SUCCESS_GREEN;
            this.view.logOutMsg.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.SessionExpired");
//             this.view.logOutMsg.lblLoggedOut.top = "22%";
//          this.view.logOutMsg.lblLoggedOut.height = "9%";
            this.restoreOriginalMainLoginUIChanges();
            this.changesAfterLogout();
            this.initializeResponsiveViews();
        },

        /**
    * selectYourLanguage :This function sets the language selected from the dropdown menu to the label and also stores it in localstore
    */
        selectYourLanguage: function () {
            this.view.flxLanguagePicker.setVisibility(false);
            var langSelected = JSON.stringify(this.view.segLanguagesList.selectedRowItems[0]["lblLang"]);
            langSelected = langSelected.replace(/"/g, "");
            this.showLanguageSelectionPopUp(langSelected);
        },
        /**
         * Method to show language selection popup
         * @param {String} langSelected - selected language
         */
        showLanguageSelectionPopUp: function (langSelected) {
            var scopeObj = this;
            this.view.CustomChangeLanguagePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.changeLanguageMessage") + " " + langSelected + "?";
            this.view.flxChangeLanguage.setVisibility(true);
            this.view.CustomChangeLanguagePopup.btnYes.onClick = function () {
                applicationManager.getStorageManager().setStoredItem("langObj", { language: scopeObj.getBackendLanguage(langSelected) });
                var localeCode = applicationManager.getConfigurationManager().locale[scopeObj.getBackendLanguage(langSelected)];
                kony.i18n.setCurrentLocaleAsync(localeCode, function () {
                    applicationManager.getStorageManager().setStoredItem("langObj", { language: scopeObj.getBackendLanguage(langSelected) });
                    applicationManager.getConfigurationManager().setLocaleAndDateFormat({ "data": {} });
                    applicationManager.getNavigationManager().navigateTo("frmLoginLanguage");
                }, function () { });
                this.view.lblLanguage.text = langSelected;
                this.hideLanguageSelectionPopUp();
            }.bind(this);
            this.view.CustomChangeLanguagePopup.btnNo.onClick = this.hideLanguageSelectionPopUp.bind(this);
            this.view.CustomChangeLanguagePopup.flxCross.onClick = this.hideLanguageSelectionPopUp.bind(this);
        },
        hideLanguageSelectionPopUp: function () {
            this.view.lblCheckBox.text = "O";
            this.view.flxChangeLanguage.setVisibility(false);
        },
        updateFeedbackId : function(){
            var feedbackID = applicationManager.getStorageManager().removeStoredItem("feedbackUserId");
            if(feedbackID){
                applicationManager.getFeedbackManager().updateFeedbackId(feedbackID);
            }
        },        
        /**
         * This UI function does the needed changes once we click on logout button to the login form
         */
        changesAfterLogout: function () {
            this.updateFeedbackId();
            this.view.flxLogoutMsg.setVisibility(true);
            if(applicationManager.getConfigurationManager().isFeedbackEnabled === "false" ){
                this.view.flxFeedbackTakeSurvey.setVisibility(false);
            }
            else{
                if(kony.application.getCurrentBreakpoint()===1400 && orientationHandler.isTablet){
                    this.view.CustomFeedbackPopup.width = "60%";
                }
                this.view.flxFeedbackTakeSurvey.setVisibility(true);
            }
            if(kony.application.getCurrentBreakpoint()===640 || orientationHandler.isMobile){
              this.view.imgKony.left = "14%";
            }
            // this.view.lblLanguage.skin = ViewConstants.SKINS.LANGUAGE1;
            this.view.flxMain.skin = ViewConstants.SKINS.LOGIN_AFTER_LOGOUT;
            // this.view.flxLogin.top = "120dp";
            //this.view.flxMain.top = "0dp";
            this.view.flxLogin.setVisibility(false);
            this.view.logOutMsg.AlterneteActionsLoginNow.fontIconOption.skin = "sknlblfonticon24px0273e3";
            this.view.logOutMsg.AlterneteActionsLoginNow.fontIconOption.text = "V";
            //this.view.main.btnForgotPassword.isVisible = false;
            this.view.flxMain.parent.forceLayout();
        },
        /**
         * This  function shows the error Message to the user
         * @param {string} errorMessage error message to display
         */
        showErrorMessage: function (errorMessage) {
            if (this.view.main.flxLoginUser.isVisible) {
                this.view.main.rtxErrorMsgUser.text = errorMessage;
                this.view.main.rtxErrorMsgUser.setVisibility(true);
            } else {
                this.view.main.rtxErrorMsg.text = errorMessage;
                this.view.main.rtxErrorMsg.setVisibility(true);
            }
            this.view.main.flxLoginUser.parent.forceLayout();
        },

         showErrorForResetPassword: function(errorMessage) {
            this.view.newpasswordsetting.lblErrorInfo.text = errorMessage.dbpErrMsg;
            this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
            this.view.newpasswordsetting.tbxNewPassword.text = "";
            this.view.newpasswordsetting.imgValidPassword.isVisible = false;
            this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
            FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            this.view.newpasswordsetting.tbxMatchPassword.text = "";
            this.view.forceLayout();
        },

        /**
         * This  function removes the special Character "*" while entering the username because for masking the username we are using "*"
         */
        removeSpecialCharacter: function () {
            var str = this.view.main.tbxUserName.text;
            if (str) {
                if (str.charAt(str.length - 1) === '*') {
                    this.view.main.tbxUserName.text = str.substring(0, str.length - 1);
                }
            }
        },

        /**
        * The function is used to show the list of masked usernames saved when the user clicks on the textbox if the value in the textbox is empty,once the user enters any characters the list is hidden
        */
        showUserNamesBasedOnlength: function () {
            var self = this;
            if (self.view.main.tbxUserName.text.trim().length === 0) {
                self.showUserNames();
            }
        },

        /**
         * The function is used to hide the list of masked usernames saved when the user enters any characters and and function call to check whether to enable login button  if both username and password are entered
         */
        hideUserNames: function () {
            var self = this;
            self.enableLogin(self.view.main.tbxUserName.text.trim(), self.view.main.tbxPassword.text);
            if (self.view.main.tbxUserName.text) {
                self.hideUserNameSuggestions();
            }
        },

        /**
         * The function is used to turn the visibility on or off for the Language Selection
         */
        languageDropdownEnabled: function (isEnabled) {
            if(isEnabled === "true") {
                this.view.flxDropdown.setVisibility(true);
            } else {
                this.view.flxDropdown.setVisibility(false);
            }
            this.view.forceLayout();
        },

        /**
         * The function is used to check whether the username contains the mask Character "*" and shows/hides the username suggestions based on the text entered in the uusername field
         */
        checkifUserNameContainsMaskCharacter: function () {
            var self = this;
            self.removeSpecialCharacter();
            if (self.view.main.tbxUserName.text.trim().length > 0) {
                self.hideUserNameSuggestions();
            } else {
                self.showUserNames();
            }
            self.enableLogin(self.view.main.tbxUserName.text.trim(), self.view.main.tbxPassword.text);
        },

        /**
         * The function is used to check whether both username and password are entered and enable the login button and onLoginClick function is called
         */
        toEnableOrDisableLogin: function () {
            var self = this;
            var username = self.view.main.tbxUserName.text.trim();
            var password = self.view.main.tbxPassword.text;
            if (self.presentationValidationUtility.isValidTextBox(username) && self.presentationValidationUtility.isValidTextBox(password) && password.length >= 1 && username.length >= 1) {
                self.onLoginClick();
            }
        },


        /**
         * This function checks whether to enable the login button if both username and password are entered or disable the login button
         * @param {string} username - user name entered by the user
         * @param {string} password - password entered by the user
         */
        enableLogin: function (username, password) {
            if (this.presentationValidationUtility.isValidTextBox(username) && this.presentationValidationUtility.isValidTextBox(password) && password.length >= 1 && username.length >= 1) {
                this.btnLoginToggle(true);
            } else {
                this.btnLoginToggle(false);
            }
        },

        /**
         * This  function enables the login button if both username and password are entered or disables the login button
         * @param {boolean} status -- true (enabled) false(disabled)
         */
        btnLoginToggle: function (status) {
            if (status === true) {
                FormControllerUtility.enableButton(this.view.main.btnLogin);
            } else {
                FormControllerUtility.disableButton(this.view.main.btnLogin);
            }
        },

        /**
         * This  function checks whether there are any usernames saved or not..if there are no usernames then it hides the dropdown or shows the masked usernames iin the dropdown
         */
        showUserNames: function () {
            var storagaManager = applicationManager.getStorageManager();
            var savedUserNames = JSON.parse(storagaManager.getStoredItem("olbNames"));
            if (savedUserNames === null || savedUserNames.length === 0) {
                this.view.main.flxUserDropdown.isVisible = false;
            } else {
                this.showUserNameSuggestions(JSON.parse(storagaManager.getStoredItem("olbNames")));
            }
        },

        /**
         * This  function shows the list of the usernames stored in the local Storage
         * @param {string} enteredText text entered by user in user name field
         */
        showUserNameSuggestions: function (enteredText) {
            if (enteredText !== null && enteredText !== undefined) {
                this.view.main.segUsers.removeAll();
                var x = [];
                for (var index in enteredText) {
                    for (var attr in enteredText[index]) {
                        var y = {
                            "lblusers": enteredText[index][attr]
                        };
                        x.push(y);
                    }
                }
                this.view.main.segUsers.addAll(x);
            }
            this.view.main.flxUserDropdown.setVisibility(true);
            this.view.main.flxUserDropdown.parent.forceLayout()
        },

        /**
         * This  function allows to select one userName from the dropDown and hides the dropdown once the username is selected
         */
        selectUserName: function () {
            var val = this.view.main.segUsers.selectedItems[0].lblusers;
            this.view.main.tbxUserName.text = val;
            this.view.main.flxUserDropdown.isVisible = false;
            this.enableLogin(val, this.view.main.tbxPassword.text);
        },
        /**
         * This function hides the dropDown if there are no usernames saved in the local Storage
         */
        hideUserNameSuggestions: function () {
            this.view.main.flxUserDropdown.isVisible = false;
        },

        /**
         * This function shows the masked password on click of the eye icon
         */
        showPassword: function () {
            this.view.main.tbxPassword.secureTextEntry = !this.view.main.tbxPassword.secureTextEntry
        },

        /**
         * This function toggle check box images of rememberMe checkbox
         */
        rememberMe: function () {
            CommonUtilities.toggleFontCheckbox(this.view.main.lblFavoriteEmailCheckBox);
        },

        /**
         * This function is called on PreShow of the Login form
         */
        preshowFrmLogin: function () {
            var scopeObj = this;
          scopeObj.view.OTPModule.lblWrongOTP.isVisible = false;
            var OLBLogoutStatus = kony.store.getItem('OLBLogoutStatus');
            this.view.EnrollAlert.flxServerError.top = "120dp";
          this.view.main.btnOnlineAccessEnroll.toolTip = kony.i18n.getLocalizedString("i18n.Login.Enroll");
           this.view.OTPModule.btnResendOTP.toolTip =  kony.i18n.getLocalizedString("i18n.login.ResendOtp");
          this.view.OTPModule.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed"); 
            if(kony.application.getCurrentBreakpoint()===640 || kony.application.getCurrentBreakpoint()==768 || orientationHandler.isMobile || orientationHandler.isTablet){
              this.view.lblCopyright.contentAlignment = "2";
            }else{
            this.view.lblCopyright.contentAlignment = "1";
            }
            this.view.EnrollAlert.lblAppTitle.isVisible = false;
            this.view.main.flxUserName.setVisibility(true);
            this.view.main.lblUsername.setVisibility(true);
           this.view.letsverify.lblSSN.text = kony.i18n.getLocalizedString("i18n.common.SSN");
            this.view.btnVeiwMore.hoverSkin ="sknbtn41a0edviewmoreHover";
            if (OLBLogoutStatus) {
                this.updateFormUI(OLBLogoutStatus);
                this.userName = OLBLogoutStatus.userName;
                kony.store.removeItem('OLBLogoutStatus');
            } else {
                this.view.flxLogoutMsg.setVisibility(false);
            }
            if (this.chkUserInLocal(this.userName)) {
                scopeObj.view.main.tbxUserName.text = this.dataProcessUtilityManager.maskUserName(this.userName);
            } else {
                scopeObj.view.main.tbxUserName.text = "";
            }
            // After Login error from forgot password flow we need to persist the user name
            if (this.view.main.flxLoginUser.isVisible) {
                scopeObj.view.main.tbxUserName.text = this.view.main.lblVerifiedUser.text;
            }
            // defined onBreakpointChange through studio action
            // this.view.onBreakpointChange = function () {
            //     scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            // }
            this.view.btnVeiwMore.onClick = function () {
                var config = applicationManager.getConfigurationManager();
                kony.application.openURL(config.getConfigurationValue("LINK_TO_DBX"));
            }
            scopeObj.view.main.tbxPassword.text = "";
            scopeObj.view.main.tbxPassword.secureTextEntry = true;
            if (this.view.flxEnrollOrServerError.isVisible || this.view.flxVerification.isVisible || this.view.flxLogoutMsg.isVisible) {
                scopeObj.view.flxLogin.setVisibility(false);
            } else {
                scopeObj.view.flxLogin.setVisibility(true);
            }
            this.view.flxPlayStore.setVisibility(false);
          this.view.forceLayout();
        },

        /**
         * This function is for recovering the original UI changes
         */
        restoreOriginalMainLoginUIChanges: function () {
            this.view.imgKony.setVisibility(true);
            this.view.flxMain.skin = ViewConstants.SKINS.LOGIN_MAIN_BAKGROUND;
            //this.view.flxLogin.centerX = "0dp";
            this.view.flxLogin.left = "0px";
            // this.view.flxLogin.top = "0dp";
            this.view.main.imgUserOutline.isVisible = false;
            this.view.main.imgUser.src = ViewConstants.IMAGES.DEFAULT_USERNAME;
            //this.view.main.lblAppName.isVisible = true;
            this.view.main.lblWelcome.isVisible = false;
            this.view.main.rtxErrorMsg.isVisible = false;
            this.view.main.flxLoginUser.isVisible = false;
            this.view.main.imgUserName.src = ViewConstants.IMAGES.USERNAME;
            //this.view.main.flxUserName.top = "9.68%";
            this.view.main.btnOnlineAccessEnroll.isVisible = true;
            this.view.flxResetUsingOTP.setVisibility(false);
            this.view.flxResetUsingCVV.setVisibility(false);
            this.view.flxResetPassword.setVisibility(false);
            this.view.flxResetSuccessful.setVisibility(false);
            this.view.flxWelcomeBack.setVisibility(false);
            this.view.flxLogin.setVisibility(true);
            this.view.flxVerification.setVisibility(false);
            this.view.flxResetPasswordOptions.setVisibility(false);
            this.view.flxSendOTP.setVisibility(false);
            this.view.flxBlocked.setVisibility(false);
            this.view.flxEnrollOrServerError.setVisibility(false);
            this.view.flxEnroll.setVisibility(false);
            this.view.main.flxLoginUser.setVisibility(false);
            this.view.flxFeedbackTakeSurvey.setVisibility(false);
            this.view.flxLoginMFA.setVisibility(false);
            this.view.imgDropdown.src = ViewConstants.IMAGES.CHEVRON_DOWN_WHITE;
            //this.view.lblLanguage.skin = ViewConstants.SKINS.LANGUAGE2;
            this.view.newpasswordsetting.tbxNewPassword.text = "";
            this.view.newpasswordsetting.tbxMatchPassword.text = "";
            this.view.newpasswordsetting.imgValidPassword.isVisible = false;
            this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
            if (this.chkUserInLocal(this.userName)) {
                this.view.main.tbxUserName.text = this.dataProcessUtilityManager.maskUserName(this.userName);
            } else {
                this.view.main.tbxUserName.text = "";
            }
            this.view.main.tbxPassword.text = "";
            this.view.flxPhoneAndEmail.setVisibility(false);
            this.view.OTPModule.flxEnterOTP.setVisibility(false);
            this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
            this.view.OTPModule.tbxCVV.text = "";
            FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
            this.view.main.rtxErrorMsg.isVisible = false;
            this.view.main.rtxErrorMsgUser.isVisible = false;
            this.view.main.tbxPassword.secureTextEntry = true;
            this.view.flxDropdown.isVisible = true;
            FormControllerUtility.disableButton(this.view.main.btnLogin);
            FormControllerUtility.disableButton(this.view.letsverify.btnProceed);
            FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            FormControllerUtility.disableButton(this.view.ResetOrEnroll.btnNext);
            FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            this.view.main.btnOpenNewAccount.isVisible = true;
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
            this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.initializeResponsiveViews();
        },

        /**
         * showServerErrorPage :This function is for showing the server DownTime Screen
         * @param {object} currentFlex - flex object
         */
        showServerErrorPage: function (currentFlex) {
            //to handle csr successs but entitlements failure
            if (!this.view.isVisible) {
                this.view.setVisibility(true);
            }
            currentFlex.isVisible = false;
            this.view.EnrollAlert.rtxServerError.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
            this.view.EnrollAlert.btnBackToLogin.setVisibility(false);
            this.view.logOutMsg.lblSuccessIcon.setVisibility(false);
            this.view.logOutMsg.lblLoggedOut.setVisibility(false);
            this.view.logOutMsg.imgLogoutSuccess.setVisibility(false);
            this.view.flxLoginMFA.setVisibility(false);
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxEnrollOrServerError.isVisible = true;
            this.view.EnrollAlert.btnBackToLogin.setVisibility(true);
            currentFlex.parent.forceLayout();
        },

        /**
         * This function is called once the user clicks on loginlater after resetting the password
         * @param {obejct} currentFlex current flex object
         */
        loginLater: function (currentFlex) {
            currentFlex.isVisible = false;
            this.restoreOriginalMainLoginUIChanges();
            applicationManager.getConfigurationManager().fetchApplicationProperties(function (res) {
                applicationManager.getNavigationManager().updateForm({
                    isLanguageSelectionEnabled: res.isLanguageSelectionEnabled,
                    defaultLanguage: res.language
                }, "frmLogin");
            }, function () { })
			// if(kony.application.getCurrentBreakpoint() === 1400){
            //   this.view.flxLogin.top = "0dp";
            // }
            // else if(kony.application.getCurrentBreakpoint() === 640){
            //   this.view.flxLogin.top = "40dp";
            // }
            // else if(kony.application.getCurrentBreakpoint() === 768 || kony.application.getCurrentBreakpoint() === 1024){
            //   this.view.flxLogin.top = "60dp";
            // }
            currentFlex.parent.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * UI Function change all button widget pointer
         */
        onLoadChangePointer: function () {
            var elems = document.querySelectorAll("input[kwidgettype='Button']");
            for (var i = 0, iMax = elems.length; i < iMax; i++) {
                elems[i].style.cursor = 'pointer';
            }
        },

        /** 
         * This function gets the unmasked username while traversing through the JSON in the local Storage
         * @param {string} enteredUserName entered username
         * @returns {string} ummasked username
         */
        getUnMaskedUserName: function (enteredUserName) {
            var names = JSON.parse(applicationManager.getStorageManager().getStoredItem("olbNames")) || [];
            var maskedUserValues = [];
            var unmaskedUserValues = [];
            maskedUserValues = names.map(function (nameObj) {
                unmaskedUserValues.push(Object.keys(nameObj)[0]);
                return nameObj[Object.keys(nameObj)[0]];
            });
            var index = maskedUserValues.indexOf(enteredUserName);
            if (index >= 0) {
                return unmaskedUserValues[index];
            }
            return null;
        },

        /**
         * This function checks if the user already present in local Storage or not
         * @param {string} username - user name entered by user
         * @returns {boolean}  true --if the username is present ,false if the username is not present
         */
        chkUserInLocal: function (username) {
            var usernames = applicationManager.getStorageManager().getStoredItem('olbNames');
            if (usernames !== null && usernames !== '') {
                usernames = JSON.parse(usernames);
                for (var index in usernames) {
                    if (this.getUnMaskedUserName(usernames[index][username]) === username) {
                        return true;
                    }
                }
            }
            return false;
        },

        /**
        * Method to show Forgot UI
        * This function is called once the user clicks on forgot button then login form visiblilty is set to false and verification flex visibility is set to true
        */
        verifyUser: function () {
            var self = this;
            this.view.flxLogoutMsg.setVisibility(false);
//             this.view.flxMain.skin = ViewConstants.SKINS.LOGIN_ERROR_BAKGROUND;
            this.view.flxLogin.isVisible = false;
            this.view.main.rtxErrorMsgUser.setVisibility(false);
            this.view.flxVerification.isVisible = true;
            this.view.flxDropdown.isVisible = false;
            if(kony.application.getCurrentBreakpoint()===640||orientationHandler.isMobile){
              this.view.flxDropdown.setVisibility(false);
              this.view.flxLanguagePicker.setVisibility(false);
            }
            if(kony.application.getCurrentBreakpoint()===768||orientationHandler.isTablet){
                this.view.flxDropdown.setVisibility(false);
                this.view.flxLanguagePicker.setVisibility(false);
            }
            if(kony.application.getCurrentBreakpoint()===1024||orientationHandler.isTablet){
                this.view.flxDropdown.setVisibility(false);
                this.view.flxLanguagePicker.setVisibility(false);
            }
          	if(kony.application.getCurrentBreakpoint() === 1400){
              	this.view.letsverify.flxLetsVerifyCntr.height = "100dp";
              	this.view.letsverify.flxLetsVerifyCntr.top = "115dp";
            }
            this.emptyUserDetails();
            this.view.imgClose.onTouchEnd = function () {
                self.view.flxLogin.isVisible = true;
                self.view.flxDropdown.isVisible = true;
                self.view.flxVerification.isVisible = false;
                self.emptyUserDetails();
                self.initializeResponsiveViews();
            }
            this.view.flxVerification.parent.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * This function clear all fields date in form Login view.
         */
        emptyLoginData: function () {
            this.emptyUserDetails();
            this.view.flxResetPasswordOptions.isVisible = false;
            this.view.flxVerification.isVisible = false;
            this.view.flxWelcomeBack.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = false;
            this.view.flxResetPassword.isVisible = false;
            this.view.flxSendOTP.isVisible = false;
            this.view.flxResetUsingOTP.isVisible = false;
            this.view.flxEnroll.isVisible = false;
            this.view.flxResetSuccessful.isVisible = false;
            this.view.flxLogin.isVisible = true;
            this.view.flxSelectUsername.isVisible = false;
            this.view.flxPhoneAndEmail.isVisible = false;
            this.view.OTPModule.flxEnterSecureAccessCode.isVisible = false;
            this.view.OTPModule.flxEnterOTP.isVisible = false;
            this.view.flxDropdown.isVisible = true;
            this.loadAuthModule().presentationController.clearForgotObject();
            this.initializeResponsiveViews();
        },

        /**
        * This function empties all the user details entered in  verify form and disable the login button
        */
        emptyUserDetails: function () {
           this.view.letsverify.tbxSSN.text = "";
            this.view.main.rtxErrorMsg.isVisible = false;
            this.view.main.flxLoginUser.setVisibility(false);
            this.view.main.flxUserName.setVisibility(true);
           this.view.main.lblUsername.setVisibility(true);
            this.view.main.rtxErrorMsgUser.isVisible = false;
            this.view.main.tbxUserName.text = "";
            this.view.main.tbxPassword.text = "";
            this.view.main.btnOnlineAccessEnroll.setVisibility(true);
            this.view.main.btnOpenNewAccount.setVisibility(true);
            this.view.main.tbxPassword.secureTextEntry = true;
            this.view.OTPModule.tbxCVV.text = "";
            FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
            this.view.letsverify.tbxLastName.text = "";
            this.view.ResetOrEnroll.tbxCVV.text = "";
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.view.newpasswordsetting.tbxNewPassword.text = "";
            this.view.newpasswordsetting.tbxMatchPassword.text = "";
            this.view.newpasswordsetting.imgValidPassword.isVisible = false;
            this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
            FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            FormControllerUtility.disableButton(this.view.ResetOrEnroll.btnNext);
            this.view.letsverify.lblWrongInfo.setVisibility(false);
          this.view.flxPhoneAndEmail.isVisible = false;
            this.view.letsverify.DateInput.setText("");
             this.view.forceLayout();
            FormControllerUtility.disableButton(this.view.letsverify.btnProceed);
        },

        /**
         * Ui Method to handle the responsive things
         * @param {integer} width value specifies width
         */
        onBreakpointChange: function (width) {
            kony.print('on breakpoint change');
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.lblCopyright.setVisibility(true);
            this.view.lblCopyrightTab1.setVisibility(false);
            this.view.lblCopyrightTab2.setVisibility(false);
            this.view.logOutMsg.lblSuccessIcon.contentAlignment = "5";
            //this.view.main.rtxErrorMsg.contentAlignment = 2;
            var responsiveFonts = new ResponsiveFonts();
            this.view.main.rtxErrorMsgUser.contentAlignment = 2;
            if (width === 1400) this.view.main.lblWelcomeMobile.left = "0dp";  
            else if((width === 1024 && orientationHandler.isTablet)||(width === 1024 && orientationHandler.isDesktop)) this.view.main.lblWelcomeMobile.left = "33%";
            else if((width === 640 && orientationHandler.isMobile)||(width === 640 && orientationHandler.isDesktop))  this.view.main.lblWelcomeMobile.left = "20%";
            else if((width === 768 && orientationHandler.isTablet)||(width === 768 && orientationHandler.isDesktop)) this.view.main.lblWelcomeMobile.left = "30%";
            if ((width <= 1024 &&orientationHandler.isTablet)||(width <= 1024 &&orientationHandler.isDesktop)) {
                this.view.letsverify.width="100%";
                this.view.letsverify.lblWrongInfo.contentAlignment = "2";
                this.view.main.rtxErrorMsg.contentAlignment = "2";
                this.view.ResetOrEnroll.lblResetPassword.contentAlignment = "2";
                this.view.ResetOrEnroll.lblWrongCvv.contentAlignment = "2";
                this.view.resetusingOTP.lblResetPasswordMsg.contentAlignment = "2";
                this.view.resetusingOTP.lblResendOTPMsg.contentAlignment = "2";
                this.view.passwordresetsuccess.lblReserSuccessMsg.contentAlignment = "2";
                this.view.main.rtxWithUsername.contentAlignment = "2";
                this.view.main.lblVerifiedUser.contentAlignment = "2";
                this.view.ResetOrEnroll.lblWrongCvv.contentAlignment = "2";
                this.view.resetusingOTPEnterOTP.lblResendOTPMsg.contentAlignment = "2";
                this.view.resetusingOTPEnterOTP.lblResetPasswordMsg.contentAlignment = "2";
                this.view.ResetOrEnroll.lblWrongCvv.contentAlignment = "2";
                this.view.resetusingOTPEnterOTP.rtxEnterCVVCode.contentAlignment = "2";
                this.view.resetusingOTP.rtxEnterCVVCode.contentAlignment = "2";
                this.view.ResetOrEnroll.rtxEnterCVV.contentAlignment = "2";
                this.view.letsverify.btnProceed.width = "85%";
                this.view.AlterneteActionsEnterCVV.rtxCVV.skin = "sknSSPLight0273E314Px";
                this.view.newpasswordsetting.lblErrorInfo.contentAlignment = "2";
                this.view.newpasswordsetting.lblResetPasswordTitle.contentAlignment = "2"
                //this.view.logOutMsg.lblLoggedOut.contentAlignment = 2;
                //this.view.logOutMsg.lblSuccessIcon.contentAlignment = 2;
            }
            if ((width === 640 &&orientationHandler.isMobile)||(width === 640 &&orientationHandler.isDesktop)) {
                this.view.letsverify.width="100%";
                this.view.flxCloseFontIconParent.left="";
                this.view.flxCloseFontIconParent.right="7.50%";
                this.view.flxCloseFontIconParent.top="40dp";
                this.view.flxDropdown.right="13.88%";
                this.view.flxFooterMenu.width="145dp";
                this.view.btnLocateUs.left="0dp";
                this.view.flxLogin.top = "40dp";
                this.view.EnrollPromptScreen.rtxServerError.contentAlignment = "2";
                this.view.main.lblWelcomeMobile.contentAlignment = "2";
              //  this.view.imgKony.left = this.view.flxLogin.frame.x + this.view.main.flxUserName.frame.x + 2 + "dp";
                this.view.logOutMsg.lblSuccessIcon.contentAlignment = 2;
                this.view.logOutMsg.lblLoggedOut.contentAlignment = 2;
            }
            if ((width <= 640  && orientationHandler.isMobile)||(width <= 640  && orientationHandler.isDesktop)) {
                this.view.letsverify.width="100%";
                this.view.AlterneteActionsSignIn.rtxCVV.skin = "sknSSPLight0273E315Px";
                this.view.AlterneteActionsResetPassword.rtxCVV.skin = "sknSSPLight0273E315Px";
                this.view.AlterneteActionsEnterCVV.rtxCVV.skin = "sknSSPLight0273E315Px";
                this.view.AlterneteActionsEnterPIN.rtxCVV.skin = "sknSSPLight0273E315Px";
                //this.view.logOutMsg.lblSuccessIcon.contentAlignment = 2;
                responsiveFonts.setMobileFonts();
                this.view.main.flxContainer.width = "100%";
                this.view.main.flxContainer.height = "100%";
                this.view.letsverify.lbxDate.left = "4%";
                this.view.forceLayout();
            }
            if ((width === 768 &&orientationHandler.isTablet)||(width === 768 &&orientationHandler.isDesktop)) {
                this.view.letsverify.width="100%";
                this.view.flxCloseFontIconParent.left="";
                this.view.flxCloseFontIconParent.right="15%";
                this.view.flxCloseFontIconParent.top="60dp";
                this.view.flxDropdown.right="20.25%";
                this.view.flxLogin.top = "60dp";
                this.view.resetusingOTPEnterOTP.rtxEnterCVVCode.left = "86dp";
                this.view.resetusingOTPEnterOTP.rtxEnterCVVCode.top = "16dp";
                this.view.main.flxContainer.width = "100%";
                this.view.main.flxContainer.height = "600dp";
                this.view.logOutMsg.lblLoggedOut.contentAlignment = 2;
                this.view.logOutMsg.lblSuccessIcon.contentAlignment = 2;
                this.view.EnrollPromptScreen.rtxServerError.contentAlignment = "2";
            }
            if ((width === 1024 &&orientationHandler.isTablet)||(width === 1024 &&orientationHandler.isDesktop)) {
                this.view.letsverify.width="100%";
                this.view.flxSelectSignInOrResetPassword.width="100%";
                this.view.flxDropdown.right="24.5%";
                this.view.flxCloseFontIconParent.left="";
                this.view.flxCloseFontIconParent.right="20%";
                this.view.flxCloseFontIconParent.top="60dp";
                this.view.ResetOrEnroll.flxHeaderNError.top = "130dp";
                this.view.resetusingOTP.flxHeaderNError.top = "130dp";
                this.view.resetusingOTPEnterOTP.flxHeaderNError.top = "130dp";
                this.view.resetusingOTPEnterOTP.flxImgTxt.height = "60dp";
                this.view.flxLogin.top = "60dp";
                this.view.main.flxContainer.width = "100%";
                //this.view.main.flxContainer.height = "600dp";
                this.view.logOutMsg.lblLoggedOut.contentAlignment = 2;
                this.view.logOutMsg.lblSuccessIcon.contentAlignment = 2;
                this.view.EnrollPromptScreen.rtxServerError.contentAlignment = "2";
            }
            if (width > 1024) {
               // this.view.logOutMsg.lblLoggedOut.contentAlignment = 4;
                //this.view.logOutMsg.lblSuccessIcon.contentAlignment = 4;
                this.view.letsverify.btnProceed.width = "68%";
                this.view.ResetOrEnroll.btnNext.width = "68%";
                this.view.resetusingOTP.btnNext.width = "68%";
                this.view.EnrollAlert.btnBackToLogin.width = "68%";
                this.view.EnrollAlert.flxServerError.width = "68%";
                this.view.resetusingOTPEnterOTP.btnNext.width = "68%";
                this.view.ResetOrEnroll.flxHeaderNError.top = "130dp";
                this.view.resetusingOTP.flxHeaderNError.top = "130dp";
                this.view.resetusingOTPEnterOTP.flxHeaderNError.top = "130dp";
                this.view.resetusingOTPEnterOTP.flxImgTxt.height = "60dp";
                this.view.ResetOrEnroll.lblWrongCvv.contentAlignment = "1";
                this.view.ResetOrEnroll.lblWrongCvv.left = "75dp";
                this.view.main.rtxErrorMsg.contentAlignment = "1";
                this.view.main.rtxErrorMsgUser.contentAlignment = "1";
                this.view.resetusingOTP.lblResetPasswordMsg.left = "86px";
                this.view.resetusingOTPEnterOTP.lblResendOTPMsg.contentAlignment = "1";
                this.view.resetusingOTPEnterOTP.lblResendOTPMsg.left = "88px";
                this.view.resetusingOTPEnterOTP.lblResetPasswordMsg.left = "88px";
                this.view.resetusingOTPEnterOTP.lblWrongOTP.left = "88px";
                this.view.newpasswordsetting.lblErrorInfo.contentAlignment = "1";
                this.view.logOutMsg.lblSuccessIcon.contentAlignment = 1;
            }
            if(width !== 1400)
            this.view.onTouchEnd = function(){}
            var scope = this;
            var views = Object.keys(this.responsiveViews);
            views.forEach(function (e) {
                scope.view[e].isVisible = scope.responsiveViews[e];
            });
          	if(width >=768 && orientationHandler.isTablet){
              this.view.flxLogin.top = "60dp";
              this.view.letsverify.flxDateInput.width = "85%";
              this.view.letsverify.btnProceed.width = "85%";
            }
            //this.allFieldsCheck();
        },

      setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024 || width==768){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();   
              } 
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
      },
        /**
         * Navigates to new user on boarding
         */
        navigateToNewUserOnBoarding: function () {
            this.loadAuthModule().presentationController.navigateToNewUserOnBoarding();
        },

        /**
         * This function enables the Proceed button only if all the fields(SSN,DOB,lastName) are correct in Forgot form /verify form.
         */
        allFieldsCheck: function () {
            var SSN = this.view.letsverify.tbxSSN.text.trim();
            var lastName = this.view.letsverify.tbxLastName.text.trim();
            var text = this.view.letsverify.DateInput.getText();
            var date = "";
            if(text.length === 10) {
                date = this.view.letsverify.DateInput.getDateObject();
            }
            if (lastName != "" && SSN != "" && date instanceof Date && !isNaN(date.getDay())) {
                FormControllerUtility.enableButton(this.view.letsverify.btnProceed);
            } else {
                FormControllerUtility.disableButton(this.view.letsverify.btnProceed);
            }
        },

        /**
        * This function used to check whether the SSN entered is correct or not ..If it is wrong the error message is shown
        */
        ssnCheck: function () {
            /*
            var input = this.view.letsverify.tbxSSN.text.trim();
            //var SSNLENGTH = 9;
            //if ((input.length < SSNLENGTH && isNaN(input)) || (input.length >= SSNLENGTH && !this.validationUtilManager.isValidSSNNumber(input))) {
            if (!this.validationUtilManager.isValidSSNNumber(input)) {
                this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.incorrectSSN");
                this.view.letsverify.lblWrongInfo.isVisible = true;
            } else {
                this.view.letsverify.lblWrongInfo.isVisible = false;
            }
            this.view.letsverify.forceLayout();
            */
        },

        /**
         * This function calls the function which fetches the user based on the details entered
         */
        verifyUserDetails: function () {
            var SSN = this.view.letsverify.tbxSSN.text;
            var LastName = this.view.letsverify.tbxLastName.text;
            var dateStr = applicationManager.getFormatUtilManager().getFormatedDateString(this.view.letsverify.DateInput.getDateObject(), applicationManager.getFormatUtilManager().getBackendDateFormat());
            this.loadAuthModule().presentationController.verifyUserName({
                "ssn": SSN,
                "lastname": LastName,
                "dob": dateStr
            });
        },

        /**
        * This function is showing the flxWelcomeBack once all the details are filled and proceed button is clicked
        * @param {object} users is success response for fetch user name
        */
        welcomeVerifiedUser: function (users) {
            this.emptyUserDetails();
            this.view.flxVerification.isVisible = false;
            var usersList = [];
            users.forEach(function (data) {
                var user = [];
                user.push(data.UserName);
                user.push(data.UserName);
                usersList.push(user);
            });
            this.view.lstBoxSelectUsername.masterData = usersList;
            this.view.flxSelectUsername.setVisibility(true);
            this.view.SignInAs.rtxCVV.text = kony.i18n.getLocalizedString("i18n.login.SignInAs") + " " + users[0].UserName;
            this.view.ResetMyPassword.rtxCVV.text = kony.i18n.getLocalizedString("i18n.login.ResetMyPassword");
            this.view.imgCloseSelectUsername.onTouchEnd = this.emptyLoginData.bind(this);
            this.view.lstBoxSelectUsername.onSelection = function() {
                this.userName = this.view.lstBoxSelectUsername.selectedKey;
                this.view.SignInAs.rtxCVV.text = kony.i18n.getLocalizedString("i18n.login.SignInAs") + " " + this.userName;
            }.bind(this);
            this.view.SignInAs.onTouchEnd = function () {
                this.userName = this.view.lstBoxSelectUsername.selectedKey;
                this.loginWithVerifiedUserName();
            }.bind(this);
            this.view.ResetMyPassword.onTouchEnd = function () {
              var authManager = applicationManager.getAuthManager();
               authManager.setUserName(this.view.lstBoxSelectUsername.selectedKey);
              this.userName = this.view.lstBoxSelectUsername.selectedKey;
                var selectedUser = users.filter(function (data) {
                    return data.UserName === this.userName;
                }.bind(this))[0];
                this.securityKey = selectedUser.securityKey;
                this.goToPasswordResetOptionsPage();
            }.bind(this);
            this.view.flxWelcomeBack.parent.forceLayout();
        },

        /**
        * This function shows the enroll page when clicked on enroll button from the login page.
        * @param {object} userDetails - user details object
        */
        showEnrollFlex: function (userDetails) {
            var self = this;
            self.emptyUserDetails();
            self.view.EnrollPromptScreen.rtxServerError.text = kony.i18n.getLocalizedString("i18n.common.EnrollAlert");
            self.view.EnrollPromptScreen.rtxServerError.setVisibility(true);
            self.view.flxVerification.setVisibility(false);
            self.view.flxEnroll.setVisibility(true);
          	var breakpoint = kony.application.getCurrentBreakpoint();
            if(breakpoint !== 1400){
                this.view.flxDropdown.setVisibility(false);
              	if(breakpoint !== 1024)
              		self.view.EnrollPromptScreen.rtxServerError.skin = ViewConstants.SKINS.RTEXT_ERROR_MOBILE;
              	else
                  	self.view.EnrollPromptScreen.rtxServerError.skin = ViewConstants.SKINS.RTEXT_ERROR_DESKTOP;
            }
          	else {
              	self.view.EnrollPromptScreen.rtxServerError.skin = ViewConstants.SKINS.RTEXT_ERROR_DESKTOP;
            }
            self.view.flxEnroll.parent.forceLayout();
            self.view.flxCloseEnroll.onClick = self.emptyLoginData.bind(self);
            self.view.EnrollPromptScreen.btnBackToLogin.onClick = function() {
                var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
                enrollModule.presentationController.showEnrollPage();
            };
            self.view.EnrollPromptScreen.btnEnroll.onClick = function () {
                self.view.EnrollPromptScreen.rtxServerError.setVisibility(true);
                self.loadAuthModule().presentationController.navigateToEnroll(userDetails);
            };
            this.initializeResponsiveViews();
        },

        /**
         * This function shows the error message if fetching user name fails.
         */
        showFetchUserNameErrorUI: function () {
            this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.wrongInfo");
            this.view.letsverify.lblWrongInfo.isVisible = true;
            this.view.letsverify.parent.forceLayout();
        },

        /**
         * This function is used to update voew for logging with the fetched username where the username is prepopulated after verifying the user
         */
        loginWithVerifiedUserName: function () {
             this.recoveredUsernameUIChanges();
            this.view.flxSelectUsername.setVisibility(false);
             this.view.main.flxUserName.setVisibility(false);
            this.view.main.lblUsername.setVisibility(false);
            this.view.main.flxLoginUser.lblVerifiedUser.text = this.userName;
            this.view.main.flxUserDropdown.setVisibility(false)
            this.view.main.tbxUserName.text = this.userName;
            this.view.main.tbxPassword.text = "";
            this.view.main.tbxPassword.secureTextEntry = true;
            FormControllerUtility.disableButton(this.view.main.btnLogin);
            var buttonText = this.view.AlterneteActionsSignIn.rtxCVV.text;
            var position = buttonText.indexOf(this.userName, 0);
            buttonText = buttonText.substring(0, position);
            this.view.AlterneteActionsSignIn.rtxCVV.text = buttonText.trim();
            this.view.AlterneteActionsSignIn.rtxCVV.toolTip = buttonText.trim();
            this.view.main.btnOpenNewAccount.isVisible = false;
            this.view.flxLogin.isVisible = true;
            this.view.main.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * This UI function is for recovered Username UI Changes after user verified
         */
        recoveredUsernameUIChanges: function () {
            this.view.main.lblAppName.isVisible = false;
            this.view.main.lblWelcome.isVisible = false;
            this.view.main.rtxErrorMsg.isVisible = false;
            this.view.main.btnOnlineAccessEnroll.isVisible = false;
            this.view.main.flxLoginUser.isVisible = true;
            //this.view.main.imgUserOutline.isVisible = false;
            //this.view.main.imgUserName.src = 'login_username_icon.png';
            //this.view.main.imgUser.src = 'user_image.png';
            //this.view.flxLogin.centerX = "50%";
            this.view.main.rtxWithUsername.skin = "sknSSPLight42424218Px";
            this.view.main.rtxWithUsername.contentAlignment = "4";
            this.view.main.lblVerifiedUser.contentAlignment = "4";
            this.view.flxLogin.left = "0dp";
        },

        /**
         * This function calls the function to retrieve the cards for the user.
         * Fetch the Cards details by username and navigate to corresponding view.
         */
        goToPasswordResetOptionsPage: function () {
          FormControllerUtility.showProgressBar(this.view);
          var authManager = applicationManager.getAuthManager();
            var params = {
              "UserName" : authManager.getUserName(),
               "MFAAttributes": {         
                 "serviceKey": authManager.getServicekey()
               } 
            };
            this.loadAuthModule().presentationController.requestResetPasswordOTP(params);
            this.initializeResponsiveViews();
        },

        /**
         * This function shows Send OTP Page for a Business Banking User
         */
        showSendOTPBB: function () {
            this.view.flxSelectUsername.setVisibility(false);
            this.view.flxSendOTP.setVisibility(true);
            this.view.resetusingOTP.orline.setVisibility(false);
            this.view.resetusingOTP.btnUseCVV.setVisibility(false);
            this.view.resetusingOTP.btnNext.onClick = this.loadAuthModule().presentationController.requestOTPBB.bind(this);
            this.view.flxCloseSendOTP.onClick = this.emptyLoginData.bind(this);
            this.view.forceLayout();
        },

        /**
         * This function shows the Reset OTP Page where you can enter the recieved otp
         */
        showResetUsingOTPBB: function () {
            this.view.flxSendOTP.setVisibility(false);
            this.view.flxResetUsingOTP.setVisibility(true);
            this.view.resetusingOTPEnterOTP.flxCVV.setVisibility(true);
            this.view.resetusingOTPEnterOTP.orline.setVisibility(false);
            this.view.resetusingOTPEnterOTP.btnUseCVV.setVisibility(false);
            this.view.resetusingOTPEnterOTP.btnNext.onClick = this.verifyOTPBB.bind(this);
            this.view.forceLayout();
        },

        /**
         * Method to Validate OTP
         */
        verifyOTPBB: function () {
            this.loadAuthModule().presentationController.verifyOTPBB(this.view.resetusingOTPEnterOTP.tbxCVV.text);
        },

        /**
         * This function shows the reset password page for a Business Banking User
         * @param {string[]} rules - password rules form service
         */
        showResetPasswordPageBB: function (rules) {
            this.view.flxResetUsingOTP.setVisibility(false);
            this.view.newpasswordsetting.rtxRulesPassword.text = rules.content;
            this.view.flxResetPassword.setVisibility(true);
            this.view.newpasswordsetting.btnNext.onClick = this.resetPasswordBB.bind(this);
            this.view.forceLayout();
        },

        /**
         * This function checks whether to show the password Confirmation Screen - Reset password button action handler.
         */
        resetPasswordBB: function () {
            var userDetails = {
                "userName": this.userName,
                "securityKey": this.securityKey,
                "password": this.view.newpasswordsetting.tbxNewPassword.text
            };
            this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
            this.loadAuthModule().presentationController.resetPassword(userDetails);
        },

        /**
         * This function show the reset password options based in cards Data
         * @param {object[]} cards -  fetch cards success response - contians cards data
         * @param {string} cards.cardHolderName -  card holder name
         * @param {string} cards.cardNumber -  card number
         * @param {string} cards.cardType -  card type
         */
        showResetPasswordUI: function (cards) {
            this.view.flxSelectUsername.setVisibility(false);
            this.view.flxResetPasswordOptions.setVisibility(true);
            if (cards.length !== 0) {
                this.showCVVCards(cards);
                this.showCVVOption();
            } else {
                this.hideCVVOption();
            }
        },

        /**
         * This function prepares the list of cards available for the user and stores in the form of key and value where the key is the actual card number and value is the masked card number
         * @param {Cards[]} presentCards -  fetch cards success response - contians cards data
         * @param {string} presentCards.cardHolderName -  card holder name
         * @param {string} presentCards.cardNumber -  card number
         * @param {string} presentCards.cardType -  card type
         */
        showCVVCards: function (presentCards) {
            var scopeObj = this;
            function maskCard(card) {
                return scopeObj.dataProcessUtilityManager.maskCardNumber(card.cardNumber);
            }
            this.view.ResetOrEnroll.lstbxCards.masterData = FormControllerUtility.getListBoxDataFromObjects(presentCards, "cardNumber", maskCard);
        },

        /**
         * This function is for hiding the CVV option if there are no cards available for the user
         */
        hideCVVOption: function () {
            this.view.flxWelcomeBack.isVisible = false;
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.AlterneteActionsEnterCVV.isVisible = false;
            this.view.OrLineForCVVandPIN.isVisible = false;
            this.view.resetusingOTP.btnUseCVV.isVisible = false;
            this.view.resetusingOTP.orline.isVisible = false;
            this.view.resetusingOTPEnterOTP.orline.isVisible = false;
            this.view.resetusingOTPEnterOTP.btnUseCVV.isVisible = false;
            this.view.flxResetPasswordOptions.parent.forceLayout();
        },

        /**
         * This function is for showing the CVV option if there are cards available for the user
         */
        showCVVOption: function () {
            this.view.flxWelcomeBack.isVisible = false;
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.resetusingOTP.btnUseCVV.isVisible = true;
            this.view.resetusingOTP.orline.isVisible = true;
            this.view.AlterneteActionsEnterCVV.isVisible = true;
            this.view.OrLineForCVVandPIN.isVisible = true;
            var buttonText = this.view.AlterneteActionsSignIn.rtxCVV.text;
            var position = buttonText.indexOf(this.userName, 0);
            buttonText = buttonText.substring(0, position);
            this.view.AlterneteActionsSignIn.rtxCVV.text = buttonText.trim();
            this.view.AlterneteActionsSignIn.rtxCVV.toolTip = buttonText.trim();
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.resetusingOTPEnterOTP.orline.isVisible = true;
            this.view.resetusingOTPEnterOTP.btnUseCVV.isVisible = true;
            this.view.flxResetPasswordOptions.parent.forceLayout();
        },

        /**
         * This function shows the CVV flex once the user clicks on "Reset using the CVV" Option    
         */
        showEnterCVVPage: function () {
            this.view.flxResetPasswordOptions.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = true;
            this.view.flxResetUsingCVV.parent.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * This function shows the masked CVV on click of eye icon
         */
        showCVV: function () {
            if (this.view.ResetOrEnroll.tbxCVV.secureTextEntry === true) {
                this.view.ResetOrEnroll.tbxCVV.secureTextEntry = false;
            } else {
                this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true;
            }
        },

        /**
         * This function shows the error message if the user enters incorrect CVV
         */
        reEnterCVV: function () {
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
            this.view.ResetOrEnroll.parent.forceLayout();
        },
        /**
        * showRulesPassword :This function shows the password rules on click of password textbox
       */
        showRulesPassword: function () {
            this.view.newpasswordsetting.flxMain.height = "610dp";
            // this.view.newpasswordsetting.flxRulesUsername.setVisibility(false);
            this.view.newpasswordsetting.flxRulesPassword.setVisibility(true);
            this.view.forceLayout();
        },

        /**
         * This function checks whether the entered CVV  is valid or not and enalbe next button if valid CVV number enter
         */
        cvvCheck: function () {
            var input = this.view.ResetOrEnroll.tbxCVV.text.trim();
            var CVVLENGTH = 3;
            if (input.length < CVVLENGTH || !this.validationUtilManager.isValidCVV(input)) {
                if (isNaN(input) || input.length >= CVVLENGTH) {
                    this.view.ResetOrEnroll.lblWrongCvv.text = kony.i18n.getLocalizedString("i18n.login.IncorrectCVV");
                    this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
                }
                FormControllerUtility.disableButton(this.view.ResetOrEnroll.btnNext);
            }
            else {
                this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
                FormControllerUtility.enableButton(this.view.ResetOrEnroll.btnNext);
            }
            this.view.ResetOrEnroll.parent.forceLayout();
        },

        /**
        * This function is for selecting the OTP option from the CVV screen
        */
        useOTPForReset: function () {
            this.view.flxResetUsingCVV.isVisible = false;
            this.view.flxSendOTP.isVisible = true;
            this.view.ResetOrEnroll.tbxCVV.text = "";
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
            FormControllerUtility.disableButton(this.view.ResetOrEnroll.btnNext);
            this.view.ResetOrEnroll.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * This function is for selecting the CVV option from the OTP screen
          */
        useCVVForReset: function () {
            this.view.flxResetUsingOTP.isVisible = false;
            this.view.flxSendOTP.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = true;
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.view.ResetOrEnroll.lstbxCards.selectedKey = this.view.ResetOrEnroll.lstbxCards.masterData[0][0];//reset drop down.
            FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true;
            this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
            this.view.ResetOrEnroll.forceLayout();
            this.initializeResponsiveViews();
        },

        /**
         * This function checks whether the  CVV  is correct  or not for the selected card
         */
        isCVVCorrect: function () {
            this.loadAuthModule().presentationController.validateCVV({
                "cvv": this.view.ResetOrEnroll.tbxCVV.text,
                "cardNumber": this.view.ResetOrEnroll.flxCards.lstbxCards.selectedKey
            });
        },

        /**
         * This function shows the error message if the user enters the incorrect CVV
         */
        showErrorForCVV: function () {
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
        },


        /**
         * This function shows Reset Password Flex form CVV with password rules
         * @param {string[]} rules - password rules form service
         */
        showResetPasswordPage: function (rules) {
            this.view.newpasswordsetting.rtxRulesPassword.text = rules.content;
            this.view.flxPhoneAndEmail.setVisibility(false);
            this.view.OTPModule.flxEnterOTP.setVisibility(false);
            this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
            this.view.OTPModule.tbxCVV.text = "";
            this.view.flxResetPassword.isVisible = true;
            this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
            //this.view.ResetOrEnroll.isVisible = true;
            this.view.flxResetPassword.parent.forceLayout();
        },

        /**
         * The function is called when the user enters the wrong password and gives an error that password doesnot meet the required criteria 
         */
        passwordEditing: function () {
            this.showRulesPassword();
            if (this.view.newpasswordsetting.lblErrorInfo.isVisible) {
                this.reEnterNewPassword();
            }
        },

        /**
         * This function shows the error message if the entered password is wrong       
         */
        reEnterNewPassword: function () {
            this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
            this.view.newpasswordsetting.flxNewPassword.top = "8.8%";
        },

        /**
         * The function is used to check whether the entered password is valid or not       
         */
        newPwdKeyUp: function () {
            this.validateNewPassword(this.view.newpasswordsetting.tbxNewPassword.text);
        },

        /**
         * This function validates whether the entered new password is correct or not and enable / disable reset password button
         * @param {string}  enteredPassword -  password entered by the user
         */
        validateNewPassword: function (enteredPassword) {
            if (this.validationUtilManager.isPasswordValidForPolicy(enteredPassword)) {
                this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
                this.view.newpasswordsetting.imgValidPassword.isVisible = true;
                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    this.view.newpasswordsetting.imgPasswordMatched.isVisible = true;
                    FormControllerUtility.enableButton(this.view.newpasswordsetting.btnNext);
                } else {
                    this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
                    FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
                }
            } else {
                this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
                this.view.newpasswordsetting.lblErrorInfo.text = kony.i18n.getLocalizedString("i18n.login.invalidPassword");
                this.view.newpasswordsetting.imgValidPassword.isVisible = false;
                this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
                FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            }
            this.view.newpasswordsetting.parent.forceLayout();
        },

        /**
         * This function checks whether the entered password and reenter password are same or not
         * @returns {boolean} true if they match,false if they do not match  
         */
        isPasswordValidAndMatchedWithReEnteredValue: function () {
            if (this.view.newpasswordsetting.tbxNewPassword.text && this.view.newpasswordsetting.tbxMatchPassword.text) {
                if (this.view.newpasswordsetting.tbxNewPassword.text === this.view.newpasswordsetting.tbxMatchPassword.text) {
                    return true;
                }
            }
            return false;
        },

        /**
         * The function is used to confirm whether the entered password and renter password are same or not
         */
        matchPwdKeyUp: function () {
            this.validateConfirmPassword(this.view.newpasswordsetting.tbxMatchPassword.text);
        },

        /**
         * This function validates whether the  re-entered password is correct or not and enable / disable reset password button
         * @param {string} confirmedPwd - confirmed password
         */
        validateConfirmPassword: function (confirmedPwd) {
            if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
                this.view.newpasswordsetting.imgPasswordMatched.isVisible = true;
                this.validateNewPassword(confirmedPwd);
            } else {
                this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
                this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
                FormControllerUtility.disableButton(this.view.newpasswordsetting.btnNext);
            }
            this.view.newpasswordsetting.forceLayout();
        },

        /**
         * This function checks whether to show the password Confirmation Screen - Reset password button action handler.
         */
        showResetConfirmationPage: function () {
            var userDetails = {
                "userName": this.userName,
                "securityKey": this.securityKey,
                "password": this.view.newpasswordsetting.tbxNewPassword.text
            };
            this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
            this.loadAuthModule().presentationController.resetPassword(userDetails);
            this.initializeResponsiveViews();
        },

        /**
        * This function shows the reset password Confirmation Screen once the password is reset successfully i.e.success from the service
        */
        showResetPasswordConfirmation: function () {
            this.showResetConfirmationScreen();
        },
        /**
         * This function update view for Confirmation Screen        
         */
        showResetConfirmationScreen: function () {
            this.view.flxResetPassword.isVisible = false;
            this.view.flxResetSuccessful.isVisible = true;
            this.view.flxResetPassword.parent.forceLayout();
        },

        /**
        * This function is to update view for Reset password using OTP- on clicking of the "Reset using OTP" option
        */
        goToResetUsingOTP: function () {
            this.view.flxResetPasswordOptions.isVisible = false;
            this.view.flxSendOTP.isVisible = true;
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true;
            this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = false;
            this.view.flxResetPasswordOptions.parent.forceLayout();
            this.initializeResponsiveViews();
            this.view.resetusingOTP.btnResendOTP.setVisibility(false);
        },

        /**
         * This function is for calling the other function in the presentation Controller for requesting OTP from the server
         */
        requestOTPValue: function () {
            this.loadAuthModule().presentationController.requestOTP();
        },

        /**
         * This function shows the OTP flex once the user clicks on "Reset using the Secure Access Code" Option
         */
        showEnterOTPPage: function () {
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            if (this.view.flxSendOTP.isVisible && !this.view.flxResetUsingOTP.isVisible) {
                this.view.flxSendOTP.isVisible = false;
                this.view.resetusingOTPEnterOTP.flxCVV.isVisible = true;
                this.view.resetusingOTPEnterOTP.btnResendOTP.isVisible = true;
                this.view.resetusingOTPEnterOTP.btnResendOTP.onClick = this.resendOTPValue;
                this.view.resetusingOTPEnterOTP.btnNext.top = "3.52%";
                this.view.resetusingOTPEnterOTP.imgCVVOrOTP.top = "3.52%";
                this.view.resetusingOTPEnterOTP.btnUseCVV.top = "2.61%";
                this.view.resetusingOTPEnterOTP.btnNext.text = kony.i18n.getLocalizedString("i18n.common.next");
                this.view.flxResetUsingOTP.isVisible = true;
            }
            FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            this.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(false);
            this.view.resetusingOTPEnterOTP.btnResendOTP.skin = ViewConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
            this.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = ViewConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
            this.view.flxResetUsingOTP.parent.forceLayout();
            this.enalbeResendButton();
        },

        /**
         * Enable Re-send button after one second.
         */
        enalbeResendButton: function () {
            var scopeObj = this;
            /**
             * Funtion to enable resend button.
             */
            var enableResendBtn = function () {
                scopeObj.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(true);
                scopeObj.view.resetusingOTPEnterOTP.btnResendOTP.skin = ViewConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
                scopeObj.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = ViewConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
            };
            kony.timer.schedule("otpTimer", enableResendBtn, 1, false); //As per the requirement need timer here. Enable OTP button after 1 sec.          
        },

        /**
         * The function is called when the user enters the wrong OTP - not requied method but placed in code bcx action was binded in snippet.
         */
        reTypeOTP: function () {
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
        },

        /**
         * This function checks the OTP entered is correct or not and shows the error message if it is incorrect and enable/ disalbe next button.
         */
        otpCheck: function () {
            var input = this.view.resetusingOTPEnterOTP.tbxCVV.text.trim();
            var OTP_LENGTH = 6;
            if (input.length < OTP_LENGTH || !this.validationUtilManager.isValidOTP(input)) {
                if (isNaN(input) || input.length >= OTP_LENGTH) {
                    this.view.resetusingOTPEnterOTP.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.login.incorrectOTP");
                    this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
                }
                FormControllerUtility.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            }
            else {
                this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
                FormControllerUtility.enableButton(this.view.resetusingOTPEnterOTP.btnNext);
            }
            this.view.resetusingOTPEnterOTP.parent.forceLayout();
        },

        /**
        * This function shows the masked OTP on click of eye icon
        */
        showOTP: function () {
            if (this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry === true) {
                this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = false;
            } else {
                this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            }
        },

        /**
        * This function is called when the user clicks on resend OTP button
        */
        resendOTPValue: function () {
            this.requestOTPValue();
        },

        /**
         * This function calls the service which validates whether entered OTP is correct or not 
         */
        isOTPCorrect: function () {
            this.loadAuthModule().presentationController.verifyOTP(this.view.resetusingOTPEnterOTP.tbxCVV.text);
        },

        /**
          *This function shows error message if user enters the wrong OTP
          */
        showErrorForOTP: function () {
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
            this.view.resetusingOTPEnterOTP.forceLayout();
        },
        responsiveViews: {},
        /**
         * Method to initialize Response views
         */
        initializeResponsiveViews: function () {
            this.responsiveViews["flxLogin"] = this.view.flxLogin.isVisible;
            this.responsiveViews["flxVerification"] = this.view.flxVerification.isVisible;
            this.responsiveViews["flxWelcomeBack"] = this.view.flxWelcomeBack.isVisible;
            this.responsiveViews["flxResetPasswordOptions"] = this.view.flxResetPasswordOptions.isVisible;
            this.responsiveViews["flxSendOTP"] = this.view.flxSendOTP.isVisible;
            this.responsiveViews["flxResetUsingOTP"] = this.view.flxResetUsingOTP.isVisible;
            this.responsiveViews["flxResetUsingCVV"] = this.view.flxResetUsingCVV.isVisible;
            this.responsiveViews["flxResetPassword"] = this.view.flxResetPassword.isVisible;
            this.responsiveViews["flxLoginMFA"] = this.view.flxLoginMFA.isVisible;
            this.responsiveViews["flxResetSuccessful"] = this.view.flxResetSuccessful.isVisible;
            this.responsiveViews["flxBlocked"] = this.view.flxBlocked.isVisible;
            this.responsiveViews["flxEnrollOrServerError"] = this.view.flxEnrollOrServerError.isVisible;
            this.responsiveViews["flxEnroll"] = this.view.flxEnroll.isVisible;
            this.responsiveViews["flxLogoutMsg"] = this.view.flxLogoutMsg.isVisible;
            this.responsiveViews["flxFeedbackTakeSurvey"] = this.view.flxFeedbackTakeSurvey.isVisible;
            this.responsiveViews["imgKony"] = this.view.imgKony.isVisible;
           this.responsiveViews["flxPhoneAndEmail"] = this.view.flxPhoneAndEmail.isVisible;
        },
        /**
         * This function navigates to the customer Feedback form.
         */
        btnYesTakeSurvey: function () {
            this.view.flxFeedbackTakeSurvey.setVisibility(false);
            this.loadAuthModule().presentationController.navigateToFeedbackPage();
        },
        /**
         * This function sets the visibility of the Survey Flex to false  
         */
        btnNoTakeSurvey: function () {
            this.view.flxFeedbackTakeSurvey.setVisibility(false);
        },

        /**
         * Set up native app link for mobile and tablet
         */
        setupNativeAppLink: function () {
            var configurationManager = applicationManager.getConfigurationManager();
            var deviceConfig = this.detectDevice();
            this.view.flxPlayStore.setVisibility(false);
            if (deviceConfig !== DEVICE_TYPES.DESKTOP) {
                this.view.imgAppstore.src = deviceConfig.image;
                this.view.flxAppStore.onClick = function () {
                    // Picking Native App Link from Configuration
                    kony.application.openURL(configurationManager[deviceConfig.linkConfigKey])
                }
            }
            this.view.forceLayout();

        },

        /**
         * detects the device type
         * @returns {Number} Type of device
         */
        detectDevice: function () {
            var userAgent = kony.os.deviceInfo().userAgent
            if (/iphone/i.test(userAgent)) {
                return DEVICE_TYPES.IPHONE;
            }
            else if (/ipad/i.test(userAgent)) {
                return DEVICE_TYPES.IPAD;
            }
            else if (/android/i.test(userAgent) && /mobile/i.test(userAgent)) {
                return DEVICE_TYPES.ANDROID_PHONE;
            }
            else if (/android/i.test(userAgent)) {
                return DEVICE_TYPES.ANDROID_TABLET;
            }
            return DEVICE_TYPES.DESKTOP;

        },
        
         /**
         * Method to close the feedback pop up.
         */
        onFeedbackCrossClick: function(){
            this.view.flxFeedbackTakeSurvey.setVisibility(false);
            this.initializeResponsiveViews();
        },
      
        showScreenToEnterSecureCode:function(response){
        var authManager = applicationManager.getAuthManager();
        var communicationType = authManager. getCommunicationType();
        if(communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_ALL){
          this.showPhoneEmailScreen(response);
        } else if(communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_PRIMARY){
          this.showPrimaryEmailScreen(response);
        }else if(communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_NO_VALUE){
          this.showDefaultPhoneEmailScreen(response);
        }
      },
      
      showSecureAccessCodeScreenAfterResend:function(response){
        var authManager = applicationManager.getAuthManager();
        var communicationType = authManager.getCommunicationType();
        if (communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_ALL) {
            this.showScreentoEnterOTP(response);
        } else if (communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_PRIMARY) {
            this.showPrimaryEmailScreen(response);
        } else if (communicationType == OLBConstants.MFA_FLOW_TYPES.DISPLAY_NO_VALUE) {
            this.showDefaultPhoneEmailScreen(response);
        }
    },

      showPhoneEmailScreen:function(response){
        var scopeObj = this;
        this.view.flxSelectUsername.setVisibility(false);
        FormControllerUtility.showProgressBar(this.view);
        this.bindUIForOTPMFAScreen(response.MFAAttributes.customerCommunication);
        this.view.OTPModule.btnProceed.onClick = function() {
          this.view.flxLoading.height  = "100%";
          FormControllerUtility.showProgressBar(scopeObj.view);
          var selectedData = {
            "phone" :scopeObj.view.OTPModule.lbxPhone.selectedKeyValue[0],
            "email" : scopeObj.view.OTPModule.lbxEmail.selectedKeyValue[0],
          };
         scopeObj.customerPhone  = selectedData.phone;
       	 scopeObj.customerEmail  = selectedData.email;
          this.requestOTP(selectedData); 
        }.bind(this);
        this.view.forceLayout();
      },
      
      bindUIForOTPMFAScreen : function(customerCommunicationInfo){
        this.view.flxPhoneAndEmail.setVisibility(true);
        this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(false);
        this.view.OTPModule.lblHeaderOTP.setVisibility(false);
        this.view.OTPModule.flxEnterOTP.setVisibility(true);
        FormControllerUtility.enableButton(this.view.OTPModule.btnProceed);
        this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTP");
        this.view.OTPModule.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");  
       if (customerCommunicationInfo.phone && customerCommunicationInfo.email) {
                this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTP");
                this.view.OTPModule.lbxPhone.masterData = this.setDataForPhoneListBox(customerCommunicationInfo.phone);
                this.view.OTPModule.lbxEmail.masterData = this.setDataForEmailListBox(customerCommunicationInfo.email);
                this.view.OTPModule.lblRegisteredPhone.setVisibility(true);
                this.view.OTPModule.lbxPhone.setVisibility(true);
                this.view.OTPModule.lblRegisteredEmail.setVisibility(true);
                this.view.OTPModule.lbxEmail.setVisibility(true);
                this.view.forceLayout();
            }
           else{
            if (customerCommunicationInfo.phone || customerCommunicationInfo.email){
                if (customerCommunicationInfo.phone) {
                    this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTPPhone");
                    this.view.OTPModule.lbxPhone.masterData = this.setDataForPhoneListBox(customerCommunicationInfo.phone);
                    this.view.OTPModule.lblRegisteredPhone.setVisibility(true);
                    this.view.OTPModule.lbxPhone.setVisibility(true);
                    this.view.OTPModule.lblRegisteredEmail.setVisibility(false);
                    this.view.OTPModule.lbxEmail.setVisibility(false);
                    this.view.forceLayout();
                } else if (customerCommunicationInfo.email) {
                    this.view.OTPModule.lblResendMessage.text = kony.i18n.getLocalizedString("i18n.MFA.headerMessageForOTPEmail");
                    this.view.OTPModule.lbxEmail.masterData = this.setDataForEmailListBox(customerCommunicationInfo.email);
                    this.view.OTPModule.lblRegisteredPhone.setVisibility(false);
                    this.view.OTPModule.lbxPhone.setVisibility(false);
                    this.view.OTPModule.lblRegisteredEmail.setVisibility(true);
                    this.view.OTPModule.lbxEmail.setVisibility(true);
                    this.view.forceLayout();
                }
            }
            }
        FormControllerUtility.hideProgressBar(this.view);
        this.view.forceLayout();
      },
      
      setDataForPhoneListBox:function(phoneObj){
      var phoneNumbers = phoneObj.map(function (dataItem) {
        var phoneNumber = [];
        phoneNumber.push(dataItem.unmasked);
        phoneNumber.push(dataItem.masked);
        return phoneNumber;
      });
      return phoneNumbers;
    },

    setDataForEmailListBox:function(emailObj){
      var emailsIds = emailObj.map(function (dataItem) {
        var email = [];
        email.push(dataItem.unmasked);
        email.push(dataItem.masked);
        return email;
      });
      return emailsIds;
    },
      
     requestOTP: function(selectedData) {
        var authManager = applicationManager.getAuthManager();
       var params = {
             "UserName" : authManager.getUserName(),
               "MFAAttributes": {         
                 "serviceKey": authManager.getServicekey(),
                 "OTP" : selectedData
               } 
       };
     this.loadAuthModule().presentationController.requestOTPUsingPhoneEmail(params);
    },
      
      resendOTP:function(params){
        var authManager = applicationManager.getAuthManager();
         var params = {
               "UserName" : authManager.getUserName(),
                 "MFAAttributes": {         
                   "serviceKey": authManager.getServicekey(),
                   "OTP" : params
                 } 
         };
       this.loadAuthModule().presentationController.resendOTPForResetPassword(params);
      },
      
      showPrimaryEmailScreen:function(response){
        this.bindUIForPrimaryScreen(response);
      },
  
      bindUIForResendButton:function(response){
      var scopeObj = this;
      this.view.OTPModule.tbxCVV.text = "";
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.btnResendOTP.onClick = function() {
        FormControllerUtility.showProgressBar(scopeObj.view);
        if(response.MFAAttributes.customerCommunication){
          var params = {
            "phone": response.MFAAttributes.customerCommunication.phone[0].unmasked,
            "email": response.MFAAttributes.customerCommunication.email[0].unmasked,
            "securityKey": response.MFAAttributes.securityKey,
          };
        }else{
          var params = {
            "phone":  scopeObj.customerPhone,
            "email":  scopeObj.customerEmail,
            "securityKey": response.MFAAttributes.securityKey,
          };
        }
        scopeObj.resendOTP(params);
      };
    },
      
      bindUIForPrimaryScreen : function(response){
      if (response.MFAAttributes.remainingResendAttempts <= 0) {
        this.view.OTPModule.btnResendOTP.setVisibility(false);
      } else {
        this.bindUIForResendButton(response);
        this.view.OTPModule.btnResendOTP.setVisibility(true);
      }
      if(response.MFAAttributes.isOTPExpired === "true"){
        this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");
        this.view.OTPModule.lblWrongOTP.setVisibility(true);
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        FormControllerUtility.hideProgressBar(this.view);
      }else{
        this.view.OTPModule.lblWrongOTP.setVisibility(false);
      } 
      this.view.flxPhoneAndEmail.setVisibility(true);
      this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
      this.view.OTPModule.flxEnterOTP.setVisibility(false);
     
      FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      this.view.OTPModule.tbxCVV.text = "";
      this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
      var phone = response.MFAAttributes.customerCommunication.phone[0].masked;
      var email = response.MFAAttributes.customerCommunication.email[0].masked;
      this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.EnterSACMobile") + phone + " & " + email;
      this.view.OTPModule.tbxCVV.onKeyUp = function() {
        this.validatetoEnableContinueButton();
      }.bind(this);
       this.view.OTPModule.tbxCVV.onDone = function() {
        FormControllerUtility.showProgressBar(this.view);
        var params = {
          "securityKey": response.MFAAttributes.securityKey,
          "otp": this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(params);
      }.bind(this);
      this.view.OTPModule.btnLogin.onClick = function() {
        FormControllerUtility.showProgressBar(this.view);
        var params = {
          "securityKey": response.MFAAttributes.securityKey,
          "otp": this.view.OTPModule.tbxCVV.text.trim()
        };
        this.verifyOTP(params);
      }.bind(this);
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
      
      
         validatetoEnableContinueButton:function(){
      var otp = this.view.OTPModule.tbxCVV.text.trim();
      if(otp === ""){
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
      }else{
        FormControllerUtility.enableButton(this.view.OTPModule.btnLogin);
      }
    },
      
          verifyOTP : function(data){
            var authManager = applicationManager.getAuthManager();
             var params = {
                     "MFAAttributes": {         
                       "serviceKey": authManager.getServicekey(),
                       "OTP" : data
                     } 
             };
     this.loadAuthModule().presentationController.verifyOTPPreLogin (params);
    },

    showIncorrectOTPError: function(response) {
        var scopeObj = this;
            if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts && response.MFAAttributes.remainingFailedAttempts > 0) {
                this.view.OTPModule.lblWrongOTP.setVisibility(true);
                this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.invalidAccessCode") + " " + response.MFAAttributes.remainingFailedAttempts + " " + kony.i18n.getLocalizedString("i18n.mfa.remainingAttempts");
                this.view.flxPhoneAndEmail.setVisibility(true);
                this.view.OTPModule.flxEnterOTP.setVisibility(false);
                this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
                this.view.OTPModule.tbxCVV.text = "";
                FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
                FormControllerUtility.hideProgressBar(this.view);
                this.view.forceLayout();
            } else if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.lockUser === "true") {
                FormControllerUtility.showProgressBar(this.view);
                 var scopeObj = this;
                 var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.showLoginScreen({"hideProgressBar": true,"errorMessage" :kony.i18n.getLocalizedString("i18n.mfalogin.lockeduser") +" "+ response.MFAAttributes.lockoutTime + " " + kony.i18n.getLocalizedString("i18n.mfa.minutes")}); 
                FormControllerUtility.hideProgressBar(this.view);
            } else if (response.MFAAttributes && response.MFAAttributes.remainingFailedAttempts === "0" && response.MFAAttributes.logoutUser === "true") {
                FormControllerUtility.showProgressBar(this.view);
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.showLoginScreen({"hideProgressBar": true,"errorMessage" :kony.i18n.getLocalizedString("i18n.mfaenroll.exceededOTP")}); 
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.view.OTPModule.lblWrongOTP.text = response.dbpErrMsg;
                this.view.OTPModule.lblWrongOTP.setVisibility(true);
                this.view.OTPModule.tbxCVV.text = "";
                FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
                FormControllerUtility.hideProgressBar(this.view);
            }
            this.view.forceLayout();
      },

      showDefaultPhoneEmailScreen : function(customerCommunicationInfo){
        this.bindUIForDefaultScreen(customerCommunicationInfo);
      },

      bindUIForDefaultScreen:function(response){
        if (response.MFAAttributes.remainingResendAttempts <= 0) {
          this.view.OTPModule.btnResendOTP.setVisibility(false);
        } else {
          this.bindUIForResendButton(response);
          this.view.OTPModule.btnResendOTP.setVisibility(true);
        }
        if(response.isOTPExpired === "true"){
          this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");  
          this.view.OTPModule.lblWrongOTP.setVisibility(true);
          this.view.OTPModule.tbxCVV.text = "";
          FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
          FormControllerUtility.hideProgressBar(this.view);
        }else{
          this.view.OTPModule.lblWrongOTP.setVisibility(false);
        } 
        this.view.flxPhoneAndEmail.setVisibility(true);
        this.view.OTPModule.flxEnterOTP.setVisibility(false);
        this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
        this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
        this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
        this.view.OTPModule.btnResendOTP.text =  kony.i18n.getLocalizedString("i18n.login.ResendOtp");
        this.view.OTPModule.lblRememberMe.text = kony.i18n.getLocalizedString("i18n.mfaprelogin.registerthisdevice");
        this.view.OTPModule.tbxCVV.text = "";
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        this.view.OTPModule.tbxCVV.onKeyUp = function() {
          this.validatetoEnableContinueButton();
        }.bind(this);
        this.view.OTPModule.tbxCVV.onDone = function() {
                  FormControllerUtility.showProgressBar(this.view);
                  this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
                  var params = {
                      "securityKey": response.MFAAttributes.securityKey,
                      "otp": this.view.OTPModule.tbxCVV.text.trim()
                  };
                  this.verifyOTP(params);
              }.bind(this);
        this.view.OTPModule.btnLogin.onClick = function() {
          FormControllerUtility.showProgressBar(this.view);
          this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.mfa.SACHeader");
          var params = {
            "securityKey": response.MFAAttributes.securityKey,
            "otp": this.view.OTPModule.tbxCVV.text.trim()
          };
          this.verifyOTP(params);
        }.bind(this);
        FormControllerUtility.hideProgressBar(this.view);
        this.view.forceLayout();
  
  
      },
       
      showScreentoEnterOTP:function(response){
        var scopeObj = this;
        if (response.MFAAttributes.remainingResendAttempts <= 0) {
          this.view.OTPModule.btnResendOTP.setVisibility(false);
        } else {
          this.bindUIForResendButton(response);
          this.view.OTPModule.btnResendOTP.setVisibility(true);
        }
        if(response.MFAAttributes.isOTPExpired === "true"){
          this.view.OTPModule.lblWrongOTP.setVisibility(true);
          this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.otpExpired");  
          this.view.OTPModule.tbxCVV.text = "";
          FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
          FormControllerUtility.hideProgressBar(this.view);
        }else{
          this.view.OTPModule.lblWrongOTP.setVisibility(false);
        }  
        this.view.flxPhoneAndEmail.setVisibility(true);
        this.view.OTPModule.flxEnterOTP.setVisibility(false);
        this.view.OTPModule.tbxCVV.text = "";
        this.view.OTPModule.rtxEnterCVVCode.text = kony.i18n.getLocalizedString("i18n.MFA.EnterSACOnPhone");
        this.view.OTPModule.flxEnterSecureAccessCode.setVisibility(true);
        FormControllerUtility.disableButton(this.view.OTPModule.btnLogin);
        this.view.OTPModule.btnLogin.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
        this.view.OTPModule.btnResendOTP.text = kony.i18n.getLocalizedString("i18n.login.ResendOtp");
        this.view.OTPModule.tbxCVV.onKeyUp = function(){
          this.validatetoEnableContinueButton();
        }.bind(this);
         this.view.OTPModule.tbxCVV.onDone = function(){
          FormControllerUtility.showProgressBar(scopeObj.view);
          var selectedData = {
            "securityKey" : response.MFAAttributes.securityKey,
            "otp" : this.view.OTPModule.tbxCVV.text.trim()
          };
          this.verifyOTP(selectedData);
        }.bind(this);
        this.view.OTPModule.btnLogin.onClick = function(){
          FormControllerUtility.showProgressBar(scopeObj.view);
          var selectedData = {
            "securityKey" : response.MFAAttributes.securityKey,
            "otp" : this.view.OTPModule.tbxCVV.text.trim()
          };
          this.verifyOTP(selectedData);
        }.bind(this);
        FormControllerUtility.hideProgressBar(this.view);
        this.view.forceLayout();
      },

      showRequestOTPError:function(error){
        this.view.OTPModule.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.mfa.requestOTPMessageFailed");
       this.view.OTPModule.lblWrongOTP.setVisibility(true);     
        FormControllerUtility.hideProgressBar(this.view);
        this.view.forceLayout();
    },


    };
});
