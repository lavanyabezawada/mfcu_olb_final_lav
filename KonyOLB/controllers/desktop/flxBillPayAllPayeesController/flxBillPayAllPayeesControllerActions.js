define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_gd8f935a4434423bb55a35c879809039: function AS_FlexContainer_gd8f935a4434423bb55a35c879809039(eventobject, context) {
        var self = this;
        kony.print("Action - Hold for R1");
        this.segmentAllPayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_icb64e39074a46459b01be994db02e0a: function AS_Button_icb64e39074a46459b01be994db02e0a(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnActivateEbill **/
    AS_Button_hb5f4f910dfc436f9ae46db883e1338b: function AS_Button_hb5f4f910dfc436f9ae46db883e1338b(eventobject, context) {
        var self = this;
        this.executeOnParent("activateEBill");
    },
    /** onClick defined for btnPayBill **/
    AS_Button_a487e1aab1ec4f00bad5b68d0b1f1f2d: function AS_Button_a487e1aab1ec4f00bad5b68d0b1f1f2d(eventobject, context) {
        var self = this;
        //Just adding comment to register action, will update in form controller
    }
});