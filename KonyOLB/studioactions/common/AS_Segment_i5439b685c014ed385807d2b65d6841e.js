function AS_Segment_i5439b685c014ed385807d2b65d6841e(eventobject, sectionNumber, rowNumber) {
    var self = this;
    var data = this.view.segTransactions.data;
    var index = this.view.segTransactions.selectedIndex[1];
    if (data[index].template == "segTransactionsRowSavings") {
        data[index].template = "segTransactionsRowSelectedSavings";
        this.view.segTransactions.setDataAt(data[index], index);
    } else {
        data[index].template = "segTransactionsRowSavings";
        this.view.segTransactions.setDataAt(data[index], index);
    }
}