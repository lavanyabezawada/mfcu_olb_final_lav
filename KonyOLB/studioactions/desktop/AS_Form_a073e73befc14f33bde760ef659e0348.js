function AS_Form_a073e73befc14f33bde760ef659e0348(eventobject) {
    this.postShowFrmAcknowledgement();
    if (this.presenter && this.presenter.loadHamburgerTransfers) {
        this.presenter.loadHamburgerTransfers("frmAcknowledgement");
    }
    if (this.presenter && this.presenter.loadHamburgerBillPay) {
        this.presenter.loadHamburgerBillPay("frmAcknowledgement");
    }
}