function AS_TextField_c0ee2d749f834994a6ad66d760df3051(eventobject, changedtext) {
    var amount = this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.tbxAmount.text;
    var a = replace(/$/g, '');
    var amt = parseInt(a);
    if (amt < 1000) {
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.lblAmount.isVisible = "false";
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.flxInfoDueAmount.isVisible = "true";
    }
}