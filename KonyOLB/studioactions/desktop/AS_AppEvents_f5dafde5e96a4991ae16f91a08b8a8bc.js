function AS_AppEvents_f5dafde5e96a4991ae16f91a08b8a8bc(eventobject) {
    kony.print("Testing JS Load");
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var ApplicationManager = require('ApplicationManager');
    applicationManager = ApplicationManager.getApplicationManager();
    var config = applicationManager.getConfigurationManager();
	if(performance.navigation.type===1){
        config.setBrowserRefreshProperty("true");
    }
    var sm = applicationManager.getStorageManager();
    var langObjFromStorage = sm.getStoredItem("langObj");
    if (!kony.sdk.isNullOrUndefined(langObjFromStorage)) {
        config.configurations.setItem("LOCALE", config.locale[langObjFromStorage.language]);
        config.configurations.setItem('DATEFORMAT', config.frontendDateFormat[config.getLocale()]);
    } else {
        config.configurations.setItem("LOCALE", "en_US");
        config.configurations.setItem('DATEFORMAT', config.frontendDateFormat["en_US"]);
    }
    kony.i18n.setCurrentLocaleAsync(config.configurations.getItem("LOCALE"), function() {}, function() {});
    applicationManager.getConfigurationManager().fetchApplicationProperties(function(res) {
			applicationManager.getNavigationManager().updateForm({isLanguageSelectionEnabled : res.isLanguageSelectionEnabled}, "frmLogin");
            if(config.isAppPropertiesLoaded === "false"){     
               config.setAppProperties("true");
			   kony.application.dismissLoadingScreen();
            }
		}, function() {
			kony.application.dismissLoadingScreen();
		})  
	document.body.addEventListener('contextmenu', function(e) {
        e.preventDefault();
        alert(kony.i18n.getLocalizedString("i18n.general.rightclickdisabled"));
    });
    kony.application.setApplicationBehaviors({
        "FormControllerSyncLoad": true
    })
}