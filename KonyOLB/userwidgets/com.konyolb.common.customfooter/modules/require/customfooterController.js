define(function() {
	var orientationHandler = new OrientationHandler();
	return {
        postshow : function(){
          var currBreakpoint = kony.application.getCurrentBreakpoint();
          this.onBreakpointChange(currBreakpoint);
        },
        onBreakpointChange: function(width){
          if(width==undefined)
            width = kony.application.getCurrentBreakpoint();
          if(width==640 || orientationHandler.isMobile){
            var menuWidth = this.view.btnLocateUs.frame.width+43+this.view.btnContactUs.frame.width;
            if(menuWidth<100){
              menuWidth = 175;
            }
            this.view.flxFooterMenu.width = menuWidth+"dp";
            this.view.flxFooterMenu.centerX = "50%";

            this.view.lblCopyright.centerX = "50%";
            this.view.lblCopyright.width = "preferred";
            this.view.lblCopyright.contentAlignment = 5;
          }else if(width==1024 || orientationHandler.isTablet){
            this.view.lblCopyright.centerX = "";
            this.view.flxFooterMenu.centerX = "";
            this.view.lblCopyright.contentAlignment = 4;
          }else if(width==1366){
            this.view.lblCopyright.centerX = "";
            this.view.flxFooterMenu.centerX = "";
            this.view.lblCopyright.contentAlignment = 4;
          }
          this.view.forceLayout();
        },
        setPosition: function(callback){
          this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();

            if(callback!=null || callback!=undefined){
              callback();
            }
        },
      /**
        * Method to laad Information Module and show Locate us
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
		    showLocateUsPage : function() {
          var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
          locateUsModule.presentationController.showLocateUsPage();
        },
      
        /**
        * Method to laad Information Module and show FAQs
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showFAQs : function(){
        var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
        InformationContentModule.presentationController.showFAQs();
        },
      	 /**
        * Method to laad Information Module and show terms and conditions page
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showTermsAndConditions:function(){
          var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
          InformationContentModule.presentationController.showTermsAndConditions();
        },
      	        /**
        * Method to laad Information Module and show ContactUs Page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */

      	showContactUsPage:function(){
          var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
          InformationContentModule.presentationController.showContactUsPage();
        },
              /**
        * Method to laad Information Module and show privacy policy page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      
      	showPrivacyPolicyPage:function(){
       	var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
         InformationContentModule.presentationController.showPrivacyPolicyPage();
    	}
      	
	};
});