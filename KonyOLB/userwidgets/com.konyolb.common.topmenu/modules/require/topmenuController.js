define(function() {

	return {
      navigateToBillPay: function () {
        this.view.flxContextualMenu.isVisible = false;
        this.view.flxMenu.skin="slFbox";
        this.view.flxaccounts.skin="slFbox";
        this.view.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
        this.view.flxSeperator3.setVisibility(true);
        this.view.forceLayout();
     },
      showContextualMenu:function()
      {
         if(this.view.flxTransfersAndPay.origin){
                // This is coming from actual click of the widget (Not from outside click)
                // So delegating the hiding to formcontouchend event (Inside PopUtils.js). 
                this.view.flxTransfersAndPay.origin = false; 
                this.view.flxTransfersAndPay.skin="flxHoverSkinPointer";
                this.view.imgLblTransfers.text = "O";
                return;
        }
        if(this.view.flxContextualMenu.isVisible === true)
          {
            this.view.flxTransfersAndPay.skin="flxHoverSkinPointer";
            this.view.flxContextualMenu.isVisible =false;
            this.view.imgLblTransfers.text = "O";
            this.view.forceLayout(); 
          }
        else
          {
            this.view.flxContextualMenu.isVisible =true;
            this.view.flxTransfersAndPay.skin="sknFlxHeaderTransfersSelected";
            this.view.imgLblTransfers.text = "P";
            this.view.forceLayout(); 
          }
      },
      
      navigateToTransfers: function () {
        this.view.flxContextualMenu.isVisible = false;
        this.view.flxMenu.skin="slFbox";
        this.view.flxaccounts.skin="slFbox";
        this.view.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
        this.view.flxSeperator3.setVisibility(true);
        this.view.forceLayout();
    },
      navigatetoAccounts: function () {
       this.view.flxMenu.skin="slFbox";
       this.view.flxaccounts.skin="sknFlxFFFFFbrdr3343a8";
       this.view.flxTransfersAndPay.skin="copyslfbox1";
       this.view.flxSeperator3.setVisibility(false);
       this.view.forceLayout();
    },
//    	transferSkin:function(){
//       if(this.view.flxContextualMenu.isVisible === true){
//       this.view.flxContextualMenu.isVisible = false;
//         if(this.view.btnAccounts.skin=="sknBtnTopmenuFocusAccounts")
//           	this.view.btnTransfers.skin="sknBtnTopmenu2";
//         else
//       		this.view.btnTransfers.skin="sknBtnTopmenuFocusTransfers";
//     }else{
//       this.view.flxContextualMenu.isVisible = true;
//       this.view.btnTransfers.skin="sknBtnTopmenuEBEBEBTransfers";
//     }
   // },
     fixContextualMenu: function () {
     var flex_menu_width=50;
     this.view.flxMenu.width=flex_menu_width+"dp";
     var flex_accounts_width=25+this.view.lblAccounts.frame.width+50;
     this.view.flxaccounts.width=flex_accounts_width+"dp";
     var flex_transfers_width=25+this.view.lblTransferAndPay.frame.width+50;
     this.view.flxTransfersAndPay.width=flex_transfers_width+"dp";
     var left_for_contextual_menu=flex_transfers_width+1;
     left_for_contextual_menu="-"+left_for_contextual_menu+"dp";
       kony.print("left:"+left_for_contextual_menu);
     this.view.flxContextualMenu.left=left_for_contextual_menu;
     this.view.flxContextualMenu.width=flex_transfers_width+"dp"; 
     this.view.flxMenusMain.forceLayout();
    }
 
	};
});