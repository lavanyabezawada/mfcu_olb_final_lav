define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for TabsHeader **/
    AS_FlexScrollContainer_j4cd64584e774f06b17bf30e936505c5: function AS_FlexScrollContainer_j4cd64584e774f06b17bf30e936505c5(eventobject) {
        var self = this;
        //this.invokePreShow();
    },
    /** onClick defined for btnTab1 **/
    AS_Button_a614515ebdd9455792ca1649dcfd4f84: function AS_Button_a614515ebdd9455792ca1649dcfd4f84(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab2 **/
    AS_Button_bf20f23cd8a44a1ba704ed6237534af0: function AS_Button_bf20f23cd8a44a1ba704ed6237534af0(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab3 **/
    AS_Button_j38695756d78492ca8da49575f68f7c8: function AS_Button_j38695756d78492ca8da49575f68f7c8(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab4 **/
    AS_Button_b01b02631ffb41a6b99207fed1af17dc: function AS_Button_b01b02631ffb41a6b99207fed1af17dc(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab5 **/
    AS_Button_c43347abd62946cf8663fa6e2a58c0e7: function AS_Button_c43347abd62946cf8663fa6e2a58c0e7(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab6 **/
    AS_Button_f2aa8b7b80444332908eb783f2534d07: function AS_Button_f2aa8b7b80444332908eb783f2534d07(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    }
});