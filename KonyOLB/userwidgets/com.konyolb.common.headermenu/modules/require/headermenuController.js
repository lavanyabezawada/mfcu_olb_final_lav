define(['ViewConstants'],function(ViewConstants) {

	return {
      
      /**
       * Set badge size 
       */
      setMessageBadgeSize: function()
      {
        var numberOfMessages = parseInt(this.view.lblNewMessages.text);
        if(numberOfMessages<=99)
          {
            this.view.lblNewMessages.width="15dp";
            this.view.lblNewMessages.height="15dp";
          }
        else
          {
            
            this.view.lblNewMessages.width="20dp";
            this.view.lblNewMessages.height="20dp";
          }
      },
      updateAlertIcon: function () {
        this.view.lblNewMessages.setVisibility(false);
        var unreadCount = applicationManager.getConfigurationManager().getUnreadMessageCount();
        if (unreadCount.count > 0) {
          this.view.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_FLAG;
        } else {
            this.view.imgNotifications.src = ViewConstants.IMAGES.NOTIFICATION_ICON;
        }

      },
      /**
       * Calls when this component shows on Form
       */
	  headerPreShow:function(){
          var configurationManager = applicationManager.getConfigurationManager();
          var isUserLoggedIn = applicationManager.getUserPreferencesManager().isUserLoggedin();
          if(configurationManager.enableAlertsIcon==="true" && 
          isUserLoggedIn){
               this.view.flxMessages.isVisible=true;
          }
          else{
           		this.view.flxMessages.isVisible=false;
          }
          this.updateAlertIcon();
        this.view.forceLayout();
      }
      

	};
});