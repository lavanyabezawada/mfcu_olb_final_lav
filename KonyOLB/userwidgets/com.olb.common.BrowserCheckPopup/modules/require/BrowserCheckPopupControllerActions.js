define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for BrowserCheckPopup **/
    AS_FlexContainer_g78da708474347e1a67bb5e057814ccf: function AS_FlexContainer_g78da708474347e1a67bb5e057814ccf(eventobject) {
        var self = this;
        if (kony.os.deviceInfo().screenWidth > 1024) {
            this.fetchSupportedBrowsersList();
            this.checkBrowser();
        }
    }
});