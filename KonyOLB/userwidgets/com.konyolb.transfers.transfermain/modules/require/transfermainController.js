define(function() {

	return {
		toggleSearch: function () {
          if(this.view.flxSearch.isVisible) {
            this.view.imgSearch.src = "search_blue.png";
            this.view.flxSearch.setVisibility(false);
          }
          else{
             this.view.imgSearch.src = "selecetd_search.png";
             this.view.flxSearch.setVisibility(true);
          }
        }
	};
});