define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgOption0 **/
    AS_Image_c201d406ce7f423ba2da0a75af7b78b5: function AS_Image_c201d406ce7f423ba2da0a75af7b78b5(eventobject, x, y) {
        var self = this;
        if (this.view.imgOption0.src === "radioinactivebb.png") {
            this.view.imgOption0.src = "radioactivebb.png";
            this.view.imgOption1.src = "radioinactivebb.png";
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** onTouchStart defined for imgOption1 **/
    AS_Image_ie3f373392b6404b8be7c574c0dbc88a: function AS_Image_ie3f373392b6404b8be7c574c0dbc88a(eventobject, x, y) {
        var self = this;
        if (this.view.imgOption1.src === "radioinactivebb.png") {
            this.view.imgOption1.src = "radioactivebb.png";
            this.view.imgOption0.src = "radioinactivebb.png";
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** preShow defined for TabSearchBarNew **/
    AS_FlexContainer_jf5a7426b53043e7877df0f35536c7e5: function AS_FlexContainer_jf5a7426b53043e7877df0f35536c7e5(eventobject) {
        var self = this;
        //this.invokePreShow();
    }
});