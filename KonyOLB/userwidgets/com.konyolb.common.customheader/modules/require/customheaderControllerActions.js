define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_f8f9ed8b20654251b5de63803fdfdc66: function AS_FlexContainer_f8f9ed8b20654251b5de63803fdfdc66(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_ibf464d4b3984ee281848819c0c78a61: function AS_FlexContainer_ibf464d4b3984ee281848819c0c78a61(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_g482d6beeaa34da28696ace775a3ca4e: function AS_FlexContainer_g482d6beeaa34da28696ace775a3ca4e(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** preShow defined for customheader **/
    AS_FlexContainer_fec7c5dc9d644874851f1959371cfd93: function AS_FlexContainer_fec7c5dc9d644874851f1959371cfd93(eventobject) {
        var self = this;
        this.initHeader();
    },
    /** postShow defined for customheader **/
    AS_FlexContainer_i3a1eb781d8d4d8b95b97fdb9016ea6c: function AS_FlexContainer_i3a1eb781d8d4d8b95b97fdb9016ea6c(eventobject) {
        var self = this;
        this.postShowFunction();
    }
});