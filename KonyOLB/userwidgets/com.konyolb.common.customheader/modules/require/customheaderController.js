/*eslint require-jsdoc:0
          complexity:0
*/

define(['CommonUtilities', 'TopbarConfig', 'FormControllerUtility'], function (CommonUtilities, TopbarConfig, FormControllerUtility) {
  function getTotalHeight() {
    var height = 0;
    var widgets = kony.application.getCurrentForm().widgets();
    for (var i = 0; i < 3; i++) {
      var widget = widgets[i];
      height += widget.frame.height;
    }
    height += kony.application.getCurrentForm().flxFooter.frame.y;
    return height;
  }
  var orientationHandler = new OrientationHandler();
  return {
    setupLogout: function () {
      this.view.customhamburger.flxLogout.onClick = this.showLogout.bind(this);
      if (TopbarConfig.config.LOGOUT.excludedForms.indexOf(kony.application.getCurrentForm().id) < 0) {
        this.view.headermenu.btnLogout.text = "";
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.setVisibility(true);
        this.view.headermenu.btnLogout.onClick = this.showLogout.bind(this);
      }
    },
    showLogout: function () {
      var currentForm = kony.application.getCurrentForm();
      if ('flxLogout' in currentForm) {
        this.closeHamburgerMenu();
        var children = currentForm.widgets();
        var footerHeight = 0;
        if(!(currentForm.id == "frmLocateUs" && orientationHandler.isMobile)){
          footerHeight = currentForm.flxFooter ? currentForm.flxFooter.frame.height +  (currentForm.flxFooter.frame.y-(children[0].frame.height+ children[1].frame.height)): 0;
        }
        var height = children[0].frame.height + children[1].frame.height + footerHeight;
        currentForm.flxLogout.setVisibility(true);
        currentForm.flxLogout.height = height + "dp";
        currentForm.flxLogout.left = "0%";
        var popupComponent = currentForm.flxLogout.widgets()[0];
        popupComponent.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        popupComponent.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        popupComponent.top = ((kony.os.deviceInfo().screenHeight / 2) - 135 ) + "px";
        popupComponent.btnYes.onClick = function () {
          FormControllerUtility.showProgressBar(kony.application.getCurrentForm());
          TopbarConfig.config.LOGOUT.onClick();
          currentForm.flxLogout.left = "-100%";
        };
        popupComponent.btnNo.onClick = function () {
          currentForm.flxLogout.left = "-100%";
        }
        popupComponent.flxCross.onClick = function () {
          currentForm.flxLogout.left = "-100%";
        }
      }
    },
    initHeader: function () {
      this.setupContextualActions();
      this.setupHamburger();
      this.setupUserProfile();
      this.setupLogout();
      this.setupUserActions();
      this.setHoverSkins();
      flag = 0;
      this.view.topmenu.flxaccounts.onClick = TopbarConfig.config.ACCOUNTS.onClick;
      this.view.headermenu.flxMessages.onClick = TopbarConfig.config.MESSAGES.onClick;
      this.view.topmenu.flxHelp.onClick = TopbarConfig.config.HELP.onClick;
      this.view.topmenu.flxFeedbackimg.onClick = TopbarConfig.config.FEEDBACK.onClick;;
      this.onBreakpointChange();
    },
    setupUserActions: function () {
      this.view.segUserActions.widgetDataMap = {
        lblSeparator:"lblSeparator",
        lblUsers: "lblUsers"
      };
      this.view.segUserActions.setData(TopbarConfig.config.USER_ACTIONS.map(function (configItem) {
        return {
          lblSeparator:"L",
           lblUsers: {
             "text": kony.i18n.getLocalizedString(configItem.text),
             "toolTip":kony.i18n.getLocalizedString(configItem.text)
           }
           }
      }));
      this.view.segUserActions.onRowClick = function (widget, section, rowIndex) {
        TopbarConfig.config.USER_ACTIONS[rowIndex].onClick();
      }
    },
    setupUserProfile: function () {
      var userObject = applicationManager.getUserPreferencesManager().getUserObj();
      this.view.headermenu.imgUserReset.src = userObject.userImageURL;
      if(userObject.userImageURL){
        this.view.headermenu.imgUserReset.src  = userObject.userImageURL;
      }else{
       this.view.headermenu.imgUserReset.src  = "user_profile_default.png";
      }
      if(userObject.userImageURL){
        this.view.CopyimgToolTip0i580d9acc07c42.src = userObject.userImageURL;
      }else{
        this.view.CopyimgToolTip0i580d9acc07c42.src = "user_profile_default.png";
      }
      this.view.lblName.text = userObject.userfirstname + "" + userObject.userlastname;
      this.view.lblUserEmail.text = userObject.email;
      this.view.flxUserActions.isVisible = false;
            this.view.segUserActions.onRowClick = function(widget, section, rowIndex) {
              var profileModule;
                if (rowIndex === 0) {
                    profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.showProfileSettings();
                }
                if (rowIndex === 1) {
                    profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showPreferredAccounts();
                }
                if (rowIndex === 2) {
                    profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.fetchAlerts("Account");
                }
            }

    },
    setupContextualActions: function () {
     var scope = this;
            var configurationManager = applicationManager.getConfigurationManager();
            if(configurationManager.isKonyBankAccountsTransfer === "false" && configurationManager.isOtherKonyAccountsTransfer === "false" && configurationManager.isOtherBankAccountsTransfer === "false" && configurationManager.isInternationalAccountsTransfer === "false" && configurationManager.isInternationalWireTransferEnabled === "false" && configurationManager.isDomesticWireTransferEnabled === "false"&& configurationManager.isBillPayEnabled === "false" && configurationManager.ispayAPersonEnabled === "false"){
                this.view.topmenu.flxTransfersAndPay.setVisibility(false);
                this.view.topmenu.imgLblTransfers.setVisibility(false);
                this.view.topmenu.lblTransferAndPay.setVisibility(false);
            }else{
                 this.view.topmenu.flxTransfersAndPay.onClick = this.openContextualMenu.bind(this);
                 this.view.topmenu.flxTransfersAndPay.onTouchStart = function () {
                   if (scope.view.topmenu.flxContextualMenu.isVisible) {
                    scope.view.topmenu.flxTransfersAndPay.origin = true;
                    if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
                          scope.view.topmenu.flxContextualMenu.isVisible = false;
                          scope.view.topmenu.flxTransfersAndPay.skin="flxHoverSkinPointer";
                          scope.view.topmenu.imgLblTransfers.text = "O";
                   }
                }
                 }
                TopbarConfig.config.TOP_BAR_CONTEXTUAL_MENU.forEach(function(contextualItem) {
                scope.view.topmenu[contextualItem.widget].setVisibility(!contextualItem.isVisible || contextualItem.isVisible())
                scope.view.topmenu[contextualItem.widget].onClick = contextualItem.onClick;
            });
            }
    },
     openContextualMenu: function() {
            var scope = this;
            if (scope.view.topmenu.flxTransfersAndPay.skin === "sknFlxHeaderTransfersSelected") {
                scope.view.topmenu.flxTransfersAndPay.skin = "flxHoverSkinPointer";
                scope.view.topmenu.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
                scope.view.topmenu.imgLblTransfers.text = "O";
            } else {
                scope.view.topmenu.flxTransfersAndPay.skin = "sknFlxHeaderTransfersSelected";
                scope.view.topmenu.imgLblTransfers.text = "P";
            }
            scope.view.topmenu.showContextualMenu();
        },
    setupHamburger: function () {
      var scope = this;
      this.view.customhamburger.setItemSelectListener(this.closeHamburgerMenu.bind(this));
      this.view.topmenu.flxMenu.onClick = function(){
        scope.openHamburgerMenu();
      }
      this.view.flxHamburgerBack.onClick = this.closeHamburgerMenu.bind(this);
      this.view.customhamburger.flxClose.onClick = this.closeHamburgerMenu.bind(this);
    },
    openHamburgerMenu: function () {
      var id = kony.application.getCurrentForm().id;
      // For Scroll to top.
      this.view.imgKony.setFocus(true);
      var element =  document.getElementById(id);
      if (element) {
       element.style.position = "fixed !important";
       element.style.overflowY = "hidden";
      }
      // if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
      //   this.view.flxHamburger.height = (kony.os.deviceInfo().screenHeight-15) + "dp";
      //   this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight-15) + "dp";
      // }else{
        this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
        this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight) + "dp";
        this.view.customhamburger.flxMenuWrapper.height = (kony.os.deviceInfo().screenHeight-160) + "dp";

        this.view.customhamburger.flxLogout.top = (kony.os.deviceInfo().screenHeight-80) + "dp";
        this.view.customhamburger.FlexContainer0a82014659b0e48.top = (kony.os.deviceInfo().screenHeight-100) + "dp";
        this.view.customhamburger.CopyFlexContainer0f46534a0a94e41.top = (kony.os.deviceInfo().screenHeight-100) + "dp";
      // }

      this.showBackFlex();
      var scope = this;
      var animationDefinition = {
        100: {
          "left": 0
        }
      };
      var animationConfiguration = {
        duration: 0.8,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {
          scope.view.customhamburger.forceLayout();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    closeHamburgerMenu: function () {
      var id = kony.application.getCurrentForm().id;
      var element = document.getElementById(id);
      if (element) {
         element.style.overflowY = "auto";
      	 element.style.position = "relative";
      }
      this.hideBackFlex();
      var animationDefinition = {
        100: {
          "left": "-200.13%"
        }
      };
      var animationConfiguration = {
        duration: 1.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {

        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    showBackFlex: function () {
      this.view.flxHamburgerBack.height = getTotalHeight() + "px";
      this.view.flxHamburgerBack.isVisible = true;
      this.view.flxHamburger.isVisible = true;
      this.view.forceLayout();
    },
    hideBackFlex: function () {
      this.view.flxHamburgerBack.height = "0px";
      this.view.forceLayout();
    },
    showUserActions: function () {
      if(this.view.FlexContainer0e2898aa93bca45.frame.x==0){
        this.view.flxUserActions.left = -410 + this.view.FlexContainer0e2898aa93bca45.frame.width + "dp";
        if(kony.application.getCurrentBreakpoint()==1024|| orientationHandler.isTablet){
          this.view.flxUserActions.left = -365 + this.view.FlexContainer0e2898aa93bca45.frame.width + "dp";
        }
      }else{
        this.view.flxUserActions.left = 945 + this.view.FlexContainer0e2898aa93bca45.frame.x + "dp";
      }
      if (this.view.flxUserActions.isVisible === false) {
        this.view.headermenu.imgDropdown.src = "profile_dropdown_uparrow.png";
        this.view.flxUserActions.isVisible = true;
        this.view.lblName.setFocus(true);
      } else {
        this.view.headermenu.imgDropdown.src = "profile_dropdown_arrow.png";
        this.view.flxUserActions.isVisible = false;
      }
    },
    forceCloseHamburger: function () {
      this.hideBackFlex();
      this.view.flxHamburger.left = "-200%";
      this.view.forceLayout();
    },
    postShowFunction: function () {
      this.view.flxUserActions.left = 945 + this.view.FlexContainer0e2898aa93bca45.frame.x + "dp";
    },
    
    setHoverSkins: function() {
       var currForm = kony.application.getCurrentForm().id;
       this.setDefaultHoverSkins();
       this.view.topmenu.flxContextualMenu.isVisible = false;
       this.view.topmenu.imgLblTransfers.text = "O";
       if(currForm === 'frmAccountsDetails' || currForm === 'frmAccountsLanding')
         this.view.topmenu.flxaccounts.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
       else if(currForm === 'frmBillPay' || currForm === 'frmTransfers' || currForm === 'frmPayAPerson' || currForm === 'frmWireTransfer' || currForm === 'frmAcknowledgement' || currForm === 'frmConfirm' || currForm === 'frmVerifyAccount')
         this.view.topmenu.flxTransfersAndPay.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
       else if(currForm === 'frmCustomerFeedback' || currForm === 'frmCustomerFeedbackSurvey'){
         this.view.topmenu.flxFeedbackimg.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.topmenu.flxFeedbackimg.skin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.topmenu.lblFeedback.skin = "sknLblffffff15pxSSP";
       }
       else if(currForm === 'frmOnlineHelp')
         this.view.topmenu.flxHelp.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";  
    },
    setDefaultHoverSkins: function() {
       this.view.topmenu.flxaccounts.hoverSkin = "flxHoverSkinPointer000000op10";
       this.view.topmenu.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
       this.view.topmenu.flxFeedbackimg.hoverSkin = "flxHoverSkinPointer000000op10";
       this.view.topmenu.flxHelp.hoverSkin = "flxHoverSkinPointer000000op10";  
    },
    onBreakpointChange: function(width){
      if(width==undefined || width==null){
        width = kony.application.getCurrentBreakpoint();
      }
      var scope = this;

      this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
      this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight) + "dp";
      this.view.customhamburger.flxMenuWrapper.height = (kony.os.deviceInfo().screenHeight-160) + "dp";
      this.view.customhamburger.flxLogout.top = (kony.os.deviceInfo().screenHeight-80) + "dp";
      this.view.customhamburger.FlexContainer0a82014659b0e48.top = (kony.os.deviceInfo().screenHeight-100) + "dp";
      this.view.customhamburger.CopyFlexContainer0f46534a0a94e41.top = (kony.os.deviceInfo().screenHeight-100) + "dp";

      this.view.flxUserActions.setVisibility(false);
      this.view.lblHeaderMobile.setVisibility(false);

      if(this.isPreLoginView){
        this.showPreLoginView();
      }else{
        this.showPostLoginView();
      }

      if(width === 640 || orientationHandler.isMobile){
        scope.view.customhamburger.width = "90%";
        scope.view.imgKony.isVisible=false;
        this.view.lblHeaderMobile.setVisibility(true);
      }else if(width === 1024 || width === 768 || orientationHandler.isTablet){
        scope.view.customhamburger.width = "60%";
        scope.view.imgKony.isVisible=true;
      }else{
        scope.view.customhamburger.width = "35%";
        scope.view.imgKony.isVisible=true;
      }
       scope.view.flxHamburger.width = "100%";
      if(width==640 || orientationHandler.isMobile){
        this.view.topmenu.flxTransfersAndPay.onTouchStart = null;
      }else if(width==1024 || orientationHandler.isTablet){
        this.view.topmenu.flxTransfersAndPay.onTouchStart = null;
      }else{

      }
    },
    isPreLoginView: false,
    showPreLoginView: function(){
      this.isPreLoginView = true;

      if(orientationHandler.isMobile || kony.application.getCurrentBreakpoint()==640){
        this.view.topmenu.flxMenusMain.setVisibility(false);
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomBlue.setVisibility(true);
      }else{
        this.view.height = "70dp";
        this.view.FlexContainer0e2898aa93bca45.height = "70dp";
        this.view.flxTopmenu.setVisibility(false);
        this.view.flxBottomContainer.height = "70dp";
        this.view.flxBottomBlue.setVisibility(false);
        this.view.flxSeperatorHor2.top = "70dp";

        this.view.headermenu.flxMessages.isVisible = false;
        this.view.headermenu.flxVerticalSeperator1.isVisible = false;
        this.view.headermenu.flxResetUserImg.isVisible = false;
        this.view.headermenu.flxUserId.isVisible = false;            
        this.view.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.src = "login_icon_locateus.png";
        this.view.headermenu.flxWarning.setVisibility(true);
      }
    },
    showPostLoginView: function(){
      this.isPreLoginView = false;

      if(orientationHandler.isMobile || kony.application.getCurrentBreakpoint()==640){
        this.view.topmenu.flxMenusMain.setVisibility(true);
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomBlue.setVisibility(true);
      }else{
        this.view.height = "120dp";
        this.view.FlexContainer0e2898aa93bca45.height = "120dp";
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomContainer.height = "120dp";
        this.view.flxBottomBlue.setVisibility(true);
        this.view.flxSeperatorHor2.top = "120dp";

        this.view.headermenu.flxMessages.isVisible = true;
        this.view.headermenu.flxVerticalSeperator1.isVisible = true;
        this.view.headermenu.flxResetUserImg.isVisible = true;
        this.view.headermenu.flxUserId.isVisible = true;            
        this.view.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.src = "logout.png";
        this.view.headermenu.flxWarning.setVisibility(false);
      }
    }
  };
});