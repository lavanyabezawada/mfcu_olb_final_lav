define(['HamburgerConfig'], function (HamburgerConfig) {
    var MENU_CONTAINER = "flxMenuWrapper";
    var SAMPLE_MENU = "flxAccountsMenu";
    var SAMPLE_SUB_MENU = "flxAccountsSubMenu";
    var SAMPLE_SUB_MENU_ITEM = "flxMyAccounts";
    var onItemSelectListener = null;
    return {

        initialized: false,
        actiavteWhenInitialized: null,

        /**
         * Sets onItemSelect Listener
         */
        setItemSelectListener: function (listener) {
            onItemSelectListener = listener;
        },
        

        /**
         * Activates the menu
         * @param {string} parentId of parent menu
         * @param {string} childId of parent menu
         */
        activateMenu: function (parentId, childId) {
            if (!this.initialized) {
                this.actiavteWhenInitialized = {
                    parentId: parentId,
                    childId: childId
                }
                return;
            }
            var parentIndex = 0;
            var menuObject = null;
            var visibleIndex = 0;
            HamburgerConfig.config.forEach(function (menuItem) {
                 if (!menuItem.isVisible || menuItem.isVisible()) {
                    if( menuItem.id.toLowerCase() === parentId.toLowerCase()) {
                        parentIndex = visibleIndex;
                        menuObject = menuItem;                        
                    }
                    visibleIndex++;
                 }
            });
            if (menuObject) {
                var childIndex = -1;
                var visibleChildIndex = 0;
                var children = this.view[MENU_CONTAINER].widgets();
                this.collapseAll();
                this.resetSkins();
                this.expandWithoutAnimation(children[parentIndex*2 +1]);
                children[parentIndex*2].widgets()[2].widgets()[0].src = "arrow_up.png";
                if (childId) {
                    var childObject = null;
                     menuObject.subMenu.children.forEach(function (childItem) {
                         if (!childItem.isVisible || childItem.isVisible()) {
                            if (childItem.id.toLowerCase() === childId.toLowerCase()) {
                                childIndex = visibleChildIndex;
                                childObject = childItem;
                             }
                             visibleChildIndex ++;
                         }
                    });
                    if (childObject) {
                        children[parentIndex*2+1].widgets()[childIndex].skin = "skncursor";
                        children[parentIndex*2+1].widgets()[childIndex].hoverSkin = "skncursor";
                        children[parentIndex*2+1].widgets()[childIndex].widgets()[0].skin = "sknLblHamburgerSelected";
                    }
                }
            }
          
        },

        /**
         * EXpand WIthout Animation
         */
        expandWithoutAnimation: function (widget) {
            widget.height = widget.widgets().length * 60;
            this.view.forceLayout();
        },

        /**
         * Resets the skin
         */
        resetSkins: function () {
            var subMenus = this.view[MENU_CONTAINER].widgets().filter(function(child, i) {
                return i % 2 !== 0;
            })

            subMenus.forEach(function (subMenu) {
                subMenu.widgets().forEach(function (subMenuItem) {
                    subMenuItem.skin = "skncursor";
                    subMenuItem.hoverSkin = "sknFlxHoverHamburger";
                    subMenuItem.widgets()[0].skin = "sknLblHamburgerUnSelected";
                })
            })
            
        },

        /**
         * Generate prefix by removing whitespace
         * @param {string} id Id of ite,
         * @returns {string} id with whitespace removed
         */
        getPrefix: function (id) {
            return id.replace(/ /g,'')
        },

        /**
         * Toggles the sub menu
         * @param {kony.ui.FlexContainer} widget Submenu widget to toggle 
         * @param {kony.ui.FlexContainer} imgArrow imgArrow to toggle
         */
        toggle: function (widget, imgArrow) {
            if (widget.frame.height > 0) {
                this.collapseAll();
            } else {
                this.collapseAll();
                if(imgArrow.src === "arrow_down.png"){
                   imgArrow.src = "arrow_up.png";
                   imgArrow.toolTip="Collapse";
                   this.view.forceLayout();
                }
                this.expand(widget);
            }
           this.view.forceLayout();
        },

        /**
         * Collapses the subMenu
         * @param {kony.ui.FlexContainer} widget Submenu widget to expand
         */
        expand: function (widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height": widget.widgets().length * 60
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function () {
                    // scope.checkLogoutPosition();
                    widget.widgets()[0].setFocus(true);
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },

        /**
         * Collapse All the sub menu items
         */
        collapseAll: function () {
            var self = this;
            var menuItems = this.view[MENU_CONTAINER].widgets();
            menuItems.forEach(function (menuItem, i) {
                if (i % 2 !== 0) {
                    self.collapseWithoutAnimation(menuItem);
                    var imageWidget = menuItems[i-1].widgets()[2].widgets()[0];
                    if(imageWidget.src === "arrow_up.png"){
                       imageWidget.src = "arrow_down.png";
                        imageWidget.toolTip = "Expand";
                    }
                  
                }
               
            })
            self.view.forceLayout();
        },

        /**
         * Collapse the submenu item
         * @param {kony.ui.FlexContainer} widget submenu flex to collapse
         */
        collapseWithoutAnimation: function (widget) {
            widget.height = 0;
            this.view.forceLayout();
        },

        /**
         * Generates View of the menu dynamically
         */
        generateMenu: function () {
            var self = this;
            HamburgerConfig.config.forEach(function (hamburgerItem) {
                if (!hamburgerItem.isVisible || hamburgerItem.isVisible()) {
                    var parentMenuView = self.getParentMenuItemView(hamburgerItem);
                    var subMenuView = self.getSubMenuView(hamburgerItem);
                    var imgArrow = parentMenuView.widgets()[2].widgets()[0];
                    var flxArrow = parentMenuView.widgets()[2];
                    if(kony.application.getCurrentBreakpoint()==640){
                        flxArrow.left = "88%";
                        self.view.flxClose.left = "88%";
                    }else{
                        flxArrow.left = "92%";
                        self.view.flxClose.left = "92%";
                    }
                    self.view.forceLayout();
                    parentMenuView.onClick = self.toggle.bind(self, subMenuView, imgArrow);
                    self.view[MENU_CONTAINER].add(parentMenuView, subMenuView);
                }
                
            });
        },

        postShowHamburger: function () {
            this.view.flxMenu.height = kony.os.deviceInfo().screenHeight + "px";
            this.view.forceLayout();
          },

        /**
         * Generates view of sub menu item
         * @param {object} subMenuItem Sub Menu Item Config
         * @param {string} id Id for prefixing
         * @param {boolean} removeSeperator Removes the seperator 
         * @returns {kony.ui.FlexContainer} Sub menu Item view
         */
        getSubMenuItemView: function (subMenuItem, id, removeSeperator) {
            var subMenuItemView = this.view[SAMPLE_SUB_MENU_ITEM].clone(id);
            subMenuItemView.widgets()[0].text = kony.i18n.getLocalizedString(subMenuItem.text);
            subMenuItemView.widgets()[0].toolTip = kony.i18n.getLocalizedString(subMenuItem.toolTip);
            if (removeSeperator) {
                subMenuItemView.removeAt(1);
            }
            subMenuItemView.onClick = this.bindAction(subMenuItem.onClick)
            return subMenuItemView;
        },

        /**
         * Bind onclick to hamburger item
         * @param {function} originalOnclick Original OnClick from config
         * @returns {function} Composed function
         */
        bindAction: function (originalOnclick) {
            return function () {
                if (onItemSelectListener) {
                    onItemSelectListener();
                }
                originalOnclick();
            }
        },

        /**
         * Generates the view of sub menu
         * @param {object} hamburgerItem config of item
         * @returns {kony.ui.FlexContainer} returns the view of submenu
         */
        getSubMenuView: function (hamburgerItem) {
            var self = this;
            var subMenuView = this.view[SAMPLE_SUB_MENU].clone(this.getPrefix(hamburgerItem.id));
            subMenuView.removeAll();
            hamburgerItem.subMenu.children.forEach(function (subMenuItem, index) {
                if (!subMenuItem.isVisible || subMenuItem.isVisible.call(HamburgerConfig)) {
                    var subMenuItemView = self.getSubMenuItemView(subMenuItem, self.getPrefix(hamburgerItem.id)+ index, index !== hamburgerItem.subMenu.children.length - 1);
                    subMenuView.add(subMenuItemView);
                }
               
            })
            return subMenuView;
        },

        /**
         * Generate View for Parent menu Item
         * @param {string} hamburgerItem Title of Parent Menu Item
         * @returns {kony.ui.FlexContainer} Returns the flex container object
         */
        getParentMenuItemView: function (hamburgerItem) {
            var parentMenuFlex = this.view[SAMPLE_MENU].clone(this.getPrefix(hamburgerItem.id));
            var childWidgets = parentMenuFlex.widgets();
            childWidgets[0].text = hamburgerItem.icon;
            childWidgets[0].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);
            childWidgets[1].text = kony.i18n.getLocalizedString(hamburgerItem.text);
            childWidgets[1].toolTip = kony.i18n.getLocalizedString(hamburgerItem.toolTip);
          if(hamburgerItem.id == "TRANSFERS")
            {
              childWidgets[0].skin ="sknLblFontType0273E330px";
            }
            if(hamburgerItem.id == "About Us")
            {
              childWidgets[0].skin = "sknLblFontType0273E320px";
            }
            return parentMenuFlex;
        },

        /**
         * Preshow of Hamburger Component
         */
        initHamburger: function () {
            if (!this.initialized && applicationManager.getUserPreferencesManager().isUserLoggedin()) {
                this.view[MENU_CONTAINER].removeAll();
                this.generateMenu();
                this.initialized = true;
                if (this.actiavteWhenInitialized) {
                    this.activateMenu(this.actiavteWhenInitialized.parentId, this.actiavteWhenInitialized.childId)
                    this.actiavteWhenInitialized = null;
                }
            }
        },
        /**
         * For Change Language Support
         */
        forceInitializeHamburger: function () {
            this.view[MENU_CONTAINER].removeAll();
            this.generateMenu();
            this.initialized = true;
        }
    }
})