define(function() {

	return {
		toggleCheckbox: function(){
          if(this.view.imgCheckbox.src == "checked_box.png"){
            this.view.imgCheckbox.src = "unchecked_box.png";
          }else{
            this.view.imgCheckbox.src = "checked_box.png";
          }
        }
	};
});