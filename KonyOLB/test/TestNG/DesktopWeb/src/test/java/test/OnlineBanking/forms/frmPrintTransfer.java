package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmPrintTransfer {


  public frmPrintTransfer() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_frmPrintTransfer"));
  }
public void btnBack() throws Exception{ 
  AppElement btnBack=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_btnBack"));
  btnBack.click();
  }
public void btnBackBottom() throws Exception{ 
  AppElement btnBackBottom=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_btnBackBottom"));
  btnBackBottom.click();
  }


public void rtxDisclaimer(String text) throws Exception{
  AppElement rtxDisclaimer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_rtxDisclaimer"));
  rtxDisclaimer.type(text);
  }


public void segTransfers(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_segTransfers"),OnlineBankingWidgetId.getWidgetId("frmPrintTransfer_flxPrintTransfer"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}