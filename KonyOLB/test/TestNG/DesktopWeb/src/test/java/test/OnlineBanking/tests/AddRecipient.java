//package test.OnlineBanking.tests;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Random;
//
//import org.openqa.selenium.WebElement;
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;
//
//import test.OnlineBanking.AppSpecificFunctions;
//import test.OnlineBanking.OnlineBankingBaseTest;
//import test.OnlineBanking.OnlineBankingWidgetId;
//import test.OnlineBanking.forms.frmAccountsLanding;
//import test.OnlineBanking.forms.frmAddInternalAccount;
//import test.OnlineBanking.forms.frmCustomHeaderForm;
//import test.OnlineBanking.forms.frmExternalAccounts;
//import test.OnlineBanking.forms.frmLogin;
//import test.OnlineBanking.forms.frmTransfers;
//import test.OnlineBanking.forms.frmConfirmAccount;
//import test.OnlineBanking.forms.frmVerifyAccount;
//import test.common.AppElement;
//import test.common.SgConfiguration;
//
//public class AddRecipient extends OnlineBankingBaseTest {
//	frmCustomHeaderForm header=new frmCustomHeaderForm();
//	SgConfiguration sg = SgConfiguration.getInstance();
//	frmAddInternalAccount addinternalacount=  new frmAddInternalAccount();
//	frmConfirmAccount frmConfirmAccount=new frmConfirmAccount();
//	frmAccountsLanding frmaccountslanding=new frmAccountsLanding();
//	frmLogin frmlogin=new frmLogin();
//	frmVerifyAccount frmVerifyAccount=new frmVerifyAccount();
//	frmTransfers transfer = new frmTransfers();
//
//	@BeforeMethod
//	public void setupBeforeTest() throws Exception {
//
//		if(!frmAddInternalAccount.isfrmAddInternalAccount())
//		{
//			if(frmAccountsLanding.isLandingForm())
//			{
//				addinternalacount.navigateTofrmAddInternalAccount(); 
//			}
//			else if(frmLogin.isLoginForm())			{
//				frmlogin.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
//				addinternalacount.navigateTofrmAddInternalAccount();
//			}
//		}
//	}
//	
//	@AfterMethod
//	public void actionAfterMethod()
//	{
//		try {
//			header.clickOnAccounts();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Verify 'Add kony account/add Non Kony account' option available in menu")
//	public void verifyRecipientOption() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		header.clickOnMenubar();
//		//		header.clickOnTransfersMenuItem();
//		WebElement ele= header.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_lblAddKonyAccounts"));
//		sa.assertEquals(ele.getText(), "Add Kony Accounts");
//		WebElement ele2= header.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_lblAddNonKonyAccounts"));
//		sa.assertEquals(ele2.getText(), "Add Non Kony Accounts");
//		header.clickOnCloseButton();
//		sa.assertAll();
//
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Verify field available in the 'Add kony account' option")
//	public void verifyFieldsInAddkonyAccount() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		AppElement BankName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblBankNameKA"));
//		sa.assertEquals(BankName.getText(), "Bank Name");
//		AppElement BankNametxtBox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_flxBankNameValue"));
//		String name =BankNametxtBox.getText();
//		sa.assertEquals(name,"Kony Bank");
//
//		AppElement AccountType=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblAccountTypeKA"));
//		sa.assertEquals(AccountType.getText(), "Account Type");
//		AppElement AccountTypebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblAccountTypeKA"));
//		sa.assertEquals(AccountTypebox.isElementVisible(), true);
//
//		AppElement AccountNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblAccountNumberKA"));
//		sa.assertEquals(AccountNumber.getText(), "Account Number");
//		AppElement AccountNumberBox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberKA"));
//		sa.assertEquals(AccountNumberBox.isElementVisible(), true);
//
//		AppElement ReAccountNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblAccountNumberAgainKA"));
//		sa.assertEquals(ReAccountNumber.getText(), "Re-Enter Account Number");
//		AppElement ReAccountNumberbox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberAgainKA"));
//		sa.assertEquals(ReAccountNumberbox.isElementVisible(), true);
//
//		AppElement BeneficiaryNamelbl=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblBeneficiaryNameKA"));
//		sa.assertEquals(BeneficiaryNamelbl.getText(), "Beneficiary Name");
//		AppElement BeneficiaryNamebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxBeneficiaryNameKA"));
//		sa.assertEquals(BeneficiaryNamebox.isElementVisible(), true);
//
//		AppElement AccountNamelbl=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblAccountNickNameKA"));
//		sa.assertEquals(AccountNamelbl.getText(), "Account Nickname");
//		AppElement AccountNamebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNickNameKA"));
//		sa.assertEquals(AccountNamebox.isElementVisible(), true);
//
//		AppElement Cancelbutton=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnCancelKA"));
//		sa.assertEquals(Cancelbutton.isElementVisible(), true);
//
//		AppElement AddAccountButton=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnAddAccountKA"));
//		sa.assertEquals(AddAccountButton.isElementVisible(), true);
//		sa.assertAll();
//
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify fields default value validation")
//	public void verifyFieldsDefaultValues() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		List<String> ExpectedAccount = Arrays.asList("Savings", "Checking");
//		AppElement bankNameValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblBankNameValue"));
//		sa.assertTrue(bankNameValue.isEnabled()); 
//		sa.assertEquals(bankNameValue.getText(), "Kony Bank");
//		List<String> Actualtype = AppSpecificFunctions.getDropDownValues(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lbxAccountTypeKA"), "id");
//		sa.assertEquals(Actualtype, ExpectedAccount);
//		AppElement accountBeneficiaryValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxBeneficiaryNameKA"));
//		sa.assertEquals(accountBeneficiaryValue.getText(), "");		
//		AppElement accountNickNameValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNickNameKA"));
//		sa.assertEquals(accountNickNameValue.getText(), "");
//		sa.assertAll();		
//
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify add account button enabled only when all mandatory fields entered")
//	public void verifyAddAccountEnabled() throws Exception {
//
//		SoftAssert sa = new SoftAssert();
//
//		AppElement addAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnAddAccountKA"));
//		sa.assertFalse(addAccount.isEnabled());
//		addinternalacount.enterAccountNumber("213123176");
//		sa.assertFalse(addAccount.isEnabled());
//		addinternalacount.reEnterAccountNumber("213123176");
//		sa.assertFalse(addAccount.isEnabled());
//		addinternalacount.enterBeneficiaryName("Sam");
//		sa.assertTrue(addAccount.isEnabled());		
//		sa.assertAll();
//
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify user is redirected to the Transfer screen once cancel button clicked")
//	public void verifyTransferFormLoaded() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		addinternalacount.clickOnCancel();		
//		sa.assertTrue(frmTransfers.isTransfersForm());
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify Transfer screen is loaded after clicking on cancel on confirmation page")
//	public void verifyTransferFormLoadedOnCancelClickOfConfirmTransfer() throws Exception {
//		SoftAssert sa = new SoftAssert();		
//		addinternalacount.enterAccountNumber("213123190");
//		addinternalacount.reEnterAccountNumber("213123190");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnCancel();
//		AppElement lblPopupMessage=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_CustomPopupCancel_lblPopupMessage"));
//		sa.assertEquals(lblPopupMessage.getText(), "Are you sure you want to undo the process of  �Adding an account�?");
//		frmConfirmAccount.clickOnYesOnCancel();
//		sa.assertTrue(frmTransfers.isTransfersForm());
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Exception
//	 */
//	@Test(description = "Verify Add Recipient screen is loaded after clicking on modify button on confirmation page")
//	public void verifyAddRecipientFormLoadedOnModifyClickOfConfirmTransfer() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		addinternalacount.enterAccountNumber("213123187");
//		addinternalacount.reEnterAccountNumber("213123187");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnModify();
//		sa.assertTrue(frmAddInternalAccount.isfrmAddInternalAccount());
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Exception
//	 */
//	@Test(description = "Verify transfers screen is loaded after clicking on make transfer")
//	public void ClickOnMakeTransfer() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		//header.clickOnMenubar();
//		//header.clickOnTransfersMenuItem();
//		//header.clickOnAddKonyAccounts();
//		addinternalacount.enterAccountNumber("213123167");
//		addinternalacount.reEnterAccountNumber("213123167");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		frmVerifyAccount.clickOnMakeTransfer();
//		sa.assertTrue(frmTransfers.isTransfersForm());
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Exception
//	 */
//	@Test(description = "Verify Add Recipient screen is loaded after clicking on cancel on confirmation page")
//	public void ClickOnAddAnotherAccount() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		//header.clickOnMenubar();
//		//header.clickOnTransfersMenuItem();
//		//header.clickOnAddKonyAccounts();
//		addinternalacount.enterAccountNumber("21312344");
//		addinternalacount.reEnterAccountNumber("21312344");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		frmVerifyAccount.clickOnAddAnotherAccount();
//		sa.assertTrue(frmAddInternalAccount.isfrmAddInternalAccount());
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify Add Recipient screen is loaded after clicking on cancel on confirmation page")
//	public void VerifyErrorOnAccountNumberMismatch() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		addinternalacount.enterAccountNumber("21323121");
//		addinternalacount.reEnterAccountNumber("213123121");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		AppElement lblWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblWarning"));
//		sa.assertEquals(lblWarning.getText(), "The Account Numbers entered do not match.");
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kashav1
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify Add Recipient screen is loaded after clicking on cancel on confirmation page")
//	public void VerifyErrorOnAddingDuplicateAccountNumber() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		addinternalacount.enterAccountNumber("119911");
//		addinternalacount.reEnterAccountNumber("119911");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		frmVerifyAccount.clickOnAddAnotherAccount();
//		sa.assertTrue(test.OnlineBanking.forms.frmAddInternalAccount.isfrmAddInternalAccount());
//		addinternalacount.enterAccountNumber("119911");
//		addinternalacount.reEnterAccountNumber("119911");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		sa.assertTrue(frmAddInternalAccount.isfrmAddInternalAccount());
//		AppElement lblWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_rtxDowntimeWarning"));
//		sa.assertEquals(lblWarning.getText(), "Account Number already exists in your Payee List");
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	} 
//
//	/*
//	 * @author kadamm
//	 * @throws Excepti
//	 */
//	@Test(description = "Verify recipient is added successfully")
//	public void verifyAddRecipientSuccess() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		frmVerifyAccount=new frmVerifyAccount();
//		addinternalacount.selectAccountType("Savings");
//		addinternalacount.enterAccountNumber("21312311234156");
//		addinternalacount.reEnterAccountNumber("21312311234156");
//		addinternalacount.enterBeneficiaryName("Sam");
//		addinternalacount.enterAccountNickName("John");
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		AppElement BeneficiaryNameValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_lblBeneficiaryNameValue"));
//		sa.assertEquals(BeneficiaryNameValue.getText(), "John");
//		AppElement lblTransactionMessage=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_acknowledgment_lblTransactionMessage"));
//		sa.assertEquals(lblTransactionMessage, "'"+BeneficiaryNameValue.getText()+"'"+"has been Added Successfully");
//		AppElement AccountNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_lblAccountNumberValue"));
//		sa.assertEquals(AccountNumber.getText(), "21312311234156");
//		AppElement AccountType=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_lblAccountTypeValue"));
//		sa.assertEquals(AccountType.getText(), "Savings");
//		header.clickOnAccounts();
//		sa.assertAll();		 
//	}
//
//	/*
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Verify added recipient details")
//	public void verifyAddRecipientDetails() throws Exception {
//		frmExternalAccounts externalaccounts = new frmExternalAccounts();
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		frmVerifyAccount=new frmVerifyAccount();
//		addinternalacount.selectAccountType("Savings");
//		String account = AppSpecificFunctions.getDynamicNumber();
//		addinternalacount.enterAccountNumber(account);
//		addinternalacount.reEnterAccountNumber(account);
//		String name = AppSpecificFunctions.getDynamicName();
//		addinternalacount.enterBeneficiaryName(name);
//		addinternalacount.enterAccountNickName(name);
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		header.clickOnMenubar();
//		header.clickOnExternalAccounts();
//		externalaccounts.clickOnSearchLogo();
//		externalaccounts.enterSearchRecipientName(name);
//		externalaccounts.clickOnSearchButton();
//		externalaccounts.viewAccountDetails();
//		AppElement AccountName=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxAccountName"));
//		sa.assertEquals(AccountName.getText(), name);
//		AppElement BankName=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxBankName"));
//		sa.assertEquals(BankName.getText(), "Kony Bank");
//
//		AppElement lblAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountNumberTitle"));
//		sa.assertEquals(lblAccount.getText(), "Account Number");
//		AppElement AccountValue=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxAccountNumberValue"));
//		sa.assertEquals(AccountValue.getText(), account);
//		AppElement lblAccountType=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountTypeTitle"));
//		sa.assertEquals(lblAccountType.getText(), "Account Type");
//
//		AppElement AccountTypeValue=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountTypeValue"));
//		sa.assertEquals(AccountTypeValue.getText(), "Savings");
//
//		AppElement lblDate=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAddedOnTitle"));
//		sa.assertEquals(lblDate.getText(), "Added On");
//
//		AppElement DateValue=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAddedOnValue"));
//
//		sa.assertEquals(DateValue.getText(), AppSpecificFunctions.getCurrentDate("MM/dd/YYYY"));
//
//		AppElement lblRoutingNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblRoutingNumberTitle"));
//		sa.assertEquals(lblRoutingNumber.getText(), "Routing Number");
//
//		AppElement RoutingNumberValue=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblRoutingNumberValue"));
//		sa.assertEquals(RoutingNumberValue.getText(), "N/A");
//
//	}
//	
//	/*
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Verify recipient is deleted successfully ")
//	public void verifyDeletedRecipientNotAvailableInTheList() throws Exception {
//		frmExternalAccounts externalaccounts = new frmExternalAccounts();
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		frmVerifyAccount=new frmVerifyAccount();
//		addinternalacount.selectAccountType("Savings");
//		String account = AppSpecificFunctions.getDynamicNumber();
//		addinternalacount.enterAccountNumber(account);
//		addinternalacount.reEnterAccountNumber(account);
//		String name = AppSpecificFunctions.getDynamicName();
//		addinternalacount.enterBeneficiaryName(name);
//		addinternalacount.enterAccountNickName(name);
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		header.clickOnMenubar();
//		header.clickOnExternalAccounts();
//		externalaccounts.clickOnSearchLogo();
//		externalaccounts.enterSearchRecipientName(name);
//		externalaccounts.clickOnSearchButton();
//		externalaccounts.viewAccountDetails();
//		AppElement AccountName=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxAccountName"));
//		sa.assertEquals(AccountName.getText(), name);
//		transfer.clickOnDeleteRecipient();
//		transfer.clickOnRecipientDeletePopupYes();
//		AppSpecificFunctions.clearInputTextBox(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_Search_txtSearch"));
//		externalaccounts.enterSearchRecipientName(name);
//		externalaccounts.clickOnSearchButton();
//		AppElement msg = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_rtxNoPaymentMessage"));
//		sa.assertEquals(msg.getText(), "No Recipients Found.");
//		sa.assertAll();
//	}
//	
//	/*
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Verify recipient name is edited successfully ")
//	public void verifyRecipientNameCanBeEditedSuccessfully() throws Exception {
//		frmExternalAccounts externalaccounts = new frmExternalAccounts();
//		SoftAssert sa = new SoftAssert();
//		frmConfirmAccount=new frmConfirmAccount();
//		frmVerifyAccount=new frmVerifyAccount();
//		addinternalacount.selectAccountType("Savings");
//		String account = AppSpecificFunctions.getDynamicNumber();
//		addinternalacount.enterAccountNumber(account);
//		addinternalacount.reEnterAccountNumber(account);
//		String name = AppSpecificFunctions.getDynamicName();
//		addinternalacount.enterBeneficiaryName(name);
//		addinternalacount.enterAccountNickName(name);
//		addinternalacount.clickAddAccount();
//		frmConfirmAccount.clickOnConfirmAccount();
//		header.clickOnMenubar();
//		header.clickOnExternalAccounts();
//		externalaccounts.clickOnSearchLogo();
//		externalaccounts.enterSearchRecipientName(name);
//		externalaccounts.clickOnSearchButton();
//		externalaccounts.viewAccountDetails();
//		AppElement AccountName=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxAccountName"));
//		sa.assertEquals(AccountName.getText(), name);
//		transfer.editRecipient();
//		String Newname = AppSpecificFunctions.getDynamicName();
//		transfer.enterAccountName(Newname);
//		AppElement SaveButton=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersEdit_btnSave"));
//		SaveButton.click();
//		externalaccounts.enterSearchRecipientName(Newname);
//		externalaccounts.clickOnSearchButton();
//		externalaccounts.viewAccountDetails();
//		AppElement AccountName1=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_flxAccountName"));
//		sa.assertEquals(AccountName1.getText(), Newname);
//		sa.assertAll();
//	}
//}
