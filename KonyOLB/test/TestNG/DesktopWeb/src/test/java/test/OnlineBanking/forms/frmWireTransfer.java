package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmWireTransfer {


  public frmWireTransfer() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_frmWireTransfer"));
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_btnCancel"));
  btnCancel.click();
  }
public void btnMakeAnotherWireTransfer() throws Exception{ 
  AppElement btnMakeAnotherWireTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_btnMakeAnotherWireTransfer"));
  btnMakeAnotherWireTransfer.click();
  }
public void btnSave() throws Exception{ 
  AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_btnSave"));
  btnSave.click();
  }
public void btnSaveRecipient() throws Exception{ 
  AppElement btnSaveRecipient=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_btnSaveRecipient"));
  btnSaveRecipient.click();
  }
public void btnViewSentTransactions() throws Exception{ 
  AppElement btnViewSentTransactions=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_btnViewSentTransactions"));
  btnViewSentTransactions.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }
public void rtxTC(String text) throws Exception{
  AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmWireTransfer_rtxTC"));
  rtxTC.type(text);
  }



}