package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;

import java.io.IOException;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmCustomHeaderForm {

	WebElement ele;
	/** Logger initialization */
	static AppLogger appLogger = AppLogger.getInstance();

	public WebElement findElement(String id) throws Exception {
		Driver driver = Driver.getInstance();
		ele = driver.getAppy().findElement(By.cssSelector("[id$=" + id + "]"));
		appLogger.logInfo("Finding element", id);
		Thread.sleep(5000);
		return ele;
	}

	public void clickOnMenubar() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_topmenu_flxMenu"));
		ele.click();
		appLogger.logInfo("clicked on the Menu button");
	}

	public void clickOnTransfersMenuItem() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_flxTransfers"));
		ele.click();
		appLogger.logInfo("clicked on the Transfer Menu button");
	}

	public void clickOnAddKonyAccounts() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_lblAddKonyAccounts"));
		ele.click();
		appLogger.logInfo("clicked on the AddKonyButton button");
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_lblAddNonKonyAcc"));
	}

	public void clickOnAddNonKonyAccounts() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_lblAddNonKonyAccounts"));
		ele.click();
		appLogger.logInfo("clicked on the Add Non Kony button");
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_lblAddNonKonyAcc"));
	}

	public void clickOnCloseButton() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_imgClose_span"));
		ele.click();
		appLogger.logInfo("clicked on the Close button");
		// AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
	}

	public void clickOnAccounts() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_topmenu_flxaccounts"));
		ele.click();
		appLogger.logInfo("clicked on the Accounts option on header");
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
	}

	public void clickOnExternalAccounts() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_flxExternalAccounts"));
		ele.click();
		appLogger.logInfo("clicked External Accounts button from menu");
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_frmTransfers"));
	}

	public void clickOnTransferMoney() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_customhamburger_flxTransfersMoney"));
		ele.click();
		appLogger.logInfo("clicked on the Transfer money option");
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_frmTransfers"));
	}
	
	public void clickOnLogOut() {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		try {
			Thread.sleep(5000);
			ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_btnLogOut"));
			ele.click();
			appLogger.logInfo("clicked on the Logout buttonn");
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmLoginKA_Header"));
		} catch (Exception e) {
			appLogger.logError("Logout button not found");
			e.printStackTrace();
		}

	}
	
	public void clickOnSettings() throws Exception {
		frmCustomHeaderForm form = new frmCustomHeaderForm();
		Thread.sleep(5000);
		ele = form.findElement(OnlineBankingWidgetId.getWidgetId("customheader_headermenu_imgDropdown"));
		ele.click();
		appLogger.logInfo("clicked on the Setting option on header");
		Thread.sleep(2000);
	}
	
	public void clickOnAccountSettings() {
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flxSegUserActions_flxSegUserActions"));
			AppElement AccountSetting = new AppElement(OnlineBankingWidgetId.getWidgetId("flxSegUserActions_flxSegUserActions"),1);
			AccountSetting.click();
		} catch (Exception e) {
			appLogger.logInfo("Account Setting option not found in the Setting option");
			e.printStackTrace();
		}
		
	}
}