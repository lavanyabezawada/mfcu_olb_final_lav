package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmNewUserOnboarding {


  public frmNewUserOnboarding() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_frmNewUserOnboarding"));
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_btnCancel"));
  btnCancel.click();
  }
public void btnContinueProceed() throws Exception{ 
  AppElement btnContinueProceed=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_btnContinueProceed"));
  btnContinueProceed.click();
  }
public void btnSave() throws Exception{ 
  AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_btnSave"));
  btnSave.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }
public void rtxTC(String text) throws Exception{
  AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_rtxTC"));
  rtxTC.type(text);
  }
public void rtxWelcomeMessage(String text) throws Exception{
  AppElement rtxWelcomeMessage=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_rtxWelcomeMessage"));
  rtxWelcomeMessage.type(text);
  }


public void segProductsNUO(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_segProductsNUO"),OnlineBankingWidgetId.getWidgetId("frmNewUserOnboarding_btnKnowMore"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}