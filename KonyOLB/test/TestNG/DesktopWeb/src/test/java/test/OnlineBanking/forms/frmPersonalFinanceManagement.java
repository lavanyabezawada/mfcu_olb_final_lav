package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmPersonalFinanceManagement {


  public frmPersonalFinanceManagement() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPersonalFinanceManagement_frmPersonalFinanceManagement"));
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPersonalFinanceManagement_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }


public void segAccounts(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmPersonalFinanceManagement_segAccounts"),OnlineBankingWidgetId.getWidgetId("frmPersonalFinanceManagement_flPFMAccountListItemWrapper"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}