package test.OnlineBanking.forms;

import test.common.AppElement;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAddExternalAccount {

  
  public static boolean isfrmAddExternalAccount() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_frmAddExternalAccount"));
	    return visible;
	}

  public String getBankName() throws Exception{
	  AppElement BankName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblBankNameValue"));
	  return BankName.getText();
	  }
  

  public void selectAccountType(String accountType) throws Exception{
	  WebElement BankName=(WebElement) new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lbxAccountTypeKA"));
	  Select select = new Select(BankName);
	  select.selectByVisibleText(accountType);

	  }
  
  public void enterAccountNumber(String text) throws Exception{
	  AppElement AccNum=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberKA"));
	  AccNum.type(text);
	  }
  
  public void reEnterAccountNumber(String text) throws Exception{
	  AppElement AccNum=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberAgainKA"));
	  AccNum.type(text);
	  }
  
  public void enterBeneficiaryName(String text) throws Exception{
	  AppElement name=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxBeneficiaryNameKA"));
	  name.type(text);
	  }
  
  public void enterAccountNickName(String text) throws Exception{
	  AppElement name=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNickNameKA"));
	  name.type(text);
	  }
  
  public void clickAddAccount() throws Exception {
		AppElement AddAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnAddAccountKA"));
		AddAccount.click();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
	}

  public void clickOnCancel() throws Exception {
		AppElement AddAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnCancelKA"));
		AddAccount.click();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
	}
  
  public void navigateTofrmAddExternalAccount() throws Exception {
		
	  frmCustomHeaderForm header=new frmCustomHeaderForm();
	  header.clickOnMenubar();
	  header.clickOnTransfersMenuItem();
	  header.clickOnAddNonKonyAccounts();

	}
  
  public void getDropDownValues(String id) throws Exception{
	  AppElement accountTypeValue=new AppElement(OnlineBankingWidgetId.getWidgetId(id));
	  String value = accountTypeValue.getText();
	  WebElement BankName=(WebElement) new AppElement(OnlineBankingWidgetId.getWidgetId(id));
	  String value2= BankName.getAttribute("Option");

	  }


}