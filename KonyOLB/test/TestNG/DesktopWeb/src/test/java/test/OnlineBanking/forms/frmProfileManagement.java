package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmProfileManagement {

	/** Logger initialization */
	static AppLogger appLogger = AppLogger.getInstance();


	public void btnCancel() throws Exception{ 
		AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_btnCancel"));
		btnCancel.click();
	}
	public void btnDeleteNo() throws Exception{ 
		AppElement btnDeleteNo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_btnDeleteNo"));
		btnDeleteNo.click();
	}
	public void btnDeleteYes() throws Exception{ 
		AppElement btnDeleteYes=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_btnDeleteYes"));
		btnDeleteYes.click();
	}
	public void btnSave() throws Exception{ 
		AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_btnSave"));
		btnSave.click();
	}


	public void rtxDowntimeWarning(String text) throws Exception{
		AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_rtxDowntimeWarning"));
		rtxDowntimeWarning.type(text);
	}
	public void rtxTC(String text) throws Exception{
		AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_rtxTC"));
		rtxTC.type(text);
	}

	public void clickOnDefaultAccount(){ 
		AppElement btnSave = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_flxSetDefaultAccount"));
			btnSave = new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_flxSetDefaultAccount"));
		}catch (Exception e) {
			appLogger.logInfo("Set Default Account button does not found on profile management page");
			e.printStackTrace();
		}
		btnSave.click();
	}

	public void clickOnEditButton(){ 
		AppElement editButton = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_btnDefaultTransactionAccountEdit"));
			editButton = new AppElement(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_btnDefaultTransactionAccountEdit"));
		}catch (Exception e) {
			appLogger.logInfo("Edit button does not found on profile management page");
			e.printStackTrace();
		}
		editButton.click();
	}

	/**
	 * Select 'transfer' account as per the provided index value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public void selectTransferAccountByIndex(int account)
	{
		WebElement Account;
		Select selectAccount=null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_lbxTransfers"));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_lbxTransfers")));
			//Account.click();
			selectAccount =  new Select(Account);
			selectAccount.selectByIndex(account);
			Thread.sleep(3000);
		} catch (Exception e) {
			appLogger.logError("'Transfer' drop down field not found, inside the profile managemenet window");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
	}
	
	public String getSelectedDropDownValue()
	{
		WebElement Account;
		Select selectAccount=null;
		String AccountName= null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_lbxTransfers"));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmProfileManagement_settings_lbxTransfers")));
			//Account.click();
			selectAccount =  new Select(Account);
			WebElement Accountvalue = selectAccount.getFirstSelectedOption();
			AccountName= Accountvalue.getText();
		} catch (Exception e) {
			appLogger.logError("'Transfer' drop down field not found, inside the profile managemenet window");
			e.printStackTrace();
		}
		return AccountName;
	}
}