package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmStopPayments {


  public frmStopPayments() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_frmStopPayments"));
  }
public void btnAddAnotherAccount() throws Exception{ 
  AppElement btnAddAnotherAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnAddAnotherAccount"));
  btnAddAnotherAccount.click();
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnCancel"));
  btnCancel.click();
  }
public void btnMakeTransfer() throws Exception{ 
  AppElement btnMakeTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnMakeTransfer"));
  btnMakeTransfer.click();
  }
public void btnSave() throws Exception{ 
  AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnSave"));
  btnSave.click();
  }
public void btnViewDisputedChecks() throws Exception{ 
  AppElement btnViewDisputedChecks=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnViewDisputedChecks"));
  btnViewDisputedChecks.click();
  }
public void btnViewDisputedTransactions() throws Exception{ 
  AppElement btnViewDisputedTransactions=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_btnViewDisputedTransactions"));
  btnViewDisputedTransactions.click();
  }


public void rtxTC(String text) throws Exception{
  AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmStopPayments_rtxTC"));
  rtxTC.type(text);
  }



}