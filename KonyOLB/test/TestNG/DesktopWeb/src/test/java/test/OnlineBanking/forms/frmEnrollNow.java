package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmEnrollNow {


  public frmEnrollNow() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_frmEnrollNow"));
  }
public void btnContactUs() throws Exception{ 
  AppElement btnContactUs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_btnContactUs"));
  btnContactUs.click();
  }
public void btnFaqs() throws Exception{ 
  AppElement btnFaqs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_btnFaqs"));
  btnFaqs.click();
  }
public void btnLocateUs() throws Exception{ 
  AppElement btnLocateUs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_btnLocateUs"));
  btnLocateUs.click();
  }
public void btnPrivacy() throws Exception{ 
  AppElement btnPrivacy=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_btnPrivacy"));
  btnPrivacy.click();
  }
public void btnTermsAndConditions() throws Exception{ 
  AppElement btnTermsAndConditions=new AppElement(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_btnTermsAndConditions"));
  btnTermsAndConditions.click();
  }




public void segLanguagesList(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmEnrollNow_segLanguagesList"),OnlineBankingWidgetId.getWidgetId("frmEnrollNow_flxLangList"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}