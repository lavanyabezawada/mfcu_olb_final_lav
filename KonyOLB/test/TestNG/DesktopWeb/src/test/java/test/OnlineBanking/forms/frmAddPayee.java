package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAddPayee {


  public frmAddPayee() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_frmAddPayee"));
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnCancel"));
  btnCancel.click();
  }
public void btnConfirm() throws Exception{ 
  AppElement btnConfirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnConfirm"));
  btnConfirm.click();
  }
public void btnDetailsBack() throws Exception{ 
  AppElement btnDetailsBack=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnDetailsBack"));
  btnDetailsBack.click();
  }
public void btnDetailsCancel() throws Exception{ 
  AppElement btnDetailsCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnDetailsCancel"));
  btnDetailsCancel.click();
  }
public void btnDetailsConfirm() throws Exception{ 
  AppElement btnDetailsConfirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnDetailsConfirm"));
  btnDetailsConfirm.click();
  }
public void btnDummy() throws Exception{ 
  AppElement btnDummy=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnDummy"));
  btnDummy.click();
  }
public void btnEnterManually() throws Exception{ 
  AppElement btnEnterManually=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnEnterManually"));
  btnEnterManually.click();
  }
public void btnEnterPayeeInfo() throws Exception{ 
  AppElement btnEnterPayeeInfo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnEnterPayeeInfo"));
  btnEnterPayeeInfo.click();
  }
public void btnLeft() throws Exception{ 
  AppElement btnLeft=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnLeft"));
  btnLeft.click();
  }
public void btnMakeBillPay() throws Exception{ 
  AppElement btnMakeBillPay=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnMakeBillPay"));
  btnMakeBillPay.click();
  }
public void btnModify() throws Exception{ 
  AppElement btnModify=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnModify"));
  btnModify.click();
  }
public void btnNext() throws Exception{ 
  AppElement btnNext=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnNext"));
  btnNext.click();
  }
public void btnNext2() throws Exception{ 
  AppElement btnNext2=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnNext2"));
  btnNext2.click();
  }
public void btnResetPayeeInfo() throws Exception{ 
  AppElement btnResetPayeeInfo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnResetPayeeInfo"));
  btnResetPayeeInfo.click();
  }
public void btnResetPayeeInfo2() throws Exception{ 
  AppElement btnResetPayeeInfo2=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnResetPayeeInfo2"));
  btnResetPayeeInfo2.click();
  }
public void btnSearchPayee() throws Exception{ 
  AppElement btnSearchPayee=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnSearchPayee"));
  btnSearchPayee.click();
  }
public void btnSetDefaultBillPayAccnt() throws Exception{ 
  AppElement btnSetDefaultBillPayAccnt=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnSetDefaultBillPayAccnt"));
  btnSetDefaultBillPayAccnt.click();
  }
public void btnViewAllPayees() throws Exception{ 
  AppElement btnViewAllPayees=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnViewAllPayees"));
  btnViewAllPayees.click();
  }
public void btnViewPayActivities() throws Exception{ 
  AppElement btnViewPayActivities=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnViewPayActivities"));
  btnViewPayActivities.click();
  }

public void tbxAdditionalNote(String text) throws Exception{
  AppElement tbxAdditionalNote=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxAdditionalNote"));
  tbxAdditionalNote.type(text);
  }
public void tbxAddNameOnBillValue(String text) throws Exception{
  AppElement tbxAddNameOnBillValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxAddNameOnBillValue"));
  tbxAddNameOnBillValue.type(text);
  }
public void tbxAddNickValue(String text) throws Exception{
  AppElement tbxAddNickValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxAddNickValue"));
  tbxAddNickValue.type(text);
  }
public void tbxCity(String text) throws Exception{
  AppElement tbxCity=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxCity"));
  tbxCity.type(text);
  }
public void tbxConfirmAccNumber(String text) throws Exception{
  AppElement tbxConfirmAccNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxConfirmAccNumber"));
  tbxConfirmAccNumber.type(text);
  }
public void tbxCustomerName(String text) throws Exception{
  AppElement tbxCustomerName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxCustomerName"));
  tbxCustomerName.type(text);
  }
public void tbxEnterAccountNmber(String text) throws Exception{
  AppElement tbxEnterAccountNmber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxEnterAccountNmber"));
  tbxEnterAccountNmber.type(text);
  }
public void tbxEnterAddress(String text) throws Exception{
  AppElement tbxEnterAddress=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxEnterAddress"));
  tbxEnterAddress.type(text);
  }
public void tbxEnterAddressLine2(String text) throws Exception{
  AppElement tbxEnterAddressLine2=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxEnterAddressLine2"));
  tbxEnterAddressLine2.type(text);
  }
public void tbxEnterName(String text) throws Exception{
  AppElement tbxEnterName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxEnterName"));
  tbxEnterName.type(text);
  }
public void tbxEnterZipCode(String text) throws Exception{
  AppElement tbxEnterZipCode=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxEnterZipCode"));
  tbxEnterZipCode.type(text);
  }
public void tbxOne(String text) throws Exception{
  AppElement tbxOne=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxOne"));
  tbxOne.type(text);
  }
public void tbxThree(String text) throws Exception{
  AppElement tbxThree=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxThree"));
  tbxThree.type(text);
  }
public void tbxTwo(String text) throws Exception{
  AppElement tbxTwo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxTwo"));
  tbxTwo.type(text);
  }
public void tbxZipcode(String text) throws Exception{
  AppElement tbxZipcode=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_tbxZipcode"));
  tbxZipcode.type(text);
  }

public void rtxAddAddressValue(String text) throws Exception{
  AppElement rtxAddAddressValue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxAddAddressValue"));
  rtxAddAddressValue.type(text);
  }
public void rtxAdditionError(String text) throws Exception{
  AppElement rtxAdditionError=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxAdditionError"));
  rtxAdditionError.type(text);
  }
public void rtxBillerAddress(String text) throws Exception{
  AppElement rtxBillerAddress=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxBillerAddress"));
  rtxBillerAddress.type(text);
  }
public void rtxDetailsAddressVal(String text) throws Exception{
  AppElement rtxDetailsAddressVal=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxDetailsAddressVal"));
  rtxDetailsAddressVal.type(text);
  }
public void rtxMisMatch(String text) throws Exception{
  AppElement rtxMisMatch=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxMisMatch"));
  rtxMisMatch.type(text);
  }
public void rtxNoExactMatch(String text) throws Exception{
  AppElement rtxNoExactMatch=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddPayee_rtxNoExactMatch"));
  rtxNoExactMatch.type(text);
  }


public void segPayeesName(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmAddPayee_segPayeesName"),OnlineBankingWidgetId.getWidgetId("frmAddPayee_flxNewPayees"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 
public void segRegisteredPayees(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmAddPayee_segRegisteredPayees"),OnlineBankingWidgetId.getWidgetId("frmAddPayee_btnPayBills"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}