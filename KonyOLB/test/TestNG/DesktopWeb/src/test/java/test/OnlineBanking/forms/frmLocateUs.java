package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmLocateUs {


  public frmLocateUs() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLocateUs_frmLocateUs"));
  }
public void btnShareCancel() throws Exception{ 
  AppElement btnShareCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLocateUs_btnShareCancel"));
  btnShareCancel.click();
  }
public void btnShareSend() throws Exception{ 
  AppElement btnShareSend=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLocateUs_btnShareSend"));
  btnShareSend.click();
  }

public void tbxMapUrl(String text) throws Exception{
  AppElement tbxMapUrl=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLocateUs_tbxMapUrl"));
  tbxMapUrl.type(text);
  }
public void tbxSendMapTo(String text) throws Exception{
  AppElement tbxSendMapTo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLocateUs_tbxSendMapTo"));
  tbxSendMapTo.type(text);
  }




}