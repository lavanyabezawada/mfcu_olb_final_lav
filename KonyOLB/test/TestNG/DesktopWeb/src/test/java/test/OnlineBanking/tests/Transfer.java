//package test.OnlineBanking.tests;
//
//import java.util.Arrays;
//import java.util.List;
//import org.kony.qa.stargate.wrappers.appy.Driver;
//import org.openqa.selenium.JavascriptExecutor;
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;
//import test.OnlineBanking.AppSpecificFunctions;
//import test.OnlineBanking.OnlineBankingBaseTest;
//import test.OnlineBanking.OnlineBankingWidgetId;
//import test.OnlineBanking.forms.frmAccountsLanding;
//import test.OnlineBanking.forms.frmCustomHeaderForm;
//import test.OnlineBanking.forms.frmLogin;
//import test.OnlineBanking.forms.frmProfileManagement;
//import test.OnlineBanking.forms.frmTransferAcknowledgement;
//import test.OnlineBanking.forms.frmTransferConfirm;
//import test.OnlineBanking.forms.frmTransfers;
//import test.common.AppElement;
//import test.common.SgConfiguration;
//
//public class Transfer extends OnlineBankingBaseTest {
//	frmTransfers transfer = new frmTransfers();
//	frmLogin login = new frmLogin();
//	SgConfiguration sg = SgConfiguration.getInstance();
//	frmCustomHeaderForm header=new frmCustomHeaderForm();
//	frmTransferConfirm transferConfirm = new frmTransferConfirm();
//	frmTransferAcknowledgement transferacknowledgement= new frmTransferAcknowledgement();
//	frmProfileManagement profilemanagement = new frmProfileManagement();
//	
//	public static int index=0;
//	
//	@BeforeMethod
//	public void setupBeforeTest() throws Exception {
//		Driver.getInstance().getAppy().manage().window().maximize();
//		
//		if(!frmTransfers.isTransfersForm())
//		{
//			if(frmAccountsLanding.isLandingForm())
//				{
//				  transfer.navigateTofrmTransferMoney(); 
//				}
//				else if(frmLogin.isLoginForm())			{
//					login.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
//					transfer.navigateTofrmTransferMoney(); 
//			}
//		}
//	}
//	
//	@AfterMethod
//	public void actionAfterMethod()
//	{
//		try {
//			header.clickOnAccounts();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	@Test(description = "Verify available transfer type option underd Make transfer tab")
//	public void verifyTransferType() throws Exception {
//		
//		SoftAssert sa = new SoftAssert(); 
//		AppElement MyKonyAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_lblToMyKonyBank"));
//		sa.assertEquals(MyKonyAccount.getText(), "To my Kony Bank Accounts");
//		AppElement OtherKonyAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_lblToOtherKonyBank"));
//		sa.assertEquals(OtherKonyAccount.getText(), "To Other Kony Bank Members");
//		//header.clickOnAccounts();
//		
//	}
//	
//	@Test(description = "Verify available account display in the FROM drop down field.(Set consumerid= 1066)")
//	public void verifyFromAccountField() throws Exception {
//		//Below list is should be used when testing is performed with fiserv server.
//		//List<String> ExpectedInfo = Arrays.asList("7200116970", "7200116980", "7204840951", "7204957772");
//		
//		//Below list is should be used when testing is performed with fiserv DNA server.
//		List<String> ExpectedInfo = Arrays.asList("4358893", "5186650", "4358900");
//		transfer.clickOnMyKonyBankAccount();
//		SoftAssert sa = new SoftAssert(); 
//		AppElement Account = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount"));
//		Account.click();
//		List<String> ActualFromAccount = AppSpecificFunctions.dropDownAccount(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount"), "id");
//		sa.assertEquals(ActualFromAccount, ExpectedInfo);
//		//header.clickOnAccounts();
//		sa.assertAll();
//
//	}
//	
//	@Test(description = "Verify transfer amount field validation")
//	public void verifyAmountFieldValidation() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnMyKonyBankAccount();
//		transfer.enterAmount("abc");
//		transfer.clickOnNoteFiled();
//		transfer.clickOnContinueButton();
//		AppElement errorMSG = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lblWarning"));
//		sa.assertEquals(errorMSG.getText(), "Invalid Amount.");
//		//header.clickOnAccounts();
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify transfer amount field validation")
//	public void verifyAmountFieldValidationWithNegativeValue() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnMyKonyBankAccount();
//		AppSpecificFunctions.clearInputTextBox(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lblWarning"));
//		transfer.enterAmount("-10");
//		transfer.clickOnNoteFiled();
//		transfer.clickOnContinueButton();
//		AppElement errorMSG = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lblWarning"));
//		sa.assertEquals(errorMSG.getText(), "Minimum value required for this Transaction $1.00");
//		//header.clickOnAccounts();
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify the frequency option available")
//	public void verifyFrequencyOptions() throws Exception {
//		List<String> ExpectedInfo = Arrays.asList("Once", "Daily", "Weekly", "Every Two Weeks", "Monthly", "Quaterly", "Half Yearly", "Yearly");
//		transfer.clickOnMyKonyBankAccount();
//		SoftAssert sa = new SoftAssert(); 
//		AppElement frequency = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFrequency"));
//		frequency.click();
//		List<String> ActualFromAccount = AppSpecificFunctions.getDropDownValues(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFrequency"), "id");
//		sa.assertEquals(ActualFromAccount, ExpectedInfo);
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify 'Continue' button should be enabled only if mandatory field is selected.")
//	public void verifyContinueButtonOnMandatoryField() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		sa.assertFalse(frmTransfers.isContiueButtonEnabled());
//		transfer.selectFromAccountByIndex(0);
//		sa.assertFalse(frmTransfers.isContiueButtonEnabled());
//		transfer.selectToAccountByIndex(0);
//		sa.assertFalse(frmTransfers.isContiueButtonEnabled());
//		transfer.enterAmount("10.00");
//		transfer.clickOnNoteFiled();
//		sa.assertTrue(frmTransfers.isContiueButtonEnabled());
//		//header.clickOnAccounts();
//		sa.assertAll();
//		
//	}
//	
//	@Test(description = "Verify 'Make transfer' page display if user clicked on the cancel button on transfer screen")
//	public void verifyMakeTransferPageDisplayOnCancel() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.clickOnCanceButton();
//		 boolean makeTransfer = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_lblToMyKonyBank"));
//		sa.assertTrue(makeTransfer);
//		sa.assertAll();
//		//header.clickOnAccounts();
//		
//	}
//	
//	@Test(description = "Verify confirmation screen showing selected/entered field values while doing transfer transaction.")
//	public void VerifyConfirmationValue() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.selectFromAccountByIndex(0);
//		String ActualfromAccount = transfer.getSelectedDropDownValueText1("frmTransfers_transfermain_maketransfer_lbxFromAccount");
//		//JavascriptExecutor js1 = (JavascriptExecutor) Driver.getInstance().getAppy();
//		//Object FromAccount=js1.executeScript("return document.getElementById('frmTransfers_transfermain_maketransfer_lbxFromAccount').value");
//		//String ActualfromAccount = FromAccount.toString();
//		transfer.selectToAccountByIndex(0);
//		String ActualToAccount = transfer.getSelectedDropDownValueText2("frmTransfers_transfermain_maketransfer_lbxToAccount");
//		//Object ToAccount=js1.executeScript("return document.getElementById('frmTransfers_transfermain_maketransfer_lbxToAccount').value");
//		//String ActualToAccount = ToAccount.toString();
//		transfer.enterAmount("10.00");
//		transfer.enterNote("For Home Loan");
//		transfer.clickOnContinueButton();
//		sa.assertEquals(transferConfirm.returnFromAccountValue(), sg.getKeyValue(ActualfromAccount));
//		sa.assertEquals(transferConfirm.returnToAccountValue(), sg.getKeyValue(ActualToAccount));
//		sa.assertEquals(transferConfirm.returnAmountValue(), "10.00");
//		sa.assertEquals(transferConfirm.returnNoteValue(), "For Home Loan");
//		sa.assertEquals(transferConfirm.returnDateValue(), AppSpecificFunctions.getCurrentDate("MM/dd/yyyy"));
//		//header.clickOnAccounts();
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify available recipient in the 'To' field with transfer type 'To My kony Bank Accounts'.")
//	public void verifyRecipient() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		header.clickOnMenubar();
//		header.clickOnExternalAccounts();
//		List<String> Expectedrecipient  = AppSpecificFunctions.getExternalAccount("Kony Bank");
//		header.clickOnMenubar();
//		header.clickOnTransferMoney();
//		transfer.clickOnMyKonyBankAccount();
//		List<String> ActualToAccount = AppSpecificFunctions.dropDownAccount(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount"), "id");
//		sa.assertEquals(ActualToAccount, Expectedrecipient);
//	}
//	
//	@Test(description = "Verify Transfer screen display once user click on the modify option on confirm transfer.")
//	public void VerifyTransferScreenDisplayOnModify() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.selectFromAccountByIndex(0);
//		transfer.selectToAccountByIndex(0);
//		transfer.enterAmount("10.00");
//		transfer.enterNote("Home Loan");
//		transfer.clickOnContinueButton();
//		transferConfirm.clickOnModifyButton();
//		boolean transferoption =frmTransfers.isMakeTransfersOption();
//		sa.assertTrue(transferoption);
//		//header.clickOnAccounts();
//	}
//	
//	@Test(description = "Verify Transfer screen display once user click on the Cancel button on confirm transfer.")
//	public void VerifyTransferScreenDisplayOnCancel() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.selectFromAccountByIndex(0);
//		transfer.selectToAccountByIndex(0);
//		transfer.enterAmount("10.00");
//		transfer.enterNote("Home Loan");
//		transfer.clickOnContinueButton();
//		transferConfirm.clickOnCancelButton();
//		transferConfirm.clickOnQuiteTransferYes();
//		boolean transferForm =frmTransfers.isTransfersForm();
//		sa.assertTrue(transferForm);
//		//header.clickOnAccounts();
//	}
//
//	@Test(description = "Verify Transaction entry listed in the Recent transaction tab.")
//	public void VerifySuccessfulTransferTransaction() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.selectFromAccountByIndex(0);
//		transfer.selectToAccountByIndex(0);
//		transfer.enterAmount("10.00");
//		transfer.enterNote("Home Loan");
//		transfer.clickOnContinueButton();
//		transferConfirm.clickOnConfirmButton();
//		sa.assertEquals(transferacknowledgement.getSuccessMsg(), "Your Transaction has been done Successfully");
//		String FromAccount =transferacknowledgement.returnFromAccountValue();
//		String ToAccount = transferacknowledgement.returnToAccountValue();
//		String amount = transferacknowledgement.returnAmountValue();
//		String dateValue = transferacknowledgement.returnDateValue();
//		String note = transferacknowledgement.returnNoteValue();
//		transferacknowledgement.clickOnViewTransferActivity();
//		AppElement transaction = new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfers_flxDropdown"),0);
//		transaction.click();
//		sa.assertEquals(transfer.returnFromAccountValue(), FromAccount);
//		sa.assertEquals(transfer.returnToAccountValue(), ToAccount);
//		sa.assertEquals(transfer.returnAmountValue(), amount);
//		sa.assertEquals(transfer.returnDateValue(), dateValue);
//		sa.assertEquals(transfer.returnNoteValue(), note);
//		sa.assertEquals(transfer.returnStatus(), "successful");
//		header.clickOnAccounts();
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify transfer transaction with amount more than $10,000.")
//	public void VerifyTransactionAmountLimit() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		transfer.clickOnMyKonyBankAccount();
//		transfer.selectFromAccountByIndex(0);
//		transfer.selectToAccountByIndex(0);
//		transfer.enterAmount("10.00");
//		transfer.enterNote("Home Loan");
//		transfer.clickOnContinueButton();
//		AppElement errorMSG = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lblWarning"));
//		sa.assertEquals(errorMSG.getText(), "The value for this Transaction cannot exceed $10,000.00");
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify the 'Previous' arrow is disabled where transaction list starts")
//	public void verifyPreviousArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnRecentTab();
//		AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//		sa.assertEquals(PreviousArrow.isEnabled(), false);
//	}
//	
//	@Test(description = "Verify pagination buttons if transaction count is less than the 10.")
//	public void verifyPreviousAndNextArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnRecentTab();
//		int recordcount = transfer.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxRecentTransfers_flxRow"));
//		if(recordcount < 10)
//		{
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), false);
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}else {
//
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), false);
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}
//		
//	}
//
//	
//	@Test(description = "Verify next arrow button when transaction count is more than 10.")
//	public void verifyNextArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnRecentTab();
//		int recordcount = transfer.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxRecentTransfers_flxRow"));
//		if(recordcount > 10)
//		{
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}else {
//			
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}
//	}
//	
//	@Test(description = "Verify 'Previous' and 'Next' arrows are available at the bottom of \"frmTransfer\" screen to view multiple transaction entries.")
//	public void verifyNextRecord() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnRecentTab();
//		int recordcount = transfer.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxRecentTransfers_flxRow"));
//		if(recordcount > 10)
//		{
//			transfer.clickOnNextArrow();
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), true);
//			
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}else {
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}
//	}
//	
//	@Test(description = "Verify the 'Next' arrow is disabled where transaction list ends.")
//	public void verifyNextArrowDisabled() throws Exception 
//	{
//		boolean value=true;
//		int recordcount=0;
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnRecentTab();
//			while(value==true)
//			{
//				recordcount = transfer.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxRecentTransfers_flxRow"));
//				if(recordcount < 10){
//					AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//					sa.assertEquals(NextArrow.isEnabled(), false);
//					value=false;
//				}else
//					{
//					 transfer.clickOnNextArrow();
//					}	
//			}
//	}
//	
//	@Test(description = "Verify default transaction account for transfer.")
//	public void verifyDefaultSelectAccountForTransferTransaction() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert(); 
//		header.clickOnSettings();
//		header.clickOnAccountSettings();
//		profilemanagement.clickOnDefaultAccount();
//		profilemanagement.clickOnEditButton();
//		profilemanagement.selectTransferAccountByIndex(0);
//		String defaultAccount = profilemanagement.getSelectedDropDownValue();
//		profilemanagement.clickOnEditButton();
//		header.clickOnMenubar();
//		header.clickOnTransfersMenuItem();
//		header.clickOnTransferMoney();
//		transfer.clickOnMyKonyBankAccount();
//		String accountNumber= transfer.getSelectedDropDownValueText();
//		sa.assertEquals(defaultAccount, accountNumber);
//		sa.assertAll();
//	}
//
//}
