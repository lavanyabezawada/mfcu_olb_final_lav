package test.OnlineBanking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.kony.qa.stargate.wrappers.appy.SgElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
//import com.github.javafaker.Faker;

import test.common.AppElement;
import test.common.AppLogger;
import test.common.SgConfiguration;

public class AppSpecificFunctions {
	

	static SgConfiguration sg = SgConfiguration.getInstance();
    static AppLogger appLogger = AppLogger.getInstance();
    
	
	public static void waitForClientSessionTimeout() throws Exception {
		int TimeToSleep = (Integer.parseInt(sg.getKeyValue("clientSessionTimeOut")) * 60 * 1000)+1;
		Thread.sleep(TimeToSleep);
	}

	
	  /**@author kadamm, This method is useful to clear text field by provind element. */
	
    public static void clearInputTextBox(String element) {
		try{
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId(element));
			WebElement ele = Driver.getInstance().getAppy().findElement(By.id(element));
			ele.clear();
			Thread.sleep(200);
		}catch(Exception e){
			appLogger.logWarning("Unable to do clear on element");
			e.printStackTrace();
		}
    }
    
    
	  /**@author kadamm, This method is useful to keyboard TAB fuctionality */
    public static void keyBoardTab() {
		try{
			Actions action = new Actions(Driver.getInstance().getAppy());
			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(200);
		}catch(Exception e){
			appLogger.logWarning("Unable to tab on the element");
			e.printStackTrace();
		}
    }
    /** 
     * 
     * @param id
     * @param index
     * @return single webelement as per the provided index
     * @throws Exception
     */
    
    public static WebElement listElement(String id, int index) throws Exception {
    	String locatorType = "id";
    	//String locator = id;
    	WebElement element= null;
    	try {
            List < SgElement > elementList = SgElement.getSgElements(locatorType, id);
            element = elementList.get(index);
            appLogger.logWarning("provided index element found");
    	} catch(ElementNotFoundException e) 
    	{
    		appLogger.logWarning("element not found");
    		e.printStackTrace();
    	}
        return element;
    }
    
    
    /** 
     * 
     * @param Elementid=elementid
     * @param locatorType=id/xpath
     * @return return List<String> on the basic of attribute name 'innerHTML'
     * @throws Exception
     */
    
    public static List<String> getDropDownValues(String Elementid, String locatorType) throws Exception {
    	List<String> AllAccountValue = new ArrayList<String>();
    	try {
    		Select dropdown = new Select(Driver.getInstance().getAppy().findElement(By.id(Elementid)));
    		List<WebElement> elementList = dropdown.getOptions();
            for(WebElement ele:elementList)
            	{
            		String AccValue = ele.getAttribute("innerHTML");
            		AllAccountValue.add(AccValue); 
            	}
            		appLogger.logWarning("provided index element found");
    		} catch(ElementNotFoundException e) 
    			{
    				appLogger.logWarning("element not found");
    				e.printStackTrace();
    			}
        return AllAccountValue;
    }
    
    
    /** 
     * 
     * @param Elementid=elementid
     * @param locatorType=id/xpath
     * @return return List<String> on the basic of attribute name 'value'
     * @throws Exception
     */
    
    public static List<String> dropDownAccount(String Elementid, String locatorType) throws Exception {
    	List<String> AllAccountValue = new ArrayList<String>();
    	try {
    		Select dropdown = new Select(Driver.getInstance().getAppy().findElement(By.id(Elementid)));
    		List<WebElement> elementList = dropdown.getOptions();
            for(WebElement ele:elementList)
            	{
            		String AccValue = ele.getAttribute("value");
            		AllAccountValue.add(AccValue); 
            	}
            		appLogger.logWarning("provided index element found");
    		} catch(ElementNotFoundException e) 
    			{
    				appLogger.logWarning("element not found");
    				e.printStackTrace();
    			}
        return AllAccountValue;
    }
    
    /**
     * 
     * @param dateFormat- pass the format in which date needs to be return.
     * @return
     */
    
    public static String getCurrentDate(String dateFormat) {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		String currentdate = format.format(date);
		return currentdate;
    	
    }
    
	/**
	 * Instantiates a new app element.
	 *
	 * @param id
	 *            the identifier of an element
	 * @param index
	 *            the index
	 * @throws Exception
	 *             the exception
	 */
	public static List<SgElement> AppElement(String id) throws Exception {
		String locatorType = "id";
		List<SgElement> elementList = SgElement.getSgElements(locatorType, id);
		return elementList;

	}
	
	public static void verifyRecipient() throws Exception 
	{
/*		List<String> memberRecipient = null;
		List<SgElement> recipient = AppSpecificFunctions.AppElement((OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxRow")));
		Iterator iterator =recipient.iterator();*/
		
		List<AppElement> appelent= AppElement.getAppElements(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxRow"));
		for(AppElement app:appelent)
		{
		List<AppElement> Childrecipient = app.getChildElementsById(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_lblBankName"));
		for(AppElement ele:Childrecipient)
		{
			if(ele.getText().equalsIgnoreCase("Kony Bank"))
			{
				
				List<AppElement> element = app.getChildElementsById(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxDropdown"));
				element.get(0).click();
				AppElement recipientAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountNumberValue"));
				String accnumber=recipientAccount.getText();
				appLogger.logInfo(accnumber);
				element.get(0).click();
			}
		}
		}
	}

	
	
	public static List<String> verifyRecipient2() throws Exception 
	{
		List<String> memberRecipient = new ArrayList<String>();
		
		Driver driver = Driver.getInstance();
		int size =driver.getAppy().findElements(By.id(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxRow"))).size();
		for(int i= 1; i<=size;i++)
		{
		List<WebElement> appele =driver.getAppy().findElements(By.xpath("//*[@id='frmTransfers_transfermain_segmentTransfers']/ul/li["+i+"]/div/div/div/div/div/div"));
		String value=appele.get(2).getText();
		if(value.equalsIgnoreCase("Kony Bank"))
		{
			appele.get(0).click();
			WebElement ele =driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountNumberValue")));
			String accountnumber =ele.getText();
			memberRecipient.add(accountnumber);	
		}
		}
		AppElement pagination = new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxRow"));
		boolean value=pagination.isEnabled();
		while(value==true)
		{
			pagination.click();
			
		}
		return memberRecipient;
	}
	
	public static List<String> getExternalAccount(String bankname) throws Exception 
	{
		List<String> memberRecipient = new ArrayList<String>();
		boolean PaginationArrow =true;
		while(PaginationArrow==true)
		{
			AppElement pagination = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
			PaginationArrow=pagination.isEnabled();
			Driver driver = Driver.getInstance();
		int size =driver.getAppy().findElements(By.id(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_flxRow"))).size();
		for(int i= 1; i<=size;i++)
		{
			List<WebElement> appele =driver.getAppy().findElements(By.xpath("//*[@id='frmTransfers_transfermain_segmentTransfers']/ul/li["+i+"]/div/div/div/div/div/div"));
			String value=appele.get(2).getText();
			if(value.equalsIgnoreCase(bankname))
				{
					appele.get(0).click();
					WebElement ele =driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_lblAccountNumberValue")));
					String accountnumber =ele.getText();
					memberRecipient.add(accountnumber);
				}
		}
		if (PaginationArrow==true){
				AppElement pagination1 = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
				pagination1.click();
				Thread.sleep(5000);
				}
		}
			return memberRecipient;
	}
	
//	public static String getDynamicName()
//	{
//		Faker faker = new Faker();
//		String name = faker.name().fullName();
//		String firstName = faker.name().firstName();
//		return firstName;
//	
//	}
	
	/**
	 * 
	 * @return return account number in the String data type from 8 to 10 digit range.
	 */
	public static String getDynamicNumber()
	{
		Random r = new Random();
		int Low = 12345678, High = 1234567891;
		int account = r.nextInt(High-Low) + Low;
		String AccountNumber = Integer.toString(account);
		return AccountNumber;
	}
}
