package test.OnlineBanking.forms;

import test.OnlineBanking.OnlineBankingWidgetId;
import test.common.AppElement;

public class frmExternalAccounts {
	
	public void clickOnSearchLogo()
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_imgSearch"));
			AppElement SearchName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_imgSearch"));
			SearchName.click();
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void enterSearchRecipientName(String name)
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_Search_txtSearch"));
			AppElement SearchInputbox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_Search_txtSearch"));
			SearchInputbox.type(name);
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickOnSearchButton()
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_Search_btnConfirm"));
			AppElement SearchButton=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_Search_btnConfirm"));
			SearchButton.click();
			Thread.sleep(5000);
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void viewAccountDetails()
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_imgDropdown_span"));
			AppElement AccDetails=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersUnselected_imgDropdown_span"));
			AccDetails.click();
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}

}
