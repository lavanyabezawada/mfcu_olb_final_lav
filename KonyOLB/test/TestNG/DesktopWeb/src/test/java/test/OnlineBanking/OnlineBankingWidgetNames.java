package test.OnlineBanking;

import test.common.SgConfiguration;
import test.common.AppResouceBundle;

public class OnlineBankingWidgetNames {

	static AppResouceBundle widgetNames;
	
	public static String getWidgetName(String key) throws Exception{
		if(widgetNames==null)
			if(SgConfiguration.getInstance().getKeyValue("Device").equalsIgnoreCase("tablet"))
			widgetNames = new AppResouceBundle("tabletWidgetName.properties");
			else if(SgConfiguration.getInstance().getKeyValue("Device").equalsIgnoreCase("mobile"))
			widgetNames = new AppResouceBundle("mobileWidgetName.properties");
			else if(SgConfiguration.getInstance().getKeyValue("Device").equalsIgnoreCase("desktop"))
			widgetNames = new AppResouceBundle("desktopWidgetNames.properties");
		return widgetNames.getProperty(key);
	}

}