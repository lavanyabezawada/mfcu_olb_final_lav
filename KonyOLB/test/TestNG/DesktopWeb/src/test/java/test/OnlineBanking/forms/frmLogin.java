package test.OnlineBanking.forms;
import test.common.AppElement;
import test.common.Segment;
import java.io.IOException;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmLogin {


	/*  public frmLogin() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_frmLogin"));
  }*/

	public void btnContactUs() throws Exception{ 
		AppElement btnContactUs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_btnContactUs"));
		btnContactUs.click();
	}
	public void btnFaqs() throws Exception{ 
		AppElement btnFaqs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_btnFaqs"));
		btnFaqs.click();
	}
	public void btnLocateUs() throws Exception{ 
		AppElement btnLocateUs=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_btnLocateUs"));
		btnLocateUs.click();
	}
	public void btnPrivacy() throws Exception{ 
		AppElement btnPrivacy=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_btnPrivacy"));
		btnPrivacy.click();
	}
	public void btnTermsAndConditions() throws Exception{ 
		AppElement btnTermsAndConditions=new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_btnTermsAndConditions"));
		btnTermsAndConditions.click();
	}


	public void segLanguagesList(String label) throws Exception{
		try {
			AppElement.scrollUntilVisible(label);
			Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmLogin_segLanguagesList"),OnlineBankingWidgetId.getWidgetId("frmLogin_flxLangList"));
			lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){

			//Handle Exception Code Here
		}
	} 

	public void doLogin(String username, String password) throws Exception {
		//Enters username, password and clicks on login
		enterUsername(username);
		enterPassword(password);
		clickLoginbtn();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));

	}

	public void enterUsername(String username) throws Exception {
		//Enters username
		AppElement tbxUserIDKA = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_frmLogin_main_tbxUserName"));
		tbxUserIDKA.typeAndClickNext(username);
	}

	public void enterPassword(String password) throws Exception {
		//Enters password
		AppElement tbxPasswordKA = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_frmLogin_main_tbxPassword"));
		tbxPasswordKA.type(password);
	}

	public void clickLoginbtn() throws Exception {
		//Clicks login btn
		AppElement signInButton = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_frmLogin_main_btnLogin"));
		signInButton.click();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
	}

	public static boolean isLoginForm() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmLoginKA_Header"));
		return visible;
	}

	public static void navigateFromLoginForm() throws Exception {
		frmCustomHeaderForm header = new frmCustomHeaderForm();

		if (frmAccountsLanding.isLandingForm()) {
			header.clickOnLogOut();

		} /*else if (FrmChangePassword.isFrmChangePassword()) {
		FrmChangePassword frmUpdatePassword = new FrmChangePassword();
		frmUpdatePassword.clickCancel();

	} else if (FrmDownTime.isFrmDownTime()) {
		FrmDownTime frmDownTime = new FrmDownTime();
		frmDownTime.clickRetry();
	}*/

	}

	public String getErrorText() throws Exception {
		String ErrText = null;
		AppElement ErrLabel = null;

		if(AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmLoginKA_txtError"))) {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmLoginKA_txtError"));
			ErrLabel = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLoginKA_txtError"));

		}/* lse if(AppElement.isElementVisible("id", AdminConsoleWidgetId.getWidgetId("frmLoginKA_txtErrorOneAttemptLeft"))) {
		ErrLabel = new AppElement(AdminConsoleWidgetId.getWidgetId("frmLoginKA_txtErrorOneAttemptLeft"));
	}*/

		if(ErrLabel == null)
			return null;

		ErrText = ErrLabel.getText();
		return ErrText;
	}

	public String getSessionTimeoutMsg() throws Exception {
		AppElement TimeOutLabel = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLoginKA_lblTimeExpired"));
		String ErrText = TimeOutLabel.getText();
		return ErrText;
	}

	public AppElement getLoginButton() throws Exception {
		AppElement LoginButton = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLoginKA_btnSignin"));
		return LoginButton;
	}

}