package test.OnlineBanking.forms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import test.common.AppElement;
import test.common.AppLogger;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAccountsLanding {
	static AppLogger appLogger = AppLogger.getInstance();

	public void btnCancel() throws Exception {
		AppElement btnCancel = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnCancel"));
		btnCancel.click();
	}

	public void btnContactUs() throws Exception {
		AppElement btnContactUs = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnContactUs"));
		btnContactUs.click();
	}

	public void btnOpenNewAccount() throws Exception {
		AppElement btnOpenNewAccount = new AppElement(
				OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnOpenNewAccount"));
		btnOpenNewAccount.click();
	}

	public void btnSave() throws Exception {
		AppElement btnSave = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnSave"));
		btnSave.click();
	}

	public void btnUpdateAccntPreferences() throws Exception {
		AppElement btnUpdateAccntPreferences = new AppElement(
				OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnUpdateAccntPreferences"));
		btnUpdateAccntPreferences.click();
	}

	public void rtxTC(String text) throws Exception {
		AppElement rtxTC = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_rtxTC"));
		rtxTC.type(text);
	}

	public static boolean isLandingForm() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id",
				OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_frmAccountsLanding"));
		return visible;
	}


	public boolean isAdsPresent() {
		boolean value = false;
		String locatorType = "id";
		try {
			value = AppElement.isElementVisible(locatorType,
					OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgPag1"));
		} catch (Exception e) {
			appLogger.logError("Advertisement tab is not present on the account landing page.");
			e.printStackTrace();
		}
		return value;
	}

	public void clickOnFirstAdsTab() {
		try {
			AppElement firstAdsTab = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1"));
			firstAdsTab.click();
		} catch (Exception e) {
			appLogger.logError("First Advertisement tab is not found on the account landing page.");
			e.printStackTrace();
		}
	}
	
	public void clickOnSecondAdsTab() {
		try {
			AppElement firstAdsTab = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1"));
			firstAdsTab.click();
		} catch (Exception e) {
			appLogger.logError("Second Advertisement tab is not found on the account landing page.");
			e.printStackTrace();
		}
	}
	
	public void clickOnThirdAdsTab() {
		try {
			AppElement firstAdsTab = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1"));
			firstAdsTab.click();
		} catch (Exception e) {
			appLogger.logError("Third Advertisement tab is not found on the account landing page.");
			e.printStackTrace();
		}
	}
	
	
	public void clickOnFirstAdsImage() {
		WebElement element = null;
		try {
			element = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1")));
			Actions action = new Actions(Driver.getInstance().getAppy());
			action.moveToElement(element).click().perform();
		} catch (Exception e) {
			appLogger.logError("First Advertisement image is not found on the account landing page.");
			e.printStackTrace();
		}
	}
	
	public void clickOnSecondAdsImage() {
		WebElement element = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd2"));
			element = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd2")));
			Actions action = new Actions(Driver.getInstance().getAppy());
			action.moveToElement(element).click().perform();
		} catch (Exception e) {
			appLogger.logError("Second Advertisement image is not found on the account landing page.");
			e.printStackTrace();
		}
	}
	
	public void clickOnThirdAdsImage() {
		WebElement element = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd3"));
			element = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd3")));
			Actions action = new Actions(Driver.getInstance().getAppy());
			action.moveToElement(element).click().perform();
		} catch (Exception e) {
			appLogger.logError("Third Advertisement image is not found on the account landing page.");
			e.printStackTrace();
		}
	}

	public String getAdsURL() {
		String url = null;
		try {
			AppElement ads = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgBanner"));
			url = ads.getAttribute("src");
		} catch (Exception e) {
			appLogger.logError("First Advertisement image is not found on the account landing page.");
			e.printStackTrace();
		}
		return url;
	}
	
	public void changeControlToNewAdsWindow(String currentTab)
	{
		Driver driver = null;
		try {
			driver = Driver.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getAppy().getWindowHandles());
		for(String tab: tabs2)
		{
			if(tab!=currentTab)
			{
				driver.getAppy().switchTo().window(tab);
			}
		}
		
	}
	
	public void changeFocusToParentWindow(String ParentTab)
	{
		Driver driver;
		try {
			driver = Driver.getInstance();
			driver.getAppy().switchTo().window(ParentTab);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}