//package test.OnlineBanking.tests;
//
//import org.kony.qa.stargate.wrappers.appy.Driver;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;
//
//import test.OnlineBanking.OnlineBankingBaseTest;
//import test.OnlineBanking.OnlineBankingWidgetId;
//import test.OnlineBanking.forms.frmAccountsDetails;
//import test.OnlineBanking.forms.frmAccountsLanding;
//import test.OnlineBanking.forms.frmCustomHeaderForm;
//import test.OnlineBanking.forms.frmLogin;
//import test.OnlineBanking.forms.frmTransferConfirm;
//import test.OnlineBanking.forms.frmTransfers;
//import test.common.AppElement;
//import test.common.SgConfiguration;
//
//public class ScheduleTransactions extends OnlineBankingBaseTest {
//	frmTransfers transfer = new frmTransfers();
//	frmLogin login = new frmLogin();
//	SgConfiguration sg = SgConfiguration.getInstance();
//	frmCustomHeaderForm header=new frmCustomHeaderForm();
//	frmTransferConfirm transferConfirm = new frmTransferConfirm();
//	frmAccountsDetails accountdetails = new frmAccountsDetails();
//	
//	@BeforeMethod
//	public void setupBeforeTest() throws Exception {	Driver.getInstance().getAppy().manage().window().maximize();
//	
//	if(!frmTransfers.isTransfersForm())
//	{
//		if(frmAccountsLanding.isLandingForm())
//			{
//			  transfer.navigateTofrmTransferMoney(); 
//			}
//			else if(frmLogin.isLoginForm())			{
//				login.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
//				transfer.navigateTofrmTransferMoney(); 
//		}
//	}
//	}
//	
//	@Test(description = "Verify scheduled Transaction record listed in the schedule transaction tab.")
//	public void verifyScheduleTransactionRecord() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		transfer.clickOnScheduleTab();
//		AppElement transaction = new AppElement(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfers_imgDropdown"),0);
//		transaction.click();
//		sa.assertEquals(transfer.getScheduleTransactionDateValue(),"");
//		sa.assertEquals(transfer.getScheduleTransactionAmountValue(), "" );
//		sa.assertEquals(transfer.getScheduleTransactionFromAccountValue(), "");
//		sa.assertEquals(transfer.getScheduleTransactionReferenceNumberValue(),"");
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify the 'Previous' arrow is disabled where transaction list starts")
//	public void verifyPreviousArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//		sa.assertEquals(PreviousArrow.isEnabled(), false);
//	}
//	
//	@Test(description = "Verify pagination buttons if transaction count is less than the 10.")
//	public void verifyPreviousAndNextArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		int recordcount = accountdetails.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfers_flxRow"));
//		if(recordcount < 10)
//		{
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), false);
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}else {
//
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), false);
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}
//		
//	}
//	
//	@Test(description = "Verify next arrow button when transaction count is more than 10.")
//	public void verifyNextArrowButton() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		int recordcount = accountdetails.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfers_flxRow"));
//		if(recordcount > 10)
//		{
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}else {
//			
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}
//		sa.assertAll();
//	}
//	
//	@Test(description = "Verify 'Previous' and 'Next' arrows are available at the bottom of \"Account\" screen to view multiple transaction entries.")
//	public void verifyNextRecord() throws Exception 
//	{
//		SoftAssert sa = new SoftAssert();
//		int recordcount = accountdetails.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfers_flxRow"));
//		if(recordcount > 10)
//		{
//			accountdetails.clickOnNextArrow();
//			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
//			sa.assertEquals(PreviousArrow.isEnabled(), true);
//			
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), true);
//		}else {
//			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//			sa.assertEquals(NextArrow.isEnabled(), false);
//		}
//		
//		sa.assertAll();
//	
//	}
//	
//	
//	@Test(description = "Verify the 'Next' arrow is disabled where transaction list ends.")
//	public void verifyNextArrowDisabled() throws Exception 
//	{
//		boolean value=true;
//		int recordcount=0;
//		SoftAssert sa = new SoftAssert();
//			while(value==true)
//			{
//				recordcount = accountdetails.getTransactionRecordCount(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfers_flxRow"));
//				if(recordcount < 10){
//					AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
//					sa.assertEquals(NextArrow.isEnabled(), false);
//					value=false;
//				}else
//					{
//					 accountdetails.clickOnNextArrow();
//					}	
//			}
//			sa.assertAll();
//	}
//	
//}
