//package test.OnlineBanking.tests;
//
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;
//
//import test.OnlineBanking.OnlineBankingBaseTest;
//import test.OnlineBanking.OnlineBankingWidgetId;
//import test.OnlineBanking.forms.frmAccountsLanding;
//import test.OnlineBanking.forms.frmAddExternalAccount;
//import test.OnlineBanking.forms.frmAddInternalAccount;
//import test.OnlineBanking.forms.frmLogin;
//import test.common.AppElement;
//import test.common.SgConfiguration;
//
//public class AddRecipientOtherBank extends OnlineBankingBaseTest 
//{
//	frmAddExternalAccount addexternalaccount = new frmAddExternalAccount();
//	frmAccountsLanding frmaccountslanding=new frmAccountsLanding();
//	frmLogin frmlogin=new frmLogin();
//	SgConfiguration sg = SgConfiguration.getInstance();
//	
//	@BeforeMethod
//	public void setupBeforeTest() throws Exception {
//		
//		if(!frmAddInternalAccount.isfrmAddInternalAccount())
//		{
//			if(frmAccountsLanding.isLandingForm())
//				{
//				addexternalaccount.navigateTofrmAddExternalAccount(); 
//				}
//				else if(frmLogin.isLoginForm())			{
//					frmlogin.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
//					addexternalaccount.navigateTofrmAddExternalAccount();
//			}
//		}
//	}
//	
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	
//	@Test(description = "Verify field available in the 'Add kony account' option")
//	public void verifyFieldsInAddkonyAccount() throws Exception {
//		SoftAssert sa = new SoftAssert();
//		
//		AppElement OwnerAccountCheckbox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_imgchecked"));
//		sa.assertTrue(OwnerAccountCheckbox.isChecked());
//		AppElement OwnerAccountCheckboxmsg=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lbliamownerofaccount"));
//		sa.assertEquals(OwnerAccountCheckboxmsg.getText(), "I am Owner of this Account");
//		AppElement RoutingNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblRoutingNumberKA"));
//		sa.assertEquals(RoutingNumber.getText(), "Routing Number");
//		
//		AppElement BankName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_tbxBankNameKA"));
//		sa.assertEquals(BankName.getText(), "Bank Name");
//		
//		AppElement AccountType=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblAccountTypeKA"));
//		sa.assertEquals(AccountType.getText(), "Account Type");
//		AppElement AccountTypebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lbxAccountTypeKA"));
//		sa.assertEquals(AccountTypebox.isElementVisible(), true);
//		
//		AppElement AccountNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblAccountNumberKA"));
//		sa.assertEquals(AccountNumber.getText(), "Account Number");
//		AppElement AccountNumberBox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_tbxAccountNumberKA"));
//		sa.assertEquals(AccountNumberBox.isElementVisible(), true);
//		
//		AppElement ReAccountNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblAccountNumberAgainKA"));
//		sa.assertEquals(ReAccountNumber.getText(), "Re-Enter Account Number");
//		AppElement ReAccountNumberbox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_tbxAccountNumberAgainKA"));
//		sa.assertEquals(ReAccountNumberbox.isElementVisible(), true);
//		
//		AppElement BeneficiaryNamelbl=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblBeneficiaryNameKA"));
//		sa.assertEquals(BeneficiaryNamelbl.getText(), "Beneficiary Name");
//		AppElement BeneficiaryNamebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_tbxBeneficiaryNameKA"));
//		sa.assertEquals(BeneficiaryNamebox.isElementVisible(), true);
//		
//		AppElement AccountNamelbl=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_lblAccountNickNameKA"));
//		sa.assertEquals(AccountNamelbl.getText(), "Account Nickname");
//		AppElement AccountNamebox=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_tbxAccountNickNameKA"));
//		sa.assertEquals(AccountNamebox.isElementVisible(), true);
//		
//		AppElement Cancelbutton=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_btnCancelKA"));
//		sa.assertEquals(Cancelbutton.isElementVisible(), true);
//		
//		AppElement AddAccountButton=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddExternalAccount_externalAccount_btnAddAccountKA"));
//		sa.assertEquals(AddAccountButton.isElementVisible(), true);
//		sa.assertAll();
//		 
//		} 
//	
//
//}
