package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmCardManagement {


  public frmCardManagement() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_frmCardManagement"));
  }
public void btnBackToCards() throws Exception{ 
  AppElement btnBackToCards=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnBackToCards"));
  btnBackToCards.click();
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnCancel"));
  btnCancel.click();
  }
public void btnCancel1() throws Exception{ 
  AppElement btnCancel1=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnCancel1"));
  btnCancel1.click();
  }
public void btnCancelPlan() throws Exception{ 
  AppElement btnCancelPlan=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnCancelPlan"));
  btnCancelPlan.click();
  }
public void btnCardsCancel() throws Exception{ 
  AppElement btnCardsCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnCardsCancel"));
  btnCardsCancel.click();
  }
public void btnCardsContinue() throws Exception{ 
  AppElement btnCardsContinue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnCardsContinue"));
  btnCardsContinue.click();
  }
public void btnConfirm() throws Exception{ 
  AppElement btnConfirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnConfirm"));
  btnConfirm.click();
  }
public void btnContinue() throws Exception{ 
  AppElement btnContinue=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnContinue"));
  btnContinue.click();
  }
public void btnModify() throws Exception{ 
  AppElement btnModify=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnModify"));
  btnModify.click();
  }
public void btnRequestReplacement() throws Exception{ 
  AppElement btnRequestReplacement=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnRequestReplacement"));
  btnRequestReplacement.click();
  }
public void btnSave() throws Exception{ 
  AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_btnSave"));
  btnSave.click();
  }

public void txtDestination(String text) throws Exception{
  AppElement txtDestination=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_txtDestination"));
  txtDestination.type(text);
  }
public void txtPhoneNumber(String text) throws Exception{
  AppElement txtPhoneNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_txtPhoneNumber"));
  txtPhoneNumber.type(text);
  }

public void rtxDestination1(String text) throws Exception{
  AppElement rtxDestination1=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDestination1"));
  rtxDestination1.type(text);
  }
public void rtxDestination2(String text) throws Exception{
  AppElement rtxDestination2=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDestination2"));
  rtxDestination2.type(text);
  }
public void rtxDestination3(String text) throws Exception{
  AppElement rtxDestination3=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDestination3"));
  rtxDestination3.type(text);
  }
public void rtxDestination4(String text) throws Exception{
  AppElement rtxDestination4=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDestination4"));
  rtxDestination4.type(text);
  }
public void rtxDestination5(String text) throws Exception{
  AppElement rtxDestination5=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDestination5"));
  rtxDestination5.type(text);
  }
public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }
public void rtxTC(String text) throws Exception{
  AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxTC"));
  rtxTC.type(text);
  }
public void rtxValue1(String text) throws Exception{
  AppElement rtxValue1=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxValue1"));
  rtxValue1.type(text);
  }
public void rtxValue2(String text) throws Exception{
  AppElement rtxValue2=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxValue2"));
  rtxValue2.type(text);
  }
public void rtxValue5(String text) throws Exception{
  AppElement rtxValue5=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxValue5"));
  rtxValue5.type(text);
  }
public void rtxValue6(String text) throws Exception{
  AppElement rtxValue6=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxValue6"));
  rtxValue6.type(text);
  }
public void rtxValueA(String text) throws Exception{
  AppElement rtxValueA=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCardManagement_rtxValueA"));
  rtxValueA.type(text);
  }


public void segDestinationList(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmCardManagement_segDestinationList"),OnlineBankingWidgetId.getWidgetId("frmCardManagement_flxCustomListBox"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 
public void segDestinations(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmCardManagement_segDestinations"),OnlineBankingWidgetId.getWidgetId("frmCardManagement_flxClose"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}