package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmCustomerFeedbackSurvey {


  public frmCustomerFeedbackSurvey() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_frmCustomerFeedbackSurvey"));
  }
public void btnAddAnotherAccount() throws Exception{ 
  AppElement btnAddAnotherAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_btnAddAnotherAccount"));
  btnAddAnotherAccount.click();
  }
public void btnDone() throws Exception{ 
  AppElement btnDone=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_btnDone"));
  btnDone.click();
  }
public void btnLogin() throws Exception{ 
  AppElement btnLogin=new AppElement(OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_btnLogin"));
  btnLogin.click();
  }




public void segSurveyQuestion(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_segSurveyQuestion"),OnlineBankingWidgetId.getWidgetId("frmCustomerFeedbackSurvey_flxQuestion1"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}