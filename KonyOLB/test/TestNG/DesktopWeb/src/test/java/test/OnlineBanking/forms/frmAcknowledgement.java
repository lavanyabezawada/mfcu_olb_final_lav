package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAcknowledgement {


  public frmAcknowledgement() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_frmAcknowledgement"));
  }
public void btnAddAnotherAccount() throws Exception{ 
  AppElement btnAddAnotherAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnAddAnotherAccount"));
  btnAddAnotherAccount.click();
  }
public void btnMakeAnotherPayment() throws Exception{ 
  AppElement btnMakeAnotherPayment=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnMakeAnotherPayment"));
  btnMakeAnotherPayment.click();
  }
public void btnMakeTransfer() throws Exception{ 
  AppElement btnMakeTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnMakeTransfer"));
  btnMakeTransfer.click();
  }
public void btnSavePayee() throws Exception{ 
  AppElement btnSavePayee=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnSavePayee"));
  btnSavePayee.click();
  }
public void btnViewPaymentActivity() throws Exception{ 
  AppElement btnViewPaymentActivity=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnViewPaymentActivity"));
  btnViewPaymentActivity.click();
  }




public void segBill(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_segBill"),OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_flxHeader"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 
public void segConfirmBulkPay(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_segConfirmBulkPay"),OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnEdit"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}