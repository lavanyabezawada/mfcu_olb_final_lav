package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;
import test.common.Segment;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmTransferConfirm {

	static AppLogger appLogger = AppLogger.getInstance();

  public static boolean isfrmTransferConfirm() throws Exception 
  {
  boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmConfirm"));
  return visible;
  }

  /**
   * 
   * @return from account value
   */
  public String returnFromAccountValue()
  {
	  AppElement FromAccount;
	  String fromaccountvalue = null;
	  try {
		  FromAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValueFrom_lblValue"));
		  fromaccountvalue= FromAccount.getText();
	} catch (Exception e) {
		appLogger.logError("'FromAccount' field value not found, inside the confimr Transfer option");
		e.printStackTrace();
	}
	return fromaccountvalue;
  }
  
  /**
   * 
   * @return To account value 
   */
  public String returnToAccountValue()
  {
	  AppElement ToAccount;
	  String toaccountvalue = null;
	  try {
		  ToAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValueTo_lblValue"));
		  toaccountvalue= ToAccount.getText();
	} catch (Exception e) {
		appLogger.logError("'ToAccount' field value not found, inside the confimr Transfer option");
		e.printStackTrace();
	}
	return toaccountvalue;
  }

  /**
   * 
   * @return Transfer Amount value 
   */
  public String returnAmountValue()
  {
	  AppElement Amount;
	  String amountvalue = null;
	  try {
		  Amount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValueAmount_lblValue"));
		  amountvalue= Amount.getText();
	} catch (Exception e) {
		appLogger.logError("'Amount' field value not found, inside the confimr Transfer option");
		e.printStackTrace();
	}
	return amountvalue;
  }

  
  /**
   * 
   * @return Transfer Date value 
   */
  public String returnDateValue()
  {
	  AppElement DateField;
	  String datefieldvalue = null;
	  try {
		  DateField=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValuePaymentDate_lblValue"));
		  datefieldvalue= DateField.getText();
	} catch (Exception e) {
		appLogger.logError("'Date' field value not found, inside the confimr Transfer option");
		e.printStackTrace();
	}
	return datefieldvalue;
  }
  
  /**
   * 
   * @return Transfer Note value 
   */
  public String returnNoteValue()
  {
	  AppElement Note;
	  String toaccountvalue = null;
	  try {
		  Note=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValueNote_lblValue"));
		  toaccountvalue= Note.getText();
	} catch (Exception e) {
		appLogger.logError("'note' field value not found, inside the confimr Transfer option");
		e.printStackTrace();
	}
	return toaccountvalue;
  }
  
/**
 * Click on the cancel button which is under confirm transfer page.
 */
  public void clickOnCancelButton()
  {
	  try {
		 AppElement cancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_keyValueNote_lblValue"));
		 cancel.click();
		 AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirm_CustomPopup_btnYes"));
	} catch (Exception e) {
		appLogger.logError("'Cancel' button not found, inside the confimr Transfer option");
		e.printStackTrace();
	}

  }
  
  /**
   * Click on the cancel button which is under confirm transfer page.
   */
    public void clickOnModifyButton()
    {
  	  try {
  		 AppElement modify=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_confirmButtons_btnModify"));
  		 modify.click();
  		 AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers"));
  	} catch (Exception e) {
  		appLogger.logError("'modify' button not found, inside the confimr Transfer option");
  		e.printStackTrace();
  	}

    }
    
    /**
     * Click on the cancel button which is under confirm transfer page.
     */
      public void clickOnConfirmButton()
      {
    	  try {
    		 AppElement Confirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_confirmDialog_confirmButtons_btnConfirm"));
    		 Confirm.click();
    		 AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement"));
    	} catch (Exception e) {
    		appLogger.logError("'Confirm' button not found, inside the confimr Transfer option");
    		e.printStackTrace();
    	}

      }
      
      
      /**
       * Click on the YES button of quite transfer window.
       */
        public void clickOnQuiteTransferYes()
        {
      	  try {
      		 AppElement quiteYes=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirm_CustomPopup_btnYes"));
      		quiteYes.click();
      	} catch (Exception e) {
      		appLogger.logError("'Yes' button not found, on quite transfer window");
      		e.printStackTrace();
      	}

        }


public void segBill(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmConfirm_segBill"),OnlineBankingWidgetId.getWidgetId("frmConfirm_btnEdit"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	}



}