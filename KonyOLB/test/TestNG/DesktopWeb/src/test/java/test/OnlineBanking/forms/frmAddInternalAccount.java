package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.kony.qa.stargate.wrappers.appy.SgElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAddInternalAccount {
	static AppLogger appLogger = AppLogger.getInstance();

	public static boolean isfrmAddInternalAccount() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount"));
		return visible;
	}

	public String getBankName() throws Exception{
		AppElement BankName=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lblBankNameValue"));
		return BankName.getText();
	}


	public void selectAccountType(String accountTypeText) throws Exception{
		try {
			WebElement AccountType = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_lbxAccountTypeKA")));
			Select select = new Select(AccountType);
			select.selectByVisibleText(accountTypeText);
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void enterAccountNumber(String text) throws Exception{
		AppElement AccNum=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberKA"));
		AccNum.type(text);
	}

	public void reEnterAccountNumber(String text) throws Exception{
		AppElement AccNum=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNumberAgainKA"));
		AccNum.type(text);
	}

	public void enterBeneficiaryName(String text) throws Exception{
		AppElement name=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxBeneficiaryNameKA"));
		name.type(text);
	}

	public void enterAccountNickName(String text) throws Exception{
		AppElement name=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_tbxAccountNickNameKA"));
		name.type(text);
	}

	public void clickAddAccount() throws Exception {
		AppElement AddAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnAddAccountKA"));
		AddAccount.click();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_frmConfirmAccount"));
	}

	public void clickOnCancel() throws Exception {
		AppElement canCelButton = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount_internalAccount_btnCancelKA"));
		canCelButton.click();
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_frmTransfers"));
	}

	public void navigateTofrmAddInternalAccount() throws Exception {

		frmCustomHeaderForm header=new frmCustomHeaderForm();
		header.clickOnMenubar();
		header.clickOnTransfersMenuItem();
		header.clickOnAddKonyAccounts();

	}

	/** 
	 * 
	 * @param Elementid=elementid
	 * @param locatorType=id/xpath
	 * @return return List<String>
	 * @throws Exception
	 */

	public static List<String> returnInternalRecipientNumber(String ParentElementid, String ChildElementid, String locatorType) throws Exception {
		List<String> AllAccountValue = new ArrayList<String>();
		try {
			List < SgElement > elementList = SgElement.getSgElements(locatorType, ParentElementid);
			for(SgElement ele:elementList)
			{
				//AllAccountValue.add(AccValue); 
			}
			appLogger.logWarning("provided index element found");
		} catch(ElementNotFoundException e) 
		{
			appLogger.logWarning("element not found");
			e.printStackTrace();
		}
		return AllAccountValue;
	}

	public int getRecipientRecordCount(String id)
	{
		int ListSize=0;
		try {
				String locatorType = "id";
				List<SgElement> elementList = SgElement.getSgElements(locatorType, id);
				ListSize = elementList.size();
		}catch(Exception e)
		{
			appLogger.logError("flxRecentTransfers_flxRow element not found on the 'Recent' tab inside frmtransfer page.");
			e.printStackTrace();
		}
		return ListSize;
	}


}