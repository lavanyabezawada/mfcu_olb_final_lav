package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;

import java.io.IOException;
import java.util.List;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.kony.qa.stargate.wrappers.appy.SgElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import test.OnlineBanking.AppSpecificFunctions;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmAccountsDetails {
	static AppLogger appLogger = AppLogger.getInstance();


	public static boolean isfrmAccountDetails() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_frmAccountsDetails"));
		return visible;
	}

	public void btnMakeTransfer() throws Exception{ 
		AppElement btnMakeTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_btnMakeTransfer"));
		btnMakeTransfer.click();
	}
	public void btnPayABill() throws Exception{ 
		AppElement btnPayABill=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_btnPayABill"));
		btnPayABill.click();
	}
	public void btnScheduledTransfer() throws Exception{ 
		AppElement btnScheduledTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_btnScheduledTransfer"));
		btnScheduledTransfer.click();
	}
	public void btnViewAccountInfo() throws Exception{ 
		AppElement btnViewAccountInfo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_btnViewAccountInfo"));
		btnViewAccountInfo.click();
	}


	public void clickOnPreviousArrow()
	{
		try {
			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_transactions_imgPaginationPrevious"));
			PreviousArrow.click();
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_frmConfirmAccount"));
		}catch(Exception e)
		{
			appLogger.logError("Previous arrow button not found on account details page.");
			e.printStackTrace();
		}
	}

	public void clickOnNextArrow()
	{
		try {
			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_transactions_imgPaginationNext"));
			NextArrow.click();
			Thread.sleep(7000);
		}catch(Exception e)
		{
			appLogger.logError("Next arrow button not found on account details page.");
			e.printStackTrace();
		}
	}

	public int getTransactionRecordCount(String id)
	{
		int ListSize=0;
		try {
			String locatorType = "id";
			List<SgElement> elementList = SgElement.getSgElements(locatorType, id);
			ListSize = elementList.size();
		}catch(Exception e)
		{
			appLogger.logError("flxSegTransactionRowSavings_flxSegTransactionRowWrapper element not found on the account details page.");
			e.printStackTrace();
		}
		return ListSize;
	}

	public void navigateToAccountDetails()
	{
		WebElement ele;
		try {
			ele = AppSpecificFunctions.listElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accountsName"), 0);
			ele.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getPaginationTransactionText()
	{
		String traText = null;
		try {
			AppElement transactioncount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsDetails_transactions_lblPagination"));
			traText=transactioncount.getText();
		} catch (Exception e) {
			appLogger.logError("'frmAccountsDetails_transactions_imgPaginationNext' element not found on the account details page.");
			e.printStackTrace();
		}
		return traText;

	}



}