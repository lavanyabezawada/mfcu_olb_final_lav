package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;

import java.io.IOException;

import test.OnlineBanking.OnlineBankingWidgetId;

public class frmConfirmAccount {
	
	public static boolean isfrmAddInternalAccount() throws IOException, Exception {
		boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_frmConfirmAccount"));
	    return visible;
	}
	
  public void clickOnConfirmAccount() throws Exception{ 
	  AppElement clickOnConfirmAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_confirmDialogAccounts_confirmButtons_btnConfirm"));
	  clickOnConfirmAccount.click();	  
	  AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount"));
	  }
	public void clickOnCancel() throws Exception{ 
	  AppElement clickOnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_confirmDialogAccounts_confirmButtons_btnCancel"));
	  clickOnCancel.click();
	  AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flexcontainer_wrapper"));
	  
	  }
	public void clickOnModify() throws Exception{ 
	  AppElement clickOnModify=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_confirmDialogAccounts_confirmButtons_btnModify"));
	  clickOnModify.click();
	  AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmAddInternalAccount"));
	  }
	public void clickOnYesOnCancel() throws Exception{ 
		  AppElement clickOnYesOnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_CustomPopupCancel_btnYes"));
		  clickOnYesOnCancel.click();
		  AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers"));
		  }
  
	public void clickOnNoOnCancel() throws Exception{ 
		  AppElement clickOnNoOnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_CustomPopupCancel_btnNo"));
		  clickOnNoOnCancel.click();
		  AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount"));
		  }
}