package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmVerifyAccount {



	public void btnAddAnotherAccount() throws Exception{ 
		AppElement btnAddAnotherAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnAddAnotherAccount"));
		btnAddAnotherAccount.click();
	}
	public void btnCancel() throws Exception{ 
		AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnCancel"));
		btnCancel.click();
	}
	public void btnConfirm() throws Exception{ 
		AppElement btnConfirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnConfirm"));
		btnConfirm.click();
	}
	public void clickOnMakeTransfer() throws Exception{ 
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnMakeTransfer"));
		AppElement clickOnMakeTransfer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnMakeTransfer"));
		clickOnMakeTransfer.click();
	}

	public void clickOnAddAnotherAccount() throws Exception{
		AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnAddAnotherAccount"));
		AppElement clickOnAddAnotherAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_btnAddAnotherAccount"));
		clickOnAddAnotherAccount.click();
	}

	public void rtxBillerAddress(String text) throws Exception{
		AppElement rtxBillerAddress=new AppElement(OnlineBankingWidgetId.getWidgetId("frmVerifyAccount_rtxBillerAddress"));
		rtxBillerAddress.type(text);
	}



}