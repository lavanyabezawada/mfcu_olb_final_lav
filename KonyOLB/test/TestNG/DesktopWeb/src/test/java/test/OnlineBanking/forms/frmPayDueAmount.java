package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmPayDueAmount {


  public frmPayDueAmount() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_frmPayDueAmount"));
  }
public void btnBackToAccountDeatil() throws Exception{ 
  AppElement btnBackToAccountDeatil=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_btnBackToAccountDeatil"));
  btnBackToAccountDeatil.click();
  }
public void btnBackToAccountSummary() throws Exception{ 
  AppElement btnBackToAccountSummary=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_btnBackToAccountSummary"));
  btnBackToAccountSummary.click();
  }
public void btnViewAccountDetail() throws Exception{ 
  AppElement btnViewAccountDetail=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_btnViewAccountDetail"));
  btnViewAccountDetail.click();
  }
public void btnViewStatement() throws Exception{ 
  AppElement btnViewStatement=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_btnViewStatement"));
  btnViewStatement.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayDueAmount_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }



}