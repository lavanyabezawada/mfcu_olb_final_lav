package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmNewOpeningAccount {


  public frmNewOpeningAccount() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewOpeningAccount_frmNewOpeningAccount"));
  }
public void btnContinueProceed() throws Exception{ 
  AppElement btnContinueProceed=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewOpeningAccount_btnContinueProceed"));
  btnContinueProceed.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNewOpeningAccount_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }


public void segProductsNUO(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmNewOpeningAccount_segProductsNUO"),OnlineBankingWidgetId.getWidgetId("frmNewOpeningAccount_btnKnowMore"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}