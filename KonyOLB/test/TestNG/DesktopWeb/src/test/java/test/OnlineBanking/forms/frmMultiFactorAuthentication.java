package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmMultiFactorAuthentication {


  public frmMultiFactorAuthentication() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmMultiFactorAuthentication_frmMultiFactorAuthentication"));
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmMultiFactorAuthentication_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }



}