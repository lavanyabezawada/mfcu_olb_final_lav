package test.OnlineBanking.tests;

import org.testng.asserts.SoftAssert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.common.SgConfiguration;
import test.OnlineBanking.OnlineBankingBaseTest;
import test.OnlineBanking.OnlineBankingWidgetId;
import test.OnlineBanking.forms.frmAccountsLanding;
import test.OnlineBanking.forms.frmLogin;

public class TestfrmLogin extends OnlineBankingBaseTest{
 
	SgConfiguration sg = SgConfiguration.getInstance();
	 

	@BeforeMethod
	public void beforeMethod() throws Exception {
		AppElement ele = null;
		try {
        	//Creating a Login form object        	
            ele = new AppElement(OnlineBankingWidgetId.getWidgetId("frmLogin_frmLogin"));
        } catch (Exception e) {
            if (ele == null)            {
            	//Not on login form, Navigating to frmLoginKA
                System.out.println("TestfrmLogin.setupBeforeTest(): Not on the  Login Form");
            }
        }
		
	}
	
	

	@Test(description = "Test case to login with correct credentials")
	public void login() throws Exception {
		SoftAssert sa = new SoftAssert();
		frmLogin frmLogin = new frmLogin();
		frmLogin.enterUsername(sg.getKeyValue("username"));
		frmLogin.enterPassword(sg.getKeyValue("password"));
		frmLogin.clickLoginbtn();
		frmAccountsLanding frmAccountsLanding = new frmAccountsLanding();
		sa.assertTrue(frmAccountsLanding.isLandingForm(), "Unable to login successfully");
        sa.assertAll();
	}
		
//		

	}