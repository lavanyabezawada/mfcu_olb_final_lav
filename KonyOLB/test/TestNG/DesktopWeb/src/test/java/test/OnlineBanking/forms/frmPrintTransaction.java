package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmPrintTransaction {


  public frmPrintTransaction() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_frmPrintTransaction"));
  }
public void btnBack() throws Exception{ 
  AppElement btnBack=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_btnBack"));
  btnBack.click();
  }
public void btnBackBottom() throws Exception{ 
  AppElement btnBackBottom=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_btnBackBottom"));
  btnBackBottom.click();
  }


public void rtxDisclaimer(String text) throws Exception{
  AppElement rtxDisclaimer=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_rtxDisclaimer"));
  rtxDisclaimer.type(text);
  }


public void segPendingTransaction(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_segPendingTransaction"),OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_flxPrintTransaction"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 
public void segPostedTransactions(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_segPostedTransactions"),OnlineBankingWidgetId.getWidgetId("frmPrintTransaction_flxPrintTransaction"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}