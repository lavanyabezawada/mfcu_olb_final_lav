package test.OnlineBanking.tests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import test.OnlineBanking.AppSpecificFunctions;
import test.OnlineBanking.OnlineBankingBaseTest;
import test.OnlineBanking.OnlineBankingWidgetId;
import test.OnlineBanking.forms.frmAccountsLanding;
import test.OnlineBanking.forms.frmCustomHeaderForm;
import test.OnlineBanking.forms.frmLogin;
import test.common.AppElement;
import test.common.SgConfiguration;

public class TestAccountLanding extends OnlineBankingBaseTest {

	SgConfiguration sg = SgConfiguration.getInstance();
	frmAccountsLanding landing = new frmAccountsLanding();
	frmCustomHeaderForm header = new frmCustomHeaderForm();

	@BeforeMethod
	public void setupBeforeTest() throws Exception {
		// Check for login form
		frmLogin Login = new frmLogin();
		if (!frmAccountsLanding.isLandingForm()) {
			// frmLogin.navigateFromLoginForm();
			Login.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
		}
	}

	@AfterMethod
	public void actionAfterMethod() {
		try {
			header.clickOnAccounts();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Test case to verify account overview/landing screen after login")
	public void accountLanding() throws Exception {
		SoftAssert sa = new SoftAssert();

		if (sg.getKeyValue("Servername").equalsIgnoreCase("Fiserv")) {
			List<String> ExpectedInfo = Arrays.asList("Club Savings Account", "Account Number XXXXXXXXXXXX5545","Savings Plus", "Account Number XXXXXXXXXXXX5125", "Club Savings Account","Account Number XXXXXXXXXXXX1200");
			List<String> ActualInfo = AppSpecificFunctions.getDropDownValues("frmAccountsLanding_accountsName", "id");
			sa.assertEquals(ActualInfo, ExpectedInfo);
		} else if (sg.getKeyValue("Servername").equalsIgnoreCase("FiservDNA")) {
			List<String> ExpectedInfo = Arrays.asList("Secondary Savings", "Account Number XXX8893","Direct Checking", "Account Number XXX8900", "Direct Checking","Account Number XXX6650");
			List<String> ActualInfo = AppSpecificFunctions.getDropDownValues("frmAccountsLanding_accountsName", "id");
			sa.assertEquals(ActualInfo, ExpectedInfo);
		}
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/YYYY");
		String currentdate = format.format(date);
		AppElement apdate = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountLanding_lblLastloggedinTime"));
		String appdate = apdate.getText().substring(15, 25);
		sa.assertEquals(appdate, currentdate);

		AppElement NewAccount = new AppElement(
				OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnOpenNewAccount"));
		sa.assertEquals(NewAccount.getAttribute("Value"), "Open New Account");
		sa.assertEquals(NewAccount.isElementVisible(), true);

		AppElement ContactButton = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_btnContactUs"));
		sa.assertEquals(ContactButton.getAttribute("Value"), "Contact Us");
		sa.assertEquals(ContactButton.isElementVisible(), true);

		AppElement TransferAndPay = new AppElement(
				OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_lblTranferAndPay"));
		sa.assertEquals(TransferAndPay.getText(), "Transfer & Pay");
		sa.assertEquals(TransferAndPay.isElementVisible(), true);

		AppElement Menu = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_Menu"));
		sa.assertEquals(Menu.getText(), "Menu");
		sa.assertEquals(Menu.isElementVisible(), true);
		sa.assertAll();
	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */

	@Test(description = "Test case to verify account overview/landing screen after login")
	public void accountDetails() throws Exception {
		SoftAssert sa = new SoftAssert();

		List<String> ActualInfo = AppSpecificFunctions.getDropDownValues("flxAccountListItem_lblAvailableBalanceValue",
				"id");
		String currentBal = ActualInfo.get(0);
		// String AvailBal = AvailBalance.getText();

		WebElement ele = AppSpecificFunctions
				.listElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accountsName"), 0);
		ele.click();
		AppElement lblcurrBal = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_lblCurrBal"));
		sa.assertEquals(lblcurrBal.getText(), "Current Balance");

		AppElement CurrBal = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_currBal"));
		sa.assertEquals(CurrBal.isEnabled(), true);

		AppElement lblAvailBal = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_lblAvailBal"));
		sa.assertEquals(lblAvailBal.getText(), "Available Balance");

		AppElement AvaiBal = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_AvailBal"));
		sa.assertEquals(currentBal, AvaiBal.getText());

		AppElement AccountLink = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_AccountPageLink"));
		AccountLink.click();
		sa.assertAll();

	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */

	@Test(description = "Test case to verify account information")
	public void viewAccountInfo() throws Exception {
		SoftAssert sa = new SoftAssert();
		String accType = null;
		String accNum = null;

		if (sg.getKeyValue("Servername").equalsIgnoreCase("Fiserv")) {
			AppElement AcctType = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accountsName"));
			accType = AcctType.getText();
			AppElement AcctNumber = new AppElement(
					OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accountsNumber"));
			accNum = (AcctNumber.getText().substring(15));
			WebElement ele = AppSpecificFunctions
					.listElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accountsName"), 0);
			ele.click();
		} else if (sg.getKeyValue("Servername").equalsIgnoreCase("FiservDNA")) {

			List<String> ActualInfo = AppSpecificFunctions
					.getDropDownValues("//div[@id='flxAccountListItem_flxAccountNameWrapper']/div/div", "xpath");
			accType = ActualInfo.get(0);
			accNum = ActualInfo.get(1).substring(15);
			WebElement ele = AppSpecificFunctions
					.listElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_accounts"), 0);
			ele.click();
		}
		AppElement ViewAccInfo = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_viewAccountInfo"));
		ViewAccInfo.click();
		AppElement lblaccNum = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_lblaccNumber"));
		sa.assertEquals(lblaccNum.getText(), "Account Number");

		AppElement AccNum = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_accNumber"));
		sa.assertEquals(accNum, AccNum.getText());

		AppElement accHolder = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountDetails_accHolder"));
		sa.assertEquals(accType, accHolder.getText());

		sa.assertAll();

	}
	
	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Verify Advertisement display once user logged into the OLB")
	public void verifyAdsOnLogin() throws Exception {
		SoftAssert sa = new SoftAssert();
		boolean adspresent = landing.isAdsPresent();
		sa.assertEquals(adspresent, true);
		sa.assertAll();
	}
	
	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Verify Advertisement display once user logged into the OLB")
	public void verifyNoAdsDisplayWhenDMIsDown() throws Exception {
		SoftAssert sa = new SoftAssert();
		boolean adspresent = landing.isAdsPresent();
		sa.assertEquals(adspresent, false);
		sa.assertAll();
	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Verify whether the user can navigate to the specific url on clicking the advertisement.")
	public void verifyNewWindowOpen() throws Exception {
		SoftAssert sa = new SoftAssert();
		//String expectedURL = landing.getAdsURL();
		WebElement element = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1")));
		//AppElement ads = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgAd1"));
		Actions action = new Actions(Driver.getInstance().getAppy());
		action.moveToElement(element).click().perform();
		Driver driver = Driver.getInstance();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getAppy().getWindowHandles());
		driver.getAppy().switchTo().window(tabs2.get(1));
		Thread.sleep(5000);
		String acutalURL = driver.getAppy().getCurrentUrl();
		sa.assertEquals(acutalURL, "http://www.w3school.com/");
		driver.getAppy().close();
		driver.getAppy().switchTo().window(tabs2.get(0));
		sa.assertAll();
	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Verify Ads changes if user clicked on the different tab.")
	public void verifyAdsChange() throws Exception {
		SoftAssert sa = new SoftAssert();
		AppElement ads1 = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgPag1"));
		String expectedURL1 = ads1.getAttribute("src");
		AppElement ads2 = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgPag2"));
		String expectedURL2 = ads2.getAttribute("src");
		sa.assertNotEquals(expectedURL1, expectedURL2);
		AppElement ads3 = new AppElement(OnlineBankingWidgetId.getWidgetId("frmAccountsLanding_banner_imgPag3"));
		String expectedURL3 = ads3.getAttribute("src");
		//sa.assertNotEquals(expectedURL2, expectedURL3);
		sa.assertAll();
	}
	
	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Verify whether the user can navigate to the specific url on clicking the advertisement.Verify navigation from each advertisement if there are multiple advertisement")
	public void verifyNewWindowOpenForMultipleAds() throws Exception {
		SoftAssert sa = new SoftAssert();
		Driver driver = Driver.getInstance();
		//get current window name
		String currentWindow = driver.getAppy().getWindowHandle();
		//click on the first ads image
		landing.clickOnFirstAdsImage();
		//pass current window and switch to the new window
		landing.changeControlToNewAdsWindow(currentWindow);
		String acutalURL = driver.getAppy().getCurrentUrl();
		//sa.assertEquals(acutalURL, "http://www.w3school.com/");
		driver.getAppy().close();
		landing.changeFocusToParentWindow(currentWindow);
		//Click on the second tab
		landing.clickOnSecondAdsTab();
		//Click on the image to open ads
		landing.clickOnSecondAdsImage();
		//pass current window and switch to the new window
		landing.changeControlToNewAdsWindow(currentWindow);
		String acutalURL2 = driver.getAppy().getCurrentUrl();
		//sa.assertEquals(acutalURL, "http://www.w3school.com/");
		driver.getAppy().close();
		landing.changeFocusToParentWindow(currentWindow);
		//Click on the third ads tab.
		landing.clickOnThirdAdsTab();
		//Click on the third ads image to open
		landing.clickOnThirdAdsImage();
		//pass current window and switch to the new window
		landing.changeControlToNewAdsWindow(currentWindow);
		String acutalURL3 = driver.getAppy().getCurrentUrl();
		//sa.assertEquals(acutalURL, "http://www.w3school.com/");
		driver.getAppy().close();
		landing.changeFocusToParentWindow(currentWindow);
		sa.assertAll();
	}

	/**
	 * @author kadamm
	 * @throws Exception
	 */
	@Test(description = "Timeout on the advertisement screen.")
	public void verifyTimeoutOnAds() throws Exception {
		SoftAssert sa = new SoftAssert();
		Driver driver = Driver.getInstance();
		//get current window name
		String currentWindow = driver.getAppy().getWindowHandle();
		//click on the first ads image
		landing.clickOnFirstAdsImage();
		//pass current window and switch to the new window
		landing.changeControlToNewAdsWindow(currentWindow);
		AppSpecificFunctions.waitForClientSessionTimeout();
		driver.getAppy().close();
		landing.changeFocusToParentWindow(currentWindow);
		sa.assertTrue(frmLogin.isLoginForm(), "verify session expiry Failed! Did not navigate to login page after idle timeout");
		sa.assertAll();
	}

}
