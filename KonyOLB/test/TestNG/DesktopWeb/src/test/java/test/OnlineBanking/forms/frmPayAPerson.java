package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmPayAPerson {


  public frmPayAPerson() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_frmPayAPerson"));
  }
public void btnAcknowledgementOne() throws Exception{ 
  AppElement btnAcknowledgementOne=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_btnAcknowledgementOne"));
  btnAcknowledgementOne.click();
  }
public void btnAcknowledgementThree() throws Exception{ 
  AppElement btnAcknowledgementThree=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_btnAcknowledgementThree"));
  btnAcknowledgementThree.click();
  }
public void btnAcknowledgementTwo() throws Exception{ 
  AppElement btnAcknowledgementTwo=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_btnAcknowledgementTwo"));
  btnAcknowledgementTwo.click();
  }
public void btnAddRecipient() throws Exception{ 
  AppElement btnAddRecipient=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_btnAddRecipient"));
  btnAddRecipient.click();
  }
public void btnSendMoneyNewRecipient() throws Exception{ 
  AppElement btnSendMoneyNewRecipient=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_btnSendMoneyNewRecipient"));
  btnSendMoneyNewRecipient.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmPayAPerson_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }



}