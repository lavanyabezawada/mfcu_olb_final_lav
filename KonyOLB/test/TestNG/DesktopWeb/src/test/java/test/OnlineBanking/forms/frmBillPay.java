package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmBillPay {


  public frmBillPay() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_frmBillPay"));
  }
public void btnAddPayee() throws Exception{ 
  AppElement btnAddPayee=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_btnAddPayee"));
  btnAddPayee.click();
  }
public void btnConfirm() throws Exception{ 
  AppElement btnConfirm=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_btnConfirm"));
  btnConfirm.click();
  }
public void btnConfirm1() throws Exception{ 
  AppElement btnConfirm1=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_btnConfirm1"));
  btnConfirm1.click();
  }
public void btnMakeonetimepayment() throws Exception{ 
  AppElement btnMakeonetimepayment=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_btnMakeonetimepayment"));
  btnMakeonetimepayment.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmBillPay_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }


public void segCustomListBoxCategory(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmBillPay_segCustomListBoxCategory"),OnlineBankingWidgetId.getWidgetId("frmBillPay_flxCustomListBox"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 
public void segCustomListBoxPayFrom(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmBillPay_segCustomListBoxPayFrom"),OnlineBankingWidgetId.getWidgetId("frmBillPay_flxCustomListBox"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}