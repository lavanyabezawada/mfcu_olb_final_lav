package test.OnlineBanking.forms;

import test.common.AppCalendar;
import test.common.AppElement;
import test.common.Segment;
import test.common.ListBox;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmNAO {


  public frmNAO() throws Exception {
  AppElement lblHeader=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_frmNAO"));
  }
public void btnCancel() throws Exception{ 
  AppElement btnCancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_btnCancel"));
  btnCancel.click();
  }
public void btnContinueProceed() throws Exception{ 
  AppElement btnContinueProceed=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_btnContinueProceed"));
  btnContinueProceed.click();
  }
public void btnSave() throws Exception{ 
  AppElement btnSave=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_btnSave"));
  btnSave.click();
  }
public void btnTermsAndConditions() throws Exception{ 
  AppElement btnTermsAndConditions=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_btnTermsAndConditions"));
  btnTermsAndConditions.click();
  }


public void rtxDowntimeWarning(String text) throws Exception{
  AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_rtxDowntimeWarning"));
  rtxDowntimeWarning.type(text);
  }
public void rtxTC(String text) throws Exception{
  AppElement rtxTC=new AppElement(OnlineBankingWidgetId.getWidgetId("frmNAO_rtxTC"));
  rtxTC.type(text);
  }


public void segProductsNAO(String label) throws Exception{
		try {
		AppElement.scrollUntilVisible(label);
		Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmNAO_segProductsNAO"),OnlineBankingWidgetId.getWidgetId("frmNAO_btnKnowMore"));
		lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){
		
			//Handle Exception Code Here
		}
	} 

}