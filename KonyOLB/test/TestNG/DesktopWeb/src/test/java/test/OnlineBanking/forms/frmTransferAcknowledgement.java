package test.OnlineBanking.forms;

import test.OnlineBanking.OnlineBankingWidgetId;
import test.common.AppElement;
import test.common.AppLogger;

public class frmTransferAcknowledgement {
	

	static AppLogger appLogger = AppLogger.getInstance();

  public static boolean isfrmTransferAcknowledgement() throws Exception 
  {
  boolean visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmAcknowledgement"));
  return visible;
  }

  /**
   * 
   * @return from account value
   */
  public String returnFromAccountValue()
  {
	  AppElement FromAccount;
	  String finalvalue = null;
	  try {
		  FromAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_keyValueFrom_lblValue"));
		  String fromaccountvalue= FromAccount.getText();
		  int endpoint = fromaccountvalue.indexOf(".");
		  finalvalue = fromaccountvalue.substring(0, endpoint-1);
	} catch (Exception e) {
		appLogger.logError("'FromAccount' field value not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}
	return finalvalue;
  }
  
  /**
   * 
   * @return To account value 
   */
  public String returnToAccountValue()
  {
	  AppElement ToAccount;
	  String finaltvalue = null;
	  try {
		  ToAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_keyValueTo_lblValue"));
		  String toaccountvalue= ToAccount.getText();
		  int endpoint = toaccountvalue.indexOf(".");
		  finaltvalue = toaccountvalue.substring(0, endpoint-1);
	} catch (Exception e) {
		appLogger.logError("'ToAccount' field value not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}
	return finaltvalue;
  }

  /**
   * 
   * @return Transfer Amount value 
   */
  public String returnAmountValue()
  {
	  AppElement Amount;
	  String amountvalue = null;
	  try {
		  Amount=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_keyValueAmount_lblValue"));
		  amountvalue= Amount.getText();
	} catch (Exception e) {
		appLogger.logError("'Amount' field value not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}
	return amountvalue;
  }

  
  /**
   * 
   * @return Transfer Date value 
   */
  public String returnDateValue()
  {
	  AppElement DateField;
	  String datefieldvalue = null;
	  try {
		  DateField=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_keyValuePaymentDate_lblValue"));
		  datefieldvalue= DateField.getText();
	} catch (Exception e) {
		appLogger.logError("'Date' field value not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}
	return datefieldvalue;
  }
  
  /**
   * 
   * @return Transfer Note value 
   */
  public String returnNoteValue()
  {
	  AppElement Note;
	  String toaccountvalue = null;
	  try {
		  Note=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_keyValueNote_lblValue"));
		  toaccountvalue= Note.getText();
	} catch (Exception e) {
		appLogger.logError("'note' field value not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}
	return toaccountvalue;
  }
  
/**
 * Click on the Make Another Transfer button which is under confirm transfer page.
 */
  public void clickOnMakeAnotherTransfer()
  {
	  try {
		 AppElement cancel=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnMakeTransfer"));
		 cancel.click();
		 AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirm_CustomPopup"));
	} catch (Exception e) {
		appLogger.logError("'Cancel' button not found, inside the Acknowledgement Transfer option");
		e.printStackTrace();
	}

  }
  
  /**
   * Click on the View Transfer Activity button which is under confirm transfer page.
   */
    public void clickOnViewTransferActivity()
    {
  	  try {
  		 AppElement modify=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_btnviewTransferActivity"));
  		 modify.click();
  		 AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirm"));
  	} catch (Exception e) {
  		appLogger.logError("'modify' button not found, inside the Acknowledgement Transfer option");
  		e.printStackTrace();
  	}

    }
    
    /**
     * 
     * @return Acknowledgement message
     */
    public String getSuccessMsg()
    {
    	String message=null;
    	  try {
    		 AppElement ackmsg=new AppElement(OnlineBankingWidgetId.getWidgetId("frmAcknowledgement_acknowledgment_lblTransactionMessage"));
    		 message =ackmsg.getText();
    	} catch (Exception e) {
    		appLogger.logError("'Acknowledgement' message label not found, inside the Acknowledgement Transfer option");
    		e.printStackTrace();
    	}
		return message;
    	
    }

}
