package test.OnlineBanking.forms;

import test.common.AppElement;
import test.common.AppLogger;
import test.common.Segment;
import java.io.IOException;
import java.util.List;

import org.kony.qa.stargate.wrappers.appy.Driver;
import org.kony.qa.stargate.wrappers.appy.SgElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import test.OnlineBanking.AppSpecificFunctions;
import test.OnlineBanking.OnlineBankingWidgetId;

public class frmTransfers {

	static AppLogger appLogger = AppLogger.getInstance();

	public static boolean isTransfersForm() {
		boolean visible = false;
		try {
			visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmTransfers_frmTransfers"));
		} catch (Exception e) {
			appLogger.logError("This is not a transfer form as 'frmTransfers_frmTransfers' element not found");
			e.printStackTrace();
		}
		return visible;

	}

	public static boolean isMakeTransfersOption() {
		boolean visible = false;
		try {
			visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_flxTransferForm"));
		} catch (IOException e) {
			appLogger.logError("This is not a make transfer tab as 'frmTransfers_transfermain_maketransfer_flxTransferForm' element not found");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return visible;
	}

	public static boolean isContiueButtonEnabled() {
		boolean visible = false;
		try {
			//		visible = AppElement.isElementVisible("id", OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_btnConfirm"));
			WebElement continueButton = Driver.getInstance().getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_btnConfirm")));
			visible=continueButton.isEnabled();
		} catch (IOException e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_btnConfirm' element not found");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return visible;
	}


	public void rtxDowntimeWarning(String text) throws Exception {
		AppElement rtxDowntimeWarning=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_rtxDowntimeWarning"));
		rtxDowntimeWarning.type(text);
	}

	public void rtxMakeTransferError(String text) throws Exception{
		AppElement rtxMakeTransferError=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_rtxMakeTransferError"));
		rtxMakeTransferError.type(text);
	}

	public void clickOnMyKonyBankAccount()
	{
		AppElement MyKonyAccount;
		try {
			MyKonyAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_btnToMyKonyBankProceed"));
			MyKonyAccount.click();
		} catch (Exception e) {
			appLogger.logError("'MyKonyAccount' option not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	public void clickOnOtherKonyBankAccount()
	{
		AppElement NonKonyAccount;
		try {
			NonKonyAccount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_btnToOtherKonyBankProceed"));
			NonKonyAccount.click();
		} catch (Exception e) {
			appLogger.logError("'NonKonyAccount' option not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	/**
	 * Select 'From' account as per the provided drop down value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public void selectFromAccount(String account)
	{
		AppElement Account;
		Select selectAccount=null;
		try {
			Account = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount"));
			Account.click();
			selectAccount =  new Select((WebElement) Account);
			selectAccount.selectByValue(account);
		} catch (Exception e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_lbxFromAccount' option not found, inside the Transfer money option");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
	}

	/**
	 * Select 'To' account as per the provided drop down value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public void selectToAccount(String account)
	{
		AppElement Account;
		Select selectAccount = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount"));
			Account = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount"));
			Account.click();
			selectAccount =  new Select((WebElement) Account);
			selectAccount.selectByValue(account);
		} catch (Exception e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_lbxToAccount' option not found, inside the Transfer money option");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
	}


	/**
	 * Select 'From' account as per the provided index value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public void selectFromAccountByIndex(int account)
	{
		WebElement Account;
		Select selectAccount=null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount"));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount")));
			//Account.click();
			selectAccount =  new Select(Account);
			selectAccount.selectByIndex(account);
		} catch (Exception e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_lbxFromAccount' option not found, inside the Transfer money option");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
	}

	/**
	 * Select 'To' account as per the provided drop down value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public String selectToAccountByIndex(int account)
	{
		WebElement Account;
		Select selectAccount = null;
		String accoutname = null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount"));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount")));
			//Account.click();
			selectAccount =  new Select(Account);
			selectAccount.selectByIndex(account);
		} catch (Exception e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_lbxToAccount' option not found, inside the Transfer money option");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
		return accoutname;

	}

	/**
	 * Select 'To' account as per the provided drop down value.
	 *
	 * @param account- account to be select 
	 * @throws Exception the exception
	 */

	public void SelectAccountNameByIndex(int account)
	{
		AppElement Account;
		Select selectAccount = null;
		try {
			Account = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxToAccount"));
			Account.click();
			selectAccount =  new Select((WebElement) Account);
			selectAccount.selectByIndex(account);
		} catch (Exception e) {
			appLogger.logError("'frmTransfers_transfermain_maketransfer_lbxToAccount' option not found, inside the Transfer money option");
			selectAccount.selectByIndex(0);
			e.printStackTrace();
		}
	}

	/**
	 * enter amount to the amount field.
	 *
	 * @param Element to locate amount field 
	 * @throws Exception the exception
	 */
	public void enterAmount(String text){
		try {
			AppElement amount = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_tbxAmount"));
			amount.type(text);
		}catch (Exception e) {
			appLogger.logError("'Amount' field not found, inside the Transfer money option");
			e.printStackTrace();
		}

	}

	public void segChangeTransferType(String label) throws Exception{
		try {
			AppElement.scrollUntilVisible(label);
			Segment lblStatusKA = new Segment(OnlineBankingWidgetId.getWidgetId("frmTransfers_segChangeTransferType"),OnlineBankingWidgetId.getWidgetId("frmTransfers_flxChangeTransferType"));
			lblStatusKA.clickSegRowElementbyLabel(label);
		}catch(Exception e){

			e.getStackTrace();
		}
	}

	/**
	 * Select 'Frequence' from drop down value.
	 *
	 * @param Frequency- enter frequency visible text 
	 * @throws Exception the exception
	 */

	public void selectFrequency(String frequency)
	{
		AppElement accountfrequency;
		try {
			accountfrequency = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFrequency"));
			accountfrequency.click();
			Select selectAccount =  new Select((WebElement) accountfrequency);
			selectAccount.selectByVisibleText(frequency);
		} catch (Exception e) {
			appLogger.logError("'Frequency' option not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	public void clickOnSendOnDate()
	{
		AppElement dateField;
		try {
			dateField = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_calSendOn"));
			dateField.click();
		} catch (Exception e) {
			appLogger.logError("'date' field not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	public void clickOnCanceButton()
	{
		AppElement dateField;
		try {
			dateField = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_btnModify"));
			dateField.click();
		} catch (Exception e) {
			appLogger.logError("'date' field not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	/**
	 * enter Note to the amount field.
	 *
	 * @param Element to locate note field 
	 * @throws Exception the exception
	 */
	public void enterNote(String text){
		try {
			AppElement note = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_txtNotes"));
			note.type(text);
		}catch (Exception e) {
			appLogger.logError("'Note' field not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	/**
	 * click on Continue button.
	 *
	 * @param Element to locate note Continue 
	 * @throws Exception the exception
	 */
	public void clickOnContinueButton()
	{
		AppElement dateField;
		try {
			dateField = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_btnConfirm"));
			dateField.click();
		} catch (Exception e) {
			appLogger.logError("'Continue' button not found, inside the Transfer money option");
			e.printStackTrace();
		}
	}

	public void navigateTofrmTransferMoney() throws Exception {

		frmCustomHeaderForm header=new frmCustomHeaderForm();
		header.clickOnMenubar();
		header.clickOnTransfersMenuItem();
		header.clickOnTransferMoney();

	}

	/**
	 * 
	 * @return from account value
	 */
	public String returnFromAccountValue()
	{
		AppElement FromAccount;
		String fromaccountvalue = null;
		try {
			FromAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblFromAccountValue"));
			fromaccountvalue= FromAccount.getText();
		} catch (Exception e) {
			appLogger.logError("'FromAccount' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return fromaccountvalue;
	}

	/**
	 * 
	 * @return To account value 
	 */
	public String returnToAccountValue()
	{
		AppElement ToAccount;
		String toaccountvalue = null;
		try {
			ToAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblSendTo"));
			toaccountvalue= ToAccount.getText();
		} catch (Exception e) {
			appLogger.logError("'ToAccount' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return toaccountvalue;
	}

	/**
	 * 
	 * @return Transfer Amount value 
	 */
	public String returnAmountValue()
	{
		AppElement Amount;
		String amountvalue = null;
		try {
			Amount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblAmount"));
			amountvalue= Amount.getText().substring(1);

		} catch (Exception e) {
			appLogger.logError("'Amount' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return amountvalue;
	}


	/**
	 * 
	 * @return Transfer Date value 
	 */
	public String returnDateValue()
	{
		AppElement DateField;
		String datefieldvalue = null;
		try {
			DateField=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblDate"));
			datefieldvalue= DateField.getText();
		} catch (Exception e) {
			appLogger.logError("'Date' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return datefieldvalue;
	}

	/**
	 * 
	 * @return Transfer Note value 
	 */
	public String returnNoteValue()
	{
		AppElement Note;
		String toaccountvalue = null;
		try {
			Note=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblNoteValue"));
			toaccountvalue= Note.getText();
		} catch (Exception e) {
			appLogger.logError("'note' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return toaccountvalue;
	}

	/**
	 * 
	 * @return Transfer Note value 
	 */
	public String returnStatus()
	{
		AppElement status;
		String toaccountvalue = null;
		try {
			status=new AppElement(OnlineBankingWidgetId.getWidgetId("flxRecentTransfersSelected_lblStatusValue"));
			toaccountvalue= status.getText();
		} catch (Exception e) {
			appLogger.logError("'note' field value not found, inside the recent Transfer transaction");
			e.printStackTrace();
		}
		return toaccountvalue;
	}

	public void clickOnNoteFiled()
	{
		try {
			AppElement note = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_txtNotes"));
			note.click();
		}catch(Exception e)
		{
			appLogger.logError("'Note' field not found, on transfer window");
			e.printStackTrace();
		}
	}

	public void clickOnPreviousArrow()
	{
		try {
			AppElement PreviousArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationPrevious"));
			PreviousArrow.click();
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmConfirmAccount_frmConfirmAccount"));
			Thread.sleep(5000);
		}catch(Exception e)
		{
			appLogger.logError("Previous arrow button not found on on frmTransfers 'Recent' tab.");
			e.printStackTrace();
		}
	}

	public void clickOnNextArrow()
	{
		try {
			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_tablePagination_imgPaginationNext"));
			NextArrow.click();
			Thread.sleep(5000);
		}catch(Exception e)
		{
			appLogger.logError("Next arrow button not found on frmTransfers 'Recent' tab.");
			e.printStackTrace();
		}
	}

	public int getTransactionRecordCount(String id)
	{
		int ListSize=0;
		try {
			String locatorType = "id";
			List<SgElement> elementList = SgElement.getSgElements(locatorType, id);
			ListSize = elementList.size();
		}catch(Exception e)
		{
			appLogger.logError("flxRecentTransfers_flxRow element not found on the 'Recent' tab inside frmtransfer page.");
			e.printStackTrace();
		}
		return ListSize;
	}

	public void clickOnRecentTab()
	{
		try {
			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_btnRecent"));
			NextArrow.click();
			Thread.sleep(5000);
		}catch(Exception e)
		{
			appLogger.logError("Next arrow button not found on frmTransfers 'Recent' tab.");
			e.printStackTrace();
		}
	}


	public void clickOnDeleteRecipient()
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_btnDelete"));
			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_btnDelete"));
			NextArrow.click();
			Thread.sleep(5000);
		}catch(Exception e)
		{
			appLogger.logError("Delete button not found on frmTransfers 'Recipient' tab.");
			e.printStackTrace();
		}
	}

	public void clickOnRecipientDeletePopupYes()
	{

		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_CustomPopup_btnYes"));
			AppElement deleteYes = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_CustomPopup_btnYes"));
			deleteYes.click();
		} catch (Exception e) {
			appLogger.logError("YES button not found on delete recipient popup  window");
			e.printStackTrace();
		}

	}

	public void editRecipient()
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_btnEdit"));
			AppElement NextArrow=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersSelected_btnEdit"));
			NextArrow.click();
		}catch(Exception e)
		{
			appLogger.logError("Edit button not found on frmTransfers 'Recipient' tab.");
			e.printStackTrace();
		}
	}

	public void enterAccountName(String name)
	{
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersEdit_txtAccountName"));
			AppElement SearchInputbox=new AppElement(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersEdit_txtAccountName"));
			AppSpecificFunctions.clearInputTextBox(OnlineBankingWidgetId.getWidgetId("flxExternalAccountsTransfersEdit_txtAccountName"));
			SearchInputbox.type(name);
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getSelectedDropDownValueText()
	{
		WebElement Account;
		Select selectAccount=null;
		String AccountName= null;
		String accountNameText=null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount"));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_maketransfer_lbxFromAccount")));
			//Account.click();
			selectAccount =  new Select(Account);
			WebElement Accountvalue = selectAccount.getFirstSelectedOption();
			AccountName= Accountvalue.getText();
			int num = AccountName.indexOf(".");
			accountNameText= AccountName.substring(0, num-1);
		} catch (Exception e) {
			appLogger.logError("'From' drop down field not found, inside transfer window");
			e.printStackTrace();
		}
		return accountNameText;
	}
	
	public void clickOnScheduleTab()
	{
		AppElement schedule;
		try {
			schedule = new AppElement(OnlineBankingWidgetId.getWidgetId("frmTransfers_transfermain_btnScheduled"));
			schedule.click();
		} catch (Exception e) {
			appLogger.logError("'Schedule' option not found, inside the Transfer History option");
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return Schedule Transfer transaction Amount value 
	 */
	public String getScheduleTransactionAmountValue()
	{
		AppElement Amount;
		String amountvalue = null;
		try {
			Amount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfersSelected_lblAmount"));
			amountvalue= Amount.getText().substring(1);

		} catch (Exception e) {
			appLogger.logError("'Amount' field value not found, inside the schedule Transfer transaction");
			e.printStackTrace();
		}
		return amountvalue;
	}


	/**
	 * 
	 * @return Schedule Transfer transaction Date value 
	 */
	public String getScheduleTransactionDateValue()
	{
		AppElement DateField;
		String datefieldvalue = null;
		try {
			DateField=new AppElement(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfersSelected_lblDate"));
			datefieldvalue= DateField.getText();
		} catch (Exception e) {
			appLogger.logError("'Date' field value not found, inside the schedule Transfer transaction");
			e.printStackTrace();
		}
		return datefieldvalue;
	}
	
	/**
	 * 
	 * @return from account value of schedule transfer transaction
	 */
	public String getScheduleTransactionFromAccountValue()
	{
		AppElement FromAccount;
		String fromaccountvalue = null;
		try {
			FromAccount=new AppElement(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfersSelected_lblFromAccountValue"));
			fromaccountvalue= FromAccount.getText();
		} catch (Exception e) {
			appLogger.logError("'FromAccount' field value not found, inside the Schedule Transfer transaction");
			e.printStackTrace();
		}
		return fromaccountvalue;
	}
	
	/**
	 * 
	 * @return reference number value of schedule transfer transaction
	 */
	public String getScheduleTransactionReferenceNumberValue()
	{
		AppElement ReferenceNumber;
		String referencevalue = null;
		try {
			ReferenceNumber=new AppElement(OnlineBankingWidgetId.getWidgetId("flxScheduledTransfersSelected_lblReferenceNumberValue"));
			referencevalue= ReferenceNumber.getText();
		} catch (Exception e) {
			appLogger.logError("'Reference' number field value not found, inside the Schedule transfer transaction");
			e.printStackTrace();
		}
		return referencevalue;
	}
	
	public String getSelectedDropDownValueText1(String WebElement)
	{
		WebElement Account;
		Select selectAccount=null;
		String AccountName= null;
		String accountNameText=null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId(WebElement));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId(WebElement)));
			//Account.click();
			selectAccount =  new Select(Account);
			WebElement Accountvalue = selectAccount.getFirstSelectedOption();
			AccountName= Accountvalue.getText();
			int num = AccountName.indexOf(".");
			accountNameText= AccountName.substring(0, num-1);
		} catch (Exception e) {
			appLogger.logError("'From' drop down field not found, inside transfer window");
			e.printStackTrace();
		}
		return accountNameText;
	}
	
	public String getSelectedDropDownValueText2(String WebElement)
	{
		WebElement Account;
		Select selectAccount=null;
		String AccountName= null;
		try {
			AppElement.waitForEnable(OnlineBankingWidgetId.getWidgetId(WebElement));
			Driver driver = Driver.getInstance();
			Account = driver.getAppy().findElement(By.id(OnlineBankingWidgetId.getWidgetId(WebElement)));
			//Account.click();
			selectAccount =  new Select(Account);
			WebElement Accountvalue = selectAccount.getFirstSelectedOption();
			AccountName= Accountvalue.getText();
		} catch (Exception e) {
			appLogger.logError("'From' drop down field not found, inside transfer window");
			e.printStackTrace();
		}
		return AccountName;
	}


}