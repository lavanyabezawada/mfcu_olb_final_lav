//package test.OnlineBanking.tests;
//import test.OnlineBanking.*;
//import test.OnlineBanking.forms.frmAccountsLanding;
//import test.OnlineBanking.forms.frmCustomHeaderForm;
//import test.OnlineBanking.forms.frmLogin;
//import org.testng.Assert;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import test.common.SgConfiguration;
//
//public class LoginModule extends OnlineBankingBaseTest {
//
//	SgConfiguration sg = SgConfiguration.getInstance();
//	frmLogin Login = new frmLogin();
//	frmCustomHeaderForm header = new frmCustomHeaderForm();
//
//	@BeforeMethod
//	public void setupBeforeTest() throws Exception {
//		// Check for login form
//		if (!frmLogin.isLoginForm())
//			header.clickOnLogOut();
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with correct credentials")
//	public void login() throws Exception {
//		Login.doLogin(sg.getKeyValue("username"), sg.getKeyValue("password"));
//		Assert.assertTrue(frmAccountsLanding.isLandingForm());
//		header.clickOnLogOut();
//		Assert.assertTrue(frmLogin.isLoginForm());
//		} 
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to verify custom error message if user is not mapped to identity service")
//	public void userNotMappedToConsumerID() throws Exception {
////		frmLogin Login = new frmLogin();
//		Login.enterUsername(sg.getKeyValue("username"));
//		Login.enterPassword(sg.getKeyValue("password"));
//		Login.clickLoginbtn();
//
//	}
//	
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to verify username is case sensitive")
//	public void userNameIsCaseSensitive() throws Exception {
//
//		Login.enterUsername(sg.getKeyValue("Case_username"));
//		Login.enterPassword(sg.getKeyValue("password"));
//		Login.clickLoginbtn();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Incorrect username or password", "Error message is different than expected");	
//	}
//	
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to verify password is case sensitive")
//	public void passwordIsCaseSensitive() throws Exception {
////		FrmLoginKA frmLogin = new FrmLoginKA();
//		Login.enterUsername(sg.getKeyValue("username"));
//		Login.enterPassword(sg.getKeyValue("Case_password"));
//		Login.clickLoginbtn();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Incorrect username or password", "Error message is different than expected");	
//	}
//	
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with wrong password")
//	public void LoginWithWrongPassword() throws Exception {
//
//
//		Login.enterUsername(sg.getKeyValue("username"));
//		Login.enterPassword(sg.getKeyValue("wrongPassword"));
//		Login.clickLoginbtn();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Incorrect username or password", "Error message is different than expected");
//
//	}
//	
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with empty fields")
//	public void LoginWithEmptyFields() throws Exception {
//
////		AppElement appele = new AppElement("element");
//		boolean LoginButton = Login.getLoginButton().isEnabled();
//		Assert.assertFalse(LoginButton, "Login button is enabled");
//	}
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with empty pwd field")
//	public void LoginWithEmptyPwdField() throws Exception {
//
//		Login.enterUsername(sg.getKeyValue("username"));
//		boolean LoginButton = Login.getLoginButton().isEnabled();
//		Assert.assertFalse(LoginButton, "Login button is enabled");
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to verify session expiry")
//	public void sessionExpiry() throws Exception {
//		Login.enterUsername(sg.getKeyValue("username"));
//		Login.enterPassword(sg.getKeyValue("password"));
//		Login.clickLoginbtn();
//
//		Assert.assertTrue(frmAccountsLanding.isLandingForm(),
//				"verify session expiry Failed! Did not navigate to dashboard page");
//		
//		AppSpecificFunctions.waitForClientSessionTimeout();
//		
//		Assert.assertTrue(frmLogin.isLoginForm(),
//				"verify session expiry Failed! Did not navigate to login page after idle timeout");
//
//		String errorLbl = Login.getSessionTimeoutMsg();
//		String expectedErrorLbl = OnlineBankingWidgetId.getWidgetId("frmLoginKA_SessionExpiredMsg");
//		Assert.assertEquals(errorLbl, expectedErrorLbl);
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with space in username")
//	public void LoginWithSpaceInUsername() throws Exception {
//
//		Login.enterUsername(sg.getKeyValue("SP_username"));
//		Login.enterPassword(sg.getKeyValue("password"));
//		Login.clickLoginbtn();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Invalid username", "Error message is different than expected");
//
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to login with space in password")
//	public void LoginWithSpaceInPassword() throws Exception {
//
//		Login.enterUsername(sg.getKeyValue("username"));
//		Login.enterPassword(sg.getKeyValue("SP_password"));
//		Login.clickLoginbtn();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Incorrect username or password", "Error message is different than expected");
//
//	}
//
//	/**
//	 * @author kadamm
//	 * @throws Exception
//	 */
//	@Test(description = "Test case to for TAB functonality")
//	public void tabFunctionality() throws Exception {
//
//		Login.enterUsername(sg.getKeyValue("username"));
//		AppSpecificFunctions.keyBoardTab();
//		//frmLogin.clickLogin();
//		String ErrorMessage = Login.getErrorText();
//		Assert.assertEquals(ErrorMessage, "Incorrect username or password", "Error message is different than expected");
//
//	}
//	
//	/*@Test(description = "Test case to verify remember me")
//	public void rememberMeEnabled() throws Exception {
//		FrmLoginKA frmLogin = new FrmLoginKA();
//		frmLogin.enterUsername(sg.getKeyValue("username"));
//		frmLogin.enterPassword(sg.getKeyValue("password"));
//		frmLogin.enableRememberMe();
//		Object nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmDashboardKA.isFormDashboardKA(),
//				"Remember me Failed! Did not navigate to dashboard page");
//		FrmDashboardKA frmDashboardKA = (FrmDashboardKA) nextForm;
//		FrmLoginKA frmLoginKA = frmDashboardKA.clickLogout();
//		
//		String txt = frmLoginKA.getUsernameText();
//		String expectedTxt = sg.getKeyValue("username");
//		Assert.assertEquals(txt, expectedTxt);
//		
//		
//	}
//
//	@Test(description = "Test case to verify remember me")
//	public void rememberMeDisabled() throws Exception {
//		FrmLoginKA frmLogin = new FrmLoginKA();
//		frmLogin.enterUsername(sg.getKeyValue("username"));
//		frmLogin.enterPassword(sg.getKeyValue("password"));
//		frmLogin.disableRememberMe();
//		Object nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmDashboardKA.isFormDashboardKA(),
//				"Remember me Failed! Did not navigate to dashboard page");
//		FrmDashboardKA frmDashboardKA = (FrmDashboardKA) nextForm;
//		FrmLoginKA frmLoginKA = frmDashboardKA.clickLogout();
//		
//		String txt = frmLoginKA.getUsernameText();
//		String expectedTxt = "";
//		Assert.assertEquals(txt, expectedTxt);
//		
//		
//	}
//*/	
///*	@Test(description = "Test case to update password for expired user")
//	public void resetPasswordForAExpiredUser() throws Exception {
//
//		FrmLoginKA frmLogin = new FrmLoginKA();
//		frmLogin.enterUsername(sg.getKeyValue("expiredUsername"));
//		frmLogin.enterPassword(sg.getKeyValue("expiredPassword"));
//		Object nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmChangePassword.isFrmChangePassword(),
//				"Reset password For a expired user Failed. Did not navigate to change password page");
//		FrmChangePassword frmChangePassword = (FrmChangePassword) nextForm;
//		frmChangePassword.enterPreviousPwd(sg.getKeyValue("expiredPassword"));
//		
//		//Enter wrong new password
//		frmChangePassword.enterNewPwd(sg.getKeyValue("wrongPassword"));
//		String errorLbl = frmChangePassword.getIncorrectPwdErr();
//		String expectedErrorLbl = AdminConsoleNames.getWidgetName("frmChangePassword_IncorrectNewPwd");
//		Assert.assertEquals(errorLbl, expectedErrorLbl);
//		
//		//Correct new pwd
//		frmChangePassword.enterNewPwd(sg.getKeyValue("newPassword"));
//		
//		//Enter wrong confirm new password
//		frmChangePassword.enterConfirmPwd(sg.getKeyValue("wrongPassword"));
//		String errorLabel = frmChangePassword.getIncorrectConfirmPwdErr();
//		String expectedErrorLabel = AdminConsoleNames.getWidgetName("frmChangePassword_IncorrectConfirmPwd");
//		Assert.assertEquals(errorLabel, expectedErrorLabel);
//		
//		//correct confirm pwd
//		frmChangePassword.enterConfirmPwd(sg.getKeyValue("newPassword"));
//		Object changePwdForm = frmChangePassword.clickSubmit();
//		Assert.assertTrue(FrmChangePasswordSuccess.isFrmChangePasswordSuccess(),
//				"Password change text failed! Did not navigate to success page");
//		FrmChangePasswordSuccess frmChangePasswordSuccess = (FrmChangePasswordSuccess) changePwdForm;
//		FrmLoginKA frmLoginKA = frmChangePasswordSuccess.clickReLogin();
//
//		frmLoginKA.enterUsername(sg.getKeyValue("expiredUsername"));
//		frmLoginKA.enterPassword(sg.getKeyValue("newPassword"));
//		nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmDashboardKA.isFormDashboardKA(), "Login failed with new password!");
//		FrmDashboardKA frmDashboardKA = (FrmDashboardKA) nextForm;
//		frmDashboardKA.clickLogout();
//		Assert.assertTrue(FrmLoginKA.isFrmLoginKA());
//
//	}
//	
//	@Test(description = "Test case to update password for a user")
//	public void resetPasswordAfterLogin() throws Exception {
//
//		FrmLoginKA frmLogin = new FrmLoginKA();
//		frmLogin.enterUsername(sg.getKeyValue("username"));
//		frmLogin.enterPassword(sg.getKeyValue("password"));
//		Object nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmDashboardKA.isFormDashboardKA(),
//				"Reset password after login Failed! Did not navigate to dashboard page");
//		FrmDashboardKA frmDashboardKA = (FrmDashboardKA) nextForm;
//
//		// Navigate to Update password page
//		FrmChangePassword frmChangePassword = frmDashboardKA.clickUpdatePassword();
//		Assert.assertTrue(FrmChangePassword.isFrmChangePassword());
//
//		// Enter previous and current passwords
//		frmChangePassword.enterPreviousPwd(sg.getKeyValue("password"));
//		frmChangePassword.enterNewPwd(sg.getKeyValue("newPassword"));
//		frmChangePassword.enterConfirmPwd(sg.getKeyValue("newPassword"));
//
//		// Submit change password details
//		Object fromObj = frmChangePassword.clickSubmit();
//		Assert.assertTrue(FrmChangePasswordSuccess.isFrmChangePasswordSuccess(), "Change password failed.");
//		FrmChangePasswordSuccess frmChangePasswordSuccess = (FrmChangePasswordSuccess)fromObj;
//		FrmLoginKA frmLoginKA = frmChangePasswordSuccess.clickReLogin();
//
//		frmLogin.enterUsername(sg.getKeyValue("username"));
//		frmLogin.enterPassword(sg.getKeyValue("newPassword"));
//		nextForm = frmLogin.clickLogin();
//
//		Assert.assertTrue(FrmDashboardKA.isFormDashboardKA(), "Login failed with new password!");
//		FrmDashboardKA frmDashboard = (FrmDashboardKA) nextForm;
//		frmDashboard.clickLogout();
//		Assert.assertTrue(FrmLoginKA.isFrmLoginKA());
//
//	}*/
//	
///*	@Test(description = "Test case to suspend a user")
//	public void suspendAUser() throws Exception {
//		int AttemptsToSuspend = Integer.parseInt(sg.getKeyValue("attemptsToSuspendAcc"));
//
//		for (int count = 1; count <= 4; count++) {
//			Login.enterUsername(sg.getKeyValue("suspendUser"));
//			Login.enterPassword(sg.getKeyValue("wrongPassword"));
//			Object nextForm = Login.clickLoginbtn();
//			if (count < AttemptsToSuspend) {
//
//				Assert.assertTrue(frmLogin.isLoginForm(),
//						"Suspend a user Failed! Application navigated to form other than loginKA with wrong password.");
//				
//				if (count == AttemptsToSuspend - 1) {
//					String errorLbl = Login.getErrorText();
//					if(errorLbl == null)
//						Assert.fail("Invalid Password error msg not visible");
//					
//					String expectedErrorLbl = OnlineBankingWidgetId.getWidgetId("frmLoginKA_oneAttemptLeftErr");
//					Assert.assertEquals(errorLbl, expectedErrorLbl);
//				} else {
//					String errorLbl = Login.getErrorText();
//					if(errorLbl == null)
//						Assert.fail("Invalid Password error msg not visible");
//					
//					String expectedErrorLbl = OnlineBankingWidgetId.getWidgetId("frmLoginKA_WrongPasswordErr");
//					Assert.assertEquals(errorLbl, expectedErrorLbl);
//				}
//
//			} else if (count == AttemptsToSuspend) {
//				Assert.assertTrue(FrmAccountSuspended.isFrmAccountSuspended(),
//						"Suspend a user Failed! Application did not navigate to suspended account page after "
//								+ AttemptsToSuspend + " incorrect  login attempts");
//				FrmAccountSuspended frmAccountSuspended = (FrmAccountSuspended)nextForm;
//				//frmAccountSuspended.clickBack();
//			}
//		}
//	}*/
//}