/*eslint olblint/image-names:0*/
define([], function () {

    var OLB_CONSTANTS = {
        FUNCTION_WAIT : 100,
        DEFAULT_OFFSET : 0,
        PAGING_ROWS_LIMIT : 10,
        ACCOUNT_LIST_NAME_MAX_LENGTH : 32,
        WIRE_ACTIVITY_LIMIT : 12,
        ASCENDING_KEY : 'asc',
        DESCENDING_KEY : 'desc',
        NOTES_LENGTH:50,
        CALENDAR_ALLOWED_FUTURE_YEARS: 3,
        ALL: 'All',
        PENDING: 'pending',
        SUCCESSFUL: 'successful',
        BOTH: 'Both',
        SEARCH : "Search",
        CURRENCY_NAME: 'Dollar',
        ANY_DATE: 'ANY_DATE',
        CUSTOM_DATE_RANGE: 'CUSTOM_DATE_RANGE',
        IBAN_MINIMUM_LENGTH: 22,
        IBAN_MAXIMUM_LENGTH: 34,
        CHECK_SERIES_SEPARATOR: "-",
        CHECK_REQUEST_TYPES: {
            SINGLE: 'Single',
            SERIES: 'Series'
        },
        UNCATEGORISED: "Uncategorised",
        DISPUTED_CHECKS: "DisputesChecks",
        DISPUTED_TRANSACTIONS: "DisputedTransactions",
        NOTES_MAX_LENGTH: 120,
        Channel : 'Online',
        MAX_CHECKS_COUNT: 50,
        OTPLength: 6,
        MASKED_CARD_NUMBER_LENGTH : 16,
        IMAGES : {
           // BANNER_IMAGE: 'http://pmqa.konylabs.net/KonyWebBanking/banner_img.png',
            SORTING_NEXT: 'sorting_next.png',
            SORTING_PREVIOUS: 'sorting_previous.png',
            SORTING: 'sorting.png',
            CHECKED_IMAGE: "checked_box.png",
            UNCHECKED_IMAGE: "unchecked_box.png",
            ARRAOW_UP : "arrow_up.png",
            ARRAOW_DOWN : "arrow_down.png",
            ACTIVE_UP : "active_up_btn.png",
            DISABLE_UP : "disable_up_btn.png",
            ACTIVE_DOWN : "active_down_btn.png",
            DISABLE_DOWN : "disable_down_btn.png",
            EAZEE_FOOD_CARD: 'eazee_food_card.png',
            GOLDEN_CARD: 'golden_card.png',
            PETRO_CARD: 'petro_card.png',
            PLATINUM_CARD: 'platinum_card.png',
            PREMIUM_CLUB_CREDIT: 'premium_club_credit.png',
            SHOPPING_CARD: 'shopping_card.png',
            EAZEE_FOOD_CARDS: 'eazee_food_cards.png',
            GOLDEN_CARDS: 'golden_cards.png',
            PETRO_CARDS: 'petro_cards.png',
            PLATINUM_CARDS: 'platinum_cards.png',
            PREMIUM_CLUB_CREDITS: 'premium_club_credits.png',
            SHOPPING_CARDS: 'shopping_cards.png',
            RADIOBTN_ACTIVE_SMALL: 'radio_butn_active.png',
            ICON_RADIOBTN: 'icon_radiobtn.png',
            ICON_RADIOBTN_ACTIVE: 'icon_radiobtn_active.png',
            SUCCESS_IMAGE :'bulk_billpay_success.png',
            SERVER_DOWN_IMAGE : 'error_yellow.png'
        },
        FONT_ICONS: {
           CHECBOX_SELECTED: "C",
           CHECBOX_UNSELECTED: "D"
        },
        SKINS: {
            CARDS_ACTIVE_STATUS_LANDING: 'sknLblActiveCard',
            CARDS_LOCKED_STATUS_LANDING: 'sknLblLockedCard',
            CARDS_REPORTED_LOST_STATUS_LANDING: 'sknLblLostCard',
            CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING: 'sknLblLostCard',
            CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING: 'sknLblLostCard',
            CARDS_CANCELLED_STATUS_LANDING: 'sknLblLostCard',
            CARDS_ACTIVE_STATUS_DETAILS: 'sknLblActiveCard',
            CARDS_LOCKED_STATUS_DETAILS: 'sknLblLockedCard',
            CARDS_REPORTED_LOST_STATUS_DETAILS: 'sknLblLostCard',
            CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS: 'sknLblLostCard',
            CARDS_CANCEL_REQUEST_SENT_STATUS_DETAILS: 'sknLblLostCard',
            CARDS_CANCELLED_STATUS_DETAILS: 'sknLblLostCard',
            CARDS_RADIOBTN_LABEL_UNSELECTED: 'sknd3d3d320pxolbfonticons',
            CARDS_RADIOBTN_LABEL_SELECTED: 'sknlblOLBFonts3343A820px',
            STOPPAYMENTS_UNSELECT_TAB: 'sknunselctednormala0a0a015px',
            STOPPAYMENTS_SELECT_TAB: 'sknBtnAccountSummarySelectedmod',
            STOPPAYMENTS_UNSELECT_HOVER: 'sknBtnA0A0A015pxSSPBgf8f7f8',
            LOGIN_RESEND_OTP_DISABLED: 'sknBtnBlockedBg0SSP0273e3Pr40',
            LOGIN_RESEND_OTP_ENABLED: 'sknBtnSSP0273E317PxBg0',
            COMMON_TEXTBOX_ERROR:'skntxtSSP424242BorderFF0000Op100Radius2px',
            COMMON_TEXTBOX_NOERROR:'sknTbxSSPffffff15PxBorder727272opa20',
            COMMON_TEXTBOX_HOVER:'sknBGFFFFFBdrE3E3E3BdrRadius2PxHover',
            INTERACTIVE_LINK:'sknlbl3343a8SSP15px',
            ALTERNATEACTIONS_NEW_SKIN: 'sknFontNewSigninOptions0273e3',
            CHECKBOX_UNSELECTED_SKIN : 'sknOlbFontsIconse3e3e3',
            CHECKBOX_SELECTED_SKIN : 'sknFontIconCheckBoxSelected'
        },
        
        CUSTOMER_TYPE : {
            BUSINESSUSER : 'Micro Business'
        },

        ACCOUNT_TYPE : {
            SAVING : 'Savings',
            CHECKING: 'Checking',
            CREDITCARD: 'CreditCard',
            LOAN : 'Loan',
            MORTGAGE : 'Mortgage',
            DEPOSIT : 'Deposit',
            OTHER : 'Other',
            EXTERNAL: 'External',
            CURRENT: 'Current',
            LINE_OF_CREDIT: 'Line of Credit'
        },

        TRANSACTION_TYPE : {
            CHECKS: 'Checks',
            DEPOSITS:  'Deposits',
            TRANSFERS: 'Transfers',
            WITHDRAWLS: 'Withdrawals',
            PAYMENTS: 'Payments',
            PURCHASES: 'Purchases',
            INTEREST: 'Interest',
            EXTERNALTRANSFER: 'ExternalTransfer',
            INTERNALTRANSFER: 'InternalTransfer',
            BILLPAY: 'BillPay',
            P2P: 'P2P',
            FEES: "Fees",
            INTERESTDEBIT: 'InterestDebit',
            INTERESTCREDIT: 'InterestCredit',
            LOAN : 'Loan',
            STOPCHECKPAYMENTREQUEST: 'StopCheckPaymentRequest',
            DISPUTEDTRANSACTIONSREQUEST: 'DisputedTransactionRequest',
            WIRE: 'Wire',
            DEPOSIT : 'Deposit',
            CARDLESS: 'Cardless',
            CHECKWITHDRAWAL : 'CheckWithdrawal',
            WITHDRAWL: 'Withdrawal',
            RECEIVEDP2P: 'ReceivedP2P',
            RECEIVEDREQUEST : 'ReceivedRequest',
            FEE: 'Fee',
            POS: 'POS',
            TAX: 'Tax',
            INTERNETTRANSACTION: 'InternetTransaction',
            CARDPAYMENT:'CardPayment',
            CREDIT: 'Credit'
        },

        TRANSACTION_STATUS: {
            INPROGRESS: "In-Progress",
            SUCCESSFUL: "Successful",
            CLEARED:"Cleared",
            REQUESTEXPIRED:"Request Expired",
            FAILED:"Failed",
            PENDING: "Pending",
            STOPPED: "Stopped"
        },
        TRANSACTION_RECURRENCE:{
            ONCE: "Once",
            DAILY: "Daily",
            WEEKLY: "Weekly",
            BIWEEKLY: "BiWeekly",
            MONTHLY: "Monthly",
            YEARLY: "Yearly",
            HALFYEARLY: "Half Yearly",
            QUARTERLY: "Quarterly",
            EVERYTWOWEEKS: "Every Two Weeks"
        },
        ACTION: {
            
            ACCOUNT_PREFERENCES: 'Account Preferences',
            EDIT_ACCOUNT: 'Edit Account',
            TRANSFER_MONEY: 'Transfer Money',
            VIEW_STATEMENTS: 'View Statements',
            UPDATE_ACCOUNT_SETTINGS: 'Update Account Settings',
            ORDER_CHECKS: 'Order Checks',
            REQUEST_OR_REPLACE_CARD: 'Request Card/Replace Card',
            GET_ASSISTANCE: 'Get Assistance',
            ECHECK_OR_ROUTING_DETAILS: 'eCheck/Routing Details',
            REWARDS_POINTS: 'Reward Points',
            PAY_A_PERSON_OR_SEND_MONEY: 'Send Money',
            PAY_DUE_AMOUNT: 'Pay Due Amount',
            PAY_A_BILL: 'Pay a Bill',
            MANAGE_CARD_OR_CARD_CONTROLS: 'Manage Card/Card Controls',
            REPORT_LOST_OR_STOLEN: 'Report Lost/Stolen',
            SETUP_NEW_PIN: 'Set up New PIN',
            LOCK_OR_DECACTICATE_CARD: 'Lock Card/Deactivate Card (Temporary)',
            TRAVEL_NOTIFICATION: 'Travel Notification',
            REMOVE_ACCOUNT: 'Remove Account',
            DOWNLOAD_STATEMENTS: 'Download Statements',
            ACCOUNT_SETTINGS: 'Account Settings',
            ACCOUNT_SERVICES: 'Account Services',
            PAYOFF_LOAN : 'Payoff Loan',
            EDIT_ACCOUNTS : 'Edit Accounts',
            SCHEDULED_TRANSACTIONS : 'Scheduled Transactions',
            MAKE_A_TRANSFER : 'Make A Transfer',
            VIEW_BILL :  'View Bill',
            SHOW_DISPUTE_TRANSACTION_FORM: 'ShowDisputeTransactionForm',
            SHOW_STOPCHECKS_FORM: 'ShowStopChecksForm',
            STOPCHECKS_PAYMENT: 'Stop Check Payment'
        },
        TRANSFER_TYPES: {
            OWN_INTERNAL_ACCOUNTS: 'OWN_INTERNAL_ACCOUNTS',
            OTHER_INTERNAL_MEMBER: 'OTHER_INTERNAL_MEMBER',
            OTHER_EXTERNAL_ACCOUNT: 'OTHER_EXTERNAL_ACCOUNT',
            INTERNATIONAL_ACCOUNT: 'INTERNATIONAL_ACCOUNT',
            WIRE_TRANSFER: 'WIRE_TRANSFER'
        },
        
      MFA_FLOW_TYPES:{
        LoginMFA : "LoginMFA",
        WIRE_TRANSFERS:"WIRE_TRANSFERS",
        SINGLE_BILL_PAY:"SINGLE_BILL_PAY",
        PAY_A_PERSON:"PAY_A_PERSON",
        TRANSFERS_EURO:"TRANSFERS_EURO",
        BULK_BILL_PAY:"BULK_BILL_PAY",
        ONE_TIME_WIRE_TRANSFERS:"ONE_TIME_WIRE_TRANSFERS",
        TRANSFERS_UPDATE:"TRANSFERS_UPDATE",
        TRANSFERS:"TRANSFERS",
        SECURE_ACCESS_CODE:"SECURE_ACCESS_CODE",
        SECURITY_QUESTIONS:"SECURITY_QUESTIONS",
        DISPLAY_ALL:"DISPLAY_ALL",
        DISPLAY_NO_VALUE:"DISPLAY_NO_VALUE",
        DISPLAY_PRIMARY:"DISPLAY_PRIMARY"
        
      },
        MONTHS_FULL: {
          January: "January",
          February: "February",
          March: "March",
          April: "April",
          May: "May",
          June: "June",
          July: "July",
          August: "August",
          September: "September",
          October: "October",
          November: "November",
          December: "December"
        },  
      
        CONFIG : {
            ACCOUNTS_QUICK_ACTIONS: {
                "Savings": [
                    "Transfer Money",
                    "Stop Check Payment",
                    "View Statements"
                ],
                "Checking": [
                    "Transfer Money",
                    "Send Money",
                    "Pay a Bill",
                    "Stop Check Payment",
                    "View Statements"
                ],
                "CreditCard": [
                    "Pay a Bill",
                    "View Statements"
                    //"Pay Due Amount" Not in scope
                ],
                "Loan": [
                    "Pay Due Amount",
                    "View Statements",
                    "Update Account Settings"
                ],
                "Line of Credit": [
                    //"Pay Due Amount",
                    "View Statements",
                    "Update Account Settings"
                ],
                "Mortgage": [
                    //"Pay Due Amount",
                    "View Statements",
                    "Update Account Settings"
                ],
                "Deposit": [
                    "View Statements",
                    "Update Account Settings"
                ]
            },
            EXTERNAL_ACCOUNT_QUICK_ACTIONS:[
                "Remove Account",
                "Account Preferences",
                "Edit Account",
            ],
            ACCOUNTS_RIGHTSIDE_ACTIONS : {
                "Savings" : [
                    "Scheduled Transactions",
                    "Make A Transfer"
                ],
                "Checking" : [
                    "Scheduled Transactions",
                    "Make A Transfer",
                    "Pay a Bill"
                ],
                "CreditCard" : [
                    "Scheduled Transactions",
                    "View Statements"
                    //"Pay Due Amount" Not in scope
                ],
                "Loan" : [
                    "Pay Due Amount",
                    "View Statements",
                    "Update Account Settings"
                ],
                "Line of Credit" : [
                    //"Pay Due Amount", Not in scope
                    "View Statements",
                    "Update Account Settings"
                ],
                "Mortgage" : [
                    //"Pay Due Amount", Not in scope
                    "View Statements",
                    "Update Account Settings"
                ],
                "Deposit" : [
                    "View Statements",
                    "Update Account Settings"
                ]
            },
            ACCOUNTS_SECONDARY_ACTIONS :   {
                "Savings" : [
                    "View Statements",
                    "Stop Check Payment",
                    "Update Account Settings"
                    //"Order Checks", //Post R4
                    //"Manage Card" //Post R4
                ],
                "Checking" : [
                    "Send Money",
                    "View Statements",
                    "Stop Check Payment",
                    "Update Account Settings",
                    //"Order Checks", //Post R4
                    //"Manage Card" //Post R4
                ],
                "CreditCard" : [
                    "Pay a Bill",
                    "Update Account Settings",
                    //"Manage Card", //Post R4
                    //"Report Lost/Stolen", //Post R4
                    //"Lock Card/Deactivate Card", //Post R4
                ],
                "Loan" : [
                    "Payoff Loan",
                    "Update Account Settings"
                ],
                "Line of Credit" : [
                    //"Payoff Loan", Not in Scope
                    "Update Account Settings"
                ],
                "Mortgage" : [
                    "Update Account Settings"
                ],
                "Deposit" : [
                    "Update Account Settings"
                ]
            }
        },
        CATEGORIES : 
        {
            "Home": "#FEDB64",
            "Transport": "#3645A7",
            "Financial ": "#6753EC",
            "Food": "#D6B9EA",
            "Utilities": "#E87C5E",
            "Health": "#04B6DF",
            "Education": "#E8A75E",
            "Other": "#B160DC",
            "Travel" : "#8ED174"
        },
        CARD_TYPE:{
            'Debit': 'Debit',
            'Credit': 'Credit'
        },
        CARD_ACTION:{
            'Lock':'Lock Card',
            'Unlock':'Unlock Card',
            'Replace':'Replace Card',
            'Report_Lost': 'Report Lost',
            'Cancel': 'Cancel Card',
            'Change_Pin': 'Change Pin',
            'Offline_Change_Pin' : 'Offline Change Pin'
        },
        CARD_REQUEST_CODE:{
            'Replacement' : 'REPLACEMENT',
            'NewPin' : 'NEW_PIN',
            'AccountType_Card' : 'CARD'
        },
        CARD_STATUS:{
            'Active': 'Active',
            'Locked': 'Locked',
            'ReportedLost': 'Reported Lost',
            'ReplaceRequestSent': 'Replace Request Sent',
            'CancelRequestSent': 'Cancel Request Sent',
            'Cancelled': 'Cancelled',
            'Inactive' : 'Inactive',
            'Replaced' : 'Replaced'
        },
        CARD_PRODUCT:{
            'PlatinumCredit': 'My Platinum Credit Card',
            'GoldDebit': 'Gold Debit Card',
            'PremiumCredit': 'Premium Club Credit Card',
            'ShoppingCard': 'Shopping Card',
            'PetroCard': 'Petro Card',
            'FoodCard': 'Eazee Food Card'
        },
        MFA_OPTIONS:{
            'SECURE_ACCESS_CODE': 'Secure Access Code',
            'SECURITY_QUESTIONS': 'Security Questions'
        },
        CHANGE_PIN_OFFLINE_OPTION:{
            'EMAIL': 'E-mail ID',
            'PHONE': 'Phone No',
            'POSTAL_ADDRESS': 'Postal Address'
        },
        CARD_CHANGE_PIN_REASON:{
            'PIN_COMPROMISED': 'PIN Compromised',
            'FORGOT_PIN': 'Lost PIN',
            'OTHER' : 'Other' 
        },
        CARD_REPORTLOST_REASON:{
            'LOST': 'Lost',
            'STOLEN': 'Stolen'
        },
        WireTransferConstants: {
            RECIPIENT_INDIVIDUAL: 'Individual',
            RECIPIENT_BUSINESS: 'Business',
            ACCOUNT_DOMESTIC: 'Domestic',
            ACCOUNT_INTERNATIONAL: 'International',
            DOMESTIC_COUNTRY: 'USA',
            DEFAULT_COUNTRY: 1  // Can be replaced with null basis on backend services.
        }
    };

   
    return OLB_CONSTANTS;
});
