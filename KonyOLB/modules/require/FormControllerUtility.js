define(["ViewConstants", "CommonUtilities"], function(ViewConstants, CommonUtilities) {
  /**
   * enableButton : Method to enable button status
   * @param {object} button widget button object
   */
  var _disableButton = function(button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedSSP0273e315px";
    button.hoverSkin = "sknBtnBlockedSSP0273e315px";
    button.focusSkin = "sknBtnBlockedSSP0273e315px";
  };

  /**
   * enableButton : Method to enable button status
   * @param {object} button widget button object
   */
  var _enableButton = function(button) {
    button.setEnabled(true);
    button.skin = "sknBtnNormalSSPFFFFFF15Px";
    button.hoverSkin = "sknBtnNormalSSPFFFFFFHover15Px";
    button.focusSkin = "sknBtnNormalSSPFFFFFF15PxFocus";
  };
  /**
   * Method to disable button
   */
  var _disableButtonSkinForCSRMode = function() {
    var skin = "sknBtnBlockedSSP0273e315px";
    return skin;
  };
  /**
   * disableButtonActionForCSRMode : Method to disable Segment buttons and buttons in csr mode
   */
  var _disableButtonActionForCSRMode = function() {
    kony.print("This action is not valid for CSR");
  };
  /**
   * disableSegmentButtonSkinForCSRMode : Method to set skin to Segment buttons in csr mode
   * @param {size} : size of the font in button
   * @return {string} skin - skin to be applied to buttons in csr mode
   */
  var _disableSegmentButtonSkinForCSRMode = function(size) {
    var HEIGHT_13 = 13,
      HEIGHT_15 = 15,
      HEIGHT_17 = 17;
    if (size === HEIGHT_13) {
      return "sknBtnSSP3343A813PxBg0CSR";
    }
    if (size === HEIGHT_15) {
      return "sknBtnSSP3343A815PxBg0CSR";
    }
    if (size === HEIGHT_17) {
      return "sknBtnSSP3343A817PxBg0CSR";
    }
  };
  /**
   * _toggleCheckbox - Toggle The check box state on a image widget. Assuming the image widget passed is being used as checkbox.
   * @param {object} imageWidget - The image widget of the check box which should be toggled.
   */
  var _toggleCheckbox = function(imageWidget) {
    imageWidget.src =
      imageWidget.src === ViewConstants.IMAGES.CHECKED_IMAGE
        ? ViewConstants.IMAGES.UNCHECKED_IMAGE
        : ViewConstants.IMAGES.CHECKED_IMAGE;
  };
  var _toggleFontCheckbox = function (imageWidget) {
            imageWidget.text = imageWidget.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED ?  ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED : ViewConstants.FONT_ICONS.CHECBOX_SELECTED; 
            imageWidget.skin = imageWidget.skin === ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN ?  ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN : ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN; 
        };

  /**
   * _isChecked - Returns the state of Checkbox (Image Widget)
   * @member of {CommonUtilities}
   * @param {object} imageWidget - The image widget of the check box of which state is needed.
   * @returns {boolean} - returns true if checkbox is checked and false if checkbox is not checked
   */

  var _isChecked = function(imageWidget) {
    return imageWidget.src === ViewConstants.IMAGES.CHECKED_IMAGE;
  };
  var _isFontIconChecked = function (imageWidget) {
            return imageWidget.text === ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
        };

  /**
   * _setCheckboxState - Sets the state of Checkbox (Image Widget)
   * @param {boolean} state - The state of checkbox which needs to be set.
   */

  var _setCheckboxState = function(state, imageWidget) {
    if (state) {
      imageWidget.src = ViewConstants.IMAGES.CHECKED_IMAGE;
    } else {
      imageWidget.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
    }
  };
  
   /**
         * _setCheckboxState - Sets the state of Checkbox (label Widget)
         * @member of {CommonUtilities}
         * @param {boolean} state - The state of checkbox which needs to be set.
         * @returns {void} - None
         * @throws {}
         */
    
        var _setLblCheckboxState = function (state, labelWidget) {
            if (state) {
                labelWidget.text = ViewConstants.FONT_ICONS.CHECBOX_SELECTED;
              	labelWidget.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
            }
            else {
                labelWidget.text = ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
              	labelWidget.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            }
        };

  /**
   * _isRadioBtnSelected - Returns the state of Radio (Image Widget)
   * @param {object} imageWidget - The image widget of the check box of which state is needed.
   * @returns {boolean} - returns true if checkbox is checked and false if checkbox is not checked
   */

  var _isRadioBtnSelected = function(imageWidget) {
    return (imageWidget.src === ViewConstants.IMAGES.RADIOBTN_ACTIVE_SMALL) || (imageWidget.src ===  ViewConstants.IMAGES.RADIO_BUTTON_ACTIVE) || (imageWidget.src ===  ViewConstants.IMAGES.RADIO_BTN_ACTIVE);
  };

  /**
   * Display loading indicator.
   * @param {object} view : view object of the form .
   */
  var _showProgressBar = function() {
    if ((document.getElementById("__loadingScreenDiv") && document.getElementById("__loadingScreenDiv").style.display === "none") || !document.getElementById("__loadingScreenDiv")){
        kony.application.showLoadingScreen();
    } 
  };

  /**
   * Hide loading indicator.
   * @param {object} view : view object of the form.
   */
  var _hideProgressBar = function() {
    kony.application.dismissLoadingScreen();
  };

  /**
   * Will generate list box data from array of object
   * @param {*[]} arrayOfObject An array of Object 
   * @param {string|function} objectPropertyForKey Property name of the object, or a mapping function to generate keys of listbox
   * @param {string|function} objectPropertyForValue Property name of the object or a mapping function to generate display values of listbox. 
   */

  var _getListBoxDataFromObjects = function (arrayOfObject, keyOrMapperForListBoxKey, keyOrMapperForListBoxValue) {
    function getValue (input, object) {
      if (typeof input === "string") {
        return object[input];
      }
      else if (typeof input === "function") {
        return input(object);
      }
    }
   return arrayOfObject.map(function (object) {
     return [getValue(keyOrMapperForListBoxKey, object), getValue(keyOrMapperForListBoxValue, object)];
   })
  }

  /**
   * Blocks the future date in calendar widget
   * @param {object} calendarWidget  The calendar whose future date is to be blocked.
   * @param {string} startDate Start date in mm/dd/yyyy format, dates greater than this will be blocked for selection.
   */
  var _blockFutureDate = function(calendarWidget, startDate) {
    var dateFormate = applicationManager.getConfigurationManager().frontendDateFormat;
    var DAYS_IN_TWO_MONTHS = 60;
    var finalDate;
    calendarWidget.dateFormat = dateFormate;
    if (startDate) {
      startDate = startDate.split("/");
      var mm = startDate[0];
      var dd = startDate[1];
      var yy = startDate[2];
      finalDate = new Date(yy, mm, dd);
      finalDate.setDate(finalDate.getDate() + DAYS_IN_TWO_MONTHS);
      var maxDate = finalDate.getDay();
      var maxMonth = finalDate.getMonth();
      var maxYear = finalDate.getFullYear();
      if (dateFormate === "dd/mm/yyyy") {
        calendarWidget.enableRangeOfDates(
          [dd, mm, yy],
          [maxDate, maxMonth, maxYear],
          "skn",
          true
        );
        calendarWidget.date = dd + "/" + mm + "/" + yy;
      } else {
        calendarWidget.enableRangeOfDates(
          [dd, mm, yy],
          [maxDate, maxMonth, maxYear],
          "skn",
          true
        );
        calendarWidget.date = mm + "/" + dd + "/" + yy;
      }
    }
  };

  /**
  * Method to update sorting icons in headers with given sortMap.
  * @param {object} sortMap - sorting map (name, imageFlx, clickContainer)  
  * @param {object} viewModel - target column and order(asc/desc).
  */
  var _updateSortFlex = function (sortMap, viewModel) {
    var configurationManager = applicationManager.getConfigurationManager();
      viewModel = viewModel || {};
      if (sortMap && sortMap.length && viewModel) {
          sortMap.forEach(function (item) {
              if (viewModel.sortBy === item.name) {
                  item.imageFlx.src = viewModel.order === configurationManager.OLBConstants.DESCENDING_KEY ? ViewConstants.IMAGES.SORTING_NEXT : ViewConstants.IMAGES.SORTING_PREVIOUS;
              } else {
                  item.imageFlx.src = ViewConstants.IMAGES.SORTING;
              }
          });
      }
  };

  /**
  * Method to attach sort Handlers
  * @param {object} sortMap - sorting map (name, imageFlx, clickContainer)
  * @param {function} clickHandler - on sort click handler
  * @param {object} scope - click handler scope.
  */
  var _setSortingHandlers = function (sortMap, clickHandler, scope) {
      var configurationManager = applicationManager.getConfigurationManager();
      var scopeObj = this;
      sortMap.forEach(function (_item) {
          _item.clickContainer.onClick = clickHandler.bind(scope || scopeObj, event, {
              'sortBy': _item.name,
              'offset' : configurationManager.OLBConstants.DEFAULT_OFFSET
          });
      });
  };

    var ALLOWED_DECIMAL_PLACES = 2;

    /**
     * This class wraps a Kony Text Box and convert it into Controlled Amount Text Field.
     * @param {object} konyTextBoxWidget Reference to a Kony Text Box Widget
     * @class
     */
    var _AmountFieldWrapper = function (konyTextBoxWidget) {
        this.widget = konyTextBoxWidget;
        this.button = null;
        this.onKeyUpListener = null;
        this.onEndEditingListener = null;
        this.onBeginEditingListener = null;
    }

    /**
     * Remove the delimeters from amount text.
     * @param {string} text Text from which delimeters to be removed
     * @returns {string} Text without delimeters
     */
    var _removeDelimeters = function (text) {
      var formatUtilManager = applicationManager.getFormatUtilManager();
        return formatUtilManager.deFormatAmount(text);
    }
    /**
     * Returns a listner which first call the inner event and then calls a interceptor.
     * @param {function} innerEvent 
     * @param {function} interceptor
     * @returns {function} Return a listener which wraps  
     */
    var _wrappedListener = function (innerEvent, interceptor) {
        return function (eventObject) {
            var value = innerEvent(eventObject);
            if (interceptor) {
                interceptor(eventObject);
            }
            return value;
        }
    }

    /**
     * A setter for optional button widget. 
     * @param {object} buttonWidget Reference to kony button widget 
     * @returns {_AmountFieldWrapper} Returns the self object for building 
     */
    _AmountFieldWrapper.prototype.buttonWidget = function (buttonWidget) {
        this.button = buttonWidget;
        return this;
    }

    /**
     * A setter for optional onKeyUp interceptor. 
     * @param {function} onKeyUpListener The interceptor for on keyup
     * @returns {_AmountFieldWrapper} Returns the self object for building 
     */
    _AmountFieldWrapper.prototype.onKeyUp = function (onKeyUpListener) {
        this.onKeyUpListener = onKeyUpListener;
        this.assignListeners();
        return this;
    }

     /**
     * A setter for optional onEndEditing interceptor. 
     * @param {function} onEndEditingListener The interceptor for onEndEditing
     * @returns {_AmountFieldWrapper} Returns the self object for building 
     */
    _AmountFieldWrapper.prototype.onEndEditing = function (onEndEditingListener) {
        this.onEndEditingListener = onEndEditingListener;
        this.assignListeners();
        return this;
    }

    /**
     * A setter for optional onBeginEditing interceptor. 
     * @param {function} onBeginEditingListener The interceptor for onBeginEditingListener
     * @returns {_AmountFieldWrapper} Returns the self object for building 
     */
    _AmountFieldWrapper.prototype.onBeginEditing = function (onBeginEditingListener) {
        this.onBeginEditingListener = onBeginEditingListener;
        this.assignListeners();
        return this;
    }

    
    /**
     * Checks if amount in Amount field is valid or not
     * @returns {boolean} Returns if amount inside wrapped text box is a valid amount or not
     */
    _AmountFieldWrapper.prototype.isValidAmount = function() {
            var amount = this.getAmount();
            return amount !== undefined && amount !== null && !isNaN(amount) && amount !== "";
    };

    /**
     * Returns the amount in number
     * @returns {Number} Returns amount of text box widget in number format
     */
    _AmountFieldWrapper.prototype.getAmountValue = function () {
        return Number(_removeDelimeters(this.widget.text));
    }
    
    /**
     * Returns the amount of the textbox
     * @returns {string} Returns amount of text box widget after removing delimeters
     */
    _AmountFieldWrapper.prototype.getAmount = function () {
        return  _removeDelimeters(this.widget.text);
    }
    

    _AmountFieldWrapper.prototype.assignListeners = function () {
        this.widget.onKeyUp = _wrappedListener(this._onKeyUp.bind(this), this.onKeyUpListener);
        this.widget.onEndEditing = _wrappedListener(this._onEndEditing.bind(this), this.onEndEditingListener)
        this.widget.onBeginEditing = _wrappedListener(this._onBeginEditing.bind(this), this.onBeginEditingListener);
    }


    /**
     * Internal onBeginEditing Event for Amount Field - Remove Delimiters 
     * @param {object} eventObject
     */
    _AmountFieldWrapper.prototype._onEndEditing = function (eventObject) {
        var amount = this.widget.text;
          if(!this.isValidAmount(amount)){
			  if(isNaN(amount)){
					this.widget.text = "";
					return true;
				}
            return false;
          }else{
            this.widget.text = CommonUtilities.formatCurrencyWithCommas(amount, true);          
            return true;
          }
    }

    
    /**
     * Internal onBeginEditing Event for Amount Field - Remove Delimiters 
     * @param {object} eventObject
     */
    _AmountFieldWrapper.prototype._onBeginEditing = function(eventObject){
        this.widget.text = _removeDelimeters(this.widget.text);
    };

    /**
     * Toggles the button state - If a button is available
     */
    _AmountFieldWrapper.prototype.toggleButtonState = function () {
        if (this.button) {
            if (this.isValidAmount()) {
                _enableButton(this.button);
            }
            else {
                _disableButton(this.button);
            }
        }
    }

    /**
     * Internal onKeyUp Event for Amount Field - Removes Extra Decimal Places 
     * @param {object} eventObject
     */
    _AmountFieldWrapper.prototype._onKeyUp = function (eventObject) {
        this.toggleButtonState();
        var amount = this.widget.text;
        var delimeter = applicationManager.getFormatUtilManager().getDecimalSeparator();
        if(amount.indexOf(delimeter) < 0) {
          return;
        }
        else{
          var arr = amount.split(delimeter);
          if(arr[1].length <= ALLOWED_DECIMAL_PLACES) {
            return;
          }
          this.widget.text = (arr[0] + delimeter + arr[1].slice(0, ALLOWED_DECIMAL_PLACES));
          return true;
        }
    }
    /**
     * Wraps a Amount Field and Return the wrapper object
     */
     var _wrapAmountField =  function (widget) {
        return new _AmountFieldWrapper(widget);
    };

  /**
* Method to change skins to reflect error in textbox fields
*@param{string} widgetIds Widget Id/Array of Widget Ids of textbox(s) that are causing error for eg Param can be like [this.view.textbox1, this.view.textbox2] or this.view.textbox
*@param{string} errorFlex Widget Id of Error Flex that needs to be turn on to show error
*/
  var _showErrorForTextboxFields = function (widgetIds, errorFlex) {
    if (Array.isArray(widgetIds)) {
      widgetIds.forEach(function (item) {
        item.skin = ViewConstants.SKINS.BORDER;
      })
    }
    else {
      widgetIds.skin = ViewConstants.SKINS.BORDER;
    }
    if (errorFlex) {
      errorFlex.setVisibility(true);
    }
  };
  /**
  * Method to change skins to hide error in textbox fields on key up
  *@param{string} widgetIds Widget Id/Array of Widget Ids of textbox(s) for eg Param can be like [this.view.textbox1, this.view.textbox2] or this.view.textbox
  *@param{string} errorFlex Widget Id of Error Flex that needs to be turn off to hide error
  */
  var _hideErrorForTextboxFields = function (widgetIds, errorFlex) {
    if (Array.isArray(widgetIds)) {
      widgetIds.forEach(function (item) {
        item.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
      })
    }
    else {
      widgetIds.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
    }
    if (errorFlex) {
      errorFlex.setVisibility(false);
    }
  };

  return {
    disableButton: _disableButton,
    enableButton: _enableButton,
    disableButtonSkinForCSRMode: _disableButtonSkinForCSRMode,
    disableButtonActionForCSRMode: _disableButtonActionForCSRMode,
    disableSegmentButtonSkinForCSRMode: _disableSegmentButtonSkinForCSRMode,
    toggleCheckbox: _toggleCheckbox,
    toggleFontCheckbox: _toggleFontCheckbox,
    isCheckboxChecked: _isChecked,
    isFontIconChecked: _isFontIconChecked,
    setCheckboxState: _setCheckboxState,
    setLblCheckboxState: _setLblCheckboxState,
    isRadioBtnSelected: _isRadioBtnSelected,
    showProgressBar: _showProgressBar,
    hideProgressBar: _hideProgressBar,
    blockFutureDate: _blockFutureDate,
    getListBoxDataFromObjects: _getListBoxDataFromObjects,
    updateSortFlex: _updateSortFlex,
    setSortingHandlers: _setSortingHandlers,
    wrapAmountField: _wrapAmountField,
    showErrorForTextboxFields:_showErrorForTextboxFields,
    hideErrorForTextboxFields:_hideErrorForTextboxFields
  };
});
