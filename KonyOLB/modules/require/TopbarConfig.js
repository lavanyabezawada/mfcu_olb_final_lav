/*eslint require-jsdoc:0
          complexity:0
*/
define([], function () {
    var config = {
        TOP_BAR_CONTEXTUAL_MENU:[
            {
                isVisible: function () {
                    var configurationManager = applicationManager.getConfigurationManager();
                    return !(configurationManager.isKonyBankAccountsTransfer === "false" 
                    && configurationManager.isOtherKonyAccountsTransfer === "false" 
                    && configurationManager.isOtherBankAccountsTransfer === "false" 
                    && configurationManager.isInternationalAccountsTransfer === "false")
                },
                id: "Transfers",
                widget: "flxTransferMoney",
                onClick: function () {
                    applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
                }
            },
            {
                isVisible: function () {
                    var configurationManager = applicationManager.getConfigurationManager();
                    return configurationManager.isBillPayEnabled === "true"
                },
                id: "Bill Pay",
                widget: "flxPayBills",
                onClick: function () {
                    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayModule.presentationController.showBillPayData(null, {
                        show: "AllPayees"
                    });
                }
            },
            {
                isVisible: function () {
                    var configurationManager = applicationManager.getConfigurationManager();                    
                    return configurationManager.ispayAPersonEnabled === "true"
                },
                id: "Pay A Person",
                widget: "flxSendMoney",
                onClick: function () {
                    var PayAPersonModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                  	 PayAPersonModule.presentationController.showPayAPerson();
                }
            },
            {
                isVisible: function () {
                    var configurationManager = applicationManager.getConfigurationManager();                    
                    var wireTransferEligible = applicationManager.getUserPreferencesManager().getWireTransferEligibleForUser();
                    return (wireTransferEligible === "true" || wireTransferEligible === true) 
                    && ((configurationManager.isInternationalWireTransferEnabled === "true") 
                    || (configurationManager.isDomesticWireTransferEnabled === "true"));
                },
                id: "Wire Transfers",
                widget: "flxWireMoney",
                onClick: function () {
                    var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                    WireTransferModule.presentationController.showWireTransfer();
                }
            }
        ],
        USER_ACTIONS: 
        [
            {   
                id: "Profile Settings",
                text: "i18n.ProfileManagement.profilesettings",
                onClick: function () {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.enterProfileSettings("profileSettings");
                }
            },
            {
                id: "Security Settings",
                text: "i18n.ProfileManagement.SecuritySettings",
                onClick: function() {
                    var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    alertsModule.presentationController.enterProfileSettings("securityQuestions");
                }
            },
            {   
                id: "Account Settings",
                text: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                onClick: function () {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    //profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.enterProfileSettings("accountSettings");
                }
            },
            {   
                id: "Alert Settings",
                text: "i18n.ProfileManagement.Alerts",
                onClick: function () {
                    var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    //alertsModule.presentationController.initializeUserProfileClass();
                    alertsModule.presentationController.enterProfileSettings("alertSettings");
                }
            }
        ],
        ACCOUNTS: {
            onClick: function () {
                var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountsModule.presentationController.showAccountsDashboard();
            }
        },
        MESSAGES: {
            onClick: function () {
                var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                 alertsModule.presentationController.showAlertsPage();
            }
        },
        HELP: {
            onClick: function () {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                var formId = kony.application.getCurrentForm().id;
                informationContentModule.presentationController.showOnlineHelp(formId);
            }
        },
        LOGOUT: {
            excludedForms: ['frmContactUsPrivacyTandC', 
                            'frmOnlineHelp', 
                            'frmLocateUs', 
                            'frmNewUserOnboarding'
                        ],
            onClick: function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                var context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
            }
        },
        FEEDBACK: {
            onClick: function () {
                if (kony.application.getCurrentForm() !== "frmCustomerFeedback") {
                    var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                    feedbackModule.presentationController.showFeedback();
                }
                
            }
        }
    }
    return {
        config: config
    }
});