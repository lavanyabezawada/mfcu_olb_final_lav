/* eslint require-jsdoc: 0,
          olblint/enforce-i18n: 0,
          olblint/image-names: 0
*/
define(['FormControllerUtility'], function(FormControllerUtility) {
    var orientationHandler = new OrientationHandler();
    var widgetsMap = [{
            id: "ACCOUNTS",
            menu: "flxAccountsMenu",
            text: "i18n.topmenu.accounts",
            toolTip: "i18n.topmenu.accounts",
            icon: "a",
            subMenu: {
                children: [{
                        id: "MY ACCOUNTS",
                        text: "i18n.hamburger.myaccounts",
                        toolTip: "i18n.hamburger.myaccounts",
                        onClick: function() {
                            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                            accountsModule.presentationController.showAccountsDashboard();
                        }
                    },
                    {
                        id: "Open New Account",
                        text: "i18n.WireTransfer.CreateNewAccount",
                        toolTip: "i18n.WireTransfer.CreateNewAccount",
                        onClick: function() {
                            var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
                            nuoModule.presentationController.showNewAccountOpening();
                        }
                    },
                    {
                        id: "Add External Bank Accounts",
                        text: "i18n.Hamburger.AddExternalBankAccounts",
                        toolTip: "i18n.Hamburger.AddExternalBankAccounts",
                        onClick: function() {
                            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                            accountsModule.presentationController.showExternalBankList();
                        },
                        isVisible: function() {
                            var configurationManager = applicationManager.getConfigurationManager();
                            return configurationManager.isAggregatedAccountsEnabled === "true";
                        }
                    },
                    {
                        id: "Card Management",
                        text: "i18n.hamburger.cardmanagement",
                        toolTip: "i18n.hamburger.cardmanagement",
                        onClick: function() {
                            var cardsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
                            cardsModule.presentationController.navigateToManageCards();
                        }
                    },
                    {
                        id: "Stop Payment Requests",
                        text: "i18n.StoCheckPayments.StopPaymentRequests",
                        toolTip: "i18n.StoCheckPayments.StopPaymentRequests",
                        onClick: function() {
                            var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                            accountsModule.presentationController.showStopPayments();
                        },
                        isVisible: function() {
                            var configurationManager = applicationManager.getConfigurationManager();
                            return configurationManager.enableStopPayments === "true"
                        }
                    },
                    {
                        id: "PFM",
                        text: "i18n.accounts.PersonalFinanceManagement",
                        toolTip: "i18n.accounts.PersonalFinanceManagement",
                        onClick: function() {
                            var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
                            pfmModule.presentationController.initPFMForm();
                        },
                        isVisible: function() {
                            var configurationManager = applicationManager.getConfigurationManager();
                            return configurationManager.isPFMWidgetEnabled === "true";
                        }
                    }
                ]

            },
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return !(configurationManager.isKonyBankAccountsTransfer === "false" &&
                    configurationManager.isOtherKonyAccountsTransfer === "false" &&
                    configurationManager.isOtherBankAccountsTransfer === "false" &&
                    configurationManager.isInternationalAccountsTransfer === "false")
            },
            id: "TRANSFERS",
            menu: "flxTransfers",
            text: "i18n.hamburger.transfers",
            toolTip: "i18n.hamburger.transfers",
            icon: "t",
            subMenu: {
                parent: "flxTransfersSubMenu",
                children: [{
                    id: "Transfer Money",
                    text: "i18n.billPay.BillPayMakeTransfer",
                    toolTip: "i18n.billPay.BillPayMakeTransfer",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
                    },
                }, {
                    id: "Transfer history",
                    text: "i18n.hamburger.transferHistory",
                    toolTip: "i18n.hamburger.transferHistory",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'recent'
                        })
                    }
                }, {
                    id: "External Accounts",
                    text: "i18n.hamburger.externalAccounts",
                    toolTip: "i18n.hamburger.externalAccounts",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'externalAccounts'
                        });
                    },
                    isVisible: function() {
                        var configurationManager = applicationManager.getConfigurationManager();
                        return !(configurationManager.isOtherKonyAccountsTransfer === "false" &&
                            configurationManager.isKonyBankAccountsTransfer === "false" &&
                            configurationManager.isInternationalAccountsTransfer)
                    }
                }, {
                    id: "Add Kony DBX Accounts",
                    text: "i18n.hamburger.addKonyAccount",
                    toolTip: "i18n.hamburger.addKonyAccount",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: "addInternalAccounts"
                        });
                    },
                    isVisible: function() {
                        var configurationManager = applicationManager.getConfigurationManager();
                        return !(configurationManager.isOtherKonyAccountsTransfer === "false" &&
                            configurationManager.isKonyBankAccountsTransfer === "false")
                    }
                }, {
                    id: "Add Non Kony Accounts",
                    text: "i18n.hamburger.addNonKonyAccount",
                    toolTip: "i18n.hamburger.addNonKonyAccount",
                    onClick: function() {
                        applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                            initialView: 'addExternalAccounts'
                        });
                    },
                    isVisible: function() {
                        var configurationManager = applicationManager.getConfigurationManager();
                        return !(configurationManager.isOtherBankAccountsTransfer === "false")
                    }
                }]
            },
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return configurationManager.isBillPayEnabled === "true";
            },
            id: "Bill Pay",
            text: "i18n.billPay.BillPay",
            toolTip: "i18n.billPay.BillPay",
            icon: "B",
            subMenu: {
                children: [{
                    id: "Pay a Bill",
                    text: "i18n.hamburger.payABill",
                    toolTip: "i18n.hamburger.payABill",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        billPayModule.presentationController.showBillPayData();
                    }
                }, {
                    id: "Bill Pay History",
                    text: "i18n.hamburger.billPayHistory",
                    toolTip: "i18n.hamburger.billPayHistory",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        billPayModule.presentationController.showBillPayData(null, "History", true);
                    }
                }, {
                    id: "My Payee List",
                    text: "i18n.hamburger.myPayeeList",
                    toolTip: "i18n.hamburger.myPayeeList",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        billPayModule.presentationController.showBillPayData(null, "ManagePayees", true);
                    }
                }, {
                    id: "Add Payee",
                    text: "i18n.billPay.addPayee",
                    toolTip: "i18n.billPay.addPayee",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        billPayModule.presentationController.showBillPayData(null, "AddPayee");
                    }
                }, {
                    id: "Make One Time Payment",
                    text: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    toolTip: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    onClick: function() {
                        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        billPayModule.presentationController.showBillPayData(null, "MakeOneTimePayment");
                    }
                }]
            }
        },
        {
            id: "ALERTS AND MESSAGES",
            text: "i18n.AlertsAndMessages.AlertsAndMessages",
            toolTip: "i18n.AlertsAndMessages.AlertsAndMessages",
            icon: "m",
            subMenu: {
                parent: "flxTransfersSubMenu",
                children: [{
                    id: "Alerts",
                    text: "i18n.AlertsAndMessages.Alerts",
                    toolTip: "i18n.AlertsAndMessages.Alerts",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage();
                    }
                }, {
                    id: "My Messages",
                    text: "i18n.AlertsAndMessages.Messages",
                    toolTip: "i18n.AlertsAndMessages.Messages",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                            show: "Messages"
                        });
                    }
                }, {
                    id: "New Message",
                    text: "i18n.AlertsAndMessages.NewMessagesMod",
                    toolTip: "i18n.AlertsAndMessages.NewMessagesMod",
                    onClick: function() {
                        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                        alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                            show: "CreateNewMessage"
                        });
                    }
                }]
            }
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return configurationManager.isBusinessOwner === "true" && configurationManager.isBusinessBankingEnabled();
            },
            id: "User Management",
            text: "i18n.common.UserManagement",
            toolTip: "i18n.common.UserManagement",
            icon: "u",
            subMenu: {
                children: [{
                    id: "All Users",
                    text: "i18n.userManagement.allUsers",
                    toolTip: "i18n.userManagement.allUsers",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'showAllUsers'
                        });
                    }
                }, {
                    id: "Create A User",
                    text: "i18n.userManagement.createAuser",
                    toolTip: "i18n.userManagement.createAuser",
                    onClick: function() {
                        var userModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
                        userModule.presentationController.showUserManagent({
                            show: 'createNewUser'
                        });
                    }
                }]
            }
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                return configurationManager.ispayAPersonEnabled === "true"
            },
            id: "Pay a Person",
            text: "i18n.p2p.PayAPerson",
            toolTip: "i18n.p2p.PayAPerson",
            icon: "P",
            subMenu: {
                children: [{
                    id: "Send Money",
                    text: "i18n.Pay.SendMoney",
                    toolTip: "i18n.Pay.SendMoney",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("sendMoneyTab");
                    }
                }, {
                    id: "History",
                    text: "i18n.billPay.History",
                    toolTip: "i18n.billPay.History",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("SentTransactionsTab");
                    }
                }, {
                    id: "My Recipients",
                    text: "i18n.WireTransfer.MyRecepient",
                    toolTip: "i18n.WireTransfer.MyRecepient",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("ManageRecipients");

                    }
                }, {
                    id: "Add Recipient",
                    text: "i18n.PayAPerson.AddRecipient",
                    toolTip: "i18n.PayAPerson.AddRecipient",
                    onClick: function() {
                        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("AddRecipient");

                    }
                }, {
                    id: "Send Money to New Recipient",
                    text: "i18n.PayAPerson.SendMoneyToNewRecipient",
                    toolTip: "i18n.PayAPerson.SendMoneyToNewRecipient",
                    onClick: function() {
                    	var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                        p2pModule.presentationController.showPayAPerson("sendMoneyToNewRecipient");
                    }
                }]
            }
        },
        {
            isVisible: function() {
              return !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile);
            },
            id: "Settings",
            text: "i18n.ProfileManagement.Settingscapson",
            toolTip: "i18n.ProfileManagement.Settingscapson",
            icon: "s",
            subMenu: {
                children: [{
                    id: "Profile Settings",
                    text: "i18n.ProfileManagement.profilesettings",
                    toolTip: "i18n.ProfileManagement.profilesettings",
                    onClick: function() {
                        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        profileModule.presentationController.enterProfileSettings("profileSettings");
                    }
                }, 
                {
                    id: "Security Settings",
                    text: "i18n.ProfileManagement.SecuritySettings",
                    toolTip: "i18n.ProfileManagement.SecuritySettings",
                    onClick: function() {
                        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        alertsModule.presentationController.enterProfileSettings("securityQuestions");
                    }
                },
                {
                    id: "Account Settings",
                    text: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                    toolTip: "i18n.Accounts.ContextualActions.updateSettingAndPreferences",
                    onClick: function() {
                        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        //profileModule.presentationController.initializeUserProfileClass();
                        profileModule.presentationController.enterProfileSettings("accountSettings");
                    }
                },
                {
                    id: "Alert Settings",
                    text: "i18n.ProfileManagement.Alerts",
                    toolTip: "i18n.ProfileManagement.Alerts",
                    onClick: function() {
                        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                        //alertsModule.presentationController.initializeUserProfileClass();
                        alertsModule.presentationController.enterProfileSettings("alertSettings");
                    }
                }]
            }
        },
        {
            isVisible: function() {
                var configurationManager = applicationManager.getConfigurationManager();
                var wireTransferEligible = applicationManager.getUserPreferencesManager().getWireTransferEligibleForUser();
                return (wireTransferEligible === "true" || wireTransferEligible === true) &&
                   ((configurationManager.isInternationalWireTransferEnabled === "true") || (configurationManager.isDomesticWireTransferEnabled === "true"));
            },
            id: "Wire Transfer",
            text: "i18n.transfers.wireTransfer",
            toolTip: "i18n.transfers.wireTransfer",
            icon: "6",
            subMenu: {
                children: [{
                    id: "Make Transfer",
                    text: "i18n.AccountDetails.MAKETRANSFER",
                    toolTip: "i18n.AccountDetails.MAKETRANSFER",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "makeTransfer"
                        })
                    }
                }, {
                    id: "History",
                    text: "i18n.billPay.History",
                    toolTip: "i18n.billPay.History",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "wireTransferHistory"
                        })
                    }
                }, {
                    id: "My Recipients",
                    text: "i18n.WireTransfer.MyRecepient",
                    toolTip: "i18n.WireTransfer.MyRecepient",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "myRecipients"
                        })
                    }
                }, {
                    id: "Add Recipient",
                    text: "i18n.PayAPerson.AddRecipient",
                    toolTip: "i18n.PayAPerson.AddRecipient",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "addRecipient"
                        })
                    }
                }, {
                    id: "Make One Time Payment",
                    text: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    toolTip: "i18n.BillPay.MAKEONETIMEPAYMENT",
                    onClick: function() {
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "oneTimeTransfer"
                        })
                    }
                }]
            }
        },
        {
            id: "About Us",
            text: "i18n.hamburger.aboutus",
            toolTip: "i18n.hamburger.aboutus",
            icon: "A",
            subMenu: {
                children: [{
                        id: "Terms & Conditions",
                        text: "i18n.common.TnC",
                        toolTip: "i18n.common.TnC",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showTermsAndConditions();
                        }
                    }, {
                        id: "Privacy Policy",
                        text: "i18n.footer.privacy",
                        toolTip: "i18n.footer.privacy",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showPrivacyPolicyPage();
                        }
                    }, {
                        id: "Contact Us",
                        text: "i18n.footer.contactUs",
                        toolTip: "i18n.footer.contactUs",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showContactUsPage();
                        }
                    },
                    {
                        id: "Locate Us",
                        text: "i18n.footer.locateUs",
                        toolTip: "i18n.footer.locateUs",
                        onClick: function() {
                            var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
                            locateUsModule.presentationController.showLocateUsPage();

                        }
                    },
                    {
                        id: "FAQs",
                        text: "i18n.footer.faqs",
                        toolTip: "i18n.footer.faqs",
                        onClick: function() {
                            var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                            informationContentModule.presentationController.showFAQs();
                        }
                    },
                    {
                        id: "Feedback",
                        text: "i18n.CustomerFeedback.Feedback",
                        toolTip: "i18n.CustomerFeedback.Feedback",
                        onClick: function() {
                            if (kony.application.getCurrentForm() !== "frmCustomerFeedback") {
                                var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                                feedbackModule.presentationController.showFeedback();
                            }            
                        }
                    }
                ]
            }
        },
    ];

    return {
        config: widgetsMap
    };
});