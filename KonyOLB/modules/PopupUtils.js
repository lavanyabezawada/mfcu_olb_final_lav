var Popupflag = 0;
var accflag = 0;
var accdetflag=0;
var accactflag = 0;
var accntflag = 0;
var flag = 0;
var Disputeflag = 0;
var showSuggestion = false;
var showMoreActionsFlag = 0;

function hidePopups() {
    var currFormObj = kony.application.getCurrentForm();
    var currForm = currFormObj.id;
    if (currForm === "frmAccountsDetails") {
      if(currFormObj.accountTypes.isVisible === true && flag === 1){
          flag = 0;
          //currFormObj.accountTypes.isVisible = false;
        }
      else if(currFormObj.accountTypes.isVisible === true && flag === 0){
        if(currFormObj.accountTypes.segAccountTypes.contentOffsetMeasured.y==0){
      	  currFormObj.accountTypes.isVisible = false;
          if (currFormObj.imgAccountTypes.src === "arrow_up.png" || currFormObj.imgAccountTypes.src === "chevron_up.png"){
            currFormObj.imgAccountTypes.src = "arrow_down.png";
          } 
        }
      }
      if(event.target.id !==  "frmAccountsDetails_imgSecondaryActions" && event.target.id !==  "frmAccountsDetails_flxSecondaryActions") {
            currFormObj.moreActions.isVisible = false;
            flag = 0;
            showMoreActionsFlag = 0;
      }
      currFormObj.accountActionsMobile.isVisible = false;
      if (currFormObj.imgSecondaryActions.src === "arrow_up.png" || currFormObj.imgSecondaryActions.src === "chevron_up.png"){
        currFormObj.imgSecondaryActions.src = "arrow_down.png";
      } 
        // currFormObj.accountInfo.isVisible = false;
      currFormObj.AllForms.setVisibility(false);
      accdetflag=0;
    } 
    else if (currForm === "frmScheduledTransactions") {
      if(currFormObj.imgAccountTypes.src === "arrow_up.png" || currFormObj.imgAccountTypes.src === "chevron_up.png")
         currFormObj.imgAccountTypes.src = "arrow_down.png";
      currFormObj.accountInfo.isVisible = false;
      if(currFormObj.accountTypes.isVisible === true && Popupflag === 1){
          Popupflag = 0;
          //currFormObj.accountTypes.isVisible = false;
        }
      else if(currFormObj.accountTypes.isVisible === true && Popupflag === 0)
      currFormObj.accountTypes.isVisible = false;
      accdetflag=0;
        }
    else if (currForm === "frmAccountsLanding") {
      currFormObj.accountListMenu.top = 280;
      currFormObj.accountListMenu.isVisible = false;
      currFormObj.FavouriteAccountTypes.isVisible = false;
      currFormObj.accountList.lblImgDropdown.text = "O";
      accflag=0;
    }else if(currForm === "frmPayAPerson"){
      currFormObj.secondaryActions.isVisible = false;
      currFormObj.imgDropdown.src = "arrow_down.png";
      currFormObj.AllFormsConfirm.isVisible = false;
      currFormObj.AllForms.isVisible = false;   
    } else if (currForm === "frmLogin") {
      if (currFormObj.lblCheckBox.text === "P"){
        currFormObj.lblCheckBox.text = "O";
        currFormObj.lblCheckBox.origin = true;
      } else {
        currFormObj.lblCheckBox.origin = false;
      }
      currFormObj.flxLanguagePicker.isVisible = false;
      currFormObj.AllForms.setVisibility(false);
      if( showSuggestion ) {
        showSuggestion = false;
      }
      else {
        currFormObj.main.flxUserDropdown.setVisibility( false );
      }
    }
    else if(currForm === "frmAddExternalAccount") {
      currFormObj.externalAccount.AllForms.setVisibility(false);  
      currFormObj.AllForms.setVisibility(false);     
    }
    else if(currForm === "frmAddInternalAccount") {
      currFormObj.internalAccount.AllForms.setVisibility(false);
      currFormObj.AllForms.setVisibility(false);     
    }
    else if(currForm === "frmUserManagement"){
      var FetchedDate = currFormObj.CustomDate.getDate();
      if(FetchedDate !== null)
      currFormObj.CustomDate.setDate(FetchedDate);
      else
      console.log("Invalid date");  
    }
    else if(currForm === "frmBBUsersDashboard") {
      currFormObj.InfoIconPopup.isVisible = false;     
    }
    else if(currForm === "frmBillPay") {
      currFormObj.AllForms.isVisible = false;     
    }
     else if(currForm == "frmNotificationsAndMessages"){
      currFormObj.customheader.flxUserActions.isVisible=false;
      currFormObj.AllForms.isVisible = false;
    }
    else if(currForm == "frmPayDueAmount"){
      currFormObj.customheader.flxUserActions.isVisible=false;
      currFormObj.LoanPayOff.AllForms.isVisible = false;
    }
    else if(currForm == "frmEnrollNow") {
      currFormObj.AllForms.setVisibility(false);
    }
    else if(currForm == "frmProfileManagement") {
      currFormObj.settings.AllForms1.isVisible = false;
      currFormObj.settings.AllForms.isVisible = false;
    }
    else if(currForm == "frmWireTransfer") {
      currFormObj.AllFormsActivateWireTransfer.isVisible = false;
      currFormObj.AllFormsConfirmDetails.isVisible = false;
    }
    else if(currForm == "frmCustomerFeedback") {
      currFormObj.Feedback.AllForms.isVisible = false;
    }
    else if(currForm == "frmAddPayee"){
      if(kony.application.getCurrentBreakpoint() == 1366 || kony.application.getCurrentBreakpoint() == 1380){
        currFormObj.flxPayeeList.setVisibility(false);
        currFormObj.oneTimePay.flxPayeeList.setVisibility(false);
      }
    }
    else if(currForm == "frmMultiFactorAuthentication"){
      currFormObj.InfoPopUpIcon.isVisible = false;
    }
        //function to adjust visibility of contextual menu as per requirement.
    if (currForm !== "frmLogin" && currForm !== "frmEnrollNow" && currForm !== "frmLoginLanguage") {
      if (currFormObj.customheader.topmenu.flxContextualMenu.isVisible === true) {
        currFormObj.customheader.topmenu.flxContextualMenu.isVisible = false;
        currFormObj.customheader.topmenu.imgLblTransfers.text = "O";
        currFormObj.customheader.topmenu.flxTransfersAndPay.skin = "flxHoverSkinPointer";
        currFormObj.customheader.topmenu.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
        if ((currForm === "frmAccountsDetails") || (currForm === "frmAccountsLanding")||(currForm==="frmCardManagement")||(currForm==="frmPersonalFinanceManagement")||(currForm==="frmContactUsPrivacyTandC")||(currForm==="frmOnlineHelp")) {
          currFormObj.customheader.topmenu.flxTransfersAndPay.skin = "flxHoverSkinPointer";
        } else {
          currFormObj.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8Pointer";
        }
      }
      if(currFormObj.customheader.headermenu.imgDropdown.src === "profile_dropdown_uparrow.png")
         currFormObj.customheader.headermenu.imgDropdown.src = "profile_dropdown_arrow.png";
      currFormObj.customheader.flxUserActions.isVisible = false;
    }
  
  }