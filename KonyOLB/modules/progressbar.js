kony = kony || {};
kony.olb = kony.olb || {};
kony.olb.utils = kony.olb.utils || {};

kony.olb.utils.showProgressBar = function (view) {
  if ((document.getElementById("__loadingScreenDiv") && document.getElementById("__loadingScreenDiv").style.display === "none") || !document.getElementById("__loadingScreenDiv")){
      kony.application.showLoadingScreen();
  }
}

kony.olb.utils.hideProgressBar = function (view) {
    kony.application.dismissLoadingScreen();
}